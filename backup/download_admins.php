<?php
$apiObj = new AmoAPI();
if (!$apiObj->testAuth()) {
    Main::setMsg('Необходимо сначала авторизоваться', 'danger');
    Main::header('auth');
}

$adminsFromFile = Main::getArrayFromFile('admins.php');
$adminsFromFileCount = $adminsFromFile ? count($adminsFromFile) : 0;
if (Main::reqGet('del')){
    $del = Main::sendToBackup('admins.php');
    if ($del)
        Main::setMsg ('Успешно');
    else
        Main::setMsg ('Данные для удаления отсутствуют','danger');
    Main::header('download_admins');
}
if (isset($_POST) && $_POST) {
    $saveSession['limit_rows_admins'] = isset($_POST['limit_rows']) ? (int)$_POST['limit_rows'] : 0;
    $saveSession['limit_offset_admins'] = isset($_POST['limit_offset']) ? (int)$_POST['limit_offset'] + $saveSession['limit_rows_admins']  : 0;
    Main::setSession($saveSession, ['limit_rows_admins','limit_offset_admins']);
    $adminsFromApi = $apiObj->getAdmins($_POST);
    dump($adminsFromApi,1);
    if ($adminsFromApi) {
        foreach ($adminsFromApi as $v) {
            $adminsFromFile[$v['id']] = $v;
        }
        Main::setArrayToFile('admins.php', $adminsFromFile);
        Main::setMsg('Успешно');
    } else
        Main::setMsg('Компании по запросу не найдено');
    Main::header('download_companies');
}
?>
<div class="col-md-12">
    <div class="alert alert-info alert-dismissible" role="alert">
        Админов загружено <?= $adminsFromFileCount ?>
    </div>
    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Количество элементов (максимум 500, ограничения апи):</label>
            <input class="form-control" name="limit_rows" type="number" value="<?=Main::getSession('limit_rows_admins')?>">
        </div>
        <div class="form-group">
            <label>С какого элемента начинать:</label>
            <input class="form-control" name="limit_offset" type="number" value="<?=Main::getSession('limit_offset_admins')?>">
        </div>
        <div class="form-group">
            <button class="btn btn-success">Загрузить</button>
        </div>

    </form> 

    <a href="/?r=download_admins&del=1">
        <button class="btn btn-danger">
            Очистить загруженные данные
        </button>
    </a>

</div>