<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AmoAPI
 *
 * @author USER
 */
class AmoAPI {

    public $basicUrl = 'https://gip.amocrm.ru';
    public $cookiePath;

    public function __construct() {
        $this->cookiePath = ROOT_DIR . '/cookie';
    }

    public function req($url = '', $data = null, $method = 'POST') {
       
        $curl = curl_init();

        $dataGetArr[] = 'type=json';
        if ($data && $method == 'GET') {
            foreach ($data as $k => $v) {
                $dataGetArr[] = $k . '=' . $v;
            }
        }
      
        curl_setopt($curl, CURLOPT_URL, $this->basicUrl . '/' . $url . '?' . implode('&', $dataGetArr));
        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookiePath . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookiePath . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
      //  dump($out);
        return json_decode($out, true);
    }

    public function auth($data) {
        $req = $this->req('private/api/auth.php', $data);
        return isset($req['response']['auth']) ? $req['response']['auth'] : false;
    }
    
    public function logout(){
        if(is_file($this->cookiePath . '/cookie.txt'))
            unlink ($this->cookiePath . '/cookie.txt');
    }

    public function testAuth() {
        $req = $this->req('api/v2/account', ['with'=>'users'], 'GET');
        return isset($req['response']['error_code']) && $req['response']['error_code'] == '110' ? false : true;
    }

    public function getCompanies($data=null){
        $req = $this->req('api/v2/companies', $data, 'GET');
        return isset($req['_embedded']['items']) ? $req['_embedded']['items'] : null;
    }
    
    public function getAdmins($data=null){
        $data_ = ['with'=>'users'];
        if ($data)
            $data_ +=$data;
        $req = $this->req('api/v2/account', $data_, 'GET');
        return isset($req['_embedded']['users']) ? $req['_embedded']['users'] : null;
    }
    
    public function getLeads($data=null){
        $req = $this->req('api/v2/leads', $data, 'GET');
     //   dump($req,1);
        return isset($req['_embedded']['items']) ? $req['_embedded']['items'] : null;
    }
    
}
