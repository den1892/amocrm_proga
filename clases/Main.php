<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Main
 *
 * @author USER
 */
class Main {

    public static function reqGet($key = null) {
        $res = $_GET;
        if ($key)
            $res = isset($res[$key]) ? $res[$key] : null;
        return $res;
    }

    public static function getSession($key = null) {
        $res = $_SESSION;
        if ($key)
            $res = isset($res[$key]) ? $res[$key] : null;
        return $res;
    }

    public static function setSession($data = null, $fields = null) {
        if (is_array($data) && count($data)) {
            foreach ($data as $k => $v) {
                if (!$fields || in_array($k, $fields))
                    $_SESSION[$k] = $v;
            }
        }
    }

    public static function setMsg($msg = null, $status = 'success') {
        $_SESSION['msg'] = !$msg ? null : ['status' => $status, 'msg' => $msg];
    }

    public static function getMsg() {
        $msg = isset($_SESSION['msg']) && $_SESSION['msg'] ? $_SESSION['msg'] : null;
        if (isset($_SESSION['msg']))
            unset($_SESSION['msg']);
        return $msg;
    }

    public static function getMenuAliases() {
        return [
            'auth' => 'Авторизация API',
            'index' => 'Главная',
            'upload_file' => 'Загрузка файла',
            'send_message' => 'Отправка письма',
            'download_companies' => 'Выгрузка компаний по АПИ',
        ];
    }

    private static function getSortMenu() {
        return [
            'upload_file' => '1',
            'auth' => '2',
            'download_admins' => '3',
            'download_companies' => '4',
            'send_message' => '5'
        ];
    }
    
    private static function sortMenu($menu=null){
        $res = [];
        $resNoSort = [];
        $sort = self::getSortMenu();
        if ($menu){
            foreach ($menu as $v){
                if (isset($sort[$v['origin_name']]))
                    $res[$sort[$v['origin_name']]] = $v;
                else
                   $resNoSort[] = $v; 
            }
            if ($res)
                ksort ($res);
            if ($resNoSort){
                foreach ($resNoSort as $v)
                    $res[] = $v;
            }
        }
        return $res;
    }

    public static function getMenu() {
        $menuAliases = self::getMenuAliases();
        $res = [];
        $includesFiles = self::scanDir('includes');
        if ($includesFiles) {
            foreach ($includesFiles as $v) {
                $name = str_replace('.php', '', $v['name']);
                $url = $name == 'index' ? '/' : '/?r=' . $name;
                if ($name !== 'index' && $name !== 'error_404') {
                    $res[] = [
                            'name' => isset($menuAliases[$name]) ? $menuAliases[$name] : mb_strtoupper($name),
                            'url' => $url,
                            'origin_name' => $name
                        ];
                }
                
                
            }
        }
        return self::sortMenu($res);
    }

    public static function getHeder($head = 'index') {
        $menuAliases = self::getMenuAliases();
        return isset($menuAliases[$head]) ? $menuAliases[$head] : mb_strtoupper($head);
    }

    public static function scanDir($path = null) {
        $files = [];
        if (is_dir(ROOT_DIR . '/' . $path))
            foreach (scandir(ROOT_DIR . '/' . $path) as $v)
                if (is_file($path . '/' . $v))
                    $files[] = [
                        'name' => $v,
                        'path' => $path . '/' . $v,
                        'allPath' => ROOT_DIR . '/' . $path . '/' . $v
                    ];
        return $files;
    }

    public static function header($file = 'index') {
        $url = file_exists(ROOT_DIR . '/includes/' . $file . '.php') ? '/index.php?r=' . $file : '/index.php?r=error_404';
        header('Location: ' . $url);
        die();
    }

    public static function validateFileName($fileName, array $type) {
        foreach ($type as $v) {
            if (strstr($fileName, '.' . $v))
                return true;
        }
        return false;
    }

    public static function setArrayToFile($path, array $array) {
        $var_str = var_export($array, true);
        return file_put_contents(ROOT_DIR . '/' . $path, '<?php ' . PHP_EOL . ' return ' . $var_str . ';');
    }

    public static function getArrayFromFile($path) {
       
        
        if (is_file(ROOT_DIR . '/' . $path))
            return include ROOT_DIR . '/' . $path;
        else if (is_dir(ROOT_DIR . '/' . $path)){
            $files = self::scanDir($path);
            $res = [];
            if ($files){
                foreach ($files as $v){
                    $res+= self::getArrayFromFile($v['path']);
                }
            }
            return $res;
        }
        else
            return [];
    }

    public static function sendToBackup($file, $backupDir = 'backup') {
        $explodePath = explode('/', $file);
        $file_name = array_pop($explodePath);
        if (is_file(ROOT_DIR . '/' . $file)) {
            if (!is_dir(ROOT_DIR . '/' . $backupDir))
                mkdir(ROOT_DIR . '/' . $backupDir);
            file_put_contents(ROOT_DIR . '/' . $backupDir . '/' . date('Y-m-d_H:j:s') . '_' . $file_name, file_get_contents(ROOT_DIR . '/' . $file));
            unlink(ROOT_DIR . '/' . $file);
            return true;
        } else
            return false;
    }
    
    public static function sendMail($mailAddress,$msg=''){
     //   $mailAddress = 'oshchyp.denys@gmail.com';
        $mail = new PHPMailer\PHPMailer\PHPMailer();
        $mail->isMail();
        $mail->setFrom("admin@buysite.ru", "GIP");
        $mail->addAddress($mailAddress);  
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Список компаний выигравших тендер!';
        $mail->Body = $msg;
        return $mail->send() ? true : $mail->ErrorInfo;
       
    }
    
    public static function getINN($arr=[]){
        if (isset($arr['custom_fields']) && $arr['custom_fields']){
            foreach ($arr['custom_fields'] as $v){
                if (isset($v['name']) && $v['name'] == 'ИНН' && isset($v['values'][0]['value'])){
                    return $v['values'][0]['value'];
                }
            }
        }
        return null;
    }
    
    public static function replaceMSG($msg='',$data=null){
        if ($data){
            foreach ($data as $k=>$v){
                $msg = str_replace('{'.$k.'}', $v, $msg);
            }
        }
        return $msg;
    } 

}
