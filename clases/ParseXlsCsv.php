<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ParseXLS
 *
 * @author admin
 */
class ParseXlsCsv {

    public $path;
    public $methodParse;

    public function setPath($path) {
        $this->path = $path;
        return $this;
    }

    public function setMethodParse($method) {
        $this->methodParse = $method;
        return $this;
    }

    public function parseFile() {
        if (!is_file($this->path))
            return false;
        if (!method_exists($this, $this->methodParse))
            return false;
        $method = $this->methodParse;
        return $this->$method();
    }

    public function parseXLS() {
        require_once(ROOT_DIR . '/libs/phpExcel/PHPExcel/IOFactory.php');
        $xls = PHPExcel_IOFactory::load($this->path);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $arr = [];
        if (count($rowIterator) && $rowIterator) {
            foreach ($rowIterator as $v) {
                $cellIterator = $v->getCellIterator();
                $fields = [];
                foreach ($cellIterator as $cell) {
                    $fields[] = iconv('windows-1251','utf-8',  $cell->getCalculatedValue());
                }
                $arr[] = $fields;
            }
        }
        return $arr ? $arr : false;
    }

    public function parseCSV() {
        $res = [];
        if ($handle = fopen($this->path, 'r')) {
            while ($data = fgetcsv($handle, 0, ';')) {
                if ($data) {
                    
                    foreach ($data as $k => $v) {
                        $data[$k] = iconv('windows-1251','utf-8', $v);
                    }
                    $res[] = $data;
                }
            }
        }
        fclose($handle);
        return $res;
    }

}
