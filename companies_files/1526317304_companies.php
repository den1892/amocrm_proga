<?php 
 return array (
  7801426604 => 
  array (
    'id' => 16933063,
    'name' => 'ООО "ЭкоМедС Северо-Запад"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510646091,
    'updated_at' => 1510646127,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32164047,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164047',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801426604',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32164047,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164047',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933063',
        'method' => 'get',
      ),
    ),
  ),
  7825454576 => 
  array (
    'id' => 16933233,
    'name' => 'ООО  "Санкт-Петербургский Институт Теплоэнергетики"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510646688,
    'updated_at' => 1510646688,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32164249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7825454576',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32164249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933233',
        'method' => 'get',
      ),
    ),
  ),
  7838434062 => 
  array (
    'id' => 16933459,
    'name' => 'ООО "ОТТО БОКК- Санкт-Петербург"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510647118,
    'updated_at' => 1510647147,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32164381,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164381',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838434062',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32164381,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164381',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933459',
        'method' => 'get',
      ),
    ),
  ),
  7838471184 => 
  array (
    'id' => 16933515,
    'name' => 'ООО "САМСОН Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510647272,
    'updated_at' => 1510647380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32164425,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164425',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838471184',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32164425,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164425',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933515',
        'method' => 'get',
      ),
    ),
  ),
  7839042597 => 
  array (
    'id' => 16933571,
    'name' => 'ООО "Испытательная Лаборатория Медтехника"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510647455,
    'updated_at' => 1510647455,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32164485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839042597',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32164485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933571',
        'method' => 'get',
      ),
    ),
  ),
  4345271577 => 
  array (
    'id' => 16933715,
    'name' => 'ООО «Комтрейд»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510647943,
    'updated_at' => 1510647959,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32164645,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164645',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345271577',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32164645,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164645',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933715',
        'method' => 'get',
      ),
    ),
  ),
  4345424696 => 
  array (
    'id' => 16942017,
    'name' => 'Федеральное государственное учреждение "Национальный парк "Лосиный остров"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510724230,
    'updated_at' => 1510724241,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345424696',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942017',
        'method' => 'get',
      ),
    ),
  ),
  5007090179 => 
  array (
    'id' => 16933821,
    'name' => 'ООО  "Трёхсвятское"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510648305,
    'updated_at' => 1510648324,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28305916,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28305916',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5007090179',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28305916,
        1 => 32164787,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28305916,32164787',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933821',
        'method' => 'get',
      ),
    ),
  ),
  7839440220 => 
  array (
    'id' => 16933623,
    'name' => 'ООО  "Директстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510647606,
    'updated_at' => 1510648522,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32164545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839440220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32164545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32164545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16933623',
        'method' => 'get',
      ),
    ),
  ),
  7842114241 => 
  array (
    'id' => 16934001,
    'name' => 'ООО  "НОРД-МАРКЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510649186,
    'updated_at' => 1510649187,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32165065,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165065',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842114241',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32165065,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165065',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16934001',
        'method' => 'get',
      ),
    ),
  ),
  8610026678 => 
  array (
    'id' => 16934205,
    'name' => 'ООО "Строительная компания Наш Дом"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510649861,
    'updated_at' => 1510649861,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32165289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8610026678',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32165289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16934205',
        'method' => 'get',
      ),
    ),
  ),
  8619010658 => 
  array (
    'id' => 16934243,
    'name' => 'ООО "ВОНТ-трейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510649994,
    'updated_at' => 1510650034,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32165351,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165351',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8619010658',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32165351,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165351',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16934243',
        'method' => 'get',
      ),
    ),
  ),
  7839472751 => 
  array (
    'id' => 16934315,
    'name' => 'ООО  "Навитэк"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510650268,
    'updated_at' => 1510650286,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32165443,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165443',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839472751',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32165443,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32165443',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16934315',
        'method' => 'get',
      ),
    ),
  ),
  3305718761 => 
  array (
    'id' => 16643707,
    'name' => 'ООО «Торговый Дом «МетизТорг»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1507719463,
    'updated_at' => 1510652758,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31829947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31829947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 31829945,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=31829945',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3305718761',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31829947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31829947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16643707',
        'method' => 'get',
      ),
    ),
  ),
  7726631320 => 
  array (
    'id' => 16936765,
    'name' => 'ООО  "Канцерна"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510660773,
    'updated_at' => 1510660828,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726631320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936765',
        'method' => 'get',
      ),
    ),
  ),
  5261063389 => 
  array (
    'id' => 16893123,
    'name' => '"СтройПроект"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1510205578,
    'updated_at' => 1510653150,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32119461,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32119461',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5261063389',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32119461,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32119461',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16893123',
        'method' => 'get',
      ),
    ),
  ),
  7726702620 => 
  array (
    'id' => 16932847,
    'name' => 'ООО «Виталек»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510645268,
    'updated_at' => 1510654422,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32163803,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32163803',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726702620',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32163803,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32163803',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16932847',
        'method' => 'get',
      ),
    ),
  ),
  7617009542 => 
  array (
    'id' => 16936381,
    'name' => 'ООО  "ЛИДЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510658982,
    'updated_at' => 1510659041,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31285691,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31285691',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7617009542',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31285691,
        1 => 32168121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31285691,32168121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936381',
        'method' => 'get',
      ),
    ),
  ),
  4029056067 => 
  array (
    'id' => 16936447,
    'name' => 'ООО "Нужно больше золота"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510659281,
    'updated_at' => 1510659282,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168205,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168205',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4029056067',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168205,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168205',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936447',
        'method' => 'get',
      ),
    ),
  ),
  4101093190 => 
  array (
    'id' => 16936479,
    'name' => 'ООО  "Электро-Гост"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510659430,
    'updated_at' => 1510659431,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101093190',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936479',
        'method' => 'get',
      ),
    ),
  ),
  3810326670 => 
  array (
    'id' => 16936511,
    'name' => 'ООО "Профарм"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510659584,
    'updated_at' => 1510659663,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3810326670',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936511',
        'method' => 'get',
      ),
    ),
  ),
  7730151446 => 
  array (
    'id' => 16936545,
    'name' => 'ООО "БизнесБланкСервис С"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510659779,
    'updated_at' => 1510659780,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168325,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168325',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730151446',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168325,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168325',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936545',
        'method' => 'get',
      ),
    ),
  ),
  2462218020 => 
  array (
    'id' => 16936643,
    'name' => 'ООО Охранная фирма "КОДЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510660288,
    'updated_at' => 1510660289,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168431,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168431',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2462218020',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168431,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168431',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936643',
        'method' => 'get',
      ),
    ),
  ),
  1832104733 => 
  array (
    'id' => 16936679,
    'name' => 'ООО  "ЭТЗ "Энергорегион"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510660423,
    'updated_at' => 1510660424,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168473,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168473',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1832104733',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168473,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168473',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936679',
        'method' => 'get',
      ),
    ),
  ),
  7715947120 => 
  array (
    'id' => 16936699,
    'name' => 'ООО "Торговый дом Лето"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510660509,
    'updated_at' => 1510660509,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715947120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936699',
        'method' => 'get',
      ),
    ),
  ),
  3904080541 => 
  array (
    'id' => 16936735,
    'name' => 'ООО "Альголь"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510660666,
    'updated_at' => 1510660667,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3904080541',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936735',
        'method' => 'get',
      ),
    ),
  ),
  7715458715 => 
  array (
    'id' => 16936841,
    'name' => 'ООО  "Форком"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510661016,
    'updated_at' => 1510661016,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32168611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715458715',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32168611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32168611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936841',
        'method' => 'get',
      ),
    ),
  ),
  7729529797 => 
  array (
    'id' => 16936075,
    'name' => 'Алгоритм. Топливный Интегратор., ЗАО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1510657659,
    'updated_at' => 1510661139,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32167759,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32167759',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32167793,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32167793',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729529797',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'крым',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32167759,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32167759',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16936075',
        'method' => 'get',
      ),
    ),
  ),
  2353005631 => 
  array (
    'id' => 16937363,
    'name' => '"Кубань"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1510662031,
    'updated_at' => 1510662294,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32169143,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32169143',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2353005631',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3 осетия северная',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32169143,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32169143',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16937363',
        'method' => 'get',
      ),
    ),
  ),
  5501217490 => 
  array (
    'id' => 16941897,
    'name' => 'ОАО "Аптечная сеть "Омское лекарство"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510722370,
    'updated_at' => 1510722387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5501217490',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16941897',
        'method' => 'get',
      ),
    ),
  ),
  5402010978 => 
  array (
    'id' => 16941905,
    'name' => 'ООО "ИЛ РИНО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510722522,
    'updated_at' => 1510722522,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186129,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186129',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402010978',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186129,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186129',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16941905',
        'method' => 'get',
      ),
    ),
  ),
  7722407214 => 
  array (
    'id' => 16941967,
    'name' => 'ООО  "МЕДИЦИНСКИЕ ТЕХНОЛОГИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510723496,
    'updated_at' => 1510723496,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186207,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186207',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722407214',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186207,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186207',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16941967',
        'method' => 'get',
      ),
    ),
  ),
  7720794060 => 
  array (
    'id' => 16941973,
    'name' => 'ООО "МОСДОРСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510723598,
    'updated_at' => 1510723632,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720794060',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16941973',
        'method' => 'get',
      ),
    ),
  ),
  890502487595 => 
  array (
    'id' => 16941981,
    'name' => 'Государственное бюджетное учреждение города Москвы Территориальный центр социального обслуживания № 11',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510723744,
    'updated_at' => 1510723775,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186219,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186219',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '890502487595',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186219,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186219',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16941981',
        'method' => 'get',
      ),
    ),
  ),
  6168028310 => 
  array (
    'id' => 16941991,
    'name' => 'ООО "СОЛОМКО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510723870,
    'updated_at' => 1510723894,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186235,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186235',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6168028310',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186235,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186235',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16941991',
        'method' => 'get',
      ),
    ),
  ),
  3528009800 => 
  array (
    'id' => 16942027,
    'name' => 'ООО КОММЕРЧЕСКАЯ ФИРМА "ВЕСТ - МАРКЕТ "',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510724350,
    'updated_at' => 1510724380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3528009800',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942027',
        'method' => 'get',
      ),
    ),
  ),
  3906263332 => 
  array (
    'id' => 16942031,
    'name' => 'ООО  "МЕДИЦИНСКИЙ ЦЕНТР "МЕДИКО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510724451,
    'updated_at' => 1510724514,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906263332',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942031',
        'method' => 'get',
      ),
    ),
  ),
  7716835317 => 
  array (
    'id' => 16942041,
    'name' => 'ООО "СИЛА ЧИСТОТЫ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510724632,
    'updated_at' => 1510724632,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186295,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186295',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716835317',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186295,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186295',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942041',
        'method' => 'get',
      ),
    ),
  ),
  9204015386 => 
  array (
    'id' => 16942047,
    'name' => 'ООО "ТИН-БАМ ПРОДСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510724728,
    'updated_at' => 1510724728,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186307,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186307',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9204015386',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186307,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186307',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942047',
        'method' => 'get',
      ),
    ),
  ),
  2221172298 => 
  array (
    'id' => 16942061,
    'name' => 'ООО "Кадастровое бюро"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510724852,
    'updated_at' => 1510724853,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221172298',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942061',
        'method' => 'get',
      ),
    ),
  ),
  277128392 => 
  array (
    'id' => 16942075,
    'name' => 'ООО  "Центр информационных технологий"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510724962,
    'updated_at' => 1510724962,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '277128392',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942075',
        'method' => 'get',
      ),
    ),
  ),
  2127001841 => 
  array (
    'id' => 16942113,
    'name' => 'ООО "АЛЭИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510725378,
    'updated_at' => 1510725378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2127001841',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942113',
        'method' => 'get',
      ),
    ),
  ),
  366508451321 => 
  array (
    'id' => 14851795,
    'name' => 'ДИП-Сервис, ООО   ИП Пекшева.',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1489051860,
    'updated_at' => 1510725567,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29205967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29205967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1492596840,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '366508451321',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская обл(GMT +3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29205967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29205967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14851795',
        'method' => 'get',
      ),
    ),
  ),
  3810312684 => 
  array (
    'id' => 16775684,
    'name' => 'впрок',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1508985292,
    'updated_at' => 1510726690,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31984332,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31984332',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3810312684',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31984332,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31984332',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16775684',
        'method' => 'get',
      ),
    ),
  ),
  3851994362 => 
  array (
    'id' => 16942119,
    'name' => 'ООО УК "ЭЛИТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510725475,
    'updated_at' => 1510726758,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186433,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186433',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3851994362',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186433,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186433',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942119',
        'method' => 'get',
      ),
    ),
  ),
  5410779492 => 
  array (
    'id' => 16942291,
    'name' => 'ООО "Гелиос"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510726876,
    'updated_at' => 1510726876,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410779492',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942291',
        'method' => 'get',
      ),
    ),
  ),
  2308068485 => 
  array (
    'id' => 16942367,
    'name' => 'ООО "ОПТОВО РОЗНИЧНАЯ КОМПАНИЯ "ЧАЙКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510727206,
    'updated_at' => 1510727800,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186789,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186789',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308068485',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186789,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186789',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942367',
        'method' => 'get',
      ),
    ),
  ),
  7820338030 => 
  array (
    'id' => 16942501,
    'name' => 'ООО "А-МЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510728053,
    'updated_at' => 1510728054,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186961,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186961',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7820338030',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186961,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186961',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942501',
        'method' => 'get',
      ),
    ),
  ),
  7130031676 => 
  array (
    'id' => 16942589,
    'name' => 'ООО «ПРОФПРОЕКТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510728548,
    'updated_at' => 1510728549,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187069,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187069',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7130031676',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187069,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187069',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942589',
        'method' => 'get',
      ),
    ),
  ),
  5009076878 => 
  array (
    'id' => 16942617,
    'name' => 'ООО "Рустехзапчасть"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510728688,
    'updated_at' => 1510728688,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187105,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187105',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5009076878',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187105,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187105',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942617',
        'method' => 'get',
      ),
    ),
  ),
  3123394470 => 
  array (
    'id' => 16942701,
    'name' => 'ООО "ТД БЕЛАГРОСЕЛЬХОЗСНАБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510729192,
    'updated_at' => 1510729193,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123394470',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942701',
        'method' => 'get',
      ),
    ),
  ),
  7725237779 => 
  array (
    'id' => 16942721,
    'name' => 'АО  "ОКТАФЛЮИД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510729296,
    'updated_at' => 1510729297,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725237779',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942721',
        'method' => 'get',
      ),
    ),
  ),
  6809006744 => 
  array (
    'id' => 16942735,
    'name' => 'ООО "БЕЛОВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510729412,
    'updated_at' => 1510729413,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187237,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187237',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6809006744',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187237,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187237',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942735',
        'method' => 'get',
      ),
    ),
  ),
  7751021656 => 
  array (
    'id' => 15051843,
    'name' => 'Билдекс, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1490948990,
    'updated_at' => 1510729722,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29480133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29480133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751021656',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29480133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29480133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15051843',
        'method' => 'get',
      ),
    ),
  ),
  7714636482 => 
  array (
    'id' => 16942835,
    'name' => 'ООО "ВИТО-сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510730131,
    'updated_at' => 1510730212,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187369,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187369',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714636482',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187369,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187369',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942835',
        'method' => 'get',
      ),
    ),
  ),
  5027080676 => 
  array (
    'id' => 16943069,
    'name' => 'ООО «ЭМВИКО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510730759,
    'updated_at' => 1510730810,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027080676',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943069',
        'method' => 'get',
      ),
    ),
  ),
  7729627120 => 
  array (
    'id' => 16943087,
    'name' => 'ООО  "ДИСофт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510730881,
    'updated_at' => 1510730881,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187539,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187539',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729627120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187539,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187539',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943087',
        'method' => 'get',
      ),
    ),
  ),
  5027133060 => 
  array (
    'id' => 16943183,
    'name' => 'ООО «Вита-ДЕЗ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510731367,
    'updated_at' => 1510731368,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027133060',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943183',
        'method' => 'get',
      ),
    ),
  ),
  7717161854 => 
  array (
    'id' => 16943209,
    'name' => 'ООО «ПРОМСНАБ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510731512,
    'updated_at' => 1510731543,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717161854',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943209',
        'method' => 'get',
      ),
    ),
  ),
  7728248017 => 
  array (
    'id' => 16945723,
    'name' => 'ООО "Атлант"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510741800,
    'updated_at' => 1510741854,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32195189,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195189',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728248017',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32195189,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195189',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945723',
        'method' => 'get',
      ),
    ),
  ),
  7701535977 => 
  array (
    'id' => 16943285,
    'name' => 'ООО "Системы безопасности"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510731865,
    'updated_at' => 1510731866,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32187793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701535977',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32187793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32187793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943285',
        'method' => 'get',
      ),
    ),
  ),
  7811491634 => 
  array (
    'id' => 16943603,
    'name' => 'ООО "МЕТАЛОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510733502,
    'updated_at' => 1510733502,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811491634',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943603',
        'method' => 'get',
      ),
    ),
  ),
  540420575699 => 
  array (
    'id' => 16943675,
    'name' => 'государственное казенное учреждение Новосибирской области "Управление контрактной системы"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510733795,
    'updated_at' => 1510733807,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188313,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188313',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '540420575699',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188313,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188313',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943675',
        'method' => 'get',
      ),
    ),
  ),
  7724321509 => 
  array (
    'id' => 16943701,
    'name' => 'ООО "Энергорезерв"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510733925,
    'updated_at' => 1510733925,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724321509',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943701',
        'method' => 'get',
      ),
    ),
  ),
  7702601213 => 
  array (
    'id' => 16943739,
    'name' => 'ООО "ХайБитек"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510734087,
    'updated_at' => 1510734087,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702601213',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943739',
        'method' => 'get',
      ),
    ),
  ),
  6312012676 => 
  array (
    'id' => 16943763,
    'name' => 'ООО ЧАСТНАЯ ОХРАННАЯ ОРГАНИЗАЦИЯ "СЕМЕРКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510734205,
    'updated_at' => 1510734205,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188459,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188459',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312012676',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188459,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188459',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943763',
        'method' => 'get',
      ),
    ),
  ),
  7743529527 => 
  array (
    'id' => 16942531,
    'name' => 'ООО "РН-Карт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510728240,
    'updated_at' => 1510812529,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186995,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186995',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743529527',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186995,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186995',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942531',
        'method' => 'get',
      ),
    ),
  ),
  4826059260 => 
  array (
    'id' => 16943851,
    'name' => 'ООО "ЦВЕТУЩИЕ ФАНТАЗИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510734487,
    'updated_at' => 1510734487,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188517,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188517',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826059260',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188517,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188517',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943851',
        'method' => 'get',
      ),
    ),
  ),
  3702173673 => 
  array (
    'id' => 16943867,
    'name' => 'ООО "ДексТрейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510734601,
    'updated_at' => 1510734602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188541,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188541',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702173673',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188541,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188541',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943867',
        'method' => 'get',
      ),
    ),
  ),
  7806175947 => 
  array (
    'id' => 16943985,
    'name' => 'ООО "АДМИРАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510734938,
    'updated_at' => 1510734938,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188667,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188667',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806175947',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188667,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188667',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16943985',
        'method' => 'get',
      ),
    ),
  ),
  7722626350 => 
  array (
    'id' => 16944031,
    'name' => 'ООО "АСМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510735036,
    'updated_at' => 1510735037,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32188723,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188723',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722626350',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32188723,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32188723',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16944031',
        'method' => 'get',
      ),
    ),
  ),
  6685071025 => 
  array (
    'id' => 16944453,
    'name' => 'ООО ПРОИЗВОДСТВЕННО - КОММЕРЧЕСКАЯ ФИРМА "ВЕСТ - МАРКЕТ "',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510736396,
    'updated_at' => 1510736397,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32189193,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32189193',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6685071025',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32189193,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32189193',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16944453',
        'method' => 'get',
      ),
    ),
  ),
  5042081699 => 
  array (
    'id' => 16945275,
    'name' => 'ООО "ПРОМСТРОЙЭНЕРГО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510739694,
    'updated_at' => 1510739733,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5042081699',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945275',
        'method' => 'get',
      ),
    ),
  ),
  6501175500 => 
  array (
    'id' => 16945313,
    'name' => 'ООО  "РОСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510739901,
    'updated_at' => 1510739970,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501175500',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945313',
        'method' => 'get',
      ),
    ),
  ),
  7813269642 => 
  array (
    'id' => 16945359,
    'name' => 'ООО  "Агентство спортивного маркетинга "Атлет"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510740121,
    'updated_at' => 1510740140,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194767,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194767',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813269642',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194767,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194767',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945359',
        'method' => 'get',
      ),
    ),
  ),
  5906148710 => 
  array (
    'id' => 16945421,
    'name' => 'ООО "ФОРЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510740415,
    'updated_at' => 1510740416,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194829,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194829',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5906148710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194829,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194829',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945421',
        'method' => 'get',
      ),
    ),
  ),
  7806516869 => 
  array (
    'id' => 16945471,
    'name' => 'ООО  "Торгово-транспортная компания "Алсар"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510740620,
    'updated_at' => 1510740649,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194877,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194877',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806516869',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194877,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194877',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945471',
        'method' => 'get',
      ),
    ),
  ),
  6952312660 => 
  array (
    'id' => 16945523,
    'name' => 'ООО  "СК СПЕКТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510740895,
    'updated_at' => 1510740895,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194923,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194923',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6952312660',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194923,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194923',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945523',
        'method' => 'get',
      ),
    ),
  ),
  3906958310 => 
  array (
    'id' => 16945549,
    'name' => 'ООО "Лестер"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510741014,
    'updated_at' => 1510741014,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194955,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194955',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906958310',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194955,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194955',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945549',
        'method' => 'get',
      ),
    ),
  ),
  7721337476 => 
  array (
    'id' => 16945753,
    'name' => 'ООО «ЛИДЕР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510741940,
    'updated_at' => 1510741941,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32195231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721337476',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32195231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945753',
        'method' => 'get',
      ),
    ),
  ),
  '' => 
  array (
    'id' => 16987701,
    'name' => 'ООО "Управление ДОРРЕМСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164221,
    'updated_at' => 1511164222,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236891,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236891',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236891,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236891',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987701',
        'method' => 'get',
      ),
    ),
  ),
  7720374570 => 
  array (
    'id' => 16946197,
    'name' => 'ООО "АР-Проект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510743583,
    'updated_at' => 1510743609,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32195743,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195743',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720374570',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32195743,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195743',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16946197',
        'method' => 'get',
      ),
    ),
  ),
  7733862173 => 
  array (
    'id' => 15607101,
    'name' => 'СТА-Столица, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1496734292,
    'updated_at' => 1510745437,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30249695,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249695',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1510745437,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733862173',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30249695,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249695',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15607101',
        'method' => 'get',
      ),
    ),
  ),
  3662232320 => 
  array (
    'id' => 16946865,
    'name' => 'ООО "Абсолют"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510746405,
    'updated_at' => 1510746512,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32196503,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32196503',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662232320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32196503,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32196503',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16946865',
        'method' => 'get',
      ),
    ),
  ),
  6950033968 => 
  array (
    'id' => 16946963,
    'name' => 'ООО "СПЕЦЭЛЕКТРОСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510746840,
    'updated_at' => 1510746841,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32196593,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32196593',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950033968',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32196593,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32196593',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16946963',
        'method' => 'get',
      ),
    ),
  ),
  7726336860 => 
  array (
    'id' => 16947255,
    'name' => 'ООО  "ГЦ Регион"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510748385,
    'updated_at' => 1510748386,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32196919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32196919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726336860',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32196919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32196919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16947255',
        'method' => 'get',
      ),
    ),
  ),
  8602007946 => 
  array (
    'id' => 16945649,
    'name' => 'ООО "ЮГРА-НЕДРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510741420,
    'updated_at' => 1510809124,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32195077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602007946',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32195077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945649',
        'method' => 'get',
      ),
    ),
  ),
  3662176160 => 
  array (
    'id' => 16952349,
    'name' => 'ООО АСК "АТРИУМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510809447,
    'updated_at' => 1510809591,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201721,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201721',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662176160',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201721,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201721',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952349',
        'method' => 'get',
      ),
    ),
  ),
  1655341268 => 
  array (
    'id' => 16952363,
    'name' => 'ООО СТК "Астория"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510809685,
    'updated_at' => 1510809686,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201749,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201749',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655341268',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201749,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201749',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952363',
        'method' => 'get',
      ),
    ),
  ),
  2320096369 => 
  array (
    'id' => 16952375,
    'name' => 'ООО  «Частная охранная организация «БАСТИОН»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510809797,
    'updated_at' => 1510809797,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201757,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201757',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2320096369',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201757,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201757',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952375',
        'method' => 'get',
      ),
    ),
  ),
  421412137480 => 
  array (
    'id' => 16952397,
    'name' => 'Гндолян Лиана Аветиковна',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510810049,
    'updated_at' => 1510810050,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201787,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201787',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '421412137480',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201787,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201787',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952397',
        'method' => 'get',
      ),
    ),
  ),
  7811635100 => 
  array (
    'id' => 16952415,
    'name' => 'ООО «РЕНТМЕДИКА»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510810186,
    'updated_at' => 1510810226,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811635100',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952415',
        'method' => 'get',
      ),
    ),
  ),
  7730234043 => 
  array (
    'id' => 16952437,
    'name' => 'ООО  «ВЕГА ДИЗАЙН»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510810419,
    'updated_at' => 1510810420,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201837,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201837',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730234043',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201837,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201837',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952437',
        'method' => 'get',
      ),
    ),
  ),
  9710023930 => 
  array (
    'id' => 16952663,
    'name' => 'ООО "ЛАБОРАТОРИЯ ИННОВАЦИЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510810750,
    'updated_at' => 1510810750,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32202837,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32202837',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9710023930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32202837,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32202837',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952663',
        'method' => 'get',
      ),
    ),
  ),
  7456029383 => 
  array (
    'id' => 16952669,
    'name' => 'ООО "ЭФФЕКТИВНОСТЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510810844,
    'updated_at' => 1510810880,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32202845,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32202845',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7456029383',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32202845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32202845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952669',
        'method' => 'get',
      ),
    ),
  ),
  7806445978 => 
  array (
    'id' => 16953461,
    'name' => 'ООО "АДВЕНТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510810970,
    'updated_at' => 1510811009,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203639,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203639',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806445978',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203639,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203639',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953461',
        'method' => 'get',
      ),
    ),
  ),
  7703792715 => 
  array (
    'id' => 16953471,
    'name' => 'ООО  «Пресня-Сервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510811146,
    'updated_at' => 1510811147,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203651,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203651',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703792715',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203651,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203651',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953471',
        'method' => 'get',
      ),
    ),
  ),
  3811152095 => 
  array (
    'id' => 16953493,
    'name' => 'ООО "СибПроектНИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510811346,
    'updated_at' => 1510811395,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203681,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203681',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811152095',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203681,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203681',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953493',
        'method' => 'get',
      ),
    ),
  ),
  2466120410 => 
  array (
    'id' => 16953519,
    'name' => 'ГПКК "Губернские аптеки"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510811582,
    'updated_at' => 1510811597,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203725,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203725',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466120410',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203725,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203725',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953519',
        'method' => 'get',
      ),
    ),
  ),
  519046870910 => 
  array (
    'id' => 16953531,
    'name' => 'Тихонова Валентина Владимировна',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510811665,
    'updated_at' => 1510811690,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203737,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203737',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '519046870910',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203737,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203737',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953531',
        'method' => 'get',
      ),
    ),
  ),
  3906311593 => 
  array (
    'id' => 16945573,
    'name' => 'ООО  "Региональная Газовая Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510741136,
    'updated_at' => 1510812513,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194993,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194993',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906311593',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194993,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194993',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945573',
        'method' => 'get',
      ),
    ),
  ),
  7725835747 => 
  array (
    'id' => 16945495,
    'name' => 'ООО «ВЕЛАР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510740732,
    'updated_at' => 1510812522,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32194899,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194899',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725835747',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32194899,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32194899',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16945495',
        'method' => 'get',
      ),
    ),
  ),
  2311220330 => 
  array (
    'id' => 16942323,
    'name' => 'ООО "КУЗНИЦА ПОДАРКОВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510727010,
    'updated_at' => 1510812535,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311220330',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942323',
        'method' => 'get',
      ),
    ),
  ),
  274018377 => 
  array (
    'id' => 16942087,
    'name' => 'ПАО "Башинформсвязь"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510725102,
    'updated_at' => 1510812543,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32186371,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186371',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274018377',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32186371,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32186371',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16942087',
        'method' => 'get',
      ),
    ),
  ),
  7811401510 => 
  array (
    'id' => 16953663,
    'name' => 'ООО "Вионет"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510812849,
    'updated_at' => 1510812850,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203929,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203929',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811401510',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203929,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203929',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953663',
        'method' => 'get',
      ),
    ),
  ),
  7805100868 => 
  array (
    'id' => 16953669,
    'name' => 'ООО "Петросервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510812917,
    'updated_at' => 1510812918,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805100868',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953669',
        'method' => 'get',
      ),
    ),
  ),
  3444173120 => 
  array (
    'id' => 16953677,
    'name' => 'ООО "Парус-Онлайн"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510812999,
    'updated_at' => 1510812999,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444173120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953677',
        'method' => 'get',
      ),
    ),
  ),
  7736272327 => 
  array (
    'id' => 16953701,
    'name' => 'ООО «ВИВАНА»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510813127,
    'updated_at' => 1510813128,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736272327',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953701',
        'method' => 'get',
      ),
    ),
  ),
  7017309872 => 
  array (
    'id' => 16953791,
    'name' => 'ООО "УКП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510813709,
    'updated_at' => 1510813752,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204087,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204087',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017309872',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204087,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204087',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953791',
        'method' => 'get',
      ),
    ),
  ),
  9102225881 => 
  array (
    'id' => 16953855,
    'name' => 'ООО "ТИГРИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510814156,
    'updated_at' => 1510814176,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204155,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204155',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102225881',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204155,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204155',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953855',
        'method' => 'get',
      ),
    ),
  ),
  5050124321 => 
  array (
    'id' => 16953903,
    'name' => 'ООО "АВТОГРЕЙДЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510814303,
    'updated_at' => 1510814303,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204195,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204195',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5050124321',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204195,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204195',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953903',
        'method' => 'get',
      ),
    ),
  ),
  9705106041 => 
  array (
    'id' => 16953931,
    'name' => 'ООО  "Интеллект групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510814515,
    'updated_at' => 1510814516,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705106041',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953931',
        'method' => 'get',
      ),
    ),
  ),
  6732129444 => 
  array (
    'id' => 16953947,
    'name' => 'ООО "ДОРКОМПЛЕКТ-ЯВИР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510814591,
    'updated_at' => 1510814647,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204245,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204245',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6732129444',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204245,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204245',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953947',
        'method' => 'get',
      ),
    ),
  ),
  164409114421 => 
  array (
    'id' => 16953987,
    'name' => 'Кистанов Евгений Викторович',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510814727,
    'updated_at' => 1510814728,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204279,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204279',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '164409114421',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204279,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204279',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953987',
        'method' => 'get',
      ),
    ),
  ),
  9715276684 => 
  array (
    'id' => 16954013,
    'name' => 'ООО «АДЕЛЬ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510814950,
    'updated_at' => 1510814980,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204319,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204319',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715276684',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204319,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204319',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954013',
        'method' => 'get',
      ),
    ),
  ),
  2634089250 => 
  array (
    'id' => 16954059,
    'name' => 'ООО "ЭНЕРГОАУДИТ СК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510815251,
    'updated_at' => 1510815263,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204377,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204377',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2634089250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204377,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204377',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954059',
        'method' => 'get',
      ),
    ),
  ),
  2703054081 => 
  array (
    'id' => 16954083,
    'name' => 'ООО "ДАЛЬТЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510815383,
    'updated_at' => 1510815384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2703054081',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954083',
        'method' => 'get',
      ),
    ),
  ),
  7710673331 => 
  array (
    'id' => 16954145,
    'name' => 'ООО "Гольф Технолоджи"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510815866,
    'updated_at' => 1510815895,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204493,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204493',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710673331',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204493,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204493',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954145',
        'method' => 'get',
      ),
    ),
  ),
  3811152761 => 
  array (
    'id' => 16954187,
    'name' => 'ООО "Медлайн"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510816130,
    'updated_at' => 1510816162,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204539,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204539',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811152761',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204539,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204539',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954187',
        'method' => 'get',
      ),
    ),
  ),
  6686032205 => 
  array (
    'id' => 16954221,
    'name' => 'ООО "ЭДС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510816315,
    'updated_at' => 1510816413,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6686032205',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954221',
        'method' => 'get',
      ),
    ),
  ),
  278211357 => 
  array (
    'id' => 16954293,
    'name' => 'ООО "МУЗЫКАЛЬНЫЕ ТЕХНОЛОГИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510816608,
    'updated_at' => 1510816650,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204651,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204651',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278211357',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204651,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204651',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954293',
        'method' => 'get',
      ),
    ),
  ),
  1435237600 => 
  array (
    'id' => 16954353,
    'name' => 'ООО "Газтепломонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510816826,
    'updated_at' => 1510816827,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204701,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204701',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435237600',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204701,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204701',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954353',
        'method' => 'get',
      ),
    ),
  ),
  9718006277 => 
  array (
    'id' => 16954407,
    'name' => 'ООО  "КОМПЛЕКТСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510817057,
    'updated_at' => 1510817058,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204775,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204775',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718006277',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204775,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204775',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954407',
        'method' => 'get',
      ),
    ),
  ),
  183600 => 
  array (
    'id' => 16954427,
    'name' => 'Рязанова Юлия Александровна',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510817132,
    'updated_at' => 1510817133,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '183600',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954427',
        'method' => 'get',
      ),
    ),
  ),
  7705314560 => 
  array (
    'id' => 16954649,
    'name' => 'ООО "ШАРКОН-КОНСАЛТИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510817954,
    'updated_at' => 1510817954,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32205067,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32205067',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705314560',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32205067,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32205067',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954649',
        'method' => 'get',
      ),
    ),
  ),
  1661051905 => 
  array (
    'id' => 16954709,
    'name' => 'ООО "ПАРАМЕТР ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510818221,
    'updated_at' => 1510818221,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32205133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32205133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1661051905',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32205133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32205133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954709',
        'method' => 'get',
      ),
    ),
  ),
  3702569361 => 
  array (
    'id' => 16954755,
    'name' => 'ООО  "Классик Текстиль"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510818387,
    'updated_at' => 1510818387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32205173,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32205173',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702569361',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32205173,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32205173',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16954755',
        'method' => 'get',
      ),
    ),
  ),
  7707371725 => 
  array (
    'id' => 16955563,
    'name' => 'ООО  "КРОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510821076,
    'updated_at' => 1510821076,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32206015,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32206015',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707371725',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32206015,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32206015',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16955563',
        'method' => 'get',
      ),
    ),
  ),
  2311234502 => 
  array (
    'id' => 16955585,
    'name' => 'ООО "ЭНКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510821160,
    'updated_at' => 1510821161,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32206033,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32206033',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311234502',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32206033,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32206033',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16955585',
        'method' => 'get',
      ),
    ),
  ),
  5410057585 => 
  array (
    'id' => 16958587,
    'name' => 'ООО "Сибпроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510826400,
    'updated_at' => 1510826400,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32209339,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209339',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410057585',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32209339,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209339',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16958587',
        'method' => 'get',
      ),
    ),
  ),
  3812019716 => 
  array (
    'id' => 16958645,
    'name' => 'ООО "МЕРКУРИЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510826679,
    'updated_at' => 1510827110,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32209399,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209399',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812019716',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32209399,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209399',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16958645',
        'method' => 'get',
      ),
    ),
  ),
  2311232390 => 
  array (
    'id' => 16961145,
    'name' => 'ООО "АВИМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510836421,
    'updated_at' => 1510836445,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32212365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311232390',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32212365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16961145',
        'method' => 'get',
      ),
    ),
  ),
  7838041209 => 
  array (
    'id' => 16958755,
    'name' => 'ООО  "СнабТехбалТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510827266,
    'updated_at' => 1510827267,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32209579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838041209',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32209579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16958755',
        'method' => 'get',
      ),
    ),
  ),
  2319057911 => 
  array (
    'id' => 16960005,
    'name' => 'ООО  «ЧАСТНАЯ ОХРАННАЯ ОРГАНИЗАЦИЯ «ОМЕГА»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510831971,
    'updated_at' => 1510831971,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32210983,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32210983',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2319057911',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32210983,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32210983',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16960005',
        'method' => 'get',
      ),
    ),
  ),
  7804576799 => 
  array (
    'id' => 16960441,
    'name' => 'ООО "САГАДАК КОММУНИКАЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510833545,
    'updated_at' => 1510833593,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32211517,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211517',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804576799',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32211517,
        1 => 32211533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211517,32211533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16960441',
        'method' => 'get',
      ),
    ),
  ),
  3907062269 => 
  array (
    'id' => 16952381,
    'name' => 'ООО "АСПЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510809928,
    'updated_at' => 1510834998,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3907062269',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952381',
        'method' => 'get',
      ),
    ),
  ),
  9718001247 => 
  array (
    'id' => 16953477,
    'name' => 'ООО  "ЦЕНТР РАЗВИТИЯ КОММУНИКАЦИОННЫХ ТЕХНОЛОГИЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510811214,
    'updated_at' => 1510835013,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32203659,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203659',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718001247',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32203659,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32203659',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953477',
        'method' => 'get',
      ),
    ),
  ),
  2543067629 => 
  array (
    'id' => 16953815,
    'name' => 'ООО "АРХИТЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510813849,
    'updated_at' => 1510835026,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32204109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2543067629',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32204109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32204109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16953815',
        'method' => 'get',
      ),
    ),
  ),
  7721845832 => 
  array (
    'id' => 16952431,
    'name' => 'ООО "Сфера Безопасности"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510810337,
    'updated_at' => 1510835042,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201829,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201829',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721845832',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201829,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201829',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16952431',
        'method' => 'get',
      ),
    ),
  ),
  3123124795 => 
  array (
    'id' => 16960821,
    'name' => 'ООО "СОЮЗ ШИННИКОВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510835100,
    'updated_at' => 1510835101,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32211973,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211973',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123124795',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32211973,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211973',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16960821',
        'method' => 'get',
      ),
    ),
  ),
  7743085744 => 
  array (
    'id' => 16960859,
    'name' => 'Государственное бюджетное профессионального образовательное учреждение города Москвы "Технический пожарно-спасательный колледж имени Героя',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510835183,
    'updated_at' => 1510835437,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32211997,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211997',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743085744',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32211997,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211997',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16960859',
        'method' => 'get',
      ),
    ),
  ),
  9102210660 => 
  array (
    'id' => 16961049,
    'name' => 'ООО "КРЫМСТРОЙСЕРВИС 2016"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510836021,
    'updated_at' => 1510836047,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32212231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102210660',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32212231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16961049',
        'method' => 'get',
      ),
    ),
  ),
  673102412306 => 
  array (
    'id' => 16961211,
    'name' => 'осударственное бюджетное образовательное учреждение среднего профессионального образования города Москвы "Медицинский колледж № 5 Департамента',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510836763,
    'updated_at' => 1510836764,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32212473,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212473',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '673102412306',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32212473,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212473',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16961211',
        'method' => 'get',
      ),
    ),
  ),
  7810058763 => 
  array (
    'id' => 16961335,
    'name' => 'ООО "БалтКамень"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510837333,
    'updated_at' => 1510837333,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32212603,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212603',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810058763',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32212603,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212603',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16961335',
        'method' => 'get',
      ),
    ),
  ),
  5029102318 => 
  array (
    'id' => 16969111,
    'name' => 'ООО  "МАСТЕР ЛАНЧ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510897960,
    'updated_at' => 1510897961,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32217271,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32217271',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5029102318',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32217271,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32217271',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969111',
        'method' => 'get',
      ),
    ),
  ),
  5036162572 => 
  array (
    'id' => 16961549,
    'name' => 'ООО  "ПРОФИТ-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510838387,
    'updated_at' => 1510838387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32212879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5036162572',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32212879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16961549',
        'method' => 'get',
      ),
    ),
  ),
  7838395656 => 
  array (
    'id' => 16961593,
    'name' => 'ЗАО "Бюро комплексного проектирования"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510838629,
    'updated_at' => 1510838647,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32212933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838395656',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32212933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16961593',
        'method' => 'get',
      ),
    ),
  ),
  274101890 => 
  array (
    'id' => 15687297,
    'name' => 'Фирма Су-10, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1497589847,
    'updated_at' => 1510896733,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30352027,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352027',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1510896733,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274101890',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30352027,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352027',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15687297',
        'method' => 'get',
      ),
    ),
  ),
  141000786218 => 
  array (
    'id' => 16969123,
    'name' => 'Администрация муниципального образования "Город Вилюйск"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510898123,
    'updated_at' => 1510898199,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32217289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32217289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '141000786218',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32217289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32217289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969123',
        'method' => 'get',
      ),
    ),
  ),
  7203302867 => 
  array (
    'id' => 16846035,
    'name' => 'ООО "СибирьДорСтрой"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1509688113,
    'updated_at' => 1510898641,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32066373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32066373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32070403,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32070403',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1510214206,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203302867',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32066373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32066373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16846035',
        'method' => 'get',
      ),
    ),
  ),
  7203252856 => 
  array (
    'id' => 16969185,
    'name' => 'ООО Диверсификационная Финансовая компания "Система"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510898742,
    'updated_at' => 1510898742,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32217361,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32217361',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203252856',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32217361,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32217361',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969185',
        'method' => 'get',
      ),
    ),
  ),
  5904118750 => 
  array (
    'id' => 16336189,
    'name' => 'Астер Плюс, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1504866263,
    'updated_at' => 1510901931,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31439257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31439257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29734263,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29734263',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904118750',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31439257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31439257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16336189',
        'method' => 'get',
      ),
    ),
  ),
  7811125331 => 
  array (
    'id' => 16969735,
    'name' => 'ООО  "Химико-фармацевтические технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510902574,
    'updated_at' => 1510902575,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218009,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218009',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811125331',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218009,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218009',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969735',
        'method' => 'get',
      ),
    ),
  ),
  5232000803 => 
  array (
    'id' => 16969753,
    'name' => 'ООО "Спасский промкомбинат"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510902673,
    'updated_at' => 1510902673,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5232000803',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969753',
        'method' => 'get',
      ),
    ),
  ),
  7604322557 => 
  array (
    'id' => 16969773,
    'name' => 'ООО "ЕВРОМАРК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510902841,
    'updated_at' => 1510902882,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7604322557',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969773',
        'method' => 'get',
      ),
    ),
  ),
  7736544740 => 
  array (
    'id' => 16969797,
    'name' => 'ООО  Медиа Группа "Апокриф"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510902964,
    'updated_at' => 1510902964,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736544740',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969797',
        'method' => 'get',
      ),
    ),
  ),
  5053068795 => 
  array (
    'id' => 16969813,
    'name' => 'ООО "ЗОЛОТОЕ РУНО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510903036,
    'updated_at' => 1510903037,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5053068795',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969813',
        'method' => 'get',
      ),
    ),
  ),
  7721789320 => 
  array (
    'id' => 16969907,
    'name' => 'ООО  "ДОМИНО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510903600,
    'updated_at' => 1510903600,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218235,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218235',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721789320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218235,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218235',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969907',
        'method' => 'get',
      ),
    ),
  ),
  7728339810 => 
  array (
    'id' => 16969951,
    'name' => 'ООО "РЕНОВАЦИО АЙТИ ЛАБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510903790,
    'updated_at' => 1510903817,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218285,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218285',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728339810',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218285,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218285',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969951',
        'method' => 'get',
      ),
    ),
  ),
  6163142309 => 
  array (
    'id' => 16970295,
    'name' => 'ООО "Градиент Капитал"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510905055,
    'updated_at' => 1510905138,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218601,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218601',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6163142309',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218601,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218601',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16970295',
        'method' => 'get',
      ),
    ),
  ),
  6672343306 => 
  array (
    'id' => 16599719,
    'name' => 'Уралзоолайн, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1507269516,
    'updated_at' => 1510905233,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31777131,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31777131',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1510905233,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672343306',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31777131,
        1 => 31777507,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31777131,31777507',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16599719',
        'method' => 'get',
      ),
    ),
  ),
  3663126998 => 
  array (
    'id' => 16970391,
    'name' => 'ООО "Дом солнца"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510905586,
    'updated_at' => 1510905586,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218747,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218747',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3663126998',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218747,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218747',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16970391',
        'method' => 'get',
      ),
    ),
  ),
  2311237609 => 
  array (
    'id' => 16970435,
    'name' => 'ООО "ТРИУМФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510905751,
    'updated_at' => 1510905751,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311237609',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16970435',
        'method' => 'get',
      ),
    ),
  ),
  7701239135 => 
  array (
    'id' => 16970969,
    'name' => 'ООО "ИНТЕРНЕТ-КОСМОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510907604,
    'updated_at' => 1510907632,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32219421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701239135',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32219421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16970969',
        'method' => 'get',
      ),
    ),
  ),
  9102231878 => 
  array (
    'id' => 16971083,
    'name' => 'ООО "Специальное проектное бюро -Крым"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510908050,
    'updated_at' => 1510908050,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32219563,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219563',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102231878',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32219563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16971083',
        'method' => 'get',
      ),
    ),
  ),
  110306423884 => 
  array (
    'id' => 16971115,
    'name' => 'Администрация муниципального образования городского округа "Воркута"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510908166,
    'updated_at' => 1510908166,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32219601,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219601',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '110306423884',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32219601,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219601',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16971115',
        'method' => 'get',
      ),
    ),
  ),
  4028052976 => 
  array (
    'id' => 16971193,
    'name' => 'ООО  "МАРК-ТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510908520,
    'updated_at' => 1510908521,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32219729,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219729',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4028052976',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32219729,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32219729',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16971193',
        'method' => 'get',
      ),
    ),
  ),
  3663127529 => 
  array (
    'id' => 16971465,
    'name' => 'ООО  "ТЕХНОРЕЛАЙН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510909818,
    'updated_at' => 1510911745,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32220083,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220083',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3663127529',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32220083,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220083',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16971465',
        'method' => 'get',
      ),
    ),
  ),
  6623090010 => 
  array (
    'id' => 16971947,
    'name' => 'ООО "ОИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510911941,
    'updated_at' => 1510911941,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32220761,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220761',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6623090010',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32220761,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220761',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16971947',
        'method' => 'get',
      ),
    ),
  ),
  9701079975 => 
  array (
    'id' => 16971975,
    'name' => 'ООО "Компания "БИНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510912052,
    'updated_at' => 1510912053,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32220781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701079975',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32220781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16971975',
        'method' => 'get',
      ),
    ),
  ),
  278213139 => 
  array (
    'id' => 16972005,
    'name' => 'ООО "ЛАБМЕД 1"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510912245,
    'updated_at' => 1510912246,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32220825,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220825',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278213139',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32220825,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220825',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16972005',
        'method' => 'get',
      ),
    ),
  ),
  4516009981 => 
  array (
    'id' => 16972061,
    'name' => 'ООО "Аквакультура Зауралья"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510912494,
    'updated_at' => 1510912494,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32220907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4516009981',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32220907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16972061',
        'method' => 'get',
      ),
    ),
  ),
  9729056946 => 
  array (
    'id' => 16972085,
    'name' => 'ООО "ОМД ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510912620,
    'updated_at' => 1510912620,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32220941,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220941',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9729056946',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32220941,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220941',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16972085',
        'method' => 'get',
      ),
    ),
  ),
  7724417151 => 
  array (
    'id' => 16972655,
    'name' => 'ООО "Транспортная компания Легтранс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510915084,
    'updated_at' => 1510915084,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32221609,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32221609',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724417151',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32221609,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32221609',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16972655',
        'method' => 'get',
      ),
    ),
  ),
  7732500413 => 
  array (
    'id' => 16973269,
    'name' => 'ООО "ДЕЛОВОЙ МИР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510917686,
    'updated_at' => 1510917686,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32222201,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222201',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7732500413',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32222201,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222201',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16973269',
        'method' => 'get',
      ),
    ),
  ),
  6950168323 => 
  array (
    'id' => 16972247,
    'name' => '"Строй-Импульс"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1510913334,
    'updated_at' => 1510917729,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32221125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32221125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950168323',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32221125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32221125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16972247',
        'method' => 'get',
      ),
    ),
  ),
  6321366872 => 
  array (
    'id' => 16973311,
    'name' => 'ООО "СТРОЙ-ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510917896,
    'updated_at' => 1510917896,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32222257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6321366872',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32222257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16973311',
        'method' => 'get',
      ),
    ),
  ),
  7719460095 => 
  array (
    'id' => 16973387,
    'name' => 'ООО  "Квартализ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510918274,
    'updated_at' => 1510918274,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32222339,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222339',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719460095',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32222339,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222339',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16973387',
        'method' => 'get',
      ),
    ),
  ),
  7447162854 => 
  array (
    'id' => 16959521,
    'name' => 'Абсолют, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1510830090,
    'updated_at' => 1510920427,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32210505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32210505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32210747,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32210747',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7447162854',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32210505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32210505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16959521',
        'method' => 'get',
      ),
    ),
  ),
  7816633280 => 
  array (
    'id' => 16973831,
    'name' => 'ООО "ГОРОДСКОЙ ДИЗАЙН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510920918,
    'updated_at' => 1510920918,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32222913,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222913',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816633280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32222913,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32222913',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16973831',
        'method' => 'get',
      ),
    ),
  ),
  1833040024 => 
  array (
    'id' => 16958969,
    'name' => 'ООО Промышленная Компания "Водная Техника"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510828144,
    'updated_at' => 1510921033,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32209811,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209811',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1833040024',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32209811,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32209811',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16958969',
        'method' => 'get',
      ),
    ),
  ),
  276130367 => 
  array (
    'id' => 15581089,
    'name' => 'А-Строй, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1496376702,
    'updated_at' => 1511151229,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30191233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30191233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1511151229,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '276130367',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30191233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30191233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15581089',
        'method' => 'get',
      ),
    ),
  ),
  2463255497 => 
  array (
    'id' => 15137227,
    'name' => 'ССК "Сибмост", ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491886194,
    'updated_at' => 1511151951,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29579141,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29579141',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463255497',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29579141,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29579141',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15137227',
        'method' => 'get',
      ),
    ),
  ),
  7720137040 => 
  array (
    'id' => 16986369,
    'name' => 'ООО "Аконит-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511156144,
    'updated_at' => 1511156198,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235201,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235201',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720137040',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235201,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235201',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986369',
        'method' => 'get',
      ),
    ),
  ),
  7727322317 => 
  array (
    'id' => 16986405,
    'name' => 'ООО  "РусСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511156670,
    'updated_at' => 1511156670,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235253,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235253',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727322317',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235253,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235253',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986405',
        'method' => 'get',
      ),
    ),
  ),
  5019023515 => 
  array (
    'id' => 16986415,
    'name' => 'ООО  "Строительно - Транспортная - Компания Альянс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511156811,
    'updated_at' => 1511156812,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5019023515',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235263,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235263',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986415',
        'method' => 'get',
      ),
    ),
  ),
  7720674920 => 
  array (
    'id' => 16986531,
    'name' => 'ООО  "Комплексаная безопасность бизнеса"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511157750,
    'updated_at' => 1511157751,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235427,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235427',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720674920',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235427,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235427',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986531',
        'method' => 'get',
      ),
    ),
  ),
  7721808319 => 
  array (
    'id' => 16986551,
    'name' => 'ООО  "ПРОМ-КОТЕЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511158042,
    'updated_at' => 1511158342,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721808319',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986551',
        'method' => 'get',
      ),
    ),
  ),
  4501216992 => 
  array (
    'id' => 16986657,
    'name' => 'ООО  “ТК СТАНДАРТ”',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511158887,
    'updated_at' => 1511158888,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235619,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235619',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4501216992',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235619,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235619',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986657',
        'method' => 'get',
      ),
    ),
  ),
  '7-499-4061610' => 
  array (
    'id' => 16986739,
    'name' => 'ООО «РЕКЛАМНЫЙ МИР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511159198,
    'updated_at' => 1511159225,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7-499-4061610',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986739',
        'method' => 'get',
      ),
    ),
  ),
  5040150097 => 
  array (
    'id' => 16986755,
    'name' => 'ООО  "НОВЫЕ МЕДИЦИНСКИЕ ТЕХНОЛОГИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511159306,
    'updated_at' => 1511159306,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235733,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235733',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040150097',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235733,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235733',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986755',
        'method' => 'get',
      ),
    ),
  ),
  6350025080 => 
  array (
    'id' => 16986793,
    'name' => 'ООО "МОЛОЧНАЯ СКАЗКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511159534,
    'updated_at' => 1511159535,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235785,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235785',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6350025080',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235785,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235785',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986793',
        'method' => 'get',
      ),
    ),
  ),
  7722392494 => 
  array (
    'id' => 16986807,
    'name' => 'ООО «ВЕЛЕС»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511159681,
    'updated_at' => 1511159682,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235809,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235809',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722392494',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235809,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235809',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986807',
        'method' => 'get',
      ),
    ),
  ),
  7718783992 => 
  array (
    'id' => 16986823,
    'name' => 'ООО "ТелекомЭнергоСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511159787,
    'updated_at' => 1511159788,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235835,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235835',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718783992',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235835,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235835',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986823',
        'method' => 'get',
      ),
    ),
  ),
  7804545021 => 
  array (
    'id' => 16986883,
    'name' => 'ООО "РЕГИОНСТРОЙКОНТРОЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511160200,
    'updated_at' => 1511160200,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235921,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235921',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804545021',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235921,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235921',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986883',
        'method' => 'get',
      ),
    ),
  ),
  7714326836 => 
  array (
    'id' => 16986907,
    'name' => 'ООО «Компания Озон»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511160310,
    'updated_at' => 1511160311,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714326836',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986907',
        'method' => 'get',
      ),
    ),
  ),
  7716668049 => 
  array (
    'id' => 16986925,
    'name' => 'ООО "БОЛЬШАЯ ТРОЙКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511160417,
    'updated_at' => 1511160417,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716668049',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986925',
        'method' => 'get',
      ),
    ),
  ),
  7327072045 => 
  array (
    'id' => 14969971,
    'name' => 'КТЦ Инжиниринг, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1490249779,
    'updated_at' => 1511160446,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372929,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372929',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 30531819,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=30531819',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7327072045',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372929,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372929',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969971',
        'method' => 'get',
      ),
    ),
  ),
  7728354713 => 
  array (
    'id' => 16986951,
    'name' => 'ООО  "ПОЖБЕЗОПАСНОСТЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511160572,
    'updated_at' => 1511160572,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235989,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235989',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728354713',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235989,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235989',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986951',
        'method' => 'get',
      ),
    ),
  ),
  9717062590 => 
  array (
    'id' => 16986987,
    'name' => 'ООО "МАДЖЕСТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511160755,
    'updated_at' => 1511160755,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236023,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236023',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9717062590',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236023,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236023',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986987',
        'method' => 'get',
      ),
    ),
  ),
  7718281752 => 
  array (
    'id' => 16987003,
    'name' => 'ООО «КОРУС»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511160889,
    'updated_at' => 1511160890,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236041,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236041',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718281752',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236041,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236041',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987003',
        'method' => 'get',
      ),
    ),
  ),
  8904068977 => 
  array (
    'id' => 16987247,
    'name' => 'ООО  "ЯМАЛСПЕЦСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511162012,
    'updated_at' => 1511162012,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236311,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236311',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8904068977',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236311,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236311',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987247',
        'method' => 'get',
      ),
    ),
  ),
  7717587360 => 
  array (
    'id' => 16992997,
    'name' => 'ООО "БРОД-ЭСТЕЙТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511180849,
    'updated_at' => 1511180849,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32243199,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243199',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717587360',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32243199,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243199',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16992997',
        'method' => 'get',
      ),
    ),
  ),
  7814100536 => 
  array (
    'id' => 16987309,
    'name' => 'ООО  "ИСТА-СИСТЕМС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511162387,
    'updated_at' => 1511162679,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236397,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236397',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814100536',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236397,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236397',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987309',
        'method' => 'get',
      ),
    ),
  ),
  7205000011 => 
  array (
    'id' => 16987369,
    'name' => 'АО "Ишимское ПАТП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511162808,
    'updated_at' => 1511162904,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236507,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236507',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7205000011',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236507,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236507',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987369',
        'method' => 'get',
      ),
    ),
  ),
  7715544509 => 
  array (
    'id' => 16987421,
    'name' => 'ООО "Научно-производственная организация "Синергетика"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511162990,
    'updated_at' => 1511163059,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236553,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236553',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715544509',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236553,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236553',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987421',
        'method' => 'get',
      ),
    ),
  ),
  2356041973 => 
  array (
    'id' => 15006213,
    'name' => 'Дорметалл, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1490681435,
    'updated_at' => 1511163362,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29423697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29423697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29914873,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29914873',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2356041973',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край( +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29423697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29423697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15006213',
        'method' => 'get',
      ),
    ),
  ),
  7722528730 => 
  array (
    'id' => 16987551,
    'name' => 'ООО "ЦЕНТР ТРАНСПОРТНЫХ ИССЛЕДОВАНИЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511163658,
    'updated_at' => 1511163712,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236731,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236731',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722528730',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236731,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236731',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987551',
        'method' => 'get',
      ),
    ),
  ),
  2635209545 => 
  array (
    'id' => 16987577,
    'name' => 'ООО "БизнесСтройГрупп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511163814,
    'updated_at' => 1511163815,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635209545',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987577',
        'method' => 'get',
      ),
    ),
  ),
  5024169742 => 
  array (
    'id' => 16987645,
    'name' => 'АО «МРТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511163879,
    'updated_at' => 1511163916,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236789,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236789',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024169742',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236789,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236789',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987645',
        'method' => 'get',
      ),
    ),
  ),
  2320101964 => 
  array (
    'id' => 16987669,
    'name' => 'ООО «Строительно-монтажное управление №3»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164037,
    'updated_at' => 1511164038,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236835,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236835',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2320101964',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236835,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236835',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987669',
        'method' => 'get',
      ),
    ),
  ),
  7807377590 => 
  array (
    'id' => 16987693,
    'name' => 'ООО  "ПРОЕКТСТРОЙИНДУСТРИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164162,
    'updated_at' => 1511164162,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236877,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236877',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807377590',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236877,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236877',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987693',
        'method' => 'get',
      ),
    ),
  ),
  6168030006 => 
  array (
    'id' => 16987735,
    'name' => 'ООО  "КОМПАНИЯ САМИТЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164361,
    'updated_at' => 1511164361,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6168030006',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987735',
        'method' => 'get',
      ),
    ),
  ),
  3329020077 => 
  array (
    'id' => 16987771,
    'name' => 'ООО "КЛЯЗЬМА-ТУР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164528,
    'updated_at' => 1511164528,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32236981,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236981',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3329020077',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32236981,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32236981',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987771',
        'method' => 'get',
      ),
    ),
  ),
  7733286811 => 
  array (
    'id' => 16987791,
    'name' => 'ООО "ЮНА ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164606,
    'updated_at' => 1511164607,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237005,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237005',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733286811',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237005,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237005',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987791',
        'method' => 'get',
      ),
    ),
  ),
  7722732736 => 
  array (
    'id' => 16987815,
    'name' => 'ООО "СФЕРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164716,
    'updated_at' => 1511164717,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722732736',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987815',
        'method' => 'get',
      ),
    ),
  ),
  7804579912 => 
  array (
    'id' => 16987845,
    'name' => 'ОО  "КОНТЛИЗ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164846,
    'updated_at' => 1511164846,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237075,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237075',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804579912',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237075,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237075',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987845',
        'method' => 'get',
      ),
    ),
  ),
  7728213590 => 
  array (
    'id' => 16987865,
    'name' => 'ООО "РЕЗЕРВ ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511164941,
    'updated_at' => 1511164941,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728213590',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987865',
        'method' => 'get',
      ),
    ),
  ),
  7714405661 => 
  array (
    'id' => 16987889,
    'name' => 'ООО "ТеплоКоннект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511165087,
    'updated_at' => 1511165088,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714405661',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987889',
        'method' => 'get',
      ),
    ),
  ),
  7718973993 => 
  array (
    'id' => 16987941,
    'name' => 'ООО «Технострой»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511165320,
    'updated_at' => 1511165321,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237207,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237207',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718973993',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237207,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237207',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987941',
        'method' => 'get',
      ),
    ),
  ),
  7707329755 => 
  array (
    'id' => 16987985,
    'name' => 'ООО "БСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511165516,
    'updated_at' => 1511165517,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707329755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237263,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237263',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16987985',
        'method' => 'get',
      ),
    ),
  ),
  7725266868 => 
  array (
    'id' => 16988011,
    'name' => 'ООО "ТеплоГазСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511165631,
    'updated_at' => 1511165632,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237299,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237299',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725266868',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237299,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237299',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988011',
        'method' => 'get',
      ),
    ),
  ),
  7730164438 => 
  array (
    'id' => 16988041,
    'name' => 'ООО «Юниделл-Строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511165703,
    'updated_at' => 1511165703,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237319,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237319',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730164438',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237319,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237319',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988041',
        'method' => 'get',
      ),
    ),
  ),
  5009084893 => 
  array (
    'id' => 16988171,
    'name' => 'ООО "Стройразвитие М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511166032,
    'updated_at' => 1511166032,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237461,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237461',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5009084893',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237461,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237461',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988171',
        'method' => 'get',
      ),
    ),
  ),
  7724369028 => 
  array (
    'id' => 16988197,
    'name' => 'ООО «ТК УНИВЕРСАЛ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511166113,
    'updated_at' => 1511166113,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237487,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237487',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724369028',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237487,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237487',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988197',
        'method' => 'get',
      ),
    ),
  ),
  7722770273 => 
  array (
    'id' => 16988283,
    'name' => 'ООО "ФлотСтройСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511166287,
    'updated_at' => 1511166318,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722770273',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988283',
        'method' => 'get',
      ),
    ),
  ),
  5027165048 => 
  array (
    'id' => 16988343,
    'name' => 'ООО  "МонтажСпецСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511166492,
    'updated_at' => 1511166493,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237653,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237653',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027165048',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237653,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237653',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988343',
        'method' => 'get',
      ),
    ),
  ),
  9705038793 => 
  array (
    'id' => 16988625,
    'name' => 'ООО "ЭЛИОТ Медицина"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511167092,
    'updated_at' => 1511167093,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237827,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237827',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705038793',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237827,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237827',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988625',
        'method' => 'get',
      ),
    ),
  ),
  7802765617 => 
  array (
    'id' => 16988679,
    'name' => 'ООО "ПО АВТОГЕН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511167330,
    'updated_at' => 1511167354,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802765617',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988679',
        'method' => 'get',
      ),
    ),
  ),
  7602088809 => 
  array (
    'id' => 16990707,
    'name' => 'ООО  «Комбинат социального питания»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511171561,
    'updated_at' => 1511171934,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32240315,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32240315',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7602088809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32240315,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32240315',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16990707',
        'method' => 'get',
      ),
    ),
  ),
  1840034840 => 
  array (
    'id' => 16959903,
    'name' => 'ООО  "Хауберк"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510831596,
    'updated_at' => 1511172031,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32210897,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32210897',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1840034840',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32210897,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32210897',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16959903',
        'method' => 'get',
      ),
    ),
  ),
  7729465769 => 
  array (
    'id' => 16990257,
    'name' => 'ООО  "НАЦИОНАЛЬНАЯ КОНТРАКТНАЯ КОРПОРАЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511170085,
    'updated_at' => 1511172058,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32239857,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32239857',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729465769',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32239857,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32239857',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16990257',
        'method' => 'get',
      ),
    ),
  ),
  7716842240 => 
  array (
    'id' => 16991247,
    'name' => 'ООО «Линия вкуса»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511173446,
    'updated_at' => 1511173447,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32240937,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32240937',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716842240',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32240937,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32240937',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16991247',
        'method' => 'get',
      ),
    ),
  ),
  4633015280 => 
  array (
    'id' => 16972121,
    'name' => 'ООО "ГЕОВОДСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510912799,
    'updated_at' => 1511174043,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32220977,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220977',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4633015280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32220977,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32220977',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16972121',
        'method' => 'get',
      ),
    ),
  ),
  7801645081 => 
  array (
    'id' => 16986461,
    'name' => 'ООО  "ВОЛЬТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511157158,
    'updated_at' => 1511175421,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801645081',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986461',
        'method' => 'get',
      ),
    ),
  ),
  7801632283 => 
  array (
    'id' => 16969983,
    'name' => 'ООО "ИНТЕРФОРМ-ДИЗАЙН СПБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510903988,
    'updated_at' => 1511175431,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801632283',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16969983',
        'method' => 'get',
      ),
    ),
  ),
  5256160115 => 
  array (
    'id' => 16970455,
    'name' => 'ООО "ПМК-НН""',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510905821,
    'updated_at' => 1511175443,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32218829,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218829',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5256160115',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32218829,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32218829',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16970455',
        'method' => 'get',
      ),
    ),
  ),
  7716794050 => 
  array (
    'id' => 16960065,
    'name' => 'ООО "НЕЛЬМА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510832289,
    'updated_at' => 1511175459,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32211039,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211039',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716794050',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32211039,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32211039',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16960065',
        'method' => 'get',
      ),
    ),
  ),
  1650234978 => 
  array (
    'id' => 16579827,
    'name' => 'Лин Вектор, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1507094333,
    'updated_at' => 1511175785,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31753579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31753579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 31753577,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=31753577',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1507291220,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1650234978',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31753579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31753579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16579827',
        'method' => 'get',
      ),
    ),
  ),
  4345334805 => 
  array (
    'id' => 16992377,
    'name' => 'ООО  "ВЯТКАЭКОПИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511178110,
    'updated_at' => 1511178110,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32242377,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32242377',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345334805',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32242377,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32242377',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16992377',
        'method' => 'get',
      ),
    ),
  ),
  2304063542 => 
  array (
    'id' => 15738515,
    'name' => 'Автострада-Юг, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1498111306,
    'updated_at' => 1511179430,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1511179430,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2304063542',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738515',
        'method' => 'get',
      ),
    ),
  ),
  9701061093 => 
  array (
    'id' => 16992839,
    'name' => 'ООО  «МК-Поставка»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511180052,
    'updated_at' => 1511180052,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32243015,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243015',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701061093',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32243015,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243015',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16992839',
        'method' => 'get',
      ),
    ),
  ),
  2635818152 => 
  array (
    'id' => 16993403,
    'name' => 'ООО "Фаворит Климат"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511182595,
    'updated_at' => 1511182596,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32243629,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243629',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635818152',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32243629,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243629',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16993403',
        'method' => 'get',
      ),
    ),
  ),
  2311165305 => 
  array (
    'id' => 16993499,
    'name' => 'ООО  "СП-ХОЛДИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511182982,
    'updated_at' => 1511182983,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32243713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311165305',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32243713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16993499',
        'method' => 'get',
      ),
    ),
  ),
  7729646839 => 
  array (
    'id' => 16993675,
    'name' => 'ООО "ВИРА-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511183748,
    'updated_at' => 1511183943,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32243903,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243903',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729646839',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32243903,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243903',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16993675',
        'method' => 'get',
      ),
    ),
  ),
  7727303360 => 
  array (
    'id' => 16993757,
    'name' => 'ООО  "ПБ Магистраль"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511184198,
    'updated_at' => 1511184199,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32244007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727303360',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32244007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16993757',
        'method' => 'get',
      ),
    ),
  ),
  7726371093 => 
  array (
    'id' => 16993903,
    'name' => 'ООО "НН-МЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511185042,
    'updated_at' => 1511185043,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32244189,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244189',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726371093',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32244189,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244189',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16993903',
        'method' => 'get',
      ),
    ),
  ),
  6162051708 => 
  array (
    'id' => 16993937,
    'name' => 'ООО  "ЗОЛОТОЕ СЕЧЕНИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511185170,
    'updated_at' => 1511185237,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32244221,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244221',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6162051708',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32244221,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244221',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16993937',
        'method' => 'get',
      ),
    ),
  ),
  7730199173 => 
  array (
    'id' => 16994053,
    'name' => 'ООО  "Прайм"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511185693,
    'updated_at' => 1511185694,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32244321,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244321',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730199173',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32244321,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32244321',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16994053',
        'method' => 'get',
      ),
    ),
  ),
  264064568 => 
  array (
    'id' => 16997313,
    'name' => 'ООО "ГеоСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243799,
    'updated_at' => 1511243799,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247745,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247745',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '264064568',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247745,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247745',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997313',
        'method' => 'get',
      ),
    ),
  ),
  6321109466 => 
  array (
    'id' => 15050007,
    'name' => 'Линия В, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1490938792,
    'updated_at' => 1511239393,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29477861,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29477861',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6321109466',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29477861,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29477861',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15050007',
        'method' => 'get',
      ),
    ),
  ),
  9102225828 => 
  array (
    'id' => 16997097,
    'name' => 'ООО ЛК РНКБ',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511240741,
    'updated_at' => 1511240742,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247455,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247455',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102225828',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247455,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247455',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997097',
        'method' => 'get',
      ),
    ),
  ),
  2225177018 => 
  array (
    'id' => 16997143,
    'name' => 'ООО "Сиб-Мед-Плюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511241314,
    'updated_at' => 1511241382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225177018',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997143',
        'method' => 'get',
      ),
    ),
  ),
  7813252568 => 
  array (
    'id' => 16997177,
    'name' => 'ООО "ИНСТИТУТ ПРОЕКТИРОВАНИЯ И СТРОИТЕЛЬСТВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511241860,
    'updated_at' => 1511241861,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813252568',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997177',
        'method' => 'get',
      ),
    ),
  ),
  6321319030 => 
  array (
    'id' => 16985625,
    'name' => '"ПОВОЛЖЬЕ-СМ"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1511155260,
    'updated_at' => 1511242247,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32234051,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32234051',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1511242247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6321319030',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32234051,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32234051',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16985625',
        'method' => 'get',
      ),
    ),
  ),
  7720820948 => 
  array (
    'id' => 16997215,
    'name' => 'ООО  "ЛЕ-Интеграция"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511242319,
    'updated_at' => 1511242320,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247597,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247597',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720820948',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247597,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247597',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997215',
        'method' => 'get',
      ),
    ),
  ),
  2721204340 => 
  array (
    'id' => 16997225,
    'name' => 'ООО  СТРОИТЕЛЬНАЯ КОМПАНИЯ "ДФО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511242458,
    'updated_at' => 1511242567,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721204340',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997225',
        'method' => 'get',
      ),
    ),
  ),
  7714795387 => 
  array (
    'id' => 16997237,
    'name' => 'ООО "КВАНТ СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511242710,
    'updated_at' => 1511242711,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714795387',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997237',
        'method' => 'get',
      ),
    ),
  ),
  2901212269 => 
  array (
    'id' => 16997255,
    'name' => 'ООО "Фишка"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511242953,
    'updated_at' => 1511242953,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247645,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247645',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901212269',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247645,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247645',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997255',
        'method' => 'get',
      ),
    ),
  ),
  1660149489 => 
  array (
    'id' => 16997271,
    'name' => 'ООО «РЕДЕВЕЛОПМЕНТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243235,
    'updated_at' => 1511243235,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660149489',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997271',
        'method' => 'get',
      ),
    ),
  ),
  7814673505 => 
  array (
    'id' => 16997285,
    'name' => 'ООО "Инженерный центр "Электронмаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243442,
    'updated_at' => 1511243443,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247709,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247709',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814673505',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247709,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247709',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997285',
        'method' => 'get',
      ),
    ),
  ),
  2318030868 => 
  array (
    'id' => 16997297,
    'name' => 'ООО  «ПРАГМА»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243571,
    'updated_at' => 1511243572,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247721,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247721',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2318030868',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247721,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247721',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997297',
        'method' => 'get',
      ),
    ),
  ),
  2463102317 => 
  array (
    'id' => 16997301,
    'name' => 'ООО  "ФУД СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243632,
    'updated_at' => 1511243632,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247723,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247723',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463102317',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247723,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247723',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997301',
        'method' => 'get',
      ),
    ),
  ),
  2901237506 => 
  array (
    'id' => 16997319,
    'name' => 'ООО "Вкусно29"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243885,
    'updated_at' => 1511243886,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247757,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247757',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901237506',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247757,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247757',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997319',
        'method' => 'get',
      ),
    ),
  ),
  7736263386 => 
  array (
    'id' => 16997335,
    'name' => 'ООО "Концепт Технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243973,
    'updated_at' => 1511243974,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736263386',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997335',
        'method' => 'get',
      ),
    ),
  ),
  7724673620 => 
  array (
    'id' => 16997339,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ КОМПАНИЯ "ЮГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511244090,
    'updated_at' => 1511244090,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247785,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247785',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724673620',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247785,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247785',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997339',
        'method' => 'get',
      ),
    ),
  ),
  2723167326 => 
  array (
    'id' => 16997359,
    'name' => 'ООО "СВЯЗЬ-ПРОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511244218,
    'updated_at' => 1511244240,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723167326',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997359',
        'method' => 'get',
      ),
    ),
  ),
  5032274966 => 
  array (
    'id' => 16997421,
    'name' => 'ООО "ОНИКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511244588,
    'updated_at' => 1511244589,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247985,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247985',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032274966',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247985,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247985',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997421',
        'method' => 'get',
      ),
    ),
  ),
  5027236919 => 
  array (
    'id' => 16997593,
    'name' => 'ООО "ЭКОСТРОЙПРОГРЕСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511245528,
    'updated_at' => 1511245577,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027236919',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997593',
        'method' => 'get',
      ),
    ),
  ),
  7801283018 => 
  array (
    'id' => 16997111,
    'name' => 'ООО "Параметр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511240912,
    'updated_at' => 1511245810,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247475,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247475',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801283018',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247475,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247475',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997111',
        'method' => 'get',
      ),
    ),
  ),
  6950133169 => 
  array (
    'id' => 16997659,
    'name' => 'ООО "ОКТАН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511246016,
    'updated_at' => 1511246016,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950133169',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997659',
        'method' => 'get',
      ),
    ),
  ),
  2634088866 => 
  array (
    'id' => 16997683,
    'name' => 'ООО "СПЕКТР-С"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511246118,
    'updated_at' => 1511246119,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2634088866',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997683',
        'method' => 'get',
      ),
    ),
  ),
  7722395738 => 
  array (
    'id' => 16997715,
    'name' => 'ООО "Вектор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511246221,
    'updated_at' => 1511246222,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248297,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248297',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722395738',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248297,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248297',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997715',
        'method' => 'get',
      ),
    ),
  ),
  1659180149 => 
  array (
    'id' => 16988103,
    'name' => 'ООО «РЕЗОН-ПРОДУКТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511165872,
    'updated_at' => 1511246368,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32237395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659180149',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32237395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32237395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16988103',
        'method' => 'get',
      ),
    ),
  ),
  7734214672 => 
  array (
    'id' => 16997721,
    'name' => 'ООО "МАРТИС КОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511246319,
    'updated_at' => 1511246407,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248305,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248305',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734214672',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248305,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248305',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997721',
        'method' => 'get',
      ),
    ),
  ),
  7451195700 => 
  array (
    'id' => 16997755,
    'name' => 'ЗАО «Южноуральский лизинговый центр»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511246541,
    'updated_at' => 1511246611,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248339,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248339',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451195700',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248339,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248339',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997755',
        'method' => 'get',
      ),
    ),
  ),
  7017387567 => 
  array (
    'id' => 16997783,
    'name' => 'ООО "АВТОСТРОЙ-Т"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511246701,
    'updated_at' => 1511246702,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017387567',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997783',
        'method' => 'get',
      ),
    ),
  ),
  3255502838 => 
  array (
    'id' => 16997847,
    'name' => 'ООО "БРЯНСКИЙ АВТОМОБИЛЬНЫЙ ЗАВОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511247013,
    'updated_at' => 1511247013,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248433,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248433',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3255502838',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248433,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248433',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997847',
        'method' => 'get',
      ),
    ),
  ),
  7708316519 => 
  array (
    'id' => 16997861,
    'name' => 'ООО "МедКомплект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511247102,
    'updated_at' => 1511247103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248453,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248453',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708316519',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248453,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248453',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997861',
        'method' => 'get',
      ),
    ),
  ),
  7704406169 => 
  array (
    'id' => 16997923,
    'name' => 'ООО  "ФЕМТОМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511247414,
    'updated_at' => 1511247440,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248515,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248515',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704406169',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248515,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248515',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997923',
        'method' => 'get',
      ),
    ),
  ),
  7813235756 => 
  array (
    'id' => 16997955,
    'name' => 'ООО "ПЕТРЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511247660,
    'updated_at' => 1511247660,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813235756',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997955',
        'method' => 'get',
      ),
    ),
  ),
  5040138220 => 
  array (
    'id' => 16998103,
    'name' => 'ООО  "ХЛЕБПРОДУКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511248535,
    'updated_at' => 1511248536,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040138220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998103',
        'method' => 'get',
      ),
    ),
  ),
  7704391177 => 
  array (
    'id' => 16998193,
    'name' => 'ООО ТОРГОВО-ТРАНСПОРТНАЯ КОМПАНИЯ "БЛЭКОЙЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511248821,
    'updated_at' => 1511248822,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248803,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248803',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704391177',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248803,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248803',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998193',
        'method' => 'get',
      ),
    ),
  ),
  7743188387 => 
  array (
    'id' => 16997483,
    'name' => 'ООО  ГК "ЛЕДОВЫЙ СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511244731,
    'updated_at' => 1511249253,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248043,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248043',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743188387',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248043,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248043',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997483',
        'method' => 'get',
      ),
    ),
  ),
  1660270277 => 
  array (
    'id' => 16997165,
    'name' => 'ООО «Системы Документооборота-Техцентр»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511241711,
    'updated_at' => 1511249264,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247561,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247561',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660270277',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247561,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247561',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997165',
        'method' => 'get',
      ),
    ),
  ),
  5032157010 => 
  array (
    'id' => 16997207,
    'name' => 'ООО "Лидер"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511242179,
    'updated_at' => 1511249322,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247591,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247591',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032157010',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247591,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247591',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997207',
        'method' => 'get',
      ),
    ),
  ),
  7734182533 => 
  array (
    'id' => 16991275,
    'name' => 'ООО "МЭЗ Спецавтоматика"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511173566,
    'updated_at' => 1511249335,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32240975,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32240975',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734182533',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32240975,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32240975',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16991275',
        'method' => 'get',
      ),
    ),
  ),
  7707623965 => 
  array (
    'id' => 16992411,
    'name' => 'ООО  «Н.В.С. Сервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511178228,
    'updated_at' => 1511249346,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32242417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32242417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707623965',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32242417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32242417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16992411',
        'method' => 'get',
      ),
    ),
  ),
  6168088430 => 
  array (
    'id' => 16998387,
    'name' => 'ООО "ЭКОВОЛЬТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511249772,
    'updated_at' => 1511249772,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32249049,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249049',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6168088430',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32249049,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249049',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998387',
        'method' => 'get',
      ),
    ),
  ),
  7224035764 => 
  array (
    'id' => 16986773,
    'name' => 'МСК "СибАгро"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1511159417,
    'updated_at' => 1511250083,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235761,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235761',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7224035764',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235761,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235761',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986773',
        'method' => 'get',
      ),
    ),
  ),
  1646032873 => 
  array (
    'id' => 16997627,
    'name' => 'ООО "Сарихат"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511245799,
    'updated_at' => 1511250278,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248191,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248191',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1646032873',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248191,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248191',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997627',
        'method' => 'get',
      ),
    ),
  ),
  7734363628 => 
  array (
    'id' => 16998569,
    'name' => 'ООО  "ХАЙДЖИН КИНЕТИКС СЕЙЛЗ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511250491,
    'updated_at' => 1511250492,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32249251,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249251',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734363628',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32249251,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249251',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998569',
        'method' => 'get',
      ),
    ),
  ),
  4826129301 => 
  array (
    'id' => 16997253,
    'name' => 'ООО  СНАБЖЕНЧЕСКОЕ ПРЕДПРИЯТИЕ "ГАСКОМПЛЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511242850,
    'updated_at' => 1511251182,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247641,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247641',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826129301',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247641,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247641',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997253',
        'method' => 'get',
      ),
    ),
  ),
  5262290401 => 
  array (
    'id' => 16998757,
    'name' => 'ООО "АВТОЛИГА-КОНЦЕПТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511251525,
    'updated_at' => 1511251783,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32249521,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249521',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5262290401',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32249521,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249521',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998757',
        'method' => 'get',
      ),
    ),
  ),
  9102190212 => 
  array (
    'id' => 16998907,
    'name' => 'ООО "КРЫМСАХСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511252256,
    'updated_at' => 1511252256,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32249705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102190212',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32249705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998907',
        'method' => 'get',
      ),
    ),
  ),
  9729007811 => 
  array (
    'id' => 16997881,
    'name' => 'ООО "ВИНАЛЬП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511247244,
    'updated_at' => 1511254578,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248471,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248471',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9729007811',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248471,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248471',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997881',
        'method' => 'get',
      ),
    ),
  ),
  2631029358 => 
  array (
    'id' => 16998047,
    'name' => 'МУНИЦИПАЛЬНОЕ УНИТАРНОЕ ПРЕДПРИЯТИЕ "АРХИТЕКТУРНО-ПЛАНИРОВОЧНОЕ БЮРО" ГОРОДА НЕВИННОМЫССКА',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511248220,
    'updated_at' => 1511254600,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248657,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248657',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2631029358',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248657,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248657',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998047',
        'method' => 'get',
      ),
    ),
  ),
  9102206141 => 
  array (
    'id' => 16998367,
    'name' => 'ООО  "СТРАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511249663,
    'updated_at' => 1511254646,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32249021,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249021',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102206141',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32249021,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32249021',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16998367',
        'method' => 'get',
      ),
    ),
  ),
  7802547369 => 
  array (
    'id' => 17000505,
    'name' => 'ООО "ПРОЕКТСПЕЦСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511258533,
    'updated_at' => 1511258621,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32251585,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32251585',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802547369',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32251585,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32251585',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17000505',
        'method' => 'get',
      ),
    ),
  ),
  5902148886 => 
  array (
    'id' => 17000709,
    'name' => 'ООО  "МЕДИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511259431,
    'updated_at' => 1511259452,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32251861,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32251861',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902148886',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край +5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32251861,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32251861',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17000709',
        'method' => 'get',
      ),
    ),
  ),
  7706426700 => 
  array (
    'id' => 17000767,
    'name' => 'ООО "СЕРВИС КЛИНИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511259747,
    'updated_at' => 1511259780,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32251951,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32251951',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706426700',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32251951,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32251951',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17000767',
        'method' => 'get',
      ),
    ),
  ),
  3665104220 => 
  array (
    'id' => 17000867,
    'name' => 'ООО  "ЭТАЛОНСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511260166,
    'updated_at' => 1511260167,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32252075,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32252075',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3665104220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32252075,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32252075',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17000867',
        'method' => 'get',
      ),
    ),
  ),
  7725717888 => 
  array (
    'id' => 17001361,
    'name' => 'ООО "Бинорт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511261709,
    'updated_at' => 1511261709,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32252603,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32252603',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725717888',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32252603,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32252603',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17001361',
        'method' => 'get',
      ),
    ),
  ),
  7804587110 => 
  array (
    'id' => 17001693,
    'name' => 'ООО «Атрибут»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511262836,
    'updated_at' => 1511262837,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32253019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804587110',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32253019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17001693',
        'method' => 'get',
      ),
    ),
  ),
  7714986720 => 
  array (
    'id' => 17001711,
    'name' => 'ООО "Что делать Решение"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511262961,
    'updated_at' => 1511262969,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32253041,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253041',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714986720',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32253041,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253041',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17001711',
        'method' => 'get',
      ),
    ),
  ),
  5024084009 => 
  array (
    'id' => 17001765,
    'name' => 'ООО "МОТОМАГАЗИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511263134,
    'updated_at' => 1511263148,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32253105,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253105',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024084009',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Владимирская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32253105,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253105',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17001765',
        'method' => 'get',
      ),
    ),
  ),
  7704308179 => 
  array (
    'id' => 17002017,
    'name' => 'ООО  «АВТОБУСНЫЕ ЗАПЧАСТИ И КОМПОНЕНТЫ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511264156,
    'updated_at' => 1511264220,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32253423,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253423',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704308179',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32253423,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253423',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17002017',
        'method' => 'get',
      ),
    ),
  ),
  3812059885 => 
  array (
    'id' => 17002135,
    'name' => 'ООО "ИСТПАРТНЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511264616,
    'updated_at' => 1511264617,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32253599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812059885',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32253599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32253599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17002135',
        'method' => 'get',
      ),
    ),
  ),
  7723888479 => 
  array (
    'id' => 17002827,
    'name' => 'ООО  «Альтеко Групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511267897,
    'updated_at' => 1511267929,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32254427,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32254427',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723888479',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32254427,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32254427',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17002827',
        'method' => 'get',
      ),
    ),
  ),
  1513055871 => 
  array (
    'id' => 17002933,
    'name' => 'ООО "ФОРТЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511268391,
    'updated_at' => 1511268391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32254563,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32254563',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1513055871',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32254563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32254563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17002933',
        'method' => 'get',
      ),
    ),
  ),
  7811476347 => 
  array (
    'id' => 17003079,
    'name' => 'ООО "ДВС Ресурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511269088,
    'updated_at' => 1511269096,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32254771,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32254771',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811476347',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ивановская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32254771,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32254771',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17003079',
        'method' => 'get',
      ),
    ),
  ),
  4101170180 => 
  array (
    'id' => 17008141,
    'name' => 'ООО "РОЯЛ-АВТО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511322938,
    'updated_at' => 1511322939,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101170180',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008141',
        'method' => 'get',
      ),
    ),
  ),
  4101170623 => 
  array (
    'id' => 17008149,
    'name' => 'ООО  "АЛЬТЕЗА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511323027,
    'updated_at' => 1511323027,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101170623',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008149',
        'method' => 'get',
      ),
    ),
  ),
  2234010520 => 
  array (
    'id' => 17008157,
    'name' => 'Государственное унитарное предприятие дорожного хозяйства Алтайского края «Юго-Восточное дорожно-строительное управление»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511323191,
    'updated_at' => 1511323191,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2234010520',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008157',
        'method' => 'get',
      ),
    ),
  ),
  5407202514 => 
  array (
    'id' => 16323191,
    'name' => 'НПФ "Ренси", ЗАО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1504759786,
    'updated_at' => 1511323485,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31423499,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31423499',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29584881,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29584881',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407202514',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31423499,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31423499',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16323191',
        'method' => 'get',
      ),
    ),
  ),
  269031848 => 
  array (
    'id' => 17008185,
    'name' => 'АО «ДЭП №103»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511323800,
    'updated_at' => 1511323826,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '269031848',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008185',
        'method' => 'get',
      ),
    ),
  ),
  5403182546 => 
  array (
    'id' => 17008215,
    'name' => 'ООО "МАСТЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511324303,
    'updated_at' => 1511324304,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260441,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260441',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5403182546',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260441,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260441',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008215',
        'method' => 'get',
      ),
    ),
  ),
  5905272352 => 
  array (
    'id' => 17008235,
    'name' => 'ООО "ЗемКом"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511324806,
    'updated_at' => 1511324860,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260479,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260479',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905272352',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260479,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260479',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008235',
        'method' => 'get',
      ),
    ),
  ),
  6670427286 => 
  array (
    'id' => 17008267,
    'name' => 'ООО "ЭНЕРГОСФЕРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511325285,
    'updated_at' => 1511325307,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260509,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260509',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670427286',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260509,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260509',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008267',
        'method' => 'get',
      ),
    ),
  ),
  2221171671 => 
  array (
    'id' => 15787685,
    'name' => 'Компас, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1498709326,
    'updated_at' => 1511325337,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30497749,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30497749',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29878005,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29878005',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221171671',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30497749,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30497749',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15787685',
        'method' => 'get',
      ),
    ),
  ),
  7524018713 => 
  array (
    'id' => 17008367,
    'name' => 'ООО "Читадорстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511326834,
    'updated_at' => 1511326892,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260643,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260643',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7524018713',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260643,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260643',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008367',
        'method' => 'get',
      ),
    ),
  ),
  8905063058 => 
  array (
    'id' => 17008387,
    'name' => 'АО "МПГЭС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511327053,
    'updated_at' => 1511327053,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260655,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260655',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8905063058',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260655,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260655',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008387',
        'method' => 'get',
      ),
    ),
  ),
  6633026198 => 
  array (
    'id' => 17008975,
    'name' => 'ООО "СЕРВИС ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511328470,
    'updated_at' => 1511328470,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6633026198',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008975',
        'method' => 'get',
      ),
    ),
  ),
  7704335870 => 
  array (
    'id' => 17008981,
    'name' => 'ООО "МИКРОСИСТЕМС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511328566,
    'updated_at' => 1511328567,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261711,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261711',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704335870',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261711,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261711',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008981',
        'method' => 'get',
      ),
    ),
  ),
  860406146339 => 
  array (
    'id' => 17008997,
    'name' => 'Шергин Сергей Викторович',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511328926,
    'updated_at' => 1511328926,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261743,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261743',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '860406146339',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261743,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261743',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008997',
        'method' => 'get',
      ),
    ),
  ),
  7725524766 => 
  array (
    'id' => 17009019,
    'name' => 'ООО Частное охранное предприятие "АЛЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511329316,
    'updated_at' => 1511329317,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261779,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261779',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725524766',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261779,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261779',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009019',
        'method' => 'get',
      ),
    ),
  ),
  7723398114 => 
  array (
    'id' => 17009071,
    'name' => 'ООО "ОМЕГА СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511329898,
    'updated_at' => 1511329978,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261831,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261831',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723398114',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261831,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261831',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009071',
        'method' => 'get',
      ),
    ),
  ),
  3665066021 => 
  array (
    'id' => 17009199,
    'name' => 'ООО ИНВЕСТИЦИОННАЯ КОМПАНИЯ "РЕГИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331061,
    'updated_at' => 1511331151,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262013,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262013',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3665066021',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262013,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262013',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009199',
        'method' => 'get',
      ),
    ),
  ),
  5043058950 => 
  array (
    'id' => 17009235,
    'name' => 'ООО "РОСЭКОСВЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331336,
    'updated_at' => 1511331337,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262057,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262057',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5043058950',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009235',
        'method' => 'get',
      ),
    ),
  ),
  2460092678 => 
  array (
    'id' => 17009243,
    'name' => 'ООО СК "Единство"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331416,
    'updated_at' => 1511331417,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262069,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262069',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460092678',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262069,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262069',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009243',
        'method' => 'get',
      ),
    ),
  ),
  2311175141 => 
  array (
    'id' => 15211071,
    'name' => 'ООО Группа Компаний «Юг-Энерго»',
    'responsible_user_id' => 1277505,
    'created_by' => 643314,
    'created_at' => 1492580628,
    'updated_at' => 1511331434,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29690207,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29690207',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311175141',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29690207,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29690207',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15211071',
        'method' => 'get',
      ),
    ),
  ),
  7736293920 => 
  array (
    'id' => 17009249,
    'name' => 'ООО «Мегаполис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331518,
    'updated_at' => 1511331518,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736293920',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009249',
        'method' => 'get',
      ),
    ),
  ),
  5009045365 => 
  array (
    'id' => 17009263,
    'name' => 'ООО "ДОМОЛАН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331651,
    'updated_at' => 1511331652,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5009045365',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009263',
        'method' => 'get',
      ),
    ),
  ),
  7733162598 => 
  array (
    'id' => 17009277,
    'name' => 'ООО НПИЦ "ЭКОРИДЖИНАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331748,
    'updated_at' => 1511331749,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733162598',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009277',
        'method' => 'get',
      ),
    ),
  ),
  7719792608 => 
  array (
    'id' => 17009287,
    'name' => 'ООО «Домстройсервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331825,
    'updated_at' => 1511331875,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719792608',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009287',
        'method' => 'get',
      ),
    ),
  ),
  7802628755 => 
  array (
    'id' => 17009297,
    'name' => 'ООО  "СканРэй"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511331936,
    'updated_at' => 1511331937,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262143,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262143',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802628755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262143,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262143',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009297',
        'method' => 'get',
      ),
    ),
  ),
  6229056420 => 
  array (
    'id' => 17009567,
    'name' => 'ООО «АльянсСтрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511333691,
    'updated_at' => 1511333765,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262473,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262473',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6229056420',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262473,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262473',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009567',
        'method' => 'get',
      ),
    ),
  ),
  105027747 => 
  array (
    'id' => 15173965,
    'name' => '"МАРК-Сервис"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1492150730,
    'updated_at' => 1511335164,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29620845,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29620845',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1511335164,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '105027747',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29620845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29620845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15173965',
        'method' => 'get',
      ),
    ),
  ),
  6450088133 => 
  array (
    'id' => 17010079,
    'name' => 'ООО «МПА САРАТОВ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511335781,
    'updated_at' => 1511335781,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6450088133',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17010079',
        'method' => 'get',
      ),
    ),
  ),
  3329091247 => 
  array (
    'id' => 17010487,
    'name' => 'ООО "ЮАМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511337786,
    'updated_at' => 1511337786,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32263443,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263443',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3329091247',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32263443,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263443',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17010487',
        'method' => 'get',
      ),
    ),
  ),
  7130031309 => 
  array (
    'id' => 17010509,
    'name' => 'ООО "Ю-ИНВЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511337904,
    'updated_at' => 1511337905,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32263473,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263473',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7130031309',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32263473,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263473',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17010509',
        'method' => 'get',
      ),
    ),
  ),
  7705574215 => 
  array (
    'id' => 17009047,
    'name' => 'ООО "Вим"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511329648,
    'updated_at' => 1511338062,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705574215',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009047',
        'method' => 'get',
      ),
    ),
  ),
  5028107352 => 
  array (
    'id' => 17012043,
    'name' => 'ООО "Окна Квадриум"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511338517,
    'updated_at' => 1511338578,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32263649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32263647,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32263647',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5028107352',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32263649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17012043',
        'method' => 'get',
      ),
    ),
  ),
  7733250205 => 
  array (
    'id' => 17000911,
    'name' => 'ООО  «Гранд Мед»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511260284,
    'updated_at' => 1511338655,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32252109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32252109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733250205',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32252109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32252109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17000911',
        'method' => 'get',
      ),
    ),
  ),
  6670442171 => 
  array (
    'id' => 17008987,
    'name' => 'ООО «ТРАСТ-ИНВЕСТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511328751,
    'updated_at' => 1511339449,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261725,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261725',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670442171',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261725,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261725',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008987',
        'method' => 'get',
      ),
    ),
  ),
  7751037504 => 
  array (
    'id' => 17010443,
    'name' => 'ООО  «ЛАБОРАТОРИЯ ИНЖЕНЕРНЫХ СИСТЕМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511337568,
    'updated_at' => 1511339707,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32263397,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263397',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751037504',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32263397,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263397',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17010443',
        'method' => 'get',
      ),
    ),
  ),
  7839369514 => 
  array (
    'id' => 17022737,
    'name' => 'ООО "Мастертурбо"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511339959,
    'updated_at' => 1511339960,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32287515,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287515',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839369514',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32287515,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287515',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17022737',
        'method' => 'get',
      ),
    ),
  ),
  5402033686 => 
  array (
    'id' => 17022807,
    'name' => 'ООО "ЭКВАТОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511340198,
    'updated_at' => 1511340199,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32287589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402033686',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32287589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17022807',
        'method' => 'get',
      ),
    ),
  ),
  5047091193 => 
  array (
    'id' => 17022881,
    'name' => 'ООО  "ДОМ СТРОЙ СПОРТ СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511340553,
    'updated_at' => 1511340627,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32287697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047091193',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32287697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17022881',
        'method' => 'get',
      ),
    ),
  ),
  2636212830 => 
  array (
    'id' => 17022939,
    'name' => 'ООО  "СТРОЙЛАЙТ СК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511340860,
    'updated_at' => 1511340861,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32287793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2636212830',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32287793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17022939',
        'method' => 'get',
      ),
    ),
  ),
  7722780602 => 
  array (
    'id' => 17023049,
    'name' => 'ООО "СоноСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511341426,
    'updated_at' => 1511341470,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32287957,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287957',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722780602',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32287957,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287957',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17023049',
        'method' => 'get',
      ),
    ),
  ),
  7017266883 => 
  array (
    'id' => 17023075,
    'name' => 'ООО «Химсофт»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511341555,
    'updated_at' => 1511341556,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32287997,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287997',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017266883',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32287997,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287997',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17023075',
        'method' => 'get',
      ),
    ),
  ),
  6670360779 => 
  array (
    'id' => 17023103,
    'name' => 'ООО "ОМЕГА-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511341707,
    'updated_at' => 1511341726,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32288047,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32288047',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670360779',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32288047,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32288047',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17023103',
        'method' => 'get',
      ),
    ),
  ),
  7733591903 => 
  array (
    'id' => 17010155,
    'name' => 'ООО «ТЭОКОМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511336206,
    'updated_at' => 1511345773,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32263019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733591903',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32263019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32263019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17010155',
        'method' => 'get',
      ),
    ),
  ),
  7804032975 => 
  array (
    'id' => 17024057,
    'name' => 'ООО "ЧАСТНОЕ АГЕНТСТВО ПО ОХРАНЕ "АРГУС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511346211,
    'updated_at' => 1511346211,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32289251,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32289251',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804032975',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32289251,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32289251',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17024057',
        'method' => 'get',
      ),
    ),
  ),
  7704406183 => 
  array (
    'id' => 17022831,
    'name' => 'ООО "СИЛА СВЕТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511340331,
    'updated_at' => 1511346517,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32287627,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287627',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704406183',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32287627,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32287627',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17022831',
        'method' => 'get',
      ),
    ),
  ),
  7714362506 => 
  array (
    'id' => 17009135,
    'name' => 'ООО  «КИР,ЛТД»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511330398,
    'updated_at' => 1511348476,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714362506',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009135',
        'method' => 'get',
      ),
    ),
  ),
  7721371935 => 
  array (
    'id' => 16997267,
    'name' => 'ООО "М-КЛАУД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511243116,
    'updated_at' => 1511348533,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247667,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247667',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721371935',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247667,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247667',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997267',
        'method' => 'get',
      ),
    ),
  ),
  1615013359 => 
  array (
    'id' => 17009015,
    'name' => 'ООО «К-ПРОЕКТЫ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511329176,
    'updated_at' => 1511348606,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32261769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1615013359',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32261769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32261769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009015',
        'method' => 'get',
      ),
    ),
  ),
  3819022500 => 
  array (
    'id' => 17008253,
    'name' => 'ООО "УСОЛЬЕ СОЛЬ ТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511325157,
    'updated_at' => 1511348632,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260493,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260493',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3819022500',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260493,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260493',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008253',
        'method' => 'get',
      ),
    ),
  ),
  2801195958 => 
  array (
    'id' => 17008249,
    'name' => 'ООО "БНК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511325003,
    'updated_at' => 1511348646,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2801195958',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Амурская область(GMT +9)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008249',
        'method' => 'get',
      ),
    ),
  ),
  264065699 => 
  array (
    'id' => 17008221,
    'name' => 'ООО "Завод модульных конструкций "Башеврокуб"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511324482,
    'updated_at' => 1511348660,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '264065699',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008221',
        'method' => 'get',
      ),
    ),
  ),
  266049452 => 
  array (
    'id' => 17008201,
    'name' => 'ООО "МонолитСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511324103,
    'updated_at' => 1511348679,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260427,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260427',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '266049452',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260427,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260427',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008201',
        'method' => 'get',
      ),
    ),
  ),
  7802182380 => 
  array (
    'id' => 17025191,
    'name' => 'ООО «Профиль»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511351400,
    'updated_at' => 1511351835,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32296207,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32296207',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802182380',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32296207,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32296207',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17025191',
        'method' => 'get',
      ),
    ),
  ),
  2721206869 => 
  array (
    'id' => 17030327,
    'name' => 'ООО "РОСИНМОНОЛИТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511412465,
    'updated_at' => 1511412482,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302531,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302531',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721206869',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хабаровский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302531,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302531',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030327',
        'method' => 'get',
      ),
    ),
  ),
  7203422730 => 
  array (
    'id' => 17030365,
    'name' => 'ООО  "АйрСофт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511413313,
    'updated_at' => 1511413313,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302565,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302565',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203422730',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302565,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302565',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030365',
        'method' => 'get',
      ),
    ),
  ),
  9705097527 => 
  array (
    'id' => 17030333,
    'name' => 'ООО Управляющая компания Скай',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511412616,
    'updated_at' => 1511413968,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705097527',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030333',
        'method' => 'get',
      ),
    ),
  ),
  7708812644 => 
  array (
    'id' => 17030565,
    'name' => 'ООО "СЕРВИС 365"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511415719,
    'updated_at' => 1511415720,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302767,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302767',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708812644',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302767,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302767',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030565',
        'method' => 'get',
      ),
    ),
  ),
  5433956571 => 
  array (
    'id' => 17030579,
    'name' => 'ООО "ТЕЛЕКОМ-ИНЖИНИРИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511415821,
    'updated_at' => 1511415842,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5433956571',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030579',
        'method' => 'get',
      ),
    ),
  ),
  3460062379 => 
  array (
    'id' => 17030599,
    'name' => 'ООО "ДМС-АВТО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511416209,
    'updated_at' => 1511416210,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3460062379',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030599',
        'method' => 'get',
      ),
    ),
  ),
  6915015243 => 
  array (
    'id' => 17030639,
    'name' => 'ООО"Торжокские Технологии и Машины"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511416739,
    'updated_at' => 1511416739,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302859,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302859',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6915015243',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302859,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302859',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030639',
        'method' => 'get',
      ),
    ),
  ),
  7702413724 => 
  array (
    'id' => 17030379,
    'name' => 'ООО  «КОМЕРЦ ПРО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511413414,
    'updated_at' => 1511417063,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302575,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302575',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702413724',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302575,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302575',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030379',
        'method' => 'get',
      ),
    ),
  ),
  5036152662 => 
  array (
    'id' => 17030349,
    'name' => 'ООО "СЕТЕВОЙ СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511412923,
    'updated_at' => 1511417131,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302553,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302553',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5036152662',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302553,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302553',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030349',
        'method' => 'get',
      ),
    ),
  ),
  5904348739 => 
  array (
    'id' => 17030291,
    'name' => 'ООО Дорожное ремонтно-строительное управление «Прикамье»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511411548,
    'updated_at' => 1511417188,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302487,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302487',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904348739',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302487,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302487',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030291',
        'method' => 'get',
      ),
    ),
  ),
  7723880367 => 
  array (
    'id' => 17024911,
    'name' => 'ООО «Айфом»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511350167,
    'updated_at' => 1511418225,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32290435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32290435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723880367',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32290435,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32290435',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17024911',
        'method' => 'get',
      ),
    ),
  ),
  1657107841 => 
  array (
    'id' => 17010129,
    'name' => 'ООО «ТехКомплект»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511336072,
    'updated_at' => 1511418270,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262987,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262987',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657107841',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262987,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262987',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17010129',
        'method' => 'get',
      ),
    ),
  ),
  8903004378 => 
  array (
    'id' => 17008271,
    'name' => 'ООО "СИМВОЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511325378,
    'updated_at' => 1511418328,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8903004378',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008271',
        'method' => 'get',
      ),
    ),
  ),
  7811473610 => 
  array (
    'id' => 17030865,
    'name' => 'ООО  "Горзеленхоз"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511418490,
    'updated_at' => 1511418492,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303179,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303179',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811473610',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303179,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303179',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030865',
        'method' => 'get',
      ),
    ),
  ),
  5031060182 => 
  array (
    'id' => 17030941,
    'name' => 'АО "Всесоюзный научный центр по безопасности биологически активных веществ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511418950,
    'updated_at' => 1511418951,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303251,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303251',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5031060182',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303251,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303251',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030941',
        'method' => 'get',
      ),
    ),
  ),
  553701472563 => 
  array (
    'id' => 17031007,
    'name' => 'бюджетное учреждение Ханты-Мансийского автономного округа - Югры "Окружной кардиологический диспансер "Центр диагностики и сердечно-сосудистой',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511419425,
    'updated_at' => 1511419426,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '553701472563',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031007',
        'method' => 'get',
      ),
    ),
  ),
  7716857712 => 
  array (
    'id' => 17031015,
    'name' => 'ООО "АгроСпецСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511419483,
    'updated_at' => 1511419483,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303359,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303359',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716857712',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303359,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303359',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031015',
        'method' => 'get',
      ),
    ),
  ),
  5405504887 => 
  array (
    'id' => 17031053,
    'name' => 'ООО "ОЛИМП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511419738,
    'updated_at' => 1511419739,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303403,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303403',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405504887',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303403,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303403',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031053',
        'method' => 'get',
      ),
    ),
  ),
  571009294 => 
  array (
    'id' => 17031139,
    'name' => 'ООО «Дортехноконтроль»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511420236,
    'updated_at' => 1511420324,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303519,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303519',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '571009294',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303519,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303519',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031139',
        'method' => 'get',
      ),
    ),
  ),
  5030072072 => 
  array (
    'id' => 17031155,
    'name' => 'ООО "Эйрвэй Ко"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511420366,
    'updated_at' => 1511420367,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303551,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303551',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5030072072',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303551,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303551',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031155',
        'method' => 'get',
      ),
    ),
  ),
  2222845316 => 
  array (
    'id' => 16292067,
    'name' => 'СТИ, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1504502896,
    'updated_at' => 1511420494,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31387177,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31387177',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32302527,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32302527',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2222845316',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31387177,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31387177',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16292067',
        'method' => 'get',
      ),
    ),
  ),
  7813538341 => 
  array (
    'id' => 17031177,
    'name' => 'ООО "ПетроРеконструкция"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511420494,
    'updated_at' => 1511420568,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303575,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303575',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813538341',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303575,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303575',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031177',
        'method' => 'get',
      ),
    ),
  ),
  7807376773 => 
  array (
    'id' => 17031213,
    'name' => 'ООО  "САТЕЛЛИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511420653,
    'updated_at' => 1511420653,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303627,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303627',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807376773',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303627,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303627',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031213',
        'method' => 'get',
      ),
    ),
  ),
  7718265486 => 
  array (
    'id' => 17031225,
    'name' => 'ООО "ЭКСЕРГИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511420769,
    'updated_at' => 1511420788,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303657,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303657',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718265486',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303657,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303657',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031225',
        'method' => 'get',
      ),
    ),
  ),
  7715841300 => 
  array (
    'id' => 17031259,
    'name' => 'ООО "ФОРСАЙТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511420886,
    'updated_at' => 1511420887,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715841300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031259',
        'method' => 'get',
      ),
    ),
  ),
  5027243352 => 
  array (
    'id' => 17031305,
    'name' => 'ООО "МЕДИРОЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511421233,
    'updated_at' => 1511421234,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303779,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303779',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027243352',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303779,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303779',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031305',
        'method' => 'get',
      ),
    ),
  ),
  7203372871 => 
  array (
    'id' => 17032195,
    'name' => 'ООО "НИАЛКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511424737,
    'updated_at' => 1511424738,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203372871',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17032195',
        'method' => 'get',
      ),
    ),
  ),
  3662196738 => 
  array (
    'id' => 17031409,
    'name' => 'ООО  "КОМПЕТО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511421541,
    'updated_at' => 1511421541,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303951,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303951',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662196738',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303951,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303951',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031409',
        'method' => 'get',
      ),
    ),
  ),
  6674189667 => 
  array (
    'id' => 17031445,
    'name' => 'ООО ПКФ "ГИДРОМЕХАНИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511421680,
    'updated_at' => 1511421681,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32303991,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303991',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6674189667',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32303991,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32303991',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031445',
        'method' => 'get',
      ),
    ),
  ),
  7811650370 => 
  array (
    'id' => 17031497,
    'name' => 'ООО  «МИТСНАБ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511421889,
    'updated_at' => 1511421955,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304055,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304055',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811650370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304055,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304055',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031497',
        'method' => 'get',
      ),
    ),
  ),
  6829015218 => 
  array (
    'id' => 17031863,
    'name' => 'ООО. "ГИППОКРАТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511423136,
    'updated_at' => 1511423226,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829015218',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031863',
        'method' => 'get',
      ),
    ),
  ),
  7715997611 => 
  array (
    'id' => 17031885,
    'name' => 'ООО «АГРОРОСТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511423261,
    'updated_at' => 1511423261,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304413,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304413',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715997611',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031885',
        'method' => 'get',
      ),
    ),
  ),
  7706243840 => 
  array (
    'id' => 17031913,
    'name' => 'ООО "Парадайм Геофизикал"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511423481,
    'updated_at' => 1511423510,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304447,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304447',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706243840',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304447,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304447',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031913',
        'method' => 'get',
      ),
    ),
  ),
  3904092963 => 
  array (
    'id' => 17031957,
    'name' => 'ООО "РосЭко"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511423678,
    'updated_at' => 1511423695,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304499,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304499',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3904092963',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304499,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304499',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031957',
        'method' => 'get',
      ),
    ),
  ),
  7203414271 => 
  array (
    'id' => 17032037,
    'name' => 'ООО «Алеон»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511424071,
    'updated_at' => 1511424071,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203414271',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17032037',
        'method' => 'get',
      ),
    ),
  ),
  6318018547 => 
  array (
    'id' => 17032333,
    'name' => 'ООО «ИНВАТЕХ-ГРУПП»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511425100,
    'updated_at' => 1511425101,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318018547',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17032333',
        'method' => 'get',
      ),
    ),
  ),
  1657136031 => 
  array (
    'id' => 17032351,
    'name' => 'ООО "СПК-СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511425172,
    'updated_at' => 1511425172,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304899,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304899',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657136031',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304899,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304899',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17032351',
        'method' => 'get',
      ),
    ),
  ),
  4401180573 => 
  array (
    'id' => 17032619,
    'name' => 'ООО "ПРОФИНВЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511426448,
    'updated_at' => 1511426449,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32305297,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32305297',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4401180573',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32305297,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32305297',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17032619',
        'method' => 'get',
      ),
    ),
  ),
  2130159910 => 
  array (
    'id' => 17032647,
    'name' => 'ООО "ЗАЩИТА Трейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511426530,
    'updated_at' => 1511426530,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32305327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32305327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130159910',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32305327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32305327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17032647',
        'method' => 'get',
      ),
    ),
  ),
  3627016580 => 
  array (
    'id' => 17025055,
    'name' => 'ООО "Россошанское ДРСУ № 1"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511350767,
    'updated_at' => 1511427206,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32290597,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32290597',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3627016580',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32290597,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32290597',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17025055',
        'method' => 'get',
      ),
    ),
  ),
  5010041037 => 
  array (
    'id' => 17032791,
    'name' => '«ПРОМТЕХ-Дубна»',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1511427189,
    'updated_at' => 1511431421,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32305579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32305579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32305577,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32305577',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5010041037',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32305579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32305579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17032791',
        'method' => 'get',
      ),
    ),
  ),
  9710007303 => 
  array (
    'id' => 17033969,
    'name' => 'ООО "АВЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511432499,
    'updated_at' => 1511432500,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32307379,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32307379',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9710007303',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32307379,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32307379',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17033969',
        'method' => 'get',
      ),
    ),
  ),
  7725804869 => 
  array (
    'id' => 17034011,
    'name' => 'ООО ЧАСТНОЕ ОХРАННОЕ ПРЕДПРИЯТИЕ "АЛЬФА-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511432717,
    'updated_at' => 1511432759,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32307435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32307435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725804869',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32307435,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32307435',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17034011',
        'method' => 'get',
      ),
    ),
  ),
  5261079332 => 
  array (
    'id' => 17033561,
    'name' => 'Научно-производственное предприятие «Салют»',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1511430910,
    'updated_at' => 1511434499,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32306917,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32306917',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1511434499,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5261079332',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32306917,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32306917',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17033561',
        'method' => 'get',
      ),
    ),
  ),
  6165152944 => 
  array (
    'id' => 17035893,
    'name' => 'ООО "Браво"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511441293,
    'updated_at' => 1511441294,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32309681,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32309681',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165152944',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32309681,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32309681',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17035893',
        'method' => 'get',
      ),
    ),
  ),
  7727836778 => 
  array (
    'id' => 17040031,
    'name' => 'ООО "АЙ-ТЕКО ВНЕДРЕНЧЕСКИЙ ЦЕНТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511500236,
    'updated_at' => 1511504099,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313235,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313235',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727836778',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313235,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313235',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040031',
        'method' => 'get',
      ),
    ),
  ),
  7734658533 => 
  array (
    'id' => 17040155,
    'name' => 'ООО «Ядерные Технологии в Медицине»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511501701,
    'updated_at' => 1511501739,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313389,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313389',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32313387,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32313387',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734658533',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313389,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313389',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040155',
        'method' => 'get',
      ),
    ),
  ),
  7207018624 => 
  array (
    'id' => 17040267,
    'name' => 'ООО «СИБПРОДУКТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511502651,
    'updated_at' => 1511502675,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313531,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313531',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7207018624',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'тюменск. об 5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313531,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313531',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040267',
        'method' => 'get',
      ),
    ),
  ),
  7704075654 => 
  array (
    'id' => 17040359,
    'name' => 'ООО "Частное охранное предприятие Фирма "ЛЕГИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511503549,
    'updated_at' => 1511503577,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313627,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313627',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704075654',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313627,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313627',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040359',
        'method' => 'get',
      ),
    ),
  ),
  7719672639 => 
  array (
    'id' => 17040427,
    'name' => 'ООО  "ГАРАНТПРОМБИЗНЕС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511504258,
    'updated_at' => 1511504333,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313707,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313707',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719672639',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313707,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313707',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040427',
        'method' => 'get',
      ),
    ),
  ),
  7724898913 => 
  array (
    'id' => 17040471,
    'name' => 'ООО "СМАРТ ТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511504369,
    'updated_at' => 1511504385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313743,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313743',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724898913',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313743,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313743',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040471',
        'method' => 'get',
      ),
    ),
  ),
  7814690187 => 
  array (
    'id' => 17040481,
    'name' => 'ООО " Строймакс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511504473,
    'updated_at' => 1511504473,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814690187',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040481',
        'method' => 'get',
      ),
    ),
  ),
  2618022593 => 
  array (
    'id' => 17040499,
    'name' => 'ООО "ВЕРИМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511504584,
    'updated_at' => 1511504614,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2618022593',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6 алтай',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040499',
        'method' => 'get',
      ),
    ),
  ),
  7721257679 => 
  array (
    'id' => 17040545,
    'name' => 'ООО "МУЗЫКА И ТЕХНОЛОГИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511504916,
    'updated_at' => 1511504922,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313853,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313853',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721257679',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313853,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313853',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040545',
        'method' => 'get',
      ),
    ),
  ),
  7709797580 => 
  array (
    'id' => 17040555,
    'name' => 'ЗАО "ГК АККОРД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511505047,
    'updated_at' => 1511505061,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709797580',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040555',
        'method' => 'get',
      ),
    ),
  ),
  7816576867 => 
  array (
    'id' => 17040571,
    'name' => 'ООО «Виком»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511505154,
    'updated_at' => 1511505240,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313889,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313889',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816576867',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313889,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313889',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040571',
        'method' => 'get',
      ),
    ),
  ),
  5611074742 => 
  array (
    'id' => 17040603,
    'name' => 'ООО "БАРГУС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511505371,
    'updated_at' => 1511505372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313925,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313925',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5611074742',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313925,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313925',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040603',
        'method' => 'get',
      ),
    ),
  ),
  7734531569 => 
  array (
    'id' => 17040615,
    'name' => 'ООО "ТД Вояж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511505502,
    'updated_at' => 1511505502,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313955,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313955',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734531569',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313955,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313955',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040615',
        'method' => 'get',
      ),
    ),
  ),
  6914019566 => 
  array (
    'id' => 17040629,
    'name' => 'ООО "КРИСТАЛЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511505617,
    'updated_at' => 1511505645,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313971,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313971',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6914019566',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313971,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313971',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040629',
        'method' => 'get',
      ),
    ),
  ),
  2419003797 => 
  array (
    'id' => 17039965,
    'name' => 'ГП КК "Каратузское ДРСУ"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1511498784,
    'updated_at' => 1511505831,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313165,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313165',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32313163,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32313163',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1511505831,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2419003797',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313165,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313165',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17039965',
        'method' => 'get',
      ),
    ),
  ),
  7714960930 => 
  array (
    'id' => 17040643,
    'name' => 'ООО «Хит Машинери»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511505754,
    'updated_at' => 1511506178,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313987,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313987',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714960930',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313987,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313987',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040643',
        'method' => 'get',
      ),
    ),
  ),
  9721048171 => 
  array (
    'id' => 17040769,
    'name' => 'ООО  "МЕД ТЕХНИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511506730,
    'updated_at' => 1511506748,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314161,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314161',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9721048171',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314161,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314161',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040769',
        'method' => 'get',
      ),
    ),
  ),
  9102204360 => 
  array (
    'id' => 17040813,
    'name' => 'ООО "ЛИДЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511506919,
    'updated_at' => 1511506919,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314221,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314221',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102204360',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314221,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314221',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040813',
        'method' => 'get',
      ),
    ),
  ),
  7203390292 => 
  array (
    'id' => 17040843,
    'name' => 'ООО "КОМПАНИЯ ПРОИЗВОДСТВЕННО-ТЕХНИЧЕСКОГО ОБЕСПЕЧЕНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511507030,
    'updated_at' => 1511507030,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203390292',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040843',
        'method' => 'get',
      ),
    ),
  ),
  5260346930 => 
  array (
    'id' => 17040863,
    'name' => 'ООО "ЛабКрафт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511507223,
    'updated_at' => 1511507224,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314299,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314299',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5260346930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314299,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314299',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040863',
        'method' => 'get',
      ),
    ),
  ),
  5406277852 => 
  array (
    'id' => 17040891,
    'name' => 'ООО СИФ "СТРОЙПАРТНЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511507387,
    'updated_at' => 1511507411,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314343,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314343',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406277852',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314343,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314343',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040891',
        'method' => 'get',
      ),
    ),
  ),
  6167090531 => 
  array (
    'id' => 17040911,
    'name' => 'ООО "Альпстройпроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511507500,
    'updated_at' => 1511507500,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6167090531',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040911',
        'method' => 'get',
      ),
    ),
  ),
  7704427472 => 
  array (
    'id' => 17041139,
    'name' => 'ООО  "СТРОЙТОРГСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511508648,
    'updated_at' => 1511508658,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314623,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314623',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704427472',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314623,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314623',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041139',
        'method' => 'get',
      ),
    ),
  ),
  262016248 => 
  array (
    'id' => 17041157,
    'name' => 'ООО "Кумертау Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511508779,
    'updated_at' => 1511508780,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '262016248',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041157',
        'method' => 'get',
      ),
    ),
  ),
  4308004372 => 
  array (
    'id' => 17041201,
    'name' => 'ООО "Стройлес"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511509007,
    'updated_at' => 1511509008,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4308004372',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041201',
        'method' => 'get',
      ),
    ),
  ),
  '7-919-3677362' => 
  array (
    'id' => 17041229,
    'name' => 'ООО "СТРОЙКОМП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511509153,
    'updated_at' => 1511509153,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314749,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314749',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7-919-3677362',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314749,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314749',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041229',
        'method' => 'get',
      ),
    ),
  ),
  1513063897 => 
  array (
    'id' => 17041255,
    'name' => 'ООО "Трест-1"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511509283,
    'updated_at' => 1511509283,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32314785,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314785',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1513063897',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32314785,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32314785',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041255',
        'method' => 'get',
      ),
    ),
  ),
  5258085135 => 
  array (
    'id' => 17033905,
    'name' => 'ООО "ГРОЛ-НН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511432303,
    'updated_at' => 1511509792,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32307323,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32307323',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5258085135',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32307323,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32307323',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17033905',
        'method' => 'get',
      ),
    ),
  ),
  2311069785 => 
  array (
    'id' => 17041753,
    'name' => 'ООО "Септа-Юг"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511511762,
    'updated_at' => 1511512033,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32315415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311069785',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32315415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041753',
        'method' => 'get',
      ),
    ),
  ),
  1660188336 => 
  array (
    'id' => 17041831,
    'name' => 'ООО «СКАЙТЕХ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511512158,
    'updated_at' => 1511512158,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32315503,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315503',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660188336',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32315503,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315503',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041831',
        'method' => 'get',
      ),
    ),
  ),
  7817321371 => 
  array (
    'id' => 17009389,
    'name' => 'ООО "Петрософт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511332654,
    'updated_at' => 1511512692,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32262259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7817321371',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'СП',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32262259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32262259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17009389',
        'method' => 'get',
      ),
    ),
  ),
  7203218100 => 
  array (
    'id' => 17030295,
    'name' => 'ООО «РОСИЛЬСТРОЙ-1»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511411610,
    'updated_at' => 1511512727,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32302495,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302495',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203218100',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32302495,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32302495',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17030295',
        'method' => 'get',
      ),
    ),
  ),
  2607002891 => 
  array (
    'id' => 17031815,
    'name' => 'ООО "Компания АМВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511422923,
    'updated_at' => 1511512744,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32304329,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304329',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2607002891',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32304329,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32304329',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17031815',
        'method' => 'get',
      ),
    ),
  ),
  7716741147 => 
  array (
    'id' => 17042079,
    'name' => 'ООО "Доброе питание"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511513183,
    'updated_at' => 1511513192,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32315793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716741147',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32315793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17042079',
        'method' => 'get',
      ),
    ),
  ),
  7743174754 => 
  array (
    'id' => 17040141,
    'name' => 'ООО "ЭЛЕКТРОЛАЙФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511501571,
    'updated_at' => 1511517048,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313363,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313363',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743174754',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313363,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313363',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040141',
        'method' => 'get',
      ),
    ),
  ),
  2014005901 => 
  array (
    'id' => 15318675,
    'name' => 'Дорстройконтроль, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1493796391,
    'updated_at' => 1511520099,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29852659,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29852659',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1511520099,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2014005901',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29852659,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29852659',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15318675',
        'method' => 'get',
      ),
    ),
  ),
  2315176322 => 
  array (
    'id' => 15444841,
    'name' => 'Основа, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1495174843,
    'updated_at' => 1511520906,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30016033,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30016033',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29784601,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29784601',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2315176322',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30016033,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30016033',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15444841',
        'method' => 'get',
      ),
    ),
  ),
  5024168072 => 
  array (
    'id' => 17040121,
    'name' => 'ООО "СТРОЙ ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511501464,
    'updated_at' => 1511520957,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313341,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313341',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024168072',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313341,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313341',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040121',
        'method' => 'get',
      ),
    ),
  ),
  1644000731 => 
  array (
    'id' => 16997839,
    'name' => 'Стройсервис, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1511246970,
    'updated_at' => 1511523380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32248429,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248429',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 18634764,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=18634764',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1644000731',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32248429,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32248429',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997839',
        'method' => 'get',
      ),
    ),
  ),
  2460058589 => 
  array (
    'id' => 17040109,
    'name' => 'ГПКК "Лесосибирск-Автодор"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1511501376,
    'updated_at' => 1511756968,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460058589',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17040109',
        'method' => 'get',
      ),
    ),
  ),
  2461114645 => 
  array (
    'id' => 16775664,
    'name' => '"ПОСАД"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1508984180,
    'updated_at' => 1511758760,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31984302,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31984302',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2461114645',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31984302,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31984302',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16775664',
        'method' => 'get',
      ),
    ),
  ),
  6658492144 => 
  array (
    'id' => 17055887,
    'name' => 'ООО  "Новые Технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511758815,
    'updated_at' => 1511758816,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330591,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330591',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658492144',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330591,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330591',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17055887',
        'method' => 'get',
      ),
    ),
  ),
  2130191624 => 
  array (
    'id' => 17056007,
    'name' => 'ООО  "Внешнеторговая Фирма "Текстильмаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511760743,
    'updated_at' => 1511760756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330765,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330765',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130191624',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330765,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330765',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056007',
        'method' => 'get',
      ),
    ),
  ),
  6623115924 => 
  array (
    'id' => 17056027,
    'name' => 'ООО "Комбинат школьного питания №1"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511761102,
    'updated_at' => 1511761123,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330801,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330801',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6623115924',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330801,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330801',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056027',
        'method' => 'get',
      ),
    ),
  ),
  5034053257 => 
  array (
    'id' => 17056043,
    'name' => 'ООО "РОСТЭКСПОРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511761296,
    'updated_at' => 1511761319,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330825,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330825',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5034053257',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330825,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330825',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056043',
        'method' => 'get',
      ),
    ),
  ),
  9729045415 => 
  array (
    'id' => 17056053,
    'name' => 'ООО «ТАРМЕД»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511761447,
    'updated_at' => 1511761512,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330841,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330841',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9729045415',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330841,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330841',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056053',
        'method' => 'get',
      ),
    ),
  ),
  5032222580 => 
  array (
    'id' => 17055865,
    'name' => 'ООО  "Авто Спец Услуги"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511758470,
    'updated_at' => 1511761751,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330575,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330575',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032222580',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330575,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330575',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17055865',
        'method' => 'get',
      ),
    ),
  ),
  5609064666 => 
  array (
    'id' => 17056129,
    'name' => 'ООО «НПО СОЮЗ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511762227,
    'updated_at' => 1511762227,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5609064666',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056129',
        'method' => 'get',
      ),
    ),
  ),
  3015105481 => 
  array (
    'id' => 17056153,
    'name' => 'ООО "ЮгСтройСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511762500,
    'updated_at' => 1511762501,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330953,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330953',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3015105481',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330953,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330953',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056153',
        'method' => 'get',
      ),
    ),
  ),
  6150095041 => 
  array (
    'id' => 17056203,
    'name' => 'ООО ТК "ГлобалТрейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511763078,
    'updated_at' => 1511763079,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330991,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330991',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6150095041',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330991,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330991',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056203',
        'method' => 'get',
      ),
    ),
  ),
  3616018106 => 
  array (
    'id' => 17041571,
    'name' => 'ООО «ЭКСПРЕССКОМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511511023,
    'updated_at' => 1511763430,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32315225,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315225',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3616018106',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32315225,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32315225',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17041571',
        'method' => 'get',
      ),
    ),
  ),
  7714995763 => 
  array (
    'id' => 17039991,
    'name' => 'АО  "Внешнеэкономическое объединение "Технопромимпорт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511499708,
    'updated_at' => 1511763441,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313195,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313195',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714995763',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313195,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313195',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17039991',
        'method' => 'get',
      ),
    ),
  ),
  3661054875 => 
  array (
    'id' => 17039999,
    'name' => 'ООО "РСО-Энерго"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511499862,
    'updated_at' => 1511763460,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32313205,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313205',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3661054875',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32313205,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32313205',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17039999',
        'method' => 'get',
      ),
    ),
  ),
  2317055161 => 
  array (
    'id' => 17056385,
    'name' => 'ООО "Черномор НГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511763701,
    'updated_at' => 1511763702,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32331303,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331303',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2317055161',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32331303,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331303',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056385',
        'method' => 'get',
      ),
    ),
  ),
  7811632211 => 
  array (
    'id' => 17056463,
    'name' => 'ООО "НЕБОСКРЕБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511764257,
    'updated_at' => 1511764911,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32331375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811632211',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32331375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056463',
        'method' => 'get',
      ),
    ),
  ),
  7726321261 => 
  array (
    'id' => 17056565,
    'name' => 'ООО "ЮГ-СТРОЙЦЕНТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511765146,
    'updated_at' => 1511765146,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32331531,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331531',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726321261',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32331531,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331531',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056565',
        'method' => 'get',
      ),
    ),
  ),
  6027160330 => 
  array (
    'id' => 17056869,
    'name' => 'ООО "ВЕНЕЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511766247,
    'updated_at' => 1511766327,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6027160330',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056869',
        'method' => 'get',
      ),
    ),
  ),
  3302000267 => 
  array (
    'id' => 17057017,
    'name' => 'АО "ВЛАДИМИРСКИЙ ХЛЕБОКОМБИНАТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511766926,
    'updated_at' => 1511766926,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332225,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332225',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3302000267',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332225,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332225',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057017',
        'method' => 'get',
      ),
    ),
  ),
  7726760967 => 
  array (
    'id' => 17057041,
    'name' => 'ООО "МОСКОВСКАЯ ЮРИДИЧЕСКАЯ КОРПОРАЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511767058,
    'updated_at' => 1511767059,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332265,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332265',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726760967',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332265,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332265',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057041',
        'method' => 'get',
      ),
    ),
  ),
  5006264993 => 
  array (
    'id' => 17057053,
    'name' => 'ООО "Первая Газовая Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511767131,
    'updated_at' => 1511767182,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332283,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332283',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5006264993',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332283,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332283',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057053',
        'method' => 'get',
      ),
    ),
  ),
  2901247286 => 
  array (
    'id' => 17057077,
    'name' => 'ООО «Витамин»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511767277,
    'updated_at' => 1511767322,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332301,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332301',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901247286',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332301,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332301',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057077',
        'method' => 'get',
      ),
    ),
  ),
  7705469323 => 
  array (
    'id' => 17057115,
    'name' => 'ООО "Ориентал Дискавери Груп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511767443,
    'updated_at' => 1511767480,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332343,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332343',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705469323',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332343,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332343',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057115',
        'method' => 'get',
      ),
    ),
  ),
  7722393113 => 
  array (
    'id' => 17055923,
    'name' => 'ООО «ПСП-ТЕЛЕКОМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511759377,
    'updated_at' => 1511767551,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330641,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330641',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722393113',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330641,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330641',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17055923',
        'method' => 'get',
      ),
    ),
  ),
  9701061456 => 
  array (
    'id' => 17055945,
    'name' => 'ООО «Виктория»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511759758,
    'updated_at' => 1511767560,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330671,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330671',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701061456',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330671,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330671',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17055945',
        'method' => 'get',
      ),
    ),
  ),
  7731354086 => 
  array (
    'id' => 17055991,
    'name' => 'ООО "МЕД ИНТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511760467,
    'updated_at' => 1511767631,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330743,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330743',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731354086',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'МО',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330743,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330743',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17055991',
        'method' => 'get',
      ),
    ),
  ),
  1659098536 => 
  array (
    'id' => 17055911,
    'name' => 'ООО «Мосты и водоотводные сооружения»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511759233,
    'updated_at' => 1511767669,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330623,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330623',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659098536',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330623,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330623',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17055911',
        'method' => 'get',
      ),
    ),
  ),
  560035775 => 
  array (
    'id' => 17057249,
    'name' => 'ООО "ЛАНДЕС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511768133,
    'updated_at' => 1511768149,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '560035775',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'ДГ 3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057249',
        'method' => 'get',
      ),
    ),
  ),
  7727577837 => 
  array (
    'id' => 17057229,
    'name' => 'ООО Торговый дом "Альфа"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511767992,
    'updated_at' => 1511769338,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332501,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332501',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727577837',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332501,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332501',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057229',
        'method' => 'get',
      ),
    ),
  ),
  7840510063 => 
  array (
    'id' => 17057493,
    'name' => 'ООО "СТУДИЯ "ШОУ КОНСАЛТИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511769505,
    'updated_at' => 1511769536,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32332849,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332849',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840510063',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32332849,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32332849',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17057493',
        'method' => 'get',
      ),
    ),
  ),
);