<?php 
 return array (
  7806131072 => 
  array (
    'id' => 14359914,
    'name' => 'ООО «ЭЛЛАДА»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484292823,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497240,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497240',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806131072',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497240,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497240',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14359914',
        'method' => 'get',
      ),
    ),
  ),
  7802780407 => 
  array (
    'id' => 14359964,
    'name' => 'ООО «Бланкт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484293090,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497300,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497300',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802780407',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497300,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497300',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14359964',
        'method' => 'get',
      ),
    ),
  ),
  7802571139 => 
  array (
    'id' => 14360036,
    'name' => 'ООО "СтройПрофТехнологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484293285,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497370,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497370',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802571139',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497370,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497370',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14360036',
        'method' => 'get',
      ),
    ),
  ),
  7743046079 => 
  array (
    'id' => 14360110,
    'name' => 'ООО Сервис безопасности по Центральному региону',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484293597,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497456,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497456',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743046079',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497456,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497456',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14360110',
        'method' => 'get',
      ),
    ),
  ),
  7735518793 => 
  array (
    'id' => 14360276,
    'name' => 'ООО Инфанта+',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484294369,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497680,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497680',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735518793',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497680,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497680',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14360276',
        'method' => 'get',
      ),
    ),
  ),
  7735150630 => 
  array (
    'id' => 14360346,
    'name' => 'ООО "АНГИОМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484294600,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497784,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497784',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382026,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735150630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497784,
        1 => 28527588,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497784,28527588',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14360346',
        'method' => 'get',
      ),
    ),
  ),
  7733296827 => 
  array (
    'id' => 14360370,
    'name' => 'ООО "СВЯЗЬТЕЛЕКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484294749,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497830,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497830',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733296827',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497830,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497830',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14360370',
        'method' => 'get',
      ),
    ),
  ),
  7728860340 => 
  array (
    'id' => 14360386,
    'name' => 'ООО "СПОРТ ДЛЯ ВСЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484294858,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497860,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497860',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728860340',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497860,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497860',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14360386',
        'method' => 'get',
      ),
    ),
  ),
  7726333884 => 
  array (
    'id' => 14360450,
    'name' => 'ООО "ЭкспоАртСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484295048,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28497928,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497928',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726333884',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28497928,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28497928',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14360450',
        'method' => 'get',
      ),
    ),
  ),
  7725850858 => 
  array (
    'id' => 14361894,
    'name' => 'ООО "ЦЕНТР ТРЕВОЖНАЯ КНОПКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484301540,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28499772,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28499772',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725850858',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28499772,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28499772',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14361894',
        'method' => 'get',
      ),
    ),
  ),
  7722362683 => 
  array (
    'id' => 14362058,
    'name' => 'ООО УНИМЕДФАРМ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484302447,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28499992,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28499992',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722362683',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28499992,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28499992',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14362058',
        'method' => 'get',
      ),
    ),
  ),
  7721376348 => 
  array (
    'id' => 14362156,
    'name' => 'ООО "СПЕКТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484302852,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28500120,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500120',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721376348',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28500120,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500120',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14362156',
        'method' => 'get',
      ),
    ),
  ),
  7720605242 => 
  array (
    'id' => 14362306,
    'name' => 'ООО "СчетМаш-Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484303503,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28500286,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500286',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720605242',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28500286,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500286',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14362306',
        'method' => 'get',
      ),
    ),
  ),
  7715899533 => 
  array (
    'id' => 14362602,
    'name' => 'ООО "ИМПУЛЬС М"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484304844,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28500700,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500700',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715899533',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28500700,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500700',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14362602',
        'method' => 'get',
      ),
    ),
  ),
  7713122220 => 
  array (
    'id' => 14362642,
    'name' => 'ООО ЧОП «КУПОЛ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484305090,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28500766,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500766',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713122220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28500766,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28500766',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14362642',
        'method' => 'get',
      ),
    ),
  ),
  8911028020 => 
  array (
    'id' => 14390500,
    'name' => 'ООО «Тюмень обл. трубопровод строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484541947,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524194,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524194',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8911028020',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524194,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524194',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390500',
        'method' => 'get',
      ),
    ),
  ),
  8602066853 => 
  array (
    'id' => 14390542,
    'name' => 'Акционерное Общество "ЭЛЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484542344,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524234,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524234',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602066853',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524234,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524234',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390542',
        'method' => 'get',
      ),
    ),
  ),
  7451392434 => 
  array (
    'id' => 14390568,
    'name' => 'ООО "МЛК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484542625,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524270,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524270',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451392434',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524270,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524270',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390568',
        'method' => 'get',
      ),
    ),
  ),
  7221002190 => 
  array (
    'id' => 14390594,
    'name' => 'ООО "МАСЛЯНСКОЕ АВТОТРАНСПОРТНОЕ ПРЕДПРИЯТИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484542924,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524310,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524310',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7221002190',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524310,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524310',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390594',
        'method' => 'get',
      ),
    ),
  ),
  7203402445 => 
  array (
    'id' => 14390602,
    'name' => 'ООО "БиоМедСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484543112,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524324,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524324',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203402445',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524324,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524324',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390602',
        'method' => 'get',
      ),
    ),
  ),
  6678037680 => 
  array (
    'id' => 14390618,
    'name' => 'ООО "Спецэлектропрофи"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484543309,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524340,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524340',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678037680',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524340,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524340',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390618',
        'method' => 'get',
      ),
    ),
  ),
  6674301774 => 
  array (
    'id' => 14390634,
    'name' => 'ООО "Компания Информационных Технологий',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484543455,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524358,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524358',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6674301774',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524358,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524358',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390634',
        'method' => 'get',
      ),
    ),
  ),
  6317110980 => 
  array (
    'id' => 14390674,
    'name' => 'ООО "СТОУН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484543775,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524392,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524392',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317110980',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524392,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524392',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390674',
        'method' => 'get',
      ),
    ),
  ),
  6316205710 => 
  array (
    'id' => 14390716,
    'name' => 'ООО СК Атлант',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484544249,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524448,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524448',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316205710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524448,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524448',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390716',
        'method' => 'get',
      ),
    ),
  ),
  6315006631 => 
  array (
    'id' => 14390734,
    'name' => 'ООО "ДЕЛЬФИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484544400,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524478,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524478',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6315006631',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524478,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524478',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390734',
        'method' => 'get',
      ),
    ),
  ),
  5902833754 => 
  array (
    'id' => 14390772,
    'name' => 'ООО «Старт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484544715,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524516,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524516',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902833754',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524516,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524516',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390772',
        'method' => 'get',
      ),
    ),
  ),
  7727273028 => 
  array (
    'id' => 14390884,
    'name' => 'ООО "ТЕХНОМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484545749,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524646,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524646',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727273028',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524646,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524646',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390884',
        'method' => 'get',
      ),
    ),
  ),
  3808221493 => 
  array (
    'id' => 14390926,
    'name' => 'ООО «ББК»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484546121,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524684,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524684',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808221493',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524684,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524684',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14390926',
        'method' => 'get',
      ),
    ),
  ),
  7840339828 => 
  array (
    'id' => 14391022,
    'name' => 'АО "ТЕЛЕКОМПАНИЯ "ПЕТЕРБУРГСКОЕ ТЕЛЕВИДЕНИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484546994,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524798,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524798',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840339828',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524798,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524798',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391022',
        'method' => 'get',
      ),
    ),
  ),
  7839406597 => 
  array (
    'id' => 14391050,
    'name' => 'ООО "РУСЬЭНЕРГОМОНТАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484547179,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524818,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524818',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839406597',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524818,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524818',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391050',
        'method' => 'get',
      ),
    ),
  ),
  7839377297 => 
  array (
    'id' => 14391068,
    'name' => 'ООО «Производственно-коммерческая фирма «РТД»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484547341,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524832,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524832',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839377297',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524832,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524832',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391068',
        'method' => 'get',
      ),
    ),
  ),
  7839033056 => 
  array (
    'id' => 14391108,
    'name' => 'ООО  "ГРУППА КОМПАНИЙ ДА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484547571,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524856,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524856',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839033056',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524856,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524856',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391108',
        'method' => 'get',
      ),
    ),
  ),
  7814658803 => 
  array (
    'id' => 14391132,
    'name' => 'ООО "Медипроф"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484547722,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524870,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524870',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814658803',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524870,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524870',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391132',
        'method' => 'get',
      ),
    ),
  ),
  7814336355 => 
  array (
    'id' => 14391142,
    'name' => 'ООО РЦА ВИСКО',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484547870,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524890,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524890',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814336355',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524890,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524890',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391142',
        'method' => 'get',
      ),
    ),
  ),
  7814219644 => 
  array (
    'id' => 14391164,
    'name' => 'ООО "Сфера"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484548019,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524912,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524912',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814219644',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524912,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524912',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391164',
        'method' => 'get',
      ),
    ),
  ),
  7813582809 => 
  array (
    'id' => 14391178,
    'name' => 'ООО «СК РУССТРОЙ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484548169,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524938,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524938',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813582809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524938,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524938',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391178',
        'method' => 'get',
      ),
    ),
  ),
  7813570497 => 
  array (
    'id' => 14391216,
    'name' => 'ООО «АРС-ПРОЕКТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484548360,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28524984,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524984',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813570497',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28524984,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28524984',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391216',
        'method' => 'get',
      ),
    ),
  ),
  7813162434 => 
  array (
    'id' => 14391280,
    'name' => 'ООО РЕСТЭК-МЕДИА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484548836,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28527036,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527036',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813162434',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28527036,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527036',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391280',
        'method' => 'get',
      ),
    ),
  ),
  7811573277 => 
  array (
    'id' => 14391304,
    'name' => 'ООО "КапиталПроектСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484548963,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28527056,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527056',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811573277',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28527056,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527056',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391304',
        'method' => 'get',
      ),
    ),
  ),
  7810999055 => 
  array (
    'id' => 14391458,
    'name' => 'ООО "РУСТРАНССОДЕРЖАНИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484549228,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28527178,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527178',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810999055',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28527178,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527178',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391458',
        'method' => 'get',
      ),
    ),
  ),
  7805343010 => 
  array (
    'id' => 14391482,
    'name' => 'ООО ВАЛИРИЯ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484549371,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28527588,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527588',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1493297982,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805343010',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28527588,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28527588',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391482',
        'method' => 'get',
      ),
    ),
  ),
  7802805450 => 
  array (
    'id' => 14391536,
    'name' => 'ООО «ЛИДЕР»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484549638,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28528204,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528204',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802805450',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28528204,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528204',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391536',
        'method' => 'get',
      ),
    ),
  ),
  7802759620 => 
  array (
    'id' => 14391734,
    'name' => 'ООО "СПЕЦИАЛИЗИРОВАННАЯ ТОРГОВАЯ КОМПАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484550009,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28528396,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528396',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802759620',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28528396,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528396',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391734',
        'method' => 'get',
      ),
    ),
  ),
  7751001875 => 
  array (
    'id' => 14391998,
    'name' => 'ООО «МИГАВТОКАР»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484550901,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28528686,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528686',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751001875',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28528686,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528686',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14391998',
        'method' => 'get',
      ),
    ),
  ),
  7734609455 => 
  array (
    'id' => 14392050,
    'name' => 'ООО "ГорЖилКомплекс""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484551130,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28528740,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528740',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734609455',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28528740,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28528740',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14392050',
        'method' => 'get',
      ),
    ),
  ),
  7729492829 => 
  array (
    'id' => 14392182,
    'name' => 'ООО "ВиаДомус"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484551607,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28529322,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28529322',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729492829',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28529322,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28529322',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14392182',
        'method' => 'get',
      ),
    ),
  ),
  7729487522 => 
  array (
    'id' => 14392254,
    'name' => 'ООО СИСТЕМА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484551722,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28529354,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28529354',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729487522',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28529354,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28529354',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14392254',
        'method' => 'get',
      ),
    ),
  ),
  7726318692 => 
  array (
    'id' => 14392656,
    'name' => 'ООО "ВЕКТОР СМР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484552948,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28530740,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28530740',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726318692',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28530740,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28530740',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14392656',
        'method' => 'get',
      ),
    ),
  ),
  7724882470 => 
  array (
    'id' => 14392726,
    'name' => 'ООО "АМ-ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484553252,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28530818,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28530818',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724882470',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28530818,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28530818',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14392726',
        'method' => 'get',
      ),
    ),
  ),
  7724315174 => 
  array (
    'id' => 14392770,
    'name' => 'ооо "Смарт Вэй"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484553420,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28531466,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28531466',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724315174',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28531466,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28531466',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14392770',
        'method' => 'get',
      ),
    ),
  ),
  7722729973 => 
  array (
    'id' => 14392810,
    'name' => 'ООО Экспрессиони',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484553601,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28531514,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28531514',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722729973',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28531514,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28531514',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14392810',
        'method' => 'get',
      ),
    ),
  ),
  7720684318 => 
  array (
    'id' => 14393356,
    'name' => 'ООО ДЭМА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484554994,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28532082,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532082',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720684318',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28532082,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532082',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14393356',
        'method' => 'get',
      ),
    ),
  ),
  7720587191 => 
  array (
    'id' => 14393404,
    'name' => 'ООО ЧОП "АМУЛЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484555227,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28532158,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532158',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720587191',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28532158,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532158',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14393404',
        'method' => 'get',
      ),
    ),
  ),
  7719547596 => 
  array (
    'id' => 14393478,
    'name' => 'ЗАО «РуСек системы безопасности»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484555573,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28532250,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532250',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719547596',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28532250,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532250',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14393478',
        'method' => 'get',
      ),
    ),
  ),
  726005425 => 
  array (
    'id' => 14393692,
    'name' => 'ООО "Олимп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484556217,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28532464,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532464',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '726005425',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28532464,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532464',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14393692',
        'method' => 'get',
      ),
    ),
  ),
  1655244480 => 
  array (
    'id' => 14393724,
    'name' => 'ООО «АКВА-МТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484556353,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28532508,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532508',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655244480',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28532508,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532508',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14393724',
        'method' => 'get',
      ),
    ),
  ),
  2224173910 => 
  array (
    'id' => 14394116,
    'name' => 'ООО "Гостиный дворик"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484557465,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28532960,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532960',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224173910',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28532960,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28532960',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394116',
        'method' => 'get',
      ),
    ),
  ),
  2353015823 => 
  array (
    'id' => 14394194,
    'name' => 'Акционерное общество "Энергетик"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484557750,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533040,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533040',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2353015823',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533040,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533040',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394194',
        'method' => 'get',
      ),
    ),
  ),
  2723164170 => 
  array (
    'id' => 14394394,
    'name' => 'ООО   КОТ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484558480,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533282,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533282',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723164170',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533282,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533282',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394394',
        'method' => 'get',
      ),
    ),
  ),
  3328465926 => 
  array (
    'id' => 14394432,
    'name' => 'ООО "Млада-Авто"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484558662,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533342,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533342',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3328465926',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533342,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533342',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394432',
        'method' => 'get',
      ),
    ),
  ),
  3443924952 => 
  array (
    'id' => 14394488,
    'name' => 'ООО "Клинико-диагностическая лаборатория "ДИАЛАЙН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484558927,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533420,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533420',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3443924952',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533420,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533420',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394488',
        'method' => 'get',
      ),
    ),
  ),
  3445073841 => 
  array (
    'id' => 14394526,
    'name' => 'ООО "ЭкоМастер"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484559130,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533490,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533490',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3445073841',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533490,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533490',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394526',
        'method' => 'get',
      ),
    ),
  ),
  3445127448 => 
  array (
    'id' => 14394546,
    'name' => 'ООО "КЛИНИКА АКАДЕМИЧЕСКАЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484559293,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533554,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533554',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3445127448',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533554,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533554',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394546',
        'method' => 'get',
      ),
    ),
  ),
  6164245635 => 
  array (
    'id' => 14394584,
    'name' => 'ООО "СЕВЕРО-КАВКАЗСКОЕ ИНФОРМАЦИОННО-АНАЛИТИЧЕСКОЕ АГЕНТСТВО"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1484559434,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533612,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533612',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6164245635',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533612,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533612',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394584',
        'method' => 'get',
      ),
    ),
  ),
  4027110079 => 
  array (
    'id' => 14394608,
    'name' => 'ООО   СУ-21',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484559491,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533634,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533634',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4027110079',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533634,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533634',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394608',
        'method' => 'get',
      ),
    ),
  ),
  5902030394 => 
  array (
    'id' => 14394820,
    'name' => 'ООО Фабрика современной мебели  Идея',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484560288,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533922,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533922',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902030394',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533922,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533922',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394820',
        'method' => 'get',
      ),
    ),
  ),
  6165193436 => 
  array (
    'id' => 14394850,
    'name' => 'ООО "Решифт"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484560399,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28533966,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533966',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165193436',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28533966,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28533966',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14394850',
        'method' => 'get',
      ),
    ),
  ),
  6678017436 => 
  array (
    'id' => 14395038,
    'name' => 'ООО Компания "Федерал"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484560896,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534130,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534130',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678017436',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534130,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534130',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395038',
        'method' => 'get',
      ),
    ),
  ),
  7716784197 => 
  array (
    'id' => 14395140,
    'name' => 'ООО «НЕОМЕД»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484561181,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534254,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534254',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716784197',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534254,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534254',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395140',
        'method' => 'get',
      ),
    ),
  ),
  7718299453 => 
  array (
    'id' => 14395172,
    'name' => 'ООО "НутриМедикал"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484561288,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534294,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534294',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718299453',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534294,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534294',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395172',
        'method' => 'get',
      ),
    ),
  ),
  7724302827 => 
  array (
    'id' => 14395206,
    'name' => 'ООО "ЭНЕРГО ПРИБОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484561489,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534346,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534346',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724302827',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534346,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534346',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395206',
        'method' => 'get',
      ),
    ),
  ),
  7728619261 => 
  array (
    'id' => 14395274,
    'name' => 'ООО "Про-Инжиниринг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484561822,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534432,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534432',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728619261',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534432,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534432',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395274',
        'method' => 'get',
      ),
    ),
  ),
  7805262139 => 
  array (
    'id' => 14395524,
    'name' => 'ООО ОП "Фарватер"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484562894,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534762,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534762',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805262139',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534762,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534762',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395524',
        'method' => 'get',
      ),
    ),
  ),
  7810389973 => 
  array (
    'id' => 14395588,
    'name' => '"СИС МЕДИКАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484563174,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534852,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534852',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810389973',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534852,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534852',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395588',
        'method' => 'get',
      ),
    ),
  ),
  7811563670 => 
  array (
    'id' => 14395650,
    'name' => 'ООО "СКС-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484563448,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28534932,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534932',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811563670',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28534932,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28534932',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14395650',
        'method' => 'get',
      ),
    ),
  ),
  7709764591 => 
  array (
    'id' => 14396384,
    'name' => 'ООО Аккорд"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484565768,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28535730,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28535730',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709764591',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28535730,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28535730',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14396384',
        'method' => 'get',
      ),
    ),
  ),
  7840447693 => 
  array (
    'id' => 14396422,
    'name' => 'АО "КОНТИНЕНТАЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484566026,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28535792,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28535792',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840447693',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28535792,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28535792',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14396422',
        'method' => 'get',
      ),
    ),
  ),
  7751510632 => 
  array (
    'id' => 14396902,
    'name' => 'ООО "СитиАльпСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484566697,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28536318,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536318',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751510632',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28536318,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536318',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14396902',
        'method' => 'get',
      ),
    ),
  ),
  7728853582 => 
  array (
    'id' => 14396990,
    'name' => 'ООО  "АВАНТАЖ - СТРОИТЕЛЬНАЯ ТЕХНИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484567009,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28536422,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536422',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728853582',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28536422,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536422',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14396990',
        'method' => 'get',
      ),
    ),
  ),
  7723011776 => 
  array (
    'id' => 14397048,
    'name' => 'ЗАО "РАК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484567223,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28536502,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536502',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723011776',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28536502,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536502',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14397048',
        'method' => 'get',
      ),
    ),
  ),
  3017036508 => 
  array (
    'id' => 14397188,
    'name' => 'ООО "ПРОИЗВОДСТВЕННО-КОММЕРЧЕСКАЯ ФИРМА "ГРАП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484567724,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28536626,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536626',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3017036508',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28536626,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536626',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14397188',
        'method' => 'get',
      ),
    ),
  ),
  7106502081 => 
  array (
    'id' => 14397258,
    'name' => 'ООО производственно коммерческая фирма "Техналюм"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484568003,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28536704,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536704',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7106502081',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28536704,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536704',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14397258',
        'method' => 'get',
      ),
    ),
  ),
  7724934760 => 
  array (
    'id' => 14397398,
    'name' => 'ООО "ИНСАЙД АРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484568525,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28536854,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536854',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724934760',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28536854,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28536854',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14397398',
        'method' => 'get',
      ),
    ),
  ),
  274099457 => 
  array (
    'id' => 14403320,
    'name' => 'ООО  "БашРемАвто"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484628483,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28542968,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28542968',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274099457',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28542968,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28542968',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403320',
        'method' => 'get',
      ),
    ),
  ),
  323368786 => 
  array (
    'id' => 14403340,
    'name' => 'ООО "МВ-ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484628873,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28542988,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28542988',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323368786',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28542988,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28542988',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403340',
        'method' => 'get',
      ),
    ),
  ),
  326016461 => 
  array (
    'id' => 14403342,
    'name' => 'ООО ЧОП "ДЕЛЬТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484628939,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28542992,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28542992',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326016461',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28542992,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28542992',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403342',
        'method' => 'get',
      ),
    ),
  ),
  2222034540 => 
  array (
    'id' => 14403382,
    'name' => 'ООО ЧОП "Дивизион"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484629219,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543036,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543036',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2222034540',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543036,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543036',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403382',
        'method' => 'get',
      ),
    ),
  ),
  2224147268 => 
  array (
    'id' => 14403406,
    'name' => 'ООО ЧОП "АНАКОНДА II""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484629486,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543092,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543092',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224147268',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543092,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543092',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403406',
        'method' => 'get',
      ),
    ),
  ),
  2225133162 => 
  array (
    'id' => 14403474,
    'name' => 'ООО "Лабико"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484630182,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543232,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543232',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225133162',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543232,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543232',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403474',
        'method' => 'get',
      ),
    ),
  ),
  2462227553 => 
  array (
    'id' => 14403532,
    'name' => 'ООО "Техника"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484631060,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543300,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543300',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2462227553',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543300,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543300',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403532',
        'method' => 'get',
      ),
    ),
  ),
  3812148609 => 
  array (
    'id' => 14403556,
    'name' => 'ООО "ФИДЕС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484631372,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543328,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543328',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812148609',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543328,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543328',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403556',
        'method' => 'get',
      ),
    ),
  ),
  4205264788 => 
  array (
    'id' => 14403600,
    'name' => 'ООО  "Альянс-К"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484631977,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543384,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543384',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205264788',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543384,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543384',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403600',
        'method' => 'get',
      ),
    ),
  ),
  5402537482 => 
  array (
    'id' => 14403618,
    'name' => 'ООО АГЕНТСТВО ДОМ ЧИСТОТЫ "СИБИРЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484632157,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543410,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543410',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402537482',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543410,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543410',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403618',
        'method' => 'get',
      ),
    ),
  ),
  5906139459 => 
  array (
    'id' => 14403726,
    'name' => 'ООО   ГЕЛИОС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484633104,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543542,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543542',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5906139459',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543542,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543542',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403726',
        'method' => 'get',
      ),
    ),
  ),
  6317103285 => 
  array (
    'id' => 14403780,
    'name' => 'ООО "ПАРИТЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484633679,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543606,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543606',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317103285',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543606,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543606',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403780',
        'method' => 'get',
      ),
    ),
  ),
  6658473938 => 
  array (
    'id' => 14403810,
    'name' => 'ООО  "Эстейт Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484633966,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543640,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543640',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658473938',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543640,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543640',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403810',
        'method' => 'get',
      ),
    ),
  ),
  6670383624 => 
  array (
    'id' => 14403824,
    'name' => 'ООО "Научно-производственное объединение СтройМедСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484634093,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543664,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543664',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670383624',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543664,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543664',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403824',
        'method' => 'get',
      ),
    ),
  ),
  7206045696 => 
  array (
    'id' => 14403862,
    'name' => 'ООО "Газстройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484634381,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543710,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543710',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7206045696',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543710,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543710',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403862',
        'method' => 'get',
      ),
    ),
  ),
  7453243220 => 
  array (
    'id' => 14403904,
    'name' => 'ООО   ЕСК-Проект',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484634725,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543754,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543754',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7453243220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543754,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543754',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403904',
        'method' => 'get',
      ),
    ),
  ),
  7707788491 => 
  array (
    'id' => 14403914,
    'name' => 'ООО "СТРОЙМОНТАЖ-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484634864,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28543764,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543764',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707788491',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28543764,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28543764',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14403914',
        'method' => 'get',
      ),
    ),
  ),
  7839473970 => 
  array (
    'id' => 14404352,
    'name' => 'ООО "Клемэ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484636985,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544214,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544214',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839473970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544214,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544214',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404352',
        'method' => 'get',
      ),
    ),
  ),
  7839306715 => 
  array (
    'id' => 14404424,
    'name' => 'ООО "Амбуланс - Транс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484637205,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544294,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544294',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839306715',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544294,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544294',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404424',
        'method' => 'get',
      ),
    ),
  ),
  7838498605 => 
  array (
    'id' => 14404452,
    'name' => 'ООО КЛИНИНГ СЕРВИС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484637346,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544324,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544324',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838498605',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544324,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544324',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404452',
        'method' => 'get',
      ),
    ),
  ),
  7816419462 => 
  array (
    'id' => 14404534,
    'name' => 'ООО Конквест',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484637726,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544398,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544398',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816419462',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544398,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544398',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404534',
        'method' => 'get',
      ),
    ),
  ),
  7816273171 => 
  array (
    'id' => 14404578,
    'name' => 'ООО "АРТПАНТЕОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484638004,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544474,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544474',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816273171',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544474,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544474',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404578',
        'method' => 'get',
      ),
    ),
  ),
  7810829984 => 
  array (
    'id' => 14404802,
    'name' => 'ООО "Арсенал"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484638912,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544696,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544696',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810829984',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544696,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544696',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404802',
        'method' => 'get',
      ),
    ),
  ),
  7810639976 => 
  array (
    'id' => 14404846,
    'name' => 'Акционерное общество "Полярная морская геологоразведочная экспедиция"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484639117,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544734,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544734',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810639976',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544734,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544734',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404846',
        'method' => 'get',
      ),
    ),
  ),
  7810566118 => 
  array (
    'id' => 14404874,
    'name' => 'ООО "Петербургский Информационно-внедренческий Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484639241,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544768,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544768',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810566118',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544768,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544768',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404874',
        'method' => 'get',
      ),
    ),
  ),
  7807035981 => 
  array (
    'id' => 14404944,
    'name' => 'ООО Петролайн',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484639437,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544836,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544836',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807035981',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544836,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544836',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14404944',
        'method' => 'get',
      ),
    ),
  ),
  7806342080 => 
  array (
    'id' => 14405088,
    'name' => 'ООО «СОЮЗ-СТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484639850,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28544948,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544948',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806342080',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28544948,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28544948',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14405088',
        'method' => 'get',
      ),
    ),
  ),
  7806207250 => 
  array (
    'id' => 14405166,
    'name' => 'ООО "ПСК "Бастион"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484640118,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28545026,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28545026',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806207250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28545026,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28545026',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14405166',
        'method' => 'get',
      ),
    ),
  ),
  7805341687 => 
  array (
    'id' => 14406066,
    'name' => 'ООО "БАЛТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484642621,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28547940,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28547940',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805341687',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28547940,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28547940',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406066',
        'method' => 'get',
      ),
    ),
  ),
  7751018149 => 
  array (
    'id' => 14406132,
    'name' => 'ООО "Жилищно-эксплуатационная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484642939,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548020,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548020',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751018149',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548020,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548020',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406132',
        'method' => 'get',
      ),
    ),
  ),
  7730026117 => 
  array (
    'id' => 14406230,
    'name' => 'ООО "АРТ дизайн"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484643443,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548160,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548160',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730026117',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548160,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548160',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406230',
        'method' => 'get',
      ),
    ),
  ),
  7729457736 => 
  array (
    'id' => 14406352,
    'name' => 'ООО "КАЛИНА ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484643605,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548226,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548226',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729457736',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548226,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548226',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406352',
        'method' => 'get',
      ),
    ),
  ),
  7729389589 => 
  array (
    'id' => 14406396,
    'name' => 'ООО "Гарант-Парк-Интернет"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484643792,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548282,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548282',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729389589',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548282,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548282',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406396',
        'method' => 'get',
      ),
    ),
  ),
  7728856181 => 
  array (
    'id' => 14406450,
    'name' => 'ООО «АА ЭМЖДИ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484644013,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548346,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548346',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728856181',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548346,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548346',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406450',
        'method' => 'get',
      ),
    ),
  ),
  7727188076 => 
  array (
    'id' => 14406510,
    'name' => 'ООО "ЕгаМедика"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484644283,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548408,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548408',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727188076',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548408,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548408',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406510',
        'method' => 'get',
      ),
    ),
  ),
  7726762428 => 
  array (
    'id' => 14406712,
    'name' => 'ООО ПИЭС СЕРВИС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484645244,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548676,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548676',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726762428',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548676,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548676',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406712',
        'method' => 'get',
      ),
    ),
  ),
  7725311214 => 
  array (
    'id' => 14406910,
    'name' => 'ООО "СОЛНЕЧНЫЙ БЕРЕГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484646108,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548928,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548928',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725311214',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548928,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548928',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406910',
        'method' => 'get',
      ),
    ),
  ),
  7722368170 => 
  array (
    'id' => 14406996,
    'name' => 'ООО «Авториум»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484646457,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28548976,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548976',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722368170',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28548976,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28548976',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14406996',
        'method' => 'get',
      ),
    ),
  ),
  7720353404 => 
  array (
    'id' => 14407034,
    'name' => 'ООО «МОРИЯ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484646567,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28549016,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549016',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720353404',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28549016,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549016',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14407034',
        'method' => 'get',
      ),
    ),
  ),
  7716685051 => 
  array (
    'id' => 14407096,
    'name' => 'ООО "НИВА-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484646820,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28549082,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549082',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716685051',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28549082,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549082',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14407096',
        'method' => 'get',
      ),
    ),
  ),
  7714831525 => 
  array (
    'id' => 14407144,
    'name' => 'ООО "ТРЕНИЛОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484646979,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28549136,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549136',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714831525',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28549136,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549136',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14407144',
        'method' => 'get',
      ),
    ),
  ),
  7709369383 => 
  array (
    'id' => 14407202,
    'name' => 'ООО "ПРОФКОМП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484647198,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28549190,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549190',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709369383',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28549190,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549190',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14407202',
        'method' => 'get',
      ),
    ),
  ),
  7705801362 => 
  array (
    'id' => 14407342,
    'name' => 'ООО Промэксплуатация',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484647738,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28549330,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549330',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705801362',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28549330,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549330',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14407342',
        'method' => 'get',
      ),
    ),
  ),
  7703406617 => 
  array (
    'id' => 14407516,
    'name' => 'ООО  "СИТИТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484648261,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28549524,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549524',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703406617',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28549524,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28549524',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14407516',
        'method' => 'get',
      ),
    ),
  ),
  2634807461 => 
  array (
    'id' => 14408470,
    'name' => 'ООО МИР КОМФОРТА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484651742,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28550742,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550742',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2634807461',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28550742,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550742',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14408470',
        'method' => 'get',
      ),
    ),
  ),
  2901249646 => 
  array (
    'id' => 14408514,
    'name' => 'ООО "ЦентрАвтоРемонт"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484651931,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28550910,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550910',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901249646',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28550910,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550910',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14408514',
        'method' => 'get',
      ),
    ),
  ),
  3906967508 => 
  array (
    'id' => 14408556,
    'name' => 'ООО  "Ал-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484652081,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28550950,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550950',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906967508',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28550950,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550950',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14408556',
        'method' => 'get',
      ),
    ),
  ),
  5050123952 => 
  array (
    'id' => 14408580,
    'name' => 'ООО "ПРОГРЕСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484652196,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28550988,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550988',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5050123952',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28550988,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28550988',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14408580',
        'method' => 'get',
      ),
    ),
  ),
  7721292507 => 
  array (
    'id' => 14408686,
    'name' => 'ООО "Нордстар"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484652701,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28551100,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28551100',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721292507',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28551100,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28551100',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14408686',
        'method' => 'get',
      ),
    ),
  ),
  7727181698 => 
  array (
    'id' => 14408732,
    'name' => 'ООО «МЕРИДИАН-СЕРВИС»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484652890,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28551146,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28551146',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727181698',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28551146,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28551146',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14408732',
        'method' => 'get',
      ),
    ),
  ),
  3444218540 => 
  array (
    'id' => 14409816,
    'name' => 'ООО СК ВОЛГОРЕМСТРОЙ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484655948,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28552484,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552484',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444218540',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28552484,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552484',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14409816',
        'method' => 'get',
      ),
    ),
  ),
  5032284516 => 
  array (
    'id' => 14410138,
    'name' => 'ООО "ИнтерМедСнаб"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484657033,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28552812,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552812',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032284516',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28552812,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552812',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14410138',
        'method' => 'get',
      ),
    ),
  ),
  7701990888 => 
  array (
    'id' => 14410220,
    'name' => 'ООО Авторъ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484657347,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28552918,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552918',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701990888',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28552918,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552918',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14410220',
        'method' => 'get',
      ),
    ),
  ),
  7715833187 => 
  array (
    'id' => 14410286,
    'name' => 'ООО "АСМГРИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484657536,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28552984,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552984',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715833187',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28552984,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28552984',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14410286',
        'method' => 'get',
      ),
    ),
  ),
  7721353446 => 
  array (
    'id' => 14410318,
    'name' => 'ООО Би энд эР',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484657682,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28553018,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28553018',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721353446',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28553018,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28553018',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14410318',
        'method' => 'get',
      ),
    ),
  ),
  7814627805 => 
  array (
    'id' => 14410486,
    'name' => 'ООО "Черный Феникс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484658464,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28553304,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28553304',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814627805',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28553304,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28553304',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14410486',
        'method' => 'get',
      ),
    ),
  ),
  1701048637 => 
  array (
    'id' => 14414720,
    'name' => 'ООО "Суугу"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484716045,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557048,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557048',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1701048637',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557048,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557048',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414720',
        'method' => 'get',
      ),
    ),
  ),
  1708004068 => 
  array (
    'id' => 14414748,
    'name' => 'ООО "ОВЮРСКИЙ ДРСУЧ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484716420,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557088,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557088',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1708004068',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557088,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557088',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414748',
        'method' => 'get',
      ),
    ),
  ),
  1710002290 => 
  array (
    'id' => 14414756,
    'name' => 'ООО "ДОРОЖНИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484716508,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557092,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557092',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1710002290',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557092,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557092',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414756',
        'method' => 'get',
      ),
    ),
  ),
  2464054225 => 
  array (
    'id' => 14414786,
    'name' => 'ООО ТК "Конверсия"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484716747,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557126,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557126',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464054225',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557126,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557126',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414786',
        'method' => 'get',
      ),
    ),
  ),
  4214032864 => 
  array (
    'id' => 14414828,
    'name' => 'ООО "СК"ПГС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484717165,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557166,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557166',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4214032864',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557166,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557166',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414828',
        'method' => 'get',
      ),
    ),
  ),
  4217161071 => 
  array (
    'id' => 14414836,
    'name' => 'ООО "АТЭК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484717234,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557182,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557182',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4217161071',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557182,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557182',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414836',
        'method' => 'get',
      ),
    ),
  ),
  5503161765 => 
  array (
    'id' => 14414862,
    'name' => 'ООО «Прогресс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484717446,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557208,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557208',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503161765',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557208,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557208',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414862',
        'method' => 'get',
      ),
    ),
  ),
  6672309658 => 
  array (
    'id' => 14414888,
    'name' => 'ООО «ПСК «Доминанта»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484717599,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557230,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557230',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672309658',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557230,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557230',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414888',
        'method' => 'get',
      ),
    ),
  ),
  7017199281 => 
  array (
    'id' => 14414960,
    'name' => 'ООО "ЛСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484718213,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557294,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557294',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017199281',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557294,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557294',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414960',
        'method' => 'get',
      ),
    ),
  ),
  7017374695 => 
  array (
    'id' => 14414966,
    'name' => 'ООО "ЭВАЗМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484718294,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557300,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557300',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017374695',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557300,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557300',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14414966',
        'method' => 'get',
      ),
    ),
  ),
  9729024408 => 
  array (
    'id' => 14415102,
    'name' => 'ООО "ПРОСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484719592,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557452,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557452',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9729024408',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557452,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557452',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415102',
        'method' => 'get',
      ),
    ),
  ),
  9718000050 => 
  array (
    'id' => 14415106,
    'name' => 'ООО "МЕДТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484719702,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557460,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557460',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718000050',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557460,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557460',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415106',
        'method' => 'get',
      ),
    ),
  ),
  9715271213 => 
  array (
    'id' => 14415128,
    'name' => 'ООО "АРАКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484719850,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557478,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557478',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715271213',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557478,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557478',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415128',
        'method' => 'get',
      ),
    ),
  ),
  9705038553 => 
  array (
    'id' => 14415140,
    'name' => 'ООО "ПрофТрейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484719940,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557496,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557496',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705038553',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557496,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557496',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415140',
        'method' => 'get',
      ),
    ),
  ),
  9705003744 => 
  array (
    'id' => 14415160,
    'name' => 'ООО "ЛЕКС ФАРМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484720127,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557516,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557516',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705003744',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557516,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557516',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415160',
        'method' => 'get',
      ),
    ),
  ),
  7842435260 => 
  array (
    'id' => 14415186,
    'name' => 'ООО "Медиа-диалог"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484720333,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557562,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557562',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842435260',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557562,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557562',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415186',
        'method' => 'get',
      ),
    ),
  ),
  7842046390 => 
  array (
    'id' => 14415200,
    'name' => 'ООО "АЛЬЯНС-БУНКЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484720491,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557584,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557584',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842046390',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557584,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557584',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415200',
        'method' => 'get',
      ),
    ),
  ),
  7816451466 => 
  array (
    'id' => 14415282,
    'name' => 'ООО РСУ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484721008,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557684,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557684',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816451466',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557684,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557684',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415282',
        'method' => 'get',
      ),
    ),
  ),
  7814629231 => 
  array (
    'id' => 14415298,
    'name' => 'ООО "АльтусМед СПб"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484721151,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557702,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557702',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814629231',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557702,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557702',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415298',
        'method' => 'get',
      ),
    ),
  ),
  7814614940 => 
  array (
    'id' => 14415340,
    'name' => 'ООО "СпецСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484721380,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557756,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557756',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814614940',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557756,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557756',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415340',
        'method' => 'get',
      ),
    ),
  ),
  7813215630 => 
  array (
    'id' => 14415364,
    'name' => 'ООО "СевЗапСпецСвязь"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484721666,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557796,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557796',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813215630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557796,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557796',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415364',
        'method' => 'get',
      ),
    ),
  ),
  7811236916 => 
  array (
    'id' => 14415394,
    'name' => 'ООО "ПАРТНЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484721904,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557846,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557846',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811236916',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557846,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557846',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415394',
        'method' => 'get',
      ),
    ),
  ),
  7811202522 => 
  array (
    'id' => 14415516,
    'name' => '"НейроМед СПб"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484722706,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28557986,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557986',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811202522',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28557986,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28557986',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415516',
        'method' => 'get',
      ),
    ),
  ),
  7811076878 => 
  array (
    'id' => 14415534,
    'name' => 'ООО "БЕТИНА ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484722812,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28558012,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558012',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811076878',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28558012,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558012',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415534',
        'method' => 'get',
      ),
    ),
  ),
  7810378971 => 
  array (
    'id' => 14415672,
    'name' => 'ООО "МегаЛит"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484723158,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28558162,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558162',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810378971',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28558162,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558162',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415672',
        'method' => 'get',
      ),
    ),
  ),
  7807080991 => 
  array (
    'id' => 14415694,
    'name' => 'ООО "М-ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484723266,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28558192,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558192',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807080991',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28558192,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558192',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415694',
        'method' => 'get',
      ),
    ),
  ),
  7806513106 => 
  array (
    'id' => 14415716,
    'name' => 'ООО "АВТО-НовА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484723361,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28558218,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558218',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806513106',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28558218,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558218',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415716',
        'method' => 'get',
      ),
    ),
  ),
  7806187692 => 
  array (
    'id' => 14415754,
    'name' => 'ООО «Мастер Кар»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484723631,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28558288,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558288',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806187692',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28558288,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558288',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415754',
        'method' => 'get',
      ),
    ),
  ),
  7734736164 => 
  array (
    'id' => 14415978,
    'name' => 'ООО "ТРУКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484724675,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28558564,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558564',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734736164',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28558564,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28558564',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14415978',
        'method' => 'get',
      ),
    ),
  ),
  7722698130 => 
  array (
    'id' => 14417440,
    'name' => 'ООО "Транспорт 21 век"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484728272,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28560326,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28560326',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722698130',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28560326,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28560326',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14417440',
        'method' => 'get',
      ),
    ),
  ),
  7709953084 => 
  array (
    'id' => 14418022,
    'name' => 'ООО "Реномекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484730454,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28560906,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28560906',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709953084',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28560906,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28560906',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418022',
        'method' => 'get',
      ),
    ),
  ),
  7709935649 => 
  array (
    'id' => 14418106,
    'name' => 'ООО "Кинопроизводственная мастерская"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484730797,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561000,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561000',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709935649',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561000,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561000',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418106',
        'method' => 'get',
      ),
    ),
  ),
  7708246519 => 
  array (
    'id' => 14418260,
    'name' => 'ООО "Акцент Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484731249,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561168,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561168',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708246519',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561168,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561168',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418260',
        'method' => 'get',
      ),
    ),
  ),
  7707319370 => 
  array (
    'id' => 14418308,
    'name' => 'ООО "ПРОСТАР+"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484731457,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561240,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561240',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707319370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561240,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561240',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418308',
        'method' => 'get',
      ),
    ),
  ),
  7705553744 => 
  array (
    'id' => 14418338,
    'name' => 'ООО "СтройТоргГрупп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484731542,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561258,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561258',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705553744',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561258,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561258',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418338',
        'method' => 'get',
      ),
    ),
  ),
  7704606425 => 
  array (
    'id' => 14418388,
    'name' => 'ООО  "ЭТАЛОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484731853,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561310,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561310',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704606425',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561310,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561310',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418388',
        'method' => 'get',
      ),
    ),
  ),
  7701082043 => 
  array (
    'id' => 14418584,
    'name' => 'ООО ОПТ-ТОРГ-МАСТЕР',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484732634,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561504,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561504',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701082043',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561504,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561504',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418584',
        'method' => 'get',
      ),
    ),
  ),
  7604210973 => 
  array (
    'id' => 14418612,
    'name' => 'ООО ЧОО "Сокол 2"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484732744,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561544,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561544',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7604210973',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561544,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561544',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418612',
        'method' => 'get',
      ),
    ),
  ),
  7203144392 => 
  array (
    'id' => 14418634,
    'name' => 'ООО «Тюменские аэрозоли»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484732868,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561562,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561562',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203144392',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561562,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561562',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418634',
        'method' => 'get',
      ),
    ),
  ),
  7810891069 => 
  array (
    'id' => 14418960,
    'name' => 'ООО "Центр Реабилитационных Услуг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484734263,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561928,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561928',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810891069',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561928,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561928',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418960',
        'method' => 'get',
      ),
    ),
  ),
  7805604270 => 
  array (
    'id' => 14419022,
    'name' => 'ООО "АСПЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484734446,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28562010,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562010',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805604270',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28562010,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562010',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14419022',
        'method' => 'get',
      ),
    ),
  ),
  7743105380 => 
  array (
    'id' => 14419138,
    'name' => 'ООО ЛОГРУС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484734998,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28562174,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562174',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743105380',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28562174,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562174',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14419138',
        'method' => 'get',
      ),
    ),
  ),
  7736252747 => 
  array (
    'id' => 14419174,
    'name' => 'ООО "ИНВЕСТ ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484735212,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28562230,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562230',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736252747',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28562230,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562230',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14419174',
        'method' => 'get',
      ),
    ),
  ),
  7729606160 => 
  array (
    'id' => 14419208,
    'name' => 'ООО "СиДиАйПи - ДжиЭс ПресиТех"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484735357,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28562258,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562258',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729606160',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28562258,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562258',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14419208',
        'method' => 'get',
      ),
    ),
  ),
  7729494287 => 
  array (
    'id' => 14419432,
    'name' => 'ООО Алестас',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484736425,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28562484,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562484',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729494287',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28562484,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562484',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14419432',
        'method' => 'get',
      ),
    ),
  ),
  2130151702 => 
  array (
    'id' => 14419490,
    'name' => 'ООО  «Ремком»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484736725,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28562572,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562572',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130151702',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28562572,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562572',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14419490',
        'method' => 'get',
      ),
    ),
  ),
  2635136706 => 
  array (
    'id' => 14419588,
    'name' => 'ООО "Профи-Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484737161,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28562686,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562686',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635136706',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28562686,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28562686',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14419588',
        'method' => 'get',
      ),
    ),
  ),
  7709484146 => 
  array (
    'id' => 14420140,
    'name' => 'ООО "СП-Комплекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484739642,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28563288,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28563288',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709484146',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28563288,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28563288',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14420140',
        'method' => 'get',
      ),
    ),
  ),
  7802520455 => 
  array (
    'id' => 14420366,
    'name' => 'ООО Производственное предприятие  Энерго-Плюс',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484739974,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28563718,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28563718',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802520455',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28563718,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28563718',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14420366',
        'method' => 'get',
      ),
    ),
  ),
  7810831239 => 
  array (
    'id' => 14420388,
    'name' => 'ООО  "Энерго-Моторс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484740099,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28563748,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28563748',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810831239',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28563748,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28563748',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14420388',
        'method' => 'get',
      ),
    ),
  ),
  278211854 => 
  array (
    'id' => 14425614,
    'name' => 'ООО "Финаудит и Консалтинг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484805132,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28569876,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569876',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278211854',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28569876,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569876',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425614',
        'method' => 'get',
      ),
    ),
  ),
  5503236139 => 
  array (
    'id' => 14425638,
    'name' => 'ООО ЧОП Арвад',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484805481,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28569908,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569908',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503236139',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28569908,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569908',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425638',
        'method' => 'get',
      ),
    ),
  ),
  5646032655 => 
  array (
    'id' => 14425662,
    'name' => 'ООО "ГАРАНТ-МАСТЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484805707,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28569934,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569934',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5646032655',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28569934,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569934',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425662',
        'method' => 'get',
      ),
    ),
  ),
  6312162960 => 
  array (
    'id' => 14425670,
    'name' => 'ООО АЛЬТЕРА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484805781,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28569944,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569944',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312162960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28569944,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28569944',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425670',
        'method' => 'get',
      ),
    ),
  ),
  274919340 => 
  array (
    'id' => 14425768,
    'name' => 'ООО РУСМЕДИКАЛ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484806734,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28570078,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570078',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274919340',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28570078,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570078',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425768',
        'method' => 'get',
      ),
    ),
  ),
  7839360180 => 
  array (
    'id' => 14425856,
    'name' => 'ООО «Транслинк Лтд»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484807519,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28570196,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570196',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839360180',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28570196,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570196',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425856',
        'method' => 'get',
      ),
    ),
  ),
  7839042597 => 
  array (
    'id' => 14425878,
    'name' => 'ООО Испытательная лаборатория  «Медтехника»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484807614,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28570206,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570206',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839042597',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28570206,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570206',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425878',
        'method' => 'get',
      ),
    ),
  ),
  7816589947 => 
  array (
    'id' => 14425960,
    'name' => 'ООО  «ОПТОВАЯ МЕДИЦИНСКАЯ КОМПАНИЯ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484808015,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28570306,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570306',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816589947',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28570306,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28570306',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14425960',
        'method' => 'get',
      ),
    ),
  ),
  7813169246 => 
  array (
    'id' => 14426678,
    'name' => 'ООО "ОХРАННАЯ ОРГАНИЗАЦИЯ "БОРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484810787,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28571106,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571106',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813169246',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28571106,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571106',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14426678',
        'method' => 'get',
      ),
    ),
  ),
  7804411042 => 
  array (
    'id' => 14426752,
    'name' => 'ООО "СКИФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484811373,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28571230,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571230',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804411042',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28571230,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571230',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14426752',
        'method' => 'get',
      ),
    ),
  ),
  7736660087 => 
  array (
    'id' => 14426816,
    'name' => 'ООО "АНБ ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484811640,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28571308,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571308',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736660087',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28571308,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571308',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14426816',
        'method' => 'get',
      ),
    ),
  ),
  7730191985 => 
  array (
    'id' => 14426898,
    'name' => 'ООО "АСТЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484812082,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28571436,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571436',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730191985',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28571436,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571436',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14426898',
        'method' => 'get',
      ),
    ),
  ),
  7727098986 => 
  array (
    'id' => 14426924,
    'name' => 'ООО «МЕГАЭКСПРЕСС.ПРО»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484812169,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28571452,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571452',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727098986',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28571452,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28571452',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14426924',
        'method' => 'get',
      ),
    ),
  ),
  7725294167 => 
  array (
    'id' => 14428078,
    'name' => 'ООО "ПРОМЕДТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484814368,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28572712,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28572712',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725294167',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28572712,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28572712',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14428078',
        'method' => 'get',
      ),
    ),
  ),
  7724338164 => 
  array (
    'id' => 14428710,
    'name' => 'ООО "Р-ТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484817411,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28573472,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573472',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724338164',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28573472,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573472',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14428710',
        'method' => 'get',
      ),
    ),
  ),
  7722714078 => 
  array (
    'id' => 14428898,
    'name' => 'ООО  "РЕЗОЛЮЦИЯ ПРАВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484818275,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28573700,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573700',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722714078',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28573700,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573700',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14428898',
        'method' => 'get',
      ),
    ),
  ),
  7721837172 => 
  array (
    'id' => 14428936,
    'name' => 'ООО МонтажСтрой',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484818424,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28573718,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573718',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721837172',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28573718,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573718',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14428936',
        'method' => 'get',
      ),
    ),
  ),
  7719863506 => 
  array (
    'id' => 14428986,
    'name' => 'ООО Ремьен',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484818726,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28573802,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573802',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719863506',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28573802,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573802',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14428986',
        'method' => 'get',
      ),
    ),
  ),
  7718644727 => 
  array (
    'id' => 14429120,
    'name' => 'ООО Логос',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484819572,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28573974,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573974',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718644727',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28573974,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28573974',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429120',
        'method' => 'get',
      ),
    ),
  ),
  7714359888 => 
  array (
    'id' => 14429180,
    'name' => 'ООО ГеннСтрой',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484819792,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574052,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574052',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714359888',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574052,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574052',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429180',
        'method' => 'get',
      ),
    ),
  ),
  7713184314 => 
  array (
    'id' => 14429198,
    'name' => 'ООО ЧОП "СПО ГРАД 9"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484819876,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574070,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574070',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713184314',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574070,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574070',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429198',
        'method' => 'get',
      ),
    ),
  ),
  7709975306 => 
  array (
    'id' => 14429224,
    'name' => 'ООО "Герда Медикал"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484819975,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574092,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574092',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709975306',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574092,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574092',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429224',
        'method' => 'get',
      ),
    ),
  ),
  7707575197 => 
  array (
    'id' => 14429262,
    'name' => 'АНО "РЕСПУБЛИКАНСКИЙ НАУЧНО - ИССЛЕДОВАТЕЛЬСКИЙ ИНСТИТУТ ИНТЕЛЛЕКТУАЛЬНОЙ СОБСТВЕННОСТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484820147,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574128,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574128',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707575197',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574128,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574128',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429262',
        'method' => 'get',
      ),
    ),
  ),
  7707364076 => 
  array (
    'id' => 14429272,
    'name' => 'ООО "МП-МЕДИКАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484820219,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574140,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574140',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707364076',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574140,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574140',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429272',
        'method' => 'get',
      ),
    ),
  ),
  7704864754 => 
  array (
    'id' => 14429308,
    'name' => 'ООО "Бурятская горнорудная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484820389,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574184,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574184',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704864754',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574184,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574184',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429308',
        'method' => 'get',
      ),
    ),
  ),
  7733812920 => 
  array (
    'id' => 14429426,
    'name' => 'ООО Мед Экспресс',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484821002,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574348,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574348',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733812920',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574348,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574348',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429426',
        'method' => 'get',
      ),
    ),
  ),
  7727271694 => 
  array (
    'id' => 14429586,
    'name' => 'ООО ЭМИССИОННЫЙ ЦЕНТР КАРТ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484821586,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28574520,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574520',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727271694',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28574520,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28574520',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14429586',
        'method' => 'get',
      ),
    ),
  ),
  3444160435 => 
  array (
    'id' => 14430320,
    'name' => 'ООО «И-ВОЛГА»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484825201,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28575422,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575422',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444160435',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28575422,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575422',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14430320',
        'method' => 'get',
      ),
    ),
  ),
  3525254177 => 
  array (
    'id' => 14430368,
    'name' => 'ООО "ГорЭнерго"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484825469,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28575492,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575492',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525254177',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28575492,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575492',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14430368',
        'method' => 'get',
      ),
    ),
  ),
  5809903005 => 
  array (
    'id' => 14430408,
    'name' => 'ООО "Центр-Сервис +"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484825639,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28575542,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575542',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5809903005',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28575542,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575542',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14430408',
        'method' => 'get',
      ),
    ),
  ),
  6903037500 => 
  array (
    'id' => 14430486,
    'name' => 'ООО ЧОО "Ракус"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484826147,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28575654,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575654',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6903037500',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28575654,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575654',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14430486',
        'method' => 'get',
      ),
    ),
  ),
  7729768241 => 
  array (
    'id' => 14430554,
    'name' => 'ООО АТИ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484826301,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28575732,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575732',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729768241',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28575732,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575732',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14430554',
        'method' => 'get',
      ),
    ),
  ),
  7743139284 => 
  array (
    'id' => 14430634,
    'name' => 'ООО «ЭНЭЙЧПИ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484826723,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28575826,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575826',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743139284',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28575826,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28575826',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14430634',
        'method' => 'get',
      ),
    ),
  ),
  4401092983 => 
  array (
    'id' => 14430848,
    'name' => 'ООО технические системы безопасности',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484827759,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28576078,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576078',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4401092983',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28576078,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576078',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14430848',
        'method' => 'get',
      ),
    ),
  ),
  7751520197 => 
  array (
    'id' => 14431208,
    'name' => 'ООО «ЮНИПАКИНГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484829184,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28576494,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576494',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751520197',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28576494,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576494',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14431208',
        'method' => 'get',
      ),
    ),
  ),
  7804122474 => 
  array (
    'id' => 14431248,
    'name' => 'ООО "Торгово-Промышленное Общество"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484829389,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28576554,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576554',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804122474',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28576554,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576554',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14431248',
        'method' => 'get',
      ),
    ),
  ),
  7706687557 => 
  array (
    'id' => 14431308,
    'name' => 'ООО "Цетус"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484829750,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28576628,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576628',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706687557',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28576628,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576628',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14431308',
        'method' => 'get',
      ),
    ),
  ),
  7723425632 => 
  array (
    'id' => 14431396,
    'name' => 'ООО СМГ ИМПОРТ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484830202,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28576762,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576762',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723425632',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28576762,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576762',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14431396',
        'method' => 'get',
      ),
    ),
  ),
  7718708730 => 
  array (
    'id' => 14431456,
    'name' => 'ООО ЭкоСити',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484830407,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28576822,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576822',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718708730',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28576822,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576822',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14431456',
        'method' => 'get',
      ),
    ),
  ),
  7707332042 => 
  array (
    'id' => 14431506,
    'name' => 'ООО НПО  ПЛАЗМА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484830636,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28576882,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576882',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707332042',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28576882,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28576882',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14431506',
        'method' => 'get',
      ),
    ),
  ),
  3305794106 => 
  array (
    'id' => 14431612,
    'name' => 'ООО "Новость"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484831082,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28577010,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28577010',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3305794106',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28577010,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28577010',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14431612',
        'method' => 'get',
      ),
    ),
  ),
  3459067291 => 
  array (
    'id' => 14432180,
    'name' => 'ООО "Безопасность труда"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484831267,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28578106,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28578106',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3459067291',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28578106,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28578106',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14432180',
        'method' => 'get',
      ),
    ),
  ),
  274154193 => 
  array (
    'id' => 14435730,
    'name' => 'ООО  "УФИМСКИЙ ЦЕНТР ПИТАНИЯ АРТ-ЛАЙФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484888213,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582124,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582124',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274154193',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582124,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582124',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435730',
        'method' => 'get',
      ),
    ),
  ),
  277135865 => 
  array (
    'id' => 14435762,
    'name' => 'ООО  "Уфа-Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484888566,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582150,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582150',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '277135865',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582150,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582150',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435762',
        'method' => 'get',
      ),
    ),
  ),
  2221204165 => 
  array (
    'id' => 14435772,
    'name' => 'АО "АЛТАЙИНДОРПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484888687,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582158,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582158',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221204165',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582158,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582158',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435772',
        'method' => 'get',
      ),
    ),
  ),
  2225159065 => 
  array (
    'id' => 14435810,
    'name' => 'ООО "Автобренд"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484888917,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582194,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582194',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225159065',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582194,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582194',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435810',
        'method' => 'get',
      ),
    ),
  ),
  2460087685 => 
  array (
    'id' => 14435832,
    'name' => 'ООО "СТРОЙСИТИ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484889084,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582216,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582216',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460087685',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582216,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582216',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435832',
        'method' => 'get',
      ),
    ),
  ),
  2463208151 => 
  array (
    'id' => 14435840,
    'name' => 'ООО «Альтерра»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484889135,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582222,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582222',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463208151',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582222,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582222',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435840',
        'method' => 'get',
      ),
    ),
  ),
  3808216574 => 
  array (
    'id' => 14435866,
    'name' => 'ООО "СИБЭНЕРГОСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484889444,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582254,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582254',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808216574',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582254,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582254',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435866',
        'method' => 'get',
      ),
    ),
  ),
  3811043603 => 
  array (
    'id' => 14435874,
    'name' => 'ООО ЧОП "АТЭКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484889549,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582256,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582256',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811043603',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582256,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582256',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435874',
        'method' => 'get',
      ),
    ),
  ),
  5405339425 => 
  array (
    'id' => 14435908,
    'name' => 'ООО «Группа Стандарт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484889944,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582282,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582282',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405339425',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582282,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582282',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435908',
        'method' => 'get',
      ),
    ),
  ),
  5407488302 => 
  array (
    'id' => 14435960,
    'name' => 'ООО "ТЕКСТИЛЬ-РЕГИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484890630,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582330,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582330',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407488302',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582330,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582330',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435960',
        'method' => 'get',
      ),
    ),
  ),
  5503157991 => 
  array (
    'id' => 14435970,
    'name' => 'ООО "Сатурн"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484890855,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582342,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582342',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503157991',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582342,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582342',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435970',
        'method' => 'get',
      ),
    ),
  ),
  5505042428 => 
  array (
    'id' => 14435982,
    'name' => 'ООО "МК "АНТЕН-СЕРВИСЕ-Омск"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484891069,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582356,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582356',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5505042428',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582356,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582356',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435982',
        'method' => 'get',
      ),
    ),
  ),
  5505052345 => 
  array (
    'id' => 14435990,
    'name' => 'ООО "АктивСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484891166,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582362,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582362',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5505052345',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582362,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582362',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14435990',
        'method' => 'get',
      ),
    ),
  ),
  5610132462 => 
  array (
    'id' => 14436102,
    'name' => 'ООО Центр по ремонту и обслуживанию автомобилей  Хайтек',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484892043,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582480,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582480',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5610132462',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582480,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582480',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14436102',
        'method' => 'get',
      ),
    ),
  ),
  6319189930 => 
  array (
    'id' => 14436404,
    'name' => 'ООО "ВЕЛЕС-АВТО"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484894976,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28582934,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582934',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319189930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28582934,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28582934',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14436404',
        'method' => 'get',
      ),
    ),
  ),
  6633023461 => 
  array (
    'id' => 14436920,
    'name' => 'ООО  ООО «Апрель-21»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484896765,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28583510,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28583510',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6633023461',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28583510,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28583510',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14436920',
        'method' => 'get',
      ),
    ),
  ),
  7202167213 => 
  array (
    'id' => 14437408,
    'name' => 'ООО «Виадук»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484898265,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28584116,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584116',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7202167213',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28584116,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584116',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14437408',
        'method' => 'get',
      ),
    ),
  ),
  7701406072 => 
  array (
    'id' => 14437430,
    'name' => 'ООО «Агенство Финансовых Сервисов»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484898535,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28584150,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584150',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701406072',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28584150,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584150',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14437430',
        'method' => 'get',
      ),
    ),
  ),
  7801297437 => 
  array (
    'id' => 14437468,
    'name' => 'ООО "ТЕХНОМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484898809,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28584210,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584210',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801297437',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28584210,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584210',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14437468',
        'method' => 'get',
      ),
    ),
  ),
  8601040355 => 
  array (
    'id' => 14437922,
    'name' => 'ООО Югорская территориальная экологическая транспортная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484900896,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28584770,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584770',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8601040355',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28584770,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28584770',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14437922',
        'method' => 'get',
      ),
    ),
  ),
  8602268112 => 
  array (
    'id' => 14438316,
    'name' => 'ООО "СТРОИТЕЛЬНО-ТРАНСПОРТНАЯ КОМПАНИЯ-1"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484902961,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28585282,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585282',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602268112',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28585282,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585282',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14438316',
        'method' => 'get',
      ),
    ),
  ),
  9721012739 => 
  array (
    'id' => 14438416,
    'name' => 'ООО "ЛАЙФТАЙМГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484903475,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28585406,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585406',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9721012739',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28585406,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585406',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14438416',
        'method' => 'get',
      ),
    ),
  ),
  7842078610 => 
  array (
    'id' => 14438450,
    'name' => 'ООО "КЛАРК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484903593,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28585440,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585440',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842078610',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28585440,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585440',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14438450',
        'method' => 'get',
      ),
    ),
  ),
  7839504097 => 
  array (
    'id' => 14438738,
    'name' => 'ООО СКС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484904656,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28585720,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585720',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839504097',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28585720,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585720',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14438738',
        'method' => 'get',
      ),
    ),
  ),
  7838462239 => 
  array (
    'id' => 14438760,
    'name' => 'ООО  "ПОЖГАРАНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484904809,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28585756,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585756',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838462239',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28585756,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28585756',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14438760',
        'method' => 'get',
      ),
    ),
  ),
  7811193691 => 
  array (
    'id' => 14439044,
    'name' => 'ООО  "НЕВСКАЯ МЕДИЦИНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484906235,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28586088,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586088',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811193691',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28586088,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586088',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439044',
        'method' => 'get',
      ),
    ),
  ),
  7801150177 => 
  array (
    'id' => 14439464,
    'name' => 'ООО "ПРОСТОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484908078,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28586588,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586588',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801150177',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28586588,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586588',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439464',
        'method' => 'get',
      ),
    ),
  ),
  7735595149 => 
  array (
    'id' => 14439522,
    'name' => 'ООО  "АВАНГАРД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484908323,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28586664,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586664',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735595149',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28586664,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586664',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439522',
        'method' => 'get',
      ),
    ),
  ),
  7729492321 => 
  array (
    'id' => 14439584,
    'name' => 'ООО "ХОЛИДЭЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484908641,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28586746,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586746',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729492321',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28586746,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586746',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439584',
        'method' => 'get',
      ),
    ),
  ),
  7727287038 => 
  array (
    'id' => 14439688,
    'name' => 'ООО "Русогнеза"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484909074,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28586864,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586864',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727287038',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28586864,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586864',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439688',
        'method' => 'get',
      ),
    ),
  ),
  7727139953 => 
  array (
    'id' => 14439712,
    'name' => 'ООО «СервисЛогистика»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484909176,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28586882,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586882',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727139953',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28586882,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586882',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439712',
        'method' => 'get',
      ),
    ),
  ),
  7721836588 => 
  array (
    'id' => 14439812,
    'name' => 'ООО "Клининг Сервис Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484909664,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587004,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587004',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721836588',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587004,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587004',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439812',
        'method' => 'get',
      ),
    ),
  ),
  7718719926 => 
  array (
    'id' => 14440032,
    'name' => 'ООО "Тайм Лайн"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484910505,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587246,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587246',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718719926',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587246,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587246',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440032',
        'method' => 'get',
      ),
    ),
  ),
  7717782763 => 
  array (
    'id' => 14440272,
    'name' => 'ООО "ЦЕНТРОСПАС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484911905,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587556,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587556',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717782763',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587556,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587556',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440272',
        'method' => 'get',
      ),
    ),
  ),
  5017054860 => 
  array (
    'id' => 14440338,
    'name' => 'ООО "РЕГИОН-МЕДИА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484912370,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587658,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587658',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5017054860',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587658,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587658',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440338',
        'method' => 'get',
      ),
    ),
  ),
  5042126660 => 
  array (
    'id' => 14440426,
    'name' => 'ООО "НОРЭНС Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484912938,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587758,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587758',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5042126660',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587758,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587758',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440426',
        'method' => 'get',
      ),
    ),
  ),
  7718549520 => 
  array (
    'id' => 14440486,
    'name' => 'ООО Философия красоты',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484913297,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587824,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587824',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718549520',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587824,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587824',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440486',
        'method' => 'get',
      ),
    ),
  ),
  7806204059 => 
  array (
    'id' => 14440518,
    'name' => 'ООО "Спецмебель"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484913479,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587868,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587868',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806204059',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587868,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587868',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440518',
        'method' => 'get',
      ),
    ),
  ),
  7814303173 => 
  array (
    'id' => 14440544,
    'name' => 'ООО «РМК»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484913591,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587896,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587896',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814303173',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587896,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587896',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440544',
        'method' => 'get',
      ),
    ),
  ),
  7810344309 => 
  array (
    'id' => 14440608,
    'name' => 'ООО "Атлант плюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484913873,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28587962,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587962',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810344309',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28587962,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28587962',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440608',
        'method' => 'get',
      ),
    ),
  ),
  7722783804 => 
  array (
    'id' => 14440638,
    'name' => '"ООО ""ДЦК АВТО ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484914134,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28588010,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588010',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722783804',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28588010,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588010',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440638',
        'method' => 'get',
      ),
    ),
  ),
  7716748463 => 
  array (
    'id' => 14440680,
    'name' => 'ООО "РусТендеры',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484914357,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28588042,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588042',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716748463',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28588042,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588042',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440680',
        'method' => 'get',
      ),
    ),
  ),
  7801602352 => 
  array (
    'id' => 14440740,
    'name' => 'ООО "Комбат Гарант"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484914641,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28588110,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588110',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801602352',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28588110,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588110',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440740',
        'method' => 'get',
      ),
    ),
  ),
  7726386830 => 
  array (
    'id' => 14440810,
    'name' => 'ООО "ФАРМЛИДЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484914924,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28588190,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588190',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726386830',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28588190,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28588190',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14440810',
        'method' => 'get',
      ),
    ),
  ),
  2721191605 => 
  array (
    'id' => 14452890,
    'name' => 'ООО «КОМПЛЕКСНЫЙ СЕРВИС»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485148040,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28612964,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28612964',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721191605',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28612964,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28612964',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452890',
        'method' => 'get',
      ),
    ),
  ),
  3328428674 => 
  array (
    'id' => 14452898,
    'name' => 'ООО "СПЕКТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485148169,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28612976,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28612976',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3328428674',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28612976,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28612976',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452898',
        'method' => 'get',
      ),
    ),
  ),
  4205294888 => 
  array (
    'id' => 14452914,
    'name' => 'ООО ЧОО "Компания "Гарант"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485148353,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613000,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613000',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205294888',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613000,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613000',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452914',
        'method' => 'get',
      ),
    ),
  ),
  4211021656 => 
  array (
    'id' => 14452936,
    'name' => 'ООО "Массмедиа"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485148484,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613028,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613028',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4211021656',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613028,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613028',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452936',
        'method' => 'get',
      ),
    ),
  ),
  4230011967 => 
  array (
    'id' => 14452944,
    'name' => 'ООО ПСК «Ремстрой-Индустрия»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485148591,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613046,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613046',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4230011967',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613046,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613046',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452944',
        'method' => 'get',
      ),
    ),
  ),
  4501211553 => 
  array (
    'id' => 14452952,
    'name' => 'ООО "АКВАМАРИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485148644,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613056,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613056',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4501211553',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613056,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613056',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452952',
        'method' => 'get',
      ),
    ),
  ),
  5902039855 => 
  array (
    'id' => 14452980,
    'name' => 'ООО "Пробинк"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485148909,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613084,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613084',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902039855',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613084,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613084',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452980',
        'method' => 'get',
      ),
    ),
  ),
  6324062294 => 
  array (
    'id' => 14452994,
    'name' => 'ООО  "Фуд-Союз"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485149047,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613098,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613098',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6324062294',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613098,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613098',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14452994',
        'method' => 'get',
      ),
    ),
  ),
  277907582 => 
  array (
    'id' => 14453034,
    'name' => 'ООО Чоо "Беркут"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485149520,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613150,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613150',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '277907582',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613150,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613150',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453034',
        'method' => 'get',
      ),
    ),
  ),
  9705053223 => 
  array (
    'id' => 14453082,
    'name' => 'ООО "МИС-ЭКСПЕРТИЗА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485150050,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613216,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613216',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705053223',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613216,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613216',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453082',
        'method' => 'get',
      ),
    ),
  ),
  7840508297 => 
  array (
    'id' => 14453094,
    'name' => 'ООО «Единый центр коммунальных услуг»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485150244,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613234,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613234',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840508297',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613234,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613234',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453094',
        'method' => 'get',
      ),
    ),
  ),
  7839408724 => 
  array (
    'id' => 14453150,
    'name' => 'ООО "АРТ-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485150824,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613284,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613284',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839408724',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613284,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613284',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453150',
        'method' => 'get',
      ),
    ),
  ),
  7805679740 => 
  array (
    'id' => 14453270,
    'name' => 'ООО Медианта',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485151595,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613400,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613400',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382026,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805679740',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613400,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613400',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453270',
        'method' => 'get',
      ),
    ),
  ),
  7802458052 => 
  array (
    'id' => 14453426,
    'name' => 'ООО "СМУ №77"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485152862,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613594,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613594',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802458052',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613594,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613594',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453426',
        'method' => 'get',
      ),
    ),
  ),
  7801564097 => 
  array (
    'id' => 14453442,
    'name' => 'ООО "ГРАНД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485152966,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613610,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613610',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801564097',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613610,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613610',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453442',
        'method' => 'get',
      ),
    ),
  ),
  7734692020 => 
  array (
    'id' => 14453516,
    'name' => 'ООО "ИНТЕЛЛЕКТУАЛЬНЫЕ СОЦИАЛЬНЫЕ СИСТЕМЫ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485153513,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613684,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613684',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734692020',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613684,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613684',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453516',
        'method' => 'get',
      ),
    ),
  ),
  7734381987 => 
  array (
    'id' => 14453568,
    'name' => 'ООО "ИНВЕСТЦЕНТРСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485153793,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28613752,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613752',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734381987',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28613752,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28613752',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453568',
        'method' => 'get',
      ),
    ),
  ),
  7814299304 => 
  array (
    'id' => 14453874,
    'name' => 'ООО "СТРОЙИНВЕСТМО"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485155446,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28614128,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614128',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814299304',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28614128,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614128',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453874',
        'method' => 'get',
      ),
    ),
  ),
  7725309832 => 
  array (
    'id' => 14453972,
    'name' => 'ООО "ММК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485156008,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28614264,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614264',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725309832',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28614264,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614264',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14453972',
        'method' => 'get',
      ),
    ),
  ),
  7721739792 => 
  array (
    'id' => 14454246,
    'name' => 'ООО Добродар-Р',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485156988,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28614574,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614574',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721739792',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28614574,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614574',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14454246',
        'method' => 'get',
      ),
    ),
  ),
  7719460095 => 
  array (
    'id' => 14454282,
    'name' => 'ООО "Квартализ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485157200,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28614614,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614614',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719460095',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28614614,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614614',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14454282',
        'method' => 'get',
      ),
    ),
  ),
  7718618999 => 
  array (
    'id' => 14454416,
    'name' => 'ООО «Рельеф»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485157754,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28614762,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614762',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718618999',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28614762,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614762',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14454416',
        'method' => 'get',
      ),
    ),
  ),
  7717286300 => 
  array (
    'id' => 14454544,
    'name' => 'ООО  "ТС КЛИНИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485158228,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28614910,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614910',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717286300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28614910,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28614910',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14454544',
        'method' => 'get',
      ),
    ),
  ),
  7717102224 => 
  array (
    'id' => 14454648,
    'name' => 'ООО "ММЦП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485158716,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28615056,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615056',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717102224',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28615056,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615056',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14454648',
        'method' => 'get',
      ),
    ),
  ),
  7716742359 => 
  array (
    'id' => 14454994,
    'name' => 'ООО "ЭКО-КЛИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485160093,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28615548,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615548',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716742359',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28615548,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615548',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14454994',
        'method' => 'get',
      ),
    ),
  ),
  7715615950 => 
  array (
    'id' => 14455024,
    'name' => 'ООО "НАШ-СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485160245,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28615602,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615602',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715615950',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28615602,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615602',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455024',
        'method' => 'get',
      ),
    ),
  ),
  7715432160 => 
  array (
    'id' => 14455036,
    'name' => 'ООО "Сантекс-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485160288,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28615616,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615616',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715432160',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28615616,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615616',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455036',
        'method' => 'get',
      ),
    ),
  ),
  7714879559 => 
  array (
    'id' => 14455058,
    'name' => 'ООО «КРоСЭл»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485160390,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28615648,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615648',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714879559',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28615648,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28615648',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455058',
        'method' => 'get',
      ),
    ),
  ),
  7714245200 => 
  array (
    'id' => 14455594,
    'name' => 'ООО "Аварийная служба МР "Беговой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485162549,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616290,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616290',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714245200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616290,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616290',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455594',
        'method' => 'get',
      ),
    ),
  ),
  7707805820 => 
  array (
    'id' => 14455654,
    'name' => 'ООО "ГОРИЗОНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485162859,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616378,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616378',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707805820',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616378,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616378',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455654',
        'method' => 'get',
      ),
    ),
  ),
  7705668390 => 
  array (
    'id' => 14455682,
    'name' => 'ООО ООП "ЗАЩИТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485162984,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616418,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616418',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705668390',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616418,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616418',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455682',
        'method' => 'get',
      ),
    ),
  ),
  7705552500 => 
  array (
    'id' => 14455716,
    'name' => 'ООО «Экспертиза. Аналитика. Сопровождение»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485163143,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616454,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616454',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705552500',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616454,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616454',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455716',
        'method' => 'get',
      ),
    ),
  ),
  7704740822 => 
  array (
    'id' => 14455760,
    'name' => 'ООО «Клининг Инжиниринг»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485163418,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616526,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616526',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704740822',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616526,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616526',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455760',
        'method' => 'get',
      ),
    ),
  ),
  7704615010 => 
  array (
    'id' => 14455776,
    'name' => 'ООО "АВЕРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485163548,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616556,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616556',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704615010',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616556,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616556',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455776',
        'method' => 'get',
      ),
    ),
  ),
  7702801660 => 
  array (
    'id' => 14455812,
    'name' => 'ООО "Клин Дьюти"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485163693,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616606,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616606',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702801660',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616606,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616606',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455812',
        'method' => 'get',
      ),
    ),
  ),
  7701753573 => 
  array (
    'id' => 14455950,
    'name' => 'ЗАО "ТВ-ТЕЛЕКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485164254,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616740,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616740',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701753573',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616740,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616740',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14455950',
        'method' => 'get',
      ),
    ),
  ),
  7701414027 => 
  array (
    'id' => 14456004,
    'name' => 'ООО ТРАНС-ЛЮКС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485164534,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616812,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616812',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701414027',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616812,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616812',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456004',
        'method' => 'get',
      ),
    ),
  ),
  6673210179 => 
  array (
    'id' => 14456060,
    'name' => 'ООО ЭнергоКомплекс',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485164848,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28616904,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616904',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6673210179',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28616904,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28616904',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456060',
        'method' => 'get',
      ),
    ),
  ),
  6168073585 => 
  array (
    'id' => 14456180,
    'name' => 'ООО "СТРОЙАВТОДОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485165672,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617158,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617158',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6168073585',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617158,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617158',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456180',
        'method' => 'get',
      ),
    ),
  ),
  6168057738 => 
  array (
    'id' => 14456194,
    'name' => 'ООО Радуга',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485165753,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617176,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617176',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6168057738',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617176,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617176',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456194',
        'method' => 'get',
      ),
    ),
  ),
  6165191679 => 
  array (
    'id' => 14456258,
    'name' => 'ООО ЮГ-ТРАНС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485166099,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617266,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617266',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165191679',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617266,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617266',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456258',
        'method' => 'get',
      ),
    ),
  ),
  6164314913 => 
  array (
    'id' => 14456272,
    'name' => 'ООО "Перспектива"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485166156,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617286,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617286',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6164314913',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617286,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617286',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456272',
        'method' => 'get',
      ),
    ),
  ),
  6155060980 => 
  array (
    'id' => 14456368,
    'name' => 'ООО Дриада',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485166454,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617376,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617376',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6155060980',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617376,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617376',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456368',
        'method' => 'get',
      ),
    ),
  ),
  1104012958 => 
  array (
    'id' => 14456750,
    'name' => 'ООО Север Строй Инвест',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485167928,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617810,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617810',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1104012958',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617810,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617810',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456750',
        'method' => 'get',
      ),
    ),
  ),
  1650262439 => 
  array (
    'id' => 14456774,
    'name' => 'ООО "Камстройпроект""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485168019,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617844,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617844',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1650262439',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617844,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617844',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456774',
        'method' => 'get',
      ),
    ),
  ),
  2124039741 => 
  array (
    'id' => 14456810,
    'name' => 'ООО Автопрестиж',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485168215,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28617888,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617888',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2124039741',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28617888,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28617888',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14456810',
        'method' => 'get',
      ),
    ),
  ),
  4632046078 => 
  array (
    'id' => 14457106,
    'name' => 'ООО  «Частная охранная организация»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485169395,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28618212,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618212',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632046078',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28618212,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618212',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14457106',
        'method' => 'get',
      ),
    ),
  ),
  4825101310 => 
  array (
    'id' => 14457122,
    'name' => 'ООО МостДорИнжиниринг',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485169445,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28618232,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618232',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825101310',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28618232,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618232',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14457122',
        'method' => 'get',
      ),
    ),
  ),
  5024099245 => 
  array (
    'id' => 14457156,
    'name' => 'ООО "ЭЙДЖИСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485169581,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28618272,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618272',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024099245',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28618272,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618272',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14457156',
        'method' => 'get',
      ),
    ),
  ),
  6027147883 => 
  array (
    'id' => 14457192,
    'name' => 'ООО "КОМСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485169731,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28618316,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618316',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6027147883',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28618316,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618316',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14457192',
        'method' => 'get',
      ),
    ),
  ),
  6161076435 => 
  array (
    'id' => 14457240,
    'name' => 'ООО Фармтехнологии',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485169895,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28618384,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618384',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6161076435',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28618384,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618384',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14457240',
        'method' => 'get',
      ),
    ),
  ),
  7704841193 => 
  array (
    'id' => 14457366,
    'name' => 'ООО "СервисГрупп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485170418,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28618552,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618552',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704841193',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28618552,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28618552',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14457366',
        'method' => 'get',
      ),
    ),
  ),
  266034150 => 
  array (
    'id' => 14463238,
    'name' => 'ООО "ТехСигнал"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485234660,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640180,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640180',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '266034150',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640180,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640180',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463238',
        'method' => 'get',
      ),
    ),
  ),
  268054507 => 
  array (
    'id' => 14463248,
    'name' => 'ООО "АКМЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485234759,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640188,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640188',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '268054507',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640188,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640188',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463248',
        'method' => 'get',
      ),
    ),
  ),
  274915828 => 
  array (
    'id' => 14463268,
    'name' => 'ООО «ВИННЕР»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485235002,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640206,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640206',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274915828',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640206,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640206',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463268',
        'method' => 'get',
      ),
    ),
  ),
  404008703 => 
  array (
    'id' => 14463398,
    'name' => 'ООО "АПЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485235236,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640326,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640326',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '404008703',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640326,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640326',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463398',
        'method' => 'get',
      ),
    ),
  ),
  5638040123 => 
  array (
    'id' => 14463468,
    'name' => 'АО «Санаторий «Дубовая роща»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485235973,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640380,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640380',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5638040123',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640380,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640380',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463468',
        'method' => 'get',
      ),
    ),
  ),
  5907998728 => 
  array (
    'id' => 14463512,
    'name' => 'ООО КОМСТРОЙСЕРВИС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485236352,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640426,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640426',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5907998728',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640426,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640426',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463512',
        'method' => 'get',
      ),
    ),
  ),
  6317105437 => 
  array (
    'id' => 14463612,
    'name' => 'ООО ПРОЕКТНОЕ БЮРО "ИНЖЕНЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485236956,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640516,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640516',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317105437',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640516,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640516',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463612',
        'method' => 'get',
      ),
    ),
  ),
  6671454973 => 
  array (
    'id' => 14463640,
    'name' => 'ООО "ИНДИ96"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485237232,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640546,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640546',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671454973',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640546,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640546',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463640',
        'method' => 'get',
      ),
    ),
  ),
  7724755714 => 
  array (
    'id' => 14463684,
    'name' => 'ООО "Межотраслевая технико-энергетическая компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485237663,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640592,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640592',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724755714',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640592,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640592',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463684',
        'method' => 'get',
      ),
    ),
  ),
  7811471772 => 
  array (
    'id' => 14463712,
    'name' => 'ЗАО "КАМАДИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485237926,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640624,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640624',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811471772',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640624,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640624',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463712',
        'method' => 'get',
      ),
    ),
  ),
  9715222657 => 
  array (
    'id' => 14463810,
    'name' => 'ООО СЕРВИС ГРУПП',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485238695,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640724,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640724',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715222657',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640724,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640724',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463810',
        'method' => 'get',
      ),
    ),
  ),
  9705045737 => 
  array (
    'id' => 14463836,
    'name' => 'ООО «Триумф»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485238974,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640756,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640756',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705045737',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640756,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640756',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463836',
        'method' => 'get',
      ),
    ),
  ),
  7841452689 => 
  array (
    'id' => 14463988,
    'name' => 'ООО Охранное предприятие "Комбат Северо-Запад"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485240166,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640936,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640936',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841452689',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640936,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640936',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14463988',
        'method' => 'get',
      ),
    ),
  ),
  7841373772 => 
  array (
    'id' => 14464026,
    'name' => 'ООО "ЧИСТОТА СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485240381,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28640974,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640974',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841373772',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28640974,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28640974',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14464026',
        'method' => 'get',
      ),
    ),
  ),
  7816250671 => 
  array (
    'id' => 14464078,
    'name' => 'ООО "Сапфир"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485240748,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28641052,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28641052',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816250671',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28641052,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28641052',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14464078',
        'method' => 'get',
      ),
    ),
  ),
  7814624233 => 
  array (
    'id' => 14464102,
    'name' => 'ООО "МЕДЕС-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485240878,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28641094,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28641094',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814624233',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28641094,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28641094',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14464102',
        'method' => 'get',
      ),
    ),
  ),
  7814389406 => 
  array (
    'id' => 14465392,
    'name' => 'ООО "ОХРАННОЕ ПРЕДПРИЯТИЕ "ЛИНОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485246736,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28642950,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28642950',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814389406',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28642950,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28642950',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14465392',
        'method' => 'get',
      ),
    ),
  ),
  7814221080 => 
  array (
    'id' => 14465474,
    'name' => 'ООО «МНОЗК»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485247062,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28643068,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643068',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814221080',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28643068,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643068',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14465474',
        'method' => 'get',
      ),
    ),
  ),
  7806539129 => 
  array (
    'id' => 14465618,
    'name' => 'ООО ПРОГРЕСС БЕЗОПАСНОСТЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485247592,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28643248,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643248',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806539129',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28643248,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643248',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14465618',
        'method' => 'get',
      ),
    ),
  ),
  7802843093 => 
  array (
    'id' => 14465754,
    'name' => 'ООО "МЕДТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485248101,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28643434,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643434',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802843093',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28643434,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643434',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14465754',
        'method' => 'get',
      ),
    ),
  ),
  7734372333 => 
  array (
    'id' => 14466022,
    'name' => 'ООО "ФИРМА КСТ-5"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485249251,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28643882,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643882',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734372333',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28643882,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28643882',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14466022',
        'method' => 'get',
      ),
    ),
  ),
  7731296814 => 
  array (
    'id' => 14466196,
    'name' => 'ООО «ФЕРМА ИНК»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485250013,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28644100,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644100',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731296814',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28644100,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644100',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14466196',
        'method' => 'get',
      ),
    ),
  ),
  7727300659 => 
  array (
    'id' => 14466284,
    'name' => 'ООО  "ПРОФЛЭД ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485250324,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28644164,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644164',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727300659',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28644164,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644164',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14466284',
        'method' => 'get',
      ),
    ),
  ),
  7726293455 => 
  array (
    'id' => 14466870,
    'name' => 'ООО "ТехноМедЛаб"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485251922,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28644836,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644836',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726293455',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28644836,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644836',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14466870',
        'method' => 'get',
      ),
    ),
  ),
  7725628268 => 
  array (
    'id' => 14466908,
    'name' => 'ООО "ЭК "Городские усадьбы"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485252035,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28644864,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644864',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725628268',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28644864,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28644864',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14466908',
        'method' => 'get',
      ),
    ),
  ),
  7724769192 => 
  array (
    'id' => 14467118,
    'name' => 'ООО БЛАГОВЕСТ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485252786,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28645102,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28645102',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724769192',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28645102,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28645102',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14467118',
        'method' => 'get',
      ),
    ),
  ),
  7717774508 => 
  array (
    'id' => 14467378,
    'name' => 'ООО "СпецСтройЭксплуатация"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485254008,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28645448,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28645448',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717774508',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28645448,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28645448',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14467378',
        'method' => 'get',
      ),
    ),
  ),
  4825089367 => 
  array (
    'id' => 14468404,
    'name' => 'ООО ТРЕСТ ИНЖЕНЕРНО-СТРОИТЕЛЬНЫХ ИЗЫСКАНИЙ ЛИПЕЦК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485257882,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28652164,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652164',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825089367',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28652164,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652164',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14468404',
        'method' => 'get',
      ),
    ),
  ),
  7806227063 => 
  array (
    'id' => 14468660,
    'name' => 'ООО  «Авиа-брифинг»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485258639,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28652428,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652428',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806227063',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28652428,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652428',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14468660',
        'method' => 'get',
      ),
    ),
  ),
  7842006119 => 
  array (
    'id' => 14468710,
    'name' => 'ООО "РРС "Звезда""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485258837,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28652492,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652492',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842006119',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28652492,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652492',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14468710',
        'method' => 'get',
      ),
    ),
  ),
  7736248691 => 
  array (
    'id' => 14468776,
    'name' => 'ООО Лидер',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485259107,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28652576,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652576',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736248691',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28652576,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28652576',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14468776',
        'method' => 'get',
      ),
    ),
  ),
  7709488856 => 
  array (
    'id' => 14469102,
    'name' => 'ООО "КАЧЕСТВО-ГАРАНТИЯ БЕЗОПАСНОСТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485260524,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28653024,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28653024',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709488856',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28653024,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28653024',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14469102',
        'method' => 'get',
      ),
    ),
  ),
  6658481664 => 
  array (
    'id' => 14469344,
    'name' => 'ООО "ГК "ДИАЛОГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485261647,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28653334,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28653334',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658481664',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28653334,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28653334',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14469344',
        'method' => 'get',
      ),
    ),
  ),
  8622023791 => 
  array (
    'id' => 14475818,
    'name' => 'ООО "РекламГрупп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485322049,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701146,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701146',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8622023791',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701146,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701146',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14475818',
        'method' => 'get',
      ),
    ),
  ),
  8601051830 => 
  array (
    'id' => 14475840,
    'name' => 'ООО Рекламное агентство "Алиса"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485322277,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701174,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701174',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8601051830',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701174,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701174',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14475840',
        'method' => 'get',
      ),
    ),
  ),
  7810397043 => 
  array (
    'id' => 14475868,
    'name' => 'ООО "Элтеко"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485322472,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701190,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701190',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810397043',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701190,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701190',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14475868',
        'method' => 'get',
      ),
    ),
  ),
  7448181761 => 
  array (
    'id' => 14475876,
    'name' => 'ООО "АНЛАМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485322694,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701202,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701202',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448181761',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701202,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701202',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14475876',
        'method' => 'get',
      ),
    ),
  ),
  7017379284 => 
  array (
    'id' => 14475904,
    'name' => 'ООО "Оптима"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485323053,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701240,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701240',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017379284',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701240,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701240',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14475904',
        'method' => 'get',
      ),
    ),
  ),
  6672341531 => 
  array (
    'id' => 14475914,
    'name' => 'ООО "Корпорация САНВУД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485323235,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701258,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701258',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672341531',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701258,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701258',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14475914',
        'method' => 'get',
      ),
    ),
  ),
  6324048645 => 
  array (
    'id' => 14475982,
    'name' => 'ООО "Компания "Невский"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485323890,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701326,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701326',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6324048645',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701326,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701326',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14475982',
        'method' => 'get',
      ),
    ),
  ),
  6323088211 => 
  array (
    'id' => 14476008,
    'name' => 'ООО "КАРИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485324144,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701366,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701366',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6323088211',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701366,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701366',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476008',
        'method' => 'get',
      ),
    ),
  ),
  6312124450 => 
  array (
    'id' => 14476018,
    'name' => 'ООО "ОМТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485324238,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701380,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701380',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312124450',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701380,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701380',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476018',
        'method' => 'get',
      ),
    ),
  ),
  5904250042 => 
  array (
    'id' => 14476072,
    'name' => 'ООО Электротехническая компания "ЭТА ТОК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485324657,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701440,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701440',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904250042',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701440,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701440',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476072',
        'method' => 'get',
      ),
    ),
  ),
  5610085300 => 
  array (
    'id' => 14476098,
    'name' => 'ООО "ДОРМАСТЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485324838,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701468,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701468',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1501479489,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5610085300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701468,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701468',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476098',
        'method' => 'get',
      ),
    ),
  ),
  5503166121 => 
  array (
    'id' => 14476118,
    'name' => 'ООО "ПРОФ-ЭНЕРДЖИ ГРУП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485325037,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701484,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701484',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503166121',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701484,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701484',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476118',
        'method' => 'get',
      ),
    ),
  ),
  7724387080 => 
  array (
    'id' => 14476240,
    'name' => 'ООО "МК МЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485325748,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701646,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701646',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724387080',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701646,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701646',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476240',
        'method' => 'get',
      ),
    ),
  ),
  5406694278 => 
  array (
    'id' => 14476406,
    'name' => 'ООО «Энергосистемы»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485326085,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701804,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701804',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406694278',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701804,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701804',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476406',
        'method' => 'get',
      ),
    ),
  ),
  278918629 => 
  array (
    'id' => 14476434,
    'name' => 'ООО "МЕДФИНАНС ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485326390,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701856,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701856',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278918629',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701856,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701856',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476434',
        'method' => 'get',
      ),
    ),
  ),
  7826046844 => 
  array (
    'id' => 14476538,
    'name' => 'ООО «Аракс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485327215,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28701984,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701984',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7826046844',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28701984,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28701984',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476538',
        'method' => 'get',
      ),
    ),
  ),
  4703145984 => 
  array (
    'id' => 14476770,
    'name' => 'ООО "Торговый Дом "Ладога СПб"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1485328571,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702256,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702256',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4703145984',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702256,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702256',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476770',
        'method' => 'get',
      ),
    ),
  ),
  7813490594 => 
  array (
    'id' => 14476796,
    'name' => 'ООО "АЛЬЯНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485328702,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702280,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702280',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813490594',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702280,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702280',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476796',
        'method' => 'get',
      ),
    ),
  ),
  7811570491 => 
  array (
    'id' => 14476830,
    'name' => 'ООО "ПАРУС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485328927,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702318,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702318',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811570491',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702318,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702318',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476830',
        'method' => 'get',
      ),
    ),
  ),
  7805657465 => 
  array (
    'id' => 14477076,
    'name' => 'ООО ЧОО "ТОПАЗ СЕВЕРО-ЗАПАД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485330374,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702624,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702624',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805657465',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702624,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702624',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477076',
        'method' => 'get',
      ),
    ),
  ),
  7805586158 => 
  array (
    'id' => 14477094,
    'name' => 'ООО Румб',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485330478,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702644,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702644',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805586158',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702644,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702644',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477094',
        'method' => 'get',
      ),
    ),
  ),
  7802583367 => 
  array (
    'id' => 14477118,
    'name' => 'ООО "Аврора-Н"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485330596,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702678,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702678',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802583367',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702678,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702678',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477118',
        'method' => 'get',
      ),
    ),
  ),
  7730669799 => 
  array (
    'id' => 14477306,
    'name' => 'ООО "ПромСнаб"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485331427,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702922,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702922',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730669799',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702922,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702922',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477306',
        'method' => 'get',
      ),
    ),
  ),
  7729463320 => 
  array (
    'id' => 14477330,
    'name' => 'ООО "ВТ-Инжиниринг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485331635,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702950,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702950',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729463320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702950,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702950',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477330',
        'method' => 'get',
      ),
    ),
  ),
  7725300607 => 
  array (
    'id' => 14477378,
    'name' => 'ООО СТРУКТУРА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485331887,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28703000,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703000',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725300607',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28703000,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703000',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477378',
        'method' => 'get',
      ),
    ),
  ),
  7723916415 => 
  array (
    'id' => 14477412,
    'name' => 'ООО "КОССПРОДЖЕКТ ГМБХ""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485332024,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28703046,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703046',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723916415',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28703046,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703046',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477412',
        'method' => 'get',
      ),
    ),
  ),
  7720531390 => 
  array (
    'id' => 14477462,
    'name' => 'ООО «АРНИ-К.А.»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485332164,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28703086,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703086',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720531390',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28703086,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703086',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477462',
        'method' => 'get',
      ),
    ),
  ),
  7718078207 => 
  array (
    'id' => 14477950,
    'name' => 'ООО "ТАЯР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485333315,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28703604,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703604',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718078207',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28703604,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28703604',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14477950',
        'method' => 'get',
      ),
    ),
  ),
  7715806970 => 
  array (
    'id' => 14478364,
    'name' => 'ООО "ГарантДезСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485335279,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704184,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704184',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715806970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704184,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704184',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478364',
        'method' => 'get',
      ),
    ),
  ),
  6312148966 => 
  array (
    'id' => 14478630,
    'name' => 'ООО «Технология чистоты»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485336459,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704528,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704528',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312148966',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704528,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704528',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478630',
        'method' => 'get',
      ),
    ),
  ),
  2204077767 => 
  array (
    'id' => 14478656,
    'name' => 'ООО "АРХИТЕКТУРНО-КОНСТРУКТОРСКАЯ ФИРМА АУРУМ-ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485336548,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704562,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704562',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2204077767',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704562,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704562',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478656',
        'method' => 'get',
      ),
    ),
  ),
  7707788484 => 
  array (
    'id' => 14478890,
    'name' => 'ООО "Техно-Л"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485337678,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704832,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704832',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707788484',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704832,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704832',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478890',
        'method' => 'get',
      ),
    ),
  ),
  7707038033 => 
  array (
    'id' => 14478940,
    'name' => 'ООО "Промтепло"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485337780,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704862,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704862',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707038033',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704862,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704862',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478940',
        'method' => 'get',
      ),
    ),
  ),
  7704831580 => 
  array (
    'id' => 14478958,
    'name' => 'ООО "Охрана окружающей среды"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485337858,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704874,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704874',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704831580',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704874,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704874',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478958',
        'method' => 'get',
      ),
    ),
  ),
  7704811103 => 
  array (
    'id' => 14478984,
    'name' => 'ООО "ЕДИНАЯ УПРАВЛЯЮЩАЯ КОМПАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485337947,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704904,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704904',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704811103',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704904,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704904',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478984',
        'method' => 'get',
      ),
    ),
  ),
  7703407138 => 
  array (
    'id' => 14479086,
    'name' => 'ООО "ЛОГИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485338410,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705062,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705062',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703407138',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705062,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705062',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479086',
        'method' => 'get',
      ),
    ),
  ),
  7702387841 => 
  array (
    'id' => 14479170,
    'name' => 'ООО «МЕДТЕХСНАБ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485338722,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705166,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705166',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702387841',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705166,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705166',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479170',
        'method' => 'get',
      ),
    ),
  ),
  7701876649 => 
  array (
    'id' => 14479204,
    'name' => 'ООО  "ВЕКТОР РАЗВИТИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485338857,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705202,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705202',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701876649',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705202,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705202',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479204',
        'method' => 'get',
      ),
    ),
  ),
  7326051934 => 
  array (
    'id' => 14479264,
    'name' => 'ООО "Фабрика мебели Форест"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485339159,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705282,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705282',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7326051934',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705282,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705282',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479264',
        'method' => 'get',
      ),
    ),
  ),
  9715215882 => 
  array (
    'id' => 14479332,
    'name' => 'ООО "АВТОСПЕЦПОСТАВКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485339450,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705364,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705364',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715215882',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705364,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705364',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479332',
        'method' => 'get',
      ),
    ),
  ),
  9715003824 => 
  array (
    'id' => 14479414,
    'name' => 'ООО "ТИПОГРАФИЯ "МИТТЕЛЬ ПРЕСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485339785,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705472,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705472',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715003824',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705472,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705472',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479414',
        'method' => 'get',
      ),
    ),
  ),
  7840505698 => 
  array (
    'id' => 14479472,
    'name' => 'ООО "СпецСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485340134,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705562,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705562',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840505698',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705562,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705562',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479472',
        'method' => 'get',
      ),
    ),
  ),
  7805651745 => 
  array (
    'id' => 14479532,
    'name' => 'ООО "ДОРТЕХЭКСПЕРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485340450,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705638,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705638',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805651745',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705638,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705638',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479532',
        'method' => 'get',
      ),
    ),
  ),
  7733261648 => 
  array (
    'id' => 14479574,
    'name' => 'ООО "СТРОИТЕЛЬНОЕ УПРАВЛЕНИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485340664,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705708,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705708',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733261648',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705708,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705708',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479574',
        'method' => 'get',
      ),
    ),
  ),
  7727731373 => 
  array (
    'id' => 14479604,
    'name' => 'ООО "Эксплуатационная компания "Санрайз"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485340848,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705746,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705746',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727731373',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705746,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705746',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479604',
        'method' => 'get',
      ),
    ),
  ),
  7715766050 => 
  array (
    'id' => 14479694,
    'name' => 'ООО ИТЦ Новые Технологии',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485341364,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28705864,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705864',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715766050',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28705864,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28705864',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14479694',
        'method' => 'get',
      ),
    ),
  ),
  7203089328 => 
  array (
    'id' => 14480150,
    'name' => 'ООО "АК "ФИНАНСИСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485343529,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28706372,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706372',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203089328',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28706372,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706372',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14480150',
        'method' => 'get',
      ),
    ),
  ),
  7734347697 => 
  array (
    'id' => 14480178,
    'name' => 'ООО Бест Фабрик',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485343659,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28706400,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706400',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734347697',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28706400,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706400',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14480178',
        'method' => 'get',
      ),
    ),
  ),
  7805328365 => 
  array (
    'id' => 14480330,
    'name' => 'ООО "ХОЛДИНГ 78-10"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485344348,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28706598,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706598',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805328365',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28706598,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706598',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14480330',
        'method' => 'get',
      ),
    ),
  ),
  7816337682 => 
  array (
    'id' => 14480536,
    'name' => 'ООО "Медстимул"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485345487,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28706862,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706862',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816337682',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28706862,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706862',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14480536',
        'method' => 'get',
      ),
    ),
  ),
  7840482987 => 
  array (
    'id' => 14480564,
    'name' => 'ООО СОНАР-медикал',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485345692,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28706906,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706906',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840482987',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28706906,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28706906',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14480564',
        'method' => 'get',
      ),
    ),
  ),
  6317102524 => 
  array (
    'id' => 14481156,
    'name' => 'ООО "СамараМазСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485348129,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28707576,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28707576',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317102524',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28707576,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28707576',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14481156',
        'method' => 'get',
      ),
    ),
  ),
  6658384491 => 
  array (
    'id' => 14481174,
    'name' => 'ООО Частная охранная организация  Горизонт',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485348234,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28707604,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28707604',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658384491',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28707604,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28707604',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14481174',
        'method' => 'get',
      ),
    ),
  ),
  1821000122 => 
  array (
    'id' => 14485316,
    'name' => 'ООО "САНАТОРИЙ УВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485405744,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28711804,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711804',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1821000122',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28711804,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711804',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485316',
        'method' => 'get',
      ),
    ),
  ),
  6319182074 => 
  array (
    'id' => 14485346,
    'name' => 'ООО "БВЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485406315,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28711846,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711846',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319182074',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28711846,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711846',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485346',
        'method' => 'get',
      ),
    ),
  ),
  6670432053 => 
  array (
    'id' => 14485362,
    'name' => 'ООО "ПРОМТОРГСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485406485,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28711862,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711862',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670432053',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28711862,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711862',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485362',
        'method' => 'get',
      ),
    ),
  ),
  7203338983 => 
  array (
    'id' => 14485388,
    'name' => 'ООО "КЛИНИНГ ЛЭНД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485406794,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28711894,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711894',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203338983',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28711894,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711894',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485388',
        'method' => 'get',
      ),
    ),
  ),
  7204015791 => 
  array (
    'id' => 14485476,
    'name' => 'ООО "ТИР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485408063,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712010,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712010',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7204015791',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712010,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712010',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485476',
        'method' => 'get',
      ),
    ),
  ),
  8911008400 => 
  array (
    'id' => 14485498,
    'name' => 'ООО "МОЛОКО"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485408480,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712040,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712040',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8911008400',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712040,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712040',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485498',
        'method' => 'get',
      ),
    ),
  ),
  7843002124 => 
  array (
    'id' => 14485618,
    'name' => 'ООО «Вита»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485409883,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712188,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712188',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7843002124',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712188,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712188',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485618',
        'method' => 'get',
      ),
    ),
  ),
  7841456612 => 
  array (
    'id' => 14485634,
    'name' => 'ООО «ДиректКонсалт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485410055,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712212,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712212',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841456612',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712212,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712212',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485634',
        'method' => 'get',
      ),
    ),
  ),
  7841388433 => 
  array (
    'id' => 14485654,
    'name' => 'ООО АРХИТЕКТУРНО-РЕСТАВРАЦИОННАЯ МАСТЕРСКАЯ "ВЕГА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485410267,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712238,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712238',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841388433',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712238,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712238',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485654',
        'method' => 'get',
      ),
    ),
  ),
  7841309336 => 
  array (
    'id' => 14485690,
    'name' => 'ООО "Техно-Дент-Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485410498,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712266,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712266',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841309336',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712266,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712266',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485690',
        'method' => 'get',
      ),
    ),
  ),
  7816546164 => 
  array (
    'id' => 14485726,
    'name' => 'ООО  "МЕРИДИАН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485410861,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712320,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712320',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816546164',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712320,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712320',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485726',
        'method' => 'get',
      ),
    ),
  ),
  7816119370 => 
  array (
    'id' => 14485744,
    'name' => 'ООО ЕВРОСТРОЙ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485411044,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712346,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712346',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816119370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712346,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712346',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485744',
        'method' => 'get',
      ),
    ),
  ),
  7814570027 => 
  array (
    'id' => 14485888,
    'name' => 'ООО Р-Индустрия',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485412093,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712482,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712482',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814570027',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712482,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712482',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485888',
        'method' => 'get',
      ),
    ),
  ),
  7729590255 => 
  array (
    'id' => 14486310,
    'name' => 'ООО «СВЕЖИЙ ВЕТЕР»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485413589,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28712918,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712918',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729590255',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28712918,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28712918',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14486310',
        'method' => 'get',
      ),
    ),
  ),
  7725690234 => 
  array (
    'id' => 14486410,
    'name' => 'ООО ИНТЕГРА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485414180,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28713046,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713046',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725690234',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28713046,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713046',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14486410',
        'method' => 'get',
      ),
    ),
  ),
  7721817017 => 
  array (
    'id' => 14486448,
    'name' => 'ООО ГП "Вятич"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485414604,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28713116,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713116',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721817017',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28713116,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713116',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14486448',
        'method' => 'get',
      ),
    ),
  ),
  7721628877 => 
  array (
    'id' => 14486476,
    'name' => 'ООО Волшебный Офис',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485414793,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28713162,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713162',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721628877',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28713162,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713162',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14486476',
        'method' => 'get',
      ),
    ),
  ),
  7721327816 => 
  array (
    'id' => 14486722,
    'name' => 'ООО "Авангард"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485416088,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28713452,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713452',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721327816',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28713452,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713452',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14486722',
        'method' => 'get',
      ),
    ),
  ),
  7719437917 => 
  array (
    'id' => 14486750,
    'name' => 'ООО "ВИТТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485416260,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28713498,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713498',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719437917',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28713498,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28713498',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14486750',
        'method' => 'get',
      ),
    ),
  ),
  7704665597 => 
  array (
    'id' => 14487674,
    'name' => 'ООО «Интаро Софт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485420329,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28714710,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28714710',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704665597',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28714710,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28714710',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14487674',
        'method' => 'get',
      ),
    ),
  ),
  8603161563 => 
  array (
    'id' => 14488360,
    'name' => 'ООО Арбат',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485422989,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28715540,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28715540',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603161563',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28715540,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28715540',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14488360',
        'method' => 'get',
      ),
    ),
  ),
  7807309671 => 
  array (
    'id' => 14488908,
    'name' => 'ООО "ФЕСТИВАЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485425284,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28716248,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28716248',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807309671',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28716248,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28716248',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14488908',
        'method' => 'get',
      ),
    ),
  ),
  7724304422 => 
  array (
    'id' => 14489046,
    'name' => 'ООО "Атлант М"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485426022,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28716442,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28716442',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724304422',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28716442,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28716442',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14489046',
        'method' => 'get',
      ),
    ),
  ),
  1121021462 => 
  array (
    'id' => 14489378,
    'name' => 'ООО Частное охранное предприятие  Рубеж',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485427561,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28188912,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28188912',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1121021462',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28188912,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28188912',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14489378',
        'method' => 'get',
      ),
    ),
  ),
  2014005411 => 
  array (
    'id' => 14489494,
    'name' => 'ООО «МЕДИКОР НЭКО»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485428123,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28716990,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28716990',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2014005411',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28716990,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28716990',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14489494',
        'method' => 'get',
      ),
    ),
  ),
  5190004433 => 
  array (
    'id' => 14489606,
    'name' => 'ООО  "ДВАЛИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485428653,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717114,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717114',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190004433',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717114,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717114',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14489606',
        'method' => 'get',
      ),
    ),
  ),
  6234092872 => 
  array (
    'id' => 14489730,
    'name' => 'ООО "Научно-производственное объеденение "Ратибор-С"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485429052,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717242,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717242',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6234092872',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717242,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717242',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14489730',
        'method' => 'get',
      ),
    ),
  ),
  7804394580 => 
  array (
    'id' => 14489996,
    'name' => 'ООО  "ЮПИТЕР ХОЛЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485430253,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717594,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717594',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804394580',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717594,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717594',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14489996',
        'method' => 'get',
      ),
    ),
  ),
  7811204294 => 
  array (
    'id' => 14490034,
    'name' => 'ООО "АЛЕКССТРОЙ ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485430398,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717644,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717644',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811204294',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717644,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717644',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490034',
        'method' => 'get',
      ),
    ),
  ),
  2130162630 => 
  array (
    'id' => 14490076,
    'name' => 'ООО "Доллимакс Плюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485430604,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717694,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717694',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130162630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717694,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717694',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490076',
        'method' => 'get',
      ),
    ),
  ),
  6230071286 => 
  array (
    'id' => 14490102,
    'name' => 'ООО "Топливная Компания СпектрАвто"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485430761,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717730,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717730',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6230071286',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717730,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717730',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490102',
        'method' => 'get',
      ),
    ),
  ),
  6829107170 => 
  array (
    'id' => 14490132,
    'name' => 'ООО "ТПС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485430899,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717766,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717766',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829107170',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717766,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717766',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490132',
        'method' => 'get',
      ),
    ),
  ),
  6832029098 => 
  array (
    'id' => 14490194,
    'name' => 'ООО фирма "Юлис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485431157,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28717846,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717846',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6832029098',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28717846,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28717846',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490194',
        'method' => 'get',
      ),
    ),
  ),
  7715488484 => 
  array (
    'id' => 14490328,
    'name' => 'ООО "НОВЫЙ ДОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485431848,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28718018,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28718018',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715488484',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28718018,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28718018',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490328',
        'method' => 'get',
      ),
    ),
  ),
  7735565923 => 
  array (
    'id' => 14490396,
    'name' => 'ООО "ТСП-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485432135,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28725810,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28725810',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735565923',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28725810,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28725810',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490396',
        'method' => 'get',
      ),
    ),
  ),
  7730144431 => 
  array (
    'id' => 14490442,
    'name' => 'ООО «ВЫСТАВОЧНАЯ КОМПАНИЯ АСТИ ГРУПП»)',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485432282,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28725868,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28725868',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730144431',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28725868,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28725868',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490442',
        'method' => 'get',
      ),
    ),
  ),
  7722334196 => 
  array (
    'id' => 14490798,
    'name' => 'ООО "ГАЛТ""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485434223,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28726394,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726394',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722334196',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28726394,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726394',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490798',
        'method' => 'get',
      ),
    ),
  ),
  7714911717 => 
  array (
    'id' => 14490844,
    'name' => 'ООО "МЕДРЕМТЕХНИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485434403,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28726450,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726450',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714911717',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28726450,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726450',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490844',
        'method' => 'get',
      ),
    ),
  ),
  1215129084 => 
  array (
    'id' => 14490912,
    'name' => 'ООО «Аверс-Медикал»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485434739,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28726516,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726516',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1215129084',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28726516,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726516',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14490912',
        'method' => 'get',
      ),
    ),
  ),
  1655290535 => 
  array (
    'id' => 14491038,
    'name' => 'ООО «Санамедикал»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485435178,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28726648,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726648',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655290535',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28726648,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726648',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14491038',
        'method' => 'get',
      ),
    ),
  ),
  5010037231 => 
  array (
    'id' => 14491096,
    'name' => 'ООО "РАЗВИТИЕ ОБЪЕДИНЕНИЙ СЕРВИСНОГО ПАРТНЕРСТВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485435429,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28726704,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726704',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5010037231',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28726704,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726704',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14491096',
        'method' => 'get',
      ),
    ),
  ),
  6829119633 => 
  array (
    'id' => 14491228,
    'name' => 'ООО "СТТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485435909,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28726826,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726826',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829119633',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28726826,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28726826',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14491228',
        'method' => 'get',
      ),
    ),
  ),
  1833057719 => 
  array (
    'id' => 14495166,
    'name' => 'ООО "АВАНГАРД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485493757,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731104,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731104',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1833057719',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731104,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731104',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495166',
        'method' => 'get',
      ),
    ),
  ),
  2225141244 => 
  array (
    'id' => 14495176,
    'name' => 'ООО "КОНТИНЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485493911,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731110,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731110',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225141244',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731110,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731110',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495176',
        'method' => 'get',
      ),
    ),
  ),
  5405187250 => 
  array (
    'id' => 14495190,
    'name' => 'ООО "ЗапСибЖилСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485494221,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731130,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731130',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405187250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731130,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731130',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495190',
        'method' => 'get',
      ),
    ),
  ),
  5503162198 => 
  array (
    'id' => 14495232,
    'name' => 'ООО "АРИС ГРУПП +"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485494972,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731196,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731196',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503162198',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731196,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731196',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495232',
        'method' => 'get',
      ),
    ),
  ),
  5905297406 => 
  array (
    'id' => 14495266,
    'name' => 'ООО Проект-Сервис',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485495453,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731232,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731232',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905297406',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731232,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731232',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495266',
        'method' => 'get',
      ),
    ),
  ),
  6316114710 => 
  array (
    'id' => 14495306,
    'name' => 'ООО "ЧОП "КАТАНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485496261,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731286,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731286',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316114710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731286,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731286',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495306',
        'method' => 'get',
      ),
    ),
  ),
  6317025929 => 
  array (
    'id' => 14495378,
    'name' => 'ООО Телефонсервис',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485496933,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731356,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731356',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317025929',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731356,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731356',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495378',
        'method' => 'get',
      ),
    ),
  ),
  6319015789 => 
  array (
    'id' => 14495436,
    'name' => 'ОАО "САНАТОРИЙ ИМ. В.П. ЧКАЛОВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485497597,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731430,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731430',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319015789',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731430,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731430',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495436',
        'method' => 'get',
      ),
    ),
  ),
  7204168974 => 
  array (
    'id' => 14495488,
    'name' => 'ООО Технологии сервиса',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485498125,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731488,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731488',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7204168974',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731488,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731488',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495488',
        'method' => 'get',
      ),
    ),
  ),
  7411088300 => 
  array (
    'id' => 14495504,
    'name' => 'ООО "ГРАНИТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485498219,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731514,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731514',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7411088300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731514,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731514',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495504',
        'method' => 'get',
      ),
    ),
  ),
  7447083433 => 
  array (
    'id' => 14495532,
    'name' => 'ООО "ДИНАМИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485498436,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731538,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731538',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7447083433',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731538,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731538',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495532',
        'method' => 'get',
      ),
    ),
  ),
  7714921137 => 
  array (
    'id' => 14495592,
    'name' => 'ООО "АЗИМУТ""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485498989,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731622,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731622',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714921137',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731622,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731622',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495592',
        'method' => 'get',
      ),
    ),
  ),
  7725549584 => 
  array (
    'id' => 14495618,
    'name' => 'ООО Нева-Проект',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485499199,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731656,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731656',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725549584',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731656,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731656',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495618',
        'method' => 'get',
      ),
    ),
  ),
  7804487524 => 
  array (
    'id' => 14495642,
    'name' => 'ООО "Базис Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485499345,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28731682,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731682',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804487524',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28731682,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28731682',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14495642',
        'method' => 'get',
      ),
    ),
  ),
  2310112710 => 
  array (
    'id' => 14496338,
    'name' => 'ООО "АРТиК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485502927,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28732508,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28732508',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2310112710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28732508,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28732508',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14496338',
        'method' => 'get',
      ),
    ),
  ),
  3444201970 => 
  array (
    'id' => 14496476,
    'name' => 'ООО "КОМПОРГСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485503649,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28732644,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28732644',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444201970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28732644,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28732644',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14496476',
        'method' => 'get',
      ),
    ),
  ),
  4632057175 => 
  array (
    'id' => 14496890,
    'name' => 'ООО "ЦЕНТР ЭКОЛОГИЧЕСКИХ АНАЛИЗОВ И РАСЧЕТОВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485505972,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28733208,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28733208',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632057175',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28733208,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28733208',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14496890',
        'method' => 'get',
      ),
    ),
  ),
  5027216510 => 
  array (
    'id' => 14496990,
    'name' => 'ООО «РМ-Системс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485506393,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28733330,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28733330',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027216510',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28733330,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28733330',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14496990',
        'method' => 'get',
      ),
    ),
  ),
  5044077666 => 
  array (
    'id' => 14497594,
    'name' => 'ООО "ГеоМакс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485509074,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734098,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734098',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5044077666',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734098,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734098',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14497594',
        'method' => 'get',
      ),
    ),
  ),
  5406790327 => 
  array (
    'id' => 14497622,
    'name' => 'ООО "Соммерс-Машиностроение"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485509237,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734140,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734140',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406790327',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734140,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734140',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14497622',
        'method' => 'get',
      ),
    ),
  ),
  5836678140 => 
  array (
    'id' => 14497636,
    'name' => 'ООО "ГАРАНТ-МЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485509296,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734154,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734154',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5836678140',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734154,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734154',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14497636',
        'method' => 'get',
      ),
    ),
  ),
  6165133652 => 
  array (
    'id' => 14497730,
    'name' => 'ООО "ВЫСТАВОЧНЫЙ ЦЕНТР "РОСТЭКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485509669,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734268,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734268',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165133652',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734268,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734268',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14497730',
        'method' => 'get',
      ),
    ),
  ),
  7701957440 => 
  array (
    'id' => 14497796,
    'name' => 'ООО "Ларсус"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485510072,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734356,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734356',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701957440',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734356,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734356',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14497796',
        'method' => 'get',
      ),
    ),
  ),
  7705808086 => 
  array (
    'id' => 14497842,
    'name' => 'ООО "ВМП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485510366,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734440,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734440',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705808086',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734440,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734440',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14497842',
        'method' => 'get',
      ),
    ),
  ),
  7715617958 => 
  array (
    'id' => 14497954,
    'name' => 'ООО "ВОЙСЛИНК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485511007,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734548,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734548',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715617958',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734548,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734548',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14497954',
        'method' => 'get',
      ),
    ),
  ),
  7718274096 => 
  array (
    'id' => 14498006,
    'name' => 'ООО «Демас»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485511221,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734604,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734604',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718274096',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734604,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734604',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498006',
        'method' => 'get',
      ),
    ),
  ),
  7718306301 => 
  array (
    'id' => 14498052,
    'name' => 'ООО «СУПЕР БЛЕСК»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485511387,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734654,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734654',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718306301',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734654,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734654',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498052',
        'method' => 'get',
      ),
    ),
  ),
  7718970135 => 
  array (
    'id' => 14498168,
    'name' => 'ООО «Димер»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485511996,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28734822,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734822',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718970135',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28734822,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28734822',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498168',
        'method' => 'get',
      ),
    ),
  ),
  7724186200 => 
  array (
    'id' => 14498548,
    'name' => 'ООО "Электросеть-96"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485514082,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28735274,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735274',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724186200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28735274,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735274',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498548',
        'method' => 'get',
      ),
    ),
  ),
  7727777787 => 
  array (
    'id' => 14498578,
    'name' => 'ООО "МедТехФарм"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485514273,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28735318,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735318',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727777787',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28735318,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735318',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498578',
        'method' => 'get',
      ),
    ),
  ),
  7728863502 => 
  array (
    'id' => 14498756,
    'name' => 'ООО "ЭНЕРГОРЕСУРССТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485515081,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28735546,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735546',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728863502',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28735546,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735546',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498756',
        'method' => 'get',
      ),
    ),
  ),
  7729446036 => 
  array (
    'id' => 14498800,
    'name' => 'ООО "АРМОРЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485515248,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28735606,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735606',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729446036',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28735606,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735606',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498800',
        'method' => 'get',
      ),
    ),
  ),
  7733800917 => 
  array (
    'id' => 14498840,
    'name' => 'ООО "Сфера"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485515456,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28735674,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735674',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733800917',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28735674,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735674',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498840',
        'method' => 'get',
      ),
    ),
  ),
  7735567335 => 
  array (
    'id' => 14498882,
    'name' => 'ООО "ЭкоБюро"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485515759,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28735728,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735728',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735567335',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28735728,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735728',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498882',
        'method' => 'get',
      ),
    ),
  ),
  7801454062 => 
  array (
    'id' => 14498950,
    'name' => 'ООО "ПЭП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485516079,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28735806,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735806',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801454062',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28735806,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28735806',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14498950',
        'method' => 'get',
      ),
    ),
  ),
  7804539275 => 
  array (
    'id' => 14499154,
    'name' => 'ООО "ПрофСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485517126,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28736096,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28736096',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804539275',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28736096,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28736096',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14499154',
        'method' => 'get',
      ),
    ),
  ),
  7804546650 => 
  array (
    'id' => 14499212,
    'name' => 'ООО «СК «Континент»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485517359,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28736158,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28736158',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804546650',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28736158,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28736158',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14499212',
        'method' => 'get',
      ),
    ),
  ),
  267019387 => 
  array (
    'id' => 14514450,
    'name' => 'ООО "Жил-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485753697,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751490,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751490',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '267019387',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751490,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751490',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514450',
        'method' => 'get',
      ),
    ),
  ),
  1832110991 => 
  array (
    'id' => 14514470,
    'name' => 'ООО "Уральская строительная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485753914,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751500,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751500',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1832110991',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751500,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751500',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514470',
        'method' => 'get',
      ),
    ),
  ),
  1841029419 => 
  array (
    'id' => 14514480,
    'name' => 'ООО "РегионСтрой 18"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485754064,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751516,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751516',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1841029419',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751516,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751516',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514480',
        'method' => 'get',
      ),
    ),
  ),
  1841044022 => 
  array (
    'id' => 14514482,
    'name' => 'ООО "УРАЛСТРОЙПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485754177,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751528,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751528',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1841044022',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751528,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751528',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514482',
        'method' => 'get',
      ),
    ),
  ),
  5903116647 => 
  array (
    'id' => 14514496,
    'name' => 'ООО "СТАНДАРТМЕДИАСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485754335,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751550,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751550',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903116647',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751550,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751550',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514496',
        'method' => 'get',
      ),
    ),
  ),
  5904198498 => 
  array (
    'id' => 14514506,
    'name' => 'ООО "ПРИОРИТЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485754448,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751566,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751566',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904198498',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751566,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751566',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514506',
        'method' => 'get',
      ),
    ),
  ),
  6316164729 => 
  array (
    'id' => 14514516,
    'name' => 'ООО "Медицинский сервисный центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485754619,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751592,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751592',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316164729',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751592,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751592',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514516',
        'method' => 'get',
      ),
    ),
  ),
  5903106600 => 
  array (
    'id' => 14514548,
    'name' => 'ООО "ВИАЛ групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485755050,
    'updated_at' => 1522728602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28751630,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751630',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903106600',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28751630,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28751630',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14514548',
        'method' => 'get',
      ),
    ),
  ),
);