<?php 
 return array (
  7723629241 => 
  array (
    'id' => 15476117,
    'name' => 'ООО АЛЬТАИР-СЕРВИС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495536675,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30052283,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30052283',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723629241',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30052283,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30052283',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15476117',
        'method' => 'get',
      ),
    ),
  ),
  7722394420 => 
  array (
    'id' => 15476979,
    'name' => 'ООО "ТЕХНОГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495536759,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30053779,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30053779',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722394420',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30053779,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30053779',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15476979',
        'method' => 'get',
      ),
    ),
  ),
  7722364698 => 
  array (
    'id' => 15476999,
    'name' => 'ООО  "ПРАЙМ МЕДИКАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495536819,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30053813,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30053813',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722364698',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30053813,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30053813',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15476999',
        'method' => 'get',
      ),
    ),
  ),
  7718293998 => 
  array (
    'id' => 15477887,
    'name' => 'ООО "Торговый Дом "Компаньон-Строй""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495536986,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30055347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718293998',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30055347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15477887',
        'method' => 'get',
      ),
    ),
  ),
  7717797368 => 
  array (
    'id' => 15477911,
    'name' => 'ООО СПФ "МОНОЛИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495537079,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30055381,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055381',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717797368',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30055381,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055381',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15477911',
        'method' => 'get',
      ),
    ),
  ),
  7716717120 => 
  array (
    'id' => 15478041,
    'name' => 'ООО "Уютный дворик"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495537515,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30055529,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055529',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1503301047,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716717120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30055529,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055529',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15478041',
        'method' => 'get',
      ),
    ),
  ),
  7714548388 => 
  array (
    'id' => 15478091,
    'name' => 'ООО НТО Промышленная Безопасность',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495537683,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30055583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714548388',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30055583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15478091',
        'method' => 'get',
      ),
    ),
  ),
  7719555540 => 
  array (
    'id' => 15478281,
    'name' => 'ООО «Электронные системы безопасности»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1495538316,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30055831,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055831',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 31464625,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=31464625',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719555540',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30055831,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30055831',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15478281',
        'method' => 'get',
      ),
    ),
  ),
  7704500210 => 
  array (
    'id' => 15478529,
    'name' => 'ООО  "САЙТЭК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495539242,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30056125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704500210',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30056125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15478529',
        'method' => 'get',
      ),
    ),
  ),
  7703819300 => 
  array (
    'id' => 15478739,
    'name' => 'ООО "СТОИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495540031,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30056407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703819300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30056407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15478739',
        'method' => 'get',
      ),
    ),
  ),
  7701916122 => 
  array (
    'id' => 15478807,
    'name' => 'ООО "Компания "Платинум"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495540228,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30056491,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056491',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701916122',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30056491,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056491',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15478807',
        'method' => 'get',
      ),
    ),
  ),
  6234148116 => 
  array (
    'id' => 15478877,
    'name' => 'ООО  "ЦЕНТР РАЗВИТИЯ-ВОСТОК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495540515,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30056583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6234148116',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30056583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30056583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15478877',
        'method' => 'get',
      ),
    ),
  ),
  '0274177779' => 
  array (
    'id' => 15483651,
    'name' => 'ООО "СИСТЕМА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495602648,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063187,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063187',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0274177779',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063187,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063187',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483651',
        'method' => 'get',
      ),
    ),
  ),
  1833048993 => 
  array (
    'id' => 15483659,
    'name' => 'ООО "Кама-стройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495602693,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063195,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063195',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1833048993',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063195,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063195',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483659',
        'method' => 'get',
      ),
    ),
  ),
  1834036172 => 
  array (
    'id' => 15483721,
    'name' => 'ООО "Диализные технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495603297,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063293,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063293',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1834036172',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063293,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063293',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483721',
        'method' => 'get',
      ),
    ),
  ),
  2225109963 => 
  array (
    'id' => 15483735,
    'name' => 'ООО"АлтайМедСнаб"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495603576,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225109963',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483735',
        'method' => 'get',
      ),
    ),
  ),
  5427128001 => 
  array (
    'id' => 15483783,
    'name' => 'ООО "ГРАЖДАНСТРОЙПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495604032,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063397,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063397',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5427128001',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063397,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063397',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483783',
        'method' => 'get',
      ),
    ),
  ),
  5919024617 => 
  array (
    'id' => 15483867,
    'name' => 'ООО "КРАСНОВИШЕРСКИЙ КАМНЕОБРАБАТЫВАЮЩИЙ ЗАВОД',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495604968,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063509,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063509',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5919024617',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063509,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063509',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483867',
        'method' => 'get',
      ),
    ),
  ),
  6350022650 => 
  array (
    'id' => 15483903,
    'name' => 'ООО "РЕАЛСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495605255,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063563,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063563',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6350022650',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483903',
        'method' => 'get',
      ),
    ),
  ),
  6604018308 => 
  array (
    'id' => 15483917,
    'name' => 'ООО "Строительная Компания-Градстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495605360,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6604018308',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483917',
        'method' => 'get',
      ),
    ),
  ),
  6686086031 => 
  array (
    'id' => 15483937,
    'name' => 'ООО ВИКТОРИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495605514,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063593,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063593',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6686086031',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063593,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063593',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483937',
        'method' => 'get',
      ),
    ),
  ),
  7610042798 => 
  array (
    'id' => 15483955,
    'name' => 'ООО ПСК "Формика"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495605639,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063617,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063617',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7610042798',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063617,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063617',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483955',
        'method' => 'get',
      ),
    ),
  ),
  4345318948 => 
  array (
    'id' => 15484021,
    'name' => 'ООО "Волна"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495605919,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063687,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063687',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345318948',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063687,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063687',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15484021',
        'method' => 'get',
      ),
    ),
  ),
  3123115208 => 
  array (
    'id' => 15484071,
    'name' => 'ООО "Ремстройкомплект"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495606212,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063751,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063751',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123115208',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063751,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063751',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15484071',
        'method' => 'get',
      ),
    ),
  ),
  7802523128 => 
  array (
    'id' => 15484105,
    'name' => 'ООО "РУСВУД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495606439,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063791,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063791',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802523128',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063791,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063791',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15484105',
        'method' => 'get',
      ),
    ),
  ),
  9723003257 => 
  array (
    'id' => 15484217,
    'name' => 'ООО СК «АТОМСПЕЦСТРОЙПРОЕКТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495607146,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063927,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063927',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9723003257',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063927,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063927',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15484217',
        'method' => 'get',
      ),
    ),
  ),
  9718045220 => 
  array (
    'id' => 15484289,
    'name' => 'ООО «2Альфа Групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495607441,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063993,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063993',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718045220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063993,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063993',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15484289',
        'method' => 'get',
      ),
    ),
  ),
  7839438172 => 
  array (
    'id' => 15484737,
    'name' => 'ООО "Областная Строительная Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495609955,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30064547,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30064547',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839438172',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30064547,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30064547',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15484737',
        'method' => 'get',
      ),
    ),
  ),
  7839060589 => 
  array (
    'id' => 15484793,
    'name' => 'ООО «ТЕХНОСЕРВИС»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495610129,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30064611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30064611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839060589',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30064611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30064611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15484793',
        'method' => 'get',
      ),
    ),
  ),
  7811460682 => 
  array (
    'id' => 15485137,
    'name' => 'ООО "ГЛОБАЛ БАС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495611650,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30065081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30065081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811460682',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30065081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30065081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15485137',
        'method' => 'get',
      ),
    ),
  ),
  7801610931 => 
  array (
    'id' => 15485647,
    'name' => 'ООО "Экофлора"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495613728,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30065693,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30065693',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801610931',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30065693,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30065693',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15485647',
        'method' => 'get',
      ),
    ),
  ),
  7729702339 => 
  array (
    'id' => 15487925,
    'name' => 'ООО "АВИСТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495619282,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30069055,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069055',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729702339',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30069055,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069055',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15487925',
        'method' => 'get',
      ),
    ),
  ),
  7729114344 => 
  array (
    'id' => 15487969,
    'name' => 'ООО "ГРИНТОП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495619487,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30069119,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069119',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729114344',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30069119,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069119',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15487969',
        'method' => 'get',
      ),
    ),
  ),
  7726696849 => 
  array (
    'id' => 15488001,
    'name' => 'ООО "МонтажСтройСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495619643,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30069173,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069173',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726696849',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30069173,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069173',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15488001',
        'method' => 'get',
      ),
    ),
  ),
  7726674612 => 
  array (
    'id' => 15488097,
    'name' => 'ООО «МаксиТек»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495620282,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30069335,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069335',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726674612',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30069335,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069335',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15488097',
        'method' => 'get',
      ),
    ),
  ),
  7725753815 => 
  array (
    'id' => 15488137,
    'name' => 'ООО "Комплексные Инженерные Системы"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495620432,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30069379,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069379',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725753815',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30069379,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069379',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15488137',
        'method' => 'get',
      ),
    ),
  ),
  7724924899 => 
  array (
    'id' => 15488519,
    'name' => 'ООО  РЕМОНТНО-СТРОИТЕЛЬНАЯ ОРГАНИЗАЦИЯ-Ю',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495621945,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30069865,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069865',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724924899',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30069865,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069865',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15488519',
        'method' => 'get',
      ),
    ),
  ),
  7724797062 => 
  array (
    'id' => 15488529,
    'name' => 'ООО Твинс',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495621987,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30069879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724797062',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30069879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30069879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15488529',
        'method' => 'get',
      ),
    ),
  ),
  7720504124 => 
  array (
    'id' => 15488619,
    'name' => 'ЗАО "СМАРТ.технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495622413,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30070011,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30070011',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720504124',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30070011,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30070011',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15488619',
        'method' => 'get',
      ),
    ),
  ),
  8906009279 => 
  array (
    'id' => 15494961,
    'name' => 'ООО "Строительная Компания "Союз"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495689728,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8906009279',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15494961',
        'method' => 'get',
      ),
    ),
  ),
  8609018961 => 
  array (
    'id' => 15494985,
    'name' => 'ООО "СЕВЕР-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495690013,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8609018961',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15494985',
        'method' => 'get',
      ),
    ),
  ),
  8601049051 => 
  array (
    'id' => 15495019,
    'name' => 'ООО "Чехов Хаус"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495690572,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078473,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078473',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8601049051',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078473,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078473',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495019',
        'method' => 'get',
      ),
    ),
  ),
  4502015544 => 
  array (
    'id' => 15495077,
    'name' => 'ООО "МультиПласт"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495691223,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4502015544',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495077',
        'method' => 'get',
      ),
    ),
  ),
  5405996886 => 
  array (
    'id' => 15495093,
    'name' => 'ООО "Мосты Сибири"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495691384,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078595,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078595',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405996886',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078595,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078595',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495093',
        'method' => 'get',
      ),
    ),
  ),
  5427107812 => 
  array (
    'id' => 15495117,
    'name' => '"ООО ПК ""Инвестстройпроект"""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495691539,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5427107812',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495117',
        'method' => 'get',
      ),
    ),
  ),
  5433183138 => 
  array (
    'id' => 15495229,
    'name' => 'ООО Экострой',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495692051,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5433183138',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495229',
        'method' => 'get',
      ),
    ),
  ),
  5609099147 => 
  array (
    'id' => 15495245,
    'name' => 'ООО "Оренбургская Транспортная Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495692311,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078735,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078735',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5609099147',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078735,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078735',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495245',
        'method' => 'get',
      ),
    ),
  ),
  9705083443 => 
  array (
    'id' => 15495319,
    'name' => 'ООО "АТЛАНТ Н"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495692888,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078831,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078831',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705083443',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078831,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078831',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495319',
        'method' => 'get',
      ),
    ),
  ),
  9701006790 => 
  array (
    'id' => 15495335,
    'name' => 'ООО "КЭМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495693059,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078847,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078847',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701006790',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078847,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078847',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495335',
        'method' => 'get',
      ),
    ),
  ),
  9204004465 => 
  array (
    'id' => 15495389,
    'name' => 'ООО "АНКОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495693179,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078905,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078905',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9204004465',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078905,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078905',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495389',
        'method' => 'get',
      ),
    ),
  ),
  7817330168 => 
  array (
    'id' => 15495409,
    'name' => 'ООО «Конструктив»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495693328,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078923,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078923',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7817330168',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078923,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078923',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495409',
        'method' => 'get',
      ),
    ),
  ),
  7805666565 => 
  array (
    'id' => 15495647,
    'name' => 'ООО "БЭСТ ИНЖИНИРИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495694733,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30079231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805666565',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30079231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495647',
        'method' => 'get',
      ),
    ),
  ),
  7804316769 => 
  array (
    'id' => 15495701,
    'name' => 'ООО "НАУЧНО-ПРОИЗВОДСТВЕННАЯ КОРПОРАЦИЯ ПРОЕКТВОДСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495695168,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30079267,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079267',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804316769',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30079267,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079267',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495701',
        'method' => 'get',
      ),
    ),
  ),
  7733898645 => 
  array (
    'id' => 15495863,
    'name' => 'ООО «РемТехник»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495695857,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30079445,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079445',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733898645',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30079445,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079445',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495863',
        'method' => 'get',
      ),
    ),
  ),
  7733885332 => 
  array (
    'id' => 15495899,
    'name' => 'ООО НПО «КОЛЬЧУГА-М»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495696015,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30079483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733885332',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30079483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495899',
        'method' => 'get',
      ),
    ),
  ),
  7733761094 => 
  array (
    'id' => 15496017,
    'name' => 'ООО "УПРАВЛЯЮЩАЯ КОМПАНИЯ "Д-ХОЛДИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495696596,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30079631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733761094',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30079631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30079631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15496017',
        'method' => 'get',
      ),
    ),
  ),
  7731298716 => 
  array (
    'id' => 15496431,
    'name' => 'ООО «Экотрейд премиум»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495698301,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30080249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30080249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731298716',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30080249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30080249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15496431',
        'method' => 'get',
      ),
    ),
  ),
  7724540765 => 
  array (
    'id' => 15498639,
    'name' => 'ООО "Абсолют-Инвест"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495707654,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30083183,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083183',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724540765',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30083183,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083183',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15498639',
        'method' => 'get',
      ),
    ),
  ),
  7722604283 => 
  array (
    'id' => 15498791,
    'name' => 'ООО «Стелс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495708304,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30083365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722604283',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30083365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15498791',
        'method' => 'get',
      ),
    ),
  ),
  7718742160 => 
  array (
    'id' => 15498823,
    'name' => 'ООО "СТРОЙТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495708498,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30083405,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083405',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718742160',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30083405,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083405',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15498823',
        'method' => 'get',
      ),
    ),
  ),
  7718146513 => 
  array (
    'id' => 15498857,
    'name' => 'АО "АЛЬЯНС-ГАММА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495708610,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30083449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718146513',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30083449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30083449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15498857',
        'method' => 'get',
      ),
    ),
  ),
  7714125128 => 
  array (
    'id' => 15499997,
    'name' => 'ООО "ГИДРОТЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495713291,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30084969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30084969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714125128',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30084969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30084969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15499997',
        'method' => 'get',
      ),
    ),
  ),
  7103049000 => 
  array (
    'id' => 15500077,
    'name' => 'ООО "ЮконСтройКомплект"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495713674,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30085087,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085087',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7103049000',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30085087,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085087',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500077',
        'method' => 'get',
      ),
    ),
  ),
  6167123346 => 
  array (
    'id' => 15500489,
    'name' => 'ООО "ПРОЕКТНАЯ ФИРМА "ПРОЕКТИННОВАЦИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495715765,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30085673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085673',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6167123346',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30085673,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085673',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500489',
        'method' => 'get',
      ),
    ),
  ),
  6027053667 => 
  array (
    'id' => 15500503,
    'name' => 'ООО "Спецпроектжилстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495715891,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30085691,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085691',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6027053667',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30085691,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085691',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500503',
        'method' => 'get',
      ),
    ),
  ),
  7714021680 => 
  array (
    'id' => 15500665,
    'name' => 'ООО ЧАСТНОЕ ОХРАННОЕ ПРЕДПРИЯТИЕ "ДОА "АНГЕЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495716562,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30085891,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085891',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714021680',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30085891,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085891',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500665',
        'method' => 'get',
      ),
    ),
  ),
  7713292817 => 
  array (
    'id' => 15500725,
    'name' => 'ООО "АЛЕКС РЕСПЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495716867,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30085973,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085973',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713292817',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30085973,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30085973',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500725',
        'method' => 'get',
      ),
    ),
  ),
  7704331273 => 
  array (
    'id' => 15500815,
    'name' => 'ООО "РУСНАН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495717375,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30086105,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086105',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704331273',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30086105,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086105',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500815',
        'method' => 'get',
      ),
    ),
  ),
  5031123450 => 
  array (
    'id' => 15500865,
    'name' => 'ООО "Клаксон"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495717618,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30086175,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086175',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5031123450',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30086175,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086175',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500865',
        'method' => 'get',
      ),
    ),
  ),
  3666199786 => 
  array (
    'id' => 15500903,
    'name' => 'ООО "ЛК-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495717780,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30086217,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086217',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3666199786',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30086217,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086217',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15500903',
        'method' => 'get',
      ),
    ),
  ),
  7706795376 => 
  array (
    'id' => 15501057,
    'name' => 'ООО "АЛ СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495718443,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30086417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706795376',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30086417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15501057',
        'method' => 'get',
      ),
    ),
  ),
  7132003699 => 
  array (
    'id' => 15501077,
    'name' => 'ООО "ЛЕДОКОЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495718515,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30086441,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086441',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7132003699',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30086441,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086441',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15501077',
        'method' => 'get',
      ),
    ),
  ),
  6229060995 => 
  array (
    'id' => 15501101,
    'name' => 'АО  «Рязанское конструкторское бюро «Глобус»»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495718584,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30086471,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086471',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6229060995',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30086471,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086471',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15501101',
        'method' => 'get',
      ),
    ),
  ),
  6147038300 => 
  array (
    'id' => 15501131,
    'name' => 'ООО "ПРОКОНСМЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495718714,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30086523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6147038300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30086523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30086523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15501131',
        'method' => 'get',
      ),
    ),
  ),
  274150953 => 
  array (
    'id' => 15504051,
    'name' => 'ООО "ЭкоСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495776440,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30089633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274150953',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30089633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504051',
        'method' => 'get',
      ),
    ),
  ),
  2234015246 => 
  array (
    'id' => 15504065,
    'name' => 'ООО "Управление механизации"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495776634,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30089649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2234015246',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30089649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504065',
        'method' => 'get',
      ),
    ),
  ),
  6685083180 => 
  array (
    'id' => 15504087,
    'name' => 'ООО «НеоКрил»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495776928,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30089689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6685083180',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30089689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504087',
        'method' => 'get',
      ),
    ),
  ),
  5906137268 => 
  array (
    'id' => 15504201,
    'name' => 'ООО "КАМА ШИППИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495777571,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30089813,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089813',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5906137268',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30089813,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089813',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504201',
        'method' => 'get',
      ),
    ),
  ),
  9718011069 => 
  array (
    'id' => 15504305,
    'name' => 'ООО "БЕНЕДИКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495778488,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30089929,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089929',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718011069',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30089929,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089929',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504305',
        'method' => 'get',
      ),
    ),
  ),
  7842430092 => 
  array (
    'id' => 15504311,
    'name' => 'ООО "Центр монтажных технологий"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495778569,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30089945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842430092',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30089945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30089945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504311',
        'method' => 'get',
      ),
    ),
  ),
  7839493536 => 
  array (
    'id' => 15504381,
    'name' => 'ООО "СК-ТАУРУС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495779360,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090047,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090047',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1501568650,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839493536',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090047,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090047',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504381',
        'method' => 'get',
      ),
    ),
  ),
  7839476097 => 
  array (
    'id' => 15504391,
    'name' => 'ООО "ГК "ЭнергоСистемы"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495779423,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839476097',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504391',
        'method' => 'get',
      ),
    ),
  ),
  7839392344 => 
  array (
    'id' => 15504407,
    'name' => 'ООО "СтройКом"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495779563,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839392344',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504407',
        'method' => 'get',
      ),
    ),
  ),
  7838338560 => 
  array (
    'id' => 15504419,
    'name' => 'ООО ЧОО "ВЕТЕРАНЫ СПЕЦСЛУЖБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495779670,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090121,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090121',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838338560',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504419',
        'method' => 'get',
      ),
    ),
  ),
  7816492328 => 
  array (
    'id' => 15504437,
    'name' => 'ООО "Техно Эйр - СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495779896,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090159,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090159',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816492328',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090159,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090159',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504437',
        'method' => 'get',
      ),
    ),
  ),
  7810955393 => 
  array (
    'id' => 15504591,
    'name' => 'ООО "СИТИСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495780779,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810955393',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504591',
        'method' => 'get',
      ),
    ),
  ),
  7805512935 => 
  array (
    'id' => 15504603,
    'name' => 'ООО "БАЛТТЕХСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495780847,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090363,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090363',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805512935',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090363,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090363',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504603',
        'method' => 'get',
      ),
    ),
  ),
  7804515108 => 
  array (
    'id' => 15504639,
    'name' => 'ООО "МК ПРОМСТРОЙМЕТАЛЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495781152,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090419,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090419',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804515108',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090419,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090419',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504639',
        'method' => 'get',
      ),
    ),
  ),
  7733598874 => 
  array (
    'id' => 15504707,
    'name' => 'ООО «ПЕРСПЕКТИВА»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495781538,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30090497,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090497',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733598874',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30090497,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30090497',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15504707',
        'method' => 'get',
      ),
    ),
  ),
  7731416110 => 
  array (
    'id' => 15505847,
    'name' => 'ООО "БСА инжстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495787242,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30091813,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30091813',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731416110',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30091813,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30091813',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15505847',
        'method' => 'get',
      ),
    ),
  ),
  5263114342 => 
  array (
    'id' => 15507609,
    'name' => 'ООО "Швейная фабрика №19"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495796073,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094005,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094005',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5263114342',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094005,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094005',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15507609',
        'method' => 'get',
      ),
    ),
  ),
  5038082788 => 
  array (
    'id' => 15507685,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ КОМПАНИЯ 336 УНР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495796332,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094059,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094059',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5038082788',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094059,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094059',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15507685',
        'method' => 'get',
      ),
    ),
  ),
  5032219108 => 
  array (
    'id' => 15507931,
    'name' => 'ООО "ТЕХНО-АРХИТЭК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495797624,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094371,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094371',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032219108',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094371,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094371',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15507931',
        'method' => 'get',
      ),
    ),
  ),
  5001104481 => 
  array (
    'id' => 15508035,
    'name' => 'ООО "БКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495798014,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094495,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094495',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5001104481',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094495,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094495',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15508035',
        'method' => 'get',
      ),
    ),
  ),
  3702743757 => 
  array (
    'id' => 15508107,
    'name' => 'ООО "Ивановское предприятие по благоустройству',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495798386,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702743757',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15508107',
        'method' => 'get',
      ),
    ),
  ),
  3525296466 => 
  array (
    'id' => 15508173,
    'name' => 'ООО ПрестижСтрой',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495798686,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094673',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525296466',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094673,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094673',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15508173',
        'method' => 'get',
      ),
    ),
  ),
  3445012937 => 
  array (
    'id' => 15508265,
    'name' => 'ООО "Прогресс-Обувь"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495799102,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094789,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094789',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3445012937',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094789,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094789',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15508265',
        'method' => 'get',
      ),
    ),
  ),
  3328481050 => 
  array (
    'id' => 15508279,
    'name' => 'ООО ДСК "Трасса"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1495799173,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30094803,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094803',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3328481050',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30094803,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30094803',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15508279',
        'method' => 'get',
      ),
    ),
  ),
  7203085179 => 
  array (
    'id' => 15519095,
    'name' => 'АО "ТЮМЕНЬТЕЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496035459,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30118687,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30118687',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203085179',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30118687,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30118687',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519095',
        'method' => 'get',
      ),
    ),
  ),
  7008006550 => 
  array (
    'id' => 15519231,
    'name' => 'ООО "Зональный комбикормовый завод"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496036118,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30118803,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30118803',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7008006550',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30118803,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30118803',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519231',
        'method' => 'get',
      ),
    ),
  ),
  5407492098 => 
  array (
    'id' => 15519401,
    'name' => 'ООО Аттракцион.Ру',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496037123,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30118947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30118947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407492098',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30118947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30118947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519401',
        'method' => 'get',
      ),
    ),
  ),
  2461030762 => 
  array (
    'id' => 15519609,
    'name' => 'ООО ЧОП "ФРЕГАТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496038758,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30119589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2461030762',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30119589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519609',
        'method' => 'get',
      ),
    ),
  ),
  2630042370 => 
  array (
    'id' => 15519635,
    'name' => 'ООО "МУП ЖКХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496038883,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30119613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2630042370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30119613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519635',
        'method' => 'get',
      ),
    ),
  ),
  2632078319 => 
  array (
    'id' => 15519659,
    'name' => 'ООО "Техресурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496038987,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30119639,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119639',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632078319',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30119639,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119639',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519659',
        'method' => 'get',
      ),
    ),
  ),
  2635819974 => 
  array (
    'id' => 15519935,
    'name' => 'ООО "ФОРТИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496040321,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30119967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635819974',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30119967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519935',
        'method' => 'get',
      ),
    ),
  ),
  3525347054 => 
  array (
    'id' => 15520421,
    'name' => 'ООО  СТРОИТЕЛЬНАЯ КОМПАНИЯ  ВОЛСПЕЦСТРОЙ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496042685,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30120505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525347054',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30120505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15520421',
        'method' => 'get',
      ),
    ),
  ),
  3702163266 => 
  array (
    'id' => 15520449,
    'name' => 'ООО "РИНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496042772,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30120539,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120539',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702163266',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30120539,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120539',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15520449',
        'method' => 'get',
      ),
    ),
  ),
  4707036104 => 
  array (
    'id' => 15520589,
    'name' => 'ООО "ГОРОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496043435,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30120705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4707036104',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30120705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15520589',
        'method' => 'get',
      ),
    ),
  ),
  4821048715 => 
  array (
    'id' => 15520697,
    'name' => 'ООО ИЛТЕХ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496043879,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30120861,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120861',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4821048715',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30120861,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30120861',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15520697',
        'method' => 'get',
      ),
    ),
  ),
  5034034543 => 
  array (
    'id' => 15521297,
    'name' => 'ООО "ГрандПроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496046157,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30121689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5034034543',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30121689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15521297',
        'method' => 'get',
      ),
    ),
  ),
  5047165021 => 
  array (
    'id' => 15521481,
    'name' => 'ООО "СК Метроном"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496046758,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30121865,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121865',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047165021',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30121865,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121865',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15521481',
        'method' => 'get',
      ),
    ),
  ),
  7107501997 => 
  array (
    'id' => 15521683,
    'name' => 'ООО ГРАНД-СТРОЙ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496047059,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30121957,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121957',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7107501997',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30121957,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121957',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15521683',
        'method' => 'get',
      ),
    ),
  ),
  7701947970 => 
  array (
    'id' => 15521695,
    'name' => 'ООО «СМК-строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496047157,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30121983,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121983',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701947970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30121983,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30121983',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15521695',
        'method' => 'get',
      ),
    ),
  ),
  7704619015 => 
  array (
    'id' => 15523125,
    'name' => 'ООО "МЕРОПРИЯТИЕ.РУ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496053011,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30123791,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30123791',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704619015',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30123791,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30123791',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15523125',
        'method' => 'get',
      ),
    ),
  ),
  7710449756 => 
  array (
    'id' => 15523187,
    'name' => 'ООО "ФАНУК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496053248,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30123867,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30123867',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710449756',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30123867,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30123867',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15523187',
        'method' => 'get',
      ),
    ),
  ),
  7714285308 => 
  array (
    'id' => 15523195,
    'name' => 'ООО "ГРЭС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496053327,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30123889,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30123889',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714285308',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30123889,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30123889',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15523195',
        'method' => 'get',
      ),
    ),
  ),
  7715145543 => 
  array (
    'id' => 15524203,
    'name' => 'ООО "Развитие"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496057810,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30125277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715145543',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30125277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15524203',
        'method' => 'get',
      ),
    ),
  ),
  7718926697 => 
  array (
    'id' => 15524387,
    'name' => 'ООО "Три Кита"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496058743,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30125493,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125493',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718926697',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30125493,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125493',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15524387',
        'method' => 'get',
      ),
    ),
  ),
  7719782416 => 
  array (
    'id' => 15524403,
    'name' => 'ООО «АЗГ-Групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496058802,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30125513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719782416',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30125513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15524403',
        'method' => 'get',
      ),
    ),
  ),
  7721481783 => 
  array (
    'id' => 15524427,
    'name' => 'ООО "УМНЫЙ ПОДРЯДЧИК""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496058886,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30125539,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125539',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721481783',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30125539,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30125539',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15524427',
        'method' => 'get',
      ),
    ),
  ),
  212007390 => 
  array (
    'id' => 15528949,
    'name' => 'ООО "Уральский Кадастровый Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496122388,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30130573,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30130573',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '212007390',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30130573,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30130573',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15528949',
        'method' => 'get',
      ),
    ),
  ),
  1841059220 => 
  array (
    'id' => 15529289,
    'name' => 'ООО "Агрострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496124692,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30130957,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30130957',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1841059220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30130957,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30130957',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15529289',
        'method' => 'get',
      ),
    ),
  ),
  2224158534 => 
  array (
    'id' => 15529505,
    'name' => 'ООО "Энергострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496125855,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30131239,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131239',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224158534',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30131239,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131239',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15529505',
        'method' => 'get',
      ),
    ),
  ),
  5948053267 => 
  array (
    'id' => 15529749,
    'name' => 'ООО "ЮГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496127010,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30131545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5948053267',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30131545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15529749',
        'method' => 'get',
      ),
    ),
  ),
  6319736011 => 
  array (
    'id' => 15529763,
    'name' => 'ООО "БЕРСО"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496127076,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30131567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319736011',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30131567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15529763',
        'method' => 'get',
      ),
    ),
  ),
  7203373882 => 
  array (
    'id' => 15529791,
    'name' => 'ООО ОРИОН',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496127209,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30131601,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131601',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203373882',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30131601,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131601',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15529791',
        'method' => 'get',
      ),
    ),
  ),
  8905052480 => 
  array (
    'id' => 15529841,
    'name' => 'НПО "НЗСМ", ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496127464,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30131677,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131677',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8905052480',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30131677,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131677',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15529841',
        'method' => 'get',
      ),
    ),
  ),
  7814489601 => 
  array (
    'id' => 15530049,
    'name' => 'ООО "Рико"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496128548,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30131977,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131977',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814489601',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30131977,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30131977',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15530049',
        'method' => 'get',
      ),
    ),
  ),
  7806508339 => 
  array (
    'id' => 15530197,
    'name' => 'ООО "Строительная Компания ПЕТЕРБУРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496129255,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30132157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30132157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806508339',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30132157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30132157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15530197',
        'method' => 'get',
      ),
    ),
  ),
  7805362165 => 
  array (
    'id' => 15530315,
    'name' => 'ООО «РА Медиасфера»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496129728,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30132339,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30132339',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805362165',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30132339,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30132339',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15530315',
        'method' => 'get',
      ),
    ),
  ),
  7734660606 => 
  array (
    'id' => 15531473,
    'name' => 'ООО «АЗИМУТ-ГЕО»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496133967,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30133631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30133631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734660606',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30133631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30133631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15531473',
        'method' => 'get',
      ),
    ),
  ),
  7734350900 => 
  array (
    'id' => 15534249,
    'name' => 'ООО "СИД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496142009,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30139211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30139211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734350900',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30139211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30139211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15534249',
        'method' => 'get',
      ),
    ),
  ),
  7724907156 => 
  array (
    'id' => 15534381,
    'name' => 'ООО "ЦПМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496142665,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30139375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30139375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724907156',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30139375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30139375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15534381',
        'method' => 'get',
      ),
    ),
  ),
  7720351943 => 
  array (
    'id' => 15534479,
    'name' => 'ООО "ДВИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496143154,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30139537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30139537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720351943',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30139537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30139537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15534479',
        'method' => 'get',
      ),
    ),
  ),
  6312169081 => 
  array (
    'id' => 15539515,
    'name' => 'ООО "Практик"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496207682,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30145337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312169081',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30145337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15539515',
        'method' => 'get',
      ),
    ),
  ),
  8602272655 => 
  array (
    'id' => 15539571,
    'name' => 'ООО «Сургутская ТопливоЭнергетическая Компания»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496208166,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30145409,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145409',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602272655',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30145409,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145409',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15539571',
        'method' => 'get',
      ),
    ),
  ),
  6646017200 => 
  array (
    'id' => 15539671,
    'name' => 'ООО "Седьмая строительная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496208981,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30145529,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145529',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6646017200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30145529,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145529',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15539671',
        'method' => 'get',
      ),
    ),
  ),
  1659173399 => 
  array (
    'id' => 15539731,
    'name' => 'ООО "АВАНГАРДЭНЕРГО""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496209890,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30145675,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145675',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659173399',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30145675,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145675',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15539731',
        'method' => 'get',
      ),
    ),
  ),
  2635062758 => 
  array (
    'id' => 15539773,
    'name' => 'ООО «Ростстройсервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496210178,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30145725,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145725',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635062758',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30145725,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145725',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15539773',
        'method' => 'get',
      ),
    ),
  ),
  2902044031 => 
  array (
    'id' => 15539793,
    'name' => 'ООО "КТА.ЛЕС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496210348,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30145765,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145765',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2902044031',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30145765,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30145765',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15539793',
        'method' => 'get',
      ),
    ),
  ),
  3662111822 => 
  array (
    'id' => 15540231,
    'name' => 'ООО "ИСТОЧНИК ЗДОРОВЬЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496213363,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30146311,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146311',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662111822',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30146311,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146311',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15540231',
        'method' => 'get',
      ),
    ),
  ),
  4825055865 => 
  array (
    'id' => 15540283,
    'name' => 'ООО "ЭЛВИ-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496213697,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30146391,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146391',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825055865',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30146391,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146391',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15540283',
        'method' => 'get',
      ),
    ),
  ),
  5005054200 => 
  array (
    'id' => 15540423,
    'name' => 'ООО "ИНТЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496214524,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30146583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5005054200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30146583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15540423',
        'method' => 'get',
      ),
    ),
  ),
  5024165579 => 
  array (
    'id' => 15540697,
    'name' => 'ООО «СЕРВИСНО-ТЕХНИЧЕСКИЙ ЦЕНТР УРАГАН»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496215845,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30146947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024165579',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30146947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15540697',
        'method' => 'get',
      ),
    ),
  ),
  5038033597 => 
  array (
    'id' => 15540849,
    'name' => 'ООО "ДОРПРОМ интернациональ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496216525,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30147145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5038033597',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30147145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15540849',
        'method' => 'get',
      ),
    ),
  ),
  5047126865 => 
  array (
    'id' => 15540903,
    'name' => 'ООО «РосТехПроект»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496216715,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30147195,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147195',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047126865',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30147195,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147195',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15540903',
        'method' => 'get',
      ),
    ),
  ),
  5245021215 => 
  array (
    'id' => 15541359,
    'name' => 'ооо "БОГОРОДСКОЕ КОЖГАЛАНТЕРЕЙНОЕ ПРЕДПРИЯТИЕ "РАДУГА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496218143,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30147773,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147773',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5245021215',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30147773,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147773',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15541359',
        'method' => 'get',
      ),
    ),
  ),
  5260214966 => 
  array (
    'id' => 15541483,
    'name' => 'ООО  "КАПИТАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496218275,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30147811,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147811',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5260214966',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30147811,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147811',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15541483',
        'method' => 'get',
      ),
    ),
  ),
  6163123948 => 
  array (
    'id' => 15543099,
    'name' => 'ООО "РОСТОВСКИЙ СТРОИТЕЛЬНЫЙ ТРЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496225811,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30150179,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30150179',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6163123948',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30150179,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30150179',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15543099',
        'method' => 'get',
      ),
    ),
  ),
  6674131089 => 
  array (
    'id' => 15543123,
    'name' => 'ООО "ГАЗ ЭНЖИНИРИНГ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496225949,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30150215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30150215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6674131089',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30150215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30150215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15543123',
        'method' => 'get',
      ),
    ),
  ),
  7703817261 => 
  array (
    'id' => 15543939,
    'name' => 'ООО "ИНВЕСТ ИНЖИНИРИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496229379,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30151333,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30151333',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703817261',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30151333,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30151333',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15543939',
        'method' => 'get',
      ),
    ),
  ),
  7707339753 => 
  array (
    'id' => 15544013,
    'name' => 'ООО ПОЛЮС ХИМ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496229834,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30151463,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30151463',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707339753',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30151463,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30151463',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15544013',
        'method' => 'get',
      ),
    ),
  ),
  7720298390 => 
  array (
    'id' => 15544109,
    'name' => 'ООО «Арманс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496230336,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30151611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30151611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720298390',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30151611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30151611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15544109',
        'method' => 'get',
      ),
    ),
  ),
  7743927221 => 
  array (
    'id' => 15544571,
    'name' => 'ООО "Глобальные Системы"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496232762,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30152259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30152259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743927221',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30152259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30152259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15544571',
        'method' => 'get',
      ),
    ),
  ),
  250014447 => 
  array (
    'id' => 15548987,
    'name' => 'ООО "АЛЬЯНС СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496296509,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30158185,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158185',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '250014447',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30158185,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158185',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15548987',
        'method' => 'get',
      ),
    ),
  ),
  808001608 => 
  array (
    'id' => 15549003,
    'name' => 'ООО "Монтажник"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496296654,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30158201,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158201',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '808001608',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30158201,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158201',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549003',
        'method' => 'get',
      ),
    ),
  ),
  1215118928 => 
  array (
    'id' => 15549027,
    'name' => 'ООО проектно-строительная кампания "Строй Дом"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496296880,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30158243,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158243',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1215118928',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30158243,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158243',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549027',
        'method' => 'get',
      ),
    ),
  ),
  2127314869 => 
  array (
    'id' => 15549061,
    'name' => 'ООО "АЛЕФ-СЕРВИС""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496297110,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30158281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382025,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2127314869',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30158281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549061',
        'method' => 'get',
      ),
    ),
  ),
  2130111555 => 
  array (
    'id' => 15549211,
    'name' => '"ООО ""ПСК ""Империя"""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496298257,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30158481,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158481',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130111555',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30158481,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158481',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549211',
        'method' => 'get',
      ),
    ),
  ),
  3426008759 => 
  array (
    'id' => 15549377,
    'name' => 'ООО "УНИВЕРСАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496299338,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30158697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3426008759',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30158697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549377',
        'method' => 'get',
      ),
    ),
  ),
  3525233515 => 
  array (
    'id' => 15549405,
    'name' => 'ООО "СТРОЙ ЦЕНТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496299499,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30158725,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158725',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525233515',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30158725,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30158725',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549405',
        'method' => 'get',
      ),
    ),
  ),
  4102007860 => 
  array (
    'id' => 15549663,
    'name' => 'АО "Судоремсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496301168,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30159029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4102007860',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30159029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549663',
        'method' => 'get',
      ),
    ),
  ),
  4825029248 => 
  array (
    'id' => 15549695,
    'name' => 'ООО ЛИПЕЦКГЛАВСНАБ "',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496301263,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30159061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825029248',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30159061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549695',
        'method' => 'get',
      ),
    ),
  ),
  5040118745 => 
  array (
    'id' => 15549745,
    'name' => 'ООО «КАМТЭС»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496301552,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30159125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040118745',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30159125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549745',
        'method' => 'get',
      ),
    ),
  ),
  5047094980 => 
  array (
    'id' => 15549757,
    'name' => 'ООО "Промышленные и специальные материалы"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496301627,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30159145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047094980',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30159145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549757',
        'method' => 'get',
      ),
    ),
  ),
  5832001502 => 
  array (
    'id' => 15549839,
    'name' => 'ООО "СтройПлюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496302161,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30159287,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159287',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5832001502',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30159287,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159287',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549839',
        'method' => 'get',
      ),
    ),
  ),
  6230088096 => 
  array (
    'id' => 15549883,
    'name' => 'ООО «МЕГАЛИТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496302387,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30159351,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159351',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6230088096',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30159351,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159351',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549883',
        'method' => 'get',
      ),
    ),
  ),
  6319187298 => 
  array (
    'id' => 15549897,
    'name' => 'ООО "Медтехлинк-Самара"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496302475,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30159367,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159367',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319187298',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30159367,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30159367',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15549897',
        'method' => 'get',
      ),
    ),
  ),
  7702267086 => 
  array (
    'id' => 15550419,
    'name' => 'ООО «РПЦ Офорт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496305228,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30160125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30160125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702267086',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30160125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30160125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15550419',
        'method' => 'get',
      ),
    ),
  ),
  7706752380 => 
  array (
    'id' => 15550497,
    'name' => 'ООО "Эколайн-Техно"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496305506,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30160219,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30160219',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706752380',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30160219,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30160219',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15550497',
        'method' => 'get',
      ),
    ),
  ),
  7715515265 => 
  array (
    'id' => 15550513,
    'name' => 'ООО  "НОТАМЕДИА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496305670,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30160255,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30160255',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715515265',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30160255,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30160255',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15550513',
        'method' => 'get',
      ),
    ),
  ),
  7715899653 => 
  array (
    'id' => 15551431,
    'name' => 'ООО "БелСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496308744,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30161649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30161649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715899653',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30161649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30161649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15551431',
        'method' => 'get',
      ),
    ),
  ),
  7751010051 => 
  array (
    'id' => 15551597,
    'name' => 'ООО "СТРОЙСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496309628,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30162081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751010051',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30162081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15551597',
        'method' => 'get',
      ),
    ),
  ),
  7810624521 => 
  array (
    'id' => 15551623,
    'name' => 'ООО "Строительная Компания "СтройСити"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496309722,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30162121,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162121',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810624521',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30162121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15551623',
        'method' => 'get',
      ),
    ),
  ),
  9102199053 => 
  array (
    'id' => 15551637,
    'name' => 'ООО НАШ ОРГАЛИТ-ПЛЮС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496309822,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30162143,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162143',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102199053',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30162143,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162143',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15551637',
        'method' => 'get',
      ),
    ),
  ),
  4027132114 => 
  array (
    'id' => 15552025,
    'name' => 'ООО "Калужская Строительная Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496311361,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30162535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4027132114',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30162535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30162535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552025',
        'method' => 'get',
      ),
    ),
  ),
  3123393491 => 
  array (
    'id' => 15552415,
    'name' => 'ООО С-Строй',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496313380,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163063,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163063',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123393491',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163063,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163063',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552415',
        'method' => 'get',
      ),
    ),
  ),
  7813227434 => 
  array (
    'id' => 15552431,
    'name' => 'ООО "Гефест"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496313455,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813227434',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552431',
        'method' => 'get',
      ),
    ),
  ),
  4345412330 => 
  array (
    'id' => 15552459,
    'name' => 'ООО "ГАС-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496313566,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163117,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163117',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345412330',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163117,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163117',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552459',
        'method' => 'get',
      ),
    ),
  ),
  7743183131 => 
  array (
    'id' => 15552509,
    'name' => 'ооо «ИНЖИСТРОЙ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496313754,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163169,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163169',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743183131',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163169,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163169',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552509',
        'method' => 'get',
      ),
    ),
  ),
  3327131374 => 
  array (
    'id' => 15552613,
    'name' => 'ООО «ДорСтрой 33»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496314199,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3327131374',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552613',
        'method' => 'get',
      ),
    ),
  ),
  7701814226 => 
  array (
    'id' => 15552627,
    'name' => 'ООО "ЛЕГИС-ГАРД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496314270,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163297,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163297',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701814226',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163297,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163297',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552627',
        'method' => 'get',
      ),
    ),
  ),
  5256148950 => 
  array (
    'id' => 15552837,
    'name' => 'ООО "ЛИДЕР-Н"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496315459,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5256148950',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552837',
        'method' => 'get',
      ),
    ),
  ),
  6027176884 => 
  array (
    'id' => 15552873,
    'name' => 'ООО "ВОЛШЕБНЫЙ ГОРОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496315569,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30163565,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163565',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6027176884',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30163565,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30163565',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15552873',
        'method' => 'get',
      ),
    ),
  ),
  5902041861 => 
  array (
    'id' => 15581449,
    'name' => 'ООО РумДан',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496381719,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30209145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30209145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902041861',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30209145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30209145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15581449',
        'method' => 'get',
      ),
    ),
  ),
  5906088740 => 
  array (
    'id' => 15581481,
    'name' => 'ООО "УралДорПроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496382047,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30219557,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30219557',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5906088740',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30219557,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30219557',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15581481',
        'method' => 'get',
      ),
    ),
  ),
  6311152624 => 
  array (
    'id' => 15581495,
    'name' => 'ООО "ЭКСПОКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496382187,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30219635,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30219635',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6311152624',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30219635,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30219635',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15581495',
        'method' => 'get',
      ),
    ),
  ),
  7813224803 => 
  array (
    'id' => 15582153,
    'name' => 'ООО «ПРОИЗВОДСТВО СПАСАТЕЛЬНОГО СНАРЯЖЕНИЯ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496386936,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30220431,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220431',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813224803',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30220431,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220431',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582153',
        'method' => 'get',
      ),
    ),
  ),
  7810897744 => 
  array (
    'id' => 15582181,
    'name' => 'ООО "ТехноМедГрупп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496387100,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30220475,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220475',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810897744',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30220475,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220475',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582181',
        'method' => 'get',
      ),
    ),
  ),
  7802865611 => 
  array (
    'id' => 15582381,
    'name' => 'ООО «Строительная Компания «СОКОЛ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496388138,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30220715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802865611',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30220715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582381',
        'method' => 'get',
      ),
    ),
  ),
  7751526657 => 
  array (
    'id' => 15582411,
    'name' => 'ООО  "АМБРЕЛЛА-ЗАЩИТА ИНФОРМАЦИОННЫХ СИСТЕМ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496388339,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30220775,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220775',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751526657',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30220775,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220775',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582411',
        'method' => 'get',
      ),
    ),
  ),
  7734677920 => 
  array (
    'id' => 15582533,
    'name' => 'ООО «ЭнергоГранд»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496388923,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30220907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734677920',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30220907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582533',
        'method' => 'get',
      ),
    ),
  ),
  7734398130 => 
  array (
    'id' => 15582575,
    'name' => 'ООО «БизнесРесурс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496389158,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30220957,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220957',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734398130',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30220957,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30220957',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582575',
        'method' => 'get',
      ),
    ),
  ),
  7733028923 => 
  array (
    'id' => 15582597,
    'name' => 'ОАО "Московский комбинат строительных материалов"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496389348,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30221023,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221023',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733028923',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30221023,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221023',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582597',
        'method' => 'get',
      ),
    ),
  ),
  7726353584 => 
  array (
    'id' => 15582951,
    'name' => 'ООО  «Медицинское оборудование - М»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496390504,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30221353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726353584',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30221353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15582951',
        'method' => 'get',
      ),
    ),
  ),
  7720345900 => 
  array (
    'id' => 15583349,
    'name' => 'ООО "ЭДУСПОРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496392078,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30221827,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221827',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720345900',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30221827,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221827',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15583349',
        'method' => 'get',
      ),
    ),
  ),
  7718847491 => 
  array (
    'id' => 15583359,
    'name' => 'ООО  "ФОРТЕ-ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496392169,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30221843,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221843',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718847491',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30221843,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30221843',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15583359',
        'method' => 'get',
      ),
    ),
  ),
  7709391156 => 
  array (
    'id' => 15583541,
    'name' => 'ООО "АУДИТЛЭНД-ОЦЕНКА И СЕРТИФИКАЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496392942,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30222061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30222061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709391156',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30222061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30222061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15583541',
        'method' => 'get',
      ),
    ),
  ),
  7701005761 => 
  array (
    'id' => 15584091,
    'name' => 'ООО фирма "ПУЛЬС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496395470,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30222723,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30222723',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701005761',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30222723,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30222723',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15584091',
        'method' => 'get',
      ),
    ),
  ),
  7603063596 => 
  array (
    'id' => 15584117,
    'name' => 'ООО "ПАЛЛАДА РЕСУРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496395606,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30222753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30222753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7603063596',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30222753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30222753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15584117',
        'method' => 'get',
      ),
    ),
  ),
  7328080610 => 
  array (
    'id' => 15584337,
    'name' => 'ООО "УлПрофиль"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496396611,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30223041,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223041',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7328080610',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30223041,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223041',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15584337',
        'method' => 'get',
      ),
    ),
  ),
  7103504690 => 
  array (
    'id' => 15584369,
    'name' => 'ООО "СТРОЙТЕХТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496396743,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30223081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7103504690',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30223081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15584369',
        'method' => 'get',
      ),
    ),
  ),
  3662189924 => 
  array (
    'id' => 15584767,
    'name' => 'ООО "Феникс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496398738,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30223545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662189924',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30223545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15584767',
        'method' => 'get',
      ),
    ),
  ),
  2130129680 => 
  array (
    'id' => 15584805,
    'name' => 'ООО "ГСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496398935,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30223601,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223601',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130129680',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30223601,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30223601',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15584805',
        'method' => 'get',
      ),
    ),
  ),
  7723478673 => 
  array (
    'id' => 15585225,
    'name' => 'ООО  "Элит-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496401139,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30224209,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30224209',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723478673',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30224209,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30224209',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15585225',
        'method' => 'get',
      ),
    ),
  ),
  7459004789 => 
  array (
    'id' => 15596727,
    'name' => 'ООО "ИНТЕРЛОГИСТИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496640202,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236129,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236129',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7459004789',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236129,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236129',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15596727',
        'method' => 'get',
      ),
    ),
  ),
  6732117248 => 
  array (
    'id' => 15596743,
    'name' => 'ООО «БУДГУД»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496640430,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236149,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236149',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6732117248',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236149,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236149',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15596743',
        'method' => 'get',
      ),
    ),
  ),
  6672309104 => 
  array (
    'id' => 15596785,
    'name' => 'ООО «Сольоптторг»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496640729,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236185,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236185',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672309104',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236185,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236185',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15596785',
        'method' => 'get',
      ),
    ),
  ),
  5406575369 => 
  array (
    'id' => 15596839,
    'name' => 'ООО "АДЭЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496641277,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236245,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236245',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406575369',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236245,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236245',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15596839',
        'method' => 'get',
      ),
    ),
  ),
  1840048169 => 
  array (
    'id' => 15596855,
    'name' => 'ООО Строительная компания "ЭнергоЭксперт"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496641448,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1840048169',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236263,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236263',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15596855',
        'method' => 'get',
      ),
    ),
  ),
  278914092 => 
  array (
    'id' => 15597011,
    'name' => 'ООО «Строй-Эколог»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496642821,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236497,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236497',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278914092',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236497,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236497',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597011',
        'method' => 'get',
      ),
    ),
  ),
  6317091150 => 
  array (
    'id' => 15597035,
    'name' => 'ООО «Стройнефть»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496643087,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236577,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236577',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317091150',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236577,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236577',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597035',
        'method' => 'get',
      ),
    ),
  ),
  9718039120 => 
  array (
    'id' => 15597111,
    'name' => 'ООО СК "ПолиСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496643594,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236681,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236681',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718039120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236681,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236681',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597111',
        'method' => 'get',
      ),
    ),
  ),
  9715294281 => 
  array (
    'id' => 15597119,
    'name' => 'ООО "КЛЕВЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496643676,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715294281',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597119',
        'method' => 'get',
      ),
    ),
  ),
  9705075682 => 
  array (
    'id' => 15597125,
    'name' => 'ООО "МеталлМаш',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496643704,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705075682',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597125',
        'method' => 'get',
      ),
    ),
  ),
  7841427964 => 
  array (
    'id' => 15597263,
    'name' => 'ООО  "МИРО ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496644355,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236895,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236895',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841427964',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236895,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236895',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597263',
        'method' => 'get',
      ),
    ),
  ),
  7817044819 => 
  array (
    'id' => 15597329,
    'name' => 'ООО "Строительная Компания Балт-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496644689,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7817044819',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597329',
        'method' => 'get',
      ),
    ),
  ),
  7816182206 => 
  array (
    'id' => 15597577,
    'name' => 'ООО "Комплекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496646036,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30237283,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237283',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816182206',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30237283,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237283',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597577',
        'method' => 'get',
      ),
    ),
  ),
  7813564694 => 
  array (
    'id' => 15597757,
    'name' => 'ООО "СКН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496647009,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30237537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813564694',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30237537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597757',
        'method' => 'get',
      ),
    ),
  ),
  7811469780 => 
  array (
    'id' => 15597883,
    'name' => 'ООО СтройКом',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496647698,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30237705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811469780',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30237705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597883',
        'method' => 'get',
      ),
    ),
  ),
  7801532240 => 
  array (
    'id' => 15597949,
    'name' => 'ЗАО "ТЕРЕС-1Т"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496648001,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30237881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801532240',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30237881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30237881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15597949',
        'method' => 'get',
      ),
    ),
  ),
  7751023685 => 
  array (
    'id' => 15598169,
    'name' => 'ООО СК "Дионис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496649044,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30238175,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238175',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751023685',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30238175,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238175',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15598169',
        'method' => 'get',
      ),
    ),
  ),
  7729755926 => 
  array (
    'id' => 15598511,
    'name' => 'ООО "Строй-Альянс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496650553,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30238659,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238659',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729755926',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30238659,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238659',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15598511',
        'method' => 'get',
      ),
    ),
  ),
  7729731940 => 
  array (
    'id' => 15598547,
    'name' => 'ООО «Гранд Фамили Групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496650672,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30238703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729731940',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30238703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15598547',
        'method' => 'get',
      ),
    ),
  ),
  7726751948 => 
  array (
    'id' => 15598699,
    'name' => 'ООО "ДРАЙВЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496651238,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30238867,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238867',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726751948',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30238867,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238867',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15598699',
        'method' => 'get',
      ),
    ),
  ),
  7724910102 => 
  array (
    'id' => 15598769,
    'name' => 'ООО "СТИНН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496651488,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30238957,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238957',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724910102',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30238957,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30238957',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15598769',
        'method' => 'get',
      ),
    ),
  ),
  7719869280 => 
  array (
    'id' => 15599963,
    'name' => 'ООО "ОЭРБИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496656605,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30240481,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30240481',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719869280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30240481,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30240481',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15599963',
        'method' => 'get',
      ),
    ),
  ),
  7719637384 => 
  array (
    'id' => 15600083,
    'name' => 'ООО "ВИРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496656989,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30240621,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30240621',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719637384',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30240621,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30240621',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15600083',
        'method' => 'get',
      ),
    ),
  ),
  7703661293 => 
  array (
    'id' => 15600339,
    'name' => 'ООО ПСК «Промжилстройинвест»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496657890,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30240881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30240881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703661293',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30240881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30240881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15600339',
        'method' => 'get',
      ),
    ),
  ),
  5027247808 => 
  array (
    'id' => 15600519,
    'name' => 'ООО "ПК БЭБИ ЛАЙФ""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496658707,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30241145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30241145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027247808',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30241145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30241145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15600519',
        'method' => 'get',
      ),
    ),
  ),
  5009078000 => 
  array (
    'id' => 15601169,
    'name' => 'ООО "Медикал Девелопмент Эдженси"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496661359,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30242147,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30242147',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5009078000',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30242147,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30242147',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15601169',
        'method' => 'get',
      ),
    ),
  ),
  7722689833 => 
  array (
    'id' => 15602325,
    'name' => 'ЗАО Трест" Электрощитмонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496666897,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30243869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30243869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722689833',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30243869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30243869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15602325',
        'method' => 'get',
      ),
    ),
  ),
  3301033245 => 
  array (
    'id' => 15602409,
    'name' => 'ООО "ИНВЕСТИЦИОННО-СТРОИТЕЛЬНАЯ КОМПАНИЯ "ЭВРИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496667338,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30243969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30243969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3301033245',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30243969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30243969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15602409',
        'method' => 'get',
      ),
    ),
  ),
  7816480837 => 
  array (
    'id' => 15605957,
    'name' => 'ООО "РемСтрой Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496726968,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30248049,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248049',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816480837',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30248049,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248049',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15605957',
        'method' => 'get',
      ),
    ),
  ),
  7415097289 => 
  array (
    'id' => 15605975,
    'name' => 'ООО "СФЕРА-МЕТАЛЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496727142,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30248073,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248073',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7415097289',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30248073,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248073',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15605975',
        'method' => 'get',
      ),
    ),
  ),
  5906140077 => 
  array (
    'id' => 15606059,
    'name' => 'ООО "ЗападУралИнжиниринг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496727958,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30248209,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248209',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5906140077',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30248209,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248209',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15606059',
        'method' => 'get',
      ),
    ),
  ),
  411156533 => 
  array (
    'id' => 15606071,
    'name' => 'ООО "Дирекция капитального строительства - 1"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496728098,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30248261,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248261',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '411156533',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30248261,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248261',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15606071',
        'method' => 'get',
      ),
    ),
  ),
  7801200558 => 
  array (
    'id' => 15606185,
    'name' => 'ЗАО Балтийская Промышленная Компания',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496729225,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30248431,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248431',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801200558',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30248431,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30248431',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15606185',
        'method' => 'get',
      ),
    ),
  ),
  9701043418 => 
  array (
    'id' => 15607063,
    'name' => 'ООО  "САНРЭМ ЭКСПЛУАТАЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496734119,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30249651,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249651',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701043418',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30249651,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249651',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15607063',
        'method' => 'get',
      ),
    ),
  ),
  7842421482 => 
  array (
    'id' => 15607095,
    'name' => 'ООО "СК "Мегастрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496734257,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30249687,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249687',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842421482',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30249687,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249687',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15607095',
        'method' => 'get',
      ),
    ),
  ),
  7840504990 => 
  array (
    'id' => 15607327,
    'name' => 'ООО  "РЕСТКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496735394,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30249979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840504990',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30249979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30249979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15607327',
        'method' => 'get',
      ),
    ),
  ),
  7840394667 => 
  array (
    'id' => 15607363,
    'name' => 'ООО «Про Декор»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496735507,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30250017,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30250017',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840394667',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30250017,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30250017',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15607363',
        'method' => 'get',
      ),
    ),
  ),
  7814638451 => 
  array (
    'id' => 15607783,
    'name' => 'ООО "Стройимпульс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496736824,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30250385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30250385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814638451',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30250385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30250385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15607783',
        'method' => 'get',
      ),
    ),
  ),
  7802196488 => 
  array (
    'id' => 15608719,
    'name' => 'ООО НОРД МЭЙНТЕНЕНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496741361,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30251763,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251763',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802196488',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30251763,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251763',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15608719',
        'method' => 'get',
      ),
    ),
  ),
  7802142669 => 
  array (
    'id' => 15608743,
    'name' => 'ООО  Транс-Груп Санкт-Петербург',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496741471,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30251795,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251795',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802142669',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30251795,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251795',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15608743',
        'method' => 'get',
      ),
    ),
  ),
  7751034180 => 
  array (
    'id' => 15608771,
    'name' => 'ООО "ТЕХЖИЛКОМСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496741655,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30251853,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251853',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751034180',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30251853,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251853',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15608771',
        'method' => 'get',
      ),
    ),
  ),
  7734528319 => 
  array (
    'id' => 15608805,
    'name' => 'ООО  "СанТрест"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496741836,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30251895,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251895',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734528319',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30251895,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30251895',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15608805',
        'method' => 'get',
      ),
    ),
  ),
  7728692374 => 
  array (
    'id' => 15609007,
    'name' => 'ООО «СКГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496742475,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30252089,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252089',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728692374',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30252089,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252089',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609007',
        'method' => 'get',
      ),
    ),
  ),
  7728270887 => 
  array (
    'id' => 15609079,
    'name' => 'ООО ЧОО "РУБЕЖ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496742832,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30252195,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252195',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728270887',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30252195,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252195',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609079',
        'method' => 'get',
      ),
    ),
  ),
  7727845902 => 
  array (
    'id' => 15609217,
    'name' => 'ООО "СОВРЕМЕННЫЙ АРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496743457,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30252369,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252369',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727845902',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30252369,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252369',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609217',
        'method' => 'get',
      ),
    ),
  ),
  7722792020 => 
  array (
    'id' => 15609321,
    'name' => 'ООО "СВМ ЭКСПЕРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496743811,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30252475,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252475',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722792020',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30252475,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252475',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609321',
        'method' => 'get',
      ),
    ),
  ),
  7721405817 => 
  array (
    'id' => 15609349,
    'name' => 'ООО  «МЕРИДИАН»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496743964,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30252515,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252515',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721405817',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30252515,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252515',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609349',
        'method' => 'get',
      ),
    ),
  ),
  7716768893 => 
  array (
    'id' => 15609519,
    'name' => 'ООО "ИСКРА СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496744736,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30252733,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252733',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716768893',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30252733,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252733',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609519',
        'method' => 'get',
      ),
    ),
  ),
  7714924522 => 
  array (
    'id' => 15609549,
    'name' => 'ООО  "ПРО-ВИЖЕН КОММУНИКЕЙШНЗ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496744847,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30252777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714924522',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30252777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30252777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609549',
        'method' => 'get',
      ),
    ),
  ),
  7713769708 => 
  array (
    'id' => 15609791,
    'name' => 'ООО "ДАВИНЧИ КОМПАНИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496745753,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30253097,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253097',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713769708',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30253097,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253097',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609791',
        'method' => 'get',
      ),
    ),
  ),
  7707331497 => 
  array (
    'id' => 15609813,
    'name' => 'ООО АРТТАСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496745866,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30253119,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253119',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707331497',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30253119,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253119',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609813',
        'method' => 'get',
      ),
    ),
  ),
  7706804327 => 
  array (
    'id' => 15609947,
    'name' => 'ООО "ЭКО РЕСУРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496746552,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30253331,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253331',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706804327',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30253331,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253331',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609947',
        'method' => 'get',
      ),
    ),
  ),
  7703795586 => 
  array (
    'id' => 15609977,
    'name' => 'ООО "ДЕЛЕГАТ-СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496746684,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30253369,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253369',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703795586',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30253369,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253369',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15609977',
        'method' => 'get',
      ),
    ),
  ),
  6731061761 => 
  array (
    'id' => 15610011,
    'name' => 'ООО "ЭНЕРГИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496746815,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30253407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6731061761',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30253407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30253407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15610011',
        'method' => 'get',
      ),
    ),
  ),
  8603000799 => 
  array (
    'id' => 15614641,
    'name' => 'ОАО "Строительное управление № 909"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496813991,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259069,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259069',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603000799',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259069,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259069',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15614641',
        'method' => 'get',
      ),
    ),
  ),
  7448147111 => 
  array (
    'id' => 15614685,
    'name' => 'ООО  "МадисСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496814326,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448147111',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15614685',
        'method' => 'get',
      ),
    ),
  ),
  6662079275 => 
  array (
    'id' => 15614747,
    'name' => 'ООО "Деловой Урал-ЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496815022,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259167,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259167',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6662079275',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259167,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259167',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15614747',
        'method' => 'get',
      ),
    ),
  ),
  6315609164 => 
  array (
    'id' => 15614843,
    'name' => 'ООО «Строительно-монтажное предприятие-Волгастрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496815675,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500616045,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6315609164',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15614843',
        'method' => 'get',
      ),
    ),
  ),
  5507214778 => 
  array (
    'id' => 15614887,
    'name' => 'ООО «Велес и К»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496816131,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259349,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259349',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5507214778',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259349,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259349',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15614887',
        'method' => 'get',
      ),
    ),
  ),
  2225147366 => 
  array (
    'id' => 15614923,
    'name' => 'ООО "РОСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496816300,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259399,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259399',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225147366',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259399,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259399',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15614923',
        'method' => 'get',
      ),
    ),
  ),
  278199910 => 
  array (
    'id' => 15615133,
    'name' => 'ООО "Кристалл"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496817932,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259701,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259701',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278199910',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259701,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259701',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615133',
        'method' => 'get',
      ),
    ),
  ),
  258014086 => 
  array (
    'id' => 15615167,
    'name' => 'ООО "Строительно-монтажное управление № 8"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496818103,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259745,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259745',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '258014086',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259745,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259745',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615167',
        'method' => 'get',
      ),
    ),
  ),
  9701061093 => 
  array (
    'id' => 15615249,
    'name' => 'ООО  "МК-ПОСТАВКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496818675,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701061093',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615249',
        'method' => 'get',
      ),
    ),
  ),
  7839419885 => 
  array (
    'id' => 15615269,
    'name' => 'ООО "Автодорстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496818813,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259891,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259891',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839419885',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259891,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259891',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615269',
        'method' => 'get',
      ),
    ),
  ),
  7814427323 => 
  array (
    'id' => 15615333,
    'name' => 'ООО "СПЕКТРЭНЕРГО"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496819232,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30259997,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259997',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814427323',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30259997,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30259997',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615333',
        'method' => 'get',
      ),
    ),
  ),
  7807150688 => 
  array (
    'id' => 15615363,
    'name' => 'ООО "ПетроСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496819407,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30260031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807150688',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30260031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615363',
        'method' => 'get',
      ),
    ),
  ),
  7736257632 => 
  array (
    'id' => 15615441,
    'name' => 'ООО «ВЕСАРТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496819848,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30260199,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260199',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736257632',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30260199,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260199',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615441',
        'method' => 'get',
      ),
    ),
  ),
  7733791853 => 
  array (
    'id' => 15615483,
    'name' => 'ООО «НК Агат»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496820046,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30260255,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260255',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733791853',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30260255,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260255',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615483',
        'method' => 'get',
      ),
    ),
  ),
  7728320464 => 
  array (
    'id' => 15615673,
    'name' => 'БЛАГОТВОРИТЕЛЬНЫЙ ФОНД ПОМОЩИ ДЕТЯМ "ЗДРАВСТВУЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496821024,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30260511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728320464',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30260511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615673',
        'method' => 'get',
      ),
    ),
  ),
  7725822603 => 
  array (
    'id' => 15615697,
    'name' => 'ООО "ТехАльянс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496821158,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30260553,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260553',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725822603',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30260553,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260553',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615697',
        'method' => 'get',
      ),
    ),
  ),
  7725325418 => 
  array (
    'id' => 15615831,
    'name' => 'ООО «СКП»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496821847,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30260749,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260749',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725325418',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30260749,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260749',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615831',
        'method' => 'get',
      ),
    ),
  ),
  7723877727 => 
  array (
    'id' => 15615943,
    'name' => 'ООО "Патриот"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496822299,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30260879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723877727',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30260879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30260879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15615943',
        'method' => 'get',
      ),
    ),
  ),
  7721340077 => 
  array (
    'id' => 15617753,
    'name' => 'ООО «Микролинк - cвязь»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496829626,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30263091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721340077',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30263091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15617753',
        'method' => 'get',
      ),
    ),
  ),
  7717667009 => 
  array (
    'id' => 15617851,
    'name' => 'ООО «Радиотехника»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496830064,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30263203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717667009',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30263203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15617851',
        'method' => 'get',
      ),
    ),
  ),
  7702771832 => 
  array (
    'id' => 15618317,
    'name' => 'ООО ТехноАльянс',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496831986,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30263705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702771832',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30263705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15618317',
        'method' => 'get',
      ),
    ),
  ),
  7702746561 => 
  array (
    'id' => 15618345,
    'name' => 'ООО "ИННОВАЦИОННЫЕ ПРОЕКТЫ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496832068,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30263739,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263739',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702746561',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30263739,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263739',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15618345',
        'method' => 'get',
      ),
    ),
  ),
  7701376935 => 
  array (
    'id' => 15618379,
    'name' => 'ООО "ПРОМСТРОЙИНВЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496832160,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30263777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701376935',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30263777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15618379',
        'method' => 'get',
      ),
    ),
  ),
  6670325693 => 
  array (
    'id' => 15618409,
    'name' => 'ООО "АЛДИ""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496832278,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30263805,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263805',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670325693',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30263805,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263805',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15618409',
        'method' => 'get',
      ),
    ),
  ),
  5905256449 => 
  array (
    'id' => 15618429,
    'name' => 'ООО «Интеллект-Ресурс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496832376,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30263825,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263825',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905256449',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30263825,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30263825',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15618429',
        'method' => 'get',
      ),
    ),
  ),
  7801480425 => 
  array (
    'id' => 15618891,
    'name' => 'ОАО «Технический надзор»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496834020,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30264375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30264375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801480425',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30264375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30264375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15618891',
        'method' => 'get',
      ),
    ),
  ),
  7714800774 => 
  array (
    'id' => 15619671,
    'name' => 'ЗАО "ТЕХНОСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496837906,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30265455,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30265455',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714800774',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30265455,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30265455',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15619671',
        'method' => 'get',
      ),
    ),
  ),
  7703755400 => 
  array (
    'id' => 15619751,
    'name' => 'ЗАО НПП «ФАРММЕДПРОМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496838344,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30265547,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30265547',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703755400',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30265547,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30265547',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15619751',
        'method' => 'get',
      ),
    ),
  ),
  5904341525 => 
  array (
    'id' => 15623647,
    'name' => 'ООО "Конкурент"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496899510,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30269669,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30269669',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904341525',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30269669,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30269669',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15623647',
        'method' => 'get',
      ),
    ),
  ),
  7842419349 => 
  array (
    'id' => 15623917,
    'name' => 'ООО ЧОО "АРЕС СЕКЬЮРИТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496902257,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30270001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842419349',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30270001,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270001',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15623917',
        'method' => 'get',
      ),
    ),
  ),
  7842113953 => 
  array (
    'id' => 15623935,
    'name' => 'ООО Эрвис',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496902367,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30270021,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270021',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842113953',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30270021,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270021',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15623935',
        'method' => 'get',
      ),
    ),
  ),
  7838043319 => 
  array (
    'id' => 15623965,
    'name' => 'ООО "Прогресс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496902753,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30270115,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270115',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838043319',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30270115,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270115',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15623965',
        'method' => 'get',
      ),
    ),
  ),
  7813365610 => 
  array (
    'id' => 15624279,
    'name' => 'ООО «Производственно-судоремонтная компания «Ладога»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496904696,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30270487,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270487',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813365610',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30270487,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270487',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15624279',
        'method' => 'get',
      ),
    ),
  ),
  7811358706 => 
  array (
    'id' => 15624417,
    'name' => 'ООО НПМ "НИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496905722,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30270713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811358706',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30270713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30270713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15624417',
        'method' => 'get',
      ),
    ),
  ),
  7733886079 => 
  array (
    'id' => 15624677,
    'name' => 'ООО "Завод Резервуарных Конструкций СтройТехМаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496907150,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30271091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733886079',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30271091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15624677',
        'method' => 'get',
      ),
    ),
  ),
  7733836649 => 
  array (
    'id' => 15624757,
    'name' => '"Экологический патруль"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496907629,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30271205,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271205',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733836649',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30271205,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271205',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15624757',
        'method' => 'get',
      ),
    ),
  ),
  7733087929 => 
  array (
    'id' => 15625167,
    'name' => 'ООО АТ-2М',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496909328,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30271705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733087929',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30271705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15625167',
        'method' => 'get',
      ),
    ),
  ),
  7730608362 => 
  array (
    'id' => 15625193,
    'name' => 'ООО «777 Сервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496909505,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30271733,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271733',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730608362',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30271733,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271733',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15625193',
        'method' => 'get',
      ),
    ),
  ),
  7725285412 => 
  array (
    'id' => 15625267,
    'name' => 'ООО "Альфа-ФТОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496909853,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30271845,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271845',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725285412',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30271845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30271845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15625267',
        'method' => 'get',
      ),
    ),
  ),
  7722372931 => 
  array (
    'id' => 15625479,
    'name' => 'ООО "ПИТУКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496910697,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30272125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30272125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722372931',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30272125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30272125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15625479',
        'method' => 'get',
      ),
    ),
  ),
  7721787700 => 
  array (
    'id' => 15625757,
    'name' => 'ООО "ХимТех"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496911180,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30272269,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30272269',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721787700',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30272269,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30272269',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15625757',
        'method' => 'get',
      ),
    ),
  ),
  7720586984 => 
  array (
    'id' => 15625773,
    'name' => 'ООО "ИНФОТЕХСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496911264,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30272289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30272289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720586984',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30272289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30272289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15625773',
        'method' => 'get',
      ),
    ),
  ),
  7720504519 => 
  array (
    'id' => 15626449,
    'name' => 'ООО «УК «РемЭкс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496914165,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30273125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720504519',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30273125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15626449',
        'method' => 'get',
      ),
    ),
  ),
  7714328985 => 
  array (
    'id' => 15626665,
    'name' => 'ООО УК РЕМКОМПЛЕКТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496915005,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30273349,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273349',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714328985',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30273349,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273349',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15626665',
        'method' => 'get',
      ),
    ),
  ),
  7713783082 => 
  array (
    'id' => 15626683,
    'name' => 'ООО "ЛАВ-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496915097,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30273373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713783082',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30273373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15626683',
        'method' => 'get',
      ),
    ),
  ),
  6950145750 => 
  array (
    'id' => 15626989,
    'name' => 'НИИИТ, АО',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496916431,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30273737,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273737',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950145750',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30273737,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273737',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15626989',
        'method' => 'get',
      ),
    ),
  ),
  6685037049 => 
  array (
    'id' => 15627057,
    'name' => 'ООО "Русбилдинг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496916837,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30273849,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273849',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6685037049',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30273849,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30273849',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15627057',
        'method' => 'get',
      ),
    ),
  ),
  7447262908 => 
  array (
    'id' => 15635749,
    'name' => 'ООО Юридическая фирма «БЮРО»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496985552,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292307,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292307',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7447262908',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292307,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292307',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15635749',
        'method' => 'get',
      ),
    ),
  ),
  7219008878 => 
  array (
    'id' => 15635755,
    'name' => 'ООО "Сибкомплектстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496985688,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7219008878',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15635755',
        'method' => 'get',
      ),
    ),
  ),
  7203398076 => 
  array (
    'id' => 15635757,
    'name' => 'ООО «РусПарт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496985755,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292329,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292329',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203398076',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292329,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292329',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15635757',
        'method' => 'get',
      ),
    ),
  ),
  6606004854 => 
  array (
    'id' => 15635791,
    'name' => 'ООО "Возрождение"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496986196,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292387,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292387',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6606004854',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292387,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292387',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15635791',
        'method' => 'get',
      ),
    ),
  ),
  6318237700 => 
  array (
    'id' => 15635795,
    'name' => 'ООО "Абсолютстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496986291,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318237700',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15635795',
        'method' => 'get',
      ),
    ),
  ),
  7842393940 => 
  array (
    'id' => 15636091,
    'name' => 'ООО "СевЗапЭнергоСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496988922,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292805,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292805',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842393940',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292805,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292805',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15636091',
        'method' => 'get',
      ),
    ),
  ),
  7841493491 => 
  array (
    'id' => 15636129,
    'name' => 'ООО "РамСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496989177,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841493491',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15636129',
        'method' => 'get',
      ),
    ),
  ),
  7840513716 => 
  array (
    'id' => 15636199,
    'name' => 'ООО "ИНСТИТУТ "РЕГИОН ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496989608,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30292989,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292989',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840513716',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30292989,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30292989',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15636199',
        'method' => 'get',
      ),
    ),
  ),
  7826000423 => 
  array (
    'id' => 15636229,
    'name' => 'ООО Научно-Производственная холдинговая компания "Ремстройкомплекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496989785,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30293031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7826000423',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30293031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15636229',
        'method' => 'get',
      ),
    ),
  ),
  7825413971 => 
  array (
    'id' => 15636305,
    'name' => 'ООО "Компания АС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496990212,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30293123,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293123',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7825413971',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30293123,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293123',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15636305',
        'method' => 'get',
      ),
    ),
  ),
  7810336474 => 
  array (
    'id' => 15636385,
    'name' => 'ООО  "ГАРАНТ-ДорСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496990787,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30293231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810336474',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30293231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15636385',
        'method' => 'get',
      ),
    ),
  ),
  7733278271 => 
  array (
    'id' => 15636897,
    'name' => 'ООО "ЕВРОСНАБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496993860,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30293905,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293905',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733278271',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30293905,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30293905',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15636897',
        'method' => 'get',
      ),
    ),
  ),
  7731593655 => 
  array (
    'id' => 15637001,
    'name' => 'ООО«Инженерный центр «Европейская Электротехника»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496994349,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30294019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731593655',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30294019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15637001',
        'method' => 'get',
      ),
    ),
  ),
  7730663476 => 
  array (
    'id' => 15637023,
    'name' => 'ООО  "Управляющая компания Эдванс-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496994443,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30294043,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294043',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730663476',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30294043,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294043',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15637023',
        'method' => 'get',
      ),
    ),
  ),
  7726763982 => 
  array (
    'id' => 15637085,
    'name' => 'ООО «НПЦ Спецоснащение МО»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496994694,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30294137,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294137',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726763982',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30294137,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294137',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15637085',
        'method' => 'get',
      ),
    ),
  ),
  7726398433 => 
  array (
    'id' => 15637145,
    'name' => 'ООО "ИНДУСТРИЯ ЛЕСА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496995102,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30294229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726398433',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30294229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15637145',
        'method' => 'get',
      ),
    ),
  ),
  7724557462 => 
  array (
    'id' => 15637205,
    'name' => 'ООО ЧОП "БАСТИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496995423,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30294295,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294295',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724557462',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30294295,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30294295',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15637205',
        'method' => 'get',
      ),
    ),
  ),
  7710385213 => 
  array (
    'id' => 15638357,
    'name' => 'ООО "Диатех СА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497001061,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30295869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30295869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710385213',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30295869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30295869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15638357',
        'method' => 'get',
      ),
    ),
  ),
  7705534364 => 
  array (
    'id' => 15639313,
    'name' => 'ООО  "Строй Индустрия"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497005657,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30296989,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30296989',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705534364',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30296989,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30296989',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15639313',
        'method' => 'get',
      ),
    ),
  ),
  7705140391 => 
  array (
    'id' => 15639327,
    'name' => 'ЗАО «Интерконсул»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497005788,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30297041,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297041',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705140391',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30297041,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297041',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15639327',
        'method' => 'get',
      ),
    ),
  ),
  7704878147 => 
  array (
    'id' => 15639341,
    'name' => 'ООО "ЛАБОРАТОРИЯ МЕДИЦИНСКИХ ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ "ЭЛЕМЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497005880,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30297071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704878147',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30297071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15639341',
        'method' => 'get',
      ),
    ),
  ),
  7703413163 => 
  array (
    'id' => 15639447,
    'name' => 'ООО "ЦЕНТР-ГОРОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497006545,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30297301,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297301',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703413163',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30297301,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297301',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15639447',
        'method' => 'get',
      ),
    ),
  ),
  7106524222 => 
  array (
    'id' => 15639509,
    'name' => 'ООО "СМС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497006901,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30297417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7106524222',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30297417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30297417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15639509',
        'method' => 'get',
      ),
    ),
  ),
  7727774289 => 
  array (
    'id' => 15657983,
    'name' => 'ООО МЕРКУРИЙ ЛАЙТ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497331258,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30316455,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30316455',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727774289',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30316455,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30316455',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15657983',
        'method' => 'get',
      ),
    ),
  ),
  7431004154 => 
  array (
    'id' => 15657993,
    'name' => 'ООО "Кизилдорстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497331389,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30316465,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30316465',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7431004154',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30316465,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30316465',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15657993',
        'method' => 'get',
      ),
    ),
  ),
  1837014096 => 
  array (
    'id' => 15658441,
    'name' => 'ООО  «Надежный Коммунальный Сервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497335211,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30317003,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317003',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1837014096',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30317003,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317003',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15658441',
        'method' => 'get',
      ),
    ),
  ),
  7814553078 => 
  array (
    'id' => 15658689,
    'name' => 'ООО   АЛЬЯНС БИЗНЕС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497336883,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30317361,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317361',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814553078',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30317361,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317361',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15658689',
        'method' => 'get',
      ),
    ),
  ),
  7814462102 => 
  array (
    'id' => 15658791,
    'name' => 'ООО "Фирма "Экон"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497337532,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30317499,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317499',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814462102',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30317499,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317499',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15658791',
        'method' => 'get',
      ),
    ),
  ),
  7802768079 => 
  array (
    'id' => 15658897,
    'name' => 'ООО "КРИСТАЛЛ НЕВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497338159,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30317637,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317637',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802768079',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30317637,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30317637',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15658897',
        'method' => 'get',
      ),
    ),
  ),
  7802550308 => 
  array (
    'id' => 15659247,
    'name' => 'ООО «СПбСтрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497339922,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30318153,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318153',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802550308',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30318153,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318153',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15659247',
        'method' => 'get',
      ),
    ),
  ),
  7802493040 => 
  array (
    'id' => 15659479,
    'name' => 'ООО "ТВОЙ ЭКСПЕРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497340922,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30318459,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318459',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802493040',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30318459,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318459',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15659479',
        'method' => 'get',
      ),
    ),
  ),
  7801319151 => 
  array (
    'id' => 15659611,
    'name' => 'ООО "Меганти"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497341478,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30318649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801319151',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30318649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15659611',
        'method' => 'get',
      ),
    ),
  ),
  7743915427 => 
  array (
    'id' => 15659645,
    'name' => 'ООО СК "ДОРЦЕНТР "',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497341572,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30318677,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318677',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743915427',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30318677,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318677',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15659645',
        'method' => 'get',
      ),
    ),
  ),
  7724860557 => 
  array (
    'id' => 15663557,
    'name' => 'ООО  Секьюрити Сервис',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497348290,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30323889,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30323889',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724860557',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30323889,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30323889',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15663557',
        'method' => 'get',
      ),
    ),
  ),
  7722829777 => 
  array (
    'id' => 15663603,
    'name' => 'ООО "СИНЕРГИЯ СМР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497348419,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30323941,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30323941',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722829777',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30323941,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30323941',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15663603',
        'method' => 'get',
      ),
    ),
  ),
  7718237785 => 
  array (
    'id' => 15663983,
    'name' => 'ООО "АРКАВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497349104,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30324199,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324199',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718237785',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30324199,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324199',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15663983',
        'method' => 'get',
      ),
    ),
  ),
  7715659997 => 
  array (
    'id' => 15664067,
    'name' => 'ООО "Щит-2007"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497349528,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30324325,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324325',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715659997',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30324325,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324325',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15664067',
        'method' => 'get',
      ),
    ),
  ),
  7709700742 => 
  array (
    'id' => 15664247,
    'name' => 'ООО "ПРОФРЕМСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497350169,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30324553,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324553',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709700742',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30324553,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324553',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15664247',
        'method' => 'get',
      ),
    ),
  ),
  6674182365 => 
  array (
    'id' => 15664799,
    'name' => 'ООО "Техносила"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497352490,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30325179,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30325179',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6674182365',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30325179,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30325179',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15664799',
        'method' => 'get',
      ),
    ),
  ),
  5902032948 => 
  array (
    'id' => 15664883,
    'name' => 'ООО  "ГОТИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497353014,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30325309,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30325309',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902032948',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30325309,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30325309',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15664883',
        'method' => 'get',
      ),
    ),
  ),
  5250061809 => 
  array (
    'id' => 15665147,
    'name' => 'ООО "Автомагистраль"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497354140,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30325665,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30325665',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5250061809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30325665,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30325665',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15665147',
        'method' => 'get',
      ),
    ),
  ),
  7718661514 => 
  array (
    'id' => 15665811,
    'name' => 'ООО "МосГрадСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497357241,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30326447,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30326447',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718661514',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30326447,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30326447',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15665811',
        'method' => 'get',
      ),
    ),
  ),
  8902012584 => 
  array (
    'id' => 15669487,
    'name' => 'ООО "АЛЬЯНС И К"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497417789,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30330423,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330423',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8902012584',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30330423,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330423',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15669487',
        'method' => 'get',
      ),
    ),
  ),
  6685042306 => 
  array (
    'id' => 15669513,
    'name' => 'ООО "МОНОЛИТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497418228,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30330479,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330479',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6685042306',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30330479,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330479',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15669513',
        'method' => 'get',
      ),
    ),
  ),
  6670423387 => 
  array (
    'id' => 15669533,
    'name' => 'ООО "ПК "ЭВЕРЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497418402,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30330495,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330495',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670423387',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30330495,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330495',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15669533',
        'method' => 'get',
      ),
    ),
  ),
  6382072615 => 
  array (
    'id' => 15669541,
    'name' => 'ООО "НОВЫЙ КВАРТАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497418491,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30330507,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330507',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6382072615',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30330507,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30330507',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15669541',
        'method' => 'get',
      ),
    ),
  ),
  2223614015 => 
  array (
    'id' => 15670021,
    'name' => 'ООО "Центр информационных технологий"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497421579,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30331157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2223614015',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30331157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15670021',
        'method' => 'get',
      ),
    ),
  ),
  276912692 => 
  array (
    'id' => 15670093,
    'name' => 'ООО "ЛИПТО СНАБ ЭКО"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497421979,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30331261,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331261',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '276912692',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30331261,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331261',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15670093',
        'method' => 'get',
      ),
    ),
  ),
  9717046140 => 
  array (
    'id' => 15670217,
    'name' => 'ООО "ТРАНСПОРТНЫЕ РЕШЕНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497422712,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30331435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9717046140',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30331435,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331435',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15670217',
        'method' => 'get',
      ),
    ),
  ),
  9710014533 => 
  array (
    'id' => 15670573,
    'name' => 'ООО «АБУ-СТРОЙ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497424339,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30331851,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331851',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9710014533',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30331851,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331851',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15670573',
        'method' => 'get',
      ),
    ),
  ),
  7841464028 => 
  array (
    'id' => 15670623,
    'name' => 'ООО "СК Монтажстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497424638,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30331919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841464028',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30331919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30331919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15670623',
        'method' => 'get',
      ),
    ),
  ),
  7814508389 => 
  array (
    'id' => 15670893,
    'name' => 'ООО "КВАЛИТЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497425826,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30332201,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30332201',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814508389',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30332201,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30332201',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15670893',
        'method' => 'get',
      ),
    ),
  ),
  7811193620 => 
  array (
    'id' => 15670995,
    'name' => 'ООО "СТРОЙСФЕРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497426215,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30332687,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30332687',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811193620',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30332687,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30332687',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15670995',
        'method' => 'get',
      ),
    ),
  ),
  7810589362 => 
  array (
    'id' => 15671217,
    'name' => 'ООО Новый город',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497427438,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30332951,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30332951',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810589362',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30332951,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30332951',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15671217',
        'method' => 'get',
      ),
    ),
  ),
  7801426040 => 
  array (
    'id' => 15671303,
    'name' => 'ООО НВА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497427842,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30333059,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30333059',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801426040',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30333059,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30333059',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15671303',
        'method' => 'get',
      ),
    ),
  ),
  7726684280 => 
  array (
    'id' => 15671673,
    'name' => 'ООО "СК-Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497429355,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30333501,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30333501',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726684280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30333501,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30333501',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15671673',
        'method' => 'get',
      ),
    ),
  ),
  7725567512 => 
  array (
    'id' => 15671759,
    'name' => 'ООО "МСК ЭНЕРГО"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497429827,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30333607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30333607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725567512',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30333607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30333607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15671759',
        'method' => 'get',
      ),
    ),
  ),
  7723373166 => 
  array (
    'id' => 15672219,
    'name' => 'ООО "Весон-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497432075,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30334131,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334131',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723373166',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30334131,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334131',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15672219',
        'method' => 'get',
      ),
    ),
  ),
  7722789891 => 
  array (
    'id' => 15672243,
    'name' => 'ООО "МАКЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497432175,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30334157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722789891',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30334157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15672243',
        'method' => 'get',
      ),
    ),
  ),
  7701371101 => 
  array (
    'id' => 15672529,
    'name' => 'ООО ЗАРКОМ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497433525,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30334531,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334531',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701371101',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30334531,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334531',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15672529',
        'method' => 'get',
      ),
    ),
  ),
  6161066405 => 
  array (
    'id' => 15672563,
    'name' => 'ООО "АВС КОМФОРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497433663,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30334577,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334577',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6161066405',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30334577,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334577',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15672563',
        'method' => 'get',
      ),
    ),
  ),
  5828005769 => 
  array (
    'id' => 15672589,
    'name' => 'ООО «СтройСтандарт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497433776,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30334605,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334605',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5828005769',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30334605,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30334605',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15672589',
        'method' => 'get',
      ),
    ),
  ),
  6671030300 => 
  array (
    'id' => 15673367,
    'name' => 'ООО СВС-Альянс',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497437644,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30335625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30335625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671030300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30335625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30335625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15673367',
        'method' => 'get',
      ),
    ),
  ),
  7456036006 => 
  array (
    'id' => 15678245,
    'name' => 'ООО "ДОРСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497505363,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30341371,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341371',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7456036006',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30341371,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341371',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15678245',
        'method' => 'get',
      ),
    ),
  ),
  7205024414 => 
  array (
    'id' => 15678251,
    'name' => 'ООО "ТЮМЕНЬСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497505466,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30341389,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341389',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7205024414',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30341389,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341389',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15678251',
        'method' => 'get',
      ),
    ),
  ),
  5404504041 => 
  array (
    'id' => 15678293,
    'name' => 'ООО "ТЕХСНАБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497506114,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30341481,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341481',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404504041',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30341481,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341481',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15678293',
        'method' => 'get',
      ),
    ),
  ),
  2225068107 => 
  array (
    'id' => 15678363,
    'name' => 'ООО "Райагропромстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497506829,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30341597,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341597',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225068107',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30341597,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341597',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15678363',
        'method' => 'get',
      ),
    ),
  ),
  273061098 => 
  array (
    'id' => 15678495,
    'name' => 'ООО "УПРАВЛЕНИЕ МЕХАНИЗИРОВАННЫХ РАБОТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497507803,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30341787,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341787',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '273061098',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30341787,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30341787',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15678495',
        'method' => 'get',
      ),
    ),
  ),
  9705083531 => 
  array (
    'id' => 15679023,
    'name' => 'ООО "Ю-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497510961,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30342403,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342403',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705083531',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30342403,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342403',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15679023',
        'method' => 'get',
      ),
    ),
  ),
  9705057348 => 
  array (
    'id' => 15679047,
    'name' => 'ООО "БРИГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497511072,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30342435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705057348',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30342435,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342435',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15679047',
        'method' => 'get',
      ),
    ),
  ),
  7842336205 => 
  array (
    'id' => 15679077,
    'name' => 'ООО "СКАЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497511261,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30342487,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342487',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382025,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842336205',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30342487,
        1 => 31804529,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342487,31804529',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15679077',
        'method' => 'get',
      ),
    ),
  ),
  7840311808 => 
  array (
    'id' => 15679299,
    'name' => 'ООО "СЗСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497512057,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30342753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840311808',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30342753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30342753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15679299',
        'method' => 'get',
      ),
    ),
  ),
  7743183036 => 
  array (
    'id' => 15679605,
    'name' => 'ООО «МАГ-МАСТЕР»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497513412,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30343107,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30343107',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743183036',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30343107,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30343107',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15679605',
        'method' => 'get',
      ),
    ),
  ),
  7728323419 => 
  array (
    'id' => 15681681,
    'name' => 'ООО ПСК "Строймаркет"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497521054,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30345559,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345559',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728323419',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30345559,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345559',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15681681',
        'method' => 'get',
      ),
    ),
  ),
  7719412341 => 
  array (
    'id' => 15681753,
    'name' => 'ООО ЭКОТЕХПРОМ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497521387,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30345659,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345659',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719412341',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30345659,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345659',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15681753',
        'method' => 'get',
      ),
    ),
  ),
  7704381041 => 
  array (
    'id' => 15681961,
    'name' => 'ООО "Эверест"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497522327,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30345847,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345847',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704381041',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30345847,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345847',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15681961',
        'method' => 'get',
      ),
    ),
  ),
  5050130734 => 
  array (
    'id' => 15682063,
    'name' => 'ООО "ТЕХНОРЕСУРС""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497522825,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30345961,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345961',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5050130734',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30345961,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30345961',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15682063',
        'method' => 'get',
      ),
    ),
  ),
  7717545628 => 
  array (
    'id' => 15682399,
    'name' => 'ООО "ХХI-ВЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497524290,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30346325,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30346325',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717545628',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30346325,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30346325',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15682399',
        'method' => 'get',
      ),
    ),
  ),
  6453144403 => 
  array (
    'id' => 15682543,
    'name' => 'ООО Рекорд',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497524972,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30346517,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30346517',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6453144403',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30346517,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30346517',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15682543',
        'method' => 'get',
      ),
    ),
  ),
  2901242633 => 
  array (
    'id' => 15682695,
    'name' => 'ООО "ТОРГОВЫЙ ДОМ "СЕВЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497525840,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30346723,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30346723',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901242633',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30346723,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30346723',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15682695',
        'method' => 'get',
      ),
    ),
  ),
  2312257703 => 
  array (
    'id' => 15683171,
    'name' => 'ООО "ГМК"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497527848,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30347335,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30347335',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312257703',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30347335,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30347335',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15683171',
        'method' => 'get',
      ),
    ),
  ),
  2627026210 => 
  array (
    'id' => 15683181,
    'name' => 'ООО «Изобилие»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497527914,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30347347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30347347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2627026210',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30347347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30347347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15683181',
        'method' => 'get',
      ),
    ),
  ),
  3123193781 => 
  array (
    'id' => 15683341,
    'name' => 'ООО "Молочные продукты"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497528616,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30347553,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30347553',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123193781',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30347553,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30347553',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15683341',
        'method' => 'get',
      ),
    ),
  ),
  7604193630 => 
  array (
    'id' => 15683837,
    'name' => 'ООО «Нуар»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497530854,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30348215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30348215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7604193630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30348215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30348215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15683837',
        'method' => 'get',
      ),
    ),
  ),
  7717696200 => 
  array (
    'id' => 15683863,
    'name' => 'ООО "ГРАНД""',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497530938,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30348243,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30348243',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717696200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30348243,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30348243',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15683863',
        'method' => 'get',
      ),
    ),
  ),
  7451331375 => 
  array (
    'id' => 15687417,
    'name' => 'ООО Строительные Ресурсы',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497591254,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30352277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451331375',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30352277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15687417',
        'method' => 'get',
      ),
    ),
  ),
  7203335044 => 
  array (
    'id' => 15687447,
    'name' => 'ООО "БУРМОНТАЖСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497591643,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30352335,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352335',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203335044',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30352335,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352335',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15687447',
        'method' => 'get',
      ),
    ),
  ),
  5507220228 => 
  array (
    'id' => 15687539,
    'name' => 'ООО "АГРО-МАСТЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497592512,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30352481,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352481',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5507220228',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30352481,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352481',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15687539',
        'method' => 'get',
      ),
    ),
  ),
  '0203004434' => 
  array (
    'id' => 15687563,
    'name' => 'ООО "АЛЬЯНСТЕХСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497592733,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30352523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0203004434',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30352523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30352523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15687563',
        'method' => 'get',
      ),
    ),
  ),
  7825113939 => 
  array (
    'id' => 15688783,
    'name' => 'ООО Армада',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497599832,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30354083,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30354083',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7825113939',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30354083,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30354083',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15688783',
        'method' => 'get',
      ),
    ),
  ),
  7733275023 => 
  array (
    'id' => 15690123,
    'name' => 'ООО  "Платформа"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497606460,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30356247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733275023',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30356247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690123',
        'method' => 'get',
      ),
    ),
  ),
  7733234073 => 
  array (
    'id' => 15690143,
    'name' => 'ООО   Аванта',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497606540,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30356269,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356269',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733234073',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30356269,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356269',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690143',
        'method' => 'get',
      ),
    ),
  ),
  7727791453 => 
  array (
    'id' => 15690181,
    'name' => 'ООО "АЙТИВЭЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497606683,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30356309,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356309',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727791453',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30356309,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356309',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690181',
        'method' => 'get',
      ),
    ),
  ),
  7723425939 => 
  array (
    'id' => 15690215,
    'name' => 'ООО "ТЕХПРОЕКТ',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497606923,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30356353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723425939',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30356353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690215',
        'method' => 'get',
      ),
    ),
  ),
  7721010618 => 
  array (
    'id' => 15690243,
    'name' => 'ЗАО "ФИРМА САБИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497607018,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30356385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721010618',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30356385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690243',
        'method' => 'get',
      ),
    ),
  ),
  7706571136 => 
  array (
    'id' => 15690299,
    'name' => 'ООО  "НОВАЯ ПРОИЗВОДСТВЕННАЯ КОМПАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497607280,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30356449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706571136',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30356449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690299',
        'method' => 'get',
      ),
    ),
  ),
  5032256766 => 
  array (
    'id' => 15690373,
    'name' => 'ООО "ИНТЭК-ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497607584,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30356525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032256766',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30356525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30356525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690373',
        'method' => 'get',
      ),
    ),
  ),
  7715517791 => 
  array (
    'id' => 15690893,
    'name' => 'ООО "СНБ МЕДИА ГРУППА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497609920,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30357055,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357055',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715517791',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30357055,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357055',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690893',
        'method' => 'get',
      ),
    ),
  ),
  7709484410 => 
  array (
    'id' => 15690961,
    'name' => 'ООО "ЭСТЭН МОСКВА',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497610313,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30357137,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357137',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709484410',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30357137,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357137',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15690961',
        'method' => 'get',
      ),
    ),
  ),
  7448069375 => 
  array (
    'id' => 15691017,
    'name' => 'ООО "Завод СпецАгрегат"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497610582,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30357229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448069375',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30357229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15691017',
        'method' => 'get',
      ),
    ),
  ),
  5902884036 => 
  array (
    'id' => 15691179,
    'name' => 'ООО "РТК "Новые Технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497611303,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30357399,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357399',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902884036',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30357399,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30357399',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15691179',
        'method' => 'get',
      ),
    ),
  ),
  2901243394 => 
  array (
    'id' => 15691699,
    'name' => 'ООО  СВД',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497613917,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30358035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30358035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901243394',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30358035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30358035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15691699',
        'method' => 'get',
      ),
    ),
  ),
  3329075968 => 
  array (
    'id' => 15691707,
    'name' => 'ООО  Компания "Мастерфайбр 33"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497613984,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30358061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30358061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3329075968',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30358061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30358061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15691707',
        'method' => 'get',
      ),
    ),
  ),
  8606012104 => 
  array (
    'id' => 15707263,
    'name' => 'ООО Верховина',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497850273,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8606012104',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707263',
        'method' => 'get',
      ),
    ),
  ),
  8603215762 => 
  array (
    'id' => 15707275,
    'name' => 'ООО "РАДИКАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497850442,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603215762',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707275',
        'method' => 'get',
      ),
    ),
  ),
  8603186222 => 
  array (
    'id' => 15707281,
    'name' => 'ООО «СеверСтрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497850523,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373709,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373709',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603186222',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373709,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373709',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707281',
        'method' => 'get',
      ),
    ),
  ),
  6670062959 => 
  array (
    'id' => 15707317,
    'name' => 'ООО "БИЗНЕСКОНСАЛТИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497850896,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670062959',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707317',
        'method' => 'get',
      ),
    ),
  ),
  5948019146 => 
  array (
    'id' => 15707341,
    'name' => 'ООО "Пермспецводстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497851200,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373815,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373815',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5948019146',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373815,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373815',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707341',
        'method' => 'get',
      ),
    ),
  ),
  5404360128 => 
  array (
    'id' => 15707397,
    'name' => 'ООО "СибСтройПлюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497851620,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373887,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373887',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404360128',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373887,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373887',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707397',
        'method' => 'get',
      ),
    ),
  ),
  'акционерное общество &quot;Сбербанк России&quot; (ИНН: 7707083893) Публичное акционерное общество &quot;Объединенный Кредитный Банк&quot; (ИНН: 5249046404) ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО &quot;СОВКОМБАНК&quot; (ИНН: 4401116480) Общество с ограниченной ответственностью &quot;Современный Коммерческий Ипотечный Банк&quot; (ИНН: 4003011294) Общество с ограниченной ответственностью &quot;Современный Коммерческий Инновационный Банк&quot; (ИНН: 4003011294)' => 
  array (
    'id' => 15707415,
    'name' => 'ООО "КомплексСпорт"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497851767,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'акционерное общество &quot;Сбербанк России&quot; (ИНН: 7707083893) Публичное акционерное общество &quot;Объединенный Кредитный Банк&quot; (ИНН: 5249046404) ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО &quot;СОВКОМБАНК&quot; (ИНН: 4401116480) Общество с ограниченной ответственностью &quot;Современный Коммерческий Ипотечный Банк&quot; (ИНН: 4003011294) Общество с ограниченной ответственностью &quot;Современный Коммерческий Инновационный Банк&quot; (ИНН: 4003011294)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707415',
        'method' => 'get',
      ),
    ),
  ),
  269033108 => 
  array (
    'id' => 15707457,
    'name' => 'ООО  "ФАСАД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497852030,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30373955,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373955',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '269033108',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30373955,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30373955',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707457',
        'method' => 'get',
      ),
    ),
  ),
  7838005183 => 
  array (
    'id' => 15707573,
    'name' => 'ООО Центр компьютерных разработок"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497852830,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30374125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838005183',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30374125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707573',
        'method' => 'get',
      ),
    ),
  ),
  7816486910 => 
  array (
    'id' => 15707625,
    'name' => 'ООО "ОРИЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497853008,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30374179,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374179',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816486910',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30374179,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374179',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707625',
        'method' => 'get',
      ),
    ),
  ),
  7813028750 => 
  array (
    'id' => 15707695,
    'name' => 'Акционерное общество "Приборостроительный завод "ВИБРАТОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497853377,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30374245,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374245',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813028750',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30374245,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374245',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707695',
        'method' => 'get',
      ),
    ),
  ),
  7805634997 => 
  array (
    'id' => 15707993,
    'name' => 'ООО «Производственно Строительное Предприятие «Рест-Арт»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497855057,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30374611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805634997',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30374611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15707993',
        'method' => 'get',
      ),
    ),
  ),
  7802525380 => 
  array (
    'id' => 15708173,
    'name' => 'ООО «ЛЕНСТРОЙРЕСТАВРАЦИЯ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497856028,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30374835,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374835',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802525380',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30374835,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374835',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15708173',
        'method' => 'get',
      ),
    ),
  ),
  7802389401 => 
  array (
    'id' => 15708223,
    'name' => 'ООО "Флагман безопасности"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497856303,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30374897,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374897',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802389401',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30374897,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374897',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15708223',
        'method' => 'get',
      ),
    ),
  ),
  7801303923 => 
  array (
    'id' => 15708253,
    'name' => 'ООО "НТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497856424,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30374939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801303923',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30374939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30374939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15708253',
        'method' => 'get',
      ),
    ),
  ),
  7743164315 => 
  array (
    'id' => 15708293,
    'name' => 'ООО "ЭКОПРОДУКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497856614,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30375001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30375001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743164315',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30375001,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30375001',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15708293',
        'method' => 'get',
      ),
    ),
  ),
  7736619610 => 
  array (
    'id' => 15709229,
    'name' => 'ООО "АРХИТЕКТУРНО-РЕСТАВРАЦИОННАЯ МАСТЕРСКАЯ АРКАДА-СТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497860968,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30376225,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30376225',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736619610',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30376225,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30376225',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15709229',
        'method' => 'get',
      ),
    ),
  ),
  7730673883 => 
  array (
    'id' => 15710155,
    'name' => 'ООО "Сельскохозяйственные  
 ресурсы"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497865138,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30377413,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30377413',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730673883',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30377413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30377413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15710155',
        'method' => 'get',
      ),
    ),
  ),
  7730202845 => 
  array (
    'id' => 15710201,
    'name' => 'ООО «Премьер»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497865365,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30377461,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30377461',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730202845',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30377461,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30377461',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15710201',
        'method' => 'get',
      ),
    ),
  ),
  7721402894 => 
  array (
    'id' => 15711327,
    'name' => 'ООО «Русские Технологии»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497870043,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30378879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30378879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721402894',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30378879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30378879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15711327',
        'method' => 'get',
      ),
    ),
  ),
  7716835772 => 
  array (
    'id' => 15711365,
    'name' => 'ООО "АСФАЛЬТКОМПОНЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497870283,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30378939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30378939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716835772',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30378939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30378939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15711365',
        'method' => 'get',
      ),
    ),
  ),
  6950173884 => 
  array (
    'id' => 15712503,
    'name' => 'ООО "БРАНД-СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497875272,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30380399,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30380399',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950173884',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30380399,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30380399',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15712503',
        'method' => 'get',
      ),
    ),
  ),
  5047148040 => 
  array (
    'id' => 15713095,
    'name' => 'ООО «БР Трейд Союз»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497878204,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30381231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30381231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047148040',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30381231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30381231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15713095',
        'method' => 'get',
      ),
    ),
  ),
  7455018318 => 
  array (
    'id' => 15717003,
    'name' => 'ООО "Компания Процион"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497936753,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30385031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7455018318',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30385031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717003',
        'method' => 'get',
      ),
    ),
  ),
  7448176151 => 
  array (
    'id' => 15717025,
    'name' => 'ООО "Капремстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497936985,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30385067,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385067',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448176151',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30385067,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385067',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717025',
        'method' => 'get',
      ),
    ),
  ),
  5902034938 => 
  array (
    'id' => 15717291,
    'name' => 'ООО "ПРОМГАЗСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497939273,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30385475,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385475',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902034938',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30385475,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385475',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717291',
        'method' => 'get',
      ),
    ),
  ),
  9723004980 => 
  array (
    'id' => 15717623,
    'name' => 'ООО «Главный Центр информационной безопасности»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497941100,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30385967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9723004980',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30385967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30385967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717623',
        'method' => 'get',
      ),
    ),
  ),
  7841446798 => 
  array (
    'id' => 15717695,
    'name' => 'ООО "А+С ТРАНСПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497941618,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30386081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841446798',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30386081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717695',
        'method' => 'get',
      ),
    ),
  ),
  7839373038 => 
  array (
    'id' => 15717767,
    'name' => 'ООО "Базис Плюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497941931,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30386145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839373038',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30386145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717767',
        'method' => 'get',
      ),
    ),
  ),
  7811445839 => 
  array (
    'id' => 15717929,
    'name' => 'ООО "ПРОФИСТРОЙ Северо-Запад"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497942742,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30386355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811445839',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30386355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717929',
        'method' => 'get',
      ),
    ),
  ),
  7802819326 => 
  array (
    'id' => 15717977,
    'name' => 'ООО "Навтелсат Технолоджис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497943044,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30386431,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386431',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802819326',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30386431,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386431',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15717977',
        'method' => 'get',
      ),
    ),
  ),
  7802620523 => 
  array (
    'id' => 15718063,
    'name' => 'ООО «МЕДЕМА»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497943431,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28371232,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28371232',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1505287207,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802620523',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28371232,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28371232',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15718063',
        'method' => 'get',
      ),
    ),
  ),
  7734689725 => 
  array (
    'id' => 15718217,
    'name' => 'ООО «МКС СТРОЙ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497943951,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30386741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734689725',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30386741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15718217',
        'method' => 'get',
      ),
    ),
  ),
  7733901739 => 
  array (
    'id' => 15718365,
    'name' => 'ООО "СТРОЙАЛЬЯНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497944521,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30386915,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386915',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733901739',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30386915,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30386915',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15718365',
        'method' => 'get',
      ),
    ),
  ),
  7733897560 => 
  array (
    'id' => 15718563,
    'name' => 'ООО "МОСТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497945218,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30387155,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387155',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733897560',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30387155,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387155',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15718563',
        'method' => 'get',
      ),
    ),
  ),
  7731593990 => 
  array (
    'id' => 15718577,
    'name' => 'ООО "Медицинский Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497945293,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30387189,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387189',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731593990',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30387189,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387189',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15718577',
        'method' => 'get',
      ),
    ),
  ),
  7727640888 => 
  array (
    'id' => 15718613,
    'name' => 'ООО "ФРЕГАТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497945410,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30387227,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387227',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727640888',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30387227,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387227',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15718613',
        'method' => 'get',
      ),
    ),
  ),
  7720733886 => 
  array (
    'id' => 15718965,
    'name' => 'ООО "ЦК Гарант"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497946866,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30387659,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387659',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720733886',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30387659,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30387659',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15718965',
        'method' => 'get',
      ),
    ),
  ),
  7716839103 => 
  array (
    'id' => 15719335,
    'name' => 'ооо "Тенкор"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497947953,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30388017,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388017',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716839103',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30388017,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388017',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15719335',
        'method' => 'get',
      ),
    ),
  ),
  7715942500 => 
  array (
    'id' => 15719367,
    'name' => 'ООО "Барстт"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497948145,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30388077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715942500',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30388077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15719367',
        'method' => 'get',
      ),
    ),
  ),
  7711071890 => 
  array (
    'id' => 15719945,
    'name' => 'ЗАО "ИНТЕРСТРОЙБАЗИС',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497950771,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30388797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7711071890',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30388797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15719945',
        'method' => 'get',
      ),
    ),
  ),
  7710958993 => 
  array (
    'id' => 15719967,
    'name' => 'ООО «ДЕГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497950851,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30388815,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388815',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710958993',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30388815,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388815',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15719967',
        'method' => 'get',
      ),
    ),
  ),
  7705786594 => 
  array (
    'id' => 15720051,
    'name' => 'ООО «СТРОЙ ПЕРСПЕКТИВА»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497951505,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30388947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705786594',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30388947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720051',
        'method' => 'get',
      ),
    ),
  ),
  7705737205 => 
  array (
    'id' => 15720069,
    'name' => 'ООО Мир Станочника',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497951574,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30388967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705737205',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30388967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30388967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720069',
        'method' => 'get',
      ),
    ),
  ),
  6950031449 => 
  array (
    'id' => 15720109,
    'name' => 'ООО Жилкомсервис',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497951800,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389027,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389027',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950031449',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389027,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389027',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720109',
        'method' => 'get',
      ),
    ),
  ),
  6829053559 => 
  array (
    'id' => 15720147,
    'name' => 'ООО "ГЛОСНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497951970,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829053559',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720147',
        'method' => 'get',
      ),
    ),
  ),
  5257104515 => 
  array (
    'id' => 15720215,
    'name' => 'ООО "ВОЛГААВТОМАТСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497952181,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389139,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389139',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5257104515',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389139,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389139',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720215',
        'method' => 'get',
      ),
    ),
  ),
  7816579480 => 
  array (
    'id' => 15720341,
    'name' => 'ООО "ЭКО-ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497952679,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389295,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389295',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816579480',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389295,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389295',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720341',
        'method' => 'get',
      ),
    ),
  ),
  7718978889 => 
  array (
    'id' => 15720489,
    'name' => 'ООО «АЛЬФА»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497953312,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718978889',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720489',
        'method' => 'get',
      ),
    ),
  ),
  7709897464 => 
  array (
    'id' => 15720567,
    'name' => 'ООО Систем Групп',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497953574,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389575,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389575',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709897464',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389575,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389575',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720567',
        'method' => 'get',
      ),
    ),
  ),
  5190119547 => 
  array (
    'id' => 15720637,
    'name' => 'АО "Мурманская горэлектросеть"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497953911,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389661,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389661',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190119547',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389661,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389661',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720637',
        'method' => 'get',
      ),
    ),
  ),
  5405425610 => 
  array (
    'id' => 15720673,
    'name' => 'ООО "ТРАНССИБМЕТАЛЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497954070,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405425610',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720673',
        'method' => 'get',
      ),
    ),
  ),
  7327002915 => 
  array (
    'id' => 15720757,
    'name' => 'ООО "ДЕЛОВОЙ КВАРТАЛ К"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497954420,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30389815,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389815',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7327002915',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30389815,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30389815',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15720757',
        'method' => 'get',
      ),
    ),
  ),
  221005575 => 
  array (
    'id' => 15727895,
    'name' => 'ООО "РЕГИОНСТРОЙУПРАВЛЕНИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498022210,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '221005575',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15727895',
        'method' => 'get',
      ),
    ),
  ),
  6630007704 => 
  array (
    'id' => 15727975,
    'name' => 'ООО «МЖК»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498023572,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407509,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407509',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6630007704',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407509,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407509',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15727975',
        'method' => 'get',
      ),
    ),
  ),
  6672306833 => 
  array (
    'id' => 15727987,
    'name' => 'ООО "Урал Инжиниринг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498023678,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407517,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407517',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672306833',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407517,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407517',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15727987',
        'method' => 'get',
      ),
    ),
  ),
  7451219091 => 
  array (
    'id' => 15728027,
    'name' => 'ООО "Энерго-ресурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498024205,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407569,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407569',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451219091',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407569,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407569',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728027',
        'method' => 'get',
      ),
    ),
  ),
  9729025835 => 
  array (
    'id' => 15728135,
    'name' => 'ООО "КОМПАНИЯ СТРОЙ СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498025099,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407747,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407747',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9729025835',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407747,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407747',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728135',
        'method' => 'get',
      ),
    ),
  ),
  9701019912 => 
  array (
    'id' => 15728175,
    'name' => 'ООО «СТРОЙСНАБ 14»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498025354,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407789,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407789',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701019912',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407789,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407789',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728175',
        'method' => 'get',
      ),
    ),
  ),
  7814608418 => 
  array (
    'id' => 15728247,
    'name' => 'ООО «СЗСК»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498025905,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814608418',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728247',
        'method' => 'get',
      ),
    ),
  ),
  7813488482 => 
  array (
    'id' => 15728253,
    'name' => 'ООО "ФлотСнаб"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498025963,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407891,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407891',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813488482',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407891,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407891',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728253',
        'method' => 'get',
      ),
    ),
  ),
  7801609485 => 
  array (
    'id' => 15728401,
    'name' => 'ООО «РосЗнак»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498026958,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408107,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408107',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801609485',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408107,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408107',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728401',
        'method' => 'get',
      ),
    ),
  ),
  7801452555 => 
  array (
    'id' => 15728417,
    'name' => 'ООО "Балтийская Коммерция"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498027115,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408137,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408137',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801452555',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408137,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408137',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728417',
        'method' => 'get',
      ),
    ),
  ),
  7751014144 => 
  array (
    'id' => 15728447,
    'name' => 'ООО «МНОГОПРОФИЛЬНЫЙ ЦЕНТР ПРОЕКТИРОВАНИЯ И СТРОИТЕЛЬСТВА «БРАНДМАЙОР»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498027362,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408181,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408181',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751014144',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408181,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408181',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728447',
        'method' => 'get',
      ),
    ),
  ),
  7736684345 => 
  array (
    'id' => 15728465,
    'name' => 'ООО "Лаборатория"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498027468,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408213,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408213',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736684345',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408213,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408213',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728465',
        'method' => 'get',
      ),
    ),
  ),
  7736679828 => 
  array (
    'id' => 15728477,
    'name' => 'ООО "ГБПринт"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498027532,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408225,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408225',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736679828',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408225,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408225',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728477',
        'method' => 'get',
      ),
    ),
  ),
  7733738602 => 
  array (
    'id' => 15728519,
    'name' => '"ООО "Триор-Мед"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498027812,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408309,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408309',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733738602',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408309,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408309',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728519',
        'method' => 'get',
      ),
    ),
  ),
  7729486350 => 
  array (
    'id' => 15728561,
    'name' => 'ООО "ТЕХСТРОЙМОНТАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498027986,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408363,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408363',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729486350',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408363,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408363',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728561',
        'method' => 'get',
      ),
    ),
  ),
  7728849113 => 
  array (
    'id' => 15728589,
    'name' => 'ООО «Ариус»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498028101,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30408395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728849113',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30408395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30408395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728589',
        'method' => 'get',
      ),
    ),
  ),
  7723899008 => 
  array (
    'id' => 15728743,
    'name' => 'ООО "МЕДИАН"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498028879,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30420533,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420533',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723899008',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30420533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728743',
        'method' => 'get',
      ),
    ),
  ),
  7722392494 => 
  array (
    'id' => 15728847,
    'name' => 'ООО "ВЕЛЕС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498029470,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30420699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722392494',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30420699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728847',
        'method' => 'get',
      ),
    ),
  ),
  7721281311 => 
  array (
    'id' => 15728867,
    'name' => 'ООО "МСБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498029548,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30420713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721281311',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30420713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15728867',
        'method' => 'get',
      ),
    ),
  ),
  7719782977 => 
  array (
    'id' => 15729067,
    'name' => 'ООО "Интенсивмед"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498030556,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30420965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719782977',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30420965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30420965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15729067',
        'method' => 'get',
      ),
    ),
  ),
  7719272976 => 
  array (
    'id' => 15729103,
    'name' => 'ООО «Диалан»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498030727,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30421019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30421019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719272976',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30421019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30421019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15729103',
        'method' => 'get',
      ),
    ),
  ),
  7718695873 => 
  array (
    'id' => 15729193,
    'name' => 'ООО "Глобал Инжиниринг"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498031049,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30421125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30421125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718695873',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30421125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30421125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15729193',
        'method' => 'get',
      ),
    ),
  ),
  7709541796 => 
  array (
    'id' => 15729563,
    'name' => 'ЗАО "ПроЛоджикс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498032225,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30421393,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30421393',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709541796',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30421393,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30421393',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15729563',
        'method' => 'get',
      ),
    ),
  ),
  5024107697 => 
  array (
    'id' => 15731645,
    'name' => 'ООО "РСУ-89"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498041230,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30424035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024107697',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30424035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15731645',
        'method' => 'get',
      ),
    ),
  ),
  5018119694 => 
  array (
    'id' => 15731675,
    'name' => 'ООО «Торговый дом «Инфогаз»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498041395,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30424079,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424079',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5018119694',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30424079,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424079',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15731675',
        'method' => 'get',
      ),
    ),
  ),
  5014100300 => 
  array (
    'id' => 15731839,
    'name' => 'ООО «Строймехсервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498041933,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30424305,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424305',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5014100300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30424305,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424305',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15731839',
        'method' => 'get',
      ),
    ),
  ),
  3702583101 => 
  array (
    'id' => 15732001,
    'name' => 'ООО "НПО "РУССКИЙ ЛЕС"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498042171,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30424493,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424493',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702583101',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30424493,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30424493',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15732001',
        'method' => 'get',
      ),
    ),
  ),
  7701890499 => 
  array (
    'id' => 15732731,
    'name' => 'ООО "ПромЗеленСтройПроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498043284,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30425333,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30425333',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701890499',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30425333,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30425333',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15732731',
        'method' => 'get',
      ),
    ),
  ),
  5408260082 => 
  array (
    'id' => 15738303,
    'name' => 'ООО "СИБТРАНССТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498108971,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5408260082',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738303',
        'method' => 'get',
      ),
    ),
  ),
  6660007606 => 
  array (
    'id' => 15738369,
    'name' => 'АКЦИОНЕРНОЕ ОБЩЕСТВО "УРАЛТИСИЗ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498109664,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432383,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432383',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6660007606',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432383,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432383',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738369',
        'method' => 'get',
      ),
    ),
  ),
  6686029851 => 
  array (
    'id' => 15738419,
    'name' => 'ООО "СК "УНИВЕРСАЛ-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498110124,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432437,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432437',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6686029851',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432437,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432437',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738419',
        'method' => 'get',
      ),
    ),
  ),
  7017403106 => 
  array (
    'id' => 15738437,
    'name' => 'ООО "Алиот"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498110338,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432465,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432465',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017403106',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432465,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432465',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738437',
        'method' => 'get',
      ),
    ),
  ),
  7204104924 => 
  array (
    'id' => 15738441,
    'name' => 'ООО "СТРОЙГЕОПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498110422,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432471,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432471',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7204104924',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432471,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432471',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738441',
        'method' => 'get',
      ),
    ),
  ),
  8602189365 => 
  array (
    'id' => 15738455,
    'name' => '"СибирьСтройКомплекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498110665,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432495,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432495',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602189365',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432495,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432495',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738455',
        'method' => 'get',
      ),
    ),
  ),
  9705077224 => 
  array (
    'id' => 15738619,
    'name' => 'ООО СЛУЖБА КОНТРОЛЯ И ДИСПЕТЧЕРИЗАЦИИ»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498112063,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705077224',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738619',
        'method' => 'get',
      ),
    ),
  ),
  7836000045 => 
  array (
    'id' => 15738645,
    'name' => 'ООО "УНР-398"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498112253,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7836000045',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738645',
        'method' => 'get',
      ),
    ),
  ),
  7820312585 => 
  array (
    'id' => 15738775,
    'name' => 'ООО Энергопроф',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498113144,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30432909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7820312585',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30432909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30432909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738775',
        'method' => 'get',
      ),
    ),
  ),
  7816564131 => 
  array (
    'id' => 15738861,
    'name' => 'ООО "Стратегия Защиты"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498113623,
    'updated_at' => 1522728603,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30433011,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30433011',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816564131',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30433011,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30433011',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15738861',
        'method' => 'get',
      ),
    ),
  ),
);