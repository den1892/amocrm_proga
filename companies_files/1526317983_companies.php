<?php 
 return array (
  1701058120 => 
  array (
    'id' => 14910299,
    'name' => 'ООО "ЦКДЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489639844,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284177,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284177',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1701058120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284177,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284177',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910299',
        'method' => 'get',
      ),
    ),
  ),
  7814378595 => 
  array (
    'id' => 14910433,
    'name' => 'ООО  Матрица-М',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489641486,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814378595',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910433',
        'method' => 'get',
      ),
    ),
  ),
  7453274852 => 
  array (
    'id' => 14910463,
    'name' => 'ООО Центр технологий АНТ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489641881,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7453274852',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910463',
        'method' => 'get',
      ),
    ),
  ),
  7203375061 => 
  array (
    'id' => 14910481,
    'name' => 'ООО "СтройСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489642095,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203375061',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910481',
        'method' => 'get',
      ),
    ),
  ),
  7725010048 => 
  array (
    'id' => 14910519,
    'name' => '"Научно-технический центр по ядерной и радиационной безопасности"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489642367,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725010048',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910519',
        'method' => 'get',
      ),
    ),
  ),
  7806005744 => 
  array (
    'id' => 14910529,
    'name' => 'НАО  «Медико-Фармацевтическая компания Северо-Запад»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489642621,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806005744',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910529',
        'method' => 'get',
      ),
    ),
  ),
  410119747189 => 
  array (
    'id' => 14910535,
    'name' => 'ИП КАЙЗЕР СЕРГЕЙ АНДРЕЕВИЧ',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489642638,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284563,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284563',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '410119747189',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910535',
        'method' => 'get',
      ),
    ),
  ),
  281800375800 => 
  array (
    'id' => 14910567,
    'name' => 'ИП Саяпина Ксения Александровна',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489642874,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284601,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284601',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '281800375800',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284601,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284601',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910567',
        'method' => 'get',
      ),
    ),
  ),
  7709031643 => 
  array (
    'id' => 14910669,
    'name' => 'ЗАО «Московская акционерная страховая компания»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489643873,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284735,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284735',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709031643',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284735,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284735',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910669',
        'method' => 'get',
      ),
    ),
  ),
  7710026574 => 
  array (
    'id' => 14910699,
    'name' => 'СТРАХОВОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО "ВСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489644159,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284771,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284771',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710026574',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Нижегородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284771,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284771',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910699',
        'method' => 'get',
      ),
    ),
  ),
  7806125671 => 
  array (
    'id' => 14910729,
    'name' => 'ЗАО "Научно-производственное объединение специальных материалов"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489644409,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284813,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284813',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806125671',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284813,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284813',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910729',
        'method' => 'get',
      ),
    ),
  ),
  9909201656 => 
  array (
    'id' => 14910745,
    'name' => 'Бюро ван Дейк Эдишнз Электроникс',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489644549,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284835,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284835',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9909201656',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284835,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284835',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910745',
        'method' => 'get',
      ),
    ),
  ),
  7724294887 => 
  array (
    'id' => 14910763,
    'name' => 'АКЦИОНЕРНОЕ ОБЩЕСТВО "РОСГЕОЛОГИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489644672,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284863,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284863',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724294887',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284863,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284863',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910763',
        'method' => 'get',
      ),
    ),
  ),
  1512015210 => 
  array (
    'id' => 14910839,
    'name' => 'ООО "ИрафТрансСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489645175,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29284969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1512015210',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29284969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29284969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910839',
        'method' => 'get',
      ),
    ),
  ),
  4826087203 => 
  array (
    'id' => 14910899,
    'name' => 'ООО "Доминант"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489645529,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826087203',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910899',
        'method' => 'get',
      ),
    ),
  ),
  5024116596 => 
  array (
    'id' => 14910921,
    'name' => 'ООО "РОСТ ФУДС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489645751,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285079,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285079',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024116596',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285079,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285079',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910921',
        'method' => 'get',
      ),
    ),
  ),
  5038095297 => 
  array (
    'id' => 14910959,
    'name' => 'ООО "Ресурс М"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489645963,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285121,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285121',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5038095297',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910959',
        'method' => 'get',
      ),
    ),
  ),
  5047183430 => 
  array (
    'id' => 14910973,
    'name' => 'ООО "ТРЕЙД СД"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489646052,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285141,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285141',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047183430',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285141,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285141',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910973',
        'method' => 'get',
      ),
    ),
  ),
  7202157342 => 
  array (
    'id' => 14910975,
    'name' => 'АО "Фармация"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489646098,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7202157342',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910975',
        'method' => 'get',
      ),
    ),
  ),
  5101311273 => 
  array (
    'id' => 14910987,
    'name' => 'ООО "Мастерстройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489646157,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285167,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285167',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5101311273',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285167,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285167',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14910987',
        'method' => 'get',
      ),
    ),
  ),
  5103300037 => 
  array (
    'id' => 14911009,
    'name' => 'ООО "ТИРВАС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489646295,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285191,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285191',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5103300037',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285191,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285191',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911009',
        'method' => 'get',
      ),
    ),
  ),
  5043014511 => 
  array (
    'id' => 14911011,
    'name' => 'МУП "Серпуховская городская электрическая сеть"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489646310,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5043014511',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911011',
        'method' => 'get',
      ),
    ),
  ),
  5190112083 => 
  array (
    'id' => 14911073,
    'name' => 'ООО "САНАТОРИЙ "ТАМАРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489646432,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285255,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285255',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190112083',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285255,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285255',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911073',
        'method' => 'get',
      ),
    ),
  ),
  5263056732 => 
  array (
    'id' => 14911139,
    'name' => 'ООО "АрморГрупп"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489646870,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285361,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285361',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5263056732',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285361,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285361',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911139',
        'method' => 'get',
      ),
    ),
  ),
  5433137090 => 
  array (
    'id' => 14911147,
    'name' => 'ООО "ГОТТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489646970,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5433137090',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911147',
        'method' => 'get',
      ),
    ),
  ),
  7735599136 => 
  array (
    'id' => 14911193,
    'name' => 'ООО ТД "БИОХИМСЕНСОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489647138,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285433,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285433',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921399,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735599136',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285433,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285433',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911193',
        'method' => 'get',
      ),
    ),
  ),
  5404505260 => 
  array (
    'id' => 14911275,
    'name' => 'ООО "Автоноватор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489647511,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285495,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285495',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921400,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404505260',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285495,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285495',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911275',
        'method' => 'get',
      ),
    ),
  ),
  6111013260 => 
  array (
    'id' => 14911329,
    'name' => 'ООО "МЕДВЕДЬ-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489647895,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285587,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285587',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6111013260',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285587,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285587',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911329',
        'method' => 'get',
      ),
    ),
  ),
  7805093681 => 
  array (
    'id' => 14911461,
    'name' => 'ООО "Центр речевых технологий"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489648743,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921400,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805093681',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911461',
        'method' => 'get',
      ),
    ),
  ),
  6155921950 => 
  array (
    'id' => 14911651,
    'name' => 'ООО "ПАНСИОНАТ ОТДЫХА САНАТОРНОГО ТИПА, КРУГЛОГОДИЧНОГО ДЕЙСТВИЯ "ШАХТИНСКИЙ ТЕКСТИЛЬЩИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489649557,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29285971,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285971',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605889,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6155921950',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29285971,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29285971',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911651',
        'method' => 'get',
      ),
    ),
  ),
  7735557785 => 
  array (
    'id' => 14911689,
    'name' => 'ООО "Внедрение Научных Исследований и Инжиниринг" Спектр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489649696,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286015,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286015',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921400,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735557785',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286015,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286015',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911689',
        'method' => 'get',
      ),
    ),
  ),
  6314042732 => 
  array (
    'id' => 14911729,
    'name' => 'ООО «РесурсКопи»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489649864,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286065,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286065',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6314042732',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286065,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286065',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911729',
        'method' => 'get',
      ),
    ),
  ),
  6670291162 => 
  array (
    'id' => 14911739,
    'name' => 'ООО "Линкос МК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489649895,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286073,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286073',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670291162',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Курганская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286073,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286073',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911739',
        'method' => 'get',
      ),
    ),
  ),
  6432010656 => 
  array (
    'id' => 14911767,
    'name' => 'ООО ФИРМА "ЗОРИНКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489650002,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286103,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286103',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6432010656',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286103,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286103',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14911767',
        'method' => 'get',
      ),
    ),
  ),
  7707262518 => 
  array (
    'id' => 14912065,
    'name' => 'ООО "РУФСИМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489650370,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286481,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286481',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707262518',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286481,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286481',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912065',
        'method' => 'get',
      ),
    ),
  ),
  6448008589 => 
  array (
    'id' => 14912099,
    'name' => 'ООО "Волгадорстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489650492,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6448008589',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912099',
        'method' => 'get',
      ),
    ),
  ),
  6670272522 => 
  array (
    'id' => 14912137,
    'name' => 'ООО "УК "ВИСТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489650691,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670272522',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912137',
        'method' => 'get',
      ),
    ),
  ),
  7107085560 => 
  array (
    'id' => 14912251,
    'name' => 'ООО "РЕГАРД"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489651162,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286751,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286751',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7107085560',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286751,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286751',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912251',
        'method' => 'get',
      ),
    ),
  ),
  7703528100 => 
  array (
    'id' => 14912303,
    'name' => 'ООО "СТРОЙСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489651428,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286831,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286831',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703528100',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286831,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286831',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912303',
        'method' => 'get',
      ),
    ),
  ),
  2634815215 => 
  array (
    'id' => 14912351,
    'name' => 'ООО «Центр медицинских инноваций»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489651546,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29286945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2634815215',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29286945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29286945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912351',
        'method' => 'get',
      ),
    ),
  ),
  4824016567 => 
  array (
    'id' => 14912407,
    'name' => 'ООО  "Авто-ММ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489651790,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29287015,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287015',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4824016567',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Липецкая область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29287015,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287015',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912407',
        'method' => 'get',
      ),
    ),
  ),
  7708783457 => 
  array (
    'id' => 14912461,
    'name' => 'ООО "ФАВОРИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489651959,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29287077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708783457',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29287077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912461',
        'method' => 'get',
      ),
    ),
  ),
  7710056593 => 
  array (
    'id' => 14912509,
    'name' => 'ЗАО "НАУЧНО-ТЕХНОЛОГИЧЕСКИЙ ЦЕНТР ИРМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489652111,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29287121,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287121',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710056593',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29287121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912509',
        'method' => 'get',
      ),
    ),
  ),
  610660464153 => 
  array (
    'id' => 14912529,
    'name' => 'ИП Кореневский Виктор Николаевич -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489652181,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29287135,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287135',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '610660464153',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29287135,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287135',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912529',
        'method' => 'get',
      ),
    ),
  ),
  6671278823 => 
  array (
    'id' => 14912705,
    'name' => 'ООО "Уралмедснаб"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489652738,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29287385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671278823',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29287385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912705',
        'method' => 'get',
      ),
    ),
  ),
  7702780040 => 
  array (
    'id' => 14912823,
    'name' => 'ООО "ААС СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489653187,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29287533,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287533',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702780040',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29287533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912823',
        'method' => 'get',
      ),
    ),
  ),
  645114020192 => 
  array (
    'id' => 14912939,
    'name' => 'ИП  Купренков Денис Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489653558,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29287677,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287677',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '645114020192',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29287677,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29287677',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14912939',
        'method' => 'get',
      ),
    ),
  ),
  7729780270 => 
  array (
    'id' => 14914689,
    'name' => 'ООО "РАЗВИТИЕ ЦЕНТРАЛЬНОГО РЕГИОНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489657713,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29290627,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29290627',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729780270',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29290627,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29290627',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14914689',
        'method' => 'get',
      ),
    ),
  ),
  7712060323 => 
  array (
    'id' => 14914729,
    'name' => 'АО  "ИРМАСТ-ХОЛДИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489657899,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29290659,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29290659',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7712060323',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29290659,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29290659',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14914729',
        'method' => 'get',
      ),
    ),
  ),
  7709219099 => 
  array (
    'id' => 14915003,
    'name' => 'Акционерное общество «Компания ТрансТелеКом»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489659098,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29290981,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29290981',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709219099',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29290981,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29290981',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915003',
        'method' => 'get',
      ),
    ),
  ),
  6658141869 => 
  array (
    'id' => 14915061,
    'name' => 'ООО СК  "ГРАНД-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489659367,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291059,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291059',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658141869',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291059,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291059',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915061',
        'method' => 'get',
      ),
    ),
  ),
  7710578494 => 
  array (
    'id' => 14915101,
    'name' => 'АО "Единые технологии в энергетике"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489659568,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291119,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291119',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710578494',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291119,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291119',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915101',
        'method' => 'get',
      ),
    ),
  ),
  1657118522 => 
  array (
    'id' => 14915165,
    'name' => 'ООО «ЭкоСтройИнжиниринг»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489659725,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291225,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291225',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657118522',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291225,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291225',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915165',
        'method' => 'get',
      ),
    ),
  ),
  3906070115 => 
  array (
    'id' => 14915247,
    'name' => 'ООО  "БАЛТИЙСКАЯ УГОЛЬНАЯ КОМПАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489660005,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291295,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291295',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906070115',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калининградская область(GMT +2)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291295,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291295',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915247',
        'method' => 'get',
      ),
    ),
  ),
  7714511476 => 
  array (
    'id' => 14915319,
    'name' => 'ООО  «ФармЛайн»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489660270,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291383,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291383',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714511476',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291383,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291383',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915319',
        'method' => 'get',
      ),
    ),
  ),
  7801178126 => 
  array (
    'id' => 14915419,
    'name' => 'ООО «Управляющая компания Бекар»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489660569,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291465,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291465',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801178126',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291465,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291465',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915419',
        'method' => 'get',
      ),
    ),
  ),
  1435009427 => 
  array (
    'id' => 14915613,
    'name' => 'ОАО  "Сахаавтодор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489661355,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291747,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291747',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435009427',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291747,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291747',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915613',
        'method' => 'get',
      ),
    ),
  ),
  7842383854 => 
  array (
    'id' => 14915755,
    'name' => 'ЗАО "НАУЧНО-ПРОИЗВОДСТВЕННОЕ ОБЪЕДИНЕНИЕ "СПЕЦПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489662074,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29291979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842383854',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ленинградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29291979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29291979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915755',
        'method' => 'get',
      ),
    ),
  ),
  7810674018 => 
  array (
    'id' => 14915795,
    'name' => 'ООО "Лапин Энтерпрайз"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489662220,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29292021,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292021',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810674018',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29292021,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292021',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915795',
        'method' => 'get',
      ),
    ),
  ),
  7724032017 => 
  array (
    'id' => 14915815,
    'name' => 'АО  "Научно-производственное предприятие КлАСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489662346,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29292071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724032017',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29292071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915815',
        'method' => 'get',
      ),
    ),
  ),
  7715586594 => 
  array (
    'id' => 14915987,
    'name' => 'ООО "РОЛЬФ МОТОРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489663270,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29292259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921395,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715586594',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29292259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14915987',
        'method' => 'get',
      ),
    ),
  ),
  2466277153 => 
  array (
    'id' => 14916481,
    'name' => 'ООО  "Проект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489665438,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29292881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466277153',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29292881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14916481',
        'method' => 'get',
      ),
    ),
  ),
  3435081089 => 
  array (
    'id' => 14916505,
    'name' => 'ООО «СП «Ахтуба»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489665561,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29292915,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292915',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382070,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435081089',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29292915,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292915',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14916505',
        'method' => 'get',
      ),
    ),
  ),
  274144861 => 
  array (
    'id' => 14916507,
    'name' => 'АО  "Башкиравтодор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489665583,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29292921,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292921',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274144861',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29292921,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29292921',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14916507',
        'method' => 'get',
      ),
    ),
  ),
  4217120621 => 
  array (
    'id' => 14916761,
    'name' => 'ООО "ИНТЕРЬЕР ОФИС+"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489666572,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293193,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293193',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4217120621',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кемеровская область(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293193,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293193',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14916761',
        'method' => 'get',
      ),
    ),
  ),
  502800718140 => 
  array (
    'id' => 14916805,
    'name' => 'ИП Судников Сергей Сергеевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489666670,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293241,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293241',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '502800718140',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293241,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293241',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14916805',
        'method' => 'get',
      ),
    ),
  ),
  7733589887 => 
  array (
    'id' => 14916967,
    'name' => 'ООО "ЦИТ Чайка"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489667291,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293411,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293411',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733589887',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293411,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293411',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14916967',
        'method' => 'get',
      ),
    ),
  ),
  7728709187 => 
  array (
    'id' => 14917005,
    'name' => 'ООО "МАРС ИНТЕРНЕЙШНЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489667445,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293465,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293465',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728709187',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293465,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293465',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917005',
        'method' => 'get',
      ),
    ),
  ),
  3443064351 => 
  array (
    'id' => 14917063,
    'name' => 'ООО "АВТОТРАНЗИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489667706,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3443064351',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917063',
        'method' => 'get',
      ),
    ),
  ),
  7806404890 => 
  array (
    'id' => 14917197,
    'name' => 'ООО "Медсервис-диагностика',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489668055,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293657,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293657',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806404890',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293657,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293657',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917197',
        'method' => 'get',
      ),
    ),
  ),
  233301765081 => 
  array (
    'id' => 14917315,
    'name' => 'ИП Степанов Виталий Валерьевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489668467,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293785,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293785',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '233301765081',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293785,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293785',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917315',
        'method' => 'get',
      ),
    ),
  ),
  614201832200 => 
  array (
    'id' => 14917405,
    'name' => 'ИП Позднышева Вера Васильевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489668990,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29293911,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293911',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '614201832200',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29293911,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29293911',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917405',
        'method' => 'get',
      ),
    ),
  ),
  4205201019 => 
  array (
    'id' => 14917509,
    'name' => 'ООО "ФСК СтройСиб-42""',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489669426,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29294025,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294025',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205201019',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кемеровская область(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29294025,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294025',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917509',
        'method' => 'get',
      ),
    ),
  ),
  7733736348 => 
  array (
    'id' => 14917617,
    'name' => 'ООО «БиоАналит»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489669908,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29294163,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294163',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733736348',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29294163,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294163',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917617',
        'method' => 'get',
      ),
    ),
  ),
  352806718182 => 
  array (
    'id' => 14917649,
    'name' => 'ИП Мочалов Сергей Вячеславович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489670090,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29294213,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294213',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '352806718182',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29294213,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294213',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917649',
        'method' => 'get',
      ),
    ),
  ),
  7438013246 => 
  array (
    'id' => 14917689,
    'name' => 'ООО "Регион"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489670321,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29294285,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294285',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7438013246',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29294285,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294285',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917689',
        'method' => 'get',
      ),
    ),
  ),
  7811055532 => 
  array (
    'id' => 14917847,
    'name' => '"Балтийская Медицинская Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489670965,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29294467,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294467',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811055532',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29294467,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294467',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917847',
        'method' => 'get',
      ),
    ),
  ),
  301710938930 => 
  array (
    'id' => 14917919,
    'name' => 'ИП  Мостовой Олег Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489671385,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29294569,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294569',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '301710938930',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29294569,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29294569',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14917919',
        'method' => 'get',
      ),
    ),
  ),
  2203002600 => 
  array (
    'id' => 14920583,
    'name' => 'ООО "РУНО"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489727324,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29297999,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29297999',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2203002600',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29297999,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29297999',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920583',
        'method' => 'get',
      ),
    ),
  ),
  2221058644 => 
  array (
    'id' => 14920605,
    'name' => 'ООО "Агростройинвест"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489727564,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298025,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298025',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221058644',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298025,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298025',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920605',
        'method' => 'get',
      ),
    ),
  ),
  2221217485 => 
  array (
    'id' => 14920659,
    'name' => 'ООО "Гарант"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489727841,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298083,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298083',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221217485',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298083,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298083',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920659',
        'method' => 'get',
      ),
    ),
  ),
  2725128354 => 
  array (
    'id' => 14920663,
    'name' => 'ООО "КАРДЭКСПЕРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489727890,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298093,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298093',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2725128354',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хабаровский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298093,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298093',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920663',
        'method' => 'get',
      ),
    ),
  ),
  2225074693 => 
  array (
    'id' => 14920675,
    'name' => 'МУП "Горзеленхоз" г. Барнаула',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489728012,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298115,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298115',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225074693',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298115,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298115',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920675',
        'method' => 'get',
      ),
    ),
  ),
  5405215998 => 
  array (
    'id' => 14920683,
    'name' => 'ООО  "Сибавтобан"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489728166,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298129,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298129',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405215998',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298129,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298129',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920683',
        'method' => 'get',
      ),
    ),
  ),
  2225126503 => 
  array (
    'id' => 14920691,
    'name' => 'ООО "ТД "Алтайнефтересурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489728351,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298151,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298151',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921396,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225126503',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298151,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298151',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920691',
        'method' => 'get',
      ),
    ),
  ),
  6671457646 => 
  array (
    'id' => 14920701,
    'name' => 'ООО "ДОМИНАНТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489728461,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298167,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298167',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671457646',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298167,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298167',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920701',
        'method' => 'get',
      ),
    ),
  ),
  3801073800 => 
  array (
    'id' => 14920707,
    'name' => 'ООО "ЭЛИТ-ДЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489728578,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298179,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298179',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3801073800',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298179,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298179',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920707',
        'method' => 'get',
      ),
    ),
  ),
  2464125162 => 
  array (
    'id' => 14920711,
    'name' => 'ООО "ИНГРЕССА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489728637,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298187,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298187',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464125162',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298187,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298187',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920711',
        'method' => 'get',
      ),
    ),
  ),
  7713201030 => 
  array (
    'id' => 14920721,
    'name' => 'ООО "А.М. ПК-Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489728753,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298209,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298209',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713201030',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298209,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298209',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920721',
        'method' => 'get',
      ),
    ),
  ),
  2464220151 => 
  array (
    'id' => 14920751,
    'name' => 'ООО "Сибпроминвест"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489729116,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464220151',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920751',
        'method' => 'get',
      ),
    ),
  ),
  2540099809 => 
  array (
    'id' => 14920765,
    'name' => 'ООО  Электроникс ДВ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489729270,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298323,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298323',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2540099809',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хабаровский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298323,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298323',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920765',
        'method' => 'get',
      ),
    ),
  ),
  2536175133 => 
  array (
    'id' => 14920767,
    'name' => 'ООО «Партнер Групп Инжиниринг»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489729361,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298349,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298349',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2536175133',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298349,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298349',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920767',
        'method' => 'get',
      ),
    ),
  ),
  140400134720 => 
  array (
    'id' => 14920779,
    'name' => 'ИП Алексеев Николай Николаевич -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489729514,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '140400134720',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920779',
        'method' => 'get',
      ),
    ),
  ),
  5410773243 => 
  array (
    'id' => 14920791,
    'name' => 'ОО"ЗООСАТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489729697,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298443,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298443',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410773243',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298443,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298443',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920791',
        'method' => 'get',
      ),
    ),
  ),
  3827044017 => 
  array (
    'id' => 14920793,
    'name' => 'ООО "НАЦИОНАЛЬНАЯ ПОЧТОВАЯ СЛУЖБА - БАЙКАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489729722,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298445,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298445',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3827044017',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298445,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298445',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920793',
        'method' => 'get',
      ),
    ),
  ),
  1434002524 => 
  array (
    'id' => 14920813,
    'name' => 'ООО ПКАП "Дельта К"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489729939,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298467,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298467',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1434002524',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298467,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298467',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920813',
        'method' => 'get',
      ),
    ),
  ),
  4909105960 => 
  array (
    'id' => 14920831,
    'name' => 'ООО "Нордтрансстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489730131,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298489,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298489',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4909105960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298489,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298489',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920831',
        'method' => 'get',
      ),
    ),
  ),
  5406632507 => 
  array (
    'id' => 14920863,
    'name' => 'ООО "ПСК "РАЗВИТИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489730469,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406632507',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14920863',
        'method' => 'get',
      ),
    ),
  ),
  2308101407 => 
  array (
    'id' => 14921005,
    'name' => 'ООО "Дорстроймеханизация"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489731677,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298755,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298755',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308101407',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298755,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298755',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921005',
        'method' => 'get',
      ),
    ),
  ),
  1660147001 => 
  array (
    'id' => 14921029,
    'name' => 'ООО "ТРАНСПРОЕКТСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489731882,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298791,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298791',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660147001',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298791,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298791',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921029',
        'method' => 'get',
      ),
    ),
  ),
  550707083800 => 
  array (
    'id' => 14921073,
    'name' => 'ИП Корчагин Андрей Сергеевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489732180,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298867,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298867',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '550707083800',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Омская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298867,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298867',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921073',
        'method' => 'get',
      ),
    ),
  ),
  6454100889 => 
  array (
    'id' => 14921095,
    'name' => 'ООО "ТУРБО-ОПТ""',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489732323,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298893,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298893',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6454100889',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саратовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298893,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298893',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921095',
        'method' => 'get',
      ),
    ),
  ),
  1660177310 => 
  array (
    'id' => 14921107,
    'name' => 'ООО СФ "Восток""',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489732424,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660177310',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921107',
        'method' => 'get',
      ),
    ),
  ),
  860507532027 => 
  array (
    'id' => 14921121,
    'name' => 'ИП  Жмаева Наталья Сергеевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489732523,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298929,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298929',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921397,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '860507532027',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298929,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298929',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921121',
        'method' => 'get',
      ),
    ),
  ),
  4345368988 => 
  array (
    'id' => 14921147,
    'name' => 'ООО "ЦИРКОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489732744,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29298975,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298975',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345368988',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Коми(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29298975,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29298975',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921147',
        'method' => 'get',
      ),
    ),
  ),
  1660183063 => 
  array (
    'id' => 14921177,
    'name' => 'ООО "МОСТПРОЕКТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489732952,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299015,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299015',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660183063',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299015,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299015',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921177',
        'method' => 'get',
      ),
    ),
  ),
  300901854800 => 
  array (
    'id' => 14921187,
    'name' => 'ИП АЖХАРИСОВА БРИЛЛИАНТ ЕНАЛИЕВНА',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489732990,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '300901854800',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Астраханская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921187',
        'method' => 'get',
      ),
    ),
  ),
  701900140675 => 
  array (
    'id' => 14921223,
    'name' => 'ИП Семкин Василий Васильевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489733167,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299057,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299057',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '701900140675',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Томская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921223',
        'method' => 'get',
      ),
    ),
  ),
  5050117067 => 
  array (
    'id' => 14921283,
    'name' => 'ООО "СТАВКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489733590,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299131,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299131',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5050117067',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299131,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299131',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921283',
        'method' => 'get',
      ),
    ),
  ),
  7701270456 => 
  array (
    'id' => 14921355,
    'name' => 'Общество с ограниченной ответственностью "Фэзера"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489733707,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299197,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299197',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701270456',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299197,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299197',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921355',
        'method' => 'get',
      ),
    ),
  ),
  165113658418 => 
  array (
    'id' => 14921377,
    'name' => 'ООО Манеев Айдар Зиятин',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489733853,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '165113658418',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921377',
        'method' => 'get',
      ),
    ),
  ),
  7715216804 => 
  array (
    'id' => 14921589,
    'name' => 'ООО "ИНЛАЙН ТЕХНОЛОДЖИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489735157,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299489,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299489',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715216804',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299489,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299489',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921589',
        'method' => 'get',
      ),
    ),
  ),
  2635825858 => 
  array (
    'id' => 14921637,
    'name' => 'ООО "СТРОЙПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489735372,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299539,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299539',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635825858',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299539,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299539',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921637',
        'method' => 'get',
      ),
    ),
  ),
  3328491524 => 
  array (
    'id' => 14921687,
    'name' => 'ООО "ТОРГОВЫЙ ДОМ "ВКЗ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489735598,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299595,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299595',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3328491524',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299595,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299595',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921687',
        'method' => 'get',
      ),
    ),
  ),
  6501156352 => 
  array (
    'id' => 14921711,
    'name' => 'ООО"Сахалин-запчастьсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489735699,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501156352',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Сахалинская область(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921711',
        'method' => 'get',
      ),
    ),
  ),
  274139540 => 
  array (
    'id' => 14921783,
    'name' => 'ООО "Инфодор-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489736039,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29299715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274139540',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29299715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29299715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14921783',
        'method' => 'get',
      ),
    ),
  ),
  3628007540 => 
  array (
    'id' => 14922075,
    'name' => 'ООО "Дорожник"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489736231,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300025,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300025',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3628007540',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300025,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300025',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922075',
        'method' => 'get',
      ),
    ),
  ),
  3661048776 => 
  array (
    'id' => 14922123,
    'name' => 'ООО "ЗМТ-ЛОГИСТИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489736422,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300073,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300073',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3661048776',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300073,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300073',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922123',
        'method' => 'get',
      ),
    ),
  ),
  3663057078 => 
  array (
    'id' => 14922163,
    'name' => 'ООО ПКФ "СТРОЙМОСТМОНТАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489736577,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300127,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300127',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3663057078',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300127,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300127',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922163',
        'method' => 'get',
      ),
    ),
  ),
  910400018814 => 
  array (
    'id' => 14922175,
    'name' => 'ИП Карев Денис Сергеевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489736606,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300135,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300135',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '910400018814',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300135,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300135',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922175',
        'method' => 'get',
      ),
    ),
  ),
  860301328003 => 
  array (
    'id' => 14922189,
    'name' => 'ИП  Закриев Ваха Турпал - Алиевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489736716,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300159,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300159',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '860301328003',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300159,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300159',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922189',
        'method' => 'get',
      ),
    ),
  ),
  770800673263 => 
  array (
    'id' => 14922237,
    'name' => 'ИП Елистратова Елена Борисовна -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489736964,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300245,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300245',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '770800673263',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300245,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300245',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922237',
        'method' => 'get',
      ),
    ),
  ),
  753702374243 => 
  array (
    'id' => 14922275,
    'name' => 'ИП Гладкова Галина Юрьевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489737118,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300287,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300287',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '753702374243',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Читинская область(GMT +9)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300287,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300287',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922275',
        'method' => 'get',
      ),
    ),
  ),
  742200151319 => 
  array (
    'id' => 14922313,
    'name' => 'ИП Пономарев Эдуард Михайлович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489737410,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300331,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300331',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '742200151319',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300331,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300331',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922313',
        'method' => 'get',
      ),
    ),
  ),
  3730002510 => 
  array (
    'id' => 14922325,
    'name' => 'ЗАО "Ивановоискож"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489737472,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300343,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300343',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3730002510',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300343,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300343',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922325',
        'method' => 'get',
      ),
    ),
  ),
  616501196608 => 
  array (
    'id' => 14922341,
    'name' => 'ИП Грецева Татьяна Григорьевна -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489737533,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '616501196608',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922341',
        'method' => 'get',
      ),
    ),
  ),
  3906208645 => 
  array (
    'id' => 14922379,
    'name' => 'ООО "Калининград МАЗ Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489737737,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300419,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300419',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906208645',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300419,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300419',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922379',
        'method' => 'get',
      ),
    ),
  ),
  4345415300 => 
  array (
    'id' => 14922521,
    'name' => 'ООО "ФАДЭЛЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489738398,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29300609,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300609',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345415300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29300609,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29300609',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922521',
        'method' => 'get',
      ),
    ),
  ),
  561406129200 => 
  array (
    'id' => 14922807,
    'name' => 'ИП Собиров Мохирджон Усмонович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489739563,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '561406129200',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922807',
        'method' => 'get',
      ),
    ),
  ),
  504800196280 => 
  array (
    'id' => 14922837,
    'name' => 'ИП Иванова Елена Владимировна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489739679,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '504800196280',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922837',
        'method' => 'get',
      ),
    ),
  ),
  421407189612 => 
  array (
    'id' => 14922903,
    'name' => 'ИП Волгина Ольга Николаевна -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489739802,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301107,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301107',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '421407189612',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кемеровская область(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301107,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301107',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922903',
        'method' => 'get',
      ),
    ),
  ),
  271500737806 => 
  array (
    'id' => 14922985,
    'name' => 'ИП Иванов Анатолий Викторович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489740114,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '271500737806',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хабаровский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14922985',
        'method' => 'get',
      ),
    ),
  ),
  230906277221 => 
  array (
    'id' => 14923043,
    'name' => 'ИП  Трофимова Людмила Сергеевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489740361,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301275,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301275',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '230906277221',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301275,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301275',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14923043',
        'method' => 'get',
      ),
    ),
  ),
  222212679080 => 
  array (
    'id' => 14923101,
    'name' => 'ИП Юстус Кристина Давыдовна -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489740636,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301363,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301363',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '222212679080',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301363,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301363',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14923101',
        'method' => 'get',
      ),
    ),
  ),
  164901493896 => 
  array (
    'id' => 14923167,
    'name' => 'ИП Загыртдинова Рамиля Назиповна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489740937,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301453,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301453',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '164901493896',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301453,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301453',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14923167',
        'method' => 'get',
      ),
    ),
  ),
  5021016272 => 
  array (
    'id' => 14923201,
    'name' => 'ООО "ЗеленХоз"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489741166,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29301509,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301509',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5021016272',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29301509,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29301509',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14923201',
        'method' => 'get',
      ),
    ),
  ),
  5903111053 => 
  array (
    'id' => 14924053,
    'name' => 'ООО "СМАРТ-ТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489744929,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29302555,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302555',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903111053',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29302555,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302555',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924053',
        'method' => 'get',
      ),
    ),
  ),
  2901213706 => 
  array (
    'id' => 14924177,
    'name' => 'ООО«Архангельское специализированное энергетическое предприятие – плюс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489745467,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29302737,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302737',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901213706',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Архангельская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29302737,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302737',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924177',
        'method' => 'get',
      ),
    ),
  ),
  278182547 => 
  array (
    'id' => 14924235,
    'name' => 'ООО "АвтоДорСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489745647,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29302785,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302785',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278182547',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29302785,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302785',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924235',
        'method' => 'get',
      ),
    ),
  ),
  2437010388 => 
  array (
    'id' => 14924273,
    'name' => 'ООО "Турухан"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489745784,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29302839,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302839',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2437010388',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29302839,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302839',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924273',
        'method' => 'get',
      ),
    ),
  ),
  6316108876 => 
  array (
    'id' => 14924331,
    'name' => 'ООО "Лабаналит"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489746015,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29302907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316108876',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29302907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29302907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924331',
        'method' => 'get',
      ),
    ),
  ),
  8603172004 => 
  array (
    'id' => 14924781,
    'name' => 'ООО «ЧОО «ПЕРИМЕТР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489747883,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303397,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303397',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603172004',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303397,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303397',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924781',
        'method' => 'get',
      ),
    ),
  ),
  326006689 => 
  array (
    'id' => 14924821,
    'name' => 'ОАО "Авиакомпания "БУРАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489748087,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303455,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303455',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326006689',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Читинская область(GMT +9)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303455,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303455',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924821',
        'method' => 'get',
      ),
    ),
  ),
  7721291503 => 
  array (
    'id' => 14924837,
    'name' => 'ООО " КАРТГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489748149,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303477,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303477',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721291503',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303477,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303477',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924837',
        'method' => 'get',
      ),
    ),
  ),
  8601032386 => 
  array (
    'id' => 14924893,
    'name' => 'ООО  "Югорская медицинская техника"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489748352,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8601032386',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924893',
        'method' => 'get',
      ),
    ),
  ),
  7730533438 => 
  array (
    'id' => 14924911,
    'name' => 'ООО СК Управление строительства-620',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489748433,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303543,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303543',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730533438',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303543,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303543',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924911',
        'method' => 'get',
      ),
    ),
  ),
  411151616 => 
  array (
    'id' => 14924945,
    'name' => 'ЗАО "МедСтандарт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489748587,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '411151616',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14924945',
        'method' => 'get',
      ),
    ),
  ),
  7118037978 => 
  array (
    'id' => 14925033,
    'name' => 'ООО "ЭКОразвитие"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489749136,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7118037978',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тульская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925033',
        'method' => 'get',
      ),
    ),
  ),
  3525162550 => 
  array (
    'id' => 14925097,
    'name' => 'ООО ИНФОСИТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489749521,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29303755,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303755',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921393,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525162550',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кировская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29303755,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29303755',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925097',
        'method' => 'get',
      ),
    ),
  ),
  7810118490 => 
  array (
    'id' => 14925303,
    'name' => 'АО "Вектор-Бест-Балтика"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489750451,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29304057,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304057',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810118490',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29304057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925303',
        'method' => 'get',
      ),
    ),
  ),
  7816388172 => 
  array (
    'id' => 14925421,
    'name' => 'ООО ПЛАЗ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489751067,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29304183,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304183',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816388172',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Томская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29304183,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304183',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925421',
        'method' => 'get',
      ),
    ),
  ),
  7718803367 => 
  array (
    'id' => 14925453,
    'name' => 'ООО "СИНТЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489751202,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29304213,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304213',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718803367',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29304213,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304213',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925453',
        'method' => 'get',
      ),
    ),
  ),
  7734690939 => 
  array (
    'id' => 14925543,
    'name' => 'ООО"ЭКОЛАЙН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489751588,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29304281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734690939',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29304281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925543',
        'method' => 'get',
      ),
    ),
  ),
  7804382472 => 
  array (
    'id' => 14925761,
    'name' => 'ООО "Служба экспресс-доставки"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489752569,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29304543,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304543',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804382472',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ленинградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29304543,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304543',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925761',
        'method' => 'get',
      ),
    ),
  ),
  6658309818 => 
  array (
    'id' => 14925809,
    'name' => 'ООО "Илар"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489752885,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29304607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658309818',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29304607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29304607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14925809',
        'method' => 'get',
      ),
    ),
  ),
  3327116440 => 
  array (
    'id' => 14926163,
    'name' => 'ООО "МЕДТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489754771,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29305097,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29305097',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3327116440',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29305097,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29305097',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14926163',
        'method' => 'get',
      ),
    ),
  ),
  323363481 => 
  array (
    'id' => 14936009,
    'name' => 'ООО Гранит',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489980735,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29315835,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315835',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323363481',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29315835,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315835',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936009',
        'method' => 'get',
      ),
    ),
  ),
  1435184570 => 
  array (
    'id' => 14936013,
    'name' => 'ООО "Сахаспецмонтаж-95"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489981068,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29315847,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315847',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435184570',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29315847,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315847',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936013',
        'method' => 'get',
      ),
    ),
  ),
  2466123241 => 
  array (
    'id' => 14936047,
    'name' => 'ООО УСК "СИБИРЯК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489981713,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29315907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466123241',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29315907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936047',
        'method' => 'get',
      ),
    ),
  ),
  6658447938 => 
  array (
    'id' => 14936107,
    'name' => 'ООО "СК "ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489982791,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29315973,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315973',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658447938',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29315973,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315973',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936107',
        'method' => 'get',
      ),
    ),
  ),
  6670170224 => 
  array (
    'id' => 14936159,
    'name' => 'ООО "Вест"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489983775,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316097,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316097',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670170224',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316097,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316097',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936159',
        'method' => 'get',
      ),
    ),
  ),
  6670430296 => 
  array (
    'id' => 14936167,
    'name' => 'ООО «Капитал-медсервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489983830,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316105,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316105',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670430296',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316105,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316105',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936167',
        'method' => 'get',
      ),
    ),
  ),
  12216407 => 
  array (
    'id' => 14936619,
    'name' => 'ООО "БЛАГОУСТРОЙСТВО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489988125,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '12216407',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936619',
        'method' => 'get',
      ),
    ),
  ),
  7825699336 => 
  array (
    'id' => 14936631,
    'name' => 'ООО "Петербургская реставрационная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489988252,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316661,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316661',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7825699336',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316661,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316661',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936631',
        'method' => 'get',
      ),
    ),
  ),
  6163139360 => 
  array (
    'id' => 14936647,
    'name' => 'ООО «АЛЬФА-ЮГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489988399,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316675,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316675',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6163139360',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316675,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316675',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936647',
        'method' => 'get',
      ),
    ),
  ),
  7717018237 => 
  array (
    'id' => 14936665,
    'name' => 'ООО "АВТОРАДИО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489988691,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717018237',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936665',
        'method' => 'get',
      ),
    ),
  ),
  7713775589 => 
  array (
    'id' => 14936709,
    'name' => 'ООО"КОМПАНИЯ САЛВЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489989018,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316767,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316767',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713775589',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316767,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316767',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936709',
        'method' => 'get',
      ),
    ),
  ),
  7709735865 => 
  array (
    'id' => 14936739,
    'name' => 'ООО АВС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489989250,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316795,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316795',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709735865',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316795,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316795',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936739',
        'method' => 'get',
      ),
    ),
  ),
  6829116826 => 
  array (
    'id' => 14936793,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ КОМПАНИЯ "МОСТОСТРОЙ 1"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489989667,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29316873,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316873',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829116826',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29316873,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29316873',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936793',
        'method' => 'get',
      ),
    ),
  ),
  573005471 => 
  array (
    'id' => 14938587,
    'name' => 'ООО " ПРОФСТРОЙЮГ "',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489989879,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29320451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '573005471',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29320451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14938587',
        'method' => 'get',
      ),
    ),
  ),
  7707244950 => 
  array (
    'id' => 14938699,
    'name' => 'ООО "Мэйджор-Авто"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489990875,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29320599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921394,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707244950',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тверская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29320599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14938699',
        'method' => 'get',
      ),
    ),
  ),
  5047067835 => 
  array (
    'id' => 14938729,
    'name' => 'ООО "Атлант-Ойл"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489991084,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29320647,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320647',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047067835',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29320647,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320647',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14938729',
        'method' => 'get',
      ),
    ),
  ),
  9108000588 => 
  array (
    'id' => 14938797,
    'name' => 'ООО "КЕДР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489991390,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29320725,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320725',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9108000588',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29320725,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320725',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14938797',
        'method' => 'get',
      ),
    ),
  ),
  7806405460 => 
  array (
    'id' => 14938919,
    'name' => 'ООО  "ПитерБасЦентр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489991915,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29320881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806405460',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29320881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14938919',
        'method' => 'get',
      ),
    ),
  ),
  505200515000 => 
  array (
    'id' => 14938955,
    'name' => 'ИП  Пащенко Илья Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489992186,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29320945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '505200515000',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29320945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29320945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14938955',
        'method' => 'get',
      ),
    ),
  ),
  1655052900 => 
  array (
    'id' => 14939069,
    'name' => 'ООО "АльтКом"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489992892,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29321115,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321115',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655052900',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29321115,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321115',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939069',
        'method' => 'get',
      ),
    ),
  ),
  6452075651 => 
  array (
    'id' => 14939095,
    'name' => 'ООО  "Торгтехоборудование"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489993052,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29321141,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321141',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6452075651',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29321141,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321141',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939095',
        'method' => 'get',
      ),
    ),
  ),
  7733240239 => 
  array (
    'id' => 14939203,
    'name' => 'ООО «ГАРАНТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489993543,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29321307,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321307',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733240239',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29321307,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321307',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939203',
        'method' => 'get',
      ),
    ),
  ),
  7802589601 => 
  array (
    'id' => 14939273,
    'name' => 'ООО "МЕРОМАКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489993825,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29321397,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321397',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802589601',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29321397,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321397',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939273',
        'method' => 'get',
      ),
    ),
  ),
  7737054681 => 
  array (
    'id' => 14939295,
    'name' => 'ООО  «Центр социального туризма и лечебно-оздоровительной реабилитации населения»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489993936,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29321421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7737054681',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29321421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939295',
        'method' => 'get',
      ),
    ),
  ),
  7730610643 => 
  array (
    'id' => 14939337,
    'name' => 'ООО  "КомплектПоставка"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489994213,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29321485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730610643',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29321485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939337',
        'method' => 'get',
      ),
    ),
  ),
  7721555026 => 
  array (
    'id' => 14939477,
    'name' => 'ООО " Персон Элит"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489995008,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29321721,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321721',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721555026',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29321721,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29321721',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939477',
        'method' => 'get',
      ),
    ),
  ),
  5257145568 => 
  array (
    'id' => 14939805,
    'name' => 'ООО "Альтаир"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489995889,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29322097,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322097',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5257145568',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саратовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29322097,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322097',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939805',
        'method' => 'get',
      ),
    ),
  ),
  6449056056 => 
  array (
    'id' => 14939831,
    'name' => 'ООО "Ф-авто"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489996054,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29322163,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322163',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6449056056',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саратовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29322163,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322163',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939831',
        'method' => 'get',
      ),
    ),
  ),
  7801575469 => 
  array (
    'id' => 14939863,
    'name' => 'ООО  «ЛабСтар»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489996168,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29322211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801575469',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29322211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939863',
        'method' => 'get',
      ),
    ),
  ),
  7453261821 => 
  array (
    'id' => 14939919,
    'name' => 'ООО "ГК ПЕЙДЖЕРКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489996372,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29322277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7453261821',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29322277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14939919',
        'method' => 'get',
      ),
    ),
  ),
  7729790039 => 
  array (
    'id' => 14940001,
    'name' => 'ООО  «МВСервис-групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489996719,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29322405,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322405',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729790039',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29322405,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29322405',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14940001',
        'method' => 'get',
      ),
    ),
  ),
  6316110547 => 
  array (
    'id' => 14940639,
    'name' => 'ООО  "Славком"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489998981,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29323357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29323357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316110547',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29323357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29323357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14940639',
        'method' => 'get',
      ),
    ),
  ),
  4706037994 => 
  array (
    'id' => 14940697,
    'name' => 'ООО "Элемент"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489999123,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29323413,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29323413',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4706037994',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29323413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29323413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14940697',
        'method' => 'get',
      ),
    ),
  ),
  9204015925 => 
  array (
    'id' => 14941067,
    'name' => 'ООО  "ЗОО-ДЖУНГЛИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490000425,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29323899,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29323899',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9204015925',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29323899,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29323899',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14941067',
        'method' => 'get',
      ),
    ),
  ),
  708013670 => 
  array (
    'id' => 14941687,
    'name' => 'ООО "Альфа Трейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490002621,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29324519,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29324519',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605888,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '708013670',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29324519,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29324519',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14941687',
        'method' => 'get',
      ),
    ),
  ),
  1101487569 => 
  array (
    'id' => 14941987,
    'name' => 'ГУП РК "ГАРК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490003650,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29324807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29324807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1101487569',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29324807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29324807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14941987',
        'method' => 'get',
      ),
    ),
  ),
  6950042722 => 
  array (
    'id' => 14942183,
    'name' => 'ООО "Чистый город"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490004229,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29325013,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325013',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500381665,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950042722',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29325013,
        1 => 29325039,
        2 => 29327025,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325013,29325039,29327025',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14942183',
        'method' => 'get',
      ),
    ),
  ),
  2129056123 => 
  array (
    'id' => 14942483,
    'name' => 'ООО "ЗЕМЛЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490004682,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29325317,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325317',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2129056123',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29325317,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325317',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14942483',
        'method' => 'get',
      ),
    ),
  ),
  6362010040 => 
  array (
    'id' => 14942549,
    'name' => 'ООО "Сигма"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490004867,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29325385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6362010040',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29325385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14942549',
        'method' => 'get',
      ),
    ),
  ),
  1328903647 => 
  array (
    'id' => 14942581,
    'name' => 'ООО МАПО-ТРАНС',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490005004,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29325425,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325425',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1328903647',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29325425,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325425',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14942581',
        'method' => 'get',
      ),
    ),
  ),
  1660145318 => 
  array (
    'id' => 14942713,
    'name' => 'ООО «Акмедика»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490005496,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29325587,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325587',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660145318',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29325587,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325587',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14942713',
        'method' => 'get',
      ),
    ),
  ),
  2005265082 => 
  array (
    'id' => 14943355,
    'name' => 'ООО  Фирма "Лидер"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490008081,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29326423,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29326423',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2005265082',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29326423,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29326423',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14943355',
        'method' => 'get',
      ),
    ),
  ),
  2302039431 => 
  array (
    'id' => 14943509,
    'name' => 'ООО ПКФ "ДТК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490008636,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29326611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29326611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2302039431',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29326611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29326611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14943509',
        'method' => 'get',
      ),
    ),
  ),
  344600260435 => 
  array (
    'id' => 14944361,
    'name' => 'ИП Горбунов Виталий Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490012421,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29327613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29327613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '344600260435',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29327613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29327613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944361',
        'method' => 'get',
      ),
    ),
  ),
  2625025027 => 
  array (
    'id' => 14944441,
    'name' => 'Георгиевское МУП ЖКХ',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490012790,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29327713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29327713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2625025027',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29327713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29327713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944441',
        'method' => 'get',
      ),
    ),
  ),
  616130956048 => 
  array (
    'id' => 14944539,
    'name' => 'ИП Кривцова Карина Владимировна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490013278,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29327819,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29327819',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921390,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '616130956048',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29327819,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29327819',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944539',
        'method' => 'get',
      ),
    ),
  ),
  3257001812 => 
  array (
    'id' => 14944773,
    'name' => 'ООО "АДАМАНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490014056,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328089,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328089',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3257001812',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328089,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328089',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944773',
        'method' => 'get',
      ),
    ),
  ),
  366406547708 => 
  array (
    'id' => 14944791,
    'name' => 'ИП САНДЛЕР АНДРЕЙ СЕРГЕЕВИЧ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490014133,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328111,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328111',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '366406547708',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328111,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328111',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944791',
        'method' => 'get',
      ),
    ),
  ),
  3435313036 => 
  array (
    'id' => 14944807,
    'name' => 'ООО "Квадра"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490014225,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328139,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328139',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435313036',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328139,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328139',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944807',
        'method' => 'get',
      ),
    ),
  ),
  614200456217 => 
  array (
    'id' => 14944809,
    'name' => 'ИП Свинарев Андрей Павлович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490014241,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328143,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328143',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '614200456217',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328143,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328143',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944809',
        'method' => 'get',
      ),
    ),
  ),
  720321158200 => 
  array (
    'id' => 14944857,
    'name' => 'ИП Третьяков Роман Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490014434,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328205,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328205',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '720321158200',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328205,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328205',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944857',
        'method' => 'get',
      ),
    ),
  ),
  3525251063 => 
  array (
    'id' => 14944875,
    'name' => 'ООО ЛИЛИЯ',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490014537,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328227,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328227',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525251063',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328227,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328227',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944875',
        'method' => 'get',
      ),
    ),
  ),
  323305022020 => 
  array (
    'id' => 14944895,
    'name' => 'ИП Седыкин Александр Николаевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490014596,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323305022020',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14944895',
        'method' => 'get',
      ),
    ),
  ),
  781905290851 => 
  array (
    'id' => 14945059,
    'name' => 'ИП Иванов Денис Сергеевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490015271,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '781905290851',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ленинградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14945059',
        'method' => 'get',
      ),
    ),
  ),
  616700299686 => 
  array (
    'id' => 14945125,
    'name' => 'ИП Попов Александр Валерьевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490015551,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328499,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328499',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '616700299686',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328499,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328499',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14945125',
        'method' => 'get',
      ),
    ),
  ),
  4821000181 => 
  array (
    'id' => 14945285,
    'name' => 'МУП "Спецавтотранс" г.Ельца',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490016338,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328809,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328809',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4821000181',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328809,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328809',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14945285',
        'method' => 'get',
      ),
    ),
  ),
  920455582695 => 
  array (
    'id' => 14945317,
    'name' => 'ИП Заиченко Борис Иванович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490016462,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328845,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328845',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '920455582695',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14945317',
        'method' => 'get',
      ),
    ),
  ),
  5001073882 => 
  array (
    'id' => 14945339,
    'name' => 'ООО «Крафт групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490016530,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5001073882',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14945339',
        'method' => 'get',
      ),
    ),
  ),
  661701975080 => 
  array (
    'id' => 14945429,
    'name' => 'ИП  Лемешко Сергей Дмитриевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490016852,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29328961,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328961',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '661701975080',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29328961,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29328961',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14945429',
        'method' => 'get',
      ),
    ),
  ),
  2204036023 => 
  array (
    'id' => 14948265,
    'name' => 'ООО "СУ-125"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490067810,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333739,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333739',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2204036023',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333739,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333739',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948265',
        'method' => 'get',
      ),
    ),
  ),
  4501104858 => 
  array (
    'id' => 14948273,
    'name' => 'ООО "Вестстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490067993,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333745,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333745',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4501104858',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333745,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333745',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948273',
        'method' => 'get',
      ),
    ),
  ),
  4505200979 => 
  array (
    'id' => 14948283,
    'name' => 'АО "Варгашинское предприятие по строительству, ремонту и содержанию автомобильных дорог"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490068192,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333755,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333755',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4505200979',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333755,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333755',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948283',
        'method' => 'get',
      ),
    ),
  ),
  5401258856 => 
  array (
    'id' => 14948291,
    'name' => 'ООО "ДЕВЕЛОПМЕНТ ГРУП"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490068354,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333767,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333767',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5401258856',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333767,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333767',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948291',
        'method' => 'get',
      ),
    ),
  ),
  5401328567 => 
  array (
    'id' => 14948299,
    'name' => 'ООО "СиТрейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490068486,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333775,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333775',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5401328567',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333775,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333775',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948299',
        'method' => 'get',
      ),
    ),
  ),
  5403354851 => 
  array (
    'id' => 14948327,
    'name' => 'ООО «Стаер»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490068954,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333813,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333813',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5403354851',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333813,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333813',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948327',
        'method' => 'get',
      ),
    ),
  ),
  5406137559 => 
  array (
    'id' => 14948333,
    'name' => 'ООО "НИТЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490069157,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333823,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333823',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 1514235540,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406137559',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333823,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333823',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948333',
        'method' => 'get',
      ),
    ),
  ),
  5407496416 => 
  array (
    'id' => 14948341,
    'name' => 'ООО "ТРАНСЕРВИС-РЕГИОНАЛЬНЫЕ ПРОДАЖИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490069383,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333829,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333829',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407496416',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333829,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333829',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948341',
        'method' => 'get',
      ),
    ),
  ),
  5408277505 => 
  array (
    'id' => 14948353,
    'name' => 'ООО "Реолгрейд сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490069572,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333849,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333849',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5408277505',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333849,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333849',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948353',
        'method' => 'get',
      ),
    ),
  ),
  5501220260 => 
  array (
    'id' => 14948395,
    'name' => 'ООО "ТЕРПЛАНПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490070111,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333889,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333889',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5501220260',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333889,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333889',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948395',
        'method' => 'get',
      ),
    ),
  ),
  5503227328 => 
  array (
    'id' => 14948401,
    'name' => 'ООО "Отечество"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490070228,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29333895,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333895',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503227328',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29333895,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29333895',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948401',
        'method' => 'get',
      ),
    ),
  ),
  3662157961 => 
  array (
    'id' => 14948591,
    'name' => 'ООО"Машпринт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490073017,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334119,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334119',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662157961',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тамбовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334119,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334119',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948591',
        'method' => 'get',
      ),
    ),
  ),
  1001029678 => 
  array (
    'id' => 14948645,
    'name' => 'Первичная студенческая организация профсоюза работников народного образования и науки РФ Петрозаводского государственного университета',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490073486,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334195,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334195',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1001029678',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Карелия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334195,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334195',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948645',
        'method' => 'get',
      ),
    ),
  ),
  2721190295 => 
  array (
    'id' => 14948753,
    'name' => 'ООО "Транс Гид"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490074596,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334323,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334323',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721190295',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хабаровский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334323,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334323',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948753',
        'method' => 'get',
      ),
    ),
  ),
  7801261913 => 
  array (
    'id' => 14948765,
    'name' => 'ООО  "Городской центр оценки"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490074763,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801261913',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948765',
        'method' => 'get',
      ),
    ),
  ),
  233612189592 => 
  array (
    'id' => 14948797,
    'name' => 'ООО  Рудь Александр Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490075081,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334389,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334389',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '233612189592',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334389,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334389',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948797',
        'method' => 'get',
      ),
    ),
  ),
  6350014850 => 
  array (
    'id' => 14948821,
    'name' => 'ООО «Аккорд»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490075277,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334413,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334413',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6350014850',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948821',
        'method' => 'get',
      ),
    ),
  ),
  860104289115 => 
  array (
    'id' => 14948845,
    'name' => 'ИП Иванов Максим Анатольевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490075600,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334445,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334445',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '860104289115',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334445,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334445',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948845',
        'method' => 'get',
      ),
    ),
  ),
  121508738797 => 
  array (
    'id' => 14948851,
    'name' => 'ИП Пуртов Сергей Витальевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490075714,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '121508738797',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Марий Эл(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948851',
        'method' => 'get',
      ),
    ),
  ),
  262806352767 => 
  array (
    'id' => 14948861,
    'name' => 'ИП  Головин Роман Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490075880,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334463,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334463',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '262806352767',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334463,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334463',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948861',
        'method' => 'get',
      ),
    ),
  ),
  253600350715 => 
  array (
    'id' => 14948885,
    'name' => 'ИП Алтунина Галина Александровна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490076029,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334489,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334489',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '253600350715',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Приморский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334489,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334489',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948885',
        'method' => 'get',
      ),
    ),
  ),
  7724647050 => 
  array (
    'id' => 14949009,
    'name' => 'ООО "АНТАНТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490076374,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334647,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334647',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724647050',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334647,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334647',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14949009',
        'method' => 'get',
      ),
    ),
  ),
  7718228406 => 
  array (
    'id' => 14949095,
    'name' => 'ООО «Альфа-Диагностика»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490076975,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334745,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334745',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718228406',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334745,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334745',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14949095',
        'method' => 'get',
      ),
    ),
  ),
  5031105933 => 
  array (
    'id' => 14949133,
    'name' => 'ООО "Новодез-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490077206,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334791,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334791',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921391,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5031105933',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334791,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334791',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14949133',
        'method' => 'get',
      ),
    ),
  ),
  7709863592 => 
  array (
    'id' => 14949191,
    'name' => 'ООО "Юнион"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490077603,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334887,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334887',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709863592',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334887,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334887',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14949191',
        'method' => 'get',
      ),
    ),
  ),
  4312125274 => 
  array (
    'id' => 14949243,
    'name' => 'ООО "Иста-ремонт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490077907,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4312125274',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кировская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14949243',
        'method' => 'get',
      ),
    ),
  ),
  7801403572 => 
  array (
    'id' => 14949285,
    'name' => 'ООО "3С"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490078163,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334991,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334991',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801403572',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334991,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334991',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14949285',
        'method' => 'get',
      ),
    ),
  ),
  7449130696 => 
  array (
    'id' => 14949323,
    'name' => 'ООО   Нефтяная Компания  УРАЛ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490078330,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29335029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29335029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7449130696',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29335029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29335029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14949323',
        'method' => 'get',
      ),
    ),
  ),
  230602086909 => 
  array (
    'id' => 14950217,
    'name' => 'ИП Ворчик Александр Евгеньевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490079531,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29335987,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29335987',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '230602086909',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29335987,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29335987',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950217',
        'method' => 'get',
      ),
    ),
  ),
  532109902701 => 
  array (
    'id' => 14950247,
    'name' => 'ИП Баранова Надежда Анатольевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490079748,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29336045,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336045',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '532109902701',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новгородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29336045,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336045',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950247',
        'method' => 'get',
      ),
    ),
  ),
  5032200065 => 
  array (
    'id' => 14950271,
    'name' => 'ОАО "БАРВИХА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490079908,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29336081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921392,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032200065',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29336081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950271',
        'method' => 'get',
      ),
    ),
  ),
  1701048242 => 
  array (
    'id' => 14950379,
    'name' => 'ООО "Се-ри"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490080385,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29336239,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336239',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1701048242',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тыва(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29336239,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336239',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950379',
        'method' => 'get',
      ),
    ),
  ),
  5504239615 => 
  array (
    'id' => 14950535,
    'name' => 'ООО «ТД СНК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490080965,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29336403,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336403',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5504239615',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Омская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29336403,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336403',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950535',
        'method' => 'get',
      ),
    ),
  ),
  1840048190 => 
  array (
    'id' => 14950583,
    'name' => 'ООО «Комос-Медиа»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490081181,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29336469,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336469',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1840048190',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Удмуртия(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29336469,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29336469',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950583',
        'method' => 'get',
      ),
    ),
  ),
  772206355710 => 
  array (
    'id' => 14950823,
    'name' => 'ИП Переслегина Татьяна Николаевна -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490082413,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29339689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29339689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '772206355710',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29339689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29339689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950823',
        'method' => 'get',
      ),
    ),
  ),
  6686015979 => 
  array (
    'id' => 14950873,
    'name' => 'ООО  "СтройМонтажСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490082626,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29340041,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29340041',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6686015979',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29340041,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29340041',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950873',
        'method' => 'get',
      ),
    ),
  ),
  382800621602 => 
  array (
    'id' => 14950895,
    'name' => 'ИП Андреева Надежда Викторовна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490082753,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29340075,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29340075',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '382800621602',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29340075,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29340075',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950895',
        'method' => 'get',
      ),
    ),
  ),
  501801319891 => 
  array (
    'id' => 14950931,
    'name' => 'ИП Романова Елена Анатольевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490082860,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29340485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29340485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '501801319891',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29340485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29340485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14950931',
        'method' => 'get',
      ),
    ),
  ),
  7328505679 => 
  array (
    'id' => 14951123,
    'name' => 'ООО "РОДОНИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490083714,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29345641,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29345641',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7328505679',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саратовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29345641,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29345641',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14951123',
        'method' => 'get',
      ),
    ),
  ),
  7713288722 => 
  array (
    'id' => 14951323,
    'name' => 'ООО «ЛенсСтрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490084604,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29348115,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29348115',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713288722',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Коми(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29348115,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29348115',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14951323',
        'method' => 'get',
      ),
    ),
  ),
  7728769355 => 
  array (
    'id' => 14951513,
    'name' => 'ООО "Дилена"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490085569,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29348355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29348355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728769355',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29348355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29348355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14951513',
        'method' => 'get',
      ),
    ),
  ),
  615508371840 => 
  array (
    'id' => 14951549,
    'name' => 'ИП Кузьменко Александр Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490085754,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29348415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29348415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '615508371840',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29348415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29348415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14951549',
        'method' => 'get',
      ),
    ),
  ),
  3441027636 => 
  array (
    'id' => 14953643,
    'name' => 'ООО Тандем-авто',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490092576,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29350749,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29350749',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3441027636',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29350749,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29350749',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14953643',
        'method' => 'get',
      ),
    ),
  ),
  301002015813 => 
  array (
    'id' => 14953697,
    'name' => 'ИП Орлова Ольга Юрьевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490092865,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29350827,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29350827',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '301002015813',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Астраханская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29350827,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29350827',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14953697',
        'method' => 'get',
      ),
    ),
  ),
  2626000522 => 
  array (
    'id' => 14953755,
    'name' => 'ООО "Ессентуки-хлеб"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490093036,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29350903,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29350903',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2626000522',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29350903,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29350903',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14953755',
        'method' => 'get',
      ),
    ),
  ),
  2626032482 => 
  array (
    'id' => 14953831,
    'name' => 'ООО "МАКСИМА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490093292,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351003,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351003',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2626032482',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351003,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351003',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14953831',
        'method' => 'get',
      ),
    ),
  ),
  6229078784 => 
  array (
    'id' => 14953895,
    'name' => 'ООО "Форт Трейд Логистик"',
    'responsible_user_id' => 1338850,
    'created_by' => 1261428,
    'created_at' => 1490093584,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351085,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351085',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500381658,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6229078784',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3 (рязанская обл.)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351085,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351085',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14953895',
        'method' => 'get',
      ),
    ),
  ),
  71508249555 => 
  array (
    'id' => 14954075,
    'name' => 'ИП Маршенкулов Альберт Генудинович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490094274,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351299,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351299',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '71508249555',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кабардино-Балкария(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351299,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351299',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954075',
        'method' => 'get',
      ),
    ),
  ),
  7719427651 => 
  array (
    'id' => 14954131,
    'name' => 'ООО "НАЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490094540,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351391,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351391',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719427651',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351391,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351391',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954131',
        'method' => 'get',
      ),
    ),
  ),
  230900344721 => 
  array (
    'id' => 14954151,
    'name' => 'ИП Мошхоев Магомет Мусаевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490094626,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351455,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351455',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '230900344721',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351455,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351455',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954151',
        'method' => 'get',
      ),
    ),
  ),
  2635826058 => 
  array (
    'id' => 14954333,
    'name' => 'ООО "ФУДЗИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490095571,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635826058',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954333',
        'method' => 'get',
      ),
    ),
  ),
  3201001360 => 
  array (
    'id' => 14954395,
    'name' => 'Муниципальное унитарное специализированное предприятие по вопросам похоронного дела г.Брянска',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490095885,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3201001360',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954395',
        'method' => 'get',
      ),
    ),
  ),
  7702811228 => 
  array (
    'id' => 14954415,
    'name' => 'ООО "ТАКСИ ВИП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490095960,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351853,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351853',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702811228',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351853,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351853',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954415',
        'method' => 'get',
      ),
    ),
  ),
  7706753016 => 
  array (
    'id' => 14954525,
    'name' => 'ООО «НЕЙРО-Н»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490096427,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29351995,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351995',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706753016',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29351995,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29351995',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954525',
        'method' => 'get',
      ),
    ),
  ),
  1215001510 => 
  array (
    'id' => 14954573,
    'name' => 'ООО  "Научно-производственная фирма "Геникс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490096685,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352059,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352059',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1215001510',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Марий Эл(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352059,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352059',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954573',
        'method' => 'get',
      ),
    ),
  ),
  7814491223 => 
  array (
    'id' => 14954597,
    'name' => 'ООО "ЛМП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490096798,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352087,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352087',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814491223',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352087,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352087',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954597',
        'method' => 'get',
      ),
    ),
  ),
  7703794430 => 
  array (
    'id' => 14954671,
    'name' => 'ООО "ИНФОКОНСАЛТИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490097097,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352183,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352183',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703794430',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352183,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352183',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954671',
        'method' => 'get',
      ),
    ),
  ),
  7806314510 => 
  array (
    'id' => 14954743,
    'name' => 'ООО ЧОП "ГРИФОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490097423,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352283,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352283',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806314510',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352283,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352283',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954743',
        'method' => 'get',
      ),
    ),
  ),
  'Санкт-Петербург(GMT +3)' => 
  array (
    'id' => 14954787,
    'name' => 'ООО "ПАСКАЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490097605,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805482261',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954787',
        'method' => 'get',
      ),
    ),
  ),
  5406718095 => 
  array (
    'id' => 14954829,
    'name' => 'ООО "Сибирская Энергосервисная Компания',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490097790,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406718095',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954829',
        'method' => 'get',
      ),
    ),
  ),
  3525011368 => 
  array (
    'id' => 14954873,
    'name' => 'МУП "Ритуал-спецслужба"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490097933,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352473,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352473',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525011368',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352473,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352473',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954873',
        'method' => 'get',
      ),
    ),
  ),
  4025075189 => 
  array (
    'id' => 14954977,
    'name' => 'ООО "МЕДИНФО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490098255,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4025075189',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14954977',
        'method' => 'get',
      ),
    ),
  ),
  7816143670 => 
  array (
    'id' => 14955023,
    'name' => 'ООО "СОФТЛЭНД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490098523,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352675,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352675',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816143670',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352675,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352675',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955023',
        'method' => 'get',
      ),
    ),
  ),
  6950145397 => 
  array (
    'id' => 14955149,
    'name' => 'ООО "Энергобаланс - Регион Учет"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490098973,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352875,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352875',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950145397',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тверская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352875,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352875',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955149',
        'method' => 'get',
      ),
    ),
  ),
  7414006465 => 
  array (
    'id' => 14955195,
    'name' => 'МПП ГОРТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490099166,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29352939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7414006465',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29352939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29352939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955195',
        'method' => 'get',
      ),
    ),
  ),
  7714395501 => 
  array (
    'id' => 14955283,
    'name' => 'ООО «Столичное благоустройство»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490099503,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29353031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714395501',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29353031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955283',
        'method' => 'get',
      ),
    ),
  ),
  7716781982 => 
  array (
    'id' => 14955345,
    'name' => 'ООО "ВОСХОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490099813,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29353117,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353117',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716781982',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29353117,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353117',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955345',
        'method' => 'get',
      ),
    ),
  ),
  5249115591 => 
  array (
    'id' => 14955353,
    'name' => 'ООО "Дзержинский лесхоз"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490099925,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29353157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5249115591',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Нижегородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29353157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955353',
        'method' => 'get',
      ),
    ),
  ),
  7842384671 => 
  array (
    'id' => 14955443,
    'name' => 'ООО ЖКС № 3 Центрального района',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490100331,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29353281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842384671',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29353281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955443',
        'method' => 'get',
      ),
    ),
  ),
  7718611665 => 
  array (
    'id' => 14955491,
    'name' => 'ООО "НПЦ РиФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490100529,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29353359,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353359',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921388,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718611665',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29353359,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353359',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955491',
        'method' => 'get',
      ),
    ),
  ),
  7719665494 => 
  array (
    'id' => 14955615,
    'name' => 'ООО  "Оконные технологии - Восток"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490101050,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29353505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719665494',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29353505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29353505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14955615',
        'method' => 'get',
      ),
    ),
  ),
  5072004116 => 
  array (
    'id' => 14956125,
    'name' => 'ООО "Стерин"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490103426,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29354163,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29354163',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5072004116',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29354163,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29354163',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14956125',
        'method' => 'get',
      ),
    ),
  ),
  7718980292 => 
  array (
    'id' => 14956163,
    'name' => 'ООО  "СУГУР-БАЙКАЛ АЛЬЯНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490103604,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29354211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29354211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718980292',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29354211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29354211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14956163',
        'method' => 'get',
      ),
    ),
  ),
  278173214 => 
  array (
    'id' => 14958963,
    'name' => 'ООО ЧОП 7.62',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490154289,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357117,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357117',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278173214',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357117,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357117',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14958963',
        'method' => 'get',
      ),
    ),
  ),
  1435308280 => 
  array (
    'id' => 14958973,
    'name' => 'ООО "ФАРМБАЗА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490154609,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435308280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14958973',
        'method' => 'get',
      ),
    ),
  ),
  2222839827 => 
  array (
    'id' => 14958989,
    'name' => 'ООО "ПряниковЪ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490155006,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357155,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357155',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2222839827',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357155,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357155',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14958989',
        'method' => 'get',
      ),
    ),
  ),
  2465290560 => 
  array (
    'id' => 14959021,
    'name' => 'ООО "Строй-СИТИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490155732,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357187,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357187',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465290560',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357187,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357187',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959021',
        'method' => 'get',
      ),
    ),
  ),
  2536289557 => 
  array (
    'id' => 14959029,
    'name' => 'ООО "ТОРГОВО-ПРОИЗВОДСТВЕННАЯ КОМПАНИЯ "СВЕТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490155856,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357195,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357195',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2536289557',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357195,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357195',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959029',
        'method' => 'get',
      ),
    ),
  ),
  2466161695 => 
  array (
    'id' => 14959037,
    'name' => 'ООО  "Общестрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490156016,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357209,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357209',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466161695',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357209,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357209',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959037',
        'method' => 'get',
      ),
    ),
  ),
  2540072388 => 
  array (
    'id' => 14959055,
    'name' => 'ООО "АЛЬЯНС-ПРИМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490156524,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357239,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357239',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2540072388',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357239,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357239',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959055',
        'method' => 'get',
      ),
    ),
  ),
  2721105170 => 
  array (
    'id' => 14959057,
    'name' => 'ООО «Кадастровый инженер – Партнер»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490156663,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721105170',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959057',
        'method' => 'get',
      ),
    ),
  ),
  4101017329 => 
  array (
    'id' => 14959081,
    'name' => 'ЗАО  "Петропавловск-Камчатский судоремонтный завод"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490157050,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357279,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357279',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101017329',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357279,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357279',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959081',
        'method' => 'get',
      ),
    ),
  ),
  4101084124 => 
  array (
    'id' => 14959089,
    'name' => 'Потребительское общество "Моховской хлеб"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490157125,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357291,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357291',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101084124',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357291,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357291',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959089',
        'method' => 'get',
      ),
    ),
  ),
  5407456501 => 
  array (
    'id' => 14959101,
    'name' => 'ООО "Диалог"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490157448,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357325,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357325',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407456501',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357325,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357325',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959101',
        'method' => 'get',
      ),
    ),
  ),
  5503126922 => 
  array (
    'id' => 14959107,
    'name' => 'ООО «Группа компаний «Мебетек»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490157700,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357343,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357343',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503126922',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357343,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357343',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959107',
        'method' => 'get',
      ),
    ),
  ),
  5903106945 => 
  array (
    'id' => 14959143,
    'name' => 'ООО  "АРМО-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490158223,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357413,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357413',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903106945',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959143',
        'method' => 'get',
      ),
    ),
  ),
  5904255266 => 
  array (
    'id' => 14959155,
    'name' => 'ООО "Веста"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490158354,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357425,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357425',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904255266',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357425,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357425',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959155',
        'method' => 'get',
      ),
    ),
  ),
  5904316014 => 
  array (
    'id' => 14959201,
    'name' => 'ООО "ВЕСТКРАУНД"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490158975,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357467,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357467',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904316014',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357467,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357467',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959201',
        'method' => 'get',
      ),
    ),
  ),
  7839453149 => 
  array (
    'id' => 14959251,
    'name' => 'ООО Альфа Сервис',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490159649,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839453149',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959251',
        'method' => 'get',
      ),
    ),
  ),
  1101015654 => 
  array (
    'id' => 14959267,
    'name' => 'ООО "Первый ремонтно-строительный трест"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490159715,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1101015654',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959267',
        'method' => 'get',
      ),
    ),
  ),
  2015003657 => 
  array (
    'id' => 14959291,
    'name' => 'ООО«Дева 62»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490159879,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357587,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357587',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2015003657',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чечня(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357587,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357587',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959291',
        'method' => 'get',
      ),
    ),
  ),
  380100409903 => 
  array (
    'id' => 14959307,
    'name' => 'ИП Бурков Сергей Геннадьевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490160046,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357605,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357605',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '380100409903',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357605,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357605',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959307',
        'method' => 'get',
      ),
    ),
  ),
  1655338000 => 
  array (
    'id' => 14959311,
    'name' => 'ООО "АРСЕНАЛ""',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490160120,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605887,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655338000',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959311',
        'method' => 'get',
      ),
    ),
  ),
  1658155083 => 
  array (
    'id' => 14959329,
    'name' => 'ООО «МТД ПЛЮС»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490160237,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1658155083',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959329',
        'method' => 'get',
      ),
    ),
  ),
  7714861350 => 
  array (
    'id' => 14959333,
    'name' => 'ООО "Радор М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490160273,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357637,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357637',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714861350',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357637,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357637',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959333',
        'method' => 'get',
      ),
    ),
  ),
  6167107369 => 
  array (
    'id' => 14959347,
    'name' => 'ООО "Релект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490160484,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357653,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357653',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6167107369',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357653,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357653',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959347',
        'method' => 'get',
      ),
    ),
  ),
  1841030439 => 
  array (
    'id' => 14959355,
    'name' => 'ООО "ВИД"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490160532,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357659,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357659',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1841030439',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357659,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357659',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959355',
        'method' => 'get',
      ),
    ),
  ),
  7722755003 => 
  array (
    'id' => 14959373,
    'name' => 'ООО  "ГрандСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490160669,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357681,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357681',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722755003',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357681,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357681',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959373',
        'method' => 'get',
      ),
    ),
  ),
  7325125862 => 
  array (
    'id' => 14959411,
    'name' => 'ООО "Единый ресурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490161180,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7325125862',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959411',
        'method' => 'get',
      ),
    ),
  ),
  2306000026 => 
  array (
    'id' => 14959435,
    'name' => 'ООО "ГРУППА КОМПАНИЙ "ИРБИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490161415,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357739,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357739',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2306000026',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357739,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357739',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959435',
        'method' => 'get',
      ),
    ),
  ),
  2310177989 => 
  array (
    'id' => 14959701,
    'name' => 'ООО "СВЯЗЬРЕСУРС-КУБАНЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490161723,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358009,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358009',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2310177989',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358009,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358009',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959701',
        'method' => 'get',
      ),
    ),
  ),
  7729459966 => 
  array (
    'id' => 14959739,
    'name' => 'ООО  "ДИРЕКЦИЯ РЕСТАВРАЦИОННО-СТРОИТЕЛЬНОГО КОНТРОЛЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490162109,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358055,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358055',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729459966',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358055,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358055',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959739',
        'method' => 'get',
      ),
    ),
  ),
  2901011040 => 
  array (
    'id' => 14959749,
    'name' => 'АО "Архангельский речной порт"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490162271,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358063,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358063',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901011040',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358063,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358063',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959749',
        'method' => 'get',
      ),
    ),
  ),
  7017368518 => 
  array (
    'id' => 14959759,
    'name' => 'ООО  "Автоматизациясервисмонтаж-Томск"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490162437,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017368518',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Томская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959759',
        'method' => 'get',
      ),
    ),
  ),
  2902058965 => 
  array (
    'id' => 14959769,
    'name' => 'ОАО  Белоснежка Северодвинска',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490162532,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358083,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358083',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2902058965',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358083,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358083',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959769',
        'method' => 'get',
      ),
    ),
  ),
  5406633109 => 
  array (
    'id' => 14959781,
    'name' => 'ООО "ТехноДор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490162708,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358097,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358097',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406633109',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358097,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358097',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959781',
        'method' => 'get',
      ),
    ),
  ),
  7713632654 => 
  array (
    'id' => 14960111,
    'name' => 'ООО  "СТРОЙТЕХНОЛОГИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490165449,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713632654',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960111',
        'method' => 'get',
      ),
    ),
  ),
  2983005260 => 
  array (
    'id' => 14960335,
    'name' => 'ООО "Транс-Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490166373,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358851,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358851',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2983005260',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ненецкий АО(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358851,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358851',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960335',
        'method' => 'get',
      ),
    ),
  ),
  7838355332 => 
  array (
    'id' => 14960357,
    'name' => 'ООО  «ЛИК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490166479,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838355332',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960357',
        'method' => 'get',
      ),
    ),
  ),
  7024033350 => 
  array (
    'id' => 14960419,
    'name' => 'АО "ОПЫТНО-ДЕМОНСТРАЦИОННЫЙ ЦЕНТР ВЫВОДА ИЗ ЭКСПЛУАТАЦИИ УРАН-ГРАФИТОВЫХ ЯДЕРНЫХ РЕАКТОРОВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490166776,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29358965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7024033350',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29358965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29358965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960419',
        'method' => 'get',
      ),
    ),
  ),
  3306013700 => 
  array (
    'id' => 14960467,
    'name' => 'ООО "Интэл"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490167043,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3306013700',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960467',
        'method' => 'get',
      ),
    ),
  ),
  3439009703 => 
  array (
    'id' => 14960507,
    'name' => 'ООО "Коммунальщик"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490167137,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3439009703',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960507',
        'method' => 'get',
      ),
    ),
  ),
  6113016884 => 
  array (
    'id' => 14960509,
    'name' => 'ООО "Группа "ТЕХНОКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490167144,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359075,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359075',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921389,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6113016884',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359075,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359075',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960509',
        'method' => 'get',
      ),
    ),
  ),
  3444067732 => 
  array (
    'id' => 14960533,
    'name' => 'ООО "ТЕРРИТОРИАЛЬНОЕ АГЕНТСТВО ВОЗДУШЫХ СООБЩЕНИЙ ВОЛГА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490167336,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359127,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359127',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444067732',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359127,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359127',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960533',
        'method' => 'get',
      ),
    ),
  ),
  7716706174 => 
  array (
    'id' => 14960647,
    'name' => 'ООО  "ПРОЕКТНАЯ МАСТЕРСКАЯ "ТОЧКА СБОРКИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490167899,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716706174',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960647',
        'method' => 'get',
      ),
    ),
  ),
  3619007590 => 
  array (
    'id' => 14960735,
    'name' => 'МУП "Острогожский комбинат по благоустройству"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490168166,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3619007590',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14960735',
        'method' => 'get',
      ),
    ),
  ),
  3661073050 => 
  array (
    'id' => 14961001,
    'name' => 'ООО «ИЗДАТ-ПРИНТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490168680,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359521,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359521',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3661073050',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359521,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359521',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961001',
        'method' => 'get',
      ),
    ),
  ),
  5001048893 => 
  array (
    'id' => 14961445,
    'name' => 'ООО "САМ-МБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490169361,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359727,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359727',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5001048893',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359727,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359727',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961445',
        'method' => 'get',
      ),
    ),
  ),
  5017104247 => 
  array (
    'id' => 14961555,
    'name' => 'ООО «Дорстрой Истра»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490169802,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359859,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359859',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5017104247',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359859,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359859',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961555',
        'method' => 'get',
      ),
    ),
  ),
  5017106519 => 
  array (
    'id' => 14961571,
    'name' => 'ООО "Дорстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490169911,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359887,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359887',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5017106519',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359887,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359887',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961571',
        'method' => 'get',
      ),
    ),
  ),
  616300824115 => 
  array (
    'id' => 14961593,
    'name' => 'ИП Тугушев Марат Рамисович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490169996,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359905,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359905',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '616300824115',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359905,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359905',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961593',
        'method' => 'get',
      ),
    ),
  ),
  5022064857 => 
  array (
    'id' => 14961601,
    'name' => 'ООО "Коломенский УМНР"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490170035,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5022064857',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961601',
        'method' => 'get',
      ),
    ),
  ),
  7813527004 => 
  array (
    'id' => 14961633,
    'name' => 'ООО "Строительное управление № 908"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490170126,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29359947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813527004',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калужская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29359947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29359947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961633',
        'method' => 'get',
      ),
    ),
  ),
  7802735041 => 
  array (
    'id' => 14961753,
    'name' => 'ООО  "ИВК Медикл"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490170390,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29360063,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360063',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802735041',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29360063,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360063',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961753',
        'method' => 'get',
      ),
    ),
  ),
  5032199652 => 
  array (
    'id' => 14961787,
    'name' => 'ОАО "ЖКХ "Горки-Х"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490170524,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29360109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032199652',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29360109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961787',
        'method' => 'get',
      ),
    ),
  ),
  773465140465 => 
  array (
    'id' => 14961845,
    'name' => 'ИП Авдеев Николай Иванович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490170848,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29360191,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360191',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '773465140465',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29360191,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360191',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961845',
        'method' => 'get',
      ),
    ),
  ),
  7802425152 => 
  array (
    'id' => 14961873,
    'name' => 'ООО "СЦ ТЕЛРОС""',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490170920,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29360215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802425152',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29360215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961873',
        'method' => 'get',
      ),
    ),
  ),
  7713206173 => 
  array (
    'id' => 14961895,
    'name' => 'ООО ТРИАЛИНК ГРУП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490171045,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29360247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713206173',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29360247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14961895',
        'method' => 'get',
      ),
    ),
  ),
  771465049004 => 
  array (
    'id' => 14962045,
    'name' => 'ИП Микрикова Любовь Викторовна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490171549,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29360423,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360423',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '771465049004',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29360423,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29360423',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14962045',
        'method' => 'get',
      ),
    ),
  ),
  7721412290 => 
  array (
    'id' => 14963285,
    'name' => 'ООО  «ВЕКТОР-М»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490177007,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29361929,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29361929',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721412290',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калужская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29361929,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29361929',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14963285',
        'method' => 'get',
      ),
    ),
  ),
  6312091861 => 
  array (
    'id' => 14963299,
    'name' => 'ООО  "АПЕЛЬСИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490177108,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29361947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29361947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312091861',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29361947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29361947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14963299',
        'method' => 'get',
      ),
    ),
  ),
  7802760369 => 
  array (
    'id' => 14963571,
    'name' => 'ООО  "Синтез"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490178196,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29362241,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362241',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802760369',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Псковская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29362241,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362241',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14963571',
        'method' => 'get',
      ),
    ),
  ),
  165501543939 => 
  array (
    'id' => 14964125,
    'name' => 'ИП САЙФУЛЛИНА ЛЯЙЛЯ ШАВКАТОВНА',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490180803,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29362931,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362931',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '165501543939',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29362931,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362931',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964125',
        'method' => 'get',
      ),
    ),
  ),
  7722749987 => 
  array (
    'id' => 14964145,
    'name' => 'ООО "Аргумент"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490180909,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29362951,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362951',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722749987',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29362951,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362951',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964145',
        'method' => 'get',
      ),
    ),
  ),
  5038116797 => 
  array (
    'id' => 14964169,
    'name' => 'ООО «ПродПоставка»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490181002,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29362991,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362991',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5038116797',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29362991,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29362991',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964169',
        'method' => 'get',
      ),
    ),
  ),
  7825705068 => 
  array (
    'id' => 14964205,
    'name' => 'ООО «Омега»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490181177,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29363033,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363033',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7825705068',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29363033,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363033',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964205',
        'method' => 'get',
      ),
    ),
  ),
  2223592844 => 
  array (
    'id' => 14964237,
    'name' => 'ООО "Сибтепломаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490181290,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29363071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2223592844',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29363071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964237',
        'method' => 'get',
      ),
    ),
  ),
  3906245936 => 
  array (
    'id' => 14964577,
    'name' => 'ООО "КлинЭксперт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490182586,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29363467,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363467',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906245936',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калининградская область(GMT +2)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29363467,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363467',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964577',
        'method' => 'get',
      ),
    ),
  ),
  540123954976 => 
  array (
    'id' => 14964613,
    'name' => 'ИП Смолин Евгений Викторович-',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490182719,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29363513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '540123954976',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29363513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29363513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964613',
        'method' => 'get',
      ),
    ),
  ),
  7743582577 => 
  array (
    'id' => 14964835,
    'name' => 'ООО "Торговая компания "Медицинская техника"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490183375,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743582577',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14964835',
        'method' => 'get',
      ),
    ),
  ),
  7701724734 => 
  array (
    'id' => 14965117,
    'name' => 'ООО "Гемамед"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490184609,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29364049,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29364049',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701724734',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29364049,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29364049',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14965117',
        'method' => 'get',
      ),
    ),
  ),
  7718743291 => 
  array (
    'id' => 14965189,
    'name' => 'ООО "ОКО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490184915,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29367049,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367049',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718743291',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29367049,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367049',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14965189',
        'method' => 'get',
      ),
    ),
  ),
  5047162493 => 
  array (
    'id' => 14965193,
    'name' => 'ООО "РАВИМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490184926,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29367053,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367053',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047162493',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29367053,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367053',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14965193',
        'method' => 'get',
      ),
    ),
  ),
  7715227394 => 
  array (
    'id' => 14965243,
    'name' => 'ЗАО  "ГЛОБУС-ТЕЛЕКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490185154,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29367101,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367101',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715227394',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT+3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29367101,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367101',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14965243',
        'method' => 'get',
      ),
    ),
  ),
  5038041781 => 
  array (
    'id' => 14965359,
    'name' => 'ООО "ЭКОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490185564,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29367265,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367265',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921385,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5038041781',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29367265,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29367265',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14965359',
        'method' => 'get',
      ),
    ),
  ),
  2460200965 => 
  array (
    'id' => 14969191,
    'name' => 'ООО "ДОРПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490242088,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29371945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460200965',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29371945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969191',
        'method' => 'get',
      ),
    ),
  ),
  2461125686 => 
  array (
    'id' => 14969201,
    'name' => 'ООО "ИНДОР-КРАСНОЯРСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490242323,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29371955,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371955',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2461125686',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29371955,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371955',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969201',
        'method' => 'get',
      ),
    ),
  ),
  2464047651 => 
  array (
    'id' => 14969223,
    'name' => 'ООО "Карьерные машины"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490242699,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29371985,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371985',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464047651',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29371985,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371985',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969223',
        'method' => 'get',
      ),
    ),
  ),
  2536092127 => 
  array (
    'id' => 14969239,
    'name' => 'ООО "Дальстройбизнес II"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490243224,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372027,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372027',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2536092127',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372027,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372027',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969239',
        'method' => 'get',
      ),
    ),
  ),
  3448003962 => 
  array (
    'id' => 14969281,
    'name' => 'АО "КАУСТИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490243892,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372089,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372089',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3448003962',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372089,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372089',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969281',
        'method' => 'get',
      ),
    ),
  ),
  5403355816 => 
  array (
    'id' => 14969529,
    'name' => 'ООО  "Новосибирский издательский дом"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490246606,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372379,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372379',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5403355816',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372379,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372379',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969529',
        'method' => 'get',
      ),
    ),
  ),
  671202675295 => 
  array (
    'id' => 14969551,
    'name' => 'ИП Анисимов Виталий Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490246782,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372393,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372393',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '671202675295',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Смоленская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372393,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372393',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969551',
        'method' => 'get',
      ),
    ),
  ),
  2312108194 => 
  array (
    'id' => 14969565,
    'name' => 'ООО "МАЛЮТКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490246916,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372413,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372413',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312108194',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969565',
        'method' => 'get',
      ),
    ),
  ),
  7536113680 => 
  array (
    'id' => 14969593,
    'name' => 'ООО "Забайкальская Клининговая Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490247217,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372453,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372453',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7536113680',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Читинская область(GMT +9)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372453,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372453',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969593',
        'method' => 'get',
      ),
    ),
  ),
  1657103526 => 
  array (
    'id' => 14969645,
    'name' => 'ООО «АвтоТехно»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490247644,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657103526',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969645',
        'method' => 'get',
      ),
    ),
  ),
  7228002061 => 
  array (
    'id' => 14969691,
    'name' => 'ООО "НИКОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490248233,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372575,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372575',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7228002061',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372575,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372575',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969691',
        'method' => 'get',
      ),
    ),
  ),
  411145965 => 
  array (
    'id' => 14969935,
    'name' => 'ООО "Горно-Алтайск Нефтепродукт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490249411,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372849,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372849',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '411145965',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтай(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372849,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372849',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969935',
        'method' => 'get',
      ),
    ),
  ),
  7207011121 => 
  array (
    'id' => 14969947,
    'name' => 'ООО "Арагацстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490249518,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372873,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372873',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7207011121',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372873,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372873',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969947',
        'method' => 'get',
      ),
    ),
  ),
  263004870903 => 
  array (
    'id' => 14969957,
    'name' => 'ИП Глинка Роман Анатольевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490249638,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29372897,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372897',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '263004870903',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29372897,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29372897',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969957',
        'method' => 'get',
      ),
    ),
  ),
  7203376386 => 
  array (
    'id' => 14970115,
    'name' => 'ООО "МК СТАНДАРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490250842,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203376386',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Омская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970115',
        'method' => 'get',
      ),
    ),
  ),
  2312191749 => 
  array (
    'id' => 14970197,
    'name' => 'ООО Приоритет',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490251355,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312191749',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970197',
        'method' => 'get',
      ),
    ),
  ),
  2221005804 => 
  array (
    'id' => 14970215,
    'name' => 'ООО "Ландора"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490251515,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221005804',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373263,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373263',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970215',
        'method' => 'get',
      ),
    ),
  ),
  2014028480 => 
  array (
    'id' => 14970235,
    'name' => 'ООО «ЮНИСО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490251688,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373297,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373297',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2014028480',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чечня(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373297,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373297',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970235',
        'method' => 'get',
      ),
    ),
  ),
  4632087606 => 
  array (
    'id' => 14970375,
    'name' => 'ООО  СЕНТОЗА',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490252359,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632087606',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Курская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970375',
        'method' => 'get',
      ),
    ),
  ),
  2539087319 => 
  array (
    'id' => 14970423,
    'name' => '"ООО ""О"Витафарм"""',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490252561,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373533,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373533',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2539087319',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Приморский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970423',
        'method' => 'get',
      ),
    ),
  ),
  7725744754 => 
  array (
    'id' => 14970461,
    'name' => 'ООО "ТАМЕРЛАН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490252777,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373569,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373569',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725744754',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373569,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373569',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970461',
        'method' => 'get',
      ),
    ),
  ),
  1833058938 => 
  array (
    'id' => 14970487,
    'name' => 'ООО"Айшоп18.ру"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490252964,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1833058938',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саратовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970487',
        'method' => 'get',
      ),
    ),
  ),
  2907006181 => 
  array (
    'id' => 14970541,
    'name' => 'ООО "Велком"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490253177,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921386,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2907006181',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Архангельская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970541',
        'method' => 'get',
      ),
    ),
  ),
  7716715242 => 
  array (
    'id' => 14970599,
    'name' => 'ООО  "Верфит"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490253394,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373759,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373759',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716715242',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373759,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373759',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970599',
        'method' => 'get',
      ),
    ),
  ),
  1841067559 => 
  array (
    'id' => 14970625,
    'name' => 'ООО "Райс ТК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490253523,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29373799,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373799',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1841067559',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29373799,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29373799',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14970625',
        'method' => 'get',
      ),
    ),
  ),
  5190929284 => 
  array (
    'id' => 14971073,
    'name' => 'ООО Информ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490255623,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29374317,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374317',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190929284',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Карелия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29374317,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374317',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14971073',
        'method' => 'get',
      ),
    ),
  ),
  6166004561 => 
  array (
    'id' => 14971145,
    'name' => 'ООО "ФИРМА ВАРИАНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490255863,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29374401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6166004561',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29374401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14971145',
        'method' => 'get',
      ),
    ),
  ),
  2320005379 => 
  array (
    'id' => 14971193,
    'name' => 'ОАО"Сочихолод"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490256067,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29374469,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374469',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2320005379',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29374469,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374469',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14971193',
        'method' => 'get',
      ),
    ),
  ),
  5043026732 => 
  array (
    'id' => 14971267,
    'name' => 'ООО  «Европрестиж»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490256304,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29374557,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374557',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5043026732',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29374557,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374557',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14971267',
        'method' => 'get',
      ),
    ),
  ),
  2320145827 => 
  array (
    'id' => 14971309,
    'name' => 'АО "Сочинский хлебокомбинат"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490256469,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29374599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2320145827',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29374599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29374599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14971309',
        'method' => 'get',
      ),
    ),
  ),
  1657100780 => 
  array (
    'id' => 14971883,
    'name' => 'ООО "Транснациональная фармацевтическая компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490257957,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29375113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29375113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657100780',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29375113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29375113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14971883',
        'method' => 'get',
      ),
    ),
  ),
  7724245174 => 
  array (
    'id' => 14972011,
    'name' => 'ООО "Строкомс СК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490258610,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29375257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29375257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921387,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724245174',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29375257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29375257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14972011',
        'method' => 'get',
      ),
    ),
  ),
  3460013798 => 
  array (
    'id' => 14972123,
    'name' => 'ООО "АЛЬБИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490258965,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29375347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29375347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3460013798',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тамбовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29375347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29375347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14972123',
        'method' => 'get',
      ),
    ),
  ),
  5809003678 => 
  array (
    'id' => 14973187,
    'name' => 'ООО  "Дорожное-строительное управление-1"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490263260,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29376621,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376621',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5809003678',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пензенская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29376621,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376621',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973187',
        'method' => 'get',
      ),
    ),
  ),
  6154100310 => 
  array (
    'id' => 14973235,
    'name' => 'ООО  "МастерСлух"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490263480,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29376675,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376675',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6154100310',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29376675,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376675',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973235',
        'method' => 'get',
      ),
    ),
  ),
  772882337591 => 
  array (
    'id' => 14973271,
    'name' => 'ИП Абросимова Екатерина Алексеевна -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490263657,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29376711,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376711',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '772882337591',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тульская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29376711,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376711',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973271',
        'method' => 'get',
      ),
    ),
  ),
  8401008386 => 
  array (
    'id' => 14973451,
    'name' => 'АО "АК "НордСтар"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490264409,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29376909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8401008386',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'мКрасноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29376909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973451',
        'method' => 'get',
      ),
    ),
  ),
  7715810214 => 
  array (
    'id' => 14973499,
    'name' => 'ООО  "МЕДИМКОМ-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490264554,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29376953,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376953',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715810214',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29376953,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29376953',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973499',
        'method' => 'get',
      ),
    ),
  ),
  7801250904 => 
  array (
    'id' => 14973669,
    'name' => 'ООО "ТЭКО-СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490265213,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29377191,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377191',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801250904',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29377191,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377191',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973669',
        'method' => 'get',
      ),
    ),
  ),
  2301076102 => 
  array (
    'id' => 14973703,
    'name' => 'СКО "Смена" (ООО)"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490265357,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29377239,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377239',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2301076102',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29377239,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377239',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973703',
        'method' => 'get',
      ),
    ),
  ),
  2901235322 => 
  array (
    'id' => 14973839,
    'name' => 'ООО «СТРОЙДВИНСЕРВИС»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490265891,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29377359,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377359',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901235322',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29377359,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377359',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973839',
        'method' => 'get',
      ),
    ),
  ),
  3123164163 => 
  array (
    'id' => 14973911,
    'name' => 'ООО "Фабрика информационных технологий"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490266334,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29377577,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377577',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123164163',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29377577,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377577',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973911',
        'method' => 'get',
      ),
    ),
  ),
  3702104670 => 
  array (
    'id' => 14973989,
    'name' => 'ООО "Дормострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490266704,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29377671,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377671',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702104670',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ивановская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29377671,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377671',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14973989',
        'method' => 'get',
      ),
    ),
  ),
  3528262970 => 
  array (
    'id' => 14974051,
    'name' => 'ООО «ХИММАРКЕТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490267011,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29377763,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377763',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3528262970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29377763,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377763',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974051',
        'method' => 'get',
      ),
    ),
  ),
  3662178471 => 
  array (
    'id' => 14974103,
    'name' => 'ООО "Спектр"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490267204,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29377815,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377815',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662178471',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29377815,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29377815',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974103',
        'method' => 'get',
      ),
    ),
  ),
  7702811130 => 
  array (
    'id' => 14974319,
    'name' => 'ООО«МонтажЭкспертБезопасность»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490268201,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378067,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378067',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702811130',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378067,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378067',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974319',
        'method' => 'get',
      ),
    ),
  ),
  1101023006 => 
  array (
    'id' => 14974397,
    'name' => 'ООО«Лема»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490268555,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378183,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378183',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1101023006',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378183,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378183',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974397',
        'method' => 'get',
      ),
    ),
  ),
  9705000373 => 
  array (
    'id' => 14974433,
    'name' => 'ООО «РЕД СОФТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490268661,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378237,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378237',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705000373',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378237,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378237',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974433',
        'method' => 'get',
      ),
    ),
  ),
  7840385310 => 
  array (
    'id' => 14974471,
    'name' => 'ООО "БАЛТИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490268813,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378293,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378293',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840385310',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378293,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378293',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974471',
        'method' => 'get',
      ),
    ),
  ),
  5505041833 => 
  array (
    'id' => 14974509,
    'name' => 'ООО "ЛОГИСТИКА-ЦЕНТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490268973,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378351,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378351',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5505041833',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Омская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378351,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378351',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974509',
        'method' => 'get',
      ),
    ),
  ),
  '' => 
  array (
    'id' => 14974517,
    'name' => 'ООО "Ландскрона"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490268977,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974517',
        'method' => 'get',
      ),
    ),
  ),
  7838460513 => 
  array (
    'id' => 14974549,
    'name' => 'АО "Новая Авиация"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490269082,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838460513',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974549',
        'method' => 'get',
      ),
    ),
  ),
  301501822393 => 
  array (
    'id' => 14974571,
    'name' => 'ИП  Иванова Ирина Анатольевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490269179,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378425,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378425',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '301501822393',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Астраханская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378425,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378425',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974571',
        'method' => 'get',
      ),
    ),
  ),
  7838392366 => 
  array (
    'id' => 14974599,
    'name' => 'ООО "Группа компаний "ОХРАНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490269292,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378463,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378463',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838392366',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378463,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378463',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974599',
        'method' => 'get',
      ),
    ),
  ),
  3906230721 => 
  array (
    'id' => 14974635,
    'name' => 'ООО "Юпитер"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490269486,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378515,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378515',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906230721',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калининградская область(GMT +2)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378515,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378515',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974635',
        'method' => 'get',
      ),
    ),
  ),
  5032168238 => 
  array (
    'id' => 14974673,
    'name' => 'ЧОП "Троя СТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490269664,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032168238',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974673',
        'method' => 'get',
      ),
    ),
  ),
  7810684400 => 
  array (
    'id' => 14974761,
    'name' => 'ООО ОП  БАРИТ',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490269962,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810684400',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974761',
        'method' => 'get',
      ),
    ),
  ),
  7810374624 => 
  array (
    'id' => 14974801,
    'name' => 'ООО "ЛЕНПРОММОНТАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490270116,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378747,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378747',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810374624',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378747,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378747',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974801',
        'method' => 'get',
      ),
    ),
  ),
  616504895403 => 
  array (
    'id' => 14974831,
    'name' => 'ИП Пивоварова Ангелина Николаевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490270259,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378803,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378803',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921382,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '616504895403',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378803,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378803',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974831',
        'method' => 'get',
      ),
    ),
  ),
  3523010908 => 
  array (
    'id' => 14974921,
    'name' => 'ООО "ГОАРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490270628,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378943,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378943',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3523010908',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Вологодская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378943,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378943',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974921',
        'method' => 'get',
      ),
    ),
  ),
  7743130718 => 
  array (
    'id' => 14974923,
    'name' => 'ООО "ТараМед"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490270630,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743130718',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974923',
        'method' => 'get',
      ),
    ),
  ),
  7737536290 => 
  array (
    'id' => 14974947,
    'name' => 'ООО "Лифтоматика"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490270750,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29378969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7737536290',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29378969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29378969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974947',
        'method' => 'get',
      ),
    ),
  ),
  1838010496 => 
  array (
    'id' => 14974967,
    'name' => 'ООО «Сарапульская птицефабрика»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490270839,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29379003,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379003',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1838010496',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Орловская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29379003,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379003',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974967',
        'method' => 'get',
      ),
    ),
  ),
  7730715332 => 
  array (
    'id' => 14974997,
    'name' => 'ООО "СВОЙ ТУРИСТИЧЕСКИЙ СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490270999,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29379059,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379059',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730715332',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29379059,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379059',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14974997',
        'method' => 'get',
      ),
    ),
  ),
  7730228970 => 
  array (
    'id' => 14976183,
    'name' => 'ООО "ВИКТОРИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490271531,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29379909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730228970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29379909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976183',
        'method' => 'get',
      ),
    ),
  ),
  5754004045 => 
  array (
    'id' => 14976211,
    'name' => 'ПО "Центр заготовок и сбыта"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490271672,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29379965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5754004045',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Орловская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29379965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976211',
        'method' => 'get',
      ),
    ),
  ),
  7728539263 => 
  array (
    'id' => 14976221,
    'name' => 'ООО "ЗИНТНЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490271755,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29379983,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379983',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728539263',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29379983,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29379983',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976221',
        'method' => 'get',
      ),
    ),
  ),
  7804366760 => 
  array (
    'id' => 14976261,
    'name' => 'ООО "Аналитика-78"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490271936,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804366760',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976261',
        'method' => 'get',
      ),
    ),
  ),
  7724871855 => 
  array (
    'id' => 14976277,
    'name' => 'ООО "Торговый Дом "Рубин"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490271991,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380053,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380053',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724871855',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380053,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380053',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976277',
        'method' => 'get',
      ),
    ),
  ),
  7720706508 => 
  array (
    'id' => 14976379,
    'name' => 'ООО "Промформат"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490272076,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380163,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380163',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720706508',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380163,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380163',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976379',
        'method' => 'get',
      ),
    ),
  ),
  7716828461 => 
  array (
    'id' => 14976427,
    'name' => 'ООО СпецПоставка',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490272279,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380261,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380261',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716828461',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380261,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380261',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976427',
        'method' => 'get',
      ),
    ),
  ),
  7804498283 => 
  array (
    'id' => 14976453,
    'name' => 'ООО  "Тендер СПб"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490272406,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380315,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380315',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804498283',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380315,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380315',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976453',
        'method' => 'get',
      ),
    ),
  ),
  7709928962 => 
  array (
    'id' => 14976485,
    'name' => 'ООО "Компаньон-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490272565,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709928962',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976485',
        'method' => 'get',
      ),
    ),
  ),
  3325008311 => 
  array (
    'id' => 14976511,
    'name' => 'ООО  «ТаДи»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490272689,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3325008311',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976511',
        'method' => 'get',
      ),
    ),
  ),
  7709666234 => 
  array (
    'id' => 14976523,
    'name' => 'ОАО "Научно-технический центр по безопасности в промышленности"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490272737,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380409,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380409',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709666234',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380409,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380409',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976523',
        'method' => 'get',
      ),
    ),
  ),
  7706418032 => 
  array (
    'id' => 14976575,
    'name' => 'ООО Смарт информационные технологии',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490272910,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380457,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380457',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706418032',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380457,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380457',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976575',
        'method' => 'get',
      ),
    ),
  ),
  7705315980 => 
  array (
    'id' => 14976605,
    'name' => 'ООО «Аналитика М»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490273049,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380497,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380497',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705315980',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380497,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380497',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976605',
        'method' => 'get',
      ),
    ),
  ),
  7701947762 => 
  array (
    'id' => 14976635,
    'name' => 'ООО «АЛЬЯНС»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490273166,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701947762',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976635',
        'method' => 'get',
      ),
    ),
  ),
  7604274085 => 
  array (
    'id' => 14976679,
    'name' => 'ООО "Даль"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490273339,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605886,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7604274085',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976679',
        'method' => 'get',
      ),
    ),
  ),
  5904177272 => 
  array (
    'id' => 14976707,
    'name' => 'ООО "Энергостандарт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490273520,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904177272',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976707',
        'method' => 'get',
      ),
    ),
  ),
  7801436602 => 
  array (
    'id' => 14976727,
    'name' => 'ЗАО«Экрос-Инжиниринг»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490273603,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380639,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380639',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801436602',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380639,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380639',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976727',
        'method' => 'get',
      ),
    ),
  ),
  7327021957 => 
  array (
    'id' => 14976743,
    'name' => 'ООО "Хайдар"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490273647,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380665,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380665',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7327021957',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380665,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380665',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976743',
        'method' => 'get',
      ),
    ),
  ),
  3329028076 => 
  array (
    'id' => 14976783,
    'name' => 'ООО РСП Богородице-Рождественского мужского монастыря',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490273790,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380709,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380709',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3329028076',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Владимирская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380709,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380709',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976783',
        'method' => 'get',
      ),
    ),
  ),
  6950172400 => 
  array (
    'id' => 14976785,
    'name' => 'ООО "РЕМСТРОЙМОНТАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490273791,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380711,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380711',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950172400',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380711,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380711',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976785',
        'method' => 'get',
      ),
    ),
  ),
  6923004598 => 
  array (
    'id' => 14976817,
    'name' => 'ООО "Дорсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490273938,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29380753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6923004598',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29380753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29380753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976817',
        'method' => 'get',
      ),
    ),
  ),
  6905009096 => 
  array (
    'id' => 14976855,
    'name' => 'ООО НПО "ТВЕМОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490274085,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29381115,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29381115',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6905009096',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29381115,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29381115',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976855',
        'method' => 'get',
      ),
    ),
  ),
  6679082283 => 
  array (
    'id' => 14976911,
    'name' => 'ООО "ДекнаУрал"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490274388,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29381527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29381527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6679082283',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29381527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29381527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976911',
        'method' => 'get',
      ),
    ),
  ),
  6453138463 => 
  array (
    'id' => 14976921,
    'name' => 'ООО «САРОПТСЕРВИС»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490274474,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29381549,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29381549',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6453138463',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29381549,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29381549',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976921',
        'method' => 'get',
      ),
    ),
  ),
  7604274046 => 
  array (
    'id' => 14976941,
    'name' => 'ООО  "ХДС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490274664,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29382461,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382461',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7604274046',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29382461,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382461',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14976941',
        'method' => 'get',
      ),
    ),
  ),
  7710144747 => 
  array (
    'id' => 14977013,
    'name' => 'Публичное акционерное общество Страхования компания "Росгосстрах"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490274989,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29382587,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382587',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710144747',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29382587,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382587',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14977013',
        'method' => 'get',
      ),
    ),
  ),
  590810223905 => 
  array (
    'id' => 14977049,
    'name' => 'ИП Бурылов Александр Константинович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490275172,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29382629,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382629',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '590810223905',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29382629,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382629',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14977049',
        'method' => 'get',
      ),
    ),
  ),
  7713624893 => 
  array (
    'id' => 14977097,
    'name' => 'ООО «центр правовой поддержки бизнеса «АТОЛЛ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490275489,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29382703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713624893',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29382703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14977097',
        'method' => 'get',
      ),
    ),
  ),
  7705919029 => 
  array (
    'id' => 14977131,
    'name' => 'ООО  «Юнион Трейд»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490275766,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29382775,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382775',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705919029',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29382775,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29382775',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14977131',
        'method' => 'get',
      ),
    ),
  ),
  1620000411 => 
  array (
    'id' => 14977163,
    'name' => 'ООО  "Васильевский"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490275935,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29383157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29383157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1620000411',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Татарстан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29383157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29383157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14977163',
        'method' => 'get',
      ),
    ),
  ),
  5837025120 => 
  array (
    'id' => 14977311,
    'name' => 'ООО "Сетевая компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490276579,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29383621,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29383621',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5837025120',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29383621,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29383621',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14977311',
        'method' => 'get',
      ),
    ),
  ),
  268036057 => 
  array (
    'id' => 14979859,
    'name' => 'ООО «Уралрегион»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490326886,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386843,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386843',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '268036057',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386843,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386843',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979859',
        'method' => 'get',
      ),
    ),
  ),
  1840007162 => 
  array (
    'id' => 14979869,
    'name' => 'ООО "УралТехПром"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490327263,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1840007162',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979869',
        'method' => 'get',
      ),
    ),
  ),
  2463075600 => 
  array (
    'id' => 14979873,
    'name' => 'АО Проектный научно-исследовательский и конструкторский институт «Красноярский ПромстройНИИпроект»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490327460,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386885,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386885',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463075600',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386885,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386885',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979873',
        'method' => 'get',
      ),
    ),
  ),
  2466169528 => 
  array (
    'id' => 14979889,
    'name' => 'ООО "ВРМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490328001,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386921,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386921',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466169528',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386921,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386921',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979889',
        'method' => 'get',
      ),
    ),
  ),
  4205028501 => 
  array (
    'id' => 14979891,
    'name' => 'ООО "КУЗБАССДОРСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490328251,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205028501',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979891',
        'method' => 'get',
      ),
    ),
  ),
  4209005047 => 
  array (
    'id' => 14979909,
    'name' => 'ООО "Волна К"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490328705,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4209005047',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979909',
        'method' => 'get',
      ),
    ),
  ),
  4213006036 => 
  array (
    'id' => 14979915,
    'name' => 'АО "ЕНИСЕЙАВТОДОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490328900,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386981,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386981',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4213006036',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386981,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386981',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979915',
        'method' => 'get',
      ),
    ),
  ),
  4252012322 => 
  array (
    'id' => 14979921,
    'name' => 'ООО "ТРЕСТ ВОСТОКГИДРОСПЕЦСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490329038,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29386987,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386987',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4252012322',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29386987,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29386987',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979921',
        'method' => 'get',
      ),
    ),
  ),
  4307013670 => 
  array (
    'id' => 14979939,
    'name' => 'ООО Сфера Принт',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490329307,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387005,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387005',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4307013670',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387005,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387005',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979939',
        'method' => 'get',
      ),
    ),
  ),
  4505000049 => 
  array (
    'id' => 14979955,
    'name' => 'ООО "Промдорстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490329548,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387021,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387021',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4505000049',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387021,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387021',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14979955',
        'method' => 'get',
      ),
    ),
  ),
  3663062952 => 
  array (
    'id' => 14980181,
    'name' => 'ООО "Строй Капитал"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490332403,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387313,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387313',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605885,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3663062952',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387313,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387313',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980181',
        'method' => 'get',
      ),
    ),
  ),
  5905030280 => 
  array (
    'id' => 14980191,
    'name' => 'ООО  "ТриОмед"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490332484,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387321,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387321',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905030280',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387321,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387321',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980191',
        'method' => 'get',
      ),
    ),
  ),
  5260351955 => 
  array (
    'id' => 14980221,
    'name' => 'ООО "Пятый Элемент"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490332828,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387361,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387361',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5260351955',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Нижегородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387361,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387361',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980221',
        'method' => 'get',
      ),
    ),
  ),
  5905290312 => 
  array (
    'id' => 14980253,
    'name' => 'ООО"ГРУЗАВТОИМПОРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490333355,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387425,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387425',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921383,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905290312',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387425,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387425',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980253',
        'method' => 'get',
      ),
    ),
  ),
  2360007358 => 
  array (
    'id' => 14980293,
    'name' => 'ООО Инженерный центр  ИОН',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490333783,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2360007358',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980293',
        'method' => 'get',
      ),
    ),
  ),
  2901148951 => 
  array (
    'id' => 14980309,
    'name' => 'ООО  "Консультант Плюс Архангельск"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490333979,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387503,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387503',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901148951',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Архангельская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387503,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387503',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980309',
        'method' => 'get',
      ),
    ),
  ),
  1435115270 => 
  array (
    'id' => 14980325,
    'name' => 'АО "Саханефтегазсбыт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490334190,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435115270',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980325',
        'method' => 'get',
      ),
    ),
  ),
  7706443085 => 
  array (
    'id' => 14980341,
    'name' => 'ООО «Плантация ЕСО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490334426,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387547,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387547',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706443085',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387547,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387547',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980341',
        'method' => 'get',
      ),
    ),
  ),
  190100331101 => 
  array (
    'id' => 14980379,
    'name' => 'ИП Городилов Валерий Васильевич -',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490334859,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '190100331101',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хакасия(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980379',
        'method' => 'get',
      ),
    ),
  ),
  7838399555 => 
  array (
    'id' => 14980435,
    'name' => 'ООО "ТендерФуд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490335515,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387675,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387675',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838399555',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387675,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387675',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980435',
        'method' => 'get',
      ),
    ),
  ),
  5410114988 => 
  array (
    'id' => 14980459,
    'name' => 'ООО"ИЗЫСКАТЕЛЬ-С"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490335637,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387707,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387707',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410114988',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387707,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387707',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980459',
        'method' => 'get',
      ),
    ),
  ),
  3319008056 => 
  array (
    'id' => 14980465,
    'name' => 'ООО  "Дорожник"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490335732,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387711,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387711',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3319008056',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Владимирская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387711,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387711',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980465',
        'method' => 'get',
      ),
    ),
  ),
  2540190399 => 
  array (
    'id' => 14980507,
    'name' => 'ООО Дальводоканал',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490336316,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387783,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387783',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2540190399',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Приморский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387783,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387783',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980507',
        'method' => 'get',
      ),
    ),
  ),
  7017378594 => 
  array (
    'id' => 14980549,
    'name' => 'ООО  "ЭКСПЕРТСТРОЙПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490336599,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29387839,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387839',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017378594',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Томская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29387839,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29387839',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980549',
        'method' => 'get',
      ),
    ),
  ),
  7704543951 => 
  array (
    'id' => 14980779,
    'name' => 'ООО "Компания Хеликон"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490337431,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29388119,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388119',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704543951',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Сахалинская область(GMT +10',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29388119,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388119',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980779',
        'method' => 'get',
      ),
    ),
  ),
  2368007938 => 
  array (
    'id' => 14980801,
    'name' => 'ООО  «Белпром»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490337555,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29388137,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388137',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2368007938',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29388137,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388137',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980801',
        'method' => 'get',
      ),
    ),
  ),
  5027072844 => 
  array (
    'id' => 14980861,
    'name' => 'ЗАО "ВР-Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490337763,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29388205,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388205',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027072844',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29388205,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388205',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980861',
        'method' => 'get',
      ),
    ),
  ),
  2508123398 => 
  array (
    'id' => 14980881,
    'name' => 'ООО  "СМС-ДВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490337910,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29388235,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388235',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2508123398',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Приморский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29388235,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388235',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14980881',
        'method' => 'get',
      ),
    ),
  ),
  1901033363 => 
  array (
    'id' => 14981003,
    'name' => 'ООО "Аналитик"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490338915,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29388393,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388393',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1901033363',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хакасия(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29388393,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388393',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981003',
        'method' => 'get',
      ),
    ),
  ),
  6324037523 => 
  array (
    'id' => 14981133,
    'name' => 'ООО "ПрофМедДезФарм"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490339566,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29388545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921384,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6324037523',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29388545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29388545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981133',
        'method' => 'get',
      ),
    ),
  ),
  7811604052 => 
  array (
    'id' => 14981523,
    'name' => 'ООО "ХАГЕМАЕР ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490341612,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921379,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811604052',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981523',
        'method' => 'get',
      ),
    ),
  ),
  7814625685 => 
  array (
    'id' => 14981559,
    'name' => 'ООО "Медитех"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490341791,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921379,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814625685',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Рязанская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981559',
        'method' => 'get',
      ),
    ),
  ),
  7726602336 => 
  array (
    'id' => 14981601,
    'name' => 'ООО"Полимедсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490342008,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389193,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389193',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921379,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726602336',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389193,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389193',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981601',
        'method' => 'get',
      ),
    ),
  ),
  6658114216 => 
  array (
    'id' => 14981619,
    'name' => 'ООО "УРАЛ РЕЛКОМ-ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490342118,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389217,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389217',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921379,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658114216',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389217,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389217',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981619',
        'method' => 'get',
      ),
    ),
  ),
  6829105824 => 
  array (
    'id' => 14981677,
    'name' => 'ООО «Имхотеп»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490342414,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389273,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389273',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829105824',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тамбовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389273,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389273',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981677',
        'method' => 'get',
      ),
    ),
  ),
  2465288402 => 
  array (
    'id' => 14981729,
    'name' => 'ООО  "Архитектурно-Строительная Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490342557,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389313,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389313',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465288402',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389313,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389313',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981729',
        'method' => 'get',
      ),
    ),
  ),
  5049009571 => 
  array (
    'id' => 14981769,
    'name' => 'ООО "Шатурский общепит"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490342716,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5049009571',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981769',
        'method' => 'get',
      ),
    ),
  ),
  5408175581 => 
  array (
    'id' => 14981861,
    'name' => 'А."СибАкадемСофт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490343086,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5408175581',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14981861',
        'method' => 'get',
      ),
    ),
  ),
  7709615751 => 
  array (
    'id' => 14982129,
    'name' => 'ООО "КАМАСпецШина"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490343595,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709615751',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14982129',
        'method' => 'get',
      ),
    ),
  ),
  1434033280 => 
  array (
    'id' => 14982271,
    'name' => 'ООО "МАРИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490344039,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29389759,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389759',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1434033280',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29389759,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29389759',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14982271',
        'method' => 'get',
      ),
    ),
  ),
  2455037777 => 
  array (
    'id' => 14983467,
    'name' => 'ООО "ЮЖНЫЙ РЕСУРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490348765,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29391175,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391175',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2455037777',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хакасия(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29391175,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391175',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14983467',
        'method' => 'get',
      ),
    ),
  ),
  6679036181 => 
  array (
    'id' => 14983505,
    'name' => 'ООО "УРАЛМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490349010,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29391223,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391223',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6679036181',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29391223,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391223',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14983505',
        'method' => 'get',
      ),
    ),
  ),
  5022046329 => 
  array (
    'id' => 14983529,
    'name' => 'ООО "ПРОГРЕСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490349134,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29391257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5022046329',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29391257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14983529',
        'method' => 'get',
      ),
    ),
  ),
  2902076280 => 
  array (
    'id' => 14983577,
    'name' => 'ООО "Северодвинское коммунальное хозяйство"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490349379,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29391297,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391297',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2902076280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29391297,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391297',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14983577',
        'method' => 'get',
      ),
    ),
  ),
  7729407630 => 
  array (
    'id' => 14983597,
    'name' => 'ООО "ФЕСТО-РФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1490349507,
    'updated_at' => 1523285103,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29391317,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391317',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921380,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729407630',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29391317,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29391317',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14983597',
        'method' => 'get',
      ),
    ),
  ),
);