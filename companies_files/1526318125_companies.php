<?php 
 return array (
  1001274790 => 
  array (
    'id' => 15889389,
    'name' => 'ООО "КАУДАЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499864086,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30694373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30694373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1001274790',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Карелия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30694373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30694373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15889389',
        'method' => 'get',
      ),
    ),
  ),
  1435108146 => 
  array (
    'id' => 15894465,
    'name' => 'ООО «СЕРВИС-ОЙЛ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499915830,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30699857,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30699857',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435108146',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30699857,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30699857',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894465',
        'method' => 'get',
      ),
    ),
  ),
  1435269070 => 
  array (
    'id' => 15894495,
    'name' => 'ООО "ЭНЕРГИЯ СЕВЕРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499916605,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30699919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30699919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435269070',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30699919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30699919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894495',
        'method' => 'get',
      ),
    ),
  ),
  1701037794 => 
  array (
    'id' => 15894503,
    'name' => 'ООО "Еврострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499917162,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30699947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30699947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1701037794',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30699947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30699947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894503',
        'method' => 'get',
      ),
    ),
  ),
  2464235461 => 
  array (
    'id' => 15894545,
    'name' => 'ООО "ТРАНСЛОГИСТИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499918495,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700065,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700065',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464235461',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700065,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700065',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894545',
        'method' => 'get',
      ),
    ),
  ),
  7017404413 => 
  array (
    'id' => 15894577,
    'name' => 'ООО "Грейтпроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499919020,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700111,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700111',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017404413',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Томская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700111,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700111',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894577',
        'method' => 'get',
      ),
    ),
  ),
  2310133879 => 
  array (
    'id' => 15894601,
    'name' => 'ООО  "ЗЕМЭНЕРГОЦЕНТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499919690,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700165,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700165',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2310133879',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700165,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700165',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894601',
        'method' => 'get',
      ),
    ),
  ),
  8902008764 => 
  array (
    'id' => 15894623,
    'name' => 'ООО "Промстройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499920077,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700201,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700201',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8902008764',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700201,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700201',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894623',
        'method' => 'get',
      ),
    ),
  ),
  5405978580 => 
  array (
    'id' => 15894705,
    'name' => 'ООО "ПТК 88"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499921486,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700313,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700313',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405978580',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700313,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700313',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894705',
        'method' => 'get',
      ),
    ),
  ),
  4632083658 => 
  array (
    'id' => 15894753,
    'name' => 'ООО"Диалог"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499922552,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700393,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700393',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632083658',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Белгородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700393,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700393',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894753',
        'method' => 'get',
      ),
    ),
  ),
  7448186784 => 
  array (
    'id' => 15894777,
    'name' => 'ООО  "СтройАркПлаза"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499923037,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700433,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700433',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448186784',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700433,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700433',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894777',
        'method' => 'get',
      ),
    ),
  ),
  3019010382 => 
  array (
    'id' => 15894825,
    'name' => 'ООО "Дорожностроительная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499923400,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700469,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700469',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3019010382',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Астраханская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700469,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700469',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894825',
        'method' => 'get',
      ),
    ),
  ),
  105071834 => 
  array (
    'id' => 15894837,
    'name' => 'ООО "СТРОЙУНИВЕРСАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499923538,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700479,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700479',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '105071834',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700479,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700479',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894837',
        'method' => 'get',
      ),
    ),
  ),
  7802430314 => 
  array (
    'id' => 15894889,
    'name' => 'ООО  "ПГС II"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499924022,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802430314',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894889',
        'method' => 'get',
      ),
    ),
  ),
  901050864 => 
  array (
    'id' => 15894923,
    'name' => 'ООО Инвестиционно-строительная компания "Кубанское"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499924489,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '901050864',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894923',
        'method' => 'get',
      ),
    ),
  ),
  1616022613 => 
  array (
    'id' => 15894995,
    'name' => 'ООО «Титан-Трейд»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499925388,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1616022613',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15894995',
        'method' => 'get',
      ),
    ),
  ),
  7723868306 => 
  array (
    'id' => 15895087,
    'name' => 'ООО  "Артстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499926256,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700799,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700799',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723868306',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700799,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700799',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895087',
        'method' => 'get',
      ),
    ),
  ),
  1659130959 => 
  array (
    'id' => 15895119,
    'name' => 'ООО "Пром Эксим"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499926448,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700837,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700837',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659130959',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700837,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700837',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895119',
        'method' => 'get',
      ),
    ),
  ),
  2016002543 => 
  array (
    'id' => 15895151,
    'name' => 'ООО "АВЭС Инжиниринг"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499926605,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2016002543',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700871,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700871',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895151',
        'method' => 'get',
      ),
    ),
  ),
  7734372566 => 
  array (
    'id' => 15895201,
    'name' => 'ООО "СВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499926827,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734372566',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895201',
        'method' => 'get',
      ),
    ),
  ),
  2130165832 => 
  array (
    'id' => 15895217,
    'name' => 'ООО "ЕВРОВИД+"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499926906,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30700931,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700931',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130165832',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30700931,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30700931',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895217',
        'method' => 'get',
      ),
    ),
  ),
  5190934460 => 
  array (
    'id' => 15895387,
    'name' => 'ООО "Эксперт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499928266,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30701203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190934460',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30701203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895387',
        'method' => 'get',
      ),
    ),
  ),
  7734355747 => 
  array (
    'id' => 15895419,
    'name' => 'ООО "ЭЛИОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499928693,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30701281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734355747',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30701281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895419',
        'method' => 'get',
      ),
    ),
  ),
  3005007252 => 
  array (
    'id' => 15895567,
    'name' => 'ООО "Дорожник"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499929572,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30701517,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701517',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3005007252',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30701517,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701517',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895567',
        'method' => 'get',
      ),
    ),
  ),
  5190021527 => 
  array (
    'id' => 15895885,
    'name' => 'ООО "ОРИОН СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499931325,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30701945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190021527',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30701945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30701945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895885',
        'method' => 'get',
      ),
    ),
  ),
  3016060346 => 
  array (
    'id' => 15895931,
    'name' => 'ООО "Технострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499931544,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30702005,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702005',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3016060346',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30702005,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702005',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15895931',
        'method' => 'get',
      ),
    ),
  ),
  3019013746 => 
  array (
    'id' => 15896145,
    'name' => 'ООО «Володарское»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499932437,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30702337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3019013746',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30702337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15896145',
        'method' => 'get',
      ),
    ),
  ),
  3022003466 => 
  array (
    'id' => 15896167,
    'name' => 'ООО "Жилсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499932563,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30702377,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702377',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3022003466',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30702377,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702377',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15896167',
        'method' => 'get',
      ),
    ),
  ),
  3022007140 => 
  array (
    'id' => 15896315,
    'name' => 'ООО "ХАРАБАЛИНСКОЕ ДОРОЖНОЕ РЕМОНТНО-СТРОИТЕЛЬНОЕ ПРЕДПРИЯТИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499933220,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30702643,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702643',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3022007140',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30702643,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702643',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15896315',
        'method' => 'get',
      ),
    ),
  ),
  3023006011 => 
  array (
    'id' => 15896345,
    'name' => 'ООО  "ВИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1499933441,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30702697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3023006011',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30702697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30702697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15896345',
        'method' => 'get',
      ),
    ),
  ),
  7801559146 => 
  array (
    'id' => 15898635,
    'name' => 'ООО "Центр Лабораторных Исследований и Проектирования "УМЭко"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499943393,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30705617,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30705617',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801559146',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30705617,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30705617',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15898635',
        'method' => 'get',
      ),
    ),
  ),
  263514570544 => 
  array (
    'id' => 15898731,
    'name' => 'ИП  СЕРОВА ИРИНА ВЛАДИМИРОВНА',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499943935,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30705779,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30705779',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '263514570544',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30705779,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30705779',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15898731',
        'method' => 'get',
      ),
    ),
  ),
  2130100578 => 
  array (
    'id' => 15898977,
    'name' => 'ООО "Скай Телеком"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499945109,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30706109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130100578',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чувашия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30706109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15898977',
        'method' => 'get',
      ),
    ),
  ),
  643909577681 => 
  array (
    'id' => 15899041,
    'name' => 'ИП  Тихонов Алексей Валерьевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499945417,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30706211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '643909577681',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30706211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15899041',
        'method' => 'get',
      ),
    ),
  ),
  5018087717 => 
  array (
    'id' => 15899141,
    'name' => 'ООО "МГК-сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499945988,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30706365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5018087717',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30706365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15899141',
        'method' => 'get',
      ),
    ),
  ),
  3406007404 => 
  array (
    'id' => 15899309,
    'name' => 'ООО "Кадастровый центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499946852,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30706613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3406007404',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30706613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15899309',
        'method' => 'get',
      ),
    ),
  ),
  6312171789 => 
  array (
    'id' => 15899959,
    'name' => 'ООО "ЯСТРЕБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499947884,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30706925,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706925',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312171789',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30706925,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30706925',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15899959',
        'method' => 'get',
      ),
    ),
  ),
  7720653581 => 
  array (
    'id' => 15900059,
    'name' => 'ООО "ПУТЕШЕСТВЕННИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1499948281,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30707051,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30707051',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720653581',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30707051,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30707051',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15900059',
        'method' => 'get',
      ),
    ),
  ),
  277137245 => 
  array (
    'id' => 15903365,
    'name' => 'ООО Инженерные системы',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500002689,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711447,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711447',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '277137245',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711447,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711447',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903365',
        'method' => 'get',
      ),
    ),
  ),
  323110762 => 
  array (
    'id' => 15903367,
    'name' => 'ООО "Бурятская Строительная Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500002752,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323110762',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903367',
        'method' => 'get',
      ),
    ),
  ),
  1101140616 => 
  array (
    'id' => 15903377,
    'name' => 'ООО "МЕД-ЭКОЛОГИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500003095,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711471,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711471',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1101140616',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711471,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711471',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903377',
        'method' => 'get',
      ),
    ),
  ),
  1411004187 => 
  array (
    'id' => 15903379,
    'name' => 'ООО Амгор',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500003170,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711477,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711477',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1411004187',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711477,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711477',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903379',
        'method' => 'get',
      ),
    ),
  ),
  1435138895 => 
  array (
    'id' => 15903391,
    'name' => 'ООО "Арктика-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500003672,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435138895',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903391',
        'method' => 'get',
      ),
    ),
  ),
  2463246615 => 
  array (
    'id' => 15903467,
    'name' => 'ООО "СИБПРОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500005836,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711635,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711635',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463246615',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711635,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711635',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903467',
        'method' => 'get',
      ),
    ),
  ),
  2540182831 => 
  array (
    'id' => 15903531,
    'name' => 'ООО «Строительное управление № 7»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500007065,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711731,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711731',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2540182831',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711731,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711731',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903531',
        'method' => 'get',
      ),
    ),
  ),
  2725002665 => 
  array (
    'id' => 15903541,
    'name' => 'ООО Фирма 2К',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500007279,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2725002665',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903541',
        'method' => 'get',
      ),
    ),
  ),
  3811137202 => 
  array (
    'id' => 15903587,
    'name' => 'ООО "ОБЪЕДИНЕННАЯ СТРОИТЕЛЬНАЯ КОМПАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500007797,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711815,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711815',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811137202',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711815,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711815',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903587',
        'method' => 'get',
      ),
    ),
  ),
  4632016651 => 
  array (
    'id' => 15903621,
    'name' => 'ОАО "СУОР-4"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500008347,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711875,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711875',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632016651',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Курская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711875,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711875',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903621',
        'method' => 'get',
      ),
    ),
  ),
  7725792042 => 
  array (
    'id' => 15903649,
    'name' => 'ООО "ГАЛАТЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500008905,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711915,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711915',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725792042',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711915,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711915',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903649',
        'method' => 'get',
      ),
    ),
  ),
  1328009775 => 
  array (
    'id' => 15903687,
    'name' => 'ООО "Мордовская механизированная колонна №89"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500009528,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1328009775',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903687',
        'method' => 'get',
      ),
    ),
  ),
  1659158376 => 
  array (
    'id' => 15903707,
    'name' => 'ООО "БарсОЙЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500009848,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711991,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711991',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659158376',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711991,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711991',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903707',
        'method' => 'get',
      ),
    ),
  ),
  2011883834 => 
  array (
    'id' => 15903713,
    'name' => 'ООО «Технопром»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500010021,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30711997,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711997',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2011883834',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30711997,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30711997',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903713',
        'method' => 'get',
      ),
    ),
  ),
  2117003014 => 
  array (
    'id' => 15903737,
    'name' => 'ООО "Строитель плюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500010351,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2117003014',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903737',
        'method' => 'get',
      ),
    ),
  ),
  7710498753 => 
  array (
    'id' => 15903817,
    'name' => 'ООО "Альфа-Трейд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500011116,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710498753',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Брянская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903817',
        'method' => 'get',
      ),
    ),
  ),
  2337042428 => 
  array (
    'id' => 15903899,
    'name' => 'ООО "Юг Альфа Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500011872,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2337042428',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903899',
        'method' => 'get',
      ),
    ),
  ),
  5020081180 => 
  array (
    'id' => 15903979,
    'name' => 'ООО "Строительное Монтажное Управление-5"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500012526,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712329,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712329',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5020081180',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712329,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712329',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15903979',
        'method' => 'get',
      ),
    ),
  ),
  7702414541 => 
  array (
    'id' => 15904045,
    'name' => 'ООО «ПрофМедСервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500013153,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702414541',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904045',
        'method' => 'get',
      ),
    ),
  ),
  5243025782 => 
  array (
    'id' => 15904089,
    'name' => 'ООО  "ТСС-Арзамас"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500013401,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712481,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712481',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5243025782',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712481,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712481',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904089',
        'method' => 'get',
      ),
    ),
  ),
  2312006139 => 
  array (
    'id' => 15904157,
    'name' => 'ООО "СМУ Гражданстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500013807,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712595,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712595',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312006139',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712595,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712595',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904157',
        'method' => 'get',
      ),
    ),
  ),
  6164266057 => 
  array (
    'id' => 15904169,
    'name' => 'АО "ЮИТ ДОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500013921,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6164266057',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904169',
        'method' => 'get',
      ),
    ),
  ),
  2635098120 => 
  array (
    'id' => 15904271,
    'name' => 'ООО «Строй-УМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500014619,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712801,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712801',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635098120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712801,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712801',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904271',
        'method' => 'get',
      ),
    ),
  ),
  3662196576 => 
  array (
    'id' => 15904277,
    'name' => 'ООО "Воронежская Строительная Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500014656,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662196576',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904277',
        'method' => 'get',
      ),
    ),
  ),
  1821011808 => 
  array (
    'id' => 15904349,
    'name' => 'ООО «Горд Шунды»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500015170,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1515488145,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1821011808',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904349',
        'method' => 'get',
      ),
    ),
  ),
  1838003026 => 
  array (
    'id' => 15904483,
    'name' => 'ООО "СИРИУС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500015974,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30713097,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30713097',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1838003026',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Удмуртия(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30713097,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30713097',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904483',
        'method' => 'get',
      ),
    ),
  ),
  7204097748 => 
  array (
    'id' => 15904543,
    'name' => 'ООО  "Северное волокно"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500016423,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30713207,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30713207',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7204097748',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30713207,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30713207',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904543',
        'method' => 'get',
      ),
    ),
  ),
  7723660001 => 
  array (
    'id' => 15904787,
    'name' => 'ООО «Строительная компания «Гарант» Столица»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500017640,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30713567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30713567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723660001',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30713567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30713567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904787',
        'method' => 'get',
      ),
    ),
  ),
  3525322067 => 
  array (
    'id' => 15905147,
    'name' => 'ООО «Эстрэйд»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500019566,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30714161,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30714161',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525322067',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30714161,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30714161',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15905147',
        'method' => 'get',
      ),
    ),
  ),
  3651009168 => 
  array (
    'id' => 15905305,
    'name' => 'ООО "Техстройпром"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500020355,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30714409,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30714409',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3651009168',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30714409,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30714409',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15905305',
        'method' => 'get',
      ),
    ),
  ),
  7801636986 => 
  array (
    'id' => 15905379,
    'name' => 'ООО ОХРАННАЯ ОРГАНИЗАЦИЯ "ОМЕГА ОХРАНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500020622,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30714527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30714527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801636986',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30714527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30714527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15905379',
        'method' => 'get',
      ),
    ),
  ),
  9102170287 => 
  array (
    'id' => 15905957,
    'name' => 'ООО "ПАРАЛЛЕЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500022769,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30715191,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30715191',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102170287',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30715191,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30715191',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15905957',
        'method' => 'get',
      ),
    ),
  ),
  2319042048 => 
  array (
    'id' => 15906115,
    'name' => 'ООО "СОЧИ-МОНОЛИТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500023388,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30715355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30715355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2319042048',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30715355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30715355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15906115',
        'method' => 'get',
      ),
    ),
  ),
  3123369474 => 
  array (
    'id' => 15906163,
    'name' => 'ООО "СТРОЙАЛЬЯНС ПГС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500023682,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30715467,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30715467',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123369474',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Белгородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30715467,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30715467',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15906163',
        'method' => 'get',
      ),
    ),
  ),
  5102046620 => 
  array (
    'id' => 15906915,
    'name' => 'ООО "ИльмА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500027579,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30716483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30716483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5102046620',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30716483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30716483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15906915',
        'method' => 'get',
      ),
    ),
  ),
  3435124720 => 
  array (
    'id' => 15907015,
    'name' => 'ООО "ИНТЕРСТРОЙ+"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500028207,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30716607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30716607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435124720',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30716607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30716607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15907015',
        'method' => 'get',
      ),
    ),
  ),
  2536209865 => 
  array (
    'id' => 15919507,
    'name' => 'ООО "Дальний Восток Энергосервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500265360,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729143,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729143',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2536209865',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729143,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729143',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919507',
        'method' => 'get',
      ),
    ),
  ),
  7457007103 => 
  array (
    'id' => 15919515,
    'name' => 'ООО "НАШ ГОРОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500265484,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729159,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729159',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7457007103',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729159,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729159',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919515',
        'method' => 'get',
      ),
    ),
  ),
  2536253529 => 
  array (
    'id' => 15919525,
    'name' => 'ООО "НТС ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500265596,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729175,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729175',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2536253529',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729175,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729175',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919525',
        'method' => 'get',
      ),
    ),
  ),
  2635079800 => 
  array (
    'id' => 15919529,
    'name' => 'ООО "МПМ-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500265740,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729189,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729189',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635079800',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729189,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729189',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919529',
        'method' => 'get',
      ),
    ),
  ),
  3805715462 => 
  array (
    'id' => 15919541,
    'name' => 'ООО "БЭСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500266023,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729227,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729227',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3805715462',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729227,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729227',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919541',
        'method' => 'get',
      ),
    ),
  ),
  5610213231 => 
  array (
    'id' => 15919583,
    'name' => 'ООО «Фарватер»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500266536,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5610213231',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Оренбургская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919583',
        'method' => 'get',
      ),
    ),
  ),
  4205349865 => 
  array (
    'id' => 15919645,
    'name' => 'ООО "СройТех"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500267347,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205349865',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919645',
        'method' => 'get',
      ),
    ),
  ),
  7204192800 => 
  array (
    'id' => 15919703,
    'name' => 'ООО "АРМРУССТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500267954,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729423,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729423',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7204192800',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729423,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729423',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919703',
        'method' => 'get',
      ),
    ),
  ),
  1660263826 => 
  array (
    'id' => 15919727,
    'name' => 'ООО "ИНФИНИТИ ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500268152,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729437,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729437',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660263826',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729437,
        1 => 30729439,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729437,30729439',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919727',
        'method' => 'get',
      ),
    ),
  ),
  7424008221 => 
  array (
    'id' => 15919747,
    'name' => 'ООО "СКУ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500268396,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7424008221',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919747',
        'method' => 'get',
      ),
    ),
  ),
  2310169667 => 
  array (
    'id' => 15919837,
    'name' => 'ООО «КапРемонт-Юг»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500269252,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2310169667',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919837',
        'method' => 'get',
      ),
    ),
  ),
  8610004360 => 
  array (
    'id' => 15919853,
    'name' => 'ООО "ПРОЭКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500269309,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729551,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729551',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8610004360',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729551,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729551',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919853',
        'method' => 'get',
      ),
    ),
  ),
  2320135402 => 
  array (
    'id' => 15919857,
    'name' => 'ООО "ЮЖНАЯ АРХИТЕКТУРНАЯ КОМПАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500269328,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729553,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729553',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2320135402',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729553,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729553',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919857',
        'method' => 'get',
      ),
    ),
  ),
  2341014896 => 
  array (
    'id' => 15919879,
    'name' => 'ООО «Екатеринодар СтройСервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500269455,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729565,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729565',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2341014896',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729565,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729565',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919879',
        'method' => 'get',
      ),
    ),
  ),
  2372005468 => 
  array (
    'id' => 15919887,
    'name' => 'ООО "САНА-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500269609,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729573,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729573',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605778,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2372005468',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729573,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729573',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919887',
        'method' => 'get',
      ),
    ),
  ),
  526000892 => 
  array (
    'id' => 15919903,
    'name' => 'ООО "РУСТРОЙ-99"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500269752,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '526000892',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Дагестан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15919903',
        'method' => 'get',
      ),
    ),
  ),
  7841500188 => 
  array (
    'id' => 15920045,
    'name' => 'ООО "ТЕХНОСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500271582,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729757,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729757',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841500188',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729757,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729757',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920045',
        'method' => 'get',
      ),
    ),
  ),
  3666000753 => 
  array (
    'id' => 15920063,
    'name' => 'ООО "Киносарг"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500271701,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729769,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729769',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3666000753',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729769,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729769',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920063',
        'method' => 'get',
      ),
    ),
  ),
  4025443964 => 
  array (
    'id' => 15920075,
    'name' => 'ООО "КомСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500271810,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4025443964',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920075',
        'method' => 'get',
      ),
    ),
  ),
  7814543552 => 
  array (
    'id' => 15920149,
    'name' => 'ООО "НАФТА Северо-Запад"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500272452,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729863,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729863',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814543552',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729863,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729863',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920149',
        'method' => 'get',
      ),
    ),
  ),
  5005050967 => 
  array (
    'id' => 15920153,
    'name' => 'ООО «LBM-строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500272507,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5005050967',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920153',
        'method' => 'get',
      ),
    ),
  ),
  5027240601 => 
  array (
    'id' => 15920179,
    'name' => 'ООО "ЗаветРос"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500272608,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30729893,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729893',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027240601',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30729893,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30729893',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920179',
        'method' => 'get',
      ),
    ),
  ),
  5260307804 => 
  array (
    'id' => 15920235,
    'name' => 'ООО "РемСтройСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500273050,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30730009,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30730009',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5260307804',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30730009,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30730009',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920235',
        'method' => 'get',
      ),
    ),
  ),
  6205000250 => 
  array (
    'id' => 15920797,
    'name' => 'ООО "АльянсДорСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500276599,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30730909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30730909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6205000250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30730909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30730909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920797',
        'method' => 'get',
      ),
    ),
  ),
  7722592415 => 
  array (
    'id' => 15920963,
    'name' => 'ООО "ПроектМастер"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500277209,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30731093,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30731093',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722592415',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30731093,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30731093',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15920963',
        'method' => 'get',
      ),
    ),
  ),
  7724408894 => 
  array (
    'id' => 15922135,
    'name' => 'ООО "ЭлектроСпецСнаб"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500282008,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30732453,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30732453',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724408894',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30732453,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30732453',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15922135',
        'method' => 'get',
      ),
    ),
  ),
  7814575794 => 
  array (
    'id' => 15922317,
    'name' => 'ООО "МегаСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500282579,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30732679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30732679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814575794',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30732679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30732679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15922317',
        'method' => 'get',
      ),
    ),
  ),
  9102004378 => 
  array (
    'id' => 15922361,
    'name' => 'ООО  "ОЛЕГОР РК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500282822,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30733337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30733337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921256,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102004378',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30733337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30733337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15922361',
        'method' => 'get',
      ),
    ),
  ),
  910900011327 => 
  array (
    'id' => 15922399,
    'name' => 'ИП Кинигопуло Дмитрий Иванович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500283038,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30734819,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30734819',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '910900011327',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30734819,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30734819',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15922399',
        'method' => 'get',
      ),
    ),
  ),
  1327157474 => 
  array (
    'id' => 15923569,
    'name' => 'ООО  "КРАУН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500288107,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30759519,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30759519',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1327157474',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мордовия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30759519,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30759519',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15923569',
        'method' => 'get',
      ),
    ),
  ),
  7719810007 => 
  array (
    'id' => 15923635,
    'name' => 'ООО "ТехСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500288360,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30759605,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30759605',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719810007',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30759605,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30759605',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15923635',
        'method' => 'get',
      ),
    ),
  ),
  7202224800 => 
  array (
    'id' => 15923741,
    'name' => 'ООО "Ник Универсал"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500288687,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30759767,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30759767',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7202224800',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30759767,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30759767',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15923741',
        'method' => 'get',
      ),
    ),
  ),
  2632102593 => 
  array (
    'id' => 15923889,
    'name' => 'ООО «Дом Строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500289413,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30760003,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30760003',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632102593',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30760003,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30760003',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15923889',
        'method' => 'get',
      ),
    ),
  ),
  6453116269 => 
  array (
    'id' => 15924173,
    'name' => 'ООО "Механизация"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500290647,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30760371,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30760371',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6453116269',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30760371,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30760371',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15924173',
        'method' => 'get',
      ),
    ),
  ),
  2904023799 => 
  array (
    'id' => 15924297,
    'name' => 'ООО «Лавандерия-К»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500291238,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30760557,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30760557',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2904023799',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Архангельская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30760557,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30760557',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15924297',
        'method' => 'get',
      ),
    ),
  ),
  5029068113 => 
  array (
    'id' => 15925409,
    'name' => 'ЗАО "Измерительные комплексы и системы"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500296303,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30762239,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30762239',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5029068113',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30762239,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30762239',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15925409',
        'method' => 'get',
      ),
    ),
  ),
  267012060 => 
  array (
    'id' => 15928513,
    'name' => 'ООО "РОССТРОЙСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500352288,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '267012060',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928513',
        'method' => 'get',
      ),
    ),
  ),
  1717010854 => 
  array (
    'id' => 15928541,
    'name' => 'ООО "Атриум"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500353114,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1717010854',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тыва(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928541',
        'method' => 'get',
      ),
    ),
  ),
  2464117838 => 
  array (
    'id' => 15928551,
    'name' => 'ООО "ЯРГРАДСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500353480,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765667,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765667',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464117838',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765667,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765667',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928551',
        'method' => 'get',
      ),
    ),
  ),
  4217177586 => 
  array (
    'id' => 15928571,
    'name' => 'ООО "ИСК МЕЖДУРЕЧЕНСКСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500353833,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765701,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765701',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4217177586',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765701,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765701',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928571',
        'method' => 'get',
      ),
    ),
  ),
  5405256987 => 
  array (
    'id' => 15928583,
    'name' => 'ООО Строительная фирма "Сибирский Мастер"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500354065,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765719,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765719',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405256987',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765719,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765719',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928583',
        'method' => 'get',
      ),
    ),
  ),
  1650301335 => 
  array (
    'id' => 15928659,
    'name' => 'ООО СК «БАРАКАТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500355106,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765789,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765789',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1650301335',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765789,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765789',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928659',
        'method' => 'get',
      ),
    ),
  ),
  3525254113 => 
  array (
    'id' => 15928661,
    'name' => 'ООО «Вологдаинждорстрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500355161,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765795,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765795',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525254113',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Вологодская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765795,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765795',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928661',
        'method' => 'get',
      ),
    ),
  ),
  1655069125 => 
  array (
    'id' => 15928665,
    'name' => 'Республиканский Фонд возрождения памятников истории и культуры Республики татарстан',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500355205,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765799,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765799',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655069125',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765799,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765799',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928665',
        'method' => 'get',
      ),
    ),
  ),
  6830004130 => 
  array (
    'id' => 15928725,
    'name' => 'ООО "Рубин"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500356057,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30765877,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765877',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6830004130',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Липецкая область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30765877,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30765877',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928725',
        'method' => 'get',
      ),
    ),
  ),
  4703114400 => 
  array (
    'id' => 15928915,
    'name' => 'ООО  "НОРДИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500357932,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30766053,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766053',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4703114400',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ленинградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30766053,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766053',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928915',
        'method' => 'get',
      ),
    ),
  ),
  7714117913 => 
  array (
    'id' => 15928953,
    'name' => 'АО "Медиаскоп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500358219,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30766113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714117913',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30766113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928953',
        'method' => 'get',
      ),
    ),
  ),
  2603010820 => 
  array (
    'id' => 15928965,
    'name' => 'ООО "Строительно-монтажное предприятие Оптимум"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500358374,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30766131,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766131',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2603010820',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30766131,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766131',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15928965',
        'method' => 'get',
      ),
    ),
  ),
  4716041799 => 
  array (
    'id' => 15929149,
    'name' => 'ООО  "ПитерБасЦентр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500359736,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30766417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4716041799',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30766417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15929149',
        'method' => 'get',
      ),
    ),
  ),
  3662017072 => 
  array (
    'id' => 15929481,
    'name' => 'ООО НПП  "НФЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500362054,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30766875,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766875',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662017072',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30766875,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766875',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15929481',
        'method' => 'get',
      ),
    ),
  ),
  3662247742 => 
  array (
    'id' => 15929527,
    'name' => 'ООО "ТА-Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500362255,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30766951,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766951',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662247742',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30766951,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30766951',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15929527',
        'method' => 'get',
      ),
    ),
  ),
  4623006745 => 
  array (
    'id' => 15929837,
    'name' => 'ООО "Спецстроймонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500363798,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30767341,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767341',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4623006745',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30767341,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767341',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15929837',
        'method' => 'get',
      ),
    ),
  ),
  6217003865 => 
  array (
    'id' => 15929853,
    'name' => 'ООО "СПЕЦСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500363882,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30767367,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767367',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6217003865',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Рязанская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30767367,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767367',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15929853',
        'method' => 'get',
      ),
    ),
  ),
  5005057377 => 
  array (
    'id' => 15929941,
    'name' => 'ООО "СКХ-ЭКО"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500364383,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30767477,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767477',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5005057377',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30767477,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767477',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15929941',
        'method' => 'get',
      ),
    ),
  ),
  7703792786 => 
  array (
    'id' => 15930019,
    'name' => 'ООО «МедиоТех»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500364822,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30767599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703792786',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30767599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15930019',
        'method' => 'get',
      ),
    ),
  ),
  5008053412 => 
  array (
    'id' => 15930025,
    'name' => 'ООО "КрилакСпецстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500364859,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30767613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5008053412',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30767613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30767613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15930025',
        'method' => 'get',
      ),
    ),
  ),
  7708058138 => 
  array (
    'id' => 15930581,
    'name' => 'ООО ФИРМА "БИЗНЕС КОНСАЛТИНГ ЮНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500367136,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30768333,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30768333',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708058138',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30768333,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30768333',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15930581',
        'method' => 'get',
      ),
    ),
  ),
  9201516063 => 
  array (
    'id' => 15931227,
    'name' => 'ООО "СТРОЙ-ЮГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500369730,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30224831,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30224831',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9201516063',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30224831,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30224831',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15931227',
        'method' => 'get',
      ),
    ),
  ),
  5836671755 => 
  array (
    'id' => 15931853,
    'name' => 'ООО "ЭкоДорСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500372944,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30769989,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30769989',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5836671755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30769989,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30769989',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15931853',
        'method' => 'get',
      ),
    ),
  ),
  7705828572 => 
  array (
    'id' => 15932117,
    'name' => 'ООО "ПромСтройЭнергосервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500374147,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30770365,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770365',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705828572',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30770365,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770365',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15932117',
        'method' => 'get',
      ),
    ),
  ),
  7804552653 => 
  array (
    'id' => 15932243,
    'name' => 'ООО "ПРИОРИТЕТ Фарм"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500374685,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30770545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804552653',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30770545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15932243',
        'method' => 'get',
      ),
    ),
  ),
  6167065479 => 
  array (
    'id' => 15932341,
    'name' => 'ЗАО "СМУ №1"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500375099,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30770675,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770675',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6167065479',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30770675,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770675',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15932341',
        'method' => 'get',
      ),
    ),
  ),
  6917010650 => 
  array (
    'id' => 15932483,
    'name' => 'ООО "АВТОДОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500375735,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30770895,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770895',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6917010650',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30770895,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770895',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15932483',
        'method' => 'get',
      ),
    ),
  ),
  6952023154 => 
  array (
    'id' => 15932511,
    'name' => 'ООО "ИнженерэлектроСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500375932,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30770941,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770941',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6952023154',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30770941,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30770941',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15932511',
        'method' => 'get',
      ),
    ),
  ),
  7714938405 => 
  array (
    'id' => 15933131,
    'name' => 'ООО "ПРИОРИТЕТ-СТРОЙ ИНЖИНИРИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500378833,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30771847,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30771847',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714938405',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30771847,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30771847',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15933131',
        'method' => 'get',
      ),
    ),
  ),
  7715772456 => 
  array (
    'id' => 15933333,
    'name' => 'ООО "Элвекон"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500379854,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30772113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30772113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715772456',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30772113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30772113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15933333',
        'method' => 'get',
      ),
    ),
  ),
  7715959694 => 
  array (
    'id' => 15933355,
    'name' => 'ООО "Торговый дом "Просвещение-Регион"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500379993,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30772157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30772157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715959694',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30772157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30772157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15933355',
        'method' => 'get',
      ),
    ),
  ),
  4028056900 => 
  array (
    'id' => 15940053,
    'name' => 'ООО "РЕЗЕРВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500449646,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30782183,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30782183',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4028056900',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30782183,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30782183',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15940053',
        'method' => 'get',
      ),
    ),
  ),
  2632076135 => 
  array (
    'id' => 15940217,
    'name' => 'ООО  "СТРОЙИНВЕСТ-КМВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1500450372,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30782459,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30782459',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632076135',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30782459,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30782459',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15940217',
        'method' => 'get',
      ),
    ),
  ),
  323202196440 => 
  array (
    'id' => 15943907,
    'name' => 'ИП  Авдеев Дмитрий  Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500466953,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30794249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30794249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323202196440',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30794249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30794249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15943907',
        'method' => 'get',
      ),
    ),
  ),
  5249113146 => 
  array (
    'id' => 15944139,
    'name' => 'ООО  "Каскад"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500468223,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30794537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30794537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5249113146',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Нижегородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30794537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30794537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15944139',
        'method' => 'get',
      ),
    ),
  ),
  2466272370 => 
  array (
    'id' => 15947347,
    'name' => 'ООО "СТРОЙТЕХКОНТРОЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500524130,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30797999,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30797999',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466272370',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30797999,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30797999',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15947347',
        'method' => 'get',
      ),
    ),
  ),
  6166093233 => 
  array (
    'id' => 15947507,
    'name' => 'ООО "ПРЕСТИЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500526861,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30798203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6166093233',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30798203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15947507',
        'method' => 'get',
      ),
    ),
  ),
  6324070898 => 
  array (
    'id' => 15947675,
    'name' => 'ООО  "БИРЮЗА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500529020,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30798385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6324070898',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30798385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15947675',
        'method' => 'get',
      ),
    ),
  ),
  7811485447 => 
  array (
    'id' => 15947773,
    'name' => 'ООО «ГЛЭСК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500530212,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30798523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811485447',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30798523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15947773',
        'method' => 'get',
      ),
    ),
  ),
  1657131160 => 
  array (
    'id' => 15947787,
    'name' => 'АК ТАШ-Инвест, ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1277505,
    'created_at' => 1500530278,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30798535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657131160',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3 казань',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30798535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30798535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15947787',
        'method' => 'get',
      ),
    ),
  ),
  7816404811 => 
  array (
    'id' => 15948747,
    'name' => 'ООО "Промсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500535825,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30799699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30799699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816404811',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30799699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30799699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15948747',
        'method' => 'get',
      ),
    ),
  ),
  1421000128 => 
  array (
    'id' => 15955557,
    'name' => 'ООО "ОЛЕКМИНСКОЕ ГАТП"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500604220,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807501,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807501',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1421000128',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807501,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807501',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955557',
        'method' => 'get',
      ),
    ),
  ),
  1435232168 => 
  array (
    'id' => 15955565,
    'name' => 'ООО «Трансснабстрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500604446,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 1500699600,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435232168',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955565',
        'method' => 'get',
      ),
    ),
  ),
  1435318383 => 
  array (
    'id' => 15955567,
    'name' => 'ООО "СУ 17"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500604684,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605777,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435318383',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955567',
        'method' => 'get',
      ),
    ),
  ),
  2224078350 => 
  array (
    'id' => 15955589,
    'name' => 'ООО "АВТОГРАФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500605797,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807561,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807561',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500606062,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224078350',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807561,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807561',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955589',
        'method' => 'get',
      ),
    ),
  ),
  2420200038 => 
  array (
    'id' => 15955595,
    'name' => 'ООО "Сибирская Лесопромышленная Компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500606361,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807569,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807569',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2420200038',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807569,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807569',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955595',
        'method' => 'get',
      ),
    ),
  ),
  2443026682 => 
  array (
    'id' => 15955611,
    'name' => 'ООО "СМЭП+"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500607251,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807595,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807595',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2443026682',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807595,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807595',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955611',
        'method' => 'get',
      ),
    ),
  ),
  2460235541 => 
  array (
    'id' => 15955615,
    'name' => 'ООО СК "Сибирь"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500607366,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460235541',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955615',
        'method' => 'get',
      ),
    ),
  ),
  2502046965 => 
  array (
    'id' => 15955617,
    'name' => 'ООО  "Технострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500607509,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807601,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807601',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2502046965',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807601,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807601',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955617',
        'method' => 'get',
      ),
    ),
  ),
  6452944742 => 
  array (
    'id' => 15955819,
    'name' => 'ООО Профессионал',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500611513,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30807869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6452944742',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30807869,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30807869',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955819',
        'method' => 'get',
      ),
    ),
  ),
  7447195730 => 
  array (
    'id' => 15955921,
    'name' => 'ООО "Интерстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500612990,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7447195730',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955921',
        'method' => 'get',
      ),
    ),
  ),
  276083011 => 
  array (
    'id' => 15955979,
    'name' => 'ООО Фирма «Стройтех»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500613797,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808069,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808069',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '276083011',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808069,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808069',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15955979',
        'method' => 'get',
      ),
    ),
  ),
  6316152071 => 
  array (
    'id' => 15956021,
    'name' => 'ООО "СтройБизнес"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500614281,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808107,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808107',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316152071',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808107,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808107',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956021',
        'method' => 'get',
      ),
    ),
  ),
  2130184458 => 
  array (
    'id' => 15956097,
    'name' => 'ООО "АВТОДОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500615378,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808199,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808199',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130184458',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808199,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808199',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956097',
        'method' => 'get',
      ),
    ),
  ),
  7730156540 => 
  array (
    'id' => 15956325,
    'name' => 'ООО ЧОП "КАСКАД - РСП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500618000,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808469,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808469',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730156540',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808469,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808469',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956325',
        'method' => 'get',
      ),
    ),
  ),
  7734393830 => 
  array (
    'id' => 15956389,
    'name' => 'ООО "ИНТЕГРАЦИОННЫЕ РЕШЕНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500618415,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808533,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808533',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734393830',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956389',
        'method' => 'get',
      ),
    ),
  ),
  2601008103 => 
  array (
    'id' => 15956449,
    'name' => 'ООО "Амфора-С"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500618734,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808585,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808585',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921253,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2601008103',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808585,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808585',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956449',
        'method' => 'get',
      ),
    ),
  ),
  2628009834 => 
  array (
    'id' => 15956481,
    'name' => 'АО «Передвижная механизированная колонна №38»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500618983,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2628009834',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956481',
        'method' => 'get',
      ),
    ),
  ),
  5190037397 => 
  array (
    'id' => 15956499,
    'name' => 'ООО "Кроссджойн"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500619165,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808653,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808653',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190037397',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808653,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808653',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956499',
        'method' => 'get',
      ),
    ),
  ),
  2632098354 => 
  array (
    'id' => 15956619,
    'name' => 'ООО «Би-Техно»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500619877,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30808797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632098354',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30808797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30808797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956619',
        'method' => 'get',
      ),
    ),
  ),
  3460007032 => 
  array (
    'id' => 15956851,
    'name' => 'ООО "ПРОЗВОДСТВЕННОЕ ОБЪЕДИНЕНИЕ "СПЕЦКОНСТРУКЦИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500621398,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30809123,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809123',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3460007032',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30809123,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809123',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956851',
        'method' => 'get',
      ),
    ),
  ),
  4826122200 => 
  array (
    'id' => 15956945,
    'name' => 'ООО "Вектор"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500621938,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30809235,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809235',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826122200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30809235,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809235',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956945',
        'method' => 'get',
      ),
    ),
  ),
  6950163170 => 
  array (
    'id' => 15957013,
    'name' => 'ООО  "ТВЕРЬГОРСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500622264,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30809333,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809333',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950163170',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30809333,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809333',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15957013',
        'method' => 'get',
      ),
    ),
  ),
  5040087818 => 
  array (
    'id' => 15957091,
    'name' => 'ООО "ЭКОСТРОЙТРЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500622789,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30809445,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809445',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040087818',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30809445,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809445',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15957091',
        'method' => 'get',
      ),
    ),
  ),
  5050091796 => 
  array (
    'id' => 15957217,
    'name' => 'ООО «ДорсСтрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500623509,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30809605,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809605',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5050091796',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30809605,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30809605',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15957217',
        'method' => 'get',
      ),
    ),
  ),
  6330069932 => 
  array (
    'id' => 15958251,
    'name' => 'Формат, ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500627044,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29325385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6330069932',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29325385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29325385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15958251',
        'method' => 'get',
      ),
    ),
  ),
  5934030943 => 
  array (
    'id' => 15959385,
    'name' => 'ООО "Урал"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500631158,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30812271,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30812271',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5934030943',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30812271,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30812271',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15959385',
        'method' => 'get',
      ),
    ),
  ),
  7731472805 => 
  array (
    'id' => 15960495,
    'name' => 'ООО ФИРМА «АЛКОР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500635394,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30813923,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30813923',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731472805',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30813923,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30813923',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15960495',
        'method' => 'get',
      ),
    ),
  ),
  3444207348 => 
  array (
    'id' => 15960921,
    'name' => 'ООО "ЕВРОАЗ СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500637288,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30814449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30814449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921254,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444207348',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30814449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30814449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15960921',
        'method' => 'get',
      ),
    ),
  ),
  7723686761 => 
  array (
    'id' => 15961113,
    'name' => 'ООО "Русток"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500638452,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30814711,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30814711',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723686761',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30814711,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30814711',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15961113',
        'method' => 'get',
      ),
    ),
  ),
  5807901080 => 
  array (
    'id' => 15961157,
    'name' => 'ООО "НИКСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500638778,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30814775,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30814775',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5807901080',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пензенская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30814775,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30814775',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15961157',
        'method' => 'get',
      ),
    ),
  ),
  326543683 => 
  array (
    'id' => 15982917,
    'name' => 'ООО "ЭЛИКОМ ТБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500864474,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30842077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30842077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326543683',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30842077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30842077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15982917',
        'method' => 'get',
      ),
    ),
  ),
  2460087156 => 
  array (
    'id' => 15982981,
    'name' => 'ООО "КРАСНОЯРСКЭНЕРГОСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500866888,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30842189,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30842189',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460087156',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30842189,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30842189',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15982981',
        'method' => 'get',
      ),
    ),
  ),
  2130042453 => 
  array (
    'id' => 15983447,
    'name' => 'ООО "ИНФОРМАЦИОННЫЕ СОЦИАЛЬНЫЕ СИСТЕМЫ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500875248,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30843111,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30843111',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130042453',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30843111,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30843111',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15983447',
        'method' => 'get',
      ),
    ),
  ),
  6950046276 => 
  array (
    'id' => 15983771,
    'name' => 'ООО "Торговый дом "Тверькоммаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500878098,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30843577,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30843577',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950046276',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30843577,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30843577',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15983771',
        'method' => 'get',
      ),
    ),
  ),
  7708810125 => 
  array (
    'id' => 15985169,
    'name' => 'ООО "ННК КОНСАЛТИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500880532,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30845183,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30845183',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708810125',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30845183,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30845183',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15985169',
        'method' => 'get',
      ),
    ),
  ),
  3906961859 => 
  array (
    'id' => 15985685,
    'name' => 'ООО "КУЗНЕЧНЫЙ ДОМ № 1"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1500882029,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30845795,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30845795',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906961859',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30845795,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30845795',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15985685',
        'method' => 'get',
      ),
    ),
  ),
  5074052193 => 
  array (
    'id' => 15986337,
    'name' => 'ООО "Система Телеком"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500884705,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30846709,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30846709',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5074052193',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30846709,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30846709',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15986337',
        'method' => 'get',
      ),
    ),
  ),
  5020076977 => 
  array (
    'id' => 15986511,
    'name' => 'ООО "ГарантСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500885347,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30846965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30846965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5020076977',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30846965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30846965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15986511',
        'method' => 'get',
      ),
    ),
  ),
  2128706361 => 
  array (
    'id' => 15988623,
    'name' => 'ООО «Регион»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500889339,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30850445,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30850445',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2128706361',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чувашия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30850445,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30850445',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15988623',
        'method' => 'get',
      ),
    ),
  ),
  4632021274 => 
  array (
    'id' => 15989077,
    'name' => 'ООО "Рубиком"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500891018,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30850929,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30850929',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632021274',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30850929,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30850929',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15989077',
        'method' => 'get',
      ),
    ),
  ),
  9201512005 => 
  array (
    'id' => 15989607,
    'name' => 'ООО "КРИСТАЛЛ СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500892716,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30851521,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30851521',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9201512005',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Севастополь(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30851521,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30851521',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15989607',
        'method' => 'get',
      ),
    ),
  ),
  1435197001 => 
  array (
    'id' => 15994611,
    'name' => 'ООО "СТАНДАРТ+"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500949972,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30901921,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30901921',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1501565930,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435197001',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30901921,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30901921',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994611',
        'method' => 'get',
      ),
    ),
  ),
  4205284135 => 
  array (
    'id' => 15994703,
    'name' => 'ООО ТД "ИНТЕРЬЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500954531,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902287,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902287',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205284135',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902287,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902287',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994703',
        'method' => 'get',
      ),
    ),
  ),
  7806257910 => 
  array (
    'id' => 15994795,
    'name' => 'ООО "Музыкальная Планета"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500956685,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902419,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902419',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806257910',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902419,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902419',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994795',
        'method' => 'get',
      ),
    ),
  ),
  6603020287 => 
  array (
    'id' => 15994803,
    'name' => 'ООО "Асбострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500956885,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902441,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902441',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6603020287',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902441,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902441',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994803',
        'method' => 'get',
      ),
    ),
  ),
  2463094360 => 
  array (
    'id' => 15994809,
    'name' => 'ООО "КРАСРОСА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500956930,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463094360',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994809',
        'method' => 'get',
      ),
    ),
  ),
  7536123254 => 
  array (
    'id' => 15994853,
    'name' => 'ООО «ДВМ-Чита»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500957460,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7536123254',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994853',
        'method' => 'get',
      ),
    ),
  ),
  5905027671 => 
  array (
    'id' => 15994881,
    'name' => 'ООО "ПЕРМЬТЕХСПЕЦ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500958295,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902569,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902569',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905027671',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902569,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902569',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994881',
        'method' => 'get',
      ),
    ),
  ),
  1655292998 => 
  array (
    'id' => 15994927,
    'name' => 'ООО «Стройсервис ТАПС»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500959267,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902671,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902671',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655292998',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902671,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902671',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994927',
        'method' => 'get',
      ),
    ),
  ),
  4825056523 => 
  array (
    'id' => 15994945,
    'name' => 'ООО "СтройМастер"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500959676,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902701,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902701',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825056523',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902701,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902701',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994945',
        'method' => 'get',
      ),
    ),
  ),
  7720372767 => 
  array (
    'id' => 15994971,
    'name' => 'ООО "АндроМед"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500960033,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30902737,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902737',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720372767',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30902737,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30902737',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15994971',
        'method' => 'get',
      ),
    ),
  ),
  7704307626 => 
  array (
    'id' => 15995251,
    'name' => 'ООО «Термо Техно Инжиниринг»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500963031,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30903079,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30903079',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704307626',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Дагестан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30903079,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30903079',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15995251',
        'method' => 'get',
      ),
    ),
  ),
  7840476084 => 
  array (
    'id' => 15995299,
    'name' => 'ООО "Авангард"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500963297,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30903147,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30903147',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840476084',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30903147,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30903147',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15995299',
        'method' => 'get',
      ),
    ),
  ),
  7804377190 => 
  array (
    'id' => 15995371,
    'name' => 'ООО «Легион Строй Монтаж»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500963767,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30903229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30903229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921250,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804377190',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30903229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30903229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15995371',
        'method' => 'get',
      ),
    ),
  ),
  6150077878 => 
  array (
    'id' => 15998417,
    'name' => 'ООО «ВОСХОД»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1500974310,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30907361,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30907361',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6150077878',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30907361,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30907361',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15998417',
        'method' => 'get',
      ),
    ),
  ),
  663100200204 => 
  array (
    'id' => 15999459,
    'name' => 'ИП  Король Людмила Дохциконовна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500978762,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30912133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30912133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '663100200204',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30912133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30912133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15999459',
        'method' => 'get',
      ),
    ),
  ),
  2348036553 => 
  array (
    'id' => 15999635,
    'name' => 'ООО "ВОСХОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500979519,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30912355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30912355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2348036553',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30912355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30912355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15999635',
        'method' => 'get',
      ),
    ),
  ),
  7603065064 => 
  array (
    'id' => 16000765,
    'name' => 'ООО "ПРОСТОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500985419,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30913903,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30913903',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7603065064',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ярославская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30913903,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30913903',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16000765',
        'method' => 'get',
      ),
    ),
  ),
  7816565696 => 
  array (
    'id' => 16000927,
    'name' => 'ООО "Строй Инвест"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1500986146,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30914103,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30914103',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816565696',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30914103,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30914103',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16000927',
        'method' => 'get',
      ),
    ),
  ),
  326492044 => 
  array (
    'id' => 16004171,
    'name' => 'ООО "КАМСТРОМ ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501039379,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30917533,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917533',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326492044',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30917533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16004171',
        'method' => 'get',
      ),
    ),
  ),
  1435317686 => 
  array (
    'id' => 16004349,
    'name' => 'ООО "ИМПУЛЬС ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501043088,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30917827,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917827',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435317686',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30917827,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917827',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16004349',
        'method' => 'get',
      ),
    ),
  ),
  7604284090 => 
  array (
    'id' => 16004371,
    'name' => 'ООО «Технопрайм»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501043316,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30917863,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917863',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7604284090',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хабаровский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30917863,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917863',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16004371',
        'method' => 'get',
      ),
    ),
  ),
  6321401277 => 
  array (
    'id' => 16004381,
    'name' => 'ООО "ГРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501043501,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30917879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6321401277',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30917879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30917879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16004381',
        'method' => 'get',
      ),
    ),
  ),
  8604060399 => 
  array (
    'id' => 16004591,
    'name' => 'ООО "Стройбест"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501046409,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30918179,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918179',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8604060399',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30918179,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918179',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16004591',
        'method' => 'get',
      ),
    ),
  ),
  6234164319 => 
  array (
    'id' => 16004615,
    'name' => 'ООО «Общестрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501046871,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30918205,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918205',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6234164319',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Рязанская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30918205,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918205',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16004615',
        'method' => 'get',
      ),
    ),
  ),
  1835085655 => 
  array (
    'id' => 16004657,
    'name' => 'ООО "Агат"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501047501,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30918273,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918273',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1835085655',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Удмуртия(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30918273,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918273',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16004657',
        'method' => 'get',
      ),
    ),
  ),
  105063939 => 
  array (
    'id' => 16005219,
    'name' => 'ООО "ГАЗОБЪЕКТ*"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501051942,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30918955,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918955',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '105063939',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30918955,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30918955',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16005219',
        'method' => 'get',
      ),
    ),
  ),
  2618021310 => 
  array (
    'id' => 16005369,
    'name' => 'ООО "РЕГИОНАЛЬНАЯ ДОРОЖНО-СТРОИТЕЛЬНАЯ КОМПАНИЯ БЕТОН ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501052834,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30919143,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919143',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2618021310',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30919143,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919143',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16005369',
        'method' => 'get',
      ),
    ),
  ),
  7710092753 => 
  array (
    'id' => 16005565,
    'name' => '"ГИЛЬДИЯ КИНОРЕЖИССЕРОВ РОССИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501053853,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30919389,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919389',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710092753',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Смоленская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30919389,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919389',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16005565',
        'method' => 'get',
      ),
    ),
  ),
  7734647620 => 
  array (
    'id' => 16005651,
    'name' => 'ООО "Маллит"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501054393,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30919523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734647620',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30919523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16005651',
        'method' => 'get',
      ),
    ),
  ),
  2635225755 => 
  array (
    'id' => 16005839,
    'name' => 'ООО «Группа Компаний «Строй-Ка»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501055174,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30919741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635225755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30919741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30919741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16005839',
        'method' => 'get',
      ),
    ),
  ),
  7718265006 => 
  array (
    'id' => 16006169,
    'name' => 'ООО "Аркс Плюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501056732,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30920119,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30920119',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718265006',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30920119,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30920119',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16006169',
        'method' => 'get',
      ),
    ),
  ),
  7714556050 => 
  array (
    'id' => 16006187,
    'name' => 'ООО  "СТРОЙТЕХПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501056867,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30920157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30920157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714556050',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30920157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30920157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16006187',
        'method' => 'get',
      ),
    ),
  ),
  5262279694 => 
  array (
    'id' => 16007479,
    'name' => 'ООО МВ52',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501062145,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30921663,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30921663',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5262279694',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30921663,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30921663',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16007479',
        'method' => 'get',
      ),
    ),
  ),
  2629800887 => 
  array (
    'id' => 16008671,
    'name' => 'ООО ТСК "Строительные технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501067196,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30923141,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30923141',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921251,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2629800887',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30923141,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30923141',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16008671',
        'method' => 'get',
      ),
    ),
  ),
  5410047523 => 
  array (
    'id' => 16013205,
    'name' => 'ООО "Навигатор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501129999,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30928557,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928557',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410047523',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30928557,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928557',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013205',
        'method' => 'get',
      ),
    ),
  ),
  2724212010 => 
  array (
    'id' => 16013299,
    'name' => 'ООО "АМУРСТРОЙБИЗНЕС ДВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501131679,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30928741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2724212010',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Еврейская АО(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30928741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013299',
        'method' => 'get',
      ),
    ),
  ),
  2465121636 => 
  array (
    'id' => 16013349,
    'name' => 'ООО "ГРАНД ХОЛЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501132699,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30928801,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928801',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465121636',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30928801,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928801',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013349',
        'method' => 'get',
      ),
    ),
  ),
  608025249 => 
  array (
    'id' => 16013439,
    'name' => 'ООО "СТРОЙГАРАНТ+"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501133659,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30928899,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928899',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '608025249',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30928899,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30928899',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013439',
        'method' => 'get',
      ),
    ),
  ),
  7801619476 => 
  array (
    'id' => 16013601,
    'name' => 'ООО  «АСКОН-Интеграционные решения»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501135274,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30929091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921252,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801619476',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Дагестан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30929091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013601',
        'method' => 'get',
      ),
    ),
  ),
  264073259 => 
  array (
    'id' => 16013915,
    'name' => 'ООО "ЭЛИТ-РЕГИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501137766,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30929525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '264073259',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30929525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013915',
        'method' => 'get',
      ),
    ),
  ),
  7701583586 => 
  array (
    'id' => 16013959,
    'name' => 'ООО "ВТК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501137957,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30929563,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929563',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701583586',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30929563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013959',
        'method' => 'get',
      ),
    ),
  ),
  7719462790 => 
  array (
    'id' => 16013995,
    'name' => 'ООО "ЭДУТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501138254,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30929623,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929623',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719462790',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30929623,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30929623',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16013995',
        'method' => 'get',
      ),
    ),
  ),
  7708277228 => 
  array (
    'id' => 16014247,
    'name' => 'ООО "АЛЬФА ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501139894,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30930007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30930007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708277228',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30930007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30930007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16014247',
        'method' => 'get',
      ),
    ),
  ),
  7725752699 => 
  array (
    'id' => 16014511,
    'name' => 'ООО "Управленческие решения в сфере образования"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501140998,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30930379,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30930379',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725752699',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30930379,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30930379',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16014511',
        'method' => 'get',
      ),
    ),
  ),
  3811105218 => 
  array (
    'id' => 16015883,
    'name' => 'ООО "Проектный центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501147282,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30941353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30941353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811105218',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30941353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30941353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16015883',
        'method' => 'get',
      ),
    ),
  ),
  816031564 => 
  array (
    'id' => 16016153,
    'name' => 'ООО "СтройИнвестМонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501148460,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30941709,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30941709',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '816031564',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калмыкия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30941709,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30941709',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16016153',
        'method' => 'get',
      ),
    ),
  ),
  7728893306 => 
  array (
    'id' => 16016445,
    'name' => 'ООО "МАКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501150001,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30942091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30942091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728893306',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30942091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30942091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16016445',
        'method' => 'get',
      ),
    ),
  ),
  7721747031 => 
  array (
    'id' => 16018341,
    'name' => 'ООО "Энерготехмонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501159928,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30972591,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30972591',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721747031',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30972591,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30972591',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16018341',
        'method' => 'get',
      ),
    ),
  ),
  2204054375 => 
  array (
    'id' => 16020731,
    'name' => 'ООО "Материалстройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501211242,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30983625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30983625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1501227915,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2204054375',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30983625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30983625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16020731',
        'method' => 'get',
      ),
    ),
  ),
  7203014481 => 
  array (
    'id' => 16020783,
    'name' => 'ЗАО "Отделочник-20"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501213474,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30983811,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30983811',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203014481',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30983811,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30983811',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16020783',
        'method' => 'get',
      ),
    ),
  ),
  6670450493 => 
  array (
    'id' => 16020841,
    'name' => 'ООО "РЕГИОНСОЛЬТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501214759,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30983941,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30983941',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670450493',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30983941,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30983941',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16020841',
        'method' => 'get',
      ),
    ),
  ),
  4205282032 => 
  array (
    'id' => 16020937,
    'name' => 'ООО  «КСГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501216834,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30984193,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984193',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205282032',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кемеровская область(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30984193,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984193',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16020937',
        'method' => 'get',
      ),
    ),
  ),
  1657220029 => 
  array (
    'id' => 16020953,
    'name' => 'ООО «ЭКОСТРОЙИНЖИНИРИНГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501216941,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30984211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657220029',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30984211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16020953',
        'method' => 'get',
      ),
    ),
  ),
  4826049102 => 
  array (
    'id' => 16021171,
    'name' => 'ООО "КровСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501219353,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30984401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826049102',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Липецкая область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30984401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16021171',
        'method' => 'get',
      ),
    ),
  ),
  7811103970 => 
  array (
    'id' => 16021233,
    'name' => 'ООО  "Рекламное Агентство "ШТРИХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501220173,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30984453,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984453',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811103970',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30984453,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984453',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16021233',
        'method' => 'get',
      ),
    ),
  ),
  5049021610 => 
  array (
    'id' => 16021389,
    'name' => 'ООО "РОСКО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501222276,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30984701,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984701',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5049021610',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30984701,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984701',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16021389',
        'method' => 'get',
      ),
    ),
  ),
  7702764056 => 
  array (
    'id' => 16021451,
    'name' => 'ООО "Группа "Энергострой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501222699,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30984773,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984773',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702764056',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30984773,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984773',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16021451',
        'method' => 'get',
      ),
    ),
  ),
  7719893211 => 
  array (
    'id' => 16021589,
    'name' => 'ООО "Автопартс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501223569,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30984981,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984981',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719893211',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30984981,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30984981',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16021589',
        'method' => 'get',
      ),
    ),
  ),
  7706647434 => 
  array (
    'id' => 16021667,
    'name' => 'ООО "ШАБАДИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501224066,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30985099,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30985099',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706647434',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30985099,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30985099',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16021667',
        'method' => 'get',
      ),
    ),
  ),
  2360005907 => 
  array (
    'id' => 16022295,
    'name' => 'ООО "СТО "ФОРМУЛА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501227857,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30985911,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30985911',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2360005907',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30985911,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30985911',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16022295',
        'method' => 'get',
      ),
    ),
  ),
  4825042400 => 
  array (
    'id' => 16023155,
    'name' => 'ООО  "ЦентрСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501231715,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30986945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30986945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825042400',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Липецкая область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30986945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30986945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16023155',
        'method' => 'get',
      ),
    ),
  ),
  7457002440 => 
  array (
    'id' => 16023517,
    'name' => 'ООО "Универсал"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501233566,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30987431,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30987431',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7457002440',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30987431,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30987431',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16023517',
        'method' => 'get',
      ),
    ),
  ),
  5611070561 => 
  array (
    'id' => 16035149,
    'name' => 'ООО "ЭМКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501475535,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31011091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31011091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5611070561',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Оренбургская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31011091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31011091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16035149',
        'method' => 'get',
      ),
    ),
  ),
  6165130122 => 
  array (
    'id' => 16035335,
    'name' => 'ЗАО "ОКН-ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501477857,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31014079,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31014079',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165130122',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31014079,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31014079',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16035335',
        'method' => 'get',
      ),
    ),
  ),
  2636806921 => 
  array (
    'id' => 16035395,
    'name' => 'ООО «КТМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501478791,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31015433,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31015433',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2636806921',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31015433,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31015433',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16035395',
        'method' => 'get',
      ),
    ),
  ),
  3435053839 => 
  array (
    'id' => 16037233,
    'name' => 'ООО  "Джоуль"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501488223,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31020075,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31020075',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435053839',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31020075,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31020075',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16037233',
        'method' => 'get',
      ),
    ),
  ),
  263201785012 => 
  array (
    'id' => 16038749,
    'name' => 'ИП Погорелов Геннадий Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501494644,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31022147,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31022147',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '263201785012',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31022147,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31022147',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16038749',
        'method' => 'get',
      ),
    ),
  ),
  1435268421 => 
  array (
    'id' => 16044547,
    'name' => 'ООО ИСК "Союз"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501557690,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31029505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31029505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435268421',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31029505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31029505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16044547',
        'method' => 'get',
      ),
    ),
  ),
  2224182791 => 
  array (
    'id' => 16044549,
    'name' => 'ООО "ЭПСИЛОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501557766,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31029513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31029513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224182791',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31029513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31029513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16044549',
        'method' => 'get',
      ),
    ),
  ),
  7722699172 => 
  array (
    'id' => 16045099,
    'name' => 'ООО "ЭТМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501566448,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31030353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31030353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722699172',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31030353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31030353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16045099',
        'method' => 'get',
      ),
    ),
  ),
  532011204 => 
  array (
    'id' => 16045313,
    'name' => 'ПК "СПУТНИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501567994,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31030615,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31030615',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '532011204',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Дагестан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31030615,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31030615',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16045313',
        'method' => 'get',
      ),
    ),
  ),
  5014011080 => 
  array (
    'id' => 16045561,
    'name' => 'ООО "ТРЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501569925,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31031071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31031071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921248,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5014011080',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31031071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31031071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16045561',
        'method' => 'get',
      ),
    ),
  ),
  2221023306 => 
  array (
    'id' => 16053905,
    'name' => 'ООО "Стройконтракт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501649022,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31040229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31040229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221023306',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31040229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31040229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16053905',
        'method' => 'get',
      ),
    ),
  ),
  5236133 => 
  array (
    'id' => 16055781,
    'name' => 'ООО "ТРАНЗИТ-Н"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501661560,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31042883,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31042883',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5236133',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31042883,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31042883',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16055781',
        'method' => 'get',
      ),
    ),
  ),
  1215120187 => 
  array (
    'id' => 16060223,
    'name' => 'ООО «Строительная индустрия»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501678343,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31048707,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31048707',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1215120187',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31048707,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31048707',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16060223',
        'method' => 'get',
      ),
    ),
  ),
  2465089904 => 
  array (
    'id' => 16063279,
    'name' => 'ООО "КРАЙИНВЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501733640,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31052287,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31052287',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465089904',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хакасия(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31052287,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31052287',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16063279',
        'method' => 'get',
      ),
    ),
  ),
  5402015849 => 
  array (
    'id' => 16063357,
    'name' => 'ООО "Гранит"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501734817,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31052401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31052401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402015849',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31052401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31052401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16063357',
        'method' => 'get',
      ),
    ),
  ),
  3628017435 => 
  array (
    'id' => 16064171,
    'name' => 'ООО "КЛИНКЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501743202,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31053569,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31053569',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3628017435',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31053569,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31053569',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16064171',
        'method' => 'get',
      ),
    ),
  ),
  4826039506 => 
  array (
    'id' => 16064457,
    'name' => 'ООО "Диджитал Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501744624,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31053935,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31053935',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826039506',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31053935,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31053935',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16064457',
        'method' => 'get',
      ),
    ),
  ),
  5009108470 => 
  array (
    'id' => 16064797,
    'name' => 'ООО "ГрадСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501746492,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31054433,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054433',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5009108470',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31054433,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054433',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16064797',
        'method' => 'get',
      ),
    ),
  ),
  5022086522 => 
  array (
    'id' => 16064851,
    'name' => 'ООО "СтройИндустрия-В"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501746750,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31054481,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054481',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5022086522',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31054481,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054481',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16064851',
        'method' => 'get',
      ),
    ),
  ),
  5029205458 => 
  array (
    'id' => 16064901,
    'name' => 'ООО «МОСКОВСКАЯ ОБЛАСТНАЯ ИНЖЕНЕРНАЯ КОМПАНИЯ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501747120,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31054559,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054559',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5029205458',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31054559,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054559',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16064901',
        'method' => 'get',
      ),
    ),
  ),
  5110002112 => 
  array (
    'id' => 16064933,
    'name' => 'ООО СТС',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501747303,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31054609,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054609',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5110002112',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31054609,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31054609',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16064933',
        'method' => 'get',
      ),
    ),
  ),
  7810496502 => 
  array (
    'id' => 16066887,
    'name' => 'ООО «ГТМ-стройсервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501756054,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31057067,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31057067',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810496502',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31057067,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31057067',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16066887',
        'method' => 'get',
      ),
    ),
  ),
  3019012870 => 
  array (
    'id' => 16067137,
    'name' => 'ООО "РосГосСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501757426,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31057417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31057417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3019012870',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Астраханская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31057417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31057417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16067137',
        'method' => 'get',
      ),
    ),
  ),
  7805675873 => 
  array (
    'id' => 16067297,
    'name' => 'ООО ГИДРАВЛИЧЕСКИЙ ИНСТРУМЕНТ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501758259,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31057799,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31057799',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805675873',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31057799,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31057799',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16067297',
        'method' => 'get',
      ),
    ),
  ),
  7723580853 => 
  array (
    'id' => 16068009,
    'name' => 'ООО "Мастер Клининг"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501761838,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31063181,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31063181',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723580853',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31063181,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31063181',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16068009',
        'method' => 'get',
      ),
    ),
  ),
  5321185962 => 
  array (
    'id' => 16071969,
    'name' => 'ООО "ТРЭКСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501823930,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31107147,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107147',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5321185962',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31107147,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107147',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16071969',
        'method' => 'get',
      ),
    ),
  ),
  1020001533 => 
  array (
    'id' => 16072027,
    'name' => 'ООО "ПСК Строитель"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501824340,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31107203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1020001533',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31107203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16072027',
        'method' => 'get',
      ),
    ),
  ),
  1660192237 => 
  array (
    'id' => 16072061,
    'name' => 'ООО «ЭКОИНЖИНИРИНГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501824857,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31107259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660192237',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31107259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16072061',
        'method' => 'get',
      ),
    ),
  ),
  7722206003 => 
  array (
    'id' => 16072111,
    'name' => 'ООО «АЛЬФ ЛЭНД»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501825473,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31107321,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107321',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722206003',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Владимирская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31107321,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107321',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16072111',
        'method' => 'get',
      ),
    ),
  ),
  7810416539 => 
  array (
    'id' => 16072465,
    'name' => 'ООО "КУПЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501828324,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31107893,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107893',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810416539',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31107893,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31107893',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16072465',
        'method' => 'get',
      ),
    ),
  ),
  614205606473 => 
  array (
    'id' => 16073017,
    'name' => 'ИП Мигулин Владимир Иванович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501831526,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31112597,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31112597',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '614205606473',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31112597,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31112597',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16073017',
        'method' => 'get',
      ),
    ),
  ),
  4027110174 => 
  array (
    'id' => 16073323,
    'name' => 'ООО "Партнёр"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1501833264,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31113557,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31113557',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921249,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4027110174',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31113557,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31113557',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16073323',
        'method' => 'get',
      ),
    ),
  ),
  7727302938 => 
  array (
    'id' => 16074721,
    'name' => 'ООО "СТРОЙЛОГИСТИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1501839781,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31116291,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31116291',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727302938',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31116291,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31116291',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16074721',
        'method' => 'get',
      ),
    ),
  ),
  2312130802 => 
  array (
    'id' => 16086577,
    'name' => 'ООО "Промспецмонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502085531,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31129203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31129203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312130802',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31129203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31129203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16086577',
        'method' => 'get',
      ),
    ),
  ),
  2452012742 => 
  array (
    'id' => 16087763,
    'name' => 'ООО «Аврора»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502091443,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31130699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31130699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2452012742',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярск(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31130699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31130699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16087763',
        'method' => 'get',
      ),
    ),
  ),
  5753052409 => 
  array (
    'id' => 16088665,
    'name' => 'ООО "Фортуна"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502094599,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31131631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31131631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5753052409',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Орловская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31131631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31131631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16088665',
        'method' => 'get',
      ),
    ),
  ),
  3911016096 => 
  array (
    'id' => 16090445,
    'name' => 'МУП "СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502101241,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31134593,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31134593',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3911016096',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калининградская область(GMT +2)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31134593,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31134593',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16090445',
        'method' => 'get',
      ),
    ),
  ),
  5029186702 => 
  array (
    'id' => 16090977,
    'name' => 'ООО "СТРОЙПОСТАВКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502103095,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31135063,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31135063',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5029186702',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31135063,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31135063',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16090977',
        'method' => 'get',
      ),
    ),
  ),
  3444216208 => 
  array (
    'id' => 16092179,
    'name' => 'ООО  "ПРОГРЕССИВНЫЕ ТЕХНОЛОГИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502107991,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31136417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31136417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444216208',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31136417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31136417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16092179',
        'method' => 'get',
      ),
    ),
  ),
  7017331067 => 
  array (
    'id' => 16095841,
    'name' => 'ООО "ТомРесурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502162833,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31140489,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140489',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017331067',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Томская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31140489,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140489',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16095841',
        'method' => 'get',
      ),
    ),
  ),
  7404061280 => 
  array (
    'id' => 16095861,
    'name' => 'ООО "СтройИндустрия"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502163677,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31140529,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140529',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7404061280',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31140529,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140529',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16095861',
        'method' => 'get',
      ),
    ),
  ),
  5902198460 => 
  array (
    'id' => 16095949,
    'name' => 'АО «КОРПОРАЦИЯ РАЗВИТИЯ ПЕРМСКОГО КРАЯ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502165581,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31140655,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140655',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902198460',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31140655,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140655',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16095949',
        'method' => 'get',
      ),
    ),
  ),
  6321379663 => 
  array (
    'id' => 16096065,
    'name' => 'ООО "Профи Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502167655,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31140849,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140849',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6321379663',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31140849,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31140849',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16096065',
        'method' => 'get',
      ),
    ),
  ),
  3025030852 => 
  array (
    'id' => 16096277,
    'name' => 'ООО  "АВТОДОРСТРОЙСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502170821,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31141159,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31141159',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3025030852',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Астраханская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31141159,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31141159',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16096277',
        'method' => 'get',
      ),
    ),
  ),
  2463101680 => 
  array (
    'id' => 16096387,
    'name' => 'ООО "ТЕРРИТОРИАЛЬНЫЙ ЦЕНТР "ЭВЕНКИЯГЕОМОНИТОРИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1502171665,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31141305,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31141305',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463101680',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31141305,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31141305',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16096387',
        'method' => 'get',
      ),
    ),
  ),
  275904794 => 
  array (
    'id' => 16096423,
    'name' => 'ООО "ОРТОСТАН.РУ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502171880,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31141345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31141345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '275904794',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31141345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31141345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16096423',
        'method' => 'get',
      ),
    ),
  ),
  7715941094 => 
  array (
    'id' => 16097169,
    'name' => 'ООО "ПАРТНЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502176486,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31142339,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31142339',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715941094',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31142339,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31142339',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16097169',
        'method' => 'get',
      ),
    ),
  ),
  6163145726 => 
  array (
    'id' => 16101245,
    'name' => 'ООО "ЮГСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502193324,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31147363,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31147363',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6163145726',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31147363,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31147363',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16101245',
        'method' => 'get',
      ),
    ),
  ),
  2450027415 => 
  array (
    'id' => 16105503,
    'name' => 'ООО "ТехСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502251798,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31152117,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152117',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2450027415',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31152117,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152117',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16105503',
        'method' => 'get',
      ),
    ),
  ),
  7719465583 => 
  array (
    'id' => 16105589,
    'name' => 'ООО "НПП ЛАБТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502253427,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31152213,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152213',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719465583',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31152213,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152213',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16105589',
        'method' => 'get',
      ),
    ),
  ),
  6316208831 => 
  array (
    'id' => 16105789,
    'name' => 'ООО  "АБСОЛЮТСПЕЦСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502256176,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31152463,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152463',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316208831',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31152463,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152463',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16105789',
        'method' => 'get',
      ),
    ),
  ),
  5918213724 => 
  array (
    'id' => 16105861,
    'name' => 'ООО "ДВИЖЕНИЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502257143,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31152555,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152555',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5918213724',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31152555,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152555',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16105861',
        'method' => 'get',
      ),
    ),
  ),
  1659174829 => 
  array (
    'id' => 16106331,
    'name' => 'ООО "ТОРГОВАЯ КОМПАНИЯ "РУТЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1502260760,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31153099,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31153099',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659174829',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31153099,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31153099',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16106331',
        'method' => 'get',
      ),
    ),
  ),
  2310137337 => 
  array (
    'id' => 16107031,
    'name' => 'ООО "Прогрессор"',
    'responsible_user_id' => 1338850,
    'created_by' => 609492,
    'created_at' => 1502263763,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31153807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31153807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2310137337',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31153807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31153807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16107031',
        'method' => 'get',
      ),
    ),
  ),
  5009073637 => 
  array (
    'id' => 16107575,
    'name' => 'ООО "НПО "ДЕКАНТЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502266779,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31154623,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31154623',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5009073637',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31154623,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31154623',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16107575',
        'method' => 'get',
      ),
    ),
  ),
  3123150837 => 
  array (
    'id' => 16109083,
    'name' => 'ООО "Пожарный сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1502273889,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31156719,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31156719',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123150837',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31156719,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31156719',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16109083',
        'method' => 'get',
      ),
    ),
  ),
  7733841504 => 
  array (
    'id' => 16109763,
    'name' => 'ООО "КАНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502277462,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31157689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31157689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733841504',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31157689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31157689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16109763',
        'method' => 'get',
      ),
    ),
  ),
  7328079822 => 
  array (
    'id' => 16110055,
    'name' => 'ООО "Симбирск-Строй-Консалт"',
    'responsible_user_id' => 1338850,
    'created_by' => 609492,
    'created_at' => 1502278847,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31158187,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31158187',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7328079822',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31158187,
        1 => 33366399,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31158187,33366399',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16110055',
        'method' => 'get',
      ),
    ),
  ),
  2309102160 => 
  array (
    'id' => 16111551,
    'name' => 'Альфа-Безопасность, ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502283742,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31160445,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31160445',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2309102160',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31160445,
        1 => 31169555,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31160445,31169555',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16111551',
        'method' => 'get',
      ),
    ),
  ),
  2724171205 => 
  array (
    'id' => 16114095,
    'name' => 'ООО "ДальИнвестРесурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502336521,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31163647,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31163647',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2724171205',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Приморский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31163647,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31163647',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16114095',
        'method' => 'get',
      ),
    ),
  ),
  6316176410 => 
  array (
    'id' => 16114273,
    'name' => 'ООО  "РегионБизнесСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502339914,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31163893,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31163893',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316176410',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31163893,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31163893',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16114273',
        'method' => 'get',
      ),
    ),
  ),
  2114000230 => 
  array (
    'id' => 16114545,
    'name' => 'ЗАО коллективная строительная организация "Урмарская"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1502343024,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31164211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2114000230',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31164211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16114545',
        'method' => 'get',
      ),
    ),
  ),
  3108000241 => 
  array (
    'id' => 16114677,
    'name' => 'ООО "Стройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502344429,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31164361,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164361',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3108000241',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Белгородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31164361,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164361',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16114677',
        'method' => 'get',
      ),
    ),
  ),
  9909322467 => 
  array (
    'id' => 16114781,
    'name' => 'АК "И-Глобалэдж Корпорейшн"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502345132,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31164439,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164439',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9909322467',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31164439,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164439',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16114781',
        'method' => 'get',
      ),
    ),
  ),
  9204004095 => 
  array (
    'id' => 16114921,
    'name' => 'ООО «Центр разработки»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502346062,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31164625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9204004095',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31164625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31164625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16114921',
        'method' => 'get',
      ),
    ),
  ),
  332604456301 => 
  array (
    'id' => 16115183,
    'name' => 'Тумасян Ашот Карлосович ИП',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502347323,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31165047,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31165047',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '332604456301',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31165047,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31165047',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16115183',
        'method' => 'get',
      ),
    ),
  ),
  5032222260 => 
  array (
    'id' => 16115607,
    'name' => 'ООО "ГОРОДСКОЕ БЛАГОУСТРОЙСТВО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502349571,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31165611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31165611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032222260',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31165611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31165611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16115607',
        'method' => 'get',
      ),
    ),
  ),
  6025038335 => 
  array (
    'id' => 16117533,
    'name' => 'ООО "ВЗМК 60"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502357672,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31167915,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31167915',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6025038335',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31167915,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31167915',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16117533',
        'method' => 'get',
      ),
    ),
  ),
  7842018844 => 
  array (
    'id' => 16119077,
    'name' => 'ООО "ЛЕВША ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502364251,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31170019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31170019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842018844',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31170019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31170019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16119077',
        'method' => 'get',
      ),
    ),
  ),
  1018004738 => 
  array (
    'id' => 16123643,
    'name' => 'ООО "РиК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1502430275,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31176295,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31176295',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1018004738',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31176295,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31176295',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16123643',
        'method' => 'get',
      ),
    ),
  ),
  2014039192 => 
  array (
    'id' => 16124095,
    'name' => 'ООО «ПГС-85»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1502433930,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31176809,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31176809',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2014039192',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31176809,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31176809',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16124095',
        'method' => 'get',
      ),
    ),
  ),
  741207548760 => 
  array (
    'id' => 16138247,
    'name' => 'ИП  Александрина Анна Николаевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502683836,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31195741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31195741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '741207548760',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31195741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31195741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16138247',
        'method' => 'get',
      ),
    ),
  ),
  7325117974 => 
  array (
    'id' => 16141387,
    'name' => 'ООО  "Симбирская энергосервисная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502702670,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31200715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31200715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1515487643,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7325117974',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31200715,
        1 => 31212875,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31200715,31212875',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16141387',
        'method' => 'get',
      ),
    ),
  ),
  9715277423 => 
  array (
    'id' => 16142041,
    'name' => 'ООО "ФАКЕЛ-СПЕЦОДЕЖДА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502705004,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31201411,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31201411',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715277423',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31201411,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31201411',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16142041',
        'method' => 'get',
      ),
    ),
  ),
  7721283100 => 
  array (
    'id' => 16143625,
    'name' => 'ООО "КСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502712690,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31203381,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31203381',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721283100',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31203381,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31203381',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16143625',
        'method' => 'get',
      ),
    ),
  ),
  7449129411 => 
  array (
    'id' => 16147235,
    'name' => 'ООО СК "АС-ЧЕЛСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502770939,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31207211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31207211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7449129411',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31207211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31207211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16147235',
        'method' => 'get',
      ),
    ),
  ),
  6613010376 => 
  array (
    'id' => 16147299,
    'name' => 'ООО "ТЕРМОКОМФОРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502771903,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31208875,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31208875',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6613010376',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31208875,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31208875',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16147299',
        'method' => 'get',
      ),
    ),
  ),
  7204204750 => 
  array (
    'id' => 16147467,
    'name' => 'ООО "СтикРолл"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502774174,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31209063,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209063',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7204204750',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31209063,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209063',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16147467',
        'method' => 'get',
      ),
    ),
  ),
  6658472557 => 
  array (
    'id' => 16147505,
    'name' => 'ООО "ПС АЛЬЯНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502774724,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31209105,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209105',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658472557',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31209105,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209105',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16147505',
        'method' => 'get',
      ),
    ),
  ),
  7203372670 => 
  array (
    'id' => 16147609,
    'name' => 'ООО "Красноселькупская топливная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502775795,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31209177,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209177',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203372670',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31209177,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209177',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16147609',
        'method' => 'get',
      ),
    ),
  ),
  2463245379 => 
  array (
    'id' => 16147623,
    'name' => 'ООО СК "Астрон"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502775963,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31209197,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209197',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463245379',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31209197,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209197',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16147623',
        'method' => 'get',
      ),
    ),
  ),
  7718987379 => 
  array (
    'id' => 16148163,
    'name' => 'ООО "КТБ строительство"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502779582,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31209849,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209849',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718987379',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31209849,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31209849',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16148163',
        'method' => 'get',
      ),
    ),
  ),
  7720370287 => 
  array (
    'id' => 16148935,
    'name' => 'ООО «АЛЬЯНС ГРУПП»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502783864,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31210965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31210965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720370287',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31210965,
        1 => 31238253,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31210965,31238253',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16148935',
        'method' => 'get',
      ),
    ),
  ),
  2317039762 => 
  array (
    'id' => 16150589,
    'name' => 'ООО "Газстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502790454,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31213347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31213347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2317039762',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31213347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31213347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16150589',
        'method' => 'get',
      ),
    ),
  ),
  7727645452 => 
  array (
    'id' => 16151907,
    'name' => 'ООО "АЛЬЯНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502795104,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31215175,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31215175',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727645452',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31215175,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31215175',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16151907',
        'method' => 'get',
      ),
    ),
  ),
  7709907169 => 
  array (
    'id' => 16152093,
    'name' => 'ООО «ЛАЙФИТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502796041,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31215415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31215415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709907169',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31215415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31215415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16152093',
        'method' => 'get',
      ),
    ),
  ),
  7810654205 => 
  array (
    'id' => 16153355,
    'name' => 'ООО "Альфатехнолоджи"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502801767,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31216881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31216881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810654205',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31216881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31216881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16153355',
        'method' => 'get',
      ),
    ),
  ),
  403004590 => 
  array (
    'id' => 16156873,
    'name' => 'ООО "Канстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502857618,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31220069,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220069',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '403004590',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтай(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31220069,
        1 => 31231563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220069,31231563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16156873',
        'method' => 'get',
      ),
    ),
  ),
  246207872828 => 
  array (
    'id' => 16157093,
    'name' => 'ИП Федорова Анастасия',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502860209,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31220281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '246207872828',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31220281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16157093',
        'method' => 'get',
      ),
    ),
  ),
  5402534724 => 
  array (
    'id' => 16157323,
    'name' => 'ООО  "Лимекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502862754,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31220531,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220531',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402534724',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31220531,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220531',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16157323',
        'method' => 'get',
      ),
    ),
  ),
  6314022158 => 
  array (
    'id' => 16157345,
    'name' => 'ООО "Волгопромлизинг"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502863008,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31220557,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220557',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921246,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6314022158',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31220557,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220557',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16157345',
        'method' => 'get',
      ),
    ),
  ),
  7810356551 => 
  array (
    'id' => 16157539,
    'name' => 'ООО «Лесофф»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502864518,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31220781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810356551',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31220781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16157539',
        'method' => 'get',
      ),
    ),
  ),
  7731535639 => 
  array (
    'id' => 16161431,
    'name' => 'ООО "Агенство развития "Союзагромаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502880623,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31225499,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31225499',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731535639',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31225499,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31225499',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16161431',
        'method' => 'get',
      ),
    ),
  ),
  7734253287 => 
  array (
    'id' => 16161669,
    'name' => 'ООО "Проектно-монтажный центр"Спецавтоматика"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502881565,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31225753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31225753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734253287',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31225753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31225753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16161669',
        'method' => 'get',
      ),
    ),
  ),
  5028032354 => 
  array (
    'id' => 16162061,
    'name' => 'ООО «КАПИТАЛСТРОЙ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502883079,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31226157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31226157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5028032354',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31226157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31226157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16162061',
        'method' => 'get',
      ),
    ),
  ),
  7719412253 => 
  array (
    'id' => 16162297,
    'name' => 'ООО "КЕМПАРТНЕРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502884267,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31226401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31226401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719412253',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31226401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31226401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16162297',
        'method' => 'get',
      ),
    ),
  ),
  7734739366 => 
  array (
    'id' => 16166501,
    'name' => 'ООО "ПИЛАДА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502945593,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31230975,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31230975',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734739366',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Сахалинская область(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31230975,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31230975',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16166501',
        'method' => 'get',
      ),
    ),
  ),
  3526013791 => 
  array (
    'id' => 16166535,
    'name' => 'ООО "СГС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502946201,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31231013,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231013',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3526013791',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Коми(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31231013,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231013',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16166535',
        'method' => 'get',
      ),
    ),
  ),
  4824092102 => 
  array (
    'id' => 16166589,
    'name' => 'ООО "Милена"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502947099,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31231087,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231087',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4824092102',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Липецкая область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31231087,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231087',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16166589',
        'method' => 'get',
      ),
    ),
  ),
  6330073463 => 
  array (
    'id' => 16166597,
    'name' => 'ООО "ТРАСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502947215,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31231103,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231103',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6330073463',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31231103,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231103',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16166597',
        'method' => 'get',
      ),
    ),
  ),
  550204150296 => 
  array (
    'id' => 16166747,
    'name' => 'ИП Щитов Игорь Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502948877,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31231289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '550204150296',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Омская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31231289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31231289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16166747',
        'method' => 'get',
      ),
    ),
  ),
  3665100226 => 
  array (
    'id' => 16167261,
    'name' => 'ООО "СТРОЙАЛЬЯНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502953096,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31232737,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31232737',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3665100226',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31232737,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31232737',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16167261',
        'method' => 'get',
      ),
    ),
  ),
  1167746268315 => 
  array (
    'id' => 16167661,
    'name' => 'ЭТАЛОНДОРСТРОЙ, ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502955067,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31220069,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220069',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1167746268315',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31220069,
        1 => 31242991,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31220069,31242991',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16167661',
        'method' => 'get',
      ),
    ),
  ),
  9201515599 => 
  array (
    'id' => 16169019,
    'name' => 'ООО  "ЧЕТЫРЕ ОКЕАНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502961283,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31234693,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31234693',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9201515599',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31234693,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31234693',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16169019',
        'method' => 'get',
      ),
    ),
  ),
  7729765201 => 
  array (
    'id' => 16171299,
    'name' => 'ООО "АЛЬФА-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1502969892,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31237563,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31237563',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729765201',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31237563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31237563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16171299',
        'method' => 'get',
      ),
    ),
  ),
  7451324096 => 
  array (
    'id' => 16175417,
    'name' => 'ООО СК "Ураллеском"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503027919,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31242543,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31242543',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451324096',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31242543,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31242543',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16175417',
        'method' => 'get',
      ),
    ),
  ),
  5402004484 => 
  array (
    'id' => 16175443,
    'name' => 'ООО "ТОП ТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503028892,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31242579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31242579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402004484',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31242579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31242579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16175443',
        'method' => 'get',
      ),
    ),
  ),
  7840444438 => 
  array (
    'id' => 16175753,
    'name' => '«Северо-Западная Инжиниринговая компания»ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503034994,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31243125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31243125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840444438',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ленинградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31243125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31243125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16175753',
        'method' => 'get',
      ),
    ),
  ),
  5044107790 => 
  array (
    'id' => 16177081,
    'name' => 'ООО "АВТОДОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503043327,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31244939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31244939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5044107790',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31244939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31244939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16177081',
        'method' => 'get',
      ),
    ),
  ),
  5262084279 => 
  array (
    'id' => 16177675,
    'name' => 'АО "ТРАНССЕТЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503045727,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31246221,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31246221',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5262084279',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31246221,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31246221',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16177675',
        'method' => 'get',
      ),
    ),
  ),
  6671159209 => 
  array (
    'id' => 16178527,
    'name' => 'ООО "ОЛДЭНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503049244,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31247245,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31247245',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671159209',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пензенская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31247245,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31247245',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16178527',
        'method' => 'get',
      ),
    ),
  ),
  7743113590 => 
  array (
    'id' => 16179513,
    'name' => 'ООО "ЭЛЕОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503055153,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31248679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31248679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743113590',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31248679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31248679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16179513',
        'method' => 'get',
      ),
    ),
  ),
  7810253370 => 
  array (
    'id' => 16190549,
    'name' => 'АКЦИОНЕРНОЕ ОБЩЕСТВО "ТЕХНОРОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503298234,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31260763,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31260763',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810253370',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31260763,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31260763',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16190549',
        'method' => 'get',
      ),
    ),
  ),
  7813518480 => 
  array (
    'id' => 16190979,
    'name' => 'ООО «ИНФОСФЕРА»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503300422,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31261275,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31261275',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813518480',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31261275,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31261275',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16190979',
        'method' => 'get',
      ),
    ),
  ),
  615428854739 => 
  array (
    'id' => 16191523,
    'name' => 'ИП  Калиниченко Екатерина Игоревна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503302842,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31261965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31261965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921247,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '615428854739',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31261965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31261965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16191523',
        'method' => 'get',
      ),
    ),
  ),
  6914013571 => 
  array (
    'id' => 16191713,
    'name' => 'АО "Ржевское Дорожное Ремонтно-Строительное Управление"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503303696,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31262203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31262203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6914013571',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тверская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31262203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31262203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16191713',
        'method' => 'get',
      ),
    ),
  ),
  365200266540 => 
  array (
    'id' => 16192441,
    'name' => 'ИП Пойманов Виталий Иванович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503306796,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31263123,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31263123',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '365200266540',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31263123,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31263123',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16192441',
        'method' => 'get',
      ),
    ),
  ),
  6920008731 => 
  array (
    'id' => 16193075,
    'name' => 'ООО «Вышневолоцкое ДРСУ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503309169,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31263783,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31263783',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6920008731',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тверская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31263783,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31263783',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16193075',
        'method' => 'get',
      ),
    ),
  ),
  5047160471 => 
  array (
    'id' => 16193637,
    'name' => 'ООО "АВИАТИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503311664,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31264545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31264545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047160471',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31264545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31264545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16193637',
        'method' => 'get',
      ),
    ),
  ),
  7743606651 => 
  array (
    'id' => 16193995,
    'name' => 'ООО "Арт-Ирис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503313559,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31265039,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31265039',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743606651',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31265039,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31265039',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16193995',
        'method' => 'get',
      ),
    ),
  ),
  7814536178 => 
  array (
    'id' => 16195355,
    'name' => 'ООО  "НИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503318254,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31266619,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31266619',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814536178',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31266619,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31266619',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16195355',
        'method' => 'get',
      ),
    ),
  ),
  3810327842 => 
  array (
    'id' => 16195947,
    'name' => 'ООО «Ангарская швейная фабрика»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503321291,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31267411,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31267411',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3810327842',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31267411,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31267411',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16195947',
        'method' => 'get',
      ),
    ),
  ),
  2204054858 => 
  array (
    'id' => 16198635,
    'name' => 'ООО  "АлтайМедПоставка"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503375614,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31270663,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31270663',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2204054858',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31270663,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31270663',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16198635',
        'method' => 'get',
      ),
    ),
  ),
  3801133128 => 
  array (
    'id' => 16198659,
    'name' => 'ООО "ЭЛМОНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503376047,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31270685,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31270685',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3801133128',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31270685,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31270685',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16198659',
        'method' => 'get',
      ),
    ),
  ),
  5035019996 => 
  array (
    'id' => 16199389,
    'name' => 'ООО "Промжилстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503384291,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31271717,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31271717',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5035019996',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31271717,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31271717',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16199389',
        'method' => 'get',
      ),
    ),
  ),
  5018183731 => 
  array (
    'id' => 16201427,
    'name' => 'ООО "ЦЕНТРСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503393750,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31275057,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31275057',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5018183731',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31275057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31275057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16201427',
        'method' => 'get',
      ),
    ),
  ),
  9204015570 => 
  array (
    'id' => 16201657,
    'name' => 'ООО "ПКФ "СВЕТОЗАР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503394767,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31275359,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31275359',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9204015570',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31275359,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31275359',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16201657',
        'method' => 'get',
      ),
    ),
  ),
  9110019748 => 
  array (
    'id' => 16204449,
    'name' => 'ООО «ЭКОГРАД +»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503406686,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31278733,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31278733',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9110019748',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31278733,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31278733',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16204449',
        'method' => 'get',
      ),
    ),
  ),
  4715015637 => 
  array (
    'id' => 16209151,
    'name' => 'ООО "СтройСтандарт"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1503472484,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31283805,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31283805',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4715015637',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31283805,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31283805',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16209151',
        'method' => 'get',
      ),
    ),
  ),
  7715970320 => 
  array (
    'id' => 16211085,
    'name' => 'ООО  "ВЕКТОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503480546,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31286203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31286203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715970320',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31286203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31286203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16211085',
        'method' => 'get',
      ),
    ),
  ),
  507800048167 => 
  array (
    'id' => 16211473,
    'name' => 'Мукозобов Валерий Витальевич ИП',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503482321,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31286683,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31286683',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '507800048167',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31286683,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31286683',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16211473',
        'method' => 'get',
      ),
    ),
  ),
  5257166261 => 
  array (
    'id' => 16211777,
    'name' => 'ООО "АБВ-СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503483626,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31287067,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287067',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5257166261',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31287067,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287067',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16211777',
        'method' => 'get',
      ),
    ),
  ),
  3662168794 => 
  array (
    'id' => 16211915,
    'name' => 'ООО ""Строй Инвест""',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503484402,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31287221,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287221',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662168794',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31287221,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287221',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16211915',
        'method' => 'get',
      ),
    ),
  ),
  402000016630 => 
  array (
    'id' => 16212055,
    'name' => 'ИП Сарычева Татьяна Семеновна',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1503485128,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31287419,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287419',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '402000016630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31287419,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287419',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16212055',
        'method' => 'get',
      ),
    ),
  ),
  5504117769 => 
  array (
    'id' => 16212481,
    'name' => 'ООО "ТРЕСТСТРОЙ-2000"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503487407,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31287979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5504117769',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31287979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31287979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16212481',
        'method' => 'get',
      ),
    ),
  ),
  7708791641 => 
  array (
    'id' => 16213605,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ КОМПАНИЯ "РЕСТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503492789,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31289525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31289525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708791641',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Владимирская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31289525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31289525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16213605',
        'method' => 'get',
      ),
    ),
  ),
  '' => 
  array (
    'id' => 16213691,
    'name' => 'ООО  «Юг-Север-Строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503493257,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31289621,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31289621',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31289621,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31289621',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16213691',
        'method' => 'get',
      ),
    ),
  ),
  5536004486 => 
  array (
    'id' => 16216337,
    'name' => 'ГП Тевризское ДРСУ',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503547801,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31292835,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31292835',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5536004486',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Омская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31292835,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31292835',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16216337',
        'method' => 'get',
      ),
    ),
  ),
  8602196404 => 
  array (
    'id' => 16216369,
    'name' => 'ООО «ИННОВАЦИОННЫЕ ТЕХНОЛОГИИ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503548449,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31292871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31292871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602196404',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31292871,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31292871',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16216369',
        'method' => 'get',
      ),
    ),
  ),
  5406782132 => 
  array (
    'id' => 16216381,
    'name' => 'ООО "НСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503548770,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31292889,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31292889',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406782132',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31292889,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31292889',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16216381',
        'method' => 'get',
      ),
    ),
  ),
  5025029191 => 
  array (
    'id' => 16216793,
    'name' => 'ООО «ЛОБЕЛЕК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503553806,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31296999,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31296999',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5025029191',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31296999,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31296999',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16216793',
        'method' => 'get',
      ),
    ),
  ),
  7724382029 => 
  array (
    'id' => 16217033,
    'name' => '"АСТ-КЛИНИНГ"ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503555925,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31297307,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297307',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921243,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724382029',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31297307,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297307',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16217033',
        'method' => 'get',
      ),
    ),
  ),
  7725265134 => 
  array (
    'id' => 16217049,
    'name' => 'ООО "Астерион"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503556110,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31297327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725265134',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31297327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16217049',
        'method' => 'get',
      ),
    ),
  ),
  7717158347 => 
  array (
    'id' => 16217165,
    'name' => 'ООО «Альянс-Универсал»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503556829,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31297479,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297479',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717158347',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31297479,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297479',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16217165',
        'method' => 'get',
      ),
    ),
  ),
  5836680767 => 
  array (
    'id' => 16217267,
    'name' => 'ООО "Феникс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503557536,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31297611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5836680767',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пензенская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31297611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31297611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16217267',
        'method' => 'get',
      ),
    ),
  ),
  5074026066 => 
  array (
    'id' => 16217757,
    'name' => 'ООО  "ПРОЕКТНАЯ КОНТОРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503560173,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31298421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31298421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5074026066',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31298421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31298421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16217757',
        'method' => 'get',
      ),
    ),
  ),
  5032288060 => 
  array (
    'id' => 16218673,
    'name' => 'ООО "БАРВИХА ЛАНДШАФТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503564322,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31299475,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31299475',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032288060',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31299475,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31299475',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16218673',
        'method' => 'get',
      ),
    ),
  ),
  6311130645 => 
  array (
    'id' => 16219401,
    'name' => 'ООО "Компрессорная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503567698,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31300369,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31300369',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6311130645',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31300369,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31300369',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16219401',
        'method' => 'get',
      ),
    ),
  ),
  'lift@liftf.ru' => 
  array (
    'id' => 16219677,
    'name' => 'ЗАО  "ЛифтМаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503569099,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31300751,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31300751',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'lift@liftf.ru',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31300751,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31300751',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16219677',
        'method' => 'get',
      ),
    ),
  ),
  9102003014 => 
  array (
    'id' => 16220161,
    'name' => 'ООО "КРЫМСПЕЦСНАБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503571868,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31301377,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31301377',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102003014',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31301377,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31301377',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16220161',
        'method' => 'get',
      ),
    ),
  ),
  770174544010 => 
  array (
    'id' => 16221179,
    'name' => 'ИП Кашу Наталья',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503576923,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31302611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31302611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '770174544010',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31302611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31302611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16221179',
        'method' => 'get',
      ),
    ),
  ),
  5007093652 => 
  array (
    'id' => 16221465,
    'name' => 'ООО "Триатекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503578201,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31302887,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31302887',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5007093652',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31302887,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31302887',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16221465',
        'method' => 'get',
      ),
    ),
  ),
  7203360185 => 
  array (
    'id' => 16224483,
    'name' => 'ООО «Автокомплекс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503634248,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31306185,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306185',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203360185',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тюменская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31306185,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306185',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16224483',
        'method' => 'get',
      ),
    ),
  ),
  4823030135 => 
  array (
    'id' => 16224501,
    'name' => 'ООО "Липецкий станкозавод "Возрождение"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503634634,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31306203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4823030135',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31306203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16224501',
        'method' => 'get',
      ),
    ),
  ),
  1435131360 => 
  array (
    'id' => 16224527,
    'name' => 'ООО  "Адгезия"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503635029,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31306233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435131360',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31306233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16224527',
        'method' => 'get',
      ),
    ),
  ),
  7723543210 => 
  array (
    'id' => 16224743,
    'name' => 'ООО "Лыжный Экипировочный Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503638590,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31306519,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306519',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723543210',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Карелия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31306519,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306519',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16224743',
        'method' => 'get',
      ),
    ),
  ),
  3435125113 => 
  array (
    'id' => 16225181,
    'name' => 'ООО «ВЫСОТА АЛЬП-СТРОЙ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1503643131,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31307173,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307173',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435125113',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31307173,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307173',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16225181',
        'method' => 'get',
      ),
    ),
  ),
  7702379840 => 
  array (
    'id' => 16225301,
    'name' => 'ООО «НОТИК.РУ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503644081,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31307347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702379840',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31307347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16225301',
        'method' => 'get',
      ),
    ),
  ),
  690401534003 => 
  array (
    'id' => 16225625,
    'name' => 'ИП Каратуева Людмила Владимировна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503646355,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31307779,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307779',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '690401534003',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тверская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31307779,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307779',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16225625',
        'method' => 'get',
      ),
    ),
  ),
  3402009249 => 
  array (
    'id' => 16225683,
    'name' => 'ООО "Газстрой- Быково"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503646737,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31307845,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307845',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3402009249',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Волгоградская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31307845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31307845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16225683',
        'method' => 'get',
      ),
    ),
  ),
  7726663106 => 
  array (
    'id' => 16226011,
    'name' => 'ООО «ДИНАР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503647920,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31308109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31308109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726663106',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31308109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31308109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16226011',
        'method' => 'get',
      ),
    ),
  ),
  6234027802 => 
  array (
    'id' => 16226285,
    'name' => 'ОО "АКВИЛОН-Р""',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503649244,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31309395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31309395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6234027802',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31309395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31309395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16226285',
        'method' => 'get',
      ),
    ),
  ),
  6670435590 => 
  array (
    'id' => 16228029,
    'name' => 'ООО "СК "Бульдозер"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503657710,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31311587,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31311587',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670435590',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31311587,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31311587',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16228029',
        'method' => 'get',
      ),
    ),
  ),
  2130005067 => 
  array (
    'id' => 16239529,
    'name' => 'ООО "Аридаль"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1503899385,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31323381,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31323381',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130005067',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31323381,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31323381',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16239529',
        'method' => 'get',
      ),
    ),
  ),
  7840492819 => 
  array (
    'id' => 16249207,
    'name' => 'ООО "Сатэл"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503985240,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31335953,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31335953',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840492819',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31335953,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31335953',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16249207',
        'method' => 'get',
      ),
    ),
  ),
  7842494385 => 
  array (
    'id' => 16249267,
    'name' => 'ООО "Мобильные Технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503985988,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31336017,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31336017',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842494385',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31336017,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31336017',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16249267',
        'method' => 'get',
      ),
    ),
  ),
  6829091032 => 
  array (
    'id' => 16249685,
    'name' => 'ООО «ТамбовИнвестСтрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1503988813,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31336621,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31336621',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829091032',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31336621,
        1 => 31337491,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31336621,31337491',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16249685',
        'method' => 'get',
      ),
    ),
  ),
  7714912693 => 
  array (
    'id' => 16249865,
    'name' => 'ООО  "ПИР ЭКСПО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503989795,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31336857,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31336857',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714912693',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31336857,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31336857',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16249865',
        'method' => 'get',
      ),
    ),
  ),
  7706746555 => 
  array (
    'id' => 16250157,
    'name' => 'ООО «Центр новых технологий «Импульс»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1503991720,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31337267,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31337267',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706746555',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31337267,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31337267',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16250157',
        'method' => 'get',
      ),
    ),
  ),
  511090425952 => 
  array (
    'id' => 16250679,
    'name' => 'ИП Погосян Ашот Рубенович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503994138,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31337917,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31337917',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '511090425952',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Мурманская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31337917,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31337917',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16250679',
        'method' => 'get',
      ),
    ),
  ),
  7813575872 => 
  array (
    'id' => 16251025,
    'name' => 'ООО "ИЛЛЮЗИОН - ПРО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503995791,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31338375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31338375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813575872',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31338375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31338375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16251025',
        'method' => 'get',
      ),
    ),
  ),
  9201002798 => 
  array (
    'id' => 16251235,
    'name' => 'ООО "ГКМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503996610,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31338635,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31338635',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9201002798',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31338635,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31338635',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16251235',
        'method' => 'get',
      ),
    ),
  ),
  7704252960 => 
  array (
    'id' => 16251257,
    'name' => 'АО "Вертолетная сервисная компания"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503996733,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31338679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31338679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704252960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31338679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31338679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16251257',
        'method' => 'get',
      ),
    ),
  ),
  7736653668 => 
  array (
    'id' => 16253681,
    'name' => 'ООО "Мастор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504007283,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31341551,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31341551',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736653668',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31341551,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31341551',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16253681',
        'method' => 'get',
      ),
    ),
  ),
  6731066336 => 
  array (
    'id' => 16260677,
    'name' => 'ООО "Ростехкомплект"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1504077926,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31349945,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31349945',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921245,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6731066336',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31349945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31349945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16260677',
        'method' => 'get',
      ),
    ),
  ),
  7718194010 => 
  array (
    'id' => 16260901,
    'name' => 'ООО "Издательский дом "Юриспруденция"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504079010,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31350279,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31350279',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718194010',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31350279,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31350279',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16260901',
        'method' => 'get',
      ),
    ),
  ),
  1001264706 => 
  array (
    'id' => 16268587,
    'name' => 'ООО "КАРЕЛСТРОЙРЕСУРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1504157202,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31359719,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31359719',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921238,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1001264706',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31359719,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31359719',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16268587',
        'method' => 'get',
      ),
    ),
  ),
  3662144634 => 
  array (
    'id' => 16269087,
    'name' => 'ООО  ""Новые технологии и системы""',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1504161895,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31360529,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31360529',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921238,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662144634',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31360529,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31360529',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16269087',
        'method' => 'get',
      ),
    ),
  ),
  9715009270 => 
  array (
    'id' => 16271571,
    'name' => 'ООО "АКСИОМА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504173332,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31364273,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31364273',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921238,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715009270',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31364273,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31364273',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16271571',
        'method' => 'get',
      ),
    ),
  ),
  7743540665 => 
  array (
    'id' => 16272813,
    'name' => 'ООО «ЭКОРЕМСПАС+»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504179552,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31366111,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31366111',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743540665',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31366111,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31366111',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16272813',
        'method' => 'get',
      ),
    ),
  ),
  7810865044 => 
  array (
    'id' => 16273769,
    'name' => 'ООО "ТД Молочный"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504184356,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31367269,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31367269',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810865044',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31367269,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31367269',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16273769',
        'method' => 'get',
      ),
    ),
  ),
  30901007258 => 
  array (
    'id' => 16276751,
    'name' => 'ИП  Коновалов Михаил Валентинович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504237530,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31370137,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31370137',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '30901007258',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Бурятия(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31370137,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31370137',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16276751',
        'method' => 'get',
      ),
    ),
  ),
  2462201509 => 
  array (
    'id' => 16277251,
    'name' => 'ООО "ИнКомСет"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1504247173,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31371061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31371061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2462201509',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31371061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31371061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16277251',
        'method' => 'get',
      ),
    ),
  ),
  2466253674 => 
  array (
    'id' => 16291917,
    'name' => 'АО "КАГП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504500882,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31386983,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31386983',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466253674',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31386983,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31386983',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16291917',
        'method' => 'get',
      ),
    ),
  ),
  5190027920 => 
  array (
    'id' => 16295899,
    'name' => 'ООО "АНИМАКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1504520076,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31392219,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31392219',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1505193569,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190027920',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31392219,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31392219',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16295899',
        'method' => 'get',
      ),
    ),
  ),
  6501281988 => 
  array (
    'id' => 16301833,
    'name' => 'ООО СК "Альянс-Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504583925,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31398321,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31398321',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501281988',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Сахалинская область(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31398321,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31398321',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16301833',
        'method' => 'get',
      ),
    ),
  ),
  3003006341 => 
  array (
    'id' => 16302075,
    'name' => 'ООО «ЮГПЛАСТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504588465,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31398663,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31398663',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3003006341',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Астраханская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31398663,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31398663',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16302075',
        'method' => 'get',
      ),
    ),
  ),
  7710591270 => 
  array (
    'id' => 16314061,
    'name' => 'ООО "СофтЭксперт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504679246,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31412607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31412607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710591270',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31412607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31412607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16314061',
        'method' => 'get',
      ),
    ),
  ),
  2308087914 => 
  array (
    'id' => 16318493,
    'name' => 'ООО МТУ "ЮКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504697462,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31418319,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31418319',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308087914',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31418319,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31418319',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16318493',
        'method' => 'get',
      ),
    ),
  ),
  890200015853 => 
  array (
    'id' => 16323169,
    'name' => 'ИП Карчава Исидор Григорьевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504759394,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31423459,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31423459',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '890200015853',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31423459,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31423459',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16323169',
        'method' => 'get',
      ),
    ),
  ),
  6611012931 => 
  array (
    'id' => 16323327,
    'name' => 'Кооператив "Ускорение"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504761135,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31423645,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31423645',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6611012931',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31423645,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31423645',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16323327',
        'method' => 'get',
      ),
    ),
  ),
  7702043311 => 
  array (
    'id' => 16324267,
    'name' => 'АО "Антикор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504768780,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31425137,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31425137',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702043311',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31425137,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31425137',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16324267',
        'method' => 'get',
      ),
    ),
  ),
  2311206199 => 
  array (
    'id' => 16325741,
    'name' => 'ООО "ЭМРОУЗ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504774233,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31426797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31426797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311206199',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31426797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31426797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16325741',
        'method' => 'get',
      ),
    ),
  ),
  7701187409 => 
  array (
    'id' => 16326307,
    'name' => '"СИСТЕМАТИКА"ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504776007,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31427331,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31427331',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701187409',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Севастополь(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31427331,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31427331',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16326307',
        'method' => 'get',
      ),
    ),
  ),
  1837004362 => 
  array (
    'id' => 16326885,
    'name' => 'ООО  "Энергоремонт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504778286,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31428123,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31428123',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1837004362',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Удмуртия(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31428123,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31428123',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16326885',
        'method' => 'get',
      ),
    ),
  ),
  2308035264 => 
  array (
    'id' => 16333979,
    'name' => 'ООО "Химзащита"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504856342,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31436537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31436537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308035264',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31436537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31436537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16333979',
        'method' => 'get',
      ),
    ),
  ),
  9204558128 => 
  array (
    'id' => 16337351,
    'name' => 'ООО  "Лаборатория кибербезопасности"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1504871188,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31440677,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31440677',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9204558128',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Севастополь(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31440677,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31440677',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16337351',
        'method' => 'get',
      ),
    ),
  ),
  7805687967 => 
  array (
    'id' => 16348541,
    'name' => 'ООО "ВЫМПЕЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505101915,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31452145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31452145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805687967',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31452145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31452145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16348541',
        'method' => 'get',
      ),
    ),
  ),
  3849003350 => 
  array (
    'id' => 16348575,
    'name' => 'ООО "Компания "Аква-Люкс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505102877,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31452203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31452203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921239,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3849003350',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31452203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31452203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16348575',
        'method' => 'get',
      ),
    ),
  ),
  2460066484 => 
  array (
    'id' => 16348675,
    'name' => 'ООО "КТС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505104471,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31452315,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31452315',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460066484',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31452315,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31452315',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16348675',
        'method' => 'get',
      ),
    ),
  ),
  5025030447 => 
  array (
    'id' => 16350089,
    'name' => 'ООО  "Городские сети"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505114578,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31454183,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31454183',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5025030447',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31454183,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31454183',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16350089',
        'method' => 'get',
      ),
    ),
  ),
  2130081068 => 
  array (
    'id' => 16353379,
    'name' => 'ООО "ПРОМТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505126502,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31458007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31458007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130081068',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31458007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31458007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16353379',
        'method' => 'get',
      ),
    ),
  ),
  7727732810 => 
  array (
    'id' => 16353625,
    'name' => 'ООО «Денекс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505127703,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31458385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31458385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727732810',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31458385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31458385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16353625',
        'method' => 'get',
      ),
    ),
  ),
  7801314178 => 
  array (
    'id' => 16354485,
    'name' => 'ООО "ИНЖИНИРИНГОВАЯ КОМПАНИЯ "КРОНШТАДТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505131487,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31459443,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31459443',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801314178',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31459443,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31459443',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16354485',
        'method' => 'get',
      ),
    ),
  ),
  7717517194 => 
  array (
    'id' => 16359785,
    'name' => 'ООО  "СтройСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505198056,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31464979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31464979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717517194',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31464979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31464979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16359785',
        'method' => 'get',
      ),
    ),
  ),
  5262321900 => 
  array (
    'id' => 16363859,
    'name' => 'ООО «СТРОЙКОМ НН»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505215073,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31469863,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31469863',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5262321900',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31469863,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31469863',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16363859',
        'method' => 'get',
      ),
    ),
  ),
  5504000440 => 
  array (
    'id' => 16370315,
    'name' => 'ЗАО "Родник"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505273239,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31476535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31476535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5504000440',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31476535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31476535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16370315',
        'method' => 'get',
      ),
    ),
  ),
  5403016475 => 
  array (
    'id' => 16370375,
    'name' => 'ООО "СибСпецСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505274601,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31476607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31476607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5403016475',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31476607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31476607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16370375',
        'method' => 'get',
      ),
    ),
  ),
  6679040597 => 
  array (
    'id' => 16370455,
    'name' => 'ООО "ГорДорСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505276570,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31476713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31476713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6679040597',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31476713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31476713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16370455',
        'method' => 'get',
      ),
    ),
  ),
  5022051992 => 
  array (
    'id' => 16370785,
    'name' => 'ООО "АЛИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505281231,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31477301,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31477301',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5022051992',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31477301,
        1 => 31477901,
        2 => 31492501,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31477301,31477901,31492501',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16370785',
        'method' => 'get',
      ),
    ),
  ),
  5405230820 => 
  array (
    'id' => 16380033,
    'name' => 'ООО  «КАСКАД-Строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505361033,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31488153,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31488153',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405230820',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31488153,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31488153',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16380033',
        'method' => 'get',
      ),
    ),
  ),
  6312058173 => 
  array (
    'id' => 16380133,
    'name' => 'ООО «АТ Инженерные системы-сервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505363247,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31488273,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31488273',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312058173',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31488273,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31488273',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16380133',
        'method' => 'get',
      ),
    ),
  ),
  2224173557 => 
  array (
    'id' => 16380189,
    'name' => 'ООО "СТРОЙ-ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505363902,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31488347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31488347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224173557',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31488347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31488347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16380189',
        'method' => 'get',
      ),
    ),
  ),
  9102165960 => 
  array (
    'id' => 16385073,
    'name' => 'ООО "СПМК-32 "КРЫМЭЛЕКТРОВОДМОНТАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505388549,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31494435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31494435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102165960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31494435,
        1 => 31494441,
        2 => 31494443,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31494435,31494441,31494443',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16385073',
        'method' => 'get',
      ),
    ),
  ),
  5404051881 => 
  array (
    'id' => 16389515,
    'name' => 'ООО "ИНТЭК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505448028,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31498927,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31498927',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404051881',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31498927,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31498927',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16389515',
        'method' => 'get',
      ),
    ),
  ),
  263515128201 => 
  array (
    'id' => 16392731,
    'name' => 'ИП  Батищев Александр Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505468202,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31502715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31502715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '263515128201',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кабардино-Балкария(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31502715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31502715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16392731',
        'method' => 'get',
      ),
    ),
  ),
  9705086412 => 
  array (
    'id' => 16393087,
    'name' => 'ООО "СТРОЙ КОНТРАКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505469576,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31503007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31503007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705086412',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31503007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31503007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16393087',
        'method' => 'get',
      ),
    ),
  ),
  6318193073 => 
  array (
    'id' => 16393675,
    'name' => 'ООО ЧАО "ГАРНИЗОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505472118,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31503619,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31503619',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318193073',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31503619,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31503619',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16393675',
        'method' => 'get',
      ),
    ),
  ),
  4825097424 => 
  array (
    'id' => 16410801,
    'name' => 'ООО Клевер',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1505732964,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31521933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31521933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1506498717,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825097424',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31521933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31521933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16410801',
        'method' => 'get',
      ),
    ),
  ),
  7203403960 => 
  array (
    'id' => 16416118,
    'name' => 'ООО "МАРЕНГО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505794624,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31527580,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31527580',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203403960',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Курганская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31527580,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31527580',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16416118',
        'method' => 'get',
      ),
    ),
  ),
  2635209168 => 
  array (
    'id' => 16416485,
    'name' => 'ООО "МЕРКУРИЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505798813,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31528063,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31528063',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635209168',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ставропольский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31528063,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31528063',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16416485',
        'method' => 'get',
      ),
    ),
  ),
  323398237 => 
  array (
    'id' => 16425425,
    'name' => 'ООО "Ренмедсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1505874823,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31538415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31538415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323398237',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31538415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31538415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16425425',
        'method' => 'get',
      ),
    ),
  ),
  400002764 => 
  array (
    'id' => 16425427,
    'name' => 'ООО «МАЙМИНСКОЕ ДРСУ»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1505874953,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31538419,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31538419',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '400002764',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31538419,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31538419',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16425427',
        'method' => 'get',
      ),
    ),
  ),
  7719622814 => 
  array (
    'id' => 16425941,
    'name' => 'ООО Вариант Техно Монтаж',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505886443,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31539149,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31539149',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719622814',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31539149,
        1 => 31539151,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31539149,31539151',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16425941',
        'method' => 'get',
      ),
    ),
  ),
  9102018684 => 
  array (
    'id' => 16431591,
    'name' => 'ООО  "КРЫМ-МАГИСТРАЛЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505912480,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31546373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31546373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102018684',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Севастополь(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31546373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31546373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16431591',
        'method' => 'get',
      ),
    ),
  ),
  4312143308 => 
  array (
    'id' => 16435527,
    'name' => 'ООО  "Специальное конструкторское бюро медицинской тематики"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505972774,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31550565,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31550565',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4312143308',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31550565,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31550565',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16435527',
        'method' => 'get',
      ),
    ),
  ),
  276922027 => 
  array (
    'id' => 16436277,
    'name' => 'ООО «СКАНДА»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1505977216,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31551633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31551633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '276922027',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31551633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31551633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16436277',
        'method' => 'get',
      ),
    ),
  ),
  6679025084 => 
  array (
    'id' => 16445295,
    'name' => 'ООО "ГЕОИД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506056571,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31567157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31567157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6679025084',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31567157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31567157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16445295',
        'method' => 'get',
      ),
    ),
  ),
  5047166160 => 
  array (
    'id' => 16445833,
    'name' => 'ООО "Стройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506062122,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31568125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31568125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047166160',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31568125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31568125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16445833',
        'method' => 'get',
      ),
    ),
  ),
  7725584290 => 
  array (
    'id' => 16449909,
    'name' => 'ООО «БАМгрупп»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506082126,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31572941,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31572941',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921241,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725584290',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Нижегородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31572941,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31572941',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16449909',
        'method' => 'get',
      ),
    ),
  ),
  5902290882 => 
  array (
    'id' => 16460891,
    'name' => 'Государственное краевое учреждение "Специализированное монтажно-эксплуатационное управление Пермского края"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506315348,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31583559,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31583559',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902290882',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31583559,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31583559',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16460891',
        'method' => 'get',
      ),
    ),
  ),
  3730012847 => 
  array (
    'id' => 16461199,
    'name' => 'ООО  "ЭНЕРГОКОМПЛЕКТСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506319491,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31584033,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31584033',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3730012847',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ивановская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31584033,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31584033',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16461199',
        'method' => 'get',
      ),
    ),
  ),
  6658475565 => 
  array (
    'id' => 16471681,
    'name' => 'ООО  "ЛайнерТек"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506402613,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31632093,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31632093',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658475565',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31632093,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31632093',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16471681',
        'method' => 'get',
      ),
    ),
  ),
  2308193038 => 
  array (
    'id' => 16481607,
    'name' => 'ООО "Экоаналитическая лаборатория "СФЕРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1506493061,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31643589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31643589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308193038',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31643589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31643589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16481607',
        'method' => 'get',
      ),
    ),
  ),
  711018573 => 
  array (
    'id' => 16483433,
    'name' => 'ООО "ДЖАТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506500837,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31645633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31645633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1538053200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '711018573',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кабардино-Балкария(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31645633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31645633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16483433',
        'method' => 'get',
      ),
    ),
  ),
  771317561018 => 
  array (
    'id' => 16484433,
    'name' => 'ИП Бакшеев Владимир Владимирович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506504999,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31646839,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31646839',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '771317561018',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31646839,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31646839',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16484433',
        'method' => 'get',
      ),
    ),
  ),
  1326960551 => 
  array (
    'id' => 16490423,
    'name' => 'Саранское городское отделение Общероссийской общественной организации "Всероссийское добровольное пожарное общество"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1506577347,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31653595,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31653595',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1326960551',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31653595,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31653595',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16490423',
        'method' => 'get',
      ),
    ),
  ),
  7701758814 => 
  array (
    'id' => 16490989,
    'name' => 'ООО "Бизнес-Азимут"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506582088,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31654393,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31654393',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701758814',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31654393,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31654393',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16490989',
        'method' => 'get',
      ),
    ),
  ),
  8602189291 => 
  array (
    'id' => 16491275,
    'name' => 'ООО «Кадастровый центр недвижимости»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506583500,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31654745,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31654745',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602189291',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31654745,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31654745',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16491275',
        'method' => 'get',
      ),
    ),
  ),
  1910012970 => 
  array (
    'id' => 16501393,
    'name' => 'ООО "АЛВАСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1506652852,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31666549,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31666549',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1910012970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31666549,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31666549',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16501393',
        'method' => 'get',
      ),
    ),
  ),
  2224121380 => 
  array (
    'id' => 16501399,
    'name' => 'ООО "Инженерные сети Проект"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1506653062,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31666555,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31666555',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224121380',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31666555,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31666555',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16501399',
        'method' => 'get',
      ),
    ),
  ),
  7705348738 => 
  array (
    'id' => 16502297,
    'name' => 'ООО "Техсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506667778,
    'updated_at' => 1523285107,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31668009,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31668009',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705348738',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31668009,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31668009',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16502297',
        'method' => 'get',
      ),
    ),
  ),
  7714832984 => 
  array (
    'id' => 16504145,
    'name' => 'ООО  "Образование Будущего"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506675477,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31669987,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31669987',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714832984',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31669987,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31669987',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16504145',
        'method' => 'get',
      ),
    ),
  ),
  2463256035 => 
  array (
    'id' => 16556295,
    'name' => 'ООО "ТРАНС7"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506919935,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31722411,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31722411',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463256035',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31722411,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31722411',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16556295',
        'method' => 'get',
      ),
    ),
  ),
  8601043846 => 
  array (
    'id' => 16560221,
    'name' => 'ООО  "ТЕХНОЛОГИИ БУДУЩЕГО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1506933973,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31727127,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31727127',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8601043846',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31727127,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31727127',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16560221',
        'method' => 'get',
      ),
    ),
  ),
);