<?php 
 return array (
  502206375106 => 
  array (
    'id' => 16567944,
    'name' => 'Бабаев Сергей Владимирович, ИП',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507000265,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31738696,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31738696',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '502206375106',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31738696,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31738696',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16567944',
        'method' => 'get',
      ),
    ),
  ),
  860237825647 => 
  array (
    'id' => 16568042,
    'name' => 'ИП ТИМОФЕЕВА ВАЛЕРИЯ ГЕННАДЬЕВНА',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507003351,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31738890,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31738890',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '860237825647',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31738890,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31738890',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16568042',
        'method' => 'get',
      ),
    ),
  ),
  502238059248 => 
  array (
    'id' => 16568452,
    'name' => 'Воронин Александр Сергеевич, ИП',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507007790,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '502238059248',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16568452',
        'method' => 'get',
      ),
    ),
  ),
  3811174998 => 
  array (
    'id' => 16579491,
    'name' => 'ООО СМП ВОСТОК',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507087530,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31753121,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31753121',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811174998',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31753121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31753121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16579491',
        'method' => 'get',
      ),
    ),
  ),
  7804584359 => 
  array (
    'id' => 16583195,
    'name' => 'ООО ПРОГРЕСС',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507111374,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31757717,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31757717',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804584359',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31757717,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31757717',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16583195',
        'method' => 'get',
      ),
    ),
  ),
  7536139254 => 
  array (
    'id' => 16589247,
    'name' => 'ООО «Дорожно-строительная компания»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507175311,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31764289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31764289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7536139254',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Читинская область(GMT +9)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31764289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31764289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16589247',
        'method' => 'get',
      ),
    ),
  ),
  860302800437 => 
  array (
    'id' => 16589289,
    'name' => 'ИП Красноперов Илья Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507177172,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31764369,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31764369',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '860302800437',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ханты-Мансийский АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31764369,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31764369',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16589289',
        'method' => 'get',
      ),
    ),
  ),
  1605007959 => 
  array (
    'id' => 16590987,
    'name' => 'ООО «Ремстрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507188506,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31766959,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31766959',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1605007959',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31766959,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31766959',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16590987',
        'method' => 'get',
      ),
    ),
  ),
  1644087700 => 
  array (
    'id' => 16591093,
    'name' => 'ООО «ДИЦЕНТРА»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507189082,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31767073,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31767073',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1644087700',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31767073,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31767073',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16591093',
        'method' => 'get',
      ),
    ),
  ),
  7842101274 => 
  array (
    'id' => 16591723,
    'name' => '"ТРАНСМЕДСТРОЙ" ооо',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507191694,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31767809,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31767809',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842101274',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31767809,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31767809',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16591723',
        'method' => 'get',
      ),
    ),
  ),
  741805613648 => 
  array (
    'id' => 16599417,
    'name' => 'ИП Яркина Ольга Ивановна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507265676,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31776721,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31776721',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '741805613648',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31776721,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31776721',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16599417',
        'method' => 'get',
      ),
    ),
  ),
  1430009825 => 
  array (
    'id' => 16616554,
    'name' => 'ООО  "Профстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507515113,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31795696,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795696',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1430009825',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31795696,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795696',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16616554',
        'method' => 'get',
      ),
    ),
  ),
  2222846091 => 
  array (
    'id' => 16616564,
    'name' => 'ООО "НЕФТЕРЕСУРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507515478,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31795712,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795712',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2222846091',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31795712,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795712',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16616564',
        'method' => 'get',
      ),
    ),
  ),
  2508119793 => 
  array (
    'id' => 16616568,
    'name' => 'ООО  «ДВ Альянс»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507516173,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31795730,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795730',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2508119793',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31795730,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795730',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16616568',
        'method' => 'get',
      ),
    ),
  ),
  6671018857 => 
  array (
    'id' => 16616727,
    'name' => 'ООО "Гарантия-ЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507521218,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31795889,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795889',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 1549429200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671018857',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31795889,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795889',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16616727',
        'method' => 'get',
      ),
    ),
  ),
  6678037440 => 
  array (
    'id' => 16616743,
    'name' => 'ООО "УРАЛЬСКАЯ ЛОГИСТИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507521586,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31795923,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795923',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678037440',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31795923,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31795923',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16616743',
        'method' => 'get',
      ),
    ),
  ),
  2209044980 => 
  array (
    'id' => 16627227,
    'name' => 'ООО СК "СИБИРЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507607680,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31807293,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31807293',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2209044980',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алтайский край(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31807293,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31807293',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16627227',
        'method' => 'get',
      ),
    ),
  ),
  4205354470 => 
  array (
    'id' => 16627311,
    'name' => 'ООО "ГЛОБУС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507609468,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31807453,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31807453',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205354470',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Кемеровская область(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31807453,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31807453',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16627311',
        'method' => 'get',
      ),
    ),
  ),
  1655232124 => 
  array (
    'id' => 16627477,
    'name' => 'ООО РемСпецМонтаж',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1507612232,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655232124',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16627477',
        'method' => 'get',
      ),
    ),
  ),
  268074870 => 
  array (
    'id' => 16627637,
    'name' => 'ООО "ТРАТАУ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507614340,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31807947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31807947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '268074870',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31807947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31807947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16627637',
        'method' => 'get',
      ),
    ),
  ),
  7708677498 => 
  array (
    'id' => 16632159,
    'name' => 'ООО "Юником"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507629620,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31814351,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31814351',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1515487427,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708677498',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31814351,
        1 => 31850165,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31814351,31850165',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16632159',
        'method' => 'get',
      ),
    ),
  ),
  7460031674 => 
  array (
    'id' => 16632299,
    'name' => 'ООО "ВОЕНСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507630170,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31814521,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31814521',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7460031674',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31814521,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31814521',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16632299',
        'method' => 'get',
      ),
    ),
  ),
  6732067815 => 
  array (
    'id' => 16640809,
    'name' => '"АКВАРОУД"ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507709111,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31824215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31824215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6732067815',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31824215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31824215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16640809',
        'method' => 'get',
      ),
    ),
  ),
  3810041949 => 
  array (
    'id' => 16659479,
    'name' => 'ООО "Уральский технический центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507866735,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31847145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31847145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3810041949',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31847145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31847145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16659479',
        'method' => 'get',
      ),
    ),
  ),
  2311230177 => 
  array (
    'id' => 16659857,
    'name' => 'ООО "РИГЕЛЬСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1507873358,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31847905,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31847905',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311230177',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31847905,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31847905',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16659857',
        'method' => 'get',
      ),
    ),
  ),
  7810028261 => 
  array (
    'id' => 16683129,
    'name' => 'НП «ФАСО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508154690,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31872629,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31872629',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810028261',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31872629,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31872629',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16683129',
        'method' => 'get',
      ),
    ),
  ),
  2450025746 => 
  array (
    'id' => 16690377,
    'name' => 'ООО "Сибторг"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508211208,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31880097,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31880097',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2450025746',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31880097,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31880097',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16690377',
        'method' => 'get',
      ),
    ),
  ),
  245713770972 => 
  array (
    'id' => 16690403,
    'name' => 'ИП Кутелев Николай Константинович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508212141,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31880131,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31880131',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '245713770972',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31880131,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31880131',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16690403',
        'method' => 'get',
      ),
    ),
  ),
  6165190805 => 
  array (
    'id' => 16696219,
    'name' => 'ООО ПМК "Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508238518,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31886903,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31886903',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165190805',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31886903,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31886903',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16696219',
        'method' => 'get',
      ),
    ),
  ),
  7728844193 => 
  array (
    'id' => 16707971,
    'name' => 'ООО "ПРОСПЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508318936,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31902939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31902939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728844193',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31902939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31902939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16707971',
        'method' => 'get',
      ),
    ),
  ),
  540542443308 => 
  array (
    'id' => 16715059,
    'name' => 'ИП  Алексеев Антон Андреевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508383616,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31909629,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31909629',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '540542443308',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31909629,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31909629',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16715059',
        'method' => 'get',
      ),
    ),
  ),
  615513136802 => 
  array (
    'id' => 16715601,
    'name' => 'ИП  Ильин Виктор Васильевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508392658,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31910333,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31910333',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '615513136802',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31910333,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31910333',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16715601',
        'method' => 'get',
      ),
    ),
  ),
  7806444491 => 
  array (
    'id' => 16751487,
    'name' => 'Брабиль, ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1508766100,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31946469,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31946469',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806444491',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31946469,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31946469',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16751487',
        'method' => 'get',
      ),
    ),
  ),
  246300086970 => 
  array (
    'id' => 16754341,
    'name' => 'ИП  Ковалева Наталья Ивановна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508820163,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31949391,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31949391',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '246300086970',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31949391,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31949391',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16754341',
        'method' => 'get',
      ),
    ),
  ),
  246502904805 => 
  array (
    'id' => 16754463,
    'name' => 'ИП  ГРИЦ ЗУХРА АХМАТХАНОВНА',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508821597,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31949667,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31949667',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '246502904805',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31949667,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31949667',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16754463',
        'method' => 'get',
      ),
    ),
  ),
  635700523000 => 
  array (
    'id' => 16754753,
    'name' => 'ИП Евдокимова Ирина Генриховна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508824923,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31950007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31950007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '635700523000',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31950007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31950007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16754753',
        'method' => 'get',
      ),
    ),
  ),
  730201760345 => 
  array (
    'id' => 16760117,
    'name' => 'ИП Бушковский Людвик Виталиевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508848055,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31956071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31956071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '730201760345',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31956071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31956071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16760117',
        'method' => 'get',
      ),
    ),
  ),
  261002710869 => 
  array (
    'id' => 16772181,
    'name' => 'ИП Алабушкин Владислав Вячеславович',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508935551,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31981001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31981001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '261002710869',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Краснодарский край(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31981001,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31981001',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16772181',
        'method' => 'get',
      ),
    ),
  ),
  5617020775 => 
  array (
    'id' => 16776339,
    'name' => 'ООО "Артель"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508996288,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31985133,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31985133',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5617020775',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31985133,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31985133',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16776339',
        'method' => 'get',
      ),
    ),
  ),
  232603078990 => 
  array (
    'id' => 16776653,
    'name' => 'ИП  Саратова Елена Александровна',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1508998723,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31985603,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31985603',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '232603078990',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Дагестан(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31985603,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31985603',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16776653',
        'method' => 'get',
      ),
    ),
  ),
  9109013734 => 
  array (
    'id' => 16806092,
    'name' => 'ООО "РЕШКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1509336167,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32017788,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32017788',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1511339876,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9109013734',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Крым(GMT +0)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32017788,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32017788',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16806092',
        'method' => 'get',
      ),
    ),
  ),
  2635228668 => 
  array (
    'id' => 16816893,
    'name' => 'ООО «Стройка»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1509431296,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32030379,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32030379',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635228668',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32030379,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32030379',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16816893',
        'method' => 'get',
      ),
    ),
  ),
  272904605 => 
  array (
    'id' => 16826189,
    'name' => 'ООО "ТехноСервисСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1509513318,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32044199,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32044199',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '272904605',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32044199,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32044199',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16826189',
        'method' => 'get',
      ),
    ),
  ),
  1832133318 => 
  array (
    'id' => 16826231,
    'name' => 'ООО "АрмНефтеГаз"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1509514085,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32044291,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32044291',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1832133318',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32044291,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32044291',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16826231',
        'method' => 'get',
      ),
    ),
  ),
  7719892088 => 
  array (
    'id' => 16830395,
    'name' => 'ООО «Сервис Групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1509533027,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32049093,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32049093',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719892088',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32049093,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32049093',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16830395',
        'method' => 'get',
      ),
    ),
  ),
  772173766870 => 
  array (
    'id' => 16837023,
    'name' => 'Толстых Федор Вячеславович',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1509608027,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32056091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32056091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '772173766870',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32056091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32056091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16837023',
        'method' => 'get',
      ),
    ),
  ),
  7811522032 => 
  array (
    'id' => 16840687,
    'name' => 'ООО "КОМПЛЕКС ИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1509620364,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32060135,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32060135',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811522032',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32060135,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32060135',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16840687',
        'method' => 'get',
      ),
    ),
  ),
  3811999558 => 
  array (
    'id' => 16892989,
    'name' => 'ООО "АГЕНТСТВО "АВРОРА-ИРКУТСК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510203491,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32119319,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32119319',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811999558',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32119319,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32119319',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16892989',
        'method' => 'get',
      ),
    ),
  ),
  2623801690 => 
  array (
    'id' => 16893597,
    'name' => 'ООО "Третий Рим"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510210225,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32120027,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32120027',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2623801690',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32120027,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32120027',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16893597',
        'method' => 'get',
      ),
    ),
  ),
  2308200214 => 
  array (
    'id' => 16896283,
    'name' => 'ООО "ГЕРМЕС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510222562,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32123029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32123029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308200214',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32123029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32123029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16896283',
        'method' => 'get',
      ),
    ),
  ),
  1635008928 => 
  array (
    'id' => 16919311,
    'name' => 'ООО «Евро Акцент Саба»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510552553,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32146775,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32146775',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1635008928',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32146775,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32146775',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16919311',
        'method' => 'get',
      ),
    ),
  ),
  2225078730 => 
  array (
    'id' => 16931341,
    'name' => 'ООО "АЛТАЙ-ТЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510632548,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32161939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32161939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225078730',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32161939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32161939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16931341',
        'method' => 'get',
      ),
    ),
  ),
  2703054081 => 
  array (
    'id' => 16951838,
    'name' => 'ООО "ДАЛЬТЕК"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510792954,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201250,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201250',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2703054081',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201250,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201250',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16951838',
        'method' => 'get',
      ),
    ),
  ),
  2707001965 => 
  array (
    'id' => 16951850,
    'name' => 'ООО "Хабэко-Партнер"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510793786,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201260,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201260',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2707001965',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201260,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201260',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16951850',
        'method' => 'get',
      ),
    ),
  ),
  4101113784 => 
  array (
    'id' => 16951893,
    'name' => 'ООО "ВЕТЕРАН-МОРТРАНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510798136,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201303,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201303',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101113784',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201303,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201303',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16951893',
        'method' => 'get',
      ),
    ),
  ),
  2452038807 => 
  array (
    'id' => 16951941,
    'name' => 'ООО "КРЕАТИВНОЕ БЮРО"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510800843,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201349,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201349',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2452038807',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201349,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201349',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16951941',
        'method' => 'get',
      ),
    ),
  ),
  2464121619 => 
  array (
    'id' => 16951995,
    'name' => 'ООО "СТРОИТЕЛИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510802617,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32201385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464121619',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32201385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32201385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16951995',
        'method' => 'get',
      ),
    ),
  ),
  1429006509 => 
  array (
    'id' => 16961073,
    'name' => 'ООО «Сахадортранс»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1510836110,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32212257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1429006509',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32212257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32212257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16961073',
        'method' => 'get',
      ),
    ),
  ),
  3702169187 => 
  array (
    'id' => 17042835,
    'name' => 'ООО Олимп',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1511515926,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32316539,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32316539',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702169187',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32316539,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32316539',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17042835',
        'method' => 'get',
      ),
    ),
  ),
  540362832560 => 
  array (
    'id' => 17069639,
    'name' => 'ИП Жамьянов К.М.',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1511833541,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29961485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1512445376,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '540362832560',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29961485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17069639',
        'method' => 'get',
      ),
    ),
  ),
  6512000558 => 
  array (
    'id' => 17119315,
    'name' => 'Государственное унитарное предприятие Сахалинской области "Макаровское дорожное ремонтно-строительное управление"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1512359204,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32398255,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32398255',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6512000558',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32398255,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32398255',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17119315',
        'method' => 'get',
      ),
    ),
  ),
  2636025149 => 
  array (
    'id' => 17124209,
    'name' => 'Муниципальное унитарное предприятие города Ставрополя "Ремонтно-строительное предприятие"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1512386753,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32403729,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32403729',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2636025149',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32403729,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32403729',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17124209',
        'method' => 'get',
      ),
    ),
  ),
  4101102503 => 
  array (
    'id' => 17141519,
    'name' => 'ООО «Жилремсервис Д»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1512530354,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32422317,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32422317',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101102503',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32422317,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32422317',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17141519',
        'method' => 'get',
      ),
    ),
  ),
  4214017908 => 
  array (
    'id' => 17141541,
    'name' => 'ООО "Дельта-Центр"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1512530918,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32422337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32422337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4214017908',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32422337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32422337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17141541',
        'method' => 'get',
      ),
    ),
  ),
  6504013112 => 
  array (
    'id' => 17150983,
    'name' => 'ООО "Частная охранная организация "Авилон-СБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1512618619,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32432969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32432969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921242,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6504013112',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32432969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32432969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17150983',
        'method' => 'get',
      ),
    ),
  ),
  277918930 => 
  array (
    'id' => 17161085,
    'name' => 'ООО "МЕДИЦИНСКИЙ СЕРВИСНЫЙ ЦЕНТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1512709556,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32444575,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32444575',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '277918930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32444575,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32444575',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17161085',
        'method' => 'get',
      ),
    ),
  ),
  1635005846 => 
  array (
    'id' => 17218401,
    'name' => 'АО "Сабинское МПП',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1513256101,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32510509,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32510509',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 32510507,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=32510507',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1514203019,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1635005846',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32510509,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32510509',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17218401',
        'method' => 'get',
      ),
    ),
  ),
  5031092811 => 
  array (
    'id' => 17225389,
    'name' => 'ООО "ИНСТРИМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1513331110,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32517679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32517679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5031092811',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32517679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32517679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17225389',
        'method' => 'get',
      ),
    ),
  ),
  323356036 => 
  array (
    'id' => 17240533,
    'name' => 'ООО Судэкс',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1513589234,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29961485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1514277951,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323356036',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29961485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17240533',
        'method' => 'get',
      ),
    ),
  ),
  4001009645 => 
  array (
    'id' => 17299499,
    'name' => 'ООО "АЛЕРТКОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1513921044,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32594621,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32594621',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4001009645',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32594621,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32594621',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17299499',
        'method' => 'get',
      ),
    ),
  ),
  276007331 => 
  array (
    'id' => 17398099,
    'name' => 'МУП "Горзеленхоз" городского округа город Уфа',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1515561008,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32702905,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32702905',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '276007331',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32702905,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32702905',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17398099',
        'method' => 'get',
      ),
    ),
  ),
  164502624201 => 
  array (
    'id' => 17398201,
    'name' => 'ИП Тихомиров Александр Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1515562675,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32703033,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32703033',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '164502624201',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32703033,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32703033',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17398201',
        'method' => 'get',
      ),
    ),
  ),
  2543116964 => 
  array (
    'id' => 17445291,
    'name' => 'ООО Кадастр',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1516093007,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32806515,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32806515',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2543116964',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32806515,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32806515',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17445291',
        'method' => 'get',
      ),
    ),
  ),
  323366274 => 
  array (
    'id' => 17571273,
    'name' => 'ООО Неомьюзик',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1517307001,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29961485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1518412516,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323366274',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29961485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17571273',
        'method' => 'get',
      ),
    ),
  ),
  6161080199 => 
  array (
    'id' => 17578835,
    'name' => 'ООО "ЮКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1517380149,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32996857,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32996857',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6161080199',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32996857,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32996857',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17578835',
        'method' => 'get',
      ),
    ),
  ),
  3666080117 => 
  array (
    'id' => 17707007,
    'name' => 'ЗАО "АВИАЦИОННАЯ КОМПАНИЯ "ПОЛЕТ""',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1518500772,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33165743,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33165743',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3666080117',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33165743,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33165743',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17707007',
        'method' => 'get',
      ),
    ),
  ),
  2320126398 => 
  array (
    'id' => 17716487,
    'name' => 'ООО "РЕКЛАМНЫЙ ДОМ "МАКСИМА"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1518586828,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33186031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33186031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2320126398',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33186031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33186031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17716487',
        'method' => 'get',
      ),
    ),
  ),
  7727218570 => 
  array (
    'id' => 17718733,
    'name' => 'ООО  "Энитэль.Ко."',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1518598279,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33189083,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33189083',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727218570',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33189083,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33189083',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17718733',
        'method' => 'get',
      ),
    ),
  ),
  4632173132 => 
  array (
    'id' => 17725445,
    'name' => 'ООО "Строй Трест"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1518675018,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33196169,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33196169',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632173132',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33196169,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33196169',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17725445',
        'method' => 'get',
      ),
    ),
  ),
  1650332301 => 
  array (
    'id' => 17739883,
    'name' => 'ООО "Автозаводский-НЧ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1518775329,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33214057,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33214057',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1650332301',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33214057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33214057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17739883',
        'method' => 'get',
      ),
    ),
  ),
  2465169684 => 
  array (
    'id' => 17753959,
    'name' => 'ООО "ДОРРЕМСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519018166,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33226935,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33226935',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465169684',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33226935,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33226935',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17753959',
        'method' => 'get',
      ),
    ),
  ),
  3703020817 => 
  array (
    'id' => 17754469,
    'name' => 'ООО "ГЛАВШВЕЙПРОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519022498,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33227609,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33227609',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3703020817',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33227609,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33227609',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17754469',
        'method' => 'get',
      ),
    ),
  ),
  7704218341 => 
  array (
    'id' => 17754949,
    'name' => 'АО "Научно-производственное предприятие "Русперфоратор"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519025184,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33228167,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33228167',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704218341',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33228167,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33228167',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17754949',
        'method' => 'get',
      ),
    ),
  ),
  278932239 => 
  array (
    'id' => 17783125,
    'name' => 'ООО "ИНВЕСТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519277074,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33269231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33269231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278932239',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33269231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33269231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17783125',
        'method' => 'get',
      ),
    ),
  ),
  4205009234 => 
  array (
    'id' => 17802919,
    'name' => 'ООО "Кузбасспецвзрыв"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519611887,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33288411,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33288411',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205009234',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33288411,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33288411',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17802919',
        'method' => 'get',
      ),
    ),
  ),
  2311214023 => 
  array (
    'id' => 17814281,
    'name' => 'ООО "Технологии Юга"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519711951,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33304751,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33304751',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311214023',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33304751,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33304751',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17814281',
        'method' => 'get',
      ),
    ),
  ),
  2902053117 => 
  array (
    'id' => 17814447,
    'name' => 'ООО "ПРОМСЕРВИС-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519713295,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33304959,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33304959',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2902053117',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33304959,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33304959',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17814447',
        'method' => 'get',
      ),
    ),
  ),
  1645023611 => 
  array (
    'id' => 17820077,
    'name' => 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ТАТАРСКАЯ ИННОВАЦИОННАЯ ПРОЕКТНАЯ ОРГАНИЗАЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519735134,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33312007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33312007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1645023611',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33312007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33312007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17820077',
        'method' => 'get',
      ),
    ),
  ),
  1120004513 => 
  array (
    'id' => 17824335,
    'name' => 'ООО «Усть-Цильмаагропромсервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519797816,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33316341,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33316341',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1120004513',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33316341,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33316341',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17824335',
        'method' => 'get',
      ),
    ),
  ),
  2634096240 => 
  array (
    'id' => 17824549,
    'name' => 'ООО СВИВ26',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519799109,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33316593,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33316593',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2634096240',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33316593,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33316593',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17824549',
        'method' => 'get',
      ),
    ),
  ),
  4317005069 => 
  array (
    'id' => 17824795,
    'name' => 'ООО  "Альбиойл"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1519800799,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33316909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33316909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4317005069',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33316909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33316909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17824795',
        'method' => 'get',
      ),
    ),
  ),
  572012927 => 
  array (
    'id' => 17861239,
    'name' => 'ООО ЧОО "АЛЬФА-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1520239546,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33388447,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33388447',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '572012927',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33388447,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33388447',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17861239',
        'method' => 'get',
      ),
    ),
  ),
  1651077044 => 
  array (
    'id' => 17968907,
    'name' => 'ООО Визор',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1521528001,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33525879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33525879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1651077044',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33525879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33525879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17968907',
        'method' => 'get',
      ),
    ),
  ),
  5051009755 => 
  array (
    'id' => 17972665,
    'name' => 'ООО "Прогресс - 12"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1521545715,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33543411,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33543411',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5051009755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33543411,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33543411',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17972665',
        'method' => 'get',
      ),
    ),
  ),
  5836655545 => 
  array (
    'id' => 17980935,
    'name' => 'ООО "СКЛАД58"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1521627470,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33551693,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33551693',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5836655545',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33551693,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33551693',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17980935',
        'method' => 'get',
      ),
    ),
  ),
  2130157542 => 
  array (
    'id' => 18053021,
    'name' => 'ООО "МОНОПОЛИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1522391779,
    'updated_at' => 1523285108,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33658061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33658061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130157542',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33658061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33658061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18053021',
        'method' => 'get',
      ),
    ),
  ),
  170101393073 => 
  array (
    'id' => 18137347,
    'name' => 'Канзан-оол Людмила Викторовна',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523333590,
    'updated_at' => 1523333591,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '170101393073',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137347',
        'method' => 'get',
      ),
    ),
  ),
  5404028762 => 
  array (
    'id' => 18137393,
    'name' => 'ООО "ОКНО-ТВ СИБИРЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523334262,
    'updated_at' => 1523334355,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773459,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773459',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404028762',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773459,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773459',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137393',
        'method' => 'get',
      ),
    ),
  ),
  5410784189 => 
  array (
    'id' => 18137421,
    'name' => 'ООО "НОТУСКАР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523335044,
    'updated_at' => 1523335044,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410784189',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137421',
        'method' => 'get',
      ),
    ),
  ),
  5905300190 => 
  array (
    'id' => 18094029,
    'name' => 'ДСК Уралдорстрой, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1522817486,
    'updated_at' => 1523335155,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33715789,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33715789',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33715787,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33715787',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 1528261200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905300190',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33715789,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33715789',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18094029',
        'method' => 'get',
      ),
    ),
  ),
  7722703171 => 
  array (
    'id' => 18127215,
    'name' => 'ООО "Кипарис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523256184,
    'updated_at' => 1523335212,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33762583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33762583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722703171',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33762583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33762583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18127215',
        'method' => 'get',
      ),
    ),
  ),
  6376010709 => 
  array (
    'id' => 18126537,
    'name' => 'ООО "АВТОритет"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523250772,
    'updated_at' => 1523335221,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33761899,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33761899',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6376010709',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'самара',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33761899,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33761899',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18126537',
        'method' => 'get',
      ),
    ),
  ),
  3254005840 => 
  array (
    'id' => 18127295,
    'name' => 'ООО "ТЕПЛО СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523256662,
    'updated_at' => 1523335249,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33762801,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33762801',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3254005840',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33762801,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33762801',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18127295',
        'method' => 'get',
      ),
    ),
  ),
  6501143650 => 
  array (
    'id' => 18137445,
    'name' => 'ООО «Проектировщик - 2»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523335514,
    'updated_at' => 1523335514,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773529,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773529',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501143650',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773529,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773529',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137445',
        'method' => 'get',
      ),
    ),
  ),
  5433172305 => 
  array (
    'id' => 18137463,
    'name' => 'ООО "НТК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523335790,
    'updated_at' => 1523336271,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773543,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773543',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5433172305',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773543,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773543',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137463',
        'method' => 'get',
      ),
    ),
  ),
  2537078566 => 
  array (
    'id' => 18137525,
    'name' => 'ООО "Влад-Ойл"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523336454,
    'updated_at' => 1523336454,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773597,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773597',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2537078566',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773597,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773597',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137525',
        'method' => 'get',
      ),
    ),
  ),
  1405000755 => 
  array (
    'id' => 18137545,
    'name' => 'УСЛУГА ООО',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523336806,
    'updated_at' => 1523336891,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773635,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773635',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1405000755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773635,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773635',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137545',
        'method' => 'get',
      ),
    ),
  ),
  700700027367 => 
  array (
    'id' => 17398483,
    'name' => 'Индивидуальный предприниматель Журавлев Сергей Александрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1515565440,
    'updated_at' => 1523337421,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32703425,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32703425',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1523337421,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '700700027367',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32703425,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32703425',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17398483',
        'method' => 'get',
      ),
    ),
  ),
  5249156710 => 
  array (
    'id' => 18137589,
    'name' => 'ООО «АВТОКОМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523337572,
    'updated_at' => 1523337572,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773691,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773691',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5249156710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773691,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773691',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137589',
        'method' => 'get',
      ),
    ),
  ),
  2540183754 => 
  array (
    'id' => 18137713,
    'name' => 'ООО  РСК "Стратег-Ост"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523339284,
    'updated_at' => 1523339284,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773851,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773851',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2540183754',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773851,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773851',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137713',
        'method' => 'get',
      ),
    ),
  ),
  253707165384 => 
  array (
    'id' => 18137715,
    'name' => 'ИП ЯШКОВ АЛЕКСЕЙ ВЛАДИМИРОВИЧ',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523339299,
    'updated_at' => 1523339413,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773853,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773853',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '253707165384',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773853,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773853',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137715',
        'method' => 'get',
      ),
    ),
  ),
  2224153078 => 
  array (
    'id' => 18137597,
    'name' => 'ООО "Эксперт"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523337716,
    'updated_at' => 1523340354,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523393940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224153078',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137597',
        'method' => 'get',
      ),
    ),
  ),
  2224138270 => 
  array (
    'id' => 18137855,
    'name' => 'ООО "Акмилайт"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523340450,
    'updated_at' => 1523340457,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33774009,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33774009',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523393940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224138270',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Алексей Иванович',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33774009,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33774009',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137855',
        'method' => 'get',
      ),
    ),
  ),
  4705070195 => 
  array (
    'id' => 18113929,
    'name' => 'ООО "ИЖОРА-АВТОСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523004669,
    'updated_at' => 1523341293,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33750001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33750001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4705070195',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33750001,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33750001',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18113929',
        'method' => 'get',
      ),
    ),
  ),
  7816591417 => 
  array (
    'id' => 18113063,
    'name' => 'Гефест, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523001402,
    'updated_at' => 1523341836,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33749031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33749031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816591417',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33749031,
        1 => 33749037,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33749031,33749037',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18113063',
        'method' => 'get',
      ),
    ),
  ),
  5406633109 => 
  array (
    'id' => 18138091,
    'name' => 'ООО "ТехноДор"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523341990,
    'updated_at' => 1523342552,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33774303,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33774303',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523393940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406633109',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33774303,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33774303',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18138091',
        'method' => 'get',
      ),
    ),
  ),
  5643022352 => 
  array (
    'id' => 15727941,
    'name' => 'Вектор, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1498022953,
    'updated_at' => 1523342948,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407471,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407471',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 31454991,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=31454991',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1505121650,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5643022352',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407471,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407471',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15727941',
        'method' => 'get',
      ),
    ),
  ),
  1650009877 => 
  array (
    'id' => 18132035,
    'name' => 'Транссервис-ЛТД, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523274709,
    'updated_at' => 1523343792,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33767811,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33767811',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 30473573,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=30473573',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1650009877',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3 тат',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33767811,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33767811',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18132035',
        'method' => 'get',
      ),
    ),
  ),
  4345140662 => 
  array (
    'id' => 18139475,
    'name' => 'ООО "АБЗ-Оричи"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523348457,
    'updated_at' => 1523348458,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33776019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33776019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345140662',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33776019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33776019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18139475',
        'method' => 'get',
      ),
    ),
  ),
  7815025049 => 
  array (
    'id' => 18139893,
    'name' => 'Страховое акционерное общество ЭРГО',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523350198,
    'updated_at' => 1523350211,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33776589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33776589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523480340,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7815025049',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33776589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33776589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18139893',
        'method' => 'get',
      ),
    ),
  ),
  2355012641 => 
  array (
    'id' => 14774364,
    'name' => 'ЗАО "САНАТОРИЙ "ЗОРЬКА"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1488187583,
    'updated_at' => 1523356883,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29109478,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29109478',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2355012641',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29109478,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29109478',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14774364',
        'method' => 'get',
      ),
    ),
  ),
  5246044511 => 
  array (
    'id' => 18142189,
    'name' => 'Благоустройство, МУП',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523359159,
    'updated_at' => 1523431658,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33779007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33779007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5246044511',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33779007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33779007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18142189',
        'method' => 'get',
      ),
    ),
  ),
  5903129283 => 
  array (
    'id' => 18137609,
    'name' => '"СУ-7"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523338114,
    'updated_at' => 1523358040,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773723,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773723',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903129283',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773723,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773723',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137609',
        'method' => 'get',
      ),
    ),
  ),
  1105018215 => 
  array (
    'id' => 18142015,
    'name' => 'ооо "СанТехСтрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523358364,
    'updated_at' => 1523358378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33778805,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33778805',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33778803,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33778803',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1105018215',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'плюс 3 коми',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33778805,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33778805',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18142015',
        'method' => 'get',
      ),
    ),
  ),
  6321421570 => 
  array (
    'id' => 18142537,
    'name' => 'ООО "Медрост"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523360816,
    'updated_at' => 1523360866,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33779513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33779513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6321421570',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33779513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33779513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18142537',
        'method' => 'get',
      ),
    ),
  ),
  6725011515 => 
  array (
    'id' => 18142357,
    'name' => '"Управляющая компания"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523360058,
    'updated_at' => 1523362778,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33779263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33779263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6725011515',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33779263,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33779263',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18142357',
        'method' => 'get',
      ),
    ),
  ),
  7710294238 => 
  array (
    'id' => 18143371,
    'name' => 'ООО "ИНВИТРО"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523364469,
    'updated_at' => 1523364469,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33780479,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33780479',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710294238',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33780479,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33780479',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18143371',
        'method' => 'get',
      ),
    ),
  ),
  '' => 
  array (
    'id' => 17976065,
    'name' => 'Простые звонки, 78442981367',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1520856674,
    'updated_at' => 1524544902,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33546707,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33546707',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33546707,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33546707',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17976065',
        'method' => 'get',
      ),
    ),
  ),
  'инн 2224171888' => 
  array (
    'id' => 18117547,
    'name' => 'ооо сет сервис',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523022826,
    'updated_at' => 1523417623,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33753893,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33753893',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33753891,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33753891',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1523417623,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'инн 2224171888',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'плюс 6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33753893,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33753893',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18117547',
        'method' => 'get',
      ),
    ),
  ),
  7727235230 => 
  array (
    'id' => 15128149,
    'name' => 'Нагваль Стройтех, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491809348,
    'updated_at' => 1523420279,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29567871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29567871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727235230',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29567871,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29567871',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15128149',
        'method' => 'get',
      ),
    ),
  ),
  6671456346 => 
  array (
    'id' => 18103489,
    'name' => 'ООО ПСБ "УЛЬТРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522907855,
    'updated_at' => 1523420563,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33726093,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33726093',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671456346',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33726093,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33726093',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18103489',
        'method' => 'get',
      ),
    ),
  ),
  6670287487 => 
  array (
    'id' => 17008163,
    'name' => 'ООО «Технострой-Гранд»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511323277,
    'updated_at' => 1523420702,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32260371,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260371',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670287487',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'свердловская область',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32260371,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32260371',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17008163',
        'method' => 'get',
      ),
    ),
  ),
  9718060563 => 
  array (
    'id' => 18032199,
    'name' => 'ООО "БИЗНЕС ГАРМОНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522211704,
    'updated_at' => 1523420753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33625617,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33625617',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718060563',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33625617,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33625617',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18032199',
        'method' => 'get',
      ),
    ),
  ),
  2466244119 => 
  array (
    'id' => 18147037,
    'name' => 'ООО "Скерцо"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523420990,
    'updated_at' => 1523420996,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784267,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784267',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523480340,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466244119',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784267,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784267',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147037',
        'method' => 'get',
      ),
    ),
  ),
  5402001973 => 
  array (
    'id' => 18147109,
    'name' => 'ООО «АВИР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523422340,
    'updated_at' => 1523422532,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784331,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784331',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402001973',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784331,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784331',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147109',
        'method' => 'get',
      ),
    ),
  ),
  7458000326 => 
  array (
    'id' => 18147181,
    'name' => 'ООО "ГазСпецСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523423171,
    'updated_at' => 1523423171,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7458000326',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147181',
        'method' => 'get',
      ),
    ),
  ),
  5903092178 => 
  array (
    'id' => 18147209,
    'name' => 'ООО "МЕТСТРОЙКОМПЛЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523423433,
    'updated_at' => 1523423457,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903092178',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147209',
        'method' => 'get',
      ),
    ),
  ),
  7451213195 => 
  array (
    'id' => 18126411,
    'name' => 'ООО "Производственно- Коммерческая Фирма "ЧелябГазСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523249160,
    'updated_at' => 1523423504,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33761773,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33761773',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451213195',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33761773,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33761773',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18126411',
        'method' => 'get',
      ),
    ),
  ),
  7448179498 => 
  array (
    'id' => 18147221,
    'name' => 'ООО Производственно-коммерческое предприятие "Спецтехника"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523423568,
    'updated_at' => 1523423725,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784429,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784429',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448179498',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784429,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784429',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147221',
        'method' => 'get',
      ),
    ),
  ),
  2225188588 => 
  array (
    'id' => 18147267,
    'name' => 'ООО "БЛОК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523424034,
    'updated_at' => 1523424034,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784461,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784461',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225188588',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784461,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784461',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147267',
        'method' => 'get',
      ),
    ),
  ),
  2506009114 => 
  array (
    'id' => 18147311,
    'name' => 'ООО "ДАЛЬНЕРЕЧЕНСК АВИА"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523424316,
    'updated_at' => 1523424321,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784491,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784491',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523480340,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2506009114',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784491,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784491',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147311',
        'method' => 'get',
      ),
    ),
  ),
  2507012279 => 
  array (
    'id' => 18147339,
    'name' => 'ООО СТРОИТЕЛЬНАЯ КОМПАНИЯ "ПАРТНЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523424637,
    'updated_at' => 1523424637,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784519,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784519',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2507012279',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784519,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784519',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147339',
        'method' => 'get',
      ),
    ),
  ),
  6671425267 => 
  array (
    'id' => 18147355,
    'name' => 'ООО «Профторг»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523424742,
    'updated_at' => 1523424771,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784531,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784531',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671425267',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784531,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784531',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147355',
        'method' => 'get',
      ),
    ),
  ),
  5402549304 => 
  array (
    'id' => 18147397,
    'name' => 'ООО "БАЙКАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523425130,
    'updated_at' => 1523425174,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784577,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784577',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402549304',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784577,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784577',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147397',
        'method' => 'get',
      ),
    ),
  ),
  6330068230 => 
  array (
    'id' => 18147415,
    'name' => 'ООО "РОССТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523425256,
    'updated_at' => 1523425303,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784595,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784595',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6330068230',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784595,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784595',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147415',
        'method' => 'get',
      ),
    ),
  ),
  107016050 => 
  array (
    'id' => 18147439,
    'name' => 'ООО «НОВАТЕК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523425413,
    'updated_at' => 1523425429,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784617,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784617',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '107016050',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784617,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784617',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147439',
        'method' => 'get',
      ),
    ),
  ),
  6670413029 => 
  array (
    'id' => 18147455,
    'name' => 'ООО "Промсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523425611,
    'updated_at' => 1523425621,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784639,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784639',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670413029',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784639,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784639',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147455',
        'method' => 'get',
      ),
    ),
  ),
  7203335735 => 
  array (
    'id' => 18147471,
    'name' => 'ООО "ТК Стимул"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523425769,
    'updated_at' => 1523425783,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784663,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784663',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203335735',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784663,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784663',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147471',
        'method' => 'get',
      ),
    ),
  ),
  8617032649 => 
  array (
    'id' => 18147479,
    'name' => 'ООО "Антей ПРО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523425903,
    'updated_at' => 1523425904,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784673',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8617032649',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784673,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784673',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147479',
        'method' => 'get',
      ),
    ),
  ),
  6501282149 => 
  array (
    'id' => 18147497,
    'name' => 'ООО "ЛОДДЕС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523426071,
    'updated_at' => 1523426230,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784691,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784691',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501282149',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784691,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784691',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147497',
        'method' => 'get',
      ),
    ),
  ),
  2460245194 => 
  array (
    'id' => 18137719,
    'name' => 'ООО "Автоспецмаш"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523339325,
    'updated_at' => 1523426257,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773859,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773859',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1523426255,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2460245194',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'красноярск плюс 7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773859,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773859',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137719',
        'method' => 'get',
      ),
    ),
  ),
  2723088258 => 
  array (
    'id' => 18147207,
    'name' => 'ООО "РОССТРОЙ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523423386,
    'updated_at' => 1523429049,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33784413,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784413',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523480340,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723088258',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33784413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33784413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18147207',
        'method' => 'get',
      ),
    ),
  ),
  7717786704 => 
  array (
    'id' => 18148221,
    'name' => 'ООО "КАПСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523430744,
    'updated_at' => 1523430744,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33785591,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33785591',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717786704',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33785591,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33785591',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18148221',
        'method' => 'get',
      ),
    ),
  ),
  5044052189 => 
  array (
    'id' => 18127445,
    'name' => 'ООО "Родер"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523257406,
    'updated_at' => 1523433151,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33762953,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33762953',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1523433151,
    'closest_task_at' => 1523566740,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5044052189',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33762953,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33762953',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18127445',
        'method' => 'get',
      ),
    ),
  ),
  5917235242 => 
  array (
    'id' => 18148077,
    'name' => 'ООО "Стройинвест"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523430001,
    'updated_at' => 1523435000,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33785407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33785407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5917235242',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33785407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33785407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18148077',
        'method' => 'get',
      ),
    ),
  ),
  7814585873 => 
  array (
    'id' => 18149747,
    'name' => 'ооо "Пелорус"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523435338,
    'updated_at' => 1523435339,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33787203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814585873',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33787203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18149747',
        'method' => 'get',
      ),
    ),
  ),
  7810149763 => 
  array (
    'id' => 18150027,
    'name' => 'ООО «ФАВОР-ГАРАНТ»',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523436477,
    'updated_at' => 1523436477,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33787505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810149763',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33787505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18150027',
        'method' => 'get',
      ),
    ),
  ),
  5040035707 => 
  array (
    'id' => 17969175,
    'name' => 'ООО Фирма "РЕЗОН-СЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1521529495,
    'updated_at' => 1523437057,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33526217,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33526217',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040035707',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33526217,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33526217',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17969175',
        'method' => 'get',
      ),
    ),
  ),
  7716870664 => 
  array (
    'id' => 18150269,
    'name' => 'ООО «Инжиниринговая компания ФОРСИС»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523437344,
    'updated_at' => 1523437495,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33787797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716870664',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33787797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18150269',
        'method' => 'get',
      ),
    ),
  ),
  5047201329 => 
  array (
    'id' => 18150495,
    'name' => 'ООО "ВСИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523438331,
    'updated_at' => 1523438396,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33788051,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33788051',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047201329',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33788051,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33788051',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18150495',
        'method' => 'get',
      ),
    ),
  ),
  9709021279 => 
  array (
    'id' => 18150659,
    'name' => 'ООО "КОНСТАНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523438945,
    'updated_at' => 1523438946,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33788231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33788231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9709021279',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33788231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33788231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18150659',
        'method' => 'get',
      ),
    ),
  ),
  6027168971 => 
  array (
    'id' => 18150837,
    'name' => 'ООО "СТО Грузтехника"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523439595,
    'updated_at' => 1523439595,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33788405,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33788405',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6027168971',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33788405,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33788405',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18150837',
        'method' => 'get',
      ),
    ),
  ),
  7728371290 => 
  array (
    'id' => 18152625,
    'name' => 'ООО "СОБНАТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523447323,
    'updated_at' => 1523447324,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33795353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728371290',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33795353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18152625',
        'method' => 'get',
      ),
    ),
  ),
  9705041370 => 
  array (
    'id' => 18152645,
    'name' => 'ООО "ССУ-6"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523447485,
    'updated_at' => 1523447486,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33795375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705041370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33795375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18152645',
        'method' => 'get',
      ),
    ),
  ),
  7743144929 => 
  array (
    'id' => 18152757,
    'name' => 'ООО "МКМ СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523448045,
    'updated_at' => 1523448046,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33795525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743144929',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33795525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18152757',
        'method' => 'get',
      ),
    ),
  ),
  1603002962 => 
  array (
    'id' => 18073883,
    'name' => 'ооо Стройсервис',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1522650465,
    'updated_at' => 1523451805,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33695355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33695355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33705697,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33705697',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1603002962',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33695355,
        1 => 33747469,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33695355,33747469',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18073883',
        'method' => 'get',
      ),
    ),
  ),
  720412279473 => 
  array (
    'id' => 18157239,
    'name' => 'ИП Полицина Вера Сергеевна',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523506458,
    'updated_at' => 1523506459,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800111,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800111',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '720412279473',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800111,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800111',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157239',
        'method' => 'get',
      ),
    ),
  ),
  7448125220 => 
  array (
    'id' => 18157245,
    'name' => 'ООО "Инженерно-производственная компания "Технологии Энергосбережения"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523506601,
    'updated_at' => 1523506727,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800121,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800121',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448125220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157245',
        'method' => 'get',
      ),
    ),
  ),
  8602178388 => 
  array (
    'id' => 18157291,
    'name' => 'ООО "ЕвроАвтоматика"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523507442,
    'updated_at' => 1523507508,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800161,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800161',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602178388',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800161,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800161',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157291',
        'method' => 'get',
      ),
    ),
  ),
  7017207983 => 
  array (
    'id' => 18157489,
    'name' => 'ООО Недвижимость',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523510295,
    'updated_at' => 1523510480,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017207983',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157489',
        'method' => 'get',
      ),
    ),
  ),
  6317068739 => 
  array (
    'id' => 18157631,
    'name' => 'ООО Экстен Медикал Самара',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523511676,
    'updated_at' => 1523511690,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800491,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800491',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317068739',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800491,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800491',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157631',
        'method' => 'get',
      ),
    ),
  ),
  5609066303 => 
  array (
    'id' => 15028247,
    'name' => 'ООО "Строймац"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1490763901,
    'updated_at' => 1523512327,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29448259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29448259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605884,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5609066303',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29448259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29448259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15028247',
        'method' => 'get',
      ),
    ),
  ),
  7450065949 => 
  array (
    'id' => 18157743,
    'name' => 'ООО Производственная Компания "СпецКомплект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523512789,
    'updated_at' => 1523512789,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800617,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800617',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7450065949',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800617,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800617',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157743',
        'method' => 'get',
      ),
    ),
  ),
  5507109413 => 
  array (
    'id' => 18157749,
    'name' => 'ООО "ИТЦ "ВТ-ПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523512871,
    'updated_at' => 1523512926,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5507109413',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157749',
        'method' => 'get',
      ),
    ),
  ),
  5406276915 => 
  array (
    'id' => 18157773,
    'name' => 'ООО "ИТ СИНТЕЗ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523513052,
    'updated_at' => 1523513061,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800651,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800651',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523566740,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406276915',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800651,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800651',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157773',
        'method' => 'get',
      ),
    ),
  ),
  6658464901 => 
  array (
    'id' => 18158081,
    'name' => 'ООО "Уральский центр шовных материалов"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523514674,
    'updated_at' => 1523514713,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658464901',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18158081',
        'method' => 'get',
      ),
    ),
  ),
  7706218957 => 
  array (
    'id' => 18158565,
    'name' => 'ООО "МЕГА ГРУП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523517292,
    'updated_at' => 1523517417,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33801527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33801527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706218957',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33801527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33801527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18158565',
        'method' => 'get',
      ),
    ),
  ),
  6316135943 => 
  array (
    'id' => 15779285,
    'name' => 'ООО "ДИАМАНТ"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1498626240,
    'updated_at' => 1523520009,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30487337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30487337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316135943',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30487337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30487337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15779285',
        'method' => 'get',
      ),
    ),
  ),
  7731406866 => 
  array (
    'id' => 18159267,
    'name' => 'ООО"ЕКЦ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523520154,
    'updated_at' => 1523520217,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33802197,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802197',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731406866',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33802197,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802197',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18159267',
        'method' => 'get',
      ),
    ),
  ),
  4825041012 => 
  array (
    'id' => 18159391,
    'name' => 'ООО "СтройАвангард"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523520348,
    'updated_at' => 1523520349,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33802237,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802237',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825041012',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33802237,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802237',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18159391',
        'method' => 'get',
      ),
    ),
  ),
  544006738 => 
  array (
    'id' => 18159431,
    'name' => 'ООО "АЛЬТЕР-МЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523520449,
    'updated_at' => 1523520468,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33802265,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802265',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '544006738',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33802265,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802265',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18159431',
        'method' => 'get',
      ),
    ),
  ),
  5190070355 => 
  array (
    'id' => 18160605,
    'name' => 'ООО "СТРОЙПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523523520,
    'updated_at' => 1523523521,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33803279,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33803279',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190070355',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33803279,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33803279',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18160605',
        'method' => 'get',
      ),
    ),
  ),
  5024168770 => 
  array (
    'id' => 18160627,
    'name' => 'ООО "ОНИКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523523618,
    'updated_at' => 1523523759,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33803307,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33803307',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024168770',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33803307,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33803307',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18160627',
        'method' => 'get',
      ),
    ),
  ),
  6670284775 => 
  array (
    'id' => 18157771,
    'name' => 'ООО СК "Анталекс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523513052,
    'updated_at' => 1523525668,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33800649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670284775',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33800649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33800649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18157771',
        'method' => 'get',
      ),
    ),
  ),
  6166095343 => 
  array (
    'id' => 18163331,
    'name' => 'ООО "СОЛОМИР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523534198,
    'updated_at' => 1523534464,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33806289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33806289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6166095343',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33806289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33806289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18163331',
        'method' => 'get',
      ),
    ),
  ),
  6116010746 => 
  array (
    'id' => 18163441,
    'name' => 'ООО "ГАРАНТСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523534781,
    'updated_at' => 1523534806,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33806423,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33806423',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6116010746',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33806423,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33806423',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18163441',
        'method' => 'get',
      ),
    ),
  ),
  6672175034 => 
  array (
    'id' => 18167527,
    'name' => 'ООО "УралДорТехнологии"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523596092,
    'updated_at' => 1523596106,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33810399,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810399',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672175034',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33810399,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810399',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18167527',
        'method' => 'get',
      ),
    ),
  ),
  6320004816 => 
  array (
    'id' => 18167931,
    'name' => 'ООО "Порт Тольятти"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523599922,
    'updated_at' => 1523599952,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33810809,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810809',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6320004816',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33810809,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810809',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18167931',
        'method' => 'get',
      ),
    ),
  ),
  1435244621 => 
  array (
    'id' => 18167971,
    'name' => 'ООО  КОМПЛЕКСНАЯ СТРОИТЕЛЬНАЯ КОМПАНИЯ "МОСТЫ СЕВЕРА"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523600256,
    'updated_at' => 1523600263,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33810853,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810853',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523653140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435244621',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33810853,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810853',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18167971',
        'method' => 'get',
      ),
    ),
  ),
  4632082333 => 
  array (
    'id' => 18153551,
    'name' => 'ООО "Остров"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523451497,
    'updated_at' => 1523601717,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33796427,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33796427',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632082333',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33796427,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33796427',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18153551',
        'method' => 'get',
      ),
    ),
  ),
  7839371697 => 
  array (
    'id' => 18168303,
    'name' => 'АКЦИОНЕРНОЕ ОБЩЕСТВО "СЛУЖБА ЗАКАЗЧИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523602445,
    'updated_at' => 1523602517,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33811169,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33811169',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839371697',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33811169,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33811169',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18168303',
        'method' => 'get',
      ),
    ),
  ),
  4018011160 => 
  array (
    'id' => 18151017,
    'name' => 'ООО "ТЭМПО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523440204,
    'updated_at' => 1523602894,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33793631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33793631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4018011160',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33793631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33793631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18151017',
        'method' => 'get',
      ),
    ),
  ),
  3808034341 => 
  array (
    'id' => 18168421,
    'name' => 'ООО "КОМПАНИЯ КИЛЬ-ИРКУТСК"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523602904,
    'updated_at' => 1523602921,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33811267,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33811267',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523653140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808034341',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33811267,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33811267',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18168421',
        'method' => 'get',
      ),
    ),
  ),
  2346017435 => 
  array (
    'id' => 18168775,
    'name' => 'ООО «Павловское ДЭП»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523604149,
    'updated_at' => 1523604150,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33811573,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33811573',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2346017435',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33811573,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33811573',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18168775',
        'method' => 'get',
      ),
    ),
  ),
  6311151420 => 
  array (
    'id' => 13712544,
    'name' => 'ООО «НетЛайн»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1478152129,
    'updated_at' => 1523604261,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27555156,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27555156',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6311151420',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27555156,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27555156',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13712544',
        'method' => 'get',
      ),
    ),
  ),
  323395758 => 
  array (
    'id' => 18169417,
    'name' => 'ООО "БФК"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523605911,
    'updated_at' => 1523605912,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33812061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323395758',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33812061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18169417',
        'method' => 'get',
      ),
    ),
  ),
  6671052840 => 
  array (
    'id' => 18170155,
    'name' => 'ООО "КРОКУС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523606495,
    'updated_at' => 1523606496,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33812311,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812311',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671052840',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33812311,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812311',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18170155',
        'method' => 'get',
      ),
    ),
  ),
  4823073594 => 
  array (
    'id' => 18171429,
    'name' => 'ООО "СитиСтройСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523607274,
    'updated_at' => 1523607275,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33812551,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812551',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4823073594',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33812551,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812551',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18171429',
        'method' => 'get',
      ),
    ),
  ),
  990199323927 => 
  array (
    'id' => 18171713,
    'name' => 'ИП Канатов Алибек Амангельдиевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523607834,
    'updated_at' => 1523607834,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33812761,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812761',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '990199323927',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33812761,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812761',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18171713',
        'method' => 'get',
      ),
    ),
  ),
  7704700450 => 
  array (
    'id' => 18174867,
    'name' => 'ООО "ПРОЕКТНОЕ БЮРО МЕГАПОЛИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523618246,
    'updated_at' => 1523618247,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33815475,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33815475',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704700450',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33815475,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33815475',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18174867',
        'method' => 'get',
      ),
    ),
  ),
  7731332808 => 
  array (
    'id' => 18152299,
    'name' => '"ЭТАЛОН"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523445740,
    'updated_at' => 1523619714,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33794953,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33794953',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731332808',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33794953,
        1 => 33815795,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33794953,33815795',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18152299',
        'method' => 'get',
      ),
    ),
  ),
  7457006798 => 
  array (
    'id' => 17673785,
    'name' => 'ООО "УРАЛВОДОКАНАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1518153772,
    'updated_at' => 1523855033,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33113603,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33113603',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1518667032,
    'closest_task_at' => 1523912340,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7457006798',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'челябинск',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33113603,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33113603',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17673785',
        'method' => 'get',
      ),
    ),
  ),
  6612039037 => 
  array (
    'id' => 18158817,
    'name' => 'ООО «ЭнергоПерспектива»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523518533,
    'updated_at' => 1523855045,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33801817,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33801817',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6612039037',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33801817,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33801817',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18158817',
        'method' => 'get',
      ),
    ),
  ),
  326495736 => 
  array (
    'id' => 18171391,
    'name' => 'ООО "Легион"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523607125,
    'updated_at' => 1523856768,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33812493,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812493',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326495736',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33812493,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33812493',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18171391',
        'method' => 'get',
      ),
    ),
  ),
  5904024710 => 
  array (
    'id' => 18073327,
    'name' => '"РЕМСТРОЙСНАБ"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1522645817,
    'updated_at' => 1523859966,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33694739,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33694739',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1523347258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904024710',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33694739,
        1 => 33694741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33694739,33694741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18073327',
        'method' => 'get',
      ),
    ),
  ),
  2312266296 => 
  array (
    'id' => 18159461,
    'name' => 'ООО "РЕГИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523520547,
    'updated_at' => 1523860808,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33802297,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802297',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312266296',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33802297,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33802297',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18159461',
        'method' => 'get',
      ),
    ),
  ),
  8602179751 => 
  array (
    'id' => 18042799,
    'name' => 'ООО "ЮграСтройИнвестПроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522298676,
    'updated_at' => 1523861750,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33635357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33635357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1522357140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602179751',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'ханты',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33635357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33635357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18042799',
        'method' => 'get',
      ),
    ),
  ),
  2308129459 => 
  array (
    'id' => 18188363,
    'name' => 'ООО "ЭРБАУЭР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523863136,
    'updated_at' => 1523863137,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33840751,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33840751',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308129459',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33840751,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33840751',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18188363',
        'method' => 'get',
      ),
    ),
  ),
  9901007255 => 
  array (
    'id' => 18188391,
    'name' => 'ООО "ТЭКС стройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523863254,
    'updated_at' => 1523863255,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33840793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33840793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9901007255',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33840793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33840793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18188391',
        'method' => 'get',
      ),
    ),
  ),
  7743227893 => 
  array (
    'id' => 18192497,
    'name' => 'ООО "ФИБОНАЧЧИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523866843,
    'updated_at' => 1523866843,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33843119,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33843119',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743227893',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33843119,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33843119',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18192497',
        'method' => 'get',
      ),
    ),
  ),
  6163157584 => 
  array (
    'id' => 18192511,
    'name' => 'ООО «АЛЬТИУС ПРО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523866898,
    'updated_at' => 1523866898,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33843145,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33843145',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6163157584',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33843145,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33843145',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18192511',
        'method' => 'get',
      ),
    ),
  ),
  1908000166 => 
  array (
    'id' => 18137195,
    'name' => 'ГУП  Республики Хакасия «Орджоникидзевское дорожное ремонтно-строительное управление»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523330776,
    'updated_at' => 1523867123,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523858400,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1908000166',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'плюс 7 хакасия',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137195',
        'method' => 'get',
      ),
    ),
  ),
  9102171869 => 
  array (
    'id' => 18193307,
    'name' => 'ООО "ТЕХНОВЕК"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523868617,
    'updated_at' => 1523868618,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33843771,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33843771',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102171869',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'крым',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33843771,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33843771',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18193307',
        'method' => 'get',
      ),
    ),
  ),
  7841381269 => 
  array (
    'id' => 18117511,
    'name' => 'ооо "Типография Глори"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1523022531,
    'updated_at' => 1523871336,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33753855,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33753855',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33753853,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33753853',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841381269',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'питер',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33753855,
        1 => 33844717,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33753855,33844717',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18117511',
        'method' => 'get',
      ),
    ),
  ),
  7802111710 => 
  array (
    'id' => 18194967,
    'name' => 'ООО "РА "Белые Ночи"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523873777,
    'updated_at' => 1523873778,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33845703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33845703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802111710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33845703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33845703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18194967',
        'method' => 'get',
      ),
    ),
  ),
  7731459508 => 
  array (
    'id' => 18196649,
    'name' => 'ООО "АРХИТЕКТПОДРЯД"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523881187,
    'updated_at' => 1523883382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33848249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33848249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524085140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731459508',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33848249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33848249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18196649',
        'method' => 'get',
      ),
    ),
  ),
  7813370716 => 
  array (
    'id' => 18188449,
    'name' => 'ООО "Меандр"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523863510,
    'updated_at' => 1523883502,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33840857,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33840857',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524085140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813370716',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33840857,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33840857',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18188449',
        'method' => 'get',
      ),
    ),
  ),
  7725807926 => 
  array (
    'id' => 18197237,
    'name' => 'ООО  "СТРОЙ-СЕРВИС"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523884232,
    'updated_at' => 1523884314,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33849227,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33849227',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524049200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725807926',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33849227,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33849227',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18197237',
        'method' => 'get',
      ),
    ),
  ),
  7801570911 => 
  array (
    'id' => 18197387,
    'name' => 'АО "КОМБИНАТ СОЦИАЛЬНОГО ПИТАНИЯ ВАСИЛЕОСТРОВСКОГО РАЙОНА"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523884899,
    'updated_at' => 1523885921,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33849463,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33849463',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1523998740,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801570911',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33849463,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33849463',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18197387',
        'method' => 'get',
      ),
    ),
  ),
  2721203917 => 
  array (
    'id' => 17981635,
    'name' => 'СТРОИТЕЛЬНО-МОНТАЖНАЯ КОМПАНИЯ "ВОСТОЧНЫЙ ПОЛЮС"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1521630345,
    'updated_at' => 1523936736,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33552389,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33552389',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524195000,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721203917',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'хабаровск плюс 10',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33552389,
        1 => 33615247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33552389,33615247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17981635',
        'method' => 'get',
      ),
    ),
  ),
  6123016763 => 
  array (
    'id' => 18212167,
    'name' => 'ООО "Промкомплект-Юг"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523967892,
    'updated_at' => 1523967893,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33869393,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33869393',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6123016763',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33869393,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33869393',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18212167',
        'method' => 'get',
      ),
    ),
  ),
  2463083745 => 
  array (
    'id' => 18200837,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ КОМПАНИЯ "АРКАДА 21"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523939992,
    'updated_at' => 1523940054,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33859037,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859037',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463083745',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33859037,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859037',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18200837',
        'method' => 'get',
      ),
    ),
  ),
  2411026697 => 
  array (
    'id' => 18200911,
    'name' => 'ООО "КСИЛ-ЕНИСЕЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523941162,
    'updated_at' => 1523941201,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33859255,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859255',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2411026697',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33859255,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859255',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18200911',
        'method' => 'get',
      ),
    ),
  ),
  4703150279 => 
  array (
    'id' => 18200915,
    'name' => 'ООО «ЭФКЕЙ-рампс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523941402,
    'updated_at' => 1523941402,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33859309,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859309',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4703150279',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33859309,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859309',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18200915',
        'method' => 'get',
      ),
    ),
  ),
  5904292998 => 
  array (
    'id' => 14866489,
    'name' => 'ООО "Феникс-Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1489147297,
    'updated_at' => 1523941913,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29226577,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29226577',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921316,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904292998',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пермский край(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29226577,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29226577',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14866489',
        'method' => 'get',
      ),
    ),
  ),
  6315656171 => 
  array (
    'id' => 18201005,
    'name' => 'ООО "НИЛЬС-ВОСТОК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523942482,
    'updated_at' => 1523942482,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33859539,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859539',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6315656171',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33859539,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859539',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18201005',
        'method' => 'get',
      ),
    ),
  ),
  2543061240 => 
  array (
    'id' => 15381193,
    'name' => 'ООО "ЦЕНТР ГЕОДЕЗИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1494564713,
    'updated_at' => 1523942761,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29924871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29924871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921292,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2543061240',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Приморский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29924871,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29924871',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15381193',
        'method' => 'get',
      ),
    ),
  ),
  3811169980 => 
  array (
    'id' => 18201051,
    'name' => 'ООО "ЭНЕРГОСЕРВЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523942872,
    'updated_at' => 1523943010,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33859613,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859613',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811169980',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33859613,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859613',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18201051',
        'method' => 'get',
      ),
    ),
  ),
  5404487710 => 
  array (
    'id' => 18200873,
    'name' => 'ООО СК "ЮНИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523940489,
    'updated_at' => 1523944583,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33859125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404487710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33859125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18200873',
        'method' => 'get',
      ),
    ),
  ),
  7706758198 => 
  array (
    'id' => 18201345,
    'name' => 'ООО  "ВЕСТА"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523945760,
    'updated_at' => 1523945761,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33860381,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33860381',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706758198',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33860381,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33860381',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18201345',
        'method' => 'get',
      ),
    ),
  ),
  2464141510 => 
  array (
    'id' => 18200713,
    'name' => 'ООО "Суббота"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523938278,
    'updated_at' => 1523950156,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33858695,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33858695',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464141510',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33858695,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33858695',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18200713',
        'method' => 'get',
      ),
    ),
  ),
  270405978 => 
  array (
    'id' => 18153113,
    'name' => 'ООО "РТК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523449457,
    'updated_at' => 1523950302,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33795907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '270405978',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33795907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18153113',
        'method' => 'get',
      ),
    ),
  ),
  1831167699 => 
  array (
    'id' => 18167587,
    'name' => 'ООО "Комбинат Благоустройства"Первомайский"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523596808,
    'updated_at' => 1523950339,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33810449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1831167699',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33810449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33810449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18167587',
        'method' => 'get',
      ),
    ),
  ),
  571010846 => 
  array (
    'id' => 18202307,
    'name' => 'ООО «САТУРН»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523950611,
    'updated_at' => 1523950612,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33862095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33862095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '571010846',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33862095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33862095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18202307',
        'method' => 'get',
      ),
    ),
  ),
  6658131074 => 
  array (
    'id' => 15709895,
    'name' => 'ООО "КЗТС-Дормаш"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1497864070,
    'updated_at' => 1523950660,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30377057,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30377057',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658131074',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30377057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30377057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15709895',
        'method' => 'get',
      ),
    ),
  ),
  7705954400 => 
  array (
    'id' => 17859019,
    'name' => 'ООО "ПРОМЭКОВОД"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1520229233,
    'updated_at' => 1523953537,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33378403,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33378403',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33433741,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33433741',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1521445523,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705954400',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'москва',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33378403,
        1 => 33863135,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33378403,33863135',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17859019',
        'method' => 'get',
      ),
    ),
  ),
  5402032523 => 
  array (
    'id' => 18203023,
    'name' => 'ООО "М-ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523953750,
    'updated_at' => 1523953751,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33863245,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863245',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402032523',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33863245,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863245',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18203023',
        'method' => 'get',
      ),
    ),
  ),
  7715862759 => 
  array (
    'id' => 18203043,
    'name' => 'ООО ЧОО «Дельта»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523953855,
    'updated_at' => 1523953856,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33863315,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863315',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715862759',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33863315,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863315',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18203043',
        'method' => 'get',
      ),
    ),
  ),
  7816644482 => 
  array (
    'id' => 18203079,
    'name' => 'ООО «СТИЛЬ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523954014,
    'updated_at' => 1523954033,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33863345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816644482',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33863345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18203079',
        'method' => 'get',
      ),
    ),
  ),
  7727779375 => 
  array (
    'id' => 18204709,
    'name' => 'ООО «Подводная робототехника»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523959446,
    'updated_at' => 1523959516,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33865747,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33865747',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727779375',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33865747,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33865747',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18204709',
        'method' => 'get',
      ),
    ),
  ),
  7714953210 => 
  array (
    'id' => 18210569,
    'name' => 'ООО Частная охранная организация "Казачья стража"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523961643,
    'updated_at' => 1523961698,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33866681,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33866681',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714953210',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33866681,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33866681',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18210569',
        'method' => 'get',
      ),
    ),
  ),
  7707280130 => 
  array (
    'id' => 18210605,
    'name' => 'АКЦИОНЕРНОЕ ОБЩЕСТВО "ВНЕШНЕЭКОНОМИЧЕСКОЕ ОБЪЕДИНЕНИЕ "СУДОИМПОРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523961781,
    'updated_at' => 1523961782,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33866713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33866713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707280130',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33866713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33866713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18210605',
        'method' => 'get',
      ),
    ),
  ),
  5031118315 => 
  array (
    'id' => 18210675,
    'name' => 'ООО "МедПолитех"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523962071,
    'updated_at' => 1523962080,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33866851,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33866851',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5031118315',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33866851,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33866851',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18210675',
        'method' => 'get',
      ),
    ),
  ),
  7736585961 => 
  array (
    'id' => 18210967,
    'name' => 'ООО ЧАСТНОЕ ОХРАННОЕ ПРЕДПРИЯТИЕ "ОЛИМП-К"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523963207,
    'updated_at' => 1523963226,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33867343,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867343',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736585961',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33867343,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867343',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18210967',
        'method' => 'get',
      ),
    ),
  ),
  7708201941 => 
  array (
    'id' => 18211003,
    'name' => 'ООО «АЭРОЛОДЖИК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523963307,
    'updated_at' => 1523963307,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33867373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708201941',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33867373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18211003',
        'method' => 'get',
      ),
    ),
  ),
  7735588279 => 
  array (
    'id' => 18211059,
    'name' => 'ООО "ЦЕНТР ОХРАНЫ И УСЛОВИЙ ТРУДА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523963567,
    'updated_at' => 1523963567,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33867489,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867489',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735588279',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33867489,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867489',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18211059',
        'method' => 'get',
      ),
    ),
  ),
  5012060130 => 
  array (
    'id' => 18211205,
    'name' => 'ООО "ВАЛБЕРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523964099,
    'updated_at' => 1523964100,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33867741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5012060130',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33867741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18211205',
        'method' => 'get',
      ),
    ),
  ),
  7716202018 => 
  array (
    'id' => 18211853,
    'name' => 'ООО "Ситек Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523966537,
    'updated_at' => 1523966538,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33868833,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868833',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716202018',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33868833,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868833',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18211853',
        'method' => 'get',
      ),
    ),
  ),
  6165000211 => 
  array (
    'id' => 18211901,
    'name' => 'АО  "РОСТОВГРАЖДАНПРОЕКТ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523966764,
    'updated_at' => 1523966764,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33868933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165000211',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33868933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18211901',
        'method' => 'get',
      ),
    ),
  ),
  7743043670 => 
  array (
    'id' => 18211931,
    'name' => 'ООО "МАНСТАР II"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523966868,
    'updated_at' => 1523966994,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33868973,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868973',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743043670',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33868973,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868973',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18211931',
        'method' => 'get',
      ),
    ),
  ),
  5001110598 => 
  array (
    'id' => 18212141,
    'name' => 'ООО "ЖИЛСЕРВИС -БАЛАШИХА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523967799,
    'updated_at' => 1523967853,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33869347,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33869347',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5001110598',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33869347,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33869347',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18212141',
        'method' => 'get',
      ),
    ),
  ),
  6324048229 => 
  array (
    'id' => 18211683,
    'name' => 'ООО "ПРИОРИТЕТ ТОЛЬЯТТИ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1523965843,
    'updated_at' => 1523968531,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33868515,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868515',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6324048229',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+1',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33868515,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33868515',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18211683',
        'method' => 'get',
      ),
    ),
  ),
  2463242025 => 
  array (
    'id' => 18210909,
    'name' => 'ЕРСМ Сибири, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523962974,
    'updated_at' => 1524024105,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33867249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463242025',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33867249,
        1 => 33882017,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33867249,33882017',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18210909',
        'method' => 'get',
      ),
    ),
  ),
  7448203944 => 
  array (
    'id' => 18215961,
    'name' => 'ООО Промзапчасть',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524025249,
    'updated_at' => 1524025249,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448203944',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18215961',
        'method' => 'get',
      ),
    ),
  ),
  8602099841 => 
  array (
    'id' => 18216007,
    'name' => 'АО Авиакомпания "АРГО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524026120,
    'updated_at' => 1524026120,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882389,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882389',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602099841',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882389,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882389',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216007',
        'method' => 'get',
      ),
    ),
  ),
  2703053867 => 
  array (
    'id' => 18216025,
    'name' => 'ООО "РУСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524026411,
    'updated_at' => 1524026425,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882439,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882439',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2703053867',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882439,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882439',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216025',
        'method' => 'get',
      ),
    ),
  ),
  2724194308 => 
  array (
    'id' => 18111461,
    'name' => 'ООО «Стройиндустрия»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522987899,
    'updated_at' => 1524026562,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33747193,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33747193',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2724194308',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33747193,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33747193',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18111461',
        'method' => 'get',
      ),
    ),
  ),
  8910003255 => 
  array (
    'id' => 18216117,
    'name' => 'ООО Гыданское строительно-монтажное предприятие "Строитель"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524028029,
    'updated_at' => 1524028039,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524085140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8910003255',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216117',
        'method' => 'get',
      ),
    ),
  ),
  594806419620 => 
  array (
    'id' => 18216135,
    'name' => 'ИП Давтян Тигран Мартикович',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524028143,
    'updated_at' => 1524028143,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882733,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882733',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '594806419620',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882733,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882733',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216135',
        'method' => 'get',
      ),
    ),
  ),
  8904066930 => 
  array (
    'id' => 18216213,
    'name' => 'ООО "Импульс"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524028985,
    'updated_at' => 1524029022,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882917,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882917',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524457800,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8904066930',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882917,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882917',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216213',
        'method' => 'get',
      ),
    ),
  ),
  8604032987 => 
  array (
    'id' => 18216273,
    'name' => 'НЕФТЕЮГАНСКОЕ ГОРОДСКОЕ МУНИЦИПАЛЬНОЕ УНИТАРНОЕ ПРЕДПРИЯТИЕ "УНИВЕРСАЛ СЕРВИС"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524029675,
    'updated_at' => 1524029696,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33883073,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883073',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524085140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8604032987',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33883073,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883073',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216273',
        'method' => 'get',
      ),
    ),
  ),
  5009108819 => 
  array (
    'id' => 18216785,
    'name' => 'ООО "БЛАГОСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524032268,
    'updated_at' => 1524032268,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33883773,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883773',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5009108819',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33883773,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883773',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216785',
        'method' => 'get',
      ),
    ),
  ),
  7725631334 => 
  array (
    'id' => 18083027,
    'name' => 'ООО "ИМПРЕЗА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522732560,
    'updated_at' => 1524034525,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33705013,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33705013',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725631334',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33705013,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33705013',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18083027',
        'method' => 'get',
      ),
    ),
  ),
  3849067674 => 
  array (
    'id' => 18215997,
    'name' => 'АО "СПЕЦАВТОХОЗЯЙСТВО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524025929,
    'updated_at' => 1524035067,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3849067674',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18215997',
        'method' => 'get',
      ),
    ),
  ),
  6623075622 => 
  array (
    'id' => 18217407,
    'name' => 'ООО «МС-ГРУП»',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524035482,
    'updated_at' => 1524035496,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33884867,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33884867',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524085140,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6623075622',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33884867,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33884867',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18217407',
        'method' => 'get',
      ),
    ),
  ),
  '0264073474' => 
  array (
    'id' => 16924183,
    'name' => 'ООО "МЕБЕЛЬРЕСУРС-РЕГИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510566913,
    'updated_at' => 1524035522,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32153869,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32153869',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0264073474',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Башкортостан(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32153869,
        1 => 32154617,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32153869,32154617',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16924183',
        'method' => 'get',
      ),
    ),
  ),
  7722409370 => 
  array (
    'id' => 18217431,
    'name' => 'ООО "ФРЕШЛОДЖИСТИКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524035571,
    'updated_at' => 1524035627,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33884885,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33884885',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722409370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33884885,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33884885',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18217431',
        'method' => 'get',
      ),
    ),
  ),
  507272320910 => 
  array (
    'id' => 18217531,
    'name' => 'ИП Кусов Владимир Иванович',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524035955,
    'updated_at' => 1524035956,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33885023,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885023',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '507272320910',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33885023,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885023',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18217531',
        'method' => 'get',
      ),
    ),
  ),
  5050117620 => 
  array (
    'id' => 17532727,
    'name' => 'ООО "СК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1516948816,
    'updated_at' => 1524036403,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32956567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32956567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1516948824,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5050117620',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32956567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32956567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17532727',
        'method' => 'get',
      ),
    ),
  ),
  7713632206 => 
  array (
    'id' => 18217713,
    'name' => 'ООО "Строй Эксперт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524036630,
    'updated_at' => 1524036730,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33885319,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885319',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713632206',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33885319,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885319',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18217713',
        'method' => 'get',
      ),
    ),
  ),
  7811138080 => 
  array (
    'id' => 16986775,
    'name' => 'ООО "ФОРА КМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511159434,
    'updated_at' => 1524036852,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32235763,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235763',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811138080',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32235763,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32235763',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16986775',
        'method' => 'get',
      ),
    ),
  ),
  7720373520 => 
  array (
    'id' => 18217871,
    'name' => 'ООО "Блэк Экспо"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524037222,
    'updated_at' => 1524037223,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33885527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720373520',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33885527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18217871',
        'method' => 'get',
      ),
    ),
  ),
  5032234459 => 
  array (
    'id' => 18217891,
    'name' => 'ООО "КРЭС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524037278,
    'updated_at' => 1524037279,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33885535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032234459',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33885535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18217891',
        'method' => 'get',
      ),
    ),
  ),
  7451404344 => 
  array (
    'id' => 18218093,
    'name' => 'ООО "КУРОРТ "КИСЕГАЧ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524038009,
    'updated_at' => 1524038026,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33885831,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885831',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451404344',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33885831,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33885831',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18218093',
        'method' => 'get',
      ),
    ),
  ),
  7751089291 => 
  array (
    'id' => 18218217,
    'name' => 'ООО "ТКР-РЕСУРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524038532,
    'updated_at' => 1524038532,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33886035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751089291',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33886035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18218217',
        'method' => 'get',
      ),
    ),
  ),
  3906353917 => 
  array (
    'id' => 18218225,
    'name' => 'ООО «Эко-Баланс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524038620,
    'updated_at' => 1524038621,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33886045,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886045',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906353917',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33886045,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886045',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18218225',
        'method' => 'get',
      ),
    ),
  ),
  7415086103 => 
  array (
    'id' => 18218261,
    'name' => 'ООО "Демидов"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524038744,
    'updated_at' => 1524038744,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33886125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7415086103',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33886125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18218261',
        'method' => 'get',
      ),
    ),
  ),
  7728372864 => 
  array (
    'id' => 18218445,
    'name' => 'ООО «Квадраком»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524039528,
    'updated_at' => 1524039710,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33886415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728372864',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33886415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18218445',
        'method' => 'get',
      ),
    ),
  ),
  7810391620 => 
  array (
    'id' => 18218891,
    'name' => 'ООО "Судоходная компания "ЭКОТЭК"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524041163,
    'updated_at' => 1524041163,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33886967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810391620',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33886967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33886967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18218891',
        'method' => 'get',
      ),
    ),
  ),
  4205316002 => 
  array (
    'id' => 18216089,
    'name' => 'ООО "Вектор"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1524027603,
    'updated_at' => 1524111449,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205316002',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216089',
        'method' => 'get',
      ),
    ),
  ),
  2983012317 => 
  array (
    'id' => 18219799,
    'name' => 'ООО «Ненецкая фармация»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524044699,
    'updated_at' => 1524044761,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33887867,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33887867',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2983012317',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33887867,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33887867',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18219799',
        'method' => 'get',
      ),
    ),
  ),
  7751139658 => 
  array (
    'id' => 18219921,
    'name' => 'ООО "Сервистон"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524045331,
    'updated_at' => 1524045332,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33888021,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33888021',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751139658',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33888021,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33888021',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18219921',
        'method' => 'get',
      ),
    ),
  ),
  2635232872 => 
  array (
    'id' => 18220125,
    'name' => 'ООО "ЮГ-ОПТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524046158,
    'updated_at' => 1524046159,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33888227,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33888227',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635232872',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33888227,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33888227',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18220125',
        'method' => 'get',
      ),
    ),
  ),
  6113022447 => 
  array (
    'id' => 15540491,
    'name' => 'ООО "ТЕМА-ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1496214900,
    'updated_at' => 1524046730,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30146679,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146679',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921282,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6113022447',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ростовская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30146679,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30146679',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15540491',
        'method' => 'get',
      ),
    ),
  ),
  6318008274 => 
  array (
    'id' => 17902563,
    'name' => 'ООО "МАХОВИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1520826013,
    'updated_at' => 1524048434,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33431673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33431673',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318008274',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'самара',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33431673,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33431673',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17902563',
        'method' => 'get',
      ),
    ),
  ),
  4813012546 => 
  array (
    'id' => 18220823,
    'name' => 'ООО "Полигон"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524048723,
    'updated_at' => 1524048724,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33888929,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33888929',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4813012546',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33888929,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33888929',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18220823',
        'method' => 'get',
      ),
    ),
  ),
  3906978193 => 
  array (
    'id' => 18220949,
    'name' => 'ООО «Проф-Стайл»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524049069,
    'updated_at' => 1524049069,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33889031,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889031',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906978193',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33889031,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889031',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18220949',
        'method' => 'get',
      ),
    ),
  ),
  7720581760 => 
  array (
    'id' => 18221039,
    'name' => 'ООО "ПРОЕКТ АЛИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524049444,
    'updated_at' => 1524049444,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33889139,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889139',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720581760',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33889139,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889139',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18221039',
        'method' => 'get',
      ),
    ),
  ),
  6950150478 => 
  array (
    'id' => 18221195,
    'name' => 'ООО "ПромСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524050086,
    'updated_at' => 1524050136,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33889305,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889305',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6950150478',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33889305,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889305',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18221195',
        'method' => 'get',
      ),
    ),
  ),
  3629007782 => 
  array (
    'id' => 18230369,
    'name' => 'ООО "Дорожник"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524133843,
    'updated_at' => 1524133880,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33907309,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907309',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524257940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3629007782',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33907309,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907309',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18230369',
        'method' => 'get',
      ),
    ),
  ),
  5047152953 => 
  array (
    'id' => 10511245,
    'name' => 'ООО Вартеск',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1453367119,
    'updated_at' => 1524052644,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 22958527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=22958527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 22958525,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=22958525',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500606258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047152953',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 22958527,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=22958527',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=10511245',
        'method' => 'get',
      ),
    ),
  ),
  7814565595 => 
  array (
    'id' => 18221829,
    'name' => 'ООО "ГТО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524052985,
    'updated_at' => 1524053185,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33890009,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33890009',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814565595',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33890009,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33890009',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18221829',
        'method' => 'get',
      ),
    ),
  ),
  '0245952720' => 
  array (
    'id' => 15528753,
    'name' => 'ООО Авиакомпания "РУСАВИА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1496120429,
    'updated_at' => 1524112022,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30130357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30130357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382025,
    'closest_task_at' => 1521552600,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0245952720',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30130357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30130357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15528753',
        'method' => 'get',
      ),
    ),
  ),
  2720051122 => 
  array (
    'id' => 16105691,
    'name' => 'ООО "КОНГЛОМЕРАТ 27"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1502254956,
    'updated_at' => 1524112141,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31152345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1503892497,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2720051122',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31152345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16105691',
        'method' => 'get',
      ),
    ),
  ),
  8911001807 => 
  array (
    'id' => 18225971,
    'name' => 'ООО «Толькинская лесоперерабатывающая компания»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524114397,
    'updated_at' => 1524114398,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33902701,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33902701',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8911001807',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33902701,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33902701',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18225971',
        'method' => 'get',
      ),
    ),
  ),
  9204566009 => 
  array (
    'id' => 17452647,
    'name' => 'ООО научно-производственное объединение "СМСил"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1516170930,
    'updated_at' => 1524114940,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32814599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32814599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9204566009',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32814599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32814599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17452647',
        'method' => 'get',
      ),
    ),
  ),
  6949003535 => 
  array (
    'id' => 14198026,
    'name' => 'Акционерное общество "Рамешковское дорожное ремонтно-строительное управление"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1482143522,
    'updated_at' => 1524115712,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28311174,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28311174',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500606151,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6949003535',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28311174,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28311174',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14198026',
        'method' => 'get',
      ),
    ),
  ),
  7735116621 => 
  array (
    'id' => 18226237,
    'name' => 'ООО "СИТРОНИКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524116652,
    'updated_at' => 1524116653,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33902899,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33902899',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735116621',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33902899,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33902899',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18226237',
        'method' => 'get',
      ),
    ),
  ),
  7730703471 => 
  array (
    'id' => 15307743,
    'name' => 'ООО "КУЛЬТРЕСТАВРАЦИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1493713113,
    'updated_at' => 1524117050,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29837581,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29837581',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730703471',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29837581,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29837581',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15307743',
        'method' => 'get',
      ),
    ),
  ),
  7802849730 => 
  array (
    'id' => 18226299,
    'name' => 'ООО "ЭНЕРГОСЕРВИСПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524117252,
    'updated_at' => 1524117252,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33902967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33902967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802849730',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33902967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33902967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18226299',
        'method' => 'get',
      ),
    ),
  ),
  3812060986 => 
  array (
    'id' => 18226541,
    'name' => 'ООО Луч-Байкал',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524118858,
    'updated_at' => 1524118858,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33903233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812060986',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33903233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18226541',
        'method' => 'get',
      ),
    ),
  ),
  7802589471 => 
  array (
    'id' => 18226615,
    'name' => 'ООО «Гранд Медикал»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524119338,
    'updated_at' => 1524119428,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33903305,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903305',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802589471',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33903305,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903305',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18226615',
        'method' => 'get',
      ),
    ),
  ),
  7802553901 => 
  array (
    'id' => 18226639,
    'name' => 'ООО "СеверТранс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524119465,
    'updated_at' => 1524119465,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33903327,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903327',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802553901',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33903327,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903327',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18226639',
        'method' => 'get',
      ),
    ),
  ),
  7715771195 => 
  array (
    'id' => 18226931,
    'name' => 'ООО "РЕМОНТНОЕ ЭКСПЛУАТАЦИОННОЕ УПРАВЛЕНИЕ "СТОЛИЦА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524121157,
    'updated_at' => 1524121157,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33903661,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903661',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715771195',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33903661,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903661',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18226931',
        'method' => 'get',
      ),
    ),
  ),
  7313009413 => 
  array (
    'id' => 18226953,
    'name' => 'ООО "Кузоватовское ДРСУ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524121305,
    'updated_at' => 1524121339,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33903685,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903685',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7313009413',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33903685,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33903685',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18226953',
        'method' => 'get',
      ),
    ),
  ),
  2222849825 => 
  array (
    'id' => 18096419,
    'name' => 'ООО «БИМ ИНЖИНИРИНГ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522830022,
    'updated_at' => 1524123670,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33718375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33718375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524123670,
    'closest_task_at' => 1523998740,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2222849825',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33718375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33718375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18096419',
        'method' => 'get',
      ),
    ),
  ),
  7724888305 => 
  array (
    'id' => 18129171,
    'name' => 'СИС, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523264160,
    'updated_at' => 1524125077,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33764597,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33764597',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724888305',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33764597,
        1 => 33904945,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33764597,33904945',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18129171',
        'method' => 'get',
      ),
    ),
  ),
  5075369605 => 
  array (
    'id' => 18221401,
    'name' => 'ООО "Рузское Подворье"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524051076,
    'updated_at' => 1524126261,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33889549,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889549',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5075369605',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33889549,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33889549',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18221401',
        'method' => 'get',
      ),
    ),
  ),
  1659186550 => 
  array (
    'id' => 18201205,
    'name' => 'ООО "ЭРФОРМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523944446,
    'updated_at' => 1524127092,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33860001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33860001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659186550',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33860001,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33860001',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18201205',
        'method' => 'get',
      ),
    ),
  ),
  6625017048 => 
  array (
    'id' => 18229447,
    'name' => 'ООО "Темир-Текс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524129793,
    'updated_at' => 1524129978,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33906215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33906215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6625017048',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33906215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33906215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18229447',
        'method' => 'get',
      ),
    ),
  ),
  7733320685 => 
  array (
    'id' => 18097351,
    'name' => 'РАСУЛОВ РУСЛАН ВАЛЕХОВИЧ',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1522833994,
    'updated_at' => 1524131672,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33719519,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33719519',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733320685',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33719519,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33719519',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18097351',
        'method' => 'get',
      ),
    ),
  ),
  7702684770 => 
  array (
    'id' => 18204761,
    'name' => 'ООО "НАГРАДЫ ОРДЕНА МЕДАЛИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523959708,
    'updated_at' => 1524132132,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33865843,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33865843',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702684770',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33865843,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33865843',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18204761',
        'method' => 'get',
      ),
    ),
  ),
  7715372874 => 
  array (
    'id' => 18215925,
    'name' => 'ООО "НПО СПЕЦТЕХНИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524024546,
    'updated_at' => 1524134305,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33882071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715372874',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33882071,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33882071',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18215925',
        'method' => 'get',
      ),
    ),
  ),
  2457071780 => 
  array (
    'id' => 18230499,
    'name' => 'ООО "СеверСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524134412,
    'updated_at' => 1524134413,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33907435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2457071780',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33907435,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907435',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18230499',
        'method' => 'get',
      ),
    ),
  ),
  7724889718 => 
  array (
    'id' => 18230671,
    'name' => 'ООО «Фармконтракт сервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524135140,
    'updated_at' => 1524135140,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33907603,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907603',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724889718',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33907603,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907603',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18230671',
        'method' => 'get',
      ),
    ),
  ),
  3525337553 => 
  array (
    'id' => 18230813,
    'name' => 'ООО "ЭталонСтрой"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524135898,
    'updated_at' => 1524135909,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33907745,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907745',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524257940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525337553',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33907745,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907745',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18230813',
        'method' => 'get',
      ),
    ),
  ),
  2130149703 => 
  array (
    'id' => 18231517,
    'name' => 'ООО "СК "ШЫГЫРДАНЫ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524139108,
    'updated_at' => 1524139120,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33908489,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33908489',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524257940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130149703',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33908489,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33908489',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18231517',
        'method' => 'get',
      ),
    ),
  ),
  7508007217 => 
  array (
    'id' => 18231927,
    'name' => 'ООО "Забайкальская Строительная Компания"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524141106,
    'updated_at' => 1524141140,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33908935,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33908935',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524257940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7508007217',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33908935,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33908935',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18231927',
        'method' => 'get',
      ),
    ),
  ),
  7728797828 => 
  array (
    'id' => 15762701,
    'name' => 'ООО "ИКИЗ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498462044,
    'updated_at' => 1524141818,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30461737,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30461737',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524141818,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728797828',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30461737,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30461737',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15762701',
        'method' => 'get',
      ),
    ),
  ),
  5903004541 => 
  array (
    'id' => 18232129,
    'name' => 'АО "ПЗСП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524141858,
    'updated_at' => 1524141902,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33909121,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33909121',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903004541',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33909121,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33909121',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18232129',
        'method' => 'get',
      ),
    ),
  ),
  7716010958 => 
  array (
    'id' => 18232477,
    'name' => 'ООО Научно-производственное предприятие "АРМОКОМ-ЦЕНТР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524143558,
    'updated_at' => 1524144056,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33909541,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33909541',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716010958',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33909541,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33909541',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18232477',
        'method' => 'get',
      ),
    ),
  ),
  6501174263 => 
  array (
    'id' => 18216515,
    'name' => 'ООО "Сахалинское монтажно-эксплуатационное предприятие"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524030681,
    'updated_at' => 1524196007,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33883345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524196007,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501174263',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'плюс 10',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33883345,
        1 => 33912077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883345,33912077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216515',
        'method' => 'get',
      ),
    ),
  ),
  3810328927 => 
  array (
    'id' => 18235485,
    'name' => 'ООО "МЕГАСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524202580,
    'updated_at' => 1524202580,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912457,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912457',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3810328927',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912457,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912457',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235485',
        'method' => 'get',
      ),
    ),
  ),
  7606113340 => 
  array (
    'id' => 18235495,
    'name' => 'ООО "Яртехноинвест"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524202645,
    'updated_at' => 1524202697,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912465,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912465',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7606113340',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912465,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912465',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235495',
        'method' => 'get',
      ),
    ),
  ),
  7734702077 => 
  array (
    'id' => 18230589,
    'name' => 'ООО "Медицина и ядерные технологии"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524134714,
    'updated_at' => 1524202836,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33907517,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907517',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734702077',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33907517,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33907517',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18230589',
        'method' => 'get',
      ),
    ),
  ),
  2623029058 => 
  array (
    'id' => 18235525,
    'name' => 'ООО "ГУАНДУН СОХУ ТЕХНОЛОДЖИ ЛТД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524202899,
    'updated_at' => 1524202900,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912501,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912501',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2623029058',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912501,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912501',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235525',
        'method' => 'get',
      ),
    ),
  ),
  8901034899 => 
  array (
    'id' => 17532137,
    'name' => 'ООО "Путь"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1516941152,
    'updated_at' => 1524203651,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32955651,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32955651',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1516941378,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8901034899',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32955651,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32955651',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17532137',
        'method' => 'get',
      ),
    ),
  ),
  7720314807 => 
  array (
    'id' => 18235803,
    'name' => 'ООО «АВОКАДО24»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524205377,
    'updated_at' => 1524205378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912827,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912827',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720314807',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912827,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912827',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235803',
        'method' => 'get',
      ),
    ),
  ),
  7804606556 => 
  array (
    'id' => 18235825,
    'name' => 'ООО "ГП СПЕЦОБОРОНА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524205561,
    'updated_at' => 1524205562,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912843,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912843',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804606556',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912843,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912843',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235825',
        'method' => 'get',
      ),
    ),
  ),
  5024180640 => 
  array (
    'id' => 18235867,
    'name' => 'ООО «Агентство «Уникальный охват»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524205750,
    'updated_at' => 1524205751,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912883,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912883',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024180640',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912883,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912883',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235867',
        'method' => 'get',
      ),
    ),
  ),
  7719470216 => 
  array (
    'id' => 18235871,
    'name' => 'ООО «ЕВРОМЕД»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524205822,
    'updated_at' => 1524205822,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912885,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912885',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719470216',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912885,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912885',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235871',
        'method' => 'get',
      ),
    ),
  ),
  5259015606 => 
  array (
    'id' => 18235895,
    'name' => 'ООО "Профиль-НН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524205973,
    'updated_at' => 1524206037,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5259015606',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235895',
        'method' => 'get',
      ),
    ),
  ),
  8602094280 => 
  array (
    'id' => 18236189,
    'name' => 'ООО "Сибстройсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524207807,
    'updated_at' => 1524207835,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33913233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602094280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33913233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18236189',
        'method' => 'get',
      ),
    ),
  ),
  7724360586 => 
  array (
    'id' => 18236207,
    'name' => 'ООО "КомплексСтройМонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524207917,
    'updated_at' => 1524207919,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33913249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724360586',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33913249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18236207',
        'method' => 'get',
      ),
    ),
  ),
  2312107031 => 
  array (
    'id' => 18236217,
    'name' => 'ООО "ЮГЭНЕРГОТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524207993,
    'updated_at' => 1524208096,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33913263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312107031',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33913263,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913263',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18236217',
        'method' => 'get',
      ),
    ),
  ),
  2308213407 => 
  array (
    'id' => 18236253,
    'name' => 'ООО "ЧАСТНАЯ ОХРАННАЯ ОРГАНИЗАЦИЯ"СПЕЦНАЗ-А"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524208192,
    'updated_at' => 1524208266,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33913303,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913303',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308213407',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33913303,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913303',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18236253',
        'method' => 'get',
      ),
    ),
  ),
  9729080890 => 
  array (
    'id' => 16946253,
    'name' => 'ООО "АЛТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1510743858,
    'updated_at' => 1524208460,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32195829,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195829',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1514265929,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9729080890',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32195829,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32195829',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16946253',
        'method' => 'get',
      ),
    ),
  ),
  6345024719 => 
  array (
    'id' => 18236331,
    'name' => 'ООО"ТРАСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524208522,
    'updated_at' => 1524208523,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33913375,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913375',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6345024719',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33913375,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913375',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18236331',
        'method' => 'get',
      ),
    ),
  ),
  7720150771 => 
  array (
    'id' => 18236521,
    'name' => 'АО "ПИК ПРОГРЕСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524209377,
    'updated_at' => 1524209377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33913591,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913591',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720150771',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33913591,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913591',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18236521',
        'method' => 'get',
      ),
    ),
  ),
  6234131257 => 
  array (
    'id' => 18236845,
    'name' => 'ООО "ЭЛЕКТРОСТРОЙМОНТАЖ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524210980,
    'updated_at' => 1524210981,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33913949,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913949',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6234131257',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33913949,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33913949',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18236845',
        'method' => 'get',
      ),
    ),
  ),
  5012070241 => 
  array (
    'id' => 18238755,
    'name' => 'ООО "Инфопроф-ИT"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524217394,
    'updated_at' => 1524217395,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33915691,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33915691',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5012070241',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33915691,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33915691',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18238755',
        'method' => 'get',
      ),
    ),
  ),
  7733799250 => 
  array (
    'id' => 18239153,
    'name' => 'ООО "Ситек Мед"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524219219,
    'updated_at' => 1524219220,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33916123,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916123',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733799250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33916123,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916123',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18239153',
        'method' => 'get',
      ),
    ),
  ),
  7705551698 => 
  array (
    'id' => 16190991,
    'name' => 'ООО «Анкор»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1503300471,
    'updated_at' => 1524220319,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31261289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31261289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524220319,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705551698',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31261289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31261289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16190991',
        'method' => 'get',
      ),
    ),
  ),
  7723430713 => 
  array (
    'id' => 18239453,
    'name' => 'ООО "ТАВИТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524220475,
    'updated_at' => 1524220475,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33916429,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916429',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723430713',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33916429,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916429',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18239453',
        'method' => 'get',
      ),
    ),
  ),
  2357005456 => 
  array (
    'id' => 18239467,
    'name' => 'ООО "Агрофирма "Агросахар"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524220559,
    'updated_at' => 1524220560,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33916449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2357005456',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33916449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18239467',
        'method' => 'get',
      ),
    ),
  ),
  7813456674 => 
  array (
    'id' => 18239653,
    'name' => 'ООО "ЮДЖИКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524221400,
    'updated_at' => 1524221537,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33916647,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916647',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813456674',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33916647,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916647',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18239653',
        'method' => 'get',
      ),
    ),
  ),
  6312161484 => 
  array (
    'id' => 18240561,
    'name' => 'ООО «АПОЛЛО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524225288,
    'updated_at' => 1524225289,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33917511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312161484',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33917511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18240561',
        'method' => 'get',
      ),
    ),
  ),
  7725335871 => 
  array (
    'id' => 18240831,
    'name' => 'ООО "МАСТЕР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524226665,
    'updated_at' => 1524226666,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33917777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725335871',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33917777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18240831',
        'method' => 'get',
      ),
    ),
  ),
  7841001742 => 
  array (
    'id' => 18240843,
    'name' => 'ООО «Львы Петербурга»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524226747,
    'updated_at' => 1524227031,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33917793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841001742',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33917793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18240843',
        'method' => 'get',
      ),
    ),
  ),
  7104040650 => 
  array (
    'id' => 18240915,
    'name' => 'ООО "Дортехразметка"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524227079,
    'updated_at' => 1524227220,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33917857,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917857',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524459600,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7104040650',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33917857,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33917857',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18240915',
        'method' => 'get',
      ),
    ),
  ),
  4217153786 => 
  array (
    'id' => 18255673,
    'name' => 'ООО "СпецСтройГрупп"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524457381,
    'updated_at' => 1524457381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929653,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929653',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4217153786',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'кемерова плюс 7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929653,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929653',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255673',
        'method' => 'get',
      ),
    ),
  ),
  3808102030 => 
  array (
    'id' => 18255651,
    'name' => 'ЗАО "Востсибпроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524457174,
    'updated_at' => 1524457454,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929641,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929641',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808102030',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929641,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929641',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255651',
        'method' => 'get',
      ),
    ),
  ),
  5904333429 => 
  array (
    'id' => 18255719,
    'name' => 'ООО "Векстройгарант"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524457944,
    'updated_at' => 1524457945,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929691,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929691',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904333429',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929691,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929691',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255719',
        'method' => 'get',
      ),
    ),
  ),
  4253011321 => 
  array (
    'id' => 18255743,
    'name' => 'ООО "СибТек"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524458202,
    'updated_at' => 1524458203,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929715,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929715',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4253011321',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929715,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929715',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255743',
        'method' => 'get',
      ),
    ),
  ),
  4101175710 => 
  array (
    'id' => 18255819,
    'name' => 'ООО "КАМСТРОЙТЕХНОЛОГИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524458904,
    'updated_at' => 1524459024,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101175710',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255819',
        'method' => 'get',
      ),
    ),
  ),
  7451328527 => 
  array (
    'id' => 18255843,
    'name' => 'ООО "КБ МИГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524459161,
    'updated_at' => 1524459225,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929803,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929803',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451328527',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929803,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929803',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255843',
        'method' => 'get',
      ),
    ),
  ),
  1841041896 => 
  array (
    'id' => 18255905,
    'name' => 'ООО "МОБИДЕЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524459712,
    'updated_at' => 1524459712,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1841041896',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929871,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929871',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255905',
        'method' => 'get',
      ),
    ),
  ),
  270406273 => 
  array (
    'id' => 18255917,
    'name' => 'ООО "ТОРГОВЫЙ ДОМ - ВОСТОКПРОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524459825,
    'updated_at' => 1524460029,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929877,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929877',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '270406273',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929877,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929877',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255917',
        'method' => 'get',
      ),
    ),
  ),
  323358690 => 
  array (
    'id' => 18255841,
    'name' => 'ООО "ТОН"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524459111,
    'updated_at' => 1524460142,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929799,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929799',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323358690',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'бурятия плюс 8',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929799,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929799',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255841',
        'method' => 'get',
      ),
    ),
  ),
  3818030882 => 
  array (
    'id' => 18255943,
    'name' => 'ООО УК ""ГОРОД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524460139,
    'updated_at' => 1524460179,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929923,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929923',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3818030882',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929923,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929923',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255943',
        'method' => 'get',
      ),
    ),
  ),
  6501237234 => 
  array (
    'id' => 18255971,
    'name' => 'ООО "ПромСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524460409,
    'updated_at' => 1524460503,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501237234',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255971',
        'method' => 'get',
      ),
    ),
  ),
  403004590 => 
  array (
    'id' => 18256703,
    'name' => 'ООО "Канстрой"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524466347,
    'updated_at' => 1524466381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33931153,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931153',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '403004590',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33931153,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931153',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18256703',
        'method' => 'get',
      ),
    ),
  ),
  2538124102 => 
  array (
    'id' => 18256171,
    'name' => 'ООО "Виктори"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524462022,
    'updated_at' => 1524462022,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33930211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33930211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2538124102',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33930211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33930211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18256171',
        'method' => 'get',
      ),
    ),
  ),
  4901007337 => 
  array (
    'id' => 18256241,
    'name' => 'ООО "Мотыклей"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524462390,
    'updated_at' => 1524463097,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33930275,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33930275',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4901007337',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33930275,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33930275',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18256241',
        'method' => 'get',
      ),
    ),
  ),
  8609018993 => 
  array (
    'id' => 18256475,
    'name' => 'ООО "Строительно-Транспортная Компания "Велес"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524463992,
    'updated_at' => 1524464073,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33930627,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33930627',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8609018993',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33930627,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33930627',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18256475',
        'method' => 'get',
      ),
    ),
  ),
  6318239793 => 
  array (
    'id' => 15340235,
    'name' => 'Строймеханизация, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1493975925,
    'updated_at' => 1524466879,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29880513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29880513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29880653,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29880653',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318239793',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29880513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29880513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15340235',
        'method' => 'get',
      ),
    ),
  ),
  7720308024 => 
  array (
    'id' => 18257307,
    'name' => 'ООО "МКС ВСЕГДА НА ВЫСОТЕ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524468112,
    'updated_at' => 1524468113,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33931781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720308024',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33931781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257307',
        'method' => 'get',
      ),
    ),
  ),
  7717781488 => 
  array (
    'id' => 18257369,
    'name' => 'ООО «ГРАВИКОМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524468320,
    'updated_at' => 1524468321,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33931833,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931833',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717781488',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33931833,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931833',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257369',
        'method' => 'get',
      ),
    ),
  ),
  5032261607 => 
  array (
    'id' => 18257423,
    'name' => 'ООО «АВ-Комплекс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524468468,
    'updated_at' => 1524468469,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33931883,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931883',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032261607',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33931883,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33931883',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257423',
        'method' => 'get',
      ),
    ),
  ),
  5053036634 => 
  array (
    'id' => 18257671,
    'name' => 'ООО «ЭЛЬФАРМА»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524469384,
    'updated_at' => 1524469409,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33932177,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932177',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5053036634',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33932177,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932177',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257671',
        'method' => 'get',
      ),
    ),
  ),
  7721772020 => 
  array (
    'id' => 18257731,
    'name' => 'ООО "АйТек Групп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524469606,
    'updated_at' => 1524469622,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33932239,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932239',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721772020',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33932239,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932239',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257731',
        'method' => 'get',
      ),
    ),
  ),
  5837053631 => 
  array (
    'id' => 15490943,
    'name' => 'ООО "Лидер"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1495629667,
    'updated_at' => 1524469782,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30073421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30073421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921449,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5837053631',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Пензенская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30073421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30073421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15490943',
        'method' => 'get',
      ),
    ),
  ),
  6686039426 => 
  array (
    'id' => 18257833,
    'name' => 'ООО «Пожмаш- Ек»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524470076,
    'updated_at' => 1524470077,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33932341,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932341',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6686039426',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33932341,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932341',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257833',
        'method' => 'get',
      ),
    ),
  ),
  5017103010 => 
  array (
    'id' => 18257887,
    'name' => 'ООО"ММ ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524470268,
    'updated_at' => 1524470268,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33932397,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932397',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5017103010',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33932397,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932397',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257887',
        'method' => 'get',
      ),
    ),
  ),
  5042091190 => 
  array (
    'id' => 18257905,
    'name' => 'ООО ПКФ "Евро Стиль"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524470357,
    'updated_at' => 1524470448,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33932419,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932419',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5042091190',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33932419,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932419',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257905',
        'method' => 'get',
      ),
    ),
  ),
  7728876318 => 
  array (
    'id' => 18257977,
    'name' => 'ООО "Салюс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524470700,
    'updated_at' => 1524470813,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33932505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728876318',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33932505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18257977',
        'method' => 'get',
      ),
    ),
  ),
  8901020254 => 
  array (
    'id' => 18258359,
    'name' => 'ООО "ОБСКАЯ СТРОИТЕЛЬНАЯ КОМПАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524471916,
    'updated_at' => 1524471917,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33932851,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932851',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8901020254',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33932851,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33932851',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18258359',
        'method' => 'get',
      ),
    ),
  ),
  7725649444 => 
  array (
    'id' => 18258555,
    'name' => 'ООО "Верконт Сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524472595,
    'updated_at' => 1524472661,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33933069,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33933069',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725649444',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33933069,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33933069',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18258555',
        'method' => 'get',
      ),
    ),
  ),
  3235015610 => 
  array (
    'id' => 18258623,
    'name' => 'ООО "ТримСтрой"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524472818,
    'updated_at' => 1524472819,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33933143,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33933143',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3235015610',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33933143,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33933143',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18258623',
        'method' => 'get',
      ),
    ),
  ),
  667104951495 => 
  array (
    'id' => 18260143,
    'name' => 'ИП Лебедев Константин Юрьевич',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524477996,
    'updated_at' => 1524477996,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33934693,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934693',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '667104951495',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33934693,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934693',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18260143',
        'method' => 'get',
      ),
    ),
  ),
  3106006230 => 
  array (
    'id' => 18260159,
    'name' => 'ООО "Ремонтно-строительный участок"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524478039,
    'updated_at' => 1524478110,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33934711,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934711',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3106006230',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33934711,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934711',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18260159',
        'method' => 'get',
      ),
    ),
  ),
  7718991054 => 
  array (
    'id' => 18260199,
    'name' => 'ООО "ТВОЙ ДОМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524478177,
    'updated_at' => 1524478208,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33934749,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934749',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718991054',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33934749,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934749',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18260199',
        'method' => 'get',
      ),
    ),
  ),
  7708733953 => 
  array (
    'id' => 18260271,
    'name' => 'ООО ГК "ИНФОРМ ДЕВЕЛОПМЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524478515,
    'updated_at' => 1524478516,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33934831,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934831',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708733953',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33934831,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33934831',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18260271',
        'method' => 'get',
      ),
    ),
  ),
  2635102295 => 
  array (
    'id' => 18260643,
    'name' => 'ООО «Дварис Моторс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524479999,
    'updated_at' => 1524480076,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33935153,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935153',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635102295',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33935153,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935153',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18260643',
        'method' => 'get',
      ),
    ),
  ),
  5047194103 => 
  array (
    'id' => 18260815,
    'name' => 'ООО "О-РСИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524480613,
    'updated_at' => 1524480614,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33935323,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935323',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047194103',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33935323,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935323',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18260815',
        'method' => 'get',
      ),
    ),
  ),
  7814142991 => 
  array (
    'id' => 15904345,
    'name' => 'ООО «Северная пальмира»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1500015142,
    'updated_at' => 1524480828,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30712905,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712905',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814142991',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30712905,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30712905',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15904345',
        'method' => 'get',
      ),
    ),
  ),
  7743601244 => 
  array (
    'id' => 18260967,
    'name' => 'ООО "ИНГРОСС Лтд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524481364,
    'updated_at' => 1524481365,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33935503,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935503',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743601244',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33935503,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935503',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18260967',
        'method' => 'get',
      ),
    ),
  ),
  7801310416 => 
  array (
    'id' => 18261069,
    'name' => 'ООО "Грация"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524481809,
    'updated_at' => 1524481934,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33935617,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935617',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801310416',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33935617,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935617',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18261069',
        'method' => 'get',
      ),
    ),
  ),
  561050342 => 
  array (
    'id' => 18261227,
    'name' => 'ООО " ЦЕНТР СОПРЯЖЕННОГО МОНИТОРИНГА ОКРУЖАЮЩЕЙ СРЕДЫ И ПРИРОДНЫХ РЕСУРСОВ "',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524482365,
    'updated_at' => 1524482392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33935753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '561050342',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33935753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18261227',
        'method' => 'get',
      ),
    ),
  ),
  7722737244 => 
  array (
    'id' => 14439770,
    'name' => 'ООО "СИТИГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484909497,
    'updated_at' => 1524482584,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28586962,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586962',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524482584,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722737244',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28586962,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28586962',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14439770',
        'method' => 'get',
      ),
    ),
  ),
  7727292944 => 
  array (
    'id' => 14613490,
    'name' => 'ООО «БАРУС»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1486364124,
    'updated_at' => 1524482635,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28879736,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28879736',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524482635,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727292944',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28879736,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28879736',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14613490',
        'method' => 'get',
      ),
    ),
  ),
  7704874103 => 
  array (
    'id' => 18261309,
    'name' => 'ООО "НАНОК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524482710,
    'updated_at' => 1524482733,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33935841,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935841',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704874103',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33935841,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935841',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18261309',
        'method' => 'get',
      ),
    ),
  ),
  7725669345 => 
  array (
    'id' => 18261345,
    'name' => 'ООО «ОргТелеСервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524482889,
    'updated_at' => 1524482890,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33935891,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935891',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725669345',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33935891,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33935891',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18261345',
        'method' => 'get',
      ),
    ),
  ),
  2010001310 => 
  array (
    'id' => 18261569,
    'name' => 'ООО "ФЕНИКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524483610,
    'updated_at' => 1524483611,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33936081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2010001310',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33936081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18261569',
        'method' => 'get',
      ),
    ),
  ),
  276161774 => 
  array (
    'id' => 18261587,
    'name' => 'ООО "Башагротехсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524483690,
    'updated_at' => 1524483690,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33936101,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936101',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '276161774',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33936101,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936101',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18261587',
        'method' => 'get',
      ),
    ),
  ),
  7730230225 => 
  array (
    'id' => 18261661,
    'name' => 'ООО "СКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524483860,
    'updated_at' => 1524483969,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33936157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730230225',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33936157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18261661',
        'method' => 'get',
      ),
    ),
  ),
  7730698408 => 
  array (
    'id' => 18262007,
    'name' => 'ООО "МЕРКОНА ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524485457,
    'updated_at' => 1524485457,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33936523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730698408',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33936523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18262007',
        'method' => 'get',
      ),
    ),
  ),
  7816631042 => 
  array (
    'id' => 18262079,
    'name' => 'ООО «ТПК «Рубин»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524485807,
    'updated_at' => 1524485808,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33936599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816631042',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33936599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18262079',
        'method' => 'get',
      ),
    ),
  ),
  7718837983 => 
  array (
    'id' => 18262175,
    'name' => 'ООО "ПМ-Эксперт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524486244,
    'updated_at' => 1524486244,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33936731,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936731',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718837983',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33936731,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936731',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18262175',
        'method' => 'get',
      ),
    ),
  ),
  7701897416 => 
  array (
    'id' => 18262363,
    'name' => 'ООО «ЕСЛ Групп»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524487019,
    'updated_at' => 1524487057,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33937003,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33937003',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701897416',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33937003,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33937003',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18262363',
        'method' => 'get',
      ),
    ),
  ),
  7728731552 => 
  array (
    'id' => 18262501,
    'name' => 'ООО  «Гематологическая Корпорация»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524487612,
    'updated_at' => 1524487674,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33937165,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33937165',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728731552',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33937165,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33937165',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18262501',
        'method' => 'get',
      ),
    ),
  ),
  7450061461 => 
  array (
    'id' => 18266121,
    'name' => 'ООО "АСПЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524542959,
    'updated_at' => 1524542959,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33940967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7450061461',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33940967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266121',
        'method' => 'get',
      ),
    ),
  ),
  4101165447 => 
  array (
    'id' => 18266131,
    'name' => 'ООО "Камчаттехноресурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524543161,
    'updated_at' => 1524543215,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33940979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101165447',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33940979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266131',
        'method' => 'get',
      ),
    ),
  ),
  3801119476 => 
  array (
    'id' => 18266137,
    'name' => 'ООО "СПЕЦТЕПЛОСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524543246,
    'updated_at' => 1524543246,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33940983,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940983',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3801119476',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33940983,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940983',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266137',
        'method' => 'get',
      ),
    ),
  ),
  2827007415 => 
  array (
    'id' => 18137529,
    'name' => 'ООО Надежда',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523336546,
    'updated_at' => 1524543620,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33773605,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773605',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2827007415',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33773605,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33773605',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18137529',
        'method' => 'get',
      ),
    ),
  ),
  7708667700 => 
  array (
    'id' => 15812953,
    'name' => 'Интерс-Рус, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1499069333,
    'updated_at' => 1524543790,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30534431,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30534431',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1501228623,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708667700',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30534431,
        1 => 30551835,
        2 => 30563551,
        3 => 33903533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30534431,30551835,30563551,33903533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15812953',
        'method' => 'get',
      ),
    ),
  ),
  6629020299 => 
  array (
    'id' => 18266155,
    'name' => 'ООО "Новоуральск-Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524543639,
    'updated_at' => 1524544355,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6629020299',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266155',
        'method' => 'get',
      ),
    ),
  ),
  2721233421 => 
  array (
    'id' => 18266209,
    'name' => 'ООО "Дальславия"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524544396,
    'updated_at' => 1524544480,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941053,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941053',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721233421',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941053,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941053',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266209',
        'method' => 'get',
      ),
    ),
  ),
  2466122054 => 
  array (
    'id' => 18266227,
    'name' => 'ООО "Бартон"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524544538,
    'updated_at' => 1524544827,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466122054',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266227',
        'method' => 'get',
      ),
    ),
  ),
  '30.12.1991' => 
  array (
    'id' => 18084649,
    'name' => 'Константин Абрамов',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1522742397,
    'updated_at' => 1524544900,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '30.12.1991',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'status_id' => 9392889,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18084649',
        'method' => 'get',
      ),
    ),
  ),
  '30.04.1991' => 
  array (
    'id' => 18084653,
    'name' => 'Марат Валеев',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1522742412,
    'updated_at' => 1524544900,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '30.04.1991',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'status_id' => 9392889,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18084653',
        'method' => 'get',
      ),
    ),
  ),
  '18 мая' => 
  array (
    'id' => 18225777,
    'name' => 'Елена Цимбалюк',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1524112013,
    'updated_at' => 1524544900,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '18 мая',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'status_id' => 9392889,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18225777',
        'method' => 'get',
      ),
    ),
  ),
);