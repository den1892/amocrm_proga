<?php 
 return array (
  7728567091 => 
  array (
    'id' => 13988714,
    'name' => 'ООО "Ремонт тепловых систем -4"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479989879,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28004658,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28004658',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728567091',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28004658,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28004658',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13988714',
        'method' => 'get',
      ),
    ),
  ),
  7727279333 => 
  array (
    'id' => 13988822,
    'name' => 'ООО "Лемавто"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479990605,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28004836,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28004836',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727279333',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28004836,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28004836',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13988822',
        'method' => 'get',
      ),
    ),
  ),
  7811524960 => 
  array (
    'id' => 13992816,
    'name' => 'ООО «Производственная Инвестиционная Компания»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480056598,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28009910,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28009910',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811524960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28009910,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28009910',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13992816',
        'method' => 'get',
      ),
    ),
  ),
  7743745341 => 
  array (
    'id' => 13992914,
    'name' => 'ООО  "СИГМА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480057358,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28010066,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010066',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743745341',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28010066,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010066',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13992914',
        'method' => 'get',
      ),
    ),
  ),
  7736668953 => 
  array (
    'id' => 13993180,
    'name' => 'ООО Темпо-Проект',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480058367,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28010430,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010430',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736668953',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28010430,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010430',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13993180',
        'method' => 'get',
      ),
    ),
  ),
  7736642458 => 
  array (
    'id' => 13993218,
    'name' => 'ООО  «ГТЦентр»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480058525,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28010480,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010480',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7736642458',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28010480,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010480',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13993218',
        'method' => 'get',
      ),
    ),
  ),
  7733248615 => 
  array (
    'id' => 13993262,
    'name' => 'ООО ИНВЕСТ-ХОЛДИНГ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480058874,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28010516,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010516',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733248615',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28010516,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28010516',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13993262',
        'method' => 'get',
      ),
    ),
  ),
  7730509322 => 
  array (
    'id' => 13993700,
    'name' => 'ООО  "Научно-производственная фирма "Генлаб"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480061103,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28011152,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28011152',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522459,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730509322',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28011152,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28011152',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13993700',
        'method' => 'get',
      ),
    ),
  ),
  7726358110 => 
  array (
    'id' => 13994242,
    'name' => 'ООО Бренд Сити"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480063448,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28011780,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28011780',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726358110',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28011780,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28011780',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13994242',
        'method' => 'get',
      ),
    ),
  ),
  7724595820 => 
  array (
    'id' => 13994682,
    'name' => 'АО  "Спецремэлектро"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480065231,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28012308,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012308',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724595820',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28012308,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012308',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13994682',
        'method' => 'get',
      ),
    ),
  ),
  7719857608 => 
  array (
    'id' => 13994814,
    'name' => 'ООО  "ЧИСТЫЙ ГОРОД"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480065866,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28012492,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012492',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719857608',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28012492,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012492',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13994814',
        'method' => 'get',
      ),
    ),
  ),
  7719154813 => 
  array (
    'id' => 13994844,
    'name' => 'ООО "ГК ОЗОН"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480066019,
    'updated_at' => 1524545370,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28012540,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012540',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719154813',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28012540,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012540',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13994844',
        'method' => 'get',
      ),
    ),
  ),
  7718307200 => 
  array (
    'id' => 13995088,
    'name' => 'ООО "ЭКОНОМИТОН"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480066769,
    'updated_at' => 1524545371,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28012766,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012766',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718307200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28012766,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012766',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13995088',
        'method' => 'get',
      ),
    ),
  ),
  7714944960 => 
  array (
    'id' => 13995226,
    'name' => 'ООО "СИСТЕМЫ УОКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480067460,
    'updated_at' => 1524545371,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28012934,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012934',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714944960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28012934,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012934',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13995226',
        'method' => 'get',
      ),
    ),
  ),
  7714897357 => 
  array (
    'id' => 13995264,
    'name' => 'ООО "Даниан-Сервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480067598,
    'updated_at' => 1524545371,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28012968,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012968',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714897357',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28012968,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28012968',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13995264',
        'method' => 'get',
      ),
    ),
  ),
  7714817954 => 
  array (
    'id' => 13995308,
    'name' => 'ООО ИНТЕРНОВА',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480067890,
    'updated_at' => 1524545371,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28013024,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28013024',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714817954',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28013024,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28013024',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13995308',
        'method' => 'get',
      ),
    ),
  ),
  7709802617 => 
  array (
    'id' => 13995380,
    'name' => 'ООО "АК Центр"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480068288,
    'updated_at' => 1524545371,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28013124,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28013124',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709802617',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28013124,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28013124',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13995380',
        'method' => 'get',
      ),
    ),
  ),
  7708701503 => 
  array (
    'id' => 13995404,
    'name' => 'ООО "Центр Логистического Сервиса"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1480068458,
    'updated_at' => 1524545371,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28013150,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28013150',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522458,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708701503',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28013150,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28013150',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13995404',
        'method' => 'get',
      ),
    ),
  ),
  2465317452 => 
  array (
    'id' => 13822740,
    'name' => 'ООО ЧОО «Алиот»',
    'responsible_user_id' => 1261404,
    'created_by' => 643314,
    'created_at' => 1479275029,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27689850,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27689850',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465317452',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27689850,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27689850',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13822740',
        'method' => 'get',
      ),
    ),
  ),
  7810990373 => 
  array (
    'id' => 13823284,
    'name' => 'ООО Интерн',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479279005,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27690498,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27690498',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810990373',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27690498,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27690498',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13823284',
        'method' => 'get',
      ),
    ),
  ),
  7802813927 => 
  array (
    'id' => 13823308,
    'name' => 'ООО Сервис Медицинских Систем',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479279162,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27690522,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27690522',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802813927',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27690522,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27690522',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13823308',
        'method' => 'get',
      ),
    ),
  ),
  7735575858 => 
  array (
    'id' => 13823352,
    'name' => 'ОАО  "НАУЧНО-ПРОИЗВОДСТВЕННОЕ ОБЪЕДИНЕНИЕ АНГСТРЕМ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479279351,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27690590,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27690590',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735575858',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27690590,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27690590',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13823352',
        'method' => 'get',
      ),
    ),
  ),
  7733705660 => 
  array (
    'id' => 13824138,
    'name' => 'ООО «ТИМЛИС-технолоджи»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479283618,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27691900,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27691900',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733705660',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27691900,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27691900',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13824138',
        'method' => 'get',
      ),
    ),
  ),
  7731453922 => 
  array (
    'id' => 13825020,
    'name' => 'ООО "АРСИЭНТЕК"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479287304,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27693034,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693034',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731453922',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27693034,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693034',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13825020',
        'method' => 'get',
      ),
    ),
  ),
  7727808273 => 
  array (
    'id' => 13825820,
    'name' => 'ООО «Международный мульти торговый оператор»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479289708,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27693650,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693650',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727808273',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27693650,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693650',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13825820',
        'method' => 'get',
      ),
    ),
  ),
  7726761897 => 
  array (
    'id' => 13825860,
    'name' => 'ООО  "РСУ-5"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479289892,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27693702,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693702',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726761897',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27693702,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693702',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13825860',
        'method' => 'get',
      ),
    ),
  ),
  7725824128 => 
  array (
    'id' => 13825934,
    'name' => 'ООО «КОМФОРТПАРКИНГ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479290211,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27693808,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693808',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725824128',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27693808,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27693808',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13825934',
        'method' => 'get',
      ),
    ),
  ),
  7445021294 => 
  array (
    'id' => 13826150,
    'name' => 'ООО ВОДОМЕР',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479291175,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27694118,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27694118',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7445021294',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27694118,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27694118',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13826150',
        'method' => 'get',
      ),
    ),
  ),
  7725268022 => 
  array (
    'id' => 13827406,
    'name' => 'ООО  "Ремонтстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479297213,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27695986,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27695986',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725268022',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27695986,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27695986',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13827406',
        'method' => 'get',
      ),
    ),
  ),
  7724745995 => 
  array (
    'id' => 13827892,
    'name' => 'ООО «ССК»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479299442,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27696522,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27696522',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724745995',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27696522,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27696522',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13827892',
        'method' => 'get',
      ),
    ),
  ),
  2722107934 => 
  array (
    'id' => 13831534,
    'name' => 'ООО "ХОРРОКС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479354199,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27701384,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701384',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2722107934',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27701384,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701384',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13831534',
        'method' => 'get',
      ),
    ),
  ),
  7705546930 => 
  array (
    'id' => 13831540,
    'name' => '"ООО ""СФЕРА"""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479354469,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27701392,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701392',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705546930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27701392,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701392',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13831540',
        'method' => 'get',
      ),
    ),
  ),
  326511473 => 
  array (
    'id' => 13831572,
    'name' => 'ООО "Сарто"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479355678,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27701446,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701446',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326511473',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27701446,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701446',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13831572',
        'method' => 'get',
      ),
    ),
  ),
  2537009749 => 
  array (
    'id' => 13831584,
    'name' => 'ООО  "РЕНОВАЦИЯ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479356026,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27701468,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701468',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2537009749',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27701468,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27701468',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13831584',
        'method' => 'get',
      ),
    ),
  ),
  5503239274 => 
  array (
    'id' => 13832076,
    'name' => 'ООО "АГЕНТСТВО ПО РАЗВИТИЮ ТЕРРИТОРИЙ "ГЕОНИКА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479363510,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27702124,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27702124',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5503239274',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27702124,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27702124',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13832076',
        'method' => 'get',
      ),
    ),
  ),
  8602202143 => 
  array (
    'id' => 13832776,
    'name' => 'ООО "ЭнергоНорд"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479366604,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27702940,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27702940',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602202143',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27702940,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27702940',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13832776',
        'method' => 'get',
      ),
    ),
  ),
  8602156592 => 
  array (
    'id' => 13832798,
    'name' => 'ООО ТА "Планета детства"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479366728,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27702974,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27702974',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602156592',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27702974,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27702974',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13832798',
        'method' => 'get',
      ),
    ),
  ),
  7841441380 => 
  array (
    'id' => 13832826,
    'name' => 'ООО "Строительная компания "Юнит"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479366834,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27703006,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27703006',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841441380',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27703006,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27703006',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13832826',
        'method' => 'get',
      ),
    ),
  ),
  7814611080 => 
  array (
    'id' => 13832986,
    'name' => 'ООО "ВЕЛЕС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479367521,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27703256,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27703256',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814611080',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27703256,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27703256',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13832986',
        'method' => 'get',
      ),
    ),
  ),
  7813225677 => 
  array (
    'id' => 13833032,
    'name' => 'ООО Нордстим',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479367735,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27703314,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27703314',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813225677',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27703314,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27703314',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13833032',
        'method' => 'get',
      ),
    ),
  ),
  7811321390 => 
  array (
    'id' => 13834766,
    'name' => 'ООО "МАСТЕР"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479373405,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27705460,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27705460',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811321390',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27705460,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27705460',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13834766',
        'method' => 'get',
      ),
    ),
  ),
  7810560081 => 
  array (
    'id' => 13835930,
    'name' => 'ООО  "ДИВА РУМИНА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479376745,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27706506,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706506',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810560081',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27706506,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706506',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13835930',
        'method' => 'get',
      ),
    ),
  ),
  7806225789 => 
  array (
    'id' => 13836210,
    'name' => 'ООО  «ЛАБМЕДКОНСАЛТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479377865,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27706866,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706866',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806225789',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27706866,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706866',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836210',
        'method' => 'get',
      ),
    ),
  ),
  7805062010 => 
  array (
    'id' => 13836234,
    'name' => 'ООО "Компакт-Плюс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479377957,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27706910,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706910',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805062010',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27706910,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706910',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836234',
        'method' => 'get',
      ),
    ),
  ),
  7804265659 => 
  array (
    'id' => 13836296,
    'name' => 'ООО Спецмедпоставка',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479378298,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27706988,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706988',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804265659',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27706988,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27706988',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836296',
        'method' => 'get',
      ),
    ),
  ),
  7802550717 => 
  array (
    'id' => 13836324,
    'name' => 'ООО  "ЭТАЛОН"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479378503,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27707044,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707044',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802550717',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27707044,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707044',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836324',
        'method' => 'get',
      ),
    ),
  ),
  7802409827 => 
  array (
    'id' => 13836402,
    'name' => 'ООО "ИнфраХит Монтаж"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479378851,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27707144,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707144',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802409827',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27707144,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707144',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836402',
        'method' => 'get',
      ),
    ),
  ),
  7743925577 => 
  array (
    'id' => 13836450,
    'name' => 'ООО  "ИНТЕГРА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479379021,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27707202,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707202',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743925577',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27707202,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707202',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836450',
        'method' => 'get',
      ),
    ),
  ),
  7743652489 => 
  array (
    'id' => 13836552,
    'name' => 'ООО  "Проектсветстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479379381,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27707318,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707318',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743652489',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27707318,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707318',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836552',
        'method' => 'get',
      ),
    ),
  ),
  7743138604 => 
  array (
    'id' => 13836618,
    'name' => 'ООО "ВИКОЛЬД ГРУПП"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479379703,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27707382,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707382',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743138604',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27707382,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27707382',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13836618',
        'method' => 'get',
      ),
    ),
  ),
  7722841647 => 
  array (
    'id' => 13837178,
    'name' => 'ООО "ВОДЭКО-СТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 643314,
    'created_at' => 1479382862,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27708054,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27708054',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722841647',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27708054,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27708054',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13837178',
        'method' => 'get',
      ),
    ),
  ),
  9715205813 => 
  array (
    'id' => 13841694,
    'name' => 'ООО «АВЕНТИС»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479448606,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27712894,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27712894',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9715205813',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27712894,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27712894',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13841694',
        'method' => 'get',
      ),
    ),
  ),
  9705001507 => 
  array (
    'id' => 13841700,
    'name' => 'ООО "ЭЛЕКТРОННЫЙ АРХИВ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479448719,
    'updated_at' => 1524545372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27712908,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27712908',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9705001507',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27712908,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27712908',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13841700',
        'method' => 'get',
      ),
    ),
  ),
  7810802206 => 
  array (
    'id' => 13841776,
    'name' => 'ЗАО "МНК-ГРУПП"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479449164,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27712972,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27712972',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810802206',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27712972,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27712972',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13841776',
        'method' => 'get',
      ),
    ),
  ),
  7205025545 => 
  array (
    'id' => 13842004,
    'name' => 'ООО Строительная компания "Стройград"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479451285,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27713254,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713254',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7205025545',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27713254,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713254',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13842004',
        'method' => 'get',
      ),
    ),
  ),
  6829113141 => 
  array (
    'id' => 13842274,
    'name' => 'ООО  «Паритет»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479453365,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27713664,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713664',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829113141',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27713664,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713664',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13842274',
        'method' => 'get',
      ),
    ),
  ),
  6671383930 => 
  array (
    'id' => 13842334,
    'name' => 'ООО Агентство содействия бизнесу',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479453679,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27713750,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713750',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671383930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27713750,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713750',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13842334',
        'method' => 'get',
      ),
    ),
  ),
  6671291239 => 
  array (
    'id' => 13842364,
    'name' => 'ОАО "И-СЕТЬ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479453835,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27713790,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713790',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671291239',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27713790,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27713790',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13842364',
        'method' => 'get',
      ),
    ),
  ),
  7810202583 => 
  array (
    'id' => 13844218,
    'name' => 'ПАО "Ленгипротранс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479461291,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27715596,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27715596',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810202583',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27715596,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27715596',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13844218',
        'method' => 'get',
      ),
    ),
  ),
  7802755231 => 
  array (
    'id' => 13844250,
    'name' => 'ООО "Блок-Монолит"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479461452,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27715640,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27715640',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802755231',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27715640,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27715640',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13844250',
        'method' => 'get',
      ),
    ),
  ),
  591803866872 => 
  array (
    'id' => 13868072,
    'name' => 'ИП КОТЕЛЬНИКОВА ЛЮДМИЛА  АЛЕКСАНДРОВНА',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479796100,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27740694,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27740694',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '591803866872',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27740694,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27740694',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13868072',
        'method' => 'get',
      ),
    ),
  ),
  9909019020 => 
  array (
    'id' => 13868158,
    'name' => 'Компания "ПрайсвотерхаусКуперс Раша Б.В."',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479796564,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27740788,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27740788',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9909019020',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27740788,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27740788',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13868158',
        'method' => 'get',
      ),
    ),
  ),
  7841467646 => 
  array (
    'id' => 13868606,
    'name' => 'ООО "ГеоКад"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479797786,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27741110,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27741110',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841467646',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27741110,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27741110',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13868606',
        'method' => 'get',
      ),
    ),
  ),
  7733237853 => 
  array (
    'id' => 13869912,
    'name' => 'ООО "ФЕНИКС ГРУПП"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479801390,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27741960,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27741960',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733237853',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27741960,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27741960',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13869912',
        'method' => 'get',
      ),
    ),
  ),
  7728711147 => 
  array (
    'id' => 13869976,
    'name' => 'ООО "Эффективная Эксплуатация"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479801693,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27742054,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27742054',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728711147',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27742054,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27742054',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13869976',
        'method' => 'get',
      ),
    ),
  ),
  7728182510 => 
  array (
    'id' => 13871072,
    'name' => 'ООО «СИТИПРИНТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479805233,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27743540,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27743540',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728182510',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27743540,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27743540',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13871072',
        'method' => 'get',
      ),
    ),
  ),
  7724363410 => 
  array (
    'id' => 13871152,
    'name' => 'ООО "СТРОЙКВЕСТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479805354,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27743624,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27743624',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724363410',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27743624,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27743624',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13871152',
        'method' => 'get',
      ),
    ),
  ),
  7718811520 => 
  array (
    'id' => 13871240,
    'name' => 'ООО "Кортекс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479805595,
    'updated_at' => 1524545373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27743726,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27743726',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522460,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718811520',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27743726,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27743726',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13871240',
        'method' => 'get',
      ),
    ),
  ),
  4205331071 => 
  array (
    'id' => 13757410,
    'name' => 'ООО "СИМБИОНТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478603635,
    'updated_at' => 1524545374,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27601182,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27601182',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205331071',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27601182,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27601182',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13757410',
        'method' => 'get',
      ),
    ),
  ),
  2721064702 => 
  array (
    'id' => 13757450,
    'name' => 'ООО «Дальневосточная компания телекоммуникаций»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478603814,
    'updated_at' => 1524545374,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27601220,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27601220',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721064702',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27601220,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27601220',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13757450',
        'method' => 'get',
      ),
    ),
  ),
  4826079851 => 
  array (
    'id' => 13764894,
    'name' => 'ООО "Центр Комплексной Безопасности Конфидент 48"',
    'responsible_user_id' => 1261404,
    'created_by' => 643314,
    'created_at' => 1478673811,
    'updated_at' => 1524545374,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27608266,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27608266',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826079851',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27608266,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27608266',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13764894',
        'method' => 'get',
      ),
    ),
  ),
  7705493372 => 
  array (
    'id' => 13767412,
    'name' => 'ООО  "Технострой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478683763,
    'updated_at' => 1524545374,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27611416,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27611416',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705493372',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27611416,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27611416',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13767412',
        'method' => 'get',
      ),
    ),
  ),
  7707779909 => 
  array (
    'id' => 13767454,
    'name' => 'ООО "ЭКО-ТЕСТ ХОЛДИНГ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478683910,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27611462,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27611462',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707779909',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27611462,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27611462',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13767454',
        'method' => 'get',
      ),
    ),
  ),
  7707782669 => 
  array (
    'id' => 13767482,
    'name' => 'ООО "ФАРМ БОКС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478684084,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27611510,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27611510',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707782669',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27611510,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27611510',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13767482',
        'method' => 'get',
      ),
    ),
  ),
  7709842257 => 
  array (
    'id' => 13768086,
    'name' => 'ООО  "ОФИС - ДЕКОР"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478686501,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27612380,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612380',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709842257',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27612380,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612380',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13768086',
        'method' => 'get',
      ),
    ),
  ),
  7713119682 => 
  array (
    'id' => 13768170,
    'name' => '"ЗАО ""Белорусская Техника"""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478686713,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27612468,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612468',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713119682',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27612468,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612468',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13768170',
        'method' => 'get',
      ),
    ),
  ),
  7714326138 => 
  array (
    'id' => 13768216,
    'name' => 'ООО ФИТОМ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478686919,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27612518,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612518',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714326138',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27612518,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612518',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13768216',
        'method' => 'get',
      ),
    ),
  ),
  7714333537 => 
  array (
    'id' => 13768244,
    'name' => 'ООО  "ЭЛИТ-МЕД"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478687015,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27612546,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612546',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714333537',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27612546,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612546',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13768244',
        'method' => 'get',
      ),
    ),
  ),
  7714693988 => 
  array (
    'id' => 13768320,
    'name' => 'ООО "Торговый дом "Теплообменник"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478687362,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27612638,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612638',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714693988',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27612638,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27612638',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13768320',
        'method' => 'get',
      ),
    ),
  ),
  7715620365 => 
  array (
    'id' => 13768554,
    'name' => '"ООО ""СИГНАЛ-СЕРВИС"""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478688613,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27613014,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27613014',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715620365',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27613014,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27613014',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13768554',
        'method' => 'get',
      ),
    ),
  ),
  4212037384 => 
  array (
    'id' => 13775180,
    'name' => 'ООО «Шанс-Сервис»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478760120,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27619934,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27619934',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4212037384',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27619934,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27619934',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13775180',
        'method' => 'get',
      ),
    ),
  ),
  7449051081 => 
  array (
    'id' => 13775434,
    'name' => 'ООО "АВТОБЛОК"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478761331,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27620216,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27620216',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7449051081',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27620216,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27620216',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13775434',
        'method' => 'get',
      ),
    ),
  ),
  278101266 => 
  array (
    'id' => 13780402,
    'name' => 'ООО "СВЯЗЬОПТИКСТРОЙ""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478778552,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27629308,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27629308',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278101266',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27629308,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27629308',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13780402',
        'method' => 'get',
      ),
    ),
  ),
  516009938 => 
  array (
    'id' => 13780480,
    'name' => 'ООО "МАГУШ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478779109,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27629440,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27629440',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '516009938',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27629440,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27629440',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13780480',
        'method' => 'get',
      ),
    ),
  ),
  1645000332 => 
  array (
    'id' => 13781054,
    'name' => 'ООО  «УЧАСТОК РЕМОНТА И СТРОИТЕЛЬСТВА ЖИЛЬЯ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478780083,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27629672,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27629672',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1645000332',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27629672,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27629672',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13781054',
        'method' => 'get',
      ),
    ),
  ),
  2724148083 => 
  array (
    'id' => 13784798,
    'name' => 'ООО "Амур-центр"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478835632,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27633864,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27633864',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2724148083',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27633864,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27633864',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13784798',
        'method' => 'get',
      ),
    ),
  ),
  1435177414 => 
  array (
    'id' => 13784804,
    'name' => 'ООО "Северная Перспектива"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478835818,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27633868,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27633868',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435177414',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27633868,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27633868',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13784804',
        'method' => 'get',
      ),
    ),
  ),
  2464266893 => 
  array (
    'id' => 13784972,
    'name' => 'ООО "СНЕГИРИ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478839172,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27634092,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27634092',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464266893',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27634092,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27634092',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13784972',
        'method' => 'get',
      ),
    ),
  ),
  2130047148 => 
  array (
    'id' => 13785136,
    'name' => 'ООО "ЭТС-ПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 643314,
    'created_at' => 1478842187,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27634378,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27634378',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130047148',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27634378,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27634378',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13785136',
        'method' => 'get',
      ),
    ),
  ),
  6501265506 => 
  array (
    'id' => 13785180,
    'name' => 'ООО "Глобал ДВ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478842868,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27634454,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27634454',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501265506',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27634454,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27634454',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13785180',
        'method' => 'get',
      ),
    ),
  ),
  7536146131 => 
  array (
    'id' => 13786014,
    'name' => 'ООО “Промышленно-гражданское строительство”',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1478848688,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27635560,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27635560',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7536146131',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Читинская область(GMT +9)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27635560,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27635560',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13786014',
        'method' => 'get',
      ),
    ),
  ),
  6501153658 => 
  array (
    'id' => 13786408,
    'name' => 'ООО  "ЛИГО-дизайн трейдинг"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478850376,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27635994,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27635994',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501153658',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27635994,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27635994',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13786408',
        'method' => 'get',
      ),
    ),
  ),
  6901090201 => 
  array (
    'id' => 13787710,
    'name' => 'Акционерное общество "ЭКСМАШ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478855046,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27637146,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27637146',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6901090201',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27637146,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27637146',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13787710',
        'method' => 'get',
      ),
    ),
  ),
  7737524369 => 
  array (
    'id' => 13790166,
    'name' => 'ООО Екатерина вторая',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478864194,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27640466,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27640466',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382026,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7737524369',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27640466,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27640466',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13790166',
        'method' => 'get',
      ),
    ),
  ),
  7804486111 => 
  array (
    'id' => 13790470,
    'name' => 'ООО "Строй-инвест"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1478865783,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27640794,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27640794',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804486111',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27640794,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27640794',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13790470',
        'method' => 'get',
      ),
    ),
  ),
  7714773859 => 
  array (
    'id' => 13790808,
    'name' => 'ООО ТонерКарт',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478866508,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27641510,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27641510',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714773859',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27641510,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27641510',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13790808',
        'method' => 'get',
      ),
    ),
  ),
  2801182645 => 
  array (
    'id' => 13802260,
    'name' => 'ООО Торгово-Строительная компания  ВОСТОК-Строймаркет',
    'responsible_user_id' => 1261404,
    'created_by' => 643314,
    'created_at' => 1479101664,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27652858,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27652858',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521308,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2801182645',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Амурская область +9',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27652858,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27652858',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13802260',
        'method' => 'get',
      ),
    ),
  ),
  8603108263 => 
  array (
    'id' => 13802678,
    'name' => 'ООО СкИВ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479105345,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27653430,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27653430',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603108263',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27653430,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27653430',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13802678',
        'method' => 'get',
      ),
    ),
  ),
  7841443300 => 
  array (
    'id' => 13803180,
    'name' => 'ООО  ПИОН',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479107149,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27654046,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654046',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841443300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27654046,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654046',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13803180',
        'method' => 'get',
      ),
    ),
  ),
  7814529477 => 
  array (
    'id' => 13803584,
    'name' => 'ООО РемСтройСервис',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479108662,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27654538,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654538',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814529477',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27654538,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654538',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13803584',
        'method' => 'get',
      ),
    ),
  ),
  7813253579 => 
  array (
    'id' => 13803626,
    'name' => 'ООО ЭЛЬБРУС',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479108805,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27654594,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654594',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813253579',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27654594,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654594',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13803626',
        'method' => 'get',
      ),
    ),
  ),
  7810823661 => 
  array (
    'id' => 13803670,
    'name' => 'ООО РемСтройРегион',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479108959,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27654642,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654642',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810823661',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27654642,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27654642',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13803670',
        'method' => 'get',
      ),
    ),
  ),
  5025012053 => 
  array (
    'id' => 13806936,
    'name' => 'ООО  ПРОМЭКСПО',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479121671,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27660830,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27660830',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5025012053',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27660830,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27660830',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13806936',
        'method' => 'get',
      ),
    ),
  ),
  6154130963 => 
  array (
    'id' => 13807382,
    'name' => 'ООО СВОБОДА',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479123668,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27661498,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27661498',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6154130963',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27661498,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27661498',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13807382',
        'method' => 'get',
      ),
    ),
  ),
  1658046623 => 
  array (
    'id' => 13813944,
    'name' => 'ООО «Квазар»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479195414,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27675396,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27675396',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1658046623',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27675396,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27675396',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13813944',
        'method' => 'get',
      ),
    ),
  ),
  7705878630 => 
  array (
    'id' => 13814064,
    'name' => 'ООО  "СИМСТРОЙСЕРВИС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479195877,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27676292,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27676292',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705878630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27676292,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27676292',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13814064',
        'method' => 'get',
      ),
    ),
  ),
  7810352645 => 
  array (
    'id' => 13814824,
    'name' => 'ООО "Спектр""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479197059,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27678734,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27678734',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810352645',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27678734,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27678734',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13814824',
        'method' => 'get',
      ),
    ),
  ),
  6905043298 => 
  array (
    'id' => 13814908,
    'name' => 'ООО "КОМПАЛЕКС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479197431,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27679540,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27679540',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6905043298',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27679540,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27679540',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13814908',
        'method' => 'get',
      ),
    ),
  ),
  4703084805 => 
  array (
    'id' => 13816004,
    'name' => 'АО "Архитектурно-градостроительное проектное бюро"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479200967,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27681502,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27681502',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4703084805',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27681502,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27681502',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13816004',
        'method' => 'get',
      ),
    ),
  ),
  7723584431 => 
  array (
    'id' => 13816418,
    'name' => 'ООО "Эндолайт Центр"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479202618,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27681976,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27681976',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723584431',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27681976,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27681976',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13816418',
        'method' => 'get',
      ),
    ),
  ),
  7801290135 => 
  array (
    'id' => 13816490,
    'name' => 'ООО  "Техно-Сервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479202816,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27682042,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27682042',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801290135',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27682042,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27682042',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13816490',
        'method' => 'get',
      ),
    ),
  ),
  7810843040 => 
  array (
    'id' => 13816610,
    'name' => 'ООО "СК "ОЛИМП""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479203278,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27682164,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27682164',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810843040',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27682164,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27682164',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13816610',
        'method' => 'get',
      ),
    ),
  ),
  9718006421 => 
  array (
    'id' => 13817020,
    'name' => 'ООО  ГАРАНТ ПОСТАВКА',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479204923,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27682660,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27682660',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718006421',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27682660,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27682660',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13817020',
        'method' => 'get',
      ),
    ),
  ),
  7810607156 => 
  array (
    'id' => 13817616,
    'name' => 'ООО ГЕРМЕС',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479207855,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27683696,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27683696',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810607156',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27683696,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27683696',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13817616',
        'method' => 'get',
      ),
    ),
  ),
  7810414387 => 
  array (
    'id' => 13818016,
    'name' => 'ООО  СтройКомплект',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479210150,
    'updated_at' => 1524545375,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27684316,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684316',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810414387',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27684316,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684316',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13818016',
        'method' => 'get',
      ),
    ),
  ),
  4401069575 => 
  array (
    'id' => 13818058,
    'name' => 'ООО  ОХОТА',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479210366,
    'updated_at' => 1524545376,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27684380,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684380',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4401069575',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27684380,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684380',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13818058',
        'method' => 'get',
      ),
    ),
  ),
  7811501804 => 
  array (
    'id' => 13818276,
    'name' => 'ООО СК Эксперт',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479211615,
    'updated_at' => 1524545376,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27684716,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684716',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811501804',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27684716,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684716',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13818276',
        'method' => 'get',
      ),
    ),
  ),
  1510016088 => 
  array (
    'id' => 13818340,
    'name' => 'ООО "СтройМонтаж"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1479211850,
    'updated_at' => 1524545376,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27684788,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684788',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522461,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1510016088',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27684788,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27684788',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13818340',
        'method' => 'get',
      ),
    ),
  ),
  7703805594 => 
  array (
    'id' => 13431274,
    'name' => 'ООО М 13',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475500067,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27200544,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27200544',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703805594',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27200544,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27200544',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13431274',
        'method' => 'get',
      ),
    ),
  ),
  2461122886 => 
  array (
    'id' => 13441018,
    'name' => 'ООО "Ракурс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475582369,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27211888,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27211888',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2461122886',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27211888,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27211888',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13441018',
        'method' => 'get',
      ),
    ),
  ),
  2724149577 => 
  array (
    'id' => 13441600,
    'name' => '"ООО ""ЛСТ- Трейд"""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475585021,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27212582,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27212582',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2724149577',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27212582,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27212582',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13441600',
        'method' => 'get',
      ),
    ),
  ),
  2463251446 => 
  array (
    'id' => 13441640,
    'name' => 'ООО "ВКС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475585273,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27212630,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27212630',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463251446',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27212630,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27212630',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13441640',
        'method' => 'get',
      ),
    ),
  ),
  7106035091 => 
  array (
    'id' => 13458878,
    'name' => 'ООО "ТУЛАМАШ-ТАРПАН"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475727902,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27234490,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27234490',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7106035091',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27234490,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27234490',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13458878',
        'method' => 'get',
      ),
    ),
  ),
  2502047849 => 
  array (
    'id' => 13465180,
    'name' => 'ООО Райский сад',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475754999,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27242080,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27242080',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2502047849',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27242080,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27242080',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13465180',
        'method' => 'get',
      ),
    ),
  ),
  6215027028 => 
  array (
    'id' => 13465666,
    'name' => 'ООО Апрель',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475756853,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28959742,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28959742',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 28960164,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=28960164',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6215027028',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28959742,
        1 => 28960140,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28959742,28960140',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13465666',
        'method' => 'get',
      ),
    ),
  ),
  5226017213 => 
  array (
    'id' => 13466244,
    'name' => 'ООО  «Надежда»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475758581,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27243306,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27243306',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5226017213',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27243306,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27243306',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13466244',
        'method' => 'get',
      ),
    ),
  ),
  6501232035 => 
  array (
    'id' => 13470494,
    'name' => 'ООО "Мастер-С"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475817996,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27247418,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27247418',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501232035',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27247418,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27247418',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13470494',
        'method' => 'get',
      ),
    ),
  ),
  7816196826 => 
  array (
    'id' => 13508274,
    'name' => 'ООО «БАСТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476188450,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816196826',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13508274',
        'method' => 'get',
      ),
    ),
  ),
  7730501972 => 
  array (
    'id' => 13509216,
    'name' => 'ООО "Звуковые решения"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476193027,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27295470,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27295470',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730501972',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27295470,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27295470',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13509216',
        'method' => 'get',
      ),
    ),
  ),
  7733269171 => 
  array (
    'id' => 13509230,
    'name' => 'ООО Целинстрой',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476193104,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27295484,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27295484',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733269171',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27295484,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27295484',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13509230',
        'method' => 'get',
      ),
    ),
  ),
  2463255000 => 
  array (
    'id' => 13512266,
    'name' => 'ООО "ССД"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476244928,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27298828,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27298828',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463255000',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27298828,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27298828',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13512266',
        'method' => 'get',
      ),
    ),
  ),
  2466138047 => 
  array (
    'id' => 13512316,
    'name' => 'ООО "Лидер"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476246369,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27298924,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27298924',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466138047',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27298924,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27298924',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13512316',
        'method' => 'get',
      ),
    ),
  ),
  7535014238 => 
  array (
    'id' => 13514604,
    'name' => 'ООО "Тнт+"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1476260406,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24187562,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24187562',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 16914116,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=16914116',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1504268756,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7535014238',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чита +9',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24187562,
        1 => 31376413,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24187562,31376413',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13514604',
        'method' => 'get',
      ),
    ),
  ),
  2463100863 => 
  array (
    'id' => 13514844,
    'name' => 'ООО «ПромЭнергоГрупп»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476261177,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27302238,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27302238',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382027,
    'closest_task_at' => 1491541200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463100863',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27302238,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27302238',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13514844',
        'method' => 'get',
      ),
    ),
  ),
  6501266500 => 
  array (
    'id' => 13524502,
    'name' => 'ООО "Строительно-монтажная компания СахСтройСервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1476344972,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27314132,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27314132',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382027,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501266500',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Сахалинская область(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27314132,
        1 => 27325556,
        2 => 29477437,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27314132,27325556,29477437',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13524502',
        'method' => 'get',
      ),
    ),
  ),
  9710015551 => 
  array (
    'id' => 13528966,
    'name' => 'ООО "ВЫСОКИЙ СТАТУС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476359711,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27320382,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27320382',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9710015551',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27320382,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27320382',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13528966',
        'method' => 'get',
      ),
    ),
  ),
  2632805116 => 
  array (
    'id' => 13529026,
    'name' => 'ООО  РАЗВИТИЕ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476359930,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27320444,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27320444',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632805116',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27320444,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27320444',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13529026',
        'method' => 'get',
      ),
    ),
  ),
  6658459980 => 
  array (
    'id' => 13534544,
    'name' => 'ООО ПКФ "УТПК"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476428466,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27328888,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27328888',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658459980',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27328888,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27328888',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13534544',
        'method' => 'get',
      ),
    ),
  ),
  1840042015 => 
  array (
    'id' => 13560912,
    'name' => 'ООО ПРОИЗВОДСТВЕННОЕ ОБЪЕДИНЕНИЕ "ЭКОСИСТЕМЫ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476706279,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27354860,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27354860',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1840042015',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27354860,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27354860',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13560912',
        'method' => 'get',
      ),
    ),
  ),
  2130042453 => 
  array (
    'id' => 13561048,
    'name' => 'ОООО "ИНФОРМАЦИОННЫЕ СОЦИАЛЬНЫЕ СИСТЕМЫ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476706915,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27355010,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27355010',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130042453',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27355010,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27355010',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13561048',
        'method' => 'get',
      ),
    ),
  ),
  5017103959 => 
  array (
    'id' => 13561590,
    'name' => 'ООО "ТОРГЭКСПОЦЕНТР"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476708862,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27355394,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27355394',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5017103959',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27355394,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27355394',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13561590',
        'method' => 'get',
      ),
    ),
  ),
  '5 октября' => 
  array (
    'id' => 13567388,
    'name' => 'Шигапов Артур',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476778484,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27484859,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27484859',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1522742374,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5 октября',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27484859,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27484859',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13567388',
        'method' => 'get',
      ),
    ),
  ),
  572011930 => 
  array (
    'id' => 13570692,
    'name' => 'ООО УК "НОВЫЙ ГОРОД"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476791090,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27366442,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27366442',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '572011930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27366442,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27366442',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13570692',
        'method' => 'get',
      ),
    ),
  ),
  1654010231 => 
  array (
    'id' => 13570712,
    'name' => 'ЗАО «Казанское специализированное управление «ГИДРОСПЕЦСТРОЙ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476791220,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27366474,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27366474',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29021510,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29021510',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1654010231',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27366474,
        1 => 29021512,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27366474,29021512',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13570712',
        'method' => 'get',
      ),
    ),
  ),
  1658146434 => 
  array (
    'id' => 13570860,
    'name' => 'ООО "Гармоника"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476791940,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27366650,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27366650',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1658146434',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27366650,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27366650',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13570860',
        'method' => 'get',
      ),
    ),
  ),
  4501116170 => 
  array (
    'id' => 13577168,
    'name' => 'ООО« Центр ЮС « Авега»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476863376,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27374774,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27374774',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4501116170',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27374774,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27374774',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13577168',
        'method' => 'get',
      ),
    ),
  ),
  5406294752 => 
  array (
    'id' => 13580606,
    'name' => 'ЗАО ИЦ ""СИП""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476875534,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27378686,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27378686',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406294752',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27378686,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27378686',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13580606',
        'method' => 'get',
      ),
    ),
  ),
  6142025548 => 
  array (
    'id' => 13580884,
    'name' => 'ООО "РЕАЛПРОДУКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476876491,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27378958,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27378958',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6142025548',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27378958,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27378958',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13580884',
        'method' => 'get',
      ),
    ),
  ),
  3801074507 => 
  array (
    'id' => 13581126,
    'name' => 'ООО Флер-Фито',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476877203,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27379156,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27379156',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3801074507',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27379156,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27379156',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13581126',
        'method' => 'get',
      ),
    ),
  ),
  2204067381 => 
  array (
    'id' => 13581230,
    'name' => 'ООО Сервисный центр БЗКО',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476877527,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27379354,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27379354',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2204067381',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27379354,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27379354',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13581230',
        'method' => 'get',
      ),
    ),
  ),
  3811089580 => 
  array (
    'id' => 13581270,
    'name' => 'ООО  "ВОСТСИБСТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1476877670,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27379402,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27379402',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811089580',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27379402,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27379402',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13581270',
        'method' => 'get',
      ),
    ),
  ),
  '0326544052' => 
  array (
    'id' => 13585878,
    'name' => 'ООО "СКИП"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1476938272,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27384722,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27384722',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 27384724,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=27384724',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0326544052',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Бурятия(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27384722,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27384722',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13585878',
        'method' => 'get',
      ),
    ),
  ),
  2816008625 => 
  array (
    'id' => 13596348,
    'name' => 'ООО СК Мега-строй',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1477033683,
    'updated_at' => 1524545377,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27397416,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27397416',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2816008625',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27397416,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27397416',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13596348',
        'method' => 'get',
      ),
    ),
  ),
  7743913476 => 
  array (
    'id' => 13600868,
    'name' => 'ООО "Прогресс Строй"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1477050752,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27402904,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27402904',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743913476',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27402904,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27402904',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13600868',
        'method' => 'get',
      ),
    ),
  ),
  '0323388239' => 
  array (
    'id' => 13645542,
    'name' => 'ООО "Технический cервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1477483921,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27459782,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27459782',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0323388239',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Бурятия(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27459782,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27459782',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13645542',
        'method' => 'get',
      ),
    ),
  ),
  890600910882 => 
  array (
    'id' => 13646006,
    'name' => 'ИП Матевосян',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1477485865,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27460510,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27460510',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382027,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '890600910882',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27460510,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27460510',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13646006',
        'method' => 'get',
      ),
    ),
  ),
  7840454595 => 
  array (
    'id' => 13654060,
    'name' => 'ООО "НЬЮ МЕДИА ГРУПП"',
    'responsible_user_id' => 1261404,
    'created_by' => 1101858,
    'created_at' => 1477564055,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27472536,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27472536',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7840454595',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27472536,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27472536',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13654060',
        'method' => 'get',
      ),
    ),
  ),
  '' => 
  array (
    'id' => 11694710,
    'name' => 'ООО «Каспий»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460372698,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24728262,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728262',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24728262,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728262',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11694710',
        'method' => 'get',
      ),
    ),
  ),
  5406727910 => 
  array (
    'id' => 13685558,
    'name' => 'ЗАО  Сибирская компания  Союзлифтмонтаж',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1477907143,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27522630,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27522630',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406727910',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27522630,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27522630',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13685558',
        'method' => 'get',
      ),
    ),
  ),
  1435289767 => 
  array (
    'id' => 13687650,
    'name' => 'ООО «СЕВЕР-ТС»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1477912289,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27524650,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27524650',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435289767',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27524650,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27524650',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13687650',
        'method' => 'get',
      ),
    ),
  ),
  3812153133 => 
  array (
    'id' => 13703830,
    'name' => 'ООО «СтК Групп»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478076232,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27545328,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27545328',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812153133',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27545328,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27545328',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13703830',
        'method' => 'get',
      ),
    ),
  ),
  1655219758 => 
  array (
    'id' => 13707302,
    'name' => 'ООО Технотел',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478087591,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27549176,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27549176',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655219758',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27549176,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27549176',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13707302',
        'method' => 'get',
      ),
    ),
  ),
  3250523760 => 
  array (
    'id' => 13707324,
    'name' => 'ООО "ИнСистем"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478087725,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27549204,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27549204',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3250523760',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27549204,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27549204',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13707324',
        'method' => 'get',
      ),
    ),
  ),
  2450023562 => 
  array (
    'id' => 13712618,
    'name' => 'ООО "БМК"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478152605,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27555210,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27555210',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2450023562',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27555210,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27555210',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13712618',
        'method' => 'get',
      ),
    ),
  ),
  608008691 => 
  array (
    'id' => 13717356,
    'name' => 'ООО "МАСТЕР-КЛАСС"',
    'responsible_user_id' => 1261404,
    'created_by' => 1101858,
    'created_at' => 1478171534,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27560624,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27560624',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 27560622,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=27560622',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '608008691',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ингушетия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27560624,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27560624',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13717356',
        'method' => 'get',
      ),
    ),
  ),
  816016301 => 
  array (
    'id' => 13717432,
    'name' => 'ООО "Сантехстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 1101858,
    'created_at' => 1478171886,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27560702,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27560702',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 27560700,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=27560700',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '816016301',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калмыкия(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27560702,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27560702',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13717432',
        'method' => 'get',
      ),
    ),
  ),
  7017062946 => 
  array (
    'id' => 13717610,
    'name' => 'ООО НЦ "Полюс"',
    'responsible_user_id' => 1261404,
    'created_by' => 1101858,
    'created_at' => 1478172839,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27561004,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27561004',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 27561002,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=27561002',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500382027,
    'closest_task_at' => 1500003000,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017062946',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Томская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27561004,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27561004',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13717610',
        'method' => 'get',
      ),
    ),
  ),
  2723175373 => 
  array (
    'id' => 13757380,
    'name' => 'ООО "Хабаровский Завод Трубной Изоляции"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1478603508,
    'updated_at' => 1524545378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27601160,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27601160',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522462,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723175373',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27601160,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27601160',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13757380',
        'method' => 'get',
      ),
    ),
  ),
  5951998276 => 
  array (
    'id' => 12997015,
    'name' => 'ООО ЭКСТРАСТРОЙ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471517860,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26586007,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26586007',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5951998276',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26586007,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26586007',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12997015',
        'method' => 'get',
      ),
    ),
  ),
  6501263964 => 
  array (
    'id' => 13024481,
    'name' => 'ООО СК  "СтройКа"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471841362,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26612919,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26612919',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382027,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501263964',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26612919,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26612919',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13024481',
        'method' => 'get',
      ),
    ),
  ),
  2508072961 => 
  array (
    'id' => 13026703,
    'name' => 'ООО  "НАБО"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471852487,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26615179,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26615179',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2508072961',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26615179,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26615179',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13026703',
        'method' => 'get',
      ),
    ),
  ),
  5904204720 => 
  array (
    'id' => 13039727,
    'name' => 'ООО "САРКО"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471933348,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26628019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26628019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5904204720',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26628019,
        1 => 26629845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26628019,26629845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13039727',
        'method' => 'get',
      ),
    ),
  ),
  262013737 => 
  array (
    'id' => 13043303,
    'name' => 'ООО  Геоплан',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471946918,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26631749,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26631749',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '262013737',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26631749,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26631749',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13043303',
        'method' => 'get',
      ),
    ),
  ),
  5534021937 => 
  array (
    'id' => 13045457,
    'name' => 'ООО  " Автодом"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471954558,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26634199,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26634199',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5534021937',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26634199,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26634199',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13045457',
        'method' => 'get',
      ),
    ),
  ),
  3812113660 => 
  array (
    'id' => 13045663,
    'name' => 'ООО "ВСЭС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471955286,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26634431,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26634431',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812113660',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26634431,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26634431',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13045663',
        'method' => 'get',
      ),
    ),
  ),
  2721219071 => 
  array (
    'id' => 13098243,
    'name' => 'ООО "НАДЁЖНОЕ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1472466245,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26786611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26786611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721219071',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26786611,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26786611',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13098243',
        'method' => 'get',
      ),
    ),
  ),
  6658368330 => 
  array (
    'id' => 13099729,
    'name' => 'ООО "АрмаСталь"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1472472245,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26788497,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26788497',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658368330',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26788497,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26788497',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13099729',
        'method' => 'get',
      ),
    ),
  ),
  6685045931 => 
  array (
    'id' => 13100275,
    'name' => 'ООО "ШВАБЕ-ЕКАТЕРИНБУРГ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1472474518,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26789231,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26789231',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6685045931',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26789231,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26789231',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13100275',
        'method' => 'get',
      ),
    ),
  ),
  5501052538 => 
  array (
    'id' => 13146011,
    'name' => 'ЗАО "ЛИВА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1472817559,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26843457,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26843457',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5501052538',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26843457,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26843457',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13146011',
        'method' => 'get',
      ),
    ),
  ),
  5321071228 => 
  array (
    'id' => 13146561,
    'name' => 'ООО "Мир"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1472819839,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26844029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26844029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5321071228',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26844029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26844029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13146561',
        'method' => 'get',
      ),
    ),
  ),
  7715680090 => 
  array (
    'id' => 13167681,
    'name' => 'ООО «Брик Хаус»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473075235,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26864681,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26864681',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715680090',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26864681,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26864681',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13167681',
        'method' => 'get',
      ),
    ),
  ),
  6504012006 => 
  array (
    'id' => 13174337,
    'name' => 'ООО "ТРАНССТРОЙ-ТЕСТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1473138116,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26870863,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26870863',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6504012006',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Сахалинская область(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26870863,
        1 => 26870877,
        2 => 26903949,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26870863,26870877,26903949',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13174337',
        'method' => 'get',
      ),
    ),
  ),
  7805489877 => 
  array (
    'id' => 13181837,
    'name' => 'ООО ««ЛенСпецСталь»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473168843,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26899633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26899633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805489877',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26899633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26899633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13181837',
        'method' => 'get',
      ),
    ),
  ),
  7536069857 => 
  array (
    'id' => 13186073,
    'name' => 'ООО "Авангард Тех"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1473229563,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26904155,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26904155',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7536069857',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чита +9',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26904155,
        1 => 26904165,
        2 => 26904177,
        3 => 26905269,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26904155,26904165,26904177,26905269',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13186073',
        'method' => 'get',
      ),
    ),
  ),
  7816235786 => 
  array (
    'id' => 13186253,
    'name' => 'ООО "Капитель"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473231003,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26904427,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26904427',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816235786',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26904427,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26904427',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13186253',
        'method' => 'get',
      ),
    ),
  ),
  7734721048 => 
  array (
    'id' => 13188965,
    'name' => 'ООО  «ПетрГазОйл коммерц групп»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473241799,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26908401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26908401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734721048',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26908401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26908401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13188965',
        'method' => 'get',
      ),
    ),
  ),
  326508375 => 
  array (
    'id' => 13225141,
    'name' => 'ООО "Астра"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473658663,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26948513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26948513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326508375',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26948513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26948513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13225141',
        'method' => 'get',
      ),
    ),
  ),
  7717695609 => 
  array (
    'id' => 13232193,
    'name' => 'ООО «Альтум-Строй»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473683109,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26955961,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26955961',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717695609',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26955961,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26955961',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13232193',
        'method' => 'get',
      ),
    ),
  ),
  5020051644 => 
  array (
    'id' => 13243221,
    'name' => 'ООО "Светофор"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473768554,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26969467,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26969467',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5020051644',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26969467,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26969467',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13243221',
        'method' => 'get',
      ),
    ),
  ),
  5001097065 => 
  array (
    'id' => 13243637,
    'name' => 'ООО "Строй-Гарант XXI"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473770166,
    'updated_at' => 1524545379,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26969989,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26969989',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5001097065',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26969989,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26969989',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13243637',
        'method' => 'get',
      ),
    ),
  ),
  4826122465 => 
  array (
    'id' => 13243783,
    'name' => 'ООО «СМУ-8»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473770750,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26970209,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26970209',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826122465',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26970209,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26970209',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13243783',
        'method' => 'get',
      ),
    ),
  ),
  4633018852 => 
  array (
    'id' => 13252879,
    'name' => 'ООО СтройМастерЛюкс',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1473846586,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26982471,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26982471',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4633018852',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26982471,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26982471',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13252879',
        'method' => 'get',
      ),
    ),
  ),
  5903126148 => 
  array (
    'id' => 13308834,
    'name' => 'ООО  Аскров',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474365726,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27052948,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27052948',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1502340839,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903126148',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27052948,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27052948',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13308834',
        'method' => 'get',
      ),
    ),
  ),
  7721853128 => 
  array (
    'id' => 13309938,
    'name' => 'ООО ИТЦ-ПРО',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474369230,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27053902,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27053902',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721853128',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27053902,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27053902',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13309938',
        'method' => 'get',
      ),
    ),
  ),
  5032204253 => 
  array (
    'id' => 13310192,
    'name' => 'ООО ДорТехнология',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474370297,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27054226,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27054226',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032204253',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27054226,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27054226',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13310192',
        'method' => 'get',
      ),
    ),
  ),
  1650073760 => 
  array (
    'id' => 13311144,
    'name' => 'ООО «Электротранспорт»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474373927,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27055316,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27055316',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1650073760',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27055316,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27055316',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13311144',
        'method' => 'get',
      ),
    ),
  ),
  1657025807 => 
  array (
    'id' => 13311296,
    'name' => 'ЗАО «Казанская фармацевтическая фабрика»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474374407,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27055468,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27055468',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657025807',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27055468,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27055468',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13311296',
        'method' => 'get',
      ),
    ),
  ),
  2005007500 => 
  array (
    'id' => 13316854,
    'name' => 'ООО "Медтехфарм"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1474435350,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27060642,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27060642',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 27060660,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=27060660',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500382027,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2005007500',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чечня +3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27060642,
        1 => 27060668,
        2 => 27060692,
        3 => 28464864,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27060642,27060668,27060692,28464864',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13316854',
        'method' => 'get',
      ),
    ),
  ),
  2226008365 => 
  array (
    'id' => 13318016,
    'name' => 'ОАО "ДСУ-1"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474441434,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27062106,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27062106',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2226008365',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27062106,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27062106',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13318016',
        'method' => 'get',
      ),
    ),
  ),
  4504005622 => 
  array (
    'id' => 13319768,
    'name' => 'ООО "Кургангазспецстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474449009,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27064788,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27064788',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4504005622',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27064788,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27064788',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13319768',
        'method' => 'get',
      ),
    ),
  ),
  323364171 => 
  array (
    'id' => 13322088,
    'name' => 'ООО Дорстрой',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474456801,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27067384,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27067384',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323364171',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27067384,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27067384',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13322088',
        'method' => 'get',
      ),
    ),
  ),
  1659140940 => 
  array (
    'id' => 13322952,
    'name' => 'ООО «КирСтрой»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474460094,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27068450,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27068450',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522510,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659140940',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27068450,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27068450',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13322952',
        'method' => 'get',
      ),
    ),
  ),
  6828004414 => 
  array (
    'id' => 13327064,
    'name' => 'ООО "Рекламное агентство "Тамбовский волк"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474462167,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27076318,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27076318',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522510,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6828004414',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27076318,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27076318',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13327064',
        'method' => 'get',
      ),
    ),
  ),
  6903036753 => 
  array (
    'id' => 13327096,
    'name' => 'ООО  "Тверская магистраль"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474462446,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27076376,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27076376',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522510,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6903036753',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27076376,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27076376',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13327096',
        'method' => 'get',
      ),
    ),
  ),
  7452075572 => 
  array (
    'id' => 13344908,
    'name' => 'ООО "ТЕХКОМПЛЕКТУРАЛ"',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1474617461,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27102876,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27102876',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7452075572',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Челябинск +5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27102876,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27102876',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13344908',
        'method' => 'get',
      ),
    ),
  ),
  2320185883 => 
  array (
    'id' => 13367680,
    'name' => 'ООО "Су 8"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474893070,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27129250,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27129250',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522510,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2320185883',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27129250,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27129250',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13367680',
        'method' => 'get',
      ),
    ),
  ),
  6317088870 => 
  array (
    'id' => 13367712,
    'name' => 'ООО "Технологии Безопасности"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474893222,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27129294,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27129294',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522510,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317088870',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27129294,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27129294',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13367712',
        'method' => 'get',
      ),
    ),
  ),
  7451199511 => 
  array (
    'id' => 13368026,
    'name' => 'ООО  "АЛЬФА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1474893923,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27129656,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27129656',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451199511',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27129656,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27129656',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13368026',
        'method' => 'get',
      ),
    ),
  ),
  7816210735 => 
  array (
    'id' => 13388308,
    'name' => 'ООО Триумф',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475064096,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27154490,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27154490',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816210735',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27154490,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27154490',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13388308',
        'method' => 'get',
      ),
    ),
  ),
  4909097100 => 
  array (
    'id' => 13393574,
    'name' => 'ООО «КАСАХ»',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1475130230,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27160004,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27160004',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4909097100',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Магаданская область(GMT +11)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27160004,
        1 => 27160032,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27160004,27160032',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13393574',
        'method' => 'get',
      ),
    ),
  ),
  8602173887 => 
  array (
    'id' => 13401468,
    'name' => 'ООО "ЮГРА-ИНВЕСТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475149744,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27168504,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27168504',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602173887',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27168504,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27168504',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13401468',
        'method' => 'get',
      ),
    ),
  ),
  6315655763 => 
  array (
    'id' => 13401660,
    'name' => 'ООО "ИЛТРАСТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475150567,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27168754,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27168754',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6315655763',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+1',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27168754,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27168754',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13401660',
        'method' => 'get',
      ),
    ),
  ),
  7730633898 => 
  array (
    'id' => 13411778,
    'name' => 'ООО Евротекстиль',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475239300,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27182124,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27182124',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730633898',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27182124,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27182124',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13411778',
        'method' => 'get',
      ),
    ),
  ),
  6501160447 => 
  array (
    'id' => 13424272,
    'name' => 'ЗАО ""Тепло Плюс""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475472070,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27193054,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27193054',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501160447',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27193054,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27193054',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13424272',
        'method' => 'get',
      ),
    ),
  ),
  2539095341 => 
  array (
    'id' => 13424686,
    'name' => 'ООО  "БК групп"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1475475652,
    'updated_at' => 1524545380,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27193618,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27193618',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522463,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2539095341',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27193618,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27193618',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13424686',
        'method' => 'get',
      ),
    ),
  ),
  1430010669 => 
  array (
    'id' => 12731523,
    'name' => 'ООО "Чурапчаавтодор"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469076463,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26171555,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26171555',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1430010669',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26171555,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26171555',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12731523',
        'method' => 'get',
      ),
    ),
  ),
  3808012250 => 
  array (
    'id' => 12735677,
    'name' => 'ООО  "Альянс делового сотрудничества"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469099748,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26177371,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26177371',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808012250',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26177371,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26177371',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12735677',
        'method' => 'get',
      ),
    ),
  ),
  5404005483 => 
  array (
    'id' => 12736093,
    'name' => 'ООО "ВЕРШИНА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469101430,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26177927,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26177927',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404005483',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26177927,
        1 => 26242921,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26177927,26242921',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12736093',
        'method' => 'get',
      ),
    ),
  ),
  7810942316 => 
  array (
    'id' => 12743303,
    'name' => 'ООО  "НЬЮЭКОТЕХ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469181133,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26193449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26193449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810942316',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26193449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26193449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12743303',
        'method' => 'get',
      ),
    ),
  ),
  7842501762 => 
  array (
    'id' => 12743785,
    'name' => 'ООО  "Мостодор"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469183311,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26193989,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26193989',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842501762',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26193989,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26193989',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12743785',
        'method' => 'get',
      ),
    ),
  ),
  2539109675 => 
  array (
    'id' => 12758295,
    'name' => 'ООО "БРИЗ-ДВ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469439699,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26235247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26235247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2539109675',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26235247,
        1 => 26340846,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26235247,26340846',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12758295',
        'method' => 'get',
      ),
    ),
  ),
  6683000501 => 
  array (
    'id' => 12758569,
    'name' => 'ООО Олимп',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469440606,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26235607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26235607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6683000501',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26235607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26235607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12758569',
        'method' => 'get',
      ),
    ),
  ),
  2465288924 => 
  array (
    'id' => 12758877,
    'name' => 'ООО "ССК"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469441623,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26235937,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26235937',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465288924',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26235937,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26235937',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12758877',
        'method' => 'get',
      ),
    ),
  ),
  7536066609 => 
  array (
    'id' => 12761269,
    'name' => 'ООО «ЭНЕРГОЖИЛСТРОЙ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469450351,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26238259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26238259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7536066609',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26238259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26238259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12761269',
        'method' => 'get',
      ),
    ),
  ),
  2221221072 => 
  array (
    'id' => 12786832,
    'name' => 'ООО  "СД"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469681666,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26271074,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26271074',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221221072',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26271074,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26271074',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12786832',
        'method' => 'get',
      ),
    ),
  ),
  1435129080 => 
  array (
    'id' => 12787114,
    'name' => 'ООО ПТК Гларимак',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469684724,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26271400,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26271400',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435129080',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26271400,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26271400',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12787114',
        'method' => 'get',
      ),
    ),
  ),
  3827036048 => 
  array (
    'id' => 12790494,
    'name' => 'ООО ИркутскДорСтрой',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469699125,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26281982,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26281982',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3827036048',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26281982,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26281982',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12790494',
        'method' => 'get',
      ),
    ),
  ),
  3702120495 => 
  array (
    'id' => 12791626,
    'name' => 'ООО  ЛЮКС',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469702398,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26283616,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26283616',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702120495',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26283616,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26283616',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12791626',
        'method' => 'get',
      ),
    ),
  ),
  6732060062 => 
  array (
    'id' => 12793188,
    'name' => 'ООО  "СтройПодряд"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469708866,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26285450,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26285450',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6732060062',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26285450,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26285450',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12793188',
        'method' => 'get',
      ),
    ),
  ),
  1203007797 => 
  array (
    'id' => 12802050,
    'name' => 'ООО ЖУК',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469789557,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26295186,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26295186',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1203007797',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26295186,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26295186',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12802050',
        'method' => 'get',
      ),
    ),
  ),
  2538087588 => 
  array (
    'id' => 12802128,
    'name' => 'ООО "РЕДУТ-ДВ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469789785,
    'updated_at' => 1524545381,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26295270,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26295270',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2538087588',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26295270,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26295270',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12802128',
        'method' => 'get',
      ),
    ),
  ),
  2722058243 => 
  array (
    'id' => 12802172,
    'name' => 'ООО "Строительная компания "Восток"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1469789924,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26295308,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26295308',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2722058243',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26295308,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26295308',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12802172',
        'method' => 'get',
      ),
    ),
  ),
  6501247458 => 
  array (
    'id' => 12820710,
    'name' => 'ООО "САХАЛИНРЕМОНТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470036203,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26315496,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26315496',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501247458',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26315496,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26315496',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12820710',
        'method' => 'get',
      ),
    ),
  ),
  3811164132 => 
  array (
    'id' => 12820782,
    'name' => 'ООО "Регион Проект"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470036541,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26315572,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26315572',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811164132',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26315572,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26315572',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12820782',
        'method' => 'get',
      ),
    ),
  ),
  3820007545 => 
  array (
    'id' => 12820862,
    'name' => 'ООО "Черемховоспецстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470036850,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26315674,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26315674',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3820007545',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26315674,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26315674',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12820862',
        'method' => 'get',
      ),
    ),
  ),
  323339778 => 
  array (
    'id' => 12838390,
    'name' => 'ООО  Никас',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470138270,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26387918,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26387918',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323339778',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26387918,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26387918',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12838390',
        'method' => 'get',
      ),
    ),
  ),
  7714243322 => 
  array (
    'id' => 12838598,
    'name' => 'ООО МИГ Проект',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470139042,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26388218,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26388218',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714243322',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26388218,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26388218',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12838598',
        'method' => 'get',
      ),
    ),
  ),
  7534014154 => 
  array (
    'id' => 12847902,
    'name' => 'ООО Крол',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470219350,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26398370,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26398370',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7534014154',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26398370,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26398370',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12847902',
        'method' => 'get',
      ),
    ),
  ),
  1701057141 => 
  array (
    'id' => 12848798,
    'name' => 'ООО "ПРИЗМА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470223282,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26399500,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26399500',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1701057141',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26399500,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26399500',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12848798',
        'method' => 'get',
      ),
    ),
  ),
  3812114423 => 
  array (
    'id' => 12849086,
    'name' => 'ООО "СОЮЗ-ГРАД"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470224421,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26399884,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26399884',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812114423',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26399884,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26399884',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12849086',
        'method' => 'get',
      ),
    ),
  ),
  2465054330 => 
  array (
    'id' => 12849402,
    'name' => 'ООО ПК фирма  АЛЬТАИР',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470225733,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26400218,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26400218',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465054330',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26400218,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26400218',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12849402',
        'method' => 'get',
      ),
    ),
  ),
  2464237268 => 
  array (
    'id' => 12849478,
    'name' => 'ООО Ремонт +',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470225954,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26400284,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26400284',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464237268',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26400284,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26400284',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12849478',
        'method' => 'get',
      ),
    ),
  ),
  5903956477 => 
  array (
    'id' => 12857206,
    'name' => 'ООО  ЛАЙК-СТРОЙ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470305331,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26408984,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26408984',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5903956477',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26408984,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26408984',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12857206',
        'method' => 'get',
      ),
    ),
  ),
  6013000288 => 
  array (
    'id' => 12857220,
    'name' => 'АО  ДСУ-3',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470305404,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26409006,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26409006',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6013000288',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26409006,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26409006',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12857220',
        'method' => 'get',
      ),
    ),
  ),
  3827023708 => 
  array (
    'id' => 12857244,
    'name' => 'ООО "АрмСтрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470305511,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26409194,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26409194',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3827023708',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26409194,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26409194',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12857244',
        'method' => 'get',
      ),
    ),
  ),
  3849050906 => 
  array (
    'id' => 12857276,
    'name' => 'ООО "РСФ "ИдеалСтрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470305638,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26409238,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26409238',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522512,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3849050906',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26409238,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26409238',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12857276',
        'method' => 'get',
      ),
    ),
  ),
  7717026213 => 
  array (
    'id' => 12866244,
    'name' => 'ООО АГРОПРОМИНВЕСТ-Н',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470391016,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26420232,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26420232',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717026213',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26420232,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26420232',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12866244',
        'method' => 'get',
      ),
    ),
  ),
  6155071929 => 
  array (
    'id' => 12882393,
    'name' => 'ООО "РНД СТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470642853,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26438283,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26438283',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6155071929',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26438283,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26438283',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12882393',
        'method' => 'get',
      ),
    ),
  ),
  326489845 => 
  array (
    'id' => 12886883,
    'name' => 'ООО «Стройтех»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470656630,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26443567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26443567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326489845',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26443567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26443567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12886883',
        'method' => 'get',
      ),
    ),
  ),
  6509018636 => 
  array (
    'id' => 12893813,
    'name' => 'ООО Конструктор-Сах',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470726533,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26453647,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26453647',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6509018636',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26453647,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26453647',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12893813',
        'method' => 'get',
      ),
    ),
  ),
  2813009437 => 
  array (
    'id' => 12893857,
    'name' => 'ООО ДСОЛ БЕЛЫЕ ГОРЫ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470726737,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26453705,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26453705',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2813009437',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26453705,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26453705',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12893857',
        'method' => 'get',
      ),
    ),
  ),
  7204194090 => 
  array (
    'id' => 12897903,
    'name' => 'ООО "ПРИЗМАПРИНТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470740332,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26460771,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26460771',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7204194090',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26460771,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26460771',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12897903',
        'method' => 'get',
      ),
    ),
  ),
  5402581428 => 
  array (
    'id' => 12919569,
    'name' => 'ООО "СТРОЙМОДУЛЬ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470829977,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26478963,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26478963',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402581428',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26478963,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26478963',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12919569',
        'method' => 'get',
      ),
    ),
  ),
  326489355 => 
  array (
    'id' => 12926021,
    'name' => 'ООО "БайкалСнабСтрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470887629,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26484879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26484879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '326489355',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26484879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26484879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12926021',
        'method' => 'get',
      ),
    ),
  ),
  2465232008 => 
  array (
    'id' => 12928465,
    'name' => 'ООО "СЭС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470902458,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26510071,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26510071',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 26981917,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=26981917',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500382027,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465232008',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26510071,
        1 => 26981919,
        2 => 30019595,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26510071,26981919,30019595',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12928465',
        'method' => 'get',
      ),
    ),
  ),
  7702131247 => 
  array (
    'id' => 12932089,
    'name' => 'ЗАО "МИХАЙЛОВ И ПАРТНЕРЫ. УПРАВЛЕНИЕ СТРАТЕГИЧЕСКИМИ КОММУНИКАЦИЯМИ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470914196,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26491173,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26491173',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702131247',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26491173,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26491173',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12932089',
        'method' => 'get',
      ),
    ),
  ),
  2723121949 => 
  array (
    'id' => 12937965,
    'name' => 'ООО "ДИКТИС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470982171,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26510505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26510505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723121949',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26510505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26510505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12937965',
        'method' => 'get',
      ),
    ),
  ),
  7906000764 => 
  array (
    'id' => 12938081,
    'name' => 'ООО ПМК "Биробиджанводстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1470982455,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26510615,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26510615',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7906000764',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26510615,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26510615',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12938081',
        'method' => 'get',
      ),
    ),
  ),
  3808219173 => 
  array (
    'id' => 12957225,
    'name' => 'ООО "Сибирские угли"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471240068,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26536019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26536019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808219173',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26536019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26536019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12957225',
        'method' => 'get',
      ),
    ),
  ),
  2466210568 => 
  array (
    'id' => 12957243,
    'name' => '"СТРОЙПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471240330,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26536051,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26536051',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466210568',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26536051,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26536051',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12957243',
        'method' => 'get',
      ),
    ),
  ),
  3808234728 => 
  array (
    'id' => 12957935,
    'name' => 'ООО "Союз 38"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471244277,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26536875,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26536875',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808234728',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26536875,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26536875',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12957935',
        'method' => 'get',
      ),
    ),
  ),
  1435260207 => 
  array (
    'id' => 12974705,
    'name' => 'ООО "Прогресс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471349188,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26555167,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26555167',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435260207',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26555167,
        1 => 26566599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26555167,26566599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12974705',
        'method' => 'get',
      ),
    ),
  ),
  2723130767 => 
  array (
    'id' => 12991947,
    'name' => 'ООО "ДАЛЬИНТЕР-СЕРВИС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471495350,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26580851,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26580851',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723130767',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26580851,
        1 => 26593953,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26580851,26593953',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12991947',
        'method' => 'get',
      ),
    ),
  ),
  3812104048 => 
  array (
    'id' => 12993889,
    'name' => 'ООО ИркутскСтройПроект',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471506764,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26582867,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26582867',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3812104048',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26582867,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26582867',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12993889',
        'method' => 'get',
      ),
    ),
  ),
  3811128342 => 
  array (
    'id' => 12994071,
    'name' => 'ООО "Атмен Хаус"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1471507475,
    'updated_at' => 1524545382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 26583115,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26583115',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522511,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811128342',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 26583115,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=26583115',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12994071',
        'method' => 'get',
      ),
    ),
  ),
  773171858486 => 
  array (
    'id' => 11727712,
    'name' => 'ИП Кривенко Владимир Иванович',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460618390,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24777908,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24777908',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '773171858486',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24777908,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24777908',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11727712',
        'method' => 'get',
      ),
    ),
  ),
  581600731094 => 
  array (
    'id' => 11727778,
    'name' => 'ИП Кирасиров Р.К.',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460618656,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24778012,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778012',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '581600731094',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24778012,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778012',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11727778',
        'method' => 'get',
      ),
    ),
  ),
  7721584852 => 
  array (
    'id' => 11728016,
    'name' => 'ООО "КС-Строй"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460619633,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24778430,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778430',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721584852',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24778430,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778430',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11728016',
        'method' => 'get',
      ),
    ),
  ),
  7602099367 => 
  array (
    'id' => 11728168,
    'name' => 'ООО "ГРАДПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460620284,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24778672,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778672',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7602099367',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24778672,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778672',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11728168',
        'method' => 'get',
      ),
    ),
  ),
  7107518341 => 
  array (
    'id' => 11728312,
    'name' => 'ЗАО ТК "МеталлРесурс""',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460620741,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24778868,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778868',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7107518341',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24778868,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24778868',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11728312',
        'method' => 'get',
      ),
    ),
  ),
  7107001659 => 
  array (
    'id' => 11728452,
    'name' => 'ЗАО "Автошина"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460621197,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24779068,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24779068',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7107001659',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24779068,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24779068',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11728452',
        'method' => 'get',
      ),
    ),
  ),
  6708005960 => 
  array (
    'id' => 11728532,
    'name' => 'ООО "АМАЛЬГАМА К"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460621624,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24779240,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24779240',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6708005960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24779240,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24779240',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11728532',
        'method' => 'get',
      ),
    ),
  ),
  6167072780 => 
  array (
    'id' => 11728598,
    'name' => 'ООО  "ЛОТОС"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460621886,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24779362,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24779362',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6167072780',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24779362,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24779362',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11728598',
        'method' => 'get',
      ),
    ),
  ),
  1841009275 => 
  array (
    'id' => 11730028,
    'name' => 'ООО "Электро-Стандарт"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460627378,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24781962,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24781962',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1841009275',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24781962,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24781962',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11730028',
        'method' => 'get',
      ),
    ),
  ),
  2130096120 => 
  array (
    'id' => 11730308,
    'name' => 'ООО "СК Приоритет"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460628508,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24782478,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24782478',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130096120',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24782478,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24782478',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11730308',
        'method' => 'get',
      ),
    ),
  ),
  2364005601 => 
  array (
    'id' => 11730458,
    'name' => 'ООО  "Фаворит"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460629083,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24782712,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24782712',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2364005601',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24782712,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24782712',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11730458',
        'method' => 'get',
      ),
    ),
  ),
  3525232399 => 
  array (
    'id' => 11730780,
    'name' => 'ООО"Роспроектконсалт"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460630438,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24783502,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24783502',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525232399',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24783502,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24783502',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11730780',
        'method' => 'get',
      ),
    ),
  ),
  7716615897 => 
  array (
    'id' => 11731158,
    'name' => 'ООО "НОРМА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460631891,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24784108,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24784108',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716615897',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24784108,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24784108',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11731158',
        'method' => 'get',
      ),
    ),
  ),
  3665103940 => 
  array (
    'id' => 11731336,
    'name' => 'ООО "АЛЬФА-СТАНДАРТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460632722,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24784424,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24784424',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3665103940',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24784424,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24784424',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11731336',
        'method' => 'get',
      ),
    ),
  ),
  3908604429 => 
  array (
    'id' => 11736730,
    'name' => 'ООО "НКВ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460633173,
    'updated_at' => 1524545384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24789830,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24789830',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3908604429',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24789830,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24789830',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11736730',
        'method' => 'get',
      ),
    ),
  ),
  4703079957 => 
  array (
    'id' => 11737282,
    'name' => 'ООО «ВСР-девелопмент»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460635071,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24790610,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24790610',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4703079957',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24790610,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24790610',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11737282',
        'method' => 'get',
      ),
    ),
  ),
  4708022552 => 
  array (
    'id' => 11737424,
    'name' => 'ООО "Охранная организация "Форсад К"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460635707,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24790874,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24790874',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4708022552',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24790874,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24790874',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11737424',
        'method' => 'get',
      ),
    ),
  ),
  5053066822 => 
  array (
    'id' => 11737746,
    'name' => 'ООО  "Частное охранное предприятие "ПИКЕТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460636944,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 23109212,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=23109212',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 23109210,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=23109210',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5053066822',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 23109212,
        1 => 24791486,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=23109212,24791486',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11737746',
        'method' => 'get',
      ),
    ),
  ),
  3329000218 => 
  array (
    'id' => 11737982,
    'name' => 'ООО ПИ  "ВЛАДИМИРАВТОДОРПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460638009,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24792008,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24792008',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3329000218',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24792008,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24792008',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11737982',
        'method' => 'get',
      ),
    ),
  ),
  5753200030 => 
  array (
    'id' => 11738318,
    'name' => 'ООО  Лаборатория "СанаТест"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460639824,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24792872,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24792872',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5753200030',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24792872,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24792872',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738318',
        'method' => 'get',
      ),
    ),
  ),
  6154131734 => 
  array (
    'id' => 11738414,
    'name' => '"ООО ""КОНСТАНТА РАВНОВЕСИЯ"""',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460640159,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793048,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793048',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6154131734',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793048,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793048',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738414',
        'method' => 'get',
      ),
    ),
  ),
  5007090186 => 
  array (
    'id' => 11738504,
    'name' => 'ООО "ГУРМАН ШЕФ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460640542,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793214,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793214',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5007090186',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793214,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793214',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738504',
        'method' => 'get',
      ),
    ),
  ),
  6154140263 => 
  array (
    'id' => 11738508,
    'name' => 'ООО  «ЭВЕРЕСТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460640549,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793218,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793218',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6154140263',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793218,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793218',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738508',
        'method' => 'get',
      ),
    ),
  ),
  5040085793 => 
  array (
    'id' => 11738728,
    'name' => 'ООО ТИТАН-СТРОЙ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460641260,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793484,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793484',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040085793',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793484,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793484',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738728',
        'method' => 'get',
      ),
    ),
  ),
  5050121095 => 
  array (
    'id' => 11738790,
    'name' => 'ООО «ФС-Сервис+»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460641487,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793582,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793582',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5050121095',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793582,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793582',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738790',
        'method' => 'get',
      ),
    ),
  ),
  5074021090 => 
  array (
    'id' => 11738882,
    'name' => 'ООО "ПОДОЛЬСКИЕ МЕТАЛЛЫ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460641825,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793752,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793752',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5074021090',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793752,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793752',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738882',
        'method' => 'get',
      ),
    ),
  ),
  6154555934 => 
  array (
    'id' => 11738910,
    'name' => 'ООО "Строй-Мир"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460641925,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793782,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793782',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6154555934',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793782,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793782',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738910',
        'method' => 'get',
      ),
    ),
  ),
  7705018916 => 
  array (
    'id' => 11738950,
    'name' => 'ОАО "МЕТРОГИПРОТРАНС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460642103,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24793872,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793872',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705018916',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24793872,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24793872',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11738950',
        'method' => 'get',
      ),
    ),
  ),
  7706156517 => 
  array (
    'id' => 11739052,
    'name' => 'АНО «Национальное информационное агентство «Природные ресурсы»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460642593,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24794076,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24794076',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706156517',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24794076,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24794076',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11739052',
        'method' => 'get',
      ),
    ),
  ),
  8606001078 => 
  array (
    'id' => 11748578,
    'name' => 'Производственный кооператив Будивельник',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460698109,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24805002,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24805002',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8606001078',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24805002,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24805002',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11748578',
        'method' => 'get',
      ),
    ),
  ),
  7207013520 => 
  array (
    'id' => 11749186,
    'name' => 'ООО  "АРАРАТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460701016,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24805728,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24805728',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7207013520',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24805728,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24805728',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11749186',
        'method' => 'get',
      ),
    ),
  ),
  7203321348 => 
  array (
    'id' => 11749342,
    'name' => 'ООО "ЗАВОД ЖБИ-3"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460701917,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24805920,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24805920',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203321348',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24805920,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24805920',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11749342',
        'method' => 'get',
      ),
    ),
  ),
  6319158890 => 
  array (
    'id' => 11749526,
    'name' => 'ООО СК Лидер',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460702800,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24806200,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24806200',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319158890',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24806200,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24806200',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11749526',
        'method' => 'get',
      ),
    ),
  ),
  6679014082 => 
  array (
    'id' => 11750240,
    'name' => 'ООО "Автодорзнак"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460705323,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24807254,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24807254',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6679014082',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24807254,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24807254',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11750240',
        'method' => 'get',
      ),
    ),
  ),
  6671323280 => 
  array (
    'id' => 11750308,
    'name' => 'ООО «Стройинициатива»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460705561,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24807336,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24807336',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671323280',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24807336,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24807336',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11750308',
        'method' => 'get',
      ),
    ),
  ),
  6229033462 => 
  array (
    'id' => 11750720,
    'name' => 'ООО «Техкорд»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460706892,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24807898,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24807898',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 24808228,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=24808228',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6229033462',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24807898,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24807898',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11750720',
        'method' => 'get',
      ),
    ),
  ),
  4807001264 => 
  array (
    'id' => 11751528,
    'name' => 'ОАО "ПМК Строитель"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460709683,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24809090,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24809090',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4807001264',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24809090,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24809090',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11751528',
        'method' => 'get',
      ),
    ),
  ),
  6163158644 => 
  array (
    'id' => 11752972,
    'name' => 'ООО "Агентство детского отдыха "Лидер"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460714734,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24811254,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24811254',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6163158644',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24811254,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24811254',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11752972',
        'method' => 'get',
      ),
    ),
  ),
  6230093040 => 
  array (
    'id' => 11753176,
    'name' => 'ООО «РОСЬ-Инжиниринг»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460715358,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24811542,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24811542',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6230093040',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24811542,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24811542',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11753176',
        'method' => 'get',
      ),
    ),
  ),
  7203336908 => 
  array (
    'id' => 11753234,
    'name' => 'ООО "АНГАРА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460715585,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24811634,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24811634',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203336908',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24811634,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24811634',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11753234',
        'method' => 'get',
      ),
    ),
  ),
  6625063502 => 
  array (
    'id' => 11753336,
    'name' => 'ООО "ТДЦ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460715907,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24812268,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24812268',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6625063502',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24812268,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24812268',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11753336',
        'method' => 'get',
      ),
    ),
  ),
  6903002602 => 
  array (
    'id' => 11753390,
    'name' => 'ООО "ДОРОЖНОЕ УПРАВЛЕНИЕ ГРАЖДАНСТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460716196,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24812346,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24812346',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6903002602',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24812346,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24812346',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11753390',
        'method' => 'get',
      ),
    ),
  ),
  4501157401 => 
  array (
    'id' => 11753572,
    'name' => 'ООО "ГПИмясомолпром"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460716929,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24812694,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24812694',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4501157401',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24812694,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24812694',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11753572',
        'method' => 'get',
      ),
    ),
  ),
  290214346320 => 
  array (
    'id' => 11754404,
    'name' => 'ИП Петров Андрей Александрович',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460720936,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24814142,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814142',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '290214346320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24814142,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814142',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11754404',
        'method' => 'get',
      ),
    ),
  ),
  7725768160 => 
  array (
    'id' => 11714264,
    'name' => 'ООО "ПрофЭксп"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460526198,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24756258,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756258',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725768160',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24756258,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756258',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11714264',
        'method' => 'get',
      ),
    ),
  ),
  7717663406 => 
  array (
    'id' => 11754554,
    'name' => 'ООО «ВМС Инжиниринг»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460721514,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24814348,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814348',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7717663406',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24814348,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814348',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11754554',
        'method' => 'get',
      ),
    ),
  ),
  4825007124 => 
  array (
    'id' => 11754562,
    'name' => 'ООО "МАГ+С"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460721539,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24814360,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814360',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825007124',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24814360,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814360',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11754562',
        'method' => 'get',
      ),
    ),
  ),
  7327021386 => 
  array (
    'id' => 11754708,
    'name' => 'ООО Стройцентрмонтаж',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1460722094,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24814550,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814550',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7327021386',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24814550,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814550',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11754708',
        'method' => 'get',
      ),
    ),
  ),
  5022013796 => 
  array (
    'id' => 11754904,
    'name' => 'ОАО "ПОРТ КОЛОМНА"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460723028,
    'updated_at' => 1524545385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24814864,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814864',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522666,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5022013796',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24814864,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24814864',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11754904',
        'method' => 'get',
      ),
    ),
  ),
  771605004180 => 
  array (
    'id' => 11710102,
    'name' => 'ИП Котюков Николай Алексеевич',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460465677,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24750158,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750158',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '771605004180',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24750158,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750158',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11710102',
        'method' => 'get',
      ),
    ),
  ),
  7703635737 => 
  array (
    'id' => 11710204,
    'name' => 'ООО "СОФТКОНСАЛТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460466144,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24750328,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750328',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703635737',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24750328,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750328',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11710204',
        'method' => 'get',
      ),
    ),
  ),
  7806335502 => 
  array (
    'id' => 11710356,
    'name' => 'ООО "Центр Авро"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460466812,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24750598,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750598',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806335502',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24750598,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750598',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11710356',
        'method' => 'get',
      ),
    ),
  ),
  7802815755 => 
  array (
    'id' => 11710466,
    'name' => '"ООО ""Статус"""',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460467422,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24750790,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750790',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802815755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24750790,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24750790',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11710466',
        'method' => 'get',
      ),
    ),
  ),
  7839315156 => 
  array (
    'id' => 11710624,
    'name' => 'ООО"БАЛТМОСТПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460468227,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24751088,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24751088',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839315156',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24751088,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24751088',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11710624',
        'method' => 'get',
      ),
    ),
  ),
  7727622688 => 
  array (
    'id' => 11710798,
    'name' => 'OOO «ВиК-Строй»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460469041,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24751406,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24751406',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727622688',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24751406,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24751406',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11710798',
        'method' => 'get',
      ),
    ),
  ),
  8603190476 => 
  array (
    'id' => 11714176,
    'name' => 'ООО "Комплект-Сервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460525343,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24756140,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756140',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603190476',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24756140,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756140',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11714176',
        'method' => 'get',
      ),
    ),
  ),
  7438019424 => 
  array (
    'id' => 11714336,
    'name' => 'ООО Научно-производственное предприятие "ЭнергоТехСервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460526783,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24756360,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756360',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7438019424',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24756360,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756360',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11714336',
        'method' => 'get',
      ),
    ),
  ),
  7203253218 => 
  array (
    'id' => 11714416,
    'name' => 'ООО "Роспроект модернизация"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460527356,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24756466,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756466',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203253218',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24756466,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756466',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11714416',
        'method' => 'get',
      ),
    ),
  ),
  6671034810 => 
  array (
    'id' => 11714570,
    'name' => 'ООО ТОРГОВЫЙ ДОМ  ВЕКТОР',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460527935,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24756664,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756664',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6671034810',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24756664,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24756664',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11714570',
        'method' => 'get',
      ),
    ),
  ),
  6659201278 => 
  array (
    'id' => 11715138,
    'name' => 'ООО "СтройСервисРесурс"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460529487,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24757402,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24757402',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6659201278',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24757402,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24757402',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11715138',
        'method' => 'get',
      ),
    ),
  ),
  6449972669 => 
  array (
    'id' => 11715700,
    'name' => 'ООО "Газэнергопром"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460531633,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24758304,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24758304',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6449972669',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24758304,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24758304',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11715700',
        'method' => 'get',
      ),
    ),
  ),
  6318228135 => 
  array (
    'id' => 11715902,
    'name' => 'ООО "Стройград"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460532237,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24758634,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24758634',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318228135',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24758634,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24758634',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11715902',
        'method' => 'get',
      ),
    ),
  ),
  5614051969 => 
  array (
    'id' => 11716260,
    'name' => 'ООО Строй-Сити',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460533399,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24759904,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24759904',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5614051969',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24759904,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24759904',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11716260',
        'method' => 'get',
      ),
    ),
  ),
  5611026996 => 
  array (
    'id' => 11716522,
    'name' => 'ООО «Оренкарт»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460534166,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24760288,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24760288',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5611026996',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24760288,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24760288',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11716522',
        'method' => 'get',
      ),
    ),
  ),
  4501206786 => 
  array (
    'id' => 11716684,
    'name' => 'ООО "Торговый дом "Айсберг"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460534690,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24760592,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24760592',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4501206786',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24760592,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24760592',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11716684',
        'method' => 'get',
      ),
    ),
  ),
  5244024189 => 
  array (
    'id' => 11716742,
    'name' => 'ООО "Завод автомобильных фургонов"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460534859,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24760698,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24760698',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5244024189',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24760698,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24760698',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11716742',
        'method' => 'get',
      ),
    ),
  ),
  268069430 => 
  array (
    'id' => 11717322,
    'name' => 'ООО "ТАШИР"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460536523,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24761578,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24761578',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '268069430',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24761578,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24761578',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11717322',
        'method' => 'get',
      ),
    ),
  ),
  277055850 => 
  array (
    'id' => 11717502,
    'name' => 'ООО "ГРУППА КОМПАНИЙ СУ-10"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460537287,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24761916,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24761916',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '277055850',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24761916,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24761916',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11717502',
        'method' => 'get',
      ),
    ),
  ),
  5615018393 => 
  array (
    'id' => 11717706,
    'name' => 'ООО Управление механизации-6',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460537886,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24762238,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24762238',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5615018393',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24762238,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24762238',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11717706',
        'method' => 'get',
      ),
    ),
  ),
  7205011895 => 
  array (
    'id' => 11718578,
    'name' => 'ООО Детский спортивно-оздоровительный лагерь  Дружба',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460540482,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24763554,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24763554',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7205011895',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24763554,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24763554',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11718578',
        'method' => 'get',
      ),
    ),
  ),
  3525306629 => 
  array (
    'id' => 11718844,
    'name' => 'ООО АВТОГРАД',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460541390,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24764034,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24764034',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525306629',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24764034,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24764034',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11718844',
        'method' => 'get',
      ),
    ),
  ),
  616270186494 => 
  array (
    'id' => 11719606,
    'name' => 'ИП Овсянникова Евгения Игоревна',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460543168,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24765474,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24765474',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '616270186494',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24765474,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24765474',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11719606',
        'method' => 'get',
      ),
    ),
  ),
  504204677384 => 
  array (
    'id' => 11719712,
    'name' => 'ИП Приходько Александр Александрович',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460543612,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24765660,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24765660',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '504204677384',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24765660,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24765660',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11719712',
        'method' => 'get',
      ),
    ),
  ),
  7842529630 => 
  array (
    'id' => 11719926,
    'name' => 'ООО "АРИНА"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460544465,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24765968,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24765968',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842529630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24765968,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24765968',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11719926',
        'method' => 'get',
      ),
    ),
  ),
  7841011370 => 
  array (
    'id' => 11720050,
    'name' => 'ООО "СОЮЗСТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460544850,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24766158,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24766158',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841011370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24766158,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24766158',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11720050',
        'method' => 'get',
      ),
    ),
  ),
  7816591417 => 
  array (
    'id' => 11720256,
    'name' => '"ГЕФЕСТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460545733,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24766512,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24766512',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816591417',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24766512,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24766512',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11720256',
        'method' => 'get',
      ),
    ),
  ),
  7814107771 => 
  array (
    'id' => 11720542,
    'name' => 'ЗАО  «ИЛН Системс»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460546711,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24766944,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24766944',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814107771',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24766944,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24766944',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11720542',
        'method' => 'get',
      ),
    ),
  ),
  7810816706 => 
  array (
    'id' => 11720708,
    'name' => 'ООО "Ультрасаунд-Мед"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460547232,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24767244,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767244',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810816706',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24767244,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767244',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11720708',
        'method' => 'get',
      ),
    ),
  ),
  7816354286 => 
  array (
    'id' => 11720830,
    'name' => 'ООО «Бранд-Мастер»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460547659,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24767468,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767468',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816354286',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24767468,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767468',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11720830',
        'method' => 'get',
      ),
    ),
  ),
  471101745053 => 
  array (
    'id' => 11721008,
    'name' => 'ИП  Никонов Андрей Александрович',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460548308,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24767760,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767760',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '471101745053',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24767760,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767760',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11721008',
        'method' => 'get',
      ),
    ),
  ),
  526311728640 => 
  array (
    'id' => 11721142,
    'name' => 'ИП  Ругалев Сергей Юрьевич',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460548887,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24767986,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767986',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '526311728640',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24767986,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24767986',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11721142',
        'method' => 'get',
      ),
    ),
  ),
  7806355756 => 
  array (
    'id' => 11721478,
    'name' => 'ЗАО "Санкт-Петербургский институт фармации"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460550221,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24768530,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24768530',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806355756',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24768530,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24768530',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11721478',
        'method' => 'get',
      ),
    ),
  ),
  7805509308 => 
  array (
    'id' => 11721560,
    'name' => '7150544@mail.ru',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460550657,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24768706,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24768706',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805509308',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24768706,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24768706',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11721560',
        'method' => 'get',
      ),
    ),
  ),
  7804388347 => 
  array (
    'id' => 11721884,
    'name' => 'ООО «Проектно-изыскательская компания «Техпроект»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460551847,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24769272,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769272',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804388347',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24769272,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769272',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11721884',
        'method' => 'get',
      ),
    ),
  ),
  7804388139 => 
  array (
    'id' => 11721988,
    'name' => 'ООО "Проектное строительное бюро"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460552232,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24769452,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769452',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804388139',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24769452,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769452',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11721988',
        'method' => 'get',
      ),
    ),
  ),
  7733713213 => 
  array (
    'id' => 11722142,
    'name' => 'ООО "СтройЭнерджи"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460552786,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24769694,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769694',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733713213',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24769694,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769694',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11722142',
        'method' => 'get',
      ),
    ),
  ),
  7731177581 => 
  array (
    'id' => 11722228,
    'name' => 'ООО «АРД САТКОМ СЕРВИС»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460553151,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24769834,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769834',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731177581',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24769834,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24769834',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11722228',
        'method' => 'get',
      ),
    ),
  ),
  7728181563 => 
  array (
    'id' => 11722378,
    'name' => 'ООО "ПРАЙМ ГРУП"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460553829,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24770080,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770080',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728181563',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24770080,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770080',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11722378',
        'method' => 'get',
      ),
    ),
  ),
  7724838223 => 
  array (
    'id' => 11722592,
    'name' => 'ООО «Фармконтракт»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460554783,
    'updated_at' => 1524545387,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24770496,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770496',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724838223',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24770496,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770496',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11722592',
        'method' => 'get',
      ),
    ),
  ),
  7723838809 => 
  array (
    'id' => 11722668,
    'name' => 'ООО СК  Стройинвест-1',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460555095,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24770632,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770632',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723838809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24770632,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770632',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11722668',
        'method' => 'get',
      ),
    ),
  ),
  7722843387 => 
  array (
    'id' => 11722770,
    'name' => 'ООО "Азимут"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460555573,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24770850,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770850',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722843387',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24770850,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24770850',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11722770',
        'method' => 'get',
      ),
    ),
  ),
  6315944860 => 
  array (
    'id' => 11726326,
    'name' => 'ООО организация спасания на водах "ОСВОД"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460611421,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24775998,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24775998',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6315944860',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24775998,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24775998',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11726326',
        'method' => 'get',
      ),
    ),
  ),
  2711009371 => 
  array (
    'id' => 11726410,
    'name' => 'ООО "Рента"',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1460612007,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24776116,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776116',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2711009371',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Хабаровский край(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24776116,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776116',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11726410',
        'method' => 'get',
      ),
    ),
  ),
  6317099222 => 
  array (
    'id' => 11726418,
    'name' => 'ООО  "ВИСТ-ПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460612070,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24776124,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776124',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317099222',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24776124,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776124',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11726418',
        'method' => 'get',
      ),
    ),
  ),
  6658097031 => 
  array (
    'id' => 11726466,
    'name' => 'АО  "Промышленно-строительная компания "Урал-Альянс"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460612613,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24776190,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776190',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658097031',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24776190,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776190',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11726466',
        'method' => 'get',
      ),
    ),
  ),
  6674322728 => 
  array (
    'id' => 11726908,
    'name' => 'ООО &#39;МВ-ТРЕЙД&#39;',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460614849,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24776772,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776772',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6674322728',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24776772,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776772',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11726908',
        'method' => 'get',
      ),
    ),
  ),
  7203264749 => 
  array (
    'id' => 11727036,
    'name' => 'ООО "Паллада".',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460615495,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24776930,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776930',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522668,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203264749',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24776930,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24776930',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11727036',
        'method' => 'get',
      ),
    ),
  ),
  741806351755 => 
  array (
    'id' => 11727202,
    'name' => 'ИП Бельков Сергей Валерьевич',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460616227,
    'updated_at' => 1524545388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24777152,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24777152',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522667,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '741806351755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24777152,
        1 => 24806136,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24777152,24806136',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11727202',
        'method' => 'get',
      ),
    ),
  ),
  7722319952 => 
  array (
    'id' => 11695148,
    'name' => 'ФГБУ "Рослесинфорг"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460373421,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24728586,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728586',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722319952',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24728586,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728586',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11695148',
        'method' => 'get',
      ),
    ),
  ),
  7710699393 => 
  array (
    'id' => 11695590,
    'name' => 'ООО "ГЕОИНФОРМ+"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460373874,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24728846,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728846',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710699393',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24728846,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728846',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11695590',
        'method' => 'get',
      ),
    ),
  ),
  7202160232 => 
  array (
    'id' => 11695978,
    'name' => 'ООО "СТРОЙПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460374585,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24729344,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24729344',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7202160232',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24729344,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24729344',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11695978',
        'method' => 'get',
      ),
    ),
  ),
  6952043457 => 
  array (
    'id' => 11696248,
    'name' => 'ООО "Центрстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460375581,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24729842,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24729842',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6952043457',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24729842,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24729842',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11696248',
        'method' => 'get',
      ),
    ),
  ),
  6732030332 => 
  array (
    'id' => 11696406,
    'name' => '"Интеллектуальные машины"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460376064,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24730106,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24730106',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6732030332',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24730106,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24730106',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11696406',
        'method' => 'get',
      ),
    ),
  ),
  6165136798 => 
  array (
    'id' => 11697092,
    'name' => 'ООО ХЭЛТ',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460377179,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24731230,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24731230',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165136798',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24731230,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24731230',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11697092',
        'method' => 'get',
      ),
    ),
  ),
  6164295555 => 
  array (
    'id' => 11697926,
    'name' => 'ООО ПАРУС',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460378866,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24732596,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24732596',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6164295555',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24732596,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24732596',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11697926',
        'method' => 'get',
      ),
    ),
  ),
  6102058699 => 
  array (
    'id' => 11698082,
    'name' => 'ООО  ТСС РОСТОВ',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460379322,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24732806,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24732806',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6102058699',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24732806,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24732806',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11698082',
        'method' => 'get',
      ),
    ),
  ),
  5836601606 => 
  array (
    'id' => 11698232,
    'name' => 'ЗАО "Пензенская горэлектросеть"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460379705,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24732982,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24732982',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5836601606',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24732982,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24732982',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11698232',
        'method' => 'get',
      ),
    ),
  ),
  5263027474 => 
  array (
    'id' => 11698362,
    'name' => 'ООО "Ректайм"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460380073,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24733146,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24733146',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5263027474',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24733146,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24733146',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11698362',
        'method' => 'get',
      ),
    ),
  ),
  753500492075 => 
  array (
    'id' => 11698388,
    'name' => 'ИП Мкоян Спартак Сережович',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1460380216,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24733216,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24733216',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382028,
    'closest_task_at' => 1502074800,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '753500492075',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чита +9',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24733216,
        1 => 24755486,
        2 => 29542487,
        3 => 29579017,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24733216,24755486,29542487,29579017',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11698388',
        'method' => 'get',
      ),
    ),
  ),
  3918504367 => 
  array (
    'id' => 11698476,
    'name' => 'ООО "ЦКМ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460380513,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24733366,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24733366',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3918504367',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24733366,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24733366',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11698476',
        'method' => 'get',
      ),
    ),
  ),
  3444014787 => 
  array (
    'id' => 11699158,
    'name' => 'ОАО "ПРИВОЛЖТРАНССТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460382257,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24734286,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24734286',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3444014787',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24734286,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24734286',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11699158',
        'method' => 'get',
      ),
    ),
  ),
  8709010677 => 
  array (
    'id' => 11702142,
    'name' => 'ООО "Анжи"',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1460437495,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24738518,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738518',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8709010677',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Чукотский АО(GMT +12)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24738518,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738518',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11702142',
        'method' => 'get',
      ),
    ),
  ),
  7207014203 => 
  array (
    'id' => 11702248,
    'name' => 'ООО "СИБИРЯЧКА"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460438447,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24738652,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738652',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7207014203',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24738652,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738652',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11702248',
        'method' => 'get',
      ),
    ),
  ),
  6678051250 => 
  array (
    'id' => 11702344,
    'name' => 'ООО «БИГРОКОТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460438879,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24738772,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738772',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678051250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24738772,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738772',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11702344',
        'method' => 'get',
      ),
    ),
  ),
  6619015331 => 
  array (
    'id' => 11702396,
    'name' => 'ООО "МСК АртСтрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460439207,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24738832,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738832',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6619015331',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24738832,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738832',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11702396',
        'method' => 'get',
      ),
    ),
  ),
  5610122979 => 
  array (
    'id' => 11702430,
    'name' => 'ООО  "Дилерский центр "Автосалон-2000"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460439660,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24738870,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738870',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5610122979',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24738870,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24738870',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11702430',
        'method' => 'get',
      ),
    ),
  ),
  6670243472 => 
  array (
    'id' => 11702814,
    'name' => 'ООО "Локаторная техника"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460441607,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24739358,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739358',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670243472',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24739358,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739358',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11702814',
        'method' => 'get',
      ),
    ),
  ),
  6678013343 => 
  array (
    'id' => 11702976,
    'name' => 'ООО "Комстар"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460442390,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24739550,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739550',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 1500879600,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678013343',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24739550,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739550',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11702976',
        'method' => 'get',
      ),
    ),
  ),
  8601016641 => 
  array (
    'id' => 11703118,
    'name' => 'ООО ЮПС',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460442919,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24739716,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739716',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8601016641',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24739716,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739716',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11703118',
        'method' => 'get',
      ),
    ),
  ),
  7733816177 => 
  array (
    'id' => 11703222,
    'name' => '"Фармэксперт"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460443311,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24739838,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739838',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733816177',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24739838,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24739838',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11703222',
        'method' => 'get',
      ),
    ),
  ),
  6670398130 => 
  array (
    'id' => 11703514,
    'name' => 'ООО "МИРУС МЕДИКАЛ УРАЛ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460444467,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24740232,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24740232',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6670398130',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24740232,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24740232',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11703514',
        'method' => 'get',
      ),
    ),
  ),
  5611033087 => 
  array (
    'id' => 11703748,
    'name' => 'ООО «Ремстройсервис»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460445335,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24740774,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24740774',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5611033087',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24740774,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24740774',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11703748',
        'method' => 'get',
      ),
    ),
  ),
  5260313854 => 
  array (
    'id' => 11704040,
    'name' => 'ООО "Регал"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460445670,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24741100,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24741100',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5260313854',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24741100,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24741100',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11704040',
        'method' => 'get',
      ),
    ),
  ),
  7735064010 => 
  array (
    'id' => 11704144,
    'name' => 'ООО Лаборатория Электроники "ЭлекЖест"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460446032,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24741252,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24741252',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735064010',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24741252,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24741252',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11704144',
        'method' => 'get',
      ),
    ),
  ),
  7728644821 => 
  array (
    'id' => 11704490,
    'name' => 'ООО Мелитэк',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460447156,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24741916,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24741916',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728644821',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24741916,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24741916',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11704490',
        'method' => 'get',
      ),
    ),
  ),
  7729634223 => 
  array (
    'id' => 11704794,
    'name' => 'ООО «Биотехмед»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460448122,
    'updated_at' => 1524545389,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24742334,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24742334',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729634223',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24742334,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24742334',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11704794',
        'method' => 'get',
      ),
    ),
  ),
  6672359641 => 
  array (
    'id' => 11689430,
    'name' => 'ООО "Компания "Реалтима"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460355698,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24720256,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24720256',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672359641',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24720256,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24720256',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11689430',
        'method' => 'get',
      ),
    ),
  ),
  7709446260 => 
  array (
    'id' => 11705230,
    'name' => 'ООО "Кордис Лайн"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460449615,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24743020,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24743020',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709446260',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24743020,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24743020',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11705230',
        'method' => 'get',
      ),
    ),
  ),
  5920023047 => 
  array (
    'id' => 11705384,
    'name' => 'ООО  Ремонт и капитальное строительство',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460450161,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24743246,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24743246',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5920023047',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24743246,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24743246',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11705384',
        'method' => 'get',
      ),
    ),
  ),
  235200538640 => 
  array (
    'id' => 11706062,
    'name' => 'ИП Назаренко Наталья Петровна',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460452443,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24744224,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24744224',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '235200538640',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24744224,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24744224',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11706062',
        'method' => 'get',
      ),
    ),
  ),
  7722860417 => 
  array (
    'id' => 11706860,
    'name' => 'АО  "ИПК "Финвал"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460454418,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24745146,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745146',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722860417',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24745146,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745146',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11706860',
        'method' => 'get',
      ),
    ),
  ),
  7710354624 => 
  array (
    'id' => 11707008,
    'name' => 'ООО "РЕГИОНКОМ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460454796,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24745352,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745352',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7710354624',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24745352,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745352',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11707008',
        'method' => 'get',
      ),
    ),
  ),
  7604244250 => 
  array (
    'id' => 11707180,
    'name' => 'ООО "Ласхорн Груп"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460455261,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24745602,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745602',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7604244250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24745602,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745602',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11707180',
        'method' => 'get',
      ),
    ),
  ),
  7603063490 => 
  array (
    'id' => 11707312,
    'name' => 'ООО «Импульс»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460455651,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24745792,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745792',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7603063490',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24745792,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24745792',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11707312',
        'method' => 'get',
      ),
    ),
  ),
  7325130809 => 
  array (
    'id' => 11660697,
    'name' => 'ООО «ПРО-СТРОЙИНВЕСТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460026413,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24681879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24681879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7325130809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24681879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24681879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11660697',
        'method' => 'get',
      ),
    ),
  ),
  6452105384 => 
  array (
    'id' => 11707748,
    'name' => 'ООО  "ВОДСТРОЙ-А"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460456914,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24746386,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24746386',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6452105384',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24746386,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24746386',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11707748',
        'method' => 'get',
      ),
    ),
  ),
  6165560823 => 
  array (
    'id' => 11707976,
    'name' => 'ООО "Роставтопром"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460457634,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24746722,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24746722',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165560823',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24746722,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24746722',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11707976',
        'method' => 'get',
      ),
    ),
  ),
  6165136396 => 
  array (
    'id' => 11708286,
    'name' => 'ООО "ДОНДОРПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460458507,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24747186,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24747186',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165136396',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24747186,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24747186',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11708286',
        'method' => 'get',
      ),
    ),
  ),
  5828004042 => 
  array (
    'id' => 11708788,
    'name' => 'ООО Пачелмская ДПМК',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460460441,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24748028,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748028',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5828004042',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24748028,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748028',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11708788',
        'method' => 'get',
      ),
    ),
  ),
  5445264180 => 
  array (
    'id' => 11708852,
    'name' => 'ООО «Ортоклуб»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460460711,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24748144,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748144',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5445264180',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24748144,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748144',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11708852',
        'method' => 'get',
      ),
    ),
  ),
  5257035967 => 
  array (
    'id' => 11708956,
    'name' => 'НЕКОММЕРЧЕСКОЕ ПАРТНЕРСТВО "СТУДИЯ ТАНЦЕВАЛЬНО-РАЗВЛЕКАТЕЛЬНЫХ ПРОГРАММ "ВАГАНТЫ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460461107,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24748330,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748330',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5257035967',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24748330,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748330',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11708956',
        'method' => 'get',
      ),
    ),
  ),
  5039009519 => 
  array (
    'id' => 11709070,
    'name' => 'ООО Новая медицинская компания',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460461597,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24748538,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748538',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5039009519',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24748538,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748538',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11709070',
        'method' => 'get',
      ),
    ),
  ),
  1307077527 => 
  array (
    'id' => 11709280,
    'name' => 'ООО Монтажремстрой',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460462644,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24748908,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748908',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1307077527',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24748908,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24748908',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11709280',
        'method' => 'get',
      ),
    ),
  ),
  503410442185 => 
  array (
    'id' => 11709578,
    'name' => 'ИП Геворгян Меружан Сергеевич',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460463745,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24749410,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749410',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '503410442185',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24749410,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749410',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11709578',
        'method' => 'get',
      ),
    ),
  ),
  7707030771 => 
  array (
    'id' => 11709664,
    'name' => 'АО "ОФИС ПРЕМЬЕР"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460464096,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24749552,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749552',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707030771',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24749552,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749552',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11709664',
        'method' => 'get',
      ),
    ),
  ),
  7715964334 => 
  array (
    'id' => 11709872,
    'name' => 'ООО«ТрансРегионСервис»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460464810,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24749802,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749802',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715964334',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24749802,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749802',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11709872',
        'method' => 'get',
      ),
    ),
  ),
  7814372868 => 
  array (
    'id' => 11709960,
    'name' => 'ООО  "СОЮЗ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460465111,
    'updated_at' => 1524545390,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24749924,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749924',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522669,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814372868',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24749924,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24749924',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11709960',
        'method' => 'get',
      ),
    ),
  ),
  6325031764 => 
  array (
    'id' => 11647561,
    'name' => 'ООО  "КТВ - ЛУЧ "',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459944312,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24663535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24663535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6325031764',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24663535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24663535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11647561',
        'method' => 'get',
      ),
    ),
  ),
  6678054773 => 
  array (
    'id' => 11648007,
    'name' => 'ООО «Тренд-Строй»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459945583,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24664079,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24664079',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678054773',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24664079,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24664079',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11648007',
        'method' => 'get',
      ),
    ),
  ),
  4825113556 => 
  array (
    'id' => 11649065,
    'name' => 'ООО"АнРав"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459949493,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24666113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24666113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825113556',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24666113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24666113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11649065',
        'method' => 'get',
      ),
    ),
  ),
  3702569361 => 
  array (
    'id' => 11656295,
    'name' => 'ООО "Классик Текстиль"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460010707,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24674643,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24674643',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702569361',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24674643,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24674643',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11656295',
        'method' => 'get',
      ),
    ),
  ),
  5919012756 => 
  array (
    'id' => 11656983,
    'name' => 'ООО "Камастрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460013749,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24675795,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24675795',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5919012756',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24675795,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24675795',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11656983',
        'method' => 'get',
      ),
    ),
  ),
  6318188203 => 
  array (
    'id' => 11657455,
    'name' => 'ООО «АгроИмпорт»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460015602,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24676463,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24676463',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318188203',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24676463,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24676463',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11657455',
        'method' => 'get',
      ),
    ),
  ),
  6376023497 => 
  array (
    'id' => 11657607,
    'name' => 'ООО "Гермес"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460016094,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24676633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24676633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6376023497',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+1',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24676633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24676633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11657607',
        'method' => 'get',
      ),
    ),
  ),
  6682005465 => 
  array (
    'id' => 11658469,
    'name' => 'ООО "Новоуральский асфальтобетонный завод"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460018974,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24677927,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24677927',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6682005465',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24677927,
        1 => 24690812,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24677927,24690812',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11658469',
        'method' => 'get',
      ),
    ),
  ),
  7842318125 => 
  array (
    'id' => 11658639,
    'name' => 'ООО «Производственно-торговая фирма «ЕВРОТЕСТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460019704,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24678263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24678263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842318125',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24678263,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24678263',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11658639',
        'method' => 'get',
      ),
    ),
  ),
  265014866 => 
  array (
    'id' => 11659121,
    'name' => 'ООО санаторий-профилакторий "Бодрость" ДО ОАО "Стронег"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460021620,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24679053,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24679053',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '265014866',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24679053,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24679053',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11659121',
        'method' => 'get',
      ),
    ),
  ),
  270000026 => 
  array (
    'id' => 11659673,
    'name' => 'МУП Фабрика химчистки, крашения и стирки  муниципального района Учалинский район Республики Башкортостан',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460023127,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24679807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24679807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '270000026',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24679807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24679807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11659673',
        'method' => 'get',
      ),
    ),
  ),
  7203362457 => 
  array (
    'id' => 11659955,
    'name' => 'ООО "Медицинская Компания"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460023972,
    'updated_at' => 1524545391,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24680823,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24680823',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203362457',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24680823,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24680823',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11659955',
        'method' => 'get',
      ),
    ),
  ),
  8602009220 => 
  array (
    'id' => 11661259,
    'name' => 'ООО Навигатор',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460028295,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24682765,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24682765',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602009220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24682765,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24682765',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11661259',
        'method' => 'get',
      ),
    ),
  ),
  6659221348 => 
  array (
    'id' => 11661599,
    'name' => 'ООО МИРАЙ',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460029588,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24683383,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24683383',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6659221348',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24683383,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24683383',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11661599',
        'method' => 'get',
      ),
    ),
  ),
  5405481090 => 
  array (
    'id' => 11661835,
    'name' => 'ООО Сфера',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460030483,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24683721,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24683721',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405481090',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24683721,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24683721',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11661835',
        'method' => 'get',
      ),
    ),
  ),
  2312222193 => 
  array (
    'id' => 11662506,
    'name' => 'ООО  "Красмедико"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460033792,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24684784,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24684784',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312222193',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24684784,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24684784',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11662506',
        'method' => 'get',
      ),
    ),
  ),
  7838365524 => 
  array (
    'id' => 11663174,
    'name' => 'ООО "БалтСтройПроект"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460036285,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24686050,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24686050',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838365524',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24686050,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24686050',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11663174',
        'method' => 'get',
      ),
    ),
  ),
  7722687402 => 
  array (
    'id' => 11663546,
    'name' => 'ООО  Инфомед-Экспо',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460037297,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24686662,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24686662',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722687402',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24686662,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24686662',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11663546',
        'method' => 'get',
      ),
    ),
  ),
  4632069893 => 
  array (
    'id' => 11667086,
    'name' => 'ООО "КОММУНСТРОЙПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460095095,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24691160,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24691160',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632069893',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24691160,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24691160',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11667086',
        'method' => 'get',
      ),
    ),
  ),
  5032209300 => 
  array (
    'id' => 11667138,
    'name' => 'ООО "Б-ФАРМ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460095471,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24691224,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24691224',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032209300',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24691224,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24691224',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11667138',
        'method' => 'get',
      ),
    ),
  ),
  6312106807 => 
  array (
    'id' => 11668156,
    'name' => 'ООО ЧАСТНАЯ ОХРАННАЯ ОРГАНИЗАЦИЯ "ФСР"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460098484,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24692576,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24692576',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312106807',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24692576,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24692576',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11668156',
        'method' => 'get',
      ),
    ),
  ),
  6658465567 => 
  array (
    'id' => 11669030,
    'name' => 'ООО "ИНВЕСТ МЕДИКА-МИ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460100036,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24693514,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24693514',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658465567',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24693514,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24693514',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11669030',
        'method' => 'get',
      ),
    ),
  ),
  4217161152 => 
  array (
    'id' => 11669300,
    'name' => 'ООО "КАПИТАЛСТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460100970,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24693884,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24693884',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4217161152',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24693884,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24693884',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11669300',
        'method' => 'get',
      ),
    ),
  ),
  5407009768 => 
  array (
    'id' => 11640015,
    'name' => 'ООО "МегаТрейд"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459922622,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24649841,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649841',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407009768',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24649841,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649841',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11640015',
        'method' => 'get',
      ),
    ),
  ),
  5407101410 => 
  array (
    'id' => 11670082,
    'name' => 'ЗАО "Мера"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460104477,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24695146,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24695146',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407101410',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24695146,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24695146',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11670082',
        'method' => 'get',
      ),
    ),
  ),
  7405000795 => 
  array (
    'id' => 11670362,
    'name' => 'ОАО «Автотранспортное предприятие»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460105609,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24695526,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24695526',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7405000795',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24695526,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24695526',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11670362',
        'method' => 'get',
      ),
    ),
  ),
  7725294223 => 
  array (
    'id' => 11671300,
    'name' => 'ООО "МКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460108964,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24696812,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24696812',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725294223',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24696812,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24696812',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11671300',
        'method' => 'get',
      ),
    ),
  ),
  8909003596 => 
  array (
    'id' => 11671386,
    'name' => 'ЗАО "Ямальская управляющая компания"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460109337,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24696928,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24696928',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8909003596',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24696928,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24696928',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11671386',
        'method' => 'get',
      ),
    ),
  ),
  890103761510 => 
  array (
    'id' => 11671974,
    'name' => 'ИП Белеков Бабырбек Аттокурович',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460111043,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24697778,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24697778',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '890103761510',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24697778,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24697778',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11671974',
        'method' => 'get',
      ),
    ),
  ),
  3018317438 => 
  array (
    'id' => 11672062,
    'name' => 'ООО Производственно коммерческая фирма   Транс-Бункер',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460111342,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24697904,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24697904',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522671,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3018317438',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24697904,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24697904',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11672062',
        'method' => 'get',
      ),
    ),
  ),
  1421000128 => 
  array (
    'id' => 11672206,
    'name' => 'ООО ОЛЕКМИНСКОЕ ГАТП',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460111932,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24698110,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24698110',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1421000128',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24698110,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24698110',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11672206',
        'method' => 'get',
      ),
    ),
  ),
  269022392 => 
  array (
    'id' => 11688866,
    'name' => 'ООО "Строительная фирма №3"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460352871,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24719594,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24719594',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '269022392',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24719594,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24719594',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11688866',
        'method' => 'get',
      ),
    ),
  ),
  1832072577 => 
  array (
    'id' => 11688988,
    'name' => 'ООО  "ВЕГА"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460353811,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24719748,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24719748',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1832072577',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24719748,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24719748',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11688988',
        'method' => 'get',
      ),
    ),
  ),
  8601046999 => 
  array (
    'id' => 11689570,
    'name' => 'ООО Югорская ЭнергоСервисная Компания',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460356524,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24720412,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24720412',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8601046999',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24720412,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24720412',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11689570',
        'method' => 'get',
      ),
    ),
  ),
  7203175930 => 
  array (
    'id' => 11690588,
    'name' => 'АО  "Тюменское областное дорожно-эксплуатационное предприятие"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460359296,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24721426,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24721426',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203175930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24721426,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24721426',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11690588',
        'method' => 'get',
      ),
    ),
  ),
  7203320457 => 
  array (
    'id' => 11690788,
    'name' => 'ООО "АПДЕЙТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460359901,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24721684,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24721684',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203320457',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24721684,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24721684',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11690788',
        'method' => 'get',
      ),
    ),
  ),
  7224025357 => 
  array (
    'id' => 11691038,
    'name' => 'ООО "полное ПРАВО"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460360801,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24722068,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24722068',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7224025357',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24722068,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24722068',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11691038',
        'method' => 'get',
      ),
    ),
  ),
  5902175142 => 
  array (
    'id' => 11691760,
    'name' => 'ООО  "УПРАВЛЕНИЕ МЕДИЦИНОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460363191,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24723130,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24723130',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5902175142',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24723130,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24723130',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11691760',
        'method' => 'get',
      ),
    ),
  ),
  7215003396 => 
  array (
    'id' => 11692444,
    'name' => 'ЗАО "Заводоуковскагрострой"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460365629,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24724386,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24724386',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7215003396',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24724386,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24724386',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11692444',
        'method' => 'get',
      ),
    ),
  ),
  6376003613 => 
  array (
    'id' => 11693206,
    'name' => 'ООО  "АМОНД"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460368048,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24725912,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24725912',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6376003613',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24725912,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24725912',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11693206',
        'method' => 'get',
      ),
    ),
  ),
  8910002565 => 
  array (
    'id' => 11693418,
    'name' => 'Тазовское муниципальное унитарное дорожно-транспортное предприятие',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460368759,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24726300,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24726300',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8910002565',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24726300,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24726300',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11693418',
        'method' => 'get',
      ),
    ),
  ),
  1840021706 => 
  array (
    'id' => 11693642,
    'name' => 'ООО СПЕЦМЕТАЛЛ',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460369322,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24726580,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24726580',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1840021706',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24726580,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24726580',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11693642',
        'method' => 'get',
      ),
    ),
  ),
  6319702703 => 
  array (
    'id' => 11693812,
    'name' => 'ООО Росхлеб',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460369813,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24726804,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24726804',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319702703',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24726804,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24726804',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11693812',
        'method' => 'get',
      ),
    ),
  ),
  670100005613 => 
  array (
    'id' => 11694024,
    'name' => 'ИП Скаловский Василий Дмитриевич',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460370514,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24727096,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24727096',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '670100005613',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24727096,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24727096',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11694024',
        'method' => 'get',
      ),
    ),
  ),
  7841489907 => 
  array (
    'id' => 11694476,
    'name' => 'ООО  «ДТБ»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460371893,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24727902,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24727902',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7841489907',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24727902,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24727902',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11694476',
        'method' => 'get',
      ),
    ),
  ),
  7729781281 => 
  array (
    'id' => 11694648,
    'name' => 'ООО«НАУЧНО-ПРОИЗВОДСТВЕННОЕ ОБЪЕДИНЕНИЕ «ЛУЧ»',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1460372454,
    'updated_at' => 1524545392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24728164,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728164',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522670,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729781281',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24728164,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24728164',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11694648',
        'method' => 'get',
      ),
    ),
  ),
  4329009727 => 
  array (
    'id' => 11619741,
    'name' => 'ООО "Коммунальное хозяйство"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1459775293,
    'updated_at' => 1524545393,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24621671,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24621671',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4329009727',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24621671,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24621671',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11619741',
        'method' => 'get',
      ),
    ),
  ),
  4717005592 => 
  array (
    'id' => 11619883,
    'name' => 'МУП "Городское хозяйство"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1459775715,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24621943,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24621943',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4717005592',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24621943,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24621943',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11619883',
        'method' => 'get',
      ),
    ),
  ),
  6162064129 => 
  array (
    'id' => 11620273,
    'name' => 'ООО РВФ-Строй',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1459777044,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24622633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24622633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6162064129',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24622633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24622633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11620273',
        'method' => 'get',
      ),
    ),
  ),
  257008019 => 
  array (
    'id' => 11624731,
    'name' => 'ООО "МАГИСТРАЛЬ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459833775,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24629139,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24629139',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '257008019',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24629139,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24629139',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11624731',
        'method' => 'get',
      ),
    ),
  ),
  2225057426 => 
  array (
    'id' => 11624865,
    'name' => 'ООО "ОХОТА"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459834705,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24629279,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24629279',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225057426',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24629279,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24629279',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11624865',
        'method' => 'get',
      ),
    ),
  ),
  '0326000447' => 
  array (
    'id' => 11625317,
    'name' => 'ООО "Байкал-Транс"',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1459836484,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24629797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24629797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0326000447',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Бурятия(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24629797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24629797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11625317',
        'method' => 'get',
      ),
    ),
  ),
  4345329107 => 
  array (
    'id' => 11625673,
    'name' => 'ООО «ВятЭнергоПроект»',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1459837677,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24630141,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24630141',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345329107',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24630141,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24630141',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11625673',
        'method' => 'get',
      ),
    ),
  ),
  6163069320 => 
  array (
    'id' => 11625723,
    'name' => 'ОАО Дорожное ремонтно-строительное управление',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459837836,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24630187,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24630187',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6163069320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24630187,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24630187',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11625723',
        'method' => 'get',
      ),
    ),
  ),
  3666182920 => 
  array (
    'id' => 11626979,
    'name' => 'ООО"АЙ ДИ КОНСАЛТИНГ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459839945,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24632323,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24632323',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3666182920',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24632323,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24632323',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11626979',
        'method' => 'get',
      ),
    ),
  ),
  231100303809 => 
  array (
    'id' => 11627169,
    'name' => 'ИПСирота Виталий Николаевич',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459840684,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24632597,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24632597',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 24632635,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=24632635',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '231100303809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24632597,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24632597',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11627169',
        'method' => 'get',
      ),
    ),
  ),
  7801426604 => 
  array (
    'id' => 11627985,
    'name' => 'ООО ЭкоМедС Северо-Запад',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459843057,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24634129,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24634129',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801426604',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24634129,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24634129',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11627985',
        'method' => 'get',
      ),
    ),
  ),
  7811486641 => 
  array (
    'id' => 11628831,
    'name' => 'ООО Лабория',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459844526,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24634793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24634793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811486641',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24634793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24634793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11628831',
        'method' => 'get',
      ),
    ),
  ),
  7802148357 => 
  array (
    'id' => 11629335,
    'name' => 'ЗАО" НЕВСКИЙ ПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459845449,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24635155,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24635155',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802148357',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24635155,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24635155',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11629335',
        'method' => 'get',
      ),
    ),
  ),
  7720760061 => 
  array (
    'id' => 11630901,
    'name' => 'ООО СВР-Техно',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459850070,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24637515,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637515',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720760061',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24637515,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637515',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11630901',
        'method' => 'get',
      ),
    ),
  ),
  4101098631 => 
  array (
    'id' => 11630921,
    'name' => 'ООО "Онтарион"',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1459850153,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24637555,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637555',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101098631',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Камчатская область(GMT +12)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24637555,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637555',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11630921',
        'method' => 'get',
      ),
    ),
  ),
  380200129466 => 
  array (
    'id' => 11631051,
    'name' => 'ИП Крантов Александр Петрович',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1459850495,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24637723,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637723',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '380200129466',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область +8',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24637723,
        1 => 24648319,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637723,24648319',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11631051',
        'method' => 'get',
      ),
    ),
  ),
  7718880178 => 
  array (
    'id' => 11631127,
    'name' => 'ООО ВЕРД',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459850716,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24637823,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637823',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718880178',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24637823,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24637823',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11631127',
        'method' => 'get',
      ),
    ),
  ),
  6234090547 => 
  array (
    'id' => 11631615,
    'name' => 'ООО ОРТОИМПЛАНТ-Н',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459852330,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24638567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24638567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6234090547',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24638567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24638567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11631615',
        'method' => 'get',
      ),
    ),
  ),
  7723896409 => 
  array (
    'id' => 11631965,
    'name' => 'ООО Меркурий+',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459853587,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24639099,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24639099',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723896409',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24639099,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24639099',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11631965',
        'method' => 'get',
      ),
    ),
  ),
  7806410615 => 
  array (
    'id' => 11632705,
    'name' => 'ООО МедТехСервис',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459856416,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24640355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24640355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806410615',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24640355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24640355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11632705',
        'method' => 'get',
      ),
    ),
  ),
  5042136980 => 
  array (
    'id' => 11632795,
    'name' => 'ООО ТЭКсервис',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459856791,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24640503,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24640503',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5042136980',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24640503,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24640503',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11632795',
        'method' => 'get',
      ),
    ),
  ),
  7719800048 => 
  array (
    'id' => 11633339,
    'name' => 'ООО  Фирма ЛасДентос',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459858808,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24641343,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24641343',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719800048',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24641343,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24641343',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11633339',
        'method' => 'get',
      ),
    ),
  ),
  7726521052 => 
  array (
    'id' => 11633499,
    'name' => 'ООО Инфосьют',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459859417,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24641625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24641625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726521052',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24641625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24641625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11633499',
        'method' => 'get',
      ),
    ),
  ),
  7814310283 => 
  array (
    'id' => 11633743,
    'name' => 'ООО Туристическая Компания  РВС',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459860112,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24641979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24641979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814310283',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24641979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24641979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11633743',
        'method' => 'get',
      ),
    ),
  ),
  6127010201 => 
  array (
    'id' => 11634751,
    'name' => 'ООО "МАСТЕР"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459863697,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24643561,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24643561',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 24643623,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=24643623',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6127010201',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24643561,
        1 => 24657505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24643561,24657505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11634751',
        'method' => 'get',
      ),
    ),
  ),
  258950060 => 
  array (
    'id' => 11639515,
    'name' => 'ООО БАШТЕХНОИНВЕСТ',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459920187,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24649273,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649273',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '258950060',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24649273,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649273',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11639515',
        'method' => 'get',
      ),
    ),
  ),
  278154557 => 
  array (
    'id' => 11639609,
    'name' => 'ООО  "Специализированное предприятие М-Стандарт"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459920759,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24649377,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649377',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '278154557',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24649377,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649377',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11639609',
        'method' => 'get',
      ),
    ),
  ),
  550303771713 => 
  array (
    'id' => 11639627,
    'name' => 'ИП Зименс Яков Яковлевич',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1459920929,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24649401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '550303771713',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24649401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24649401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11639627',
        'method' => 'get',
      ),
    ),
  ),
  6142025450 => 
  array (
    'id' => 11640179,
    'name' => 'ООО "Союз"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459923307,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24661845,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24661845',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6142025450',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24661845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24661845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11640179',
        'method' => 'get',
      ),
    ),
  ),
  6672173037 => 
  array (
    'id' => 11640381,
    'name' => 'ООО "Трио"',
    'responsible_user_id' => 1261404,
    'created_by' => 693760,
    'created_at' => 1459924251,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24650299,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24650299',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672173037',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24650299,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24650299',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11640381',
        'method' => 'get',
      ),
    ),
  ),
  7020032677 => 
  array (
    'id' => 11640733,
    'name' => 'ООО "БРАТСТВО"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1459925515,
    'updated_at' => 1524545394,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24650817,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24650817',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522672,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7020032677',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24650817,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24650817',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11640733',
        'method' => 'get',
      ),
    ),
  ),
);