<?php 
 return array (
  5028018007 => 
  array (
    'id' => 14811267,
    'name' => 'ООО "СВОД"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488529302,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5028018007',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811267',
        'method' => 'get',
      ),
    ),
  ),
  7743762107 => 
  array (
    'id' => 14811299,
    'name' => 'ООО  «Лан Технолоджи»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488529383,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157721,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157721',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743762107',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157721,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157721',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811299',
        'method' => 'get',
      ),
    ),
  ),
  1327006852 => 
  array (
    'id' => 14811349,
    'name' => 'Общество с ограниченной ответственностью "Медицинская компания"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488529591,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1327006852',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811349',
        'method' => 'get',
      ),
    ),
  ),
  5190142426 => 
  array (
    'id' => 14811377,
    'name' => 'ООО "Севертранс"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488529650,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5190142426',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811377',
        'method' => 'get',
      ),
    ),
  ),
  '0390000' => 
  array (
    'id' => 14811413,
    'name' => 'Общество с ограниченной ответственностью "НЧ Престиж"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488529774,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157839,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157839',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0390000',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157839,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157839',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811413',
        'method' => 'get',
      ),
    ),
  ),
  7731579315 => 
  array (
    'id' => 14811523,
    'name' => 'ООО "Евромакс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488530037,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157947,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157947',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7731579315',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157947,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157947',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811523',
        'method' => 'get',
      ),
    ),
  ),
  7728643105 => 
  array (
    'id' => 14811549,
    'name' => 'ООО «Национальный оператор «Максима»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488530135,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728643105',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811549',
        'method' => 'get',
      ),
    ),
  ),
  1658140520 => 
  array (
    'id' => 14811595,
    'name' => 'Общество с ограниченной ответственностью Медицинская Компания "ОЛАМЕД"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488530264,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29158015,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158015',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1658140520',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29158015,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158015',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811595',
        'method' => 'get',
      ),
    ),
  ),
  1832117595 => 
  array (
    'id' => 14811631,
    'name' => 'Общество с ограниченной ответственностью  Регионсбыт',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488530441,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29158057,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158057',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1832117595',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29158057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811631',
        'method' => 'get',
      ),
    ),
  ),
  6125029119 => 
  array (
    'id' => 14811661,
    'name' => 'ООО «Райэнерго»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488530564,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29158107,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158107',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6125029119',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29158107,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158107',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811661',
        'method' => 'get',
      ),
    ),
  ),
  2312178145 => 
  array (
    'id' => 14811855,
    'name' => 'Общество с ограниченной ответственностью «Фармсервис»',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488531431,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29158315,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158315',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2312178145',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29158315,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158315',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811855',
        'method' => 'get',
      ),
    ),
  ),
  2315088919 => 
  array (
    'id' => 14811947,
    'name' => 'Общество с ограниченной ответственностью  ВЕДА',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488531827,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29158415,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158415',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2315088919',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29158415,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158415',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811947',
        'method' => 'get',
      ),
    ),
  ),
  2355003083 => 
  array (
    'id' => 14811983,
    'name' => 'ООО СОК "Золотой колос"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488532035,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29158465,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158465',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2355003083',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29158465,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158465',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14811983',
        'method' => 'get',
      ),
    ),
  ),
  2365013475 => 
  array (
    'id' => 14812365,
    'name' => 'Общество с Ограниченной Ответственностью "Торгмортранс"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488533864,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29158887,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158887',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2365013475',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29158887,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29158887',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812365',
        'method' => 'get',
      ),
    ),
  ),
  2902076594 => 
  array (
    'id' => 14812615,
    'name' => 'ООО "АЦ "Рик-ком Информ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488534906,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159177,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159177',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2902076594',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159177,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159177',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812615',
        'method' => 'get',
      ),
    ),
  ),
  2903009488 => 
  array (
    'id' => 14812697,
    'name' => 'Общество с ограниченной ответственностью ПКФ "ТЭЧ-Сервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488535238,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159287,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159287',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2903009488',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159287,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159287',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812697',
        'method' => 'get',
      ),
    ),
  ),
  7718872360 => 
  array (
    'id' => 14812781,
    'name' => 'ООО "Культурный центр "МИК-Альянс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488535545,
    'updated_at' => 1524547753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159385,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159385',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718872360',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159385,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159385',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812781',
        'method' => 'get',
      ),
    ),
  ),
  2918010221 => 
  array (
    'id' => 14812833,
    'name' => 'Общество с ограниченной ответственностью "Шестиозерье-Лес"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488535711,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2918010221',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812833',
        'method' => 'get',
      ),
    ),
  ),
  7610102687 => 
  array (
    'id' => 14812865,
    'name' => 'ООО "ЦЕНТР ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488535894,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159459,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159459',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7610102687',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159459,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159459',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812865',
        'method' => 'get',
      ),
    ),
  ),
  3329025501 => 
  array (
    'id' => 14812915,
    'name' => 'Общество с ограниченной ответственностью БЕАМЕД',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488536050,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3329025501',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812915',
        'method' => 'get',
      ),
    ),
  ),
  7715232490 => 
  array (
    'id' => 14812929,
    'name' => 'ООО "РЕЛИФ-МС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488536116,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159529,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159529',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521589,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715232490',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159529,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159529',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812929',
        'method' => 'get',
      ),
    ),
  ),
  3435059809 => 
  array (
    'id' => 14812975,
    'name' => 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МНОГООТРАСЛЕВАЯ ПРОИЗВОДСТВЕННАЯ КОМПАНИЯ "ГУДВИЛЛ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488536250,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159561,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159561',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435059809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159561,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159561',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14812975',
        'method' => 'get',
      ),
    ),
  ),
  7727585764 => 
  array (
    'id' => 14813003,
    'name' => 'ООО «Вертоград»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488536383,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7727585764',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159589,
        1 => 29159651,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159589,29159651',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813003',
        'method' => 'get',
      ),
    ),
  ),
  3460064753 => 
  array (
    'id' => 14813009,
    'name' => 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "УЮТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488536408,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159593,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159593',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3460064753',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159593,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159593',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813009',
        'method' => 'get',
      ),
    ),
  ),
  3663030020 => 
  array (
    'id' => 14813037,
    'name' => 'Общество с ограниченной ответственностью Торговая Компания "Тракт Сервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488536554,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3663030020',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813037',
        'method' => 'get',
      ),
    ),
  ),
  7707060751 => 
  array (
    'id' => 14813039,
    'name' => 'ООО "Вартамана Интернэшнл Тредерс"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488536570,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159633,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159633',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521589,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707060751',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159633,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159633',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813039',
        'method' => 'get',
      ),
    ),
  ),
  3664069478 => 
  array (
    'id' => 14813073,
    'name' => 'Общество с ограниченной ответственностью"Компания СКВ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488536683,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159653,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159653',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3664069478',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159653,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159653',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813073',
        'method' => 'get',
      ),
    ),
  ),
  7704305026 => 
  array (
    'id' => 14813095,
    'name' => 'ООО "Региональный Информационный Центр"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488536766,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159685,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159685',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521589,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704305026',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159685,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159685',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813095',
        'method' => 'get',
      ),
    ),
  ),
  7802485948 => 
  array (
    'id' => 14813117,
    'name' => 'ООО "КСБ Групп"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488536837,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802485948',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813117',
        'method' => 'get',
      ),
    ),
  ),
  3702716104 => 
  array (
    'id' => 14813143,
    'name' => 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ГАРМОНИЯ ПЛЮС"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488536930,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159735,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159735',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702716104',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159735,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159735',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813143',
        'method' => 'get',
      ),
    ),
  ),
  7701415091 => 
  array (
    'id' => 14813189,
    'name' => 'ООО "МАРКЕТИНГ, ВЫСТАВКИ, КОНСАЛТИНГ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488537059,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521589,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7701415091',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813189',
        'method' => 'get',
      ),
    ),
  ),
  7816597391 => 
  array (
    'id' => 14813197,
    'name' => 'ООО "ВЫСОКИЕ ТЕХНОЛОГИИ ФАРМАЦЕВТИКИ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488537122,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159795,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159795',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816597391',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159795,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159795',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813197',
        'method' => 'get',
      ),
    ),
  ),
  9710002922 => 
  array (
    'id' => 14813259,
    'name' => 'Акционерное общество ТАГОР',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488537357,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159841,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159841',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9710002922',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159841,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159841',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813259',
        'method' => 'get',
      ),
    ),
  ),
  3904009242 => 
  array (
    'id' => 14813261,
    'name' => 'Общество с ограниченной ответственностью "Фармед"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488537364,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159845,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159845',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3904009242',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159845,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159845',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813261',
        'method' => 'get',
      ),
    ),
  ),
  3904604192 => 
  array (
    'id' => 14813337,
    'name' => 'Общество с ограниченной ответственностью "Диагностика"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488537715,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159943,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159943',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3904604192',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159943,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159943',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813337',
        'method' => 'get',
      ),
    ),
  ),
  3906194008 => 
  array (
    'id' => 14813355,
    'name' => '"Джи-Кей Системс "',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488537806,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29159971,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159971',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906194008',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29159971,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29159971',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813355',
        'method' => 'get',
      ),
    ),
  ),
  3906215480 => 
  array (
    'id' => 14813371,
    'name' => 'Общество с Ограниченной Ответственностью "Зимний сад"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488537908,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160003,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160003',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906215480',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160003,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160003',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813371',
        'method' => 'get',
      ),
    ),
  ),
  4345274313 => 
  array (
    'id' => 14813407,
    'name' => 'Общество с ограниченной ответственностью "Кислород Сервис Фарм"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488538038,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160043,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160043',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345274313',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160043,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160043',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813407',
        'method' => 'get',
      ),
    ),
  ),
  5047189583 => 
  array (
    'id' => 14813455,
    'name' => 'Общество с ограниченной ответственностью "ПРОДУКТОВАЯ ЛАВКА"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488538338,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047189583',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813455',
        'method' => 'get',
      ),
    ),
  ),
  5838012363 => 
  array (
    'id' => 14813535,
    'name' => 'Общество с ограниченной ответственностью «Сириус»',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488538726,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160201,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160201',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5838012363',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160201,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160201',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813535',
        'method' => 'get',
      ),
    ),
  ),
  6102040860 => 
  array (
    'id' => 14813583,
    'name' => 'Общество с ограниченной ответственностью  Октябрьский 2012',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488538872,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6102040860',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813583',
        'method' => 'get',
      ),
    ),
  ),
  6450086601 => 
  array (
    'id' => 14813621,
    'name' => 'Общество с ограниченной ответственностью "Ассоциация благоустройства"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488539055,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160297,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160297',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6450086601',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160297,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160297',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813621',
        'method' => 'get',
      ),
    ),
  ),
  6714034609 => 
  array (
    'id' => 14813653,
    'name' => 'ООО "ФАВОРИТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488539217,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6714034609',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813653',
        'method' => 'get',
      ),
    ),
  ),
  7104069384 => 
  array (
    'id' => 14813715,
    'name' => 'Общество с ограниченной ответственностью "СК МЕГА"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488539470,
    'updated_at' => 1524547754,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29160417,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160417',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521257,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7104069384',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29160417,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29160417',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14813715',
        'method' => 'get',
      ),
    ),
  ),
  7838034177 => 
  array (
    'id' => 14801359,
    'name' => 'ООО «МК-АЭфТ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488441958,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29146969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29146969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7838034177',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29146969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29146969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14801359',
        'method' => 'get',
      ),
    ),
  ),
  7816385816 => 
  array (
    'id' => 14801483,
    'name' => 'ООО "ГАРАНТ-КАДАСТР"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488442402,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29147091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816385816',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29147091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14801483',
        'method' => 'get',
      ),
    ),
  ),
  7814655591 => 
  array (
    'id' => 14801511,
    'name' => 'ООО «ВЕТСНАБ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488442524,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29147111,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147111',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814655591',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29147111,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147111',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14801511',
        'method' => 'get',
      ),
    ),
  ),
  7814427524 => 
  array (
    'id' => 14801615,
    'name' => 'ООО  "ВЕТЕРИНАРНЫЙ ГОСПИТАЛЬ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488442940,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29147233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814427524',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29147233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14801615',
        'method' => 'get',
      ),
    ),
  ),
  2308218268 => 
  array (
    'id' => 14801623,
    'name' => '0318200064217000002',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488442950,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29147247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29147245,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29147245',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2308218268',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29147247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14801623',
        'method' => 'get',
      ),
    ),
  ),
  7814148908 => 
  array (
    'id' => 14801695,
    'name' => 'ООО  "РемКомплектСервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488443281,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29147333,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147333',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814148908',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29147333,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147333',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14801695',
        'method' => 'get',
      ),
    ),
  ),
  7813403048 => 
  array (
    'id' => 14801777,
    'name' => 'ООО «Омега-СПб»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488443573,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29147407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813403048',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29147407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14801777',
        'method' => 'get',
      ),
    ),
  ),
  7811533972 => 
  array (
    'id' => 14802337,
    'name' => 'ООО "ТЕХНОЛОГИЯ ВКУСА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488443866,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29147483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811533972',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29147483,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29147483',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14802337',
        'method' => 'get',
      ),
    ),
  ),
  2309085910 => 
  array (
    'id' => 14803769,
    'name' => 'ООО "Кубань-Сервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488450099,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29149109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29149107,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29149107',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2309085910',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29149109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14803769',
        'method' => 'get',
      ),
    ),
  ),
  7805666029 => 
  array (
    'id' => 14803981,
    'name' => 'ООО  «Производство Завод имени Шаумяна»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488451122,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29149357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805666029',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29149357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14803981',
        'method' => 'get',
      ),
    ),
  ),
  2632016104 => 
  array (
    'id' => 14804031,
    'name' => 'ООО "Строительное предприятие "Стройрезерв"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488451290,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29149421,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149421',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29149419,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29149419',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632016104',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29149421,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149421',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14804031',
        'method' => 'get',
      ),
    ),
  ),
  2925004420 => 
  array (
    'id' => 14804109,
    'name' => 'ООО "Стройсервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488451638,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29149511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29149507,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29149507',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2925004420',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29149511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29149511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14804109',
        'method' => 'get',
      ),
    ),
  ),
  7804524102 => 
  array (
    'id' => 14804759,
    'name' => 'ООО  "ЗВЕЗДОЧКА"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488454227,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29150213,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150213',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7804524102',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29150213,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150213',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14804759',
        'method' => 'get',
      ),
    ),
  ),
  7743120251 => 
  array (
    'id' => 14805049,
    'name' => 'ООО "Третий Торговый Дом"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488455515,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29150585,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150585',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743120251',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29150585,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150585',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805049',
        'method' => 'get',
      ),
    ),
  ),
  7734675426 => 
  array (
    'id' => 14805135,
    'name' => 'ООО "БАРКО"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488455952,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29150699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734675426',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29150699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805135',
        'method' => 'get',
      ),
    ),
  ),
  7734368337 => 
  array (
    'id' => 14805187,
    'name' => 'ООО  «СпецТехСервис»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488456147,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29150761,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150761',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7734368337',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29150761,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150761',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805187',
        'method' => 'get',
      ),
    ),
  ),
  2928524 => 
  array (
    'id' => 14805315,
    'name' => 'ООО "Строймостсервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488456636,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29150889,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150889',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29150887,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29150887',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2928524',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29150889,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150889',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805315',
        'method' => 'get',
      ),
    ),
  ),
  7730680672 => 
  array (
    'id' => 14805341,
    'name' => 'ООО «ПраймМед»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488456781,
    'updated_at' => 1524547756,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29150931,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150931',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730680672',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29150931,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29150931',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805341',
        'method' => 'get',
      ),
    ),
  ),
  7729380346 => 
  array (
    'id' => 14805413,
    'name' => 'ООО «БИОТЕ»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488457127,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29151023,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151023',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729380346',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29151023,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151023',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805413',
        'method' => 'get',
      ),
    ),
  ),
  3231000557 => 
  array (
    'id' => 14805427,
    'name' => 'Открытое акционерное общество Унечское автотранспортное предприятие',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488457220,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29151035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3231000557',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29151035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805427',
        'method' => 'get',
      ),
    ),
  ),
  7725264067 => 
  array (
    'id' => 14805485,
    'name' => 'ООО "Антель"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488457369,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29151085,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151085',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725264067',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29151085,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151085',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805485',
        'method' => 'get',
      ),
    ),
  ),
  7723763670 => 
  array (
    'id' => 14805611,
    'name' => 'ООО  "Сервисный центр "Гарант-Виктория"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488457834,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29151219,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151219',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723763670',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29151219,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151219',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805611',
        'method' => 'get',
      ),
    ),
  ),
  3435108661 => 
  array (
    'id' => 14805741,
    'name' => 'ООО «ВолгаСервисПродукт»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488458406,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29151355,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151355',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435108661',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29151355,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151355',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805741',
        'method' => 'get',
      ),
    ),
  ),
  7723555328 => 
  array (
    'id' => 14805763,
    'name' => 'ООО "Интеграл-С"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488458491,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29151387,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151387',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723555328',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29151387,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151387',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14805763',
        'method' => 'get',
      ),
    ),
  ),
  3443933058 => 
  array (
    'id' => 14806271,
    'name' => 'ООО "АВТОЦЕНТР"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488460665,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29151935,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151935',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3443933058',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29151935,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29151935',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14806271',
        'method' => 'get',
      ),
    ),
  ),
  1101090147 => 
  array (
    'id' => 14809401,
    'name' => 'ООО "Бюро независимых экспертиз и оценок"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488518201,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1101090147',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155435,
        1 => 29162241,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155435,29162241',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809401',
        'method' => 'get',
      ),
    ),
  ),
  1101099686 => 
  array (
    'id' => 14809431,
    'name' => 'ООО "Грин Лайн"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488518456,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155479,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155479',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1101099686',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155479,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155479',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809431',
        'method' => 'get',
      ),
    ),
  ),
  2311175991 => 
  array (
    'id' => 14809443,
    'name' => 'ООО "Первая межевая компания"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488518627,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155503,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155503',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311175991',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155503,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155503',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809443',
        'method' => 'get',
      ),
    ),
  ),
  2311201419 => 
  array (
    'id' => 14809471,
    'name' => 'ООО "СОВРЕМЕННАЯ МЕДИЦИНА"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488519061,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155549,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155549',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311201419',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155549,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155549',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809471',
        'method' => 'get',
      ),
    ),
  ),
  2330040340 => 
  array (
    'id' => 14809493,
    'name' => 'ЗАО "Пансионат с лечением "Импульс"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488519306,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155587,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155587',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2330040340',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155587,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155587',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809493',
        'method' => 'get',
      ),
    ),
  ),
  2373003463 => 
  array (
    'id' => 14809513,
    'name' => 'ООО "Кореновск-ТВ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488519501,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2373003463',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809513',
        'method' => 'get',
      ),
    ),
  ),
  2632049220 => 
  array (
    'id' => 14809583,
    'name' => 'ООО "Феликс"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488520272,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632049220',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809583',
        'method' => 'get',
      ),
    ),
  ),
  9710013466 => 
  array (
    'id' => 14809615,
    'name' => 'ООО  "НОВОМЕД"',
    'responsible_user_id' => 1261404,
    'created_by' => 1310841,
    'created_at' => 1488520547,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155747,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155747',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1501748837,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9710013466',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155747,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155747',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809615',
        'method' => 'get',
      ),
    ),
  ),
  2634093827 => 
  array (
    'id' => 14809621,
    'name' => 'ООО «Элмос»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488520577,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155755,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155755',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2634093827',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155755,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155755',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809621',
        'method' => 'get',
      ),
    ),
  ),
  2901205198 => 
  array (
    'id' => 14809679,
    'name' => 'ООО "ИМПУЛЬС М"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488521069,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155811,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155811',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901205198',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155811,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155811',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809679',
        'method' => 'get',
      ),
    ),
  ),
  3316010882 => 
  array (
    'id' => 14809713,
    'name' => 'Унитарное муниципальное предприятие по приёму, переработке и утилизации отходов "Полигон" Киржачского района',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488521366,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155867,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155867',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3316010882',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155867,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155867',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809713',
        'method' => 'get',
      ),
    ),
  ),
  7842508782 => 
  array (
    'id' => 14809723,
    'name' => 'ООО  "ОРИОН"',
    'responsible_user_id' => 1261404,
    'created_by' => 1310841,
    'created_at' => 1488521434,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1504089659,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842508782',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Санкт-Петербург(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809723',
        'method' => 'get',
      ),
    ),
  ),
  3443133132 => 
  array (
    'id' => 14809795,
    'name' => 'ООО «СТАНДАРТ-ОЙЛ»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488522028,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29155971,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155971',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3443133132',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29155971,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29155971',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809795',
        'method' => 'get',
      ),
    ),
  ),
  3524013901 => 
  array (
    'id' => 14809815,
    'name' => 'ООО "МАЛС"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488522188,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29156003,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156003',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521259,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3524013901',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29156003,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156003',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809815',
        'method' => 'get',
      ),
    ),
  ),
  3525320084 => 
  array (
    'id' => 14809857,
    'name' => 'ООО "Компания ГРАНДАВТО"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488522563,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29156077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525320084',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29156077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809857',
        'method' => 'get',
      ),
    ),
  ),
  3665125125 => 
  array (
    'id' => 14809961,
    'name' => 'ООО «Лист»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488523403,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29156255,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156255',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3665125125',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29156255,
        1 => 29156349,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156255,29156349',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14809961',
        'method' => 'get',
      ),
    ),
  ),
  7733258250 => 
  array (
    'id' => 14810229,
    'name' => 'ООО  "Премиумгрупп"',
    'responsible_user_id' => 1261404,
    'created_by' => 1310841,
    'created_at' => 1488524975,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29156579,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156579',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1503488066,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7733258250',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Москва(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29156579,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156579',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810229',
        'method' => 'get',
      ),
    ),
  ),
  7719420712 => 
  array (
    'id' => 14810495,
    'name' => 'ООО "ТЕКТУС.ИТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1310841,
    'created_at' => 1488526358,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29156951,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156951',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1505810866,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719420712',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Московская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29156951,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29156951',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810495',
        'method' => 'get',
      ),
    ),
  ),
  '0560035246' => 
  array (
    'id' => 14810583,
    'name' => 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ ЧАСТНАЯ ОХРАННАЯ ОРГАНИЗАЦИЯ "ЭНО"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488526759,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157045,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157045',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0560035246',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157045,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157045',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810583',
        'method' => 'get',
      ),
    ),
  ),
  705006700 => 
  array (
    'id' => 14810759,
    'name' => 'Общество с ограниченной ответственностью "СВ-ЮГ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1488527345,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157175,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157175',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '705006700',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157175,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157175',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810759',
        'method' => 'get',
      ),
    ),
  ),
  9102028562 => 
  array (
    'id' => 14810817,
    'name' => 'ООО "Феликс Мебель"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488527563,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157239,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157239',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102028562',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157239,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157239',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810817',
        'method' => 'get',
      ),
    ),
  ),
  3703043596 => 
  array (
    'id' => 14810855,
    'name' => 'ООО "Трансжилсервис"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488527670,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157263,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157263',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3703043596',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157263,
        1 => 29157367,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157263,29157367',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810855',
        'method' => 'get',
      ),
    ),
  ),
  7816587918 => 
  array (
    'id' => 14810885,
    'name' => 'ООО  "Мармари"',
    'responsible_user_id' => 1261404,
    'created_by' => 1310841,
    'created_at' => 1488527786,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157299,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157299',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1507029163,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7816587918',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ненецкий АО(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157299,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157299',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810885',
        'method' => 'get',
      ),
    ),
  ),
  7839474822 => 
  array (
    'id' => 14810949,
    'name' => 'ООО "Аврора"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1488528019,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521590,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839474822',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810949',
        'method' => 'get',
      ),
    ),
  ),
  4401059087 => 
  array (
    'id' => 14810993,
    'name' => 'ООО "ЧОП "Гранд""',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1488528127,
    'updated_at' => 1524547757,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157409,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157409',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521258,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4401059087',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157409,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157409',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810993',
        'method' => 'get',
      ),
    ),
  ),
  5406700757 => 
  array (
    'id' => 18266425,
    'name' => 'ООО фирма "Комбест"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524547728,
    'updated_at' => 1524548604,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941283,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941283',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406700757',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941283,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941283',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266425',
        'method' => 'get',
      ),
    ),
  ),
  5609095456 => 
  array (
    'id' => 18266531,
    'name' => 'ООО ВК ПРОМСТРОЙ',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524548639,
    'updated_at' => 1524548639,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941373,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941373',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5609095456',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941373,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941373',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266531',
        'method' => 'get',
      ),
    ),
  ),
  7810541586 => 
  array (
    'id' => 18266669,
    'name' => 'ООО "ЛМЦ Валмет"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524549976,
    'updated_at' => 1524550248,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941551,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941551',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810541586',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941551,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941551',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266669',
        'method' => 'get',
      ),
    ),
  ),
  7807164698 => 
  array (
    'id' => 18266733,
    'name' => 'ООО "Строй Ресурс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524550538,
    'updated_at' => 1524550539,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807164698',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266733',
        'method' => 'get',
      ),
    ),
  ),
  5614051969 => 
  array (
    'id' => 17813729,
    'name' => 'ООО "Строй-Сити"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1519706158,
    'updated_at' => 1524551229,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33303871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33303871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5614051969',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33303871,
        1 => 33941729,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33303871,33941729',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17813729',
        'method' => 'get',
      ),
    ),
  ),
  7720337339 => 
  array (
    'id' => 18266865,
    'name' => 'ООО "Корранд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524551384,
    'updated_at' => 1524551384,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941761,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941761',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720337339',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941761,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941761',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266865',
        'method' => 'get',
      ),
    ),
  ),
  2926007007 => 
  array (
    'id' => 18266877,
    'name' => 'ООО "НОРД-ВУД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524551526,
    'updated_at' => 1524551772,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2926007007',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266877',
        'method' => 'get',
      ),
    ),
  ),
  6829025953 => 
  array (
    'id' => 18266961,
    'name' => 'ООО "Волгопромвентиляция"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524551935,
    'updated_at' => 1524551977,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829025953',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941871,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941871',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266961',
        'method' => 'get',
      ),
    ),
  ),
  9901035598 => 
  array (
    'id' => 18266981,
    'name' => 'ООО "ЭЛМОНТ ВОСТОК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524552058,
    'updated_at' => 1524552059,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941899,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941899',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9901035598',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941899,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941899',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266981',
        'method' => 'get',
      ),
    ),
  ),
  5038129860 => 
  array (
    'id' => 18267021,
    'name' => 'ООО «ГЕРМЕС-ОПТ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524552221,
    'updated_at' => 1524552260,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941943,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941943',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5038129860',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941943,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941943',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267021',
        'method' => 'get',
      ),
    ),
  ),
  7707380494 => 
  array (
    'id' => 18267037,
    'name' => 'ООО «АЕКОМпроект»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524552339,
    'updated_at' => 1524552341,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941967,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941967',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707380494',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941967,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941967',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267037',
        'method' => 'get',
      ),
    ),
  ),
  5032261477 => 
  array (
    'id' => 18267049,
    'name' => 'ООО "Детский Наукоград"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524552385,
    'updated_at' => 1524552386,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032261477',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267049',
        'method' => 'get',
      ),
    ),
  ),
  3662258582 => 
  array (
    'id' => 18267101,
    'name' => 'ООО "НИКАСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524552702,
    'updated_at' => 1524552838,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942035,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942035',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3662258582',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942035,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942035',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267101',
        'method' => 'get',
      ),
    ),
  ),
  7703397289 => 
  array (
    'id' => 18267281,
    'name' => 'ООО "ПЛАНЕТА 9"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524553577,
    'updated_at' => 1524553577,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703397289',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267281',
        'method' => 'get',
      ),
    ),
  ),
  6165178759 => 
  array (
    'id' => 18267521,
    'name' => 'ООО ТеплоДон',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524554732,
    'updated_at' => 1524554753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942503,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942503',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165178759',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942503,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942503',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267521',
        'method' => 'get',
      ),
    ),
  ),
  546023425 => 
  array (
    'id' => 18267575,
    'name' => 'ООО "АГРОПРАЙМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524555021,
    'updated_at' => 1524555231,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942577,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942577',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '546023425',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942577,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942577',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267575',
        'method' => 'get',
      ),
    ),
  ),
  7707810725 => 
  array (
    'id' => 14418290,
    'name' => 'ООО Вестен Хаус',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484731383,
    'updated_at' => 1524555367,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561212,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561212',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524555367,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7707810725',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561212,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561212',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418290',
        'method' => 'get',
      ),
    ),
  ),
  7725754992 => 
  array (
    'id' => 18267671,
    'name' => 'ООО "ТАНАИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524555424,
    'updated_at' => 1524555482,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725754992',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267671',
        'method' => 'get',
      ),
    ),
  ),
  3123350850 => 
  array (
    'id' => 18267713,
    'name' => 'ООО "БЕЛСТРОЙТРЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524555596,
    'updated_at' => 1524555597,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942753,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942753',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123350850',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942753,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942753',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267713',
        'method' => 'get',
      ),
    ),
  ),
  3123374210 => 
  array (
    'id' => 15110083,
    'name' => 'ДЭП-31, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491546638,
    'updated_at' => 1524555967,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29547991,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29547991',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123374210',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29547991,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29547991',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15110083',
        'method' => 'get',
      ),
    ),
  ),
  7702804870 => 
  array (
    'id' => 18267847,
    'name' => 'ООО "АЙСБЕРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524556155,
    'updated_at' => 1524556156,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942895,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942895',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702804870',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942895,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942895',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267847',
        'method' => 'get',
      ),
    ),
  ),
  9718034193 => 
  array (
    'id' => 18267889,
    'name' => 'ООО "ЭРА ТЕХНОЛОГИЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524556282,
    'updated_at' => 1524556283,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33942937,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942937',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718034193',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33942937,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33942937',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267889',
        'method' => 'get',
      ),
    ),
  ),
  7728297511 => 
  array (
    'id' => 18267963,
    'name' => 'ООО "Контэн"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524556564,
    'updated_at' => 1524556614,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33943013,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943013',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728297511',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33943013,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943013',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18267963',
        'method' => 'get',
      ),
    ),
  ),
  3664215908 => 
  array (
    'id' => 16225041,
    'name' => 'ООО "Дороги-капремСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1503642006,
    'updated_at' => 1524556838,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31306979,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306979',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921244,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3664215908',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Воронежская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31306979,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31306979',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16225041',
        'method' => 'get',
      ),
    ),
  ),
  6902006700 => 
  array (
    'id' => 18268035,
    'name' => 'ООО "Стровиза"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524556974,
    'updated_at' => 1524557040,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33943127,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943127',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6902006700',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33943127,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943127',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18268035',
        'method' => 'get',
      ),
    ),
  ),
  3124020118 => 
  array (
    'id' => 15256705,
    'name' => 'СТРОИТЕЛЬНО-ТЕХНОЛОГИЧЕСКОЕ УПРАВЛЕНИЕ ВИОГЕМ"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1493098538,
    'updated_at' => 1524557319,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29745611,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29745611',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 29745649,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=29745649',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3124020118',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29745611,
        1 => 33887189,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29745611,33887189',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15256705',
        'method' => 'get',
      ),
    ),
  ),
  5027184820 => 
  array (
    'id' => 18268199,
    'name' => 'ООО "СтройСвязьПроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524557606,
    'updated_at' => 1524557777,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33943345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5027184820',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33943345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18268199',
        'method' => 'get',
      ),
    ),
  ),
  3702530607 => 
  array (
    'id' => 18268461,
    'name' => 'ООО "ЭКОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524558487,
    'updated_at' => 1524558768,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33943691,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943691',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3702530607',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33943691,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943691',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18268461',
        'method' => 'get',
      ),
    ),
  ),
  7723737769 => 
  array (
    'id' => 18268551,
    'name' => 'ООО "Лико-Финанс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524558835,
    'updated_at' => 1524558893,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33943811,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943811',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723737769',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33943811,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943811',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18268551',
        'method' => 'get',
      ),
    ),
  ),
  3115006607 => 
  array (
    'id' => 18268595,
    'name' => 'ООО «БелЗНАК-Прохоровка»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524558988,
    'updated_at' => 1524558989,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33943879,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943879',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3115006607',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33943879,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943879',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18268595',
        'method' => 'get',
      ),
    ),
  ),
  7118503192 => 
  array (
    'id' => 18268683,
    'name' => 'ООО "РЕГИОНСТРОЙ 71"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524559415,
    'updated_at' => 1524559434,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33943963,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943963',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7118503192',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33943963,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33943963',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18268683',
        'method' => 'get',
      ),
    ),
  ),
  5040121770 => 
  array (
    'id' => 14418408,
    'name' => 'ООО "ЛЕРДОРСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1484731918,
    'updated_at' => 1524559695,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28561328,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561328',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500606071,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040121770',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28561328,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28561328',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14418408',
        'method' => 'get',
      ),
    ),
  ),
  1215148432 => 
  array (
    'id' => 18269255,
    'name' => 'ООО «Принтекс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524561148,
    'updated_at' => 1524561149,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33944513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33944513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1215148432',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33944513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33944513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18269255',
        'method' => 'get',
      ),
    ),
  ),
  7451307982 => 
  array (
    'id' => 18273155,
    'name' => 'ООО Торговый дом "Олимп"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524566426,
    'updated_at' => 1524566427,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33948215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33948215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7451307982',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33948215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33948215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18273155',
        'method' => 'get',
      ),
    ),
  ),
  7718955842 => 
  array (
    'id' => 18273405,
    'name' => 'ООО "АСТРОМЕДИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524567596,
    'updated_at' => 1524567621,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33948467,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33948467',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718955842',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33948467,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33948467',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18273405',
        'method' => 'get',
      ),
    ),
  ),
  7716790850 => 
  array (
    'id' => 18273837,
    'name' => 'ООО "ГИПЕРТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524569376,
    'updated_at' => 1524569376,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33948921,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33948921',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716790850',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33948921,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33948921',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18273837',
        'method' => 'get',
      ),
    ),
  ),
  '' => 
  array (
    'id' => 18302861,
    'name' => 'Название сделки',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524814791,
    'updated_at' => 1524814791,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'contacts' => 
    array (
    ),
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302861',
        'method' => 'get',
      ),
    ),
  ),
  5404016051 => 
  array (
    'id' => 18273985,
    'name' => 'ООО "ЛАБОРАТОРИЯ ШКОЛЬНОГО ОБОРУДОВАНИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524569874,
    'updated_at' => 1524570041,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33949063,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33949063',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404016051',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33949063,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33949063',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18273985',
        'method' => 'get',
      ),
    ),
  ),
  3906358714 => 
  array (
    'id' => 18266837,
    'name' => 'ООО «САЛЮТЕМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524551280,
    'updated_at' => 1524571882,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33941735,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941735',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906358714',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33941735,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33941735',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266837',
        'method' => 'get',
      ),
    ),
  ),
  2909000146 => 
  array (
    'id' => 18274669,
    'name' => 'ООО  "Дорожно-передвижная механизированная колонна "Вилегодская"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524572865,
    'updated_at' => 1524572896,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33949861,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33949861',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2909000146',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33949861,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33949861',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18274669',
        'method' => 'get',
      ),
    ),
  ),
  533020145 => 
  array (
    'id' => 18274873,
    'name' => 'ООО "ГРИДМА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524573867,
    'updated_at' => 1524573911,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33950113,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33950113',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '533020145',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33950113,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33950113',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18274873',
        'method' => 'get',
      ),
    ),
  ),
  3446009990 => 
  array (
    'id' => 18274913,
    'name' => 'ООО "Сенар"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524574090,
    'updated_at' => 1524574090,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33950147,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33950147',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3446009990',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33950147,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33950147',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18274913',
        'method' => 'get',
      ),
    ),
  ),
  7810449781 => 
  array (
    'id' => 18274917,
    'name' => 'ООО "КОРНЕО"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524574128,
    'updated_at' => 1524574128,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33950153,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33950153',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810449781',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '-1',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33950153,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33950153',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18274917',
        'method' => 'get',
      ),
    ),
  ),
  7810578868 => 
  array (
    'id' => 15607861,
    'name' => 'ООО Люксорта',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496737184,
    'updated_at' => 1524574227,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30250491,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30250491',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524574227,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810578868',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30250491,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30250491',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15607861',
        'method' => 'get',
      ),
    ),
  ),
  272907606 => 
  array (
    'id' => 18255791,
    'name' => 'ООО «Башстрой-7»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524458659,
    'updated_at' => 1524574594,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33929755,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929755',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '272907606',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33929755,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33929755',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18255791',
        'method' => 'get',
      ),
    ),
  ),
  2723194048 => 
  array (
    'id' => 18266143,
    'name' => 'ООО ЧОО "Добрыня"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1524543381,
    'updated_at' => 1524627717,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33940989,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940989',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723194048',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'хабаровск плюс 10',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33940989,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33940989',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18266143',
        'method' => 'get',
      ),
    ),
  ),
  7412008731 => 
  array (
    'id' => 18278487,
    'name' => 'ООО "Агротэк"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524629539,
    'updated_at' => 1524629540,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33953407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7412008731',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33953407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278487',
        'method' => 'get',
      ),
    ),
  ),
  6685094329 => 
  array (
    'id' => 18032253,
    'name' => 'ООО "СТРОИТЕЛЬНО-МОНТАЖНОЕ ПРЕДПРИЯТИЕ-13"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522212649,
    'updated_at' => 1524630801,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33625689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33625689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6685094329',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33625689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33625689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18032253',
        'method' => 'get',
      ),
    ),
  ),
  1435302376 => 
  array (
    'id' => 18278551,
    'name' => 'ООО "ВОСТОК КАПИТАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524630838,
    'updated_at' => 1524630838,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33953459,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953459',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435302376',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33953459,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953459',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278551',
        'method' => 'get',
      ),
    ),
  ),
  5406582694 => 
  array (
    'id' => 18278553,
    'name' => 'ООО НПО "ОРИОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524630939,
    'updated_at' => 1524630940,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33953465,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953465',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406582694',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33953465,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953465',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278553',
        'method' => 'get',
      ),
    ),
  ),
  6325014960 => 
  array (
    'id' => 18278635,
    'name' => 'ООО "АЛЕВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524632767,
    'updated_at' => 1524632877,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33953573,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953573',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6325014960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33953573,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953573',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278635',
        'method' => 'get',
      ),
    ),
  ),
  2466274755 => 
  array (
    'id' => 18289277,
    'name' => 'ООО СтройМонтажИнвест',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524720975,
    'updated_at' => 1524720976,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466274755',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289277',
        'method' => 'get',
      ),
    ),
  ),
  2466262630 => 
  array (
    'id' => 18278937,
    'name' => 'ООО "БРОНЕВИК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524636128,
    'updated_at' => 1524636298,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33953931,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953931',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466262630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33953931,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953931',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278937',
        'method' => 'get',
      ),
    ),
  ),
  7714825828 => 
  array (
    'id' => 18278973,
    'name' => 'ООО «РЕМОНТНО-СТРОИТЕЛЬНАЯ КОМПАНИЯ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524636430,
    'updated_at' => 1524636431,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714825828',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954001,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954001',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278973',
        'method' => 'get',
      ),
    ),
  ),
  7814571976 => 
  array (
    'id' => 18023187,
    'name' => 'ООО "ГЕФЕСТСТРОЙСНАБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522131928,
    'updated_at' => 1524636661,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33616301,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33616301',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814571976',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33616301,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33616301',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18023187',
        'method' => 'get',
      ),
    ),
  ),
  6234154430 => 
  array (
    'id' => 18278999,
    'name' => 'ООО "ГРУППА КОМПАНИЙ ЭКСПРЕССМЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524636731,
    'updated_at' => 1524636731,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954041,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954041',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6234154430',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954041,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954041',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278999',
        'method' => 'get',
      ),
    ),
  ),
  5040134730 => 
  array (
    'id' => 15206187,
    'name' => 'ООО "МУЗШОП"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1492513259,
    'updated_at' => 1524636944,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29684229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29684229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040134730',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29684229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29684229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15206187',
        'method' => 'get',
      ),
    ),
  ),
  7813528262 => 
  array (
    'id' => 18279023,
    'name' => 'ООО "ГолдВеси"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524637005,
    'updated_at' => 1524637006,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954081,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954081',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813528262',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954081,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954081',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18279023',
        'method' => 'get',
      ),
    ),
  ),
  275063809 => 
  array (
    'id' => 11230426,
    'name' => 'ООО Комстрой"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1456899870,
    'updated_at' => 1524637323,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 23974030,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=23974030',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522724,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '275063809',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 23974030,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=23974030',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11230426',
        'method' => 'get',
      ),
    ),
  ),
  6102067855 => 
  array (
    'id' => 18279067,
    'name' => 'ООО "РУБИКОН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524637267,
    'updated_at' => 1524637619,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954123,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954123',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6102067855',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954123,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954123',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18279067',
        'method' => 'get',
      ),
    ),
  ),
  7704397235 => 
  array (
    'id' => 18279129,
    'name' => 'ООО "ФИНАНСОВАЯ СТРОИТЕЛЬНАЯ КОМПАНИЯ ГЕНСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524637688,
    'updated_at' => 1524637755,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954177,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954177',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704397235',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954177,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954177',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18279129',
        'method' => 'get',
      ),
    ),
  ),
  773571720274 => 
  array (
    'id' => 18279145,
    'name' => 'ИП Шумский Леонид Петрович',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524637819,
    'updated_at' => 1524637853,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954199,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954199',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '773571720274',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954199,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954199',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18279145',
        'method' => 'get',
      ),
    ),
  ),
  9701089966 => 
  array (
    'id' => 18149691,
    'name' => 'ООО Торговый Дом "СФИНКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523435130,
    'updated_at' => 1524638009,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33787151,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787151',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9701089966',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33787151,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33787151',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18149691',
        'method' => 'get',
      ),
    ),
  ),
  4706039014 => 
  array (
    'id' => 18279187,
    'name' => 'ООО «ОБЛФАРМ АЛЬЯНС»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524638153,
    'updated_at' => 1524638154,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954301,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954301',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4706039014',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954301,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954301',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18279187',
        'method' => 'get',
      ),
    ),
  ),
  1835060795 => 
  array (
    'id' => 18279239,
    'name' => 'ООО "УДМУРТГАЗПРОЕКТ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524638328,
    'updated_at' => 1524638362,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524689940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1835060795',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18279239',
        'method' => 'get',
      ),
    ),
  ),
  5401954003 => 
  array (
    'id' => 18278561,
    'name' => 'ООО СК "СИБИРЬСПЕЦАВТОМАТИКА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524631037,
    'updated_at' => 1524642939,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33953471,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953471',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5401954003',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33953471,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953471',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278561',
        'method' => 'get',
      ),
    ),
  ),
  7729498517 => 
  array (
    'id' => 18280101,
    'name' => 'ООО "СТРОЙСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524643015,
    'updated_at' => 1524643016,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33955335,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955335',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7729498517',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33955335,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955335',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18280101',
        'method' => 'get',
      ),
    ),
  ),
  6453040958 => 
  array (
    'id' => 15139125,
    'name' => 'СМУ-3 "Саратовавтодор", ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491896923,
    'updated_at' => 1524643468,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29581289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29581289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6453040958',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29581289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29581289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15139125',
        'method' => 'get',
      ),
    ),
  ),
  1001189689 => 
  array (
    'id' => 18280397,
    'name' => 'ООО "СПЕЦСТРОЙПРОЕКТ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524643969,
    'updated_at' => 1524644005,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33955631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524736800,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1001189689',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33955631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18280397',
        'method' => 'get',
      ),
    ),
  ),
  8603224870 => 
  array (
    'id' => 18280143,
    'name' => 'ООО "НОВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524643168,
    'updated_at' => 1524644098,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33955379,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955379',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603224870',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33955379,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955379',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18280143',
        'method' => 'get',
      ),
    ),
  ),
  5829003683 => 
  array (
    'id' => 18280519,
    'name' => 'ООО "ТЕХНО ТРЕЙД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524644324,
    'updated_at' => 1524644385,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33955763,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955763',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5829003683',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33955763,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955763',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18280519',
        'method' => 'get',
      ),
    ),
  ),
  7447276227 => 
  array (
    'id' => 18280713,
    'name' => 'ООО "ТД" "ЛД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524644440,
    'updated_at' => 1524644441,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33955793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7447276227',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33955793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33955793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18280713',
        'method' => 'get',
      ),
    ),
  ),
  8905055829 => 
  array (
    'id' => 17932427,
    'name' => 'ООО "Ноябрьская ремонто -эксплуатационная компания "№4',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1521088181,
    'updated_at' => 1524647586,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33476671,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33476671',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8905055829',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33476671,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33476671',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17932427',
        'method' => 'get',
      ),
    ),
  ),
  4632069798 => 
  array (
    'id' => 18281567,
    'name' => 'ООО "Бахус"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524647914,
    'updated_at' => 1524648128,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33956777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33956777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632069798',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33956777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33956777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18281567',
        'method' => 'get',
      ),
    ),
  ),
  7805645340 => 
  array (
    'id' => 18281625,
    'name' => 'ООО "СК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524648160,
    'updated_at' => 1524648337,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33956827,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33956827',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805645340',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33956827,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33956827',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18281625',
        'method' => 'get',
      ),
    ),
  ),
  7751008704 => 
  array (
    'id' => 18282369,
    'name' => 'ООО "Новый Век"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524650985,
    'updated_at' => 1524651020,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33957571,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33957571',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751008704',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33957571,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33957571',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18282369',
        'method' => 'get',
      ),
    ),
  ),
  6314022493 => 
  array (
    'id' => 18282423,
    'name' => 'ООО Веха- Регион',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524651220,
    'updated_at' => 1524651399,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33957629,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33957629',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6314022493',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33957629,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33957629',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18282423',
        'method' => 'get',
      ),
    ),
  ),
  8905050436 => 
  array (
    'id' => 18282487,
    'name' => 'ООО "МРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524651507,
    'updated_at' => 1524651507,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33957699,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33957699',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8905050436',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33957699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33957699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18282487',
        'method' => 'get',
      ),
    ),
  ),
  9102189560 => 
  array (
    'id' => 18282843,
    'name' => 'ООО "АВИАКОМПАНИЯ "СЕВ-АВИА"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524652869,
    'updated_at' => 1524652870,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33958045,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33958045',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102189560',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33958045,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33958045',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18282843',
        'method' => 'get',
      ),
    ),
  ),
  7716783450 => 
  array (
    'id' => 15664035,
    'name' => 'ООО СКАНДИМЕД',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1497349387,
    'updated_at' => 1524655222,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30324273,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324273',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716783450',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30324273,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30324273',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15664035',
        'method' => 'get',
      ),
    ),
  ),
  7735148060 => 
  array (
    'id' => 18283571,
    'name' => 'ООО "РТ-медкомплект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524655747,
    'updated_at' => 1524655748,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33958873,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33958873',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735148060',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33958873,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33958873',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18283571',
        'method' => 'get',
      ),
    ),
  ),
  2536283675 => 
  array (
    'id' => 18284347,
    'name' => 'ООО "Бизнес Поставка"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524658869,
    'updated_at' => 1524658869,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33959763,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959763',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2536283675',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33959763,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959763',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18284347',
        'method' => 'get',
      ),
    ),
  ),
  7842352630 => 
  array (
    'id' => 18284371,
    'name' => 'ООО "Пассажир ТВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524658969,
    'updated_at' => 1524658969,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33959785,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959785',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842352630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33959785,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959785',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18284371',
        'method' => 'get',
      ),
    ),
  ),
  2635106412 => 
  array (
    'id' => 16179335,
    'name' => 'ООО  "АМАЛЬГАМА СК"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1503054166,
    'updated_at' => 1524659252,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31248447,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31248447',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1503054261,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2635106412',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31248447,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31248447',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16179335',
        'method' => 'get',
      ),
    ),
  ),
  170102448303 => 
  array (
    'id' => 15727499,
    'name' => 'ИП САМЧАН АЛЕКСЕЙ ДЕЛГЕР-ООЛОВИЧ',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1498016294,
    'updated_at' => 1524715862,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '170102448303',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'плюс 7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407001,
        1 => 30407047,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407001,30407047',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15727499',
        'method' => 'get',
      ),
    ),
  ),
  4205295240 => 
  array (
    'id' => 18288845,
    'name' => 'ООО "СПМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524715951,
    'updated_at' => 1524715952,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4205295240',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288845',
        'method' => 'get',
      ),
    ),
  ),
  7203406383 => 
  array (
    'id' => 18288853,
    'name' => 'ООО "ПИАРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524716084,
    'updated_at' => 1524716127,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964083,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964083',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203406383',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964083,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964083',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288853',
        'method' => 'get',
      ),
    ),
  ),
  5404484902 => 
  array (
    'id' => 18288877,
    'name' => 'ООО "ПРОМСТРОЙИНВЕСТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524716459,
    'updated_at' => 1524716893,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964101,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964101',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404484902',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964101,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964101',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288877',
        'method' => 'get',
      ),
    ),
  ),
  7447030495 => 
  array (
    'id' => 18288933,
    'name' => 'ООО МП "Гидроремонт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524716993,
    'updated_at' => 1524717403,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964149,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964149',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7447030495',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964149,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964149',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288933',
        'method' => 'get',
      ),
    ),
  ),
  2465166468 => 
  array (
    'id' => 18288989,
    'name' => 'ООО "СК Грантэк"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524717651,
    'updated_at' => 1524718341,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964207,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964207',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465166468',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964207,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964207',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288989',
        'method' => 'get',
      ),
    ),
  ),
  1435156326 => 
  array (
    'id' => 18288849,
    'name' => '"СПЕЦВОДСТРОЙ"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1524716011,
    'updated_at' => 1524719069,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964079,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964079',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435156326',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '10',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964079,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964079',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288849',
        'method' => 'get',
      ),
    ),
  ),
  8620022778 => 
  array (
    'id' => 18289119,
    'name' => 'ООО "СК Сибтрансбизнес"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524719002,
    'updated_at' => 1524719553,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964323,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964323',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8620022778',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964323,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964323',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289119',
        'method' => 'get',
      ),
    ),
  ),
  6658473007 => 
  array (
    'id' => 15761519,
    'name' => 'ООО "ЛЕВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1498455161,
    'updated_at' => 1524719625,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30460047,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30460047',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524719625,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6658473007',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30460047,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30460047',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15761519',
        'method' => 'get',
      ),
    ),
  ),
  5404493223 => 
  array (
    'id' => 18289193,
    'name' => 'ООО "ПАРНИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524719825,
    'updated_at' => 1524720046,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964383,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964383',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404493223',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964383,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964383',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289193',
        'method' => 'get',
      ),
    ),
  ),
  26005704861 => 
  array (
    'id' => 18289211,
    'name' => 'ИП Исаев Марат Мидхатович',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524720103,
    'updated_at' => 1524720153,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964405,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964405',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '26005704861',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964405,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964405',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289211',
        'method' => 'get',
      ),
    ),
  ),
  5410056782 => 
  array (
    'id' => 18289237,
    'name' => 'ООО ХимЭлектроМонтаж',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524720580,
    'updated_at' => 1524720742,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964443,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964443',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410056782',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964443,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964443',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289237',
        'method' => 'get',
      ),
    ),
  ),
  3801137732 => 
  array (
    'id' => 15614425,
    'name' => 'ООО "Сибирский стиль"',
    'responsible_user_id' => 1261404,
    'created_by' => 1310841,
    'created_at' => 1496809890,
    'updated_at' => 1524720873,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30258741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30258741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1508210231,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3801137732',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30258741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30258741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15614425',
        'method' => 'get',
      ),
    ),
  ),
  667359019240 => 
  array (
    'id' => 18289309,
    'name' => 'ИП Шалаев Андрей Андреевич',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524721392,
    'updated_at' => 1524721465,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964525,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964525',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '667359019240',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964525,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964525',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289309',
        'method' => 'get',
      ),
    ),
  ),
  2464063773 => 
  array (
    'id' => 15483789,
    'name' => 'ООО "КСТС"',
    'responsible_user_id' => 1261404,
    'created_by' => 1310841,
    'created_at' => 1495604118,
    'updated_at' => 1524722044,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30063405,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063405',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1508382890,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464063773',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Красноярский край(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30063405,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30063405',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15483789',
        'method' => 'get',
      ),
    ),
  ),
  910207912861 => 
  array (
    'id' => 18107061,
    'name' => 'РОМАНЧУК АЛЕКСЕЙ ВИКТОРОВИЧ -',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1522924995,
    'updated_at' => 1524722777,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33742095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33742095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524722773,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '910207912861',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'крым',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33742095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33742095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18107061',
        'method' => 'get',
      ),
    ),
  ),
  7730239394 => 
  array (
    'id' => 17299529,
    'name' => 'ООО "Феникс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1513921612,
    'updated_at' => 1524722946,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32594655,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32594655',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513928150,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7730239394',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32594655,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32594655',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17299529',
        'method' => 'get',
      ),
    ),
  ),
  5047166723 => 
  array (
    'id' => 18289501,
    'name' => 'ООО "ВИАК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524723414,
    'updated_at' => 1524723415,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964775,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964775',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047166723',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964775,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964775',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289501',
        'method' => 'get',
      ),
    ),
  ),
  5031123731 => 
  array (
    'id' => 18289527,
    'name' => 'ООО "Оздоровительный комплекс"МИР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524723617,
    'updated_at' => 1524723618,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5031123731',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289527',
        'method' => 'get',
      ),
    ),
  ),
  3443134400 => 
  array (
    'id' => 18289559,
    'name' => 'ООО "Альпика"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524723815,
    'updated_at' => 1524723815,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964829,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964829',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3443134400',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964829,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964829',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289559',
        'method' => 'get',
      ),
    ),
  ),
  544011343 => 
  array (
    'id' => 18289571,
    'name' => 'ООО «Благоустройство»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524723902,
    'updated_at' => 1524724810,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964841,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964841',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '544011343',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964841,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964841',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289571',
        'method' => 'get',
      ),
    ),
  ),
  3666206850 => 
  array (
    'id' => 18289741,
    'name' => 'ООО "АТОМЭНЕРГОКОМПЛЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524724940,
    'updated_at' => 1524725069,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33965015,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965015',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3666206850',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33965015,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965015',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289741',
        'method' => 'get',
      ),
    ),
  ),
  6657000375 => 
  array (
    'id' => 18289853,
    'name' => 'ооо "Шалинское строительное управление"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524725523,
    'updated_at' => 1524725605,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33965135,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965135',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33965133,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33965133',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525406400,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6657000375',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'ека плюс 5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33965135,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965135',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289853',
        'method' => 'get',
      ),
    ),
  ),
  7719027526 => 
  array (
    'id' => 15406439,
    'name' => 'Финком - А, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1494843872,
    'updated_at' => 1524725737,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29954601,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29954601',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7719027526',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29954601,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29954601',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15406439',
        'method' => 'get',
      ),
    ),
  ),
  1435203093 => 
  array (
    'id' => 11575474,
    'name' => 'ООО "ГЕОПРОЕКТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 553668,
    'created_at' => 1459403085,
    'updated_at' => 1524725916,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 24538784,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24538784',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521309,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435203093',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Саха (Якутия)(GMT +10)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 24538784,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=24538784',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11575474',
        'method' => 'get',
      ),
    ),
  ),
  2311187108 => 
  array (
    'id' => 18289933,
    'name' => 'ООО "САНАРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524725948,
    'updated_at' => 1524725948,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33965227,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965227',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311187108',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33965227,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965227',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289933',
        'method' => 'get',
      ),
    ),
  ),
  1660264474 => 
  array (
    'id' => 18290315,
    'name' => 'ООО «Автолюкс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524727625,
    'updated_at' => 1524727626,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33965647,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965647',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660264474',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33965647,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965647',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18290315',
        'method' => 'get',
      ),
    ),
  ),
  7703386417 => 
  array (
    'id' => 17416465,
    'name' => 'ООО «БАОЛИРУС Интернейшнл Инвестментс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1515744519,
    'updated_at' => 1524728150,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32736027,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32736027',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1515744585,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7703386417',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32736027,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32736027',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17416465',
        'method' => 'get',
      ),
    ),
  ),
  7704378955 => 
  array (
    'id' => 15495141,
    'name' => 'ООО "Учебное оборудование"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1495691665,
    'updated_at' => 1524728165,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921445,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704378955',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Ямало-Ненецкий АО(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15495141',
        'method' => 'get',
      ),
    ),
  ),
  3525376810 => 
  array (
    'id' => 18291033,
    'name' => 'ООО "ИНСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524730270,
    'updated_at' => 1524730382,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33966407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33966407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525376810',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33966407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33966407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18291033',
        'method' => 'get',
      ),
    ),
  ),
  3442043694 => 
  array (
    'id' => 18291289,
    'name' => 'ЗАО "ДОРИС"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524731407,
    'updated_at' => 1524731408,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33966709,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33966709',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3442043694',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33966709,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33966709',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18291289',
        'method' => 'get',
      ),
    ),
  ),
  3525003462 => 
  array (
    'id' => 18288833,
    'name' => 'ООО "Стройтранс"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524715836,
    'updated_at' => 1524737968,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964073,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964073',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3525003462',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964073,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964073',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288833',
        'method' => 'get',
      ),
    ),
  ),
  5406604838 => 
  array (
    'id' => 18292065,
    'name' => 'ООО "ПРОФИТ-Н"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524733873,
    'updated_at' => 1524733874,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33967427,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33967427',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406604838',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33967427,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33967427',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18292065',
        'method' => 'get',
      ),
    ),
  ),
  7720380133 => 
  array (
    'id' => 18292125,
    'name' => 'ООО "ЭкоСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524734136,
    'updated_at' => 1524734168,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33967493,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33967493',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720380133',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33967493,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33967493',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18292125',
        'method' => 'get',
      ),
    ),
  ),
  323091284 => 
  array (
    'id' => 18292133,
    'name' => 'АО Медтехника',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524734167,
    'updated_at' => 1524734198,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33967501,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33967501',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524805200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323091284',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33967501,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33967501',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18292133',
        'method' => 'get',
      ),
    ),
  ),
  7813413840 => 
  array (
    'id' => 18216575,
    'name' => '"ДС-Инжиниринг"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1524031140,
    'updated_at' => 1524734205,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33883449,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883449',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33933313,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33933313',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813413840',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3 питер',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33883449,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33883449',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18216575',
        'method' => 'get',
      ),
    ),
  ),
  7814622469 => 
  array (
    'id' => 14810767,
    'name' => 'ООО  "СПБ-ЭНЕРГОТЕХНОЛОГИИ"',
    'responsible_user_id' => 1277505,
    'created_by' => 892789,
    'created_at' => 1488527362,
    'updated_at' => 1524736738,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29157181,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157181',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814622469',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29157181,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29157181',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14810767',
        'method' => 'get',
      ),
    ),
  ),
  7704267236 => 
  array (
    'id' => 18283913,
    'name' => 'ООО "Романов 5"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524657058,
    'updated_at' => 1524737979,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33959259,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959259',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704267236',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33959259,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959259',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18283913',
        'method' => 'get',
      ),
    ),
  ),
  7728380431 => 
  array (
    'id' => 18289467,
    'name' => 'ООО «Аналитэксперт»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524723052,
    'updated_at' => 1524737990,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964729,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964729',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7728380431',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964729,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964729',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289467',
        'method' => 'get',
      ),
    ),
  ),
  2204084250 => 
  array (
    'id' => 18288869,
    'name' => 'ООО «Сервисный центр котельного завода»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524716306,
    'updated_at' => 1524738006,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2204084250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288869',
        'method' => 'get',
      ),
    ),
  ),
  6629015764 => 
  array (
    'id' => 18296121,
    'name' => 'ООО  "Рукор"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524741626,
    'updated_at' => 1524741627,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33971061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33971061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6629015764',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33971061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33971061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18296121',
        'method' => 'get',
      ),
    ),
  ),
  7802604715 => 
  array (
    'id' => 18279037,
    'name' => 'ООО ДОЛ "СЕРЕБРЯНЫЙ РУЧЕЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524637088,
    'updated_at' => 1524741871,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33954095,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954095',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802604715',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33954095,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33954095',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18279037',
        'method' => 'get',
      ),
    ),
  ),
  2632106559 => 
  array (
    'id' => 18296819,
    'name' => 'ООО "ЛОТОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524742217,
    'updated_at' => 1524742279,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33971655,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33971655',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2632106559',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33971655,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33971655',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18296819',
        'method' => 'get',
      ),
    ),
  ),
  3906262674 => 
  array (
    'id' => 15270791,
    'name' => 'ООО «КЛГД Строй»',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1493207317,
    'updated_at' => 1524742810,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29791631,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29791631',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906262674',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29791631,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29791631',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15270791',
        'method' => 'get',
      ),
    ),
  ),
  7224044208 => 
  array (
    'id' => 18297065,
    'name' => 'ООО ТПК ЛИДЕР',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524742971,
    'updated_at' => 1524743076,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33971901,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33971901',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7224044208',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33971901,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33971901',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18297065',
        'method' => 'get',
      ),
    ),
  ),
  7724947864 => 
  array (
    'id' => 18301355,
    'name' => 'ооо экоконсалтинг',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524798985,
    'updated_at' => 1524799047,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33863135,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863135',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33433741,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33433741',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724947864',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'москва',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33863135,
        1 => 33975747,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33863135,33975747',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301355',
        'method' => 'get',
      ),
    ),
  ),
  382501967909 => 
  array (
    'id' => 18301411,
    'name' => 'Овчинников Николай Александрович',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524800830,
    'updated_at' => 1524800909,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975783,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975783',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525663800,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '382501967909',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'иркутск 8',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975783,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975783',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301411',
        'method' => 'get',
      ),
    ),
  ),
  410100370394 => 
  array (
    'id' => 18301427,
    'name' => 'ИП Костюк Андрей Анатольевич',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524801069,
    'updated_at' => 1524801085,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975791,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975791',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '410100370394',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975791,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975791',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301427',
        'method' => 'get',
      ),
    ),
  ),
  7203181570 => 
  array (
    'id' => 18301455,
    'name' => 'ооо "РусЛайнСпорт"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524801588,
    'updated_at' => 1524801602,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975813,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975813',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203181570',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975813,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975813',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301455',
        'method' => 'get',
      ),
    ),
  ),
  7017250700 => 
  array (
    'id' => 18301499,
    'name' => 'ооо "ПромСтройЗащита"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524802357,
    'updated_at' => 1524802367,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975859,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975859',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017250700',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975859,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975859',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301499',
        'method' => 'get',
      ),
    ),
  ),
  7452129203 => 
  array (
    'id' => 18301437,
    'name' => 'ооо "СТРОЙСЕРВИС"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524801382,
    'updated_at' => 1524802587,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975801,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975801',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7452129203',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'плюс 5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975801,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975801',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301437',
        'method' => 'get',
      ),
    ),
  ),
  720319878507 => 
  array (
    'id' => 7047316,
    'name' => 'ИП Столбов Андрей Сергеевич',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1444626913,
    'updated_at' => 1524802743,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 18567028,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=18567028',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 18567326,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=18567326',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521351,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '720319878507',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 18567028,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=18567028',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=7047316',
        'method' => 'get',
      ),
    ),
  ),
  2222789968 => 
  array (
    'id' => 18301533,
    'name' => 'ООО "СК ВИКТОРИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524802766,
    'updated_at' => 1524802781,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975881,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975881',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2222789968',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975881,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975881',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301533',
        'method' => 'get',
      ),
    ),
  ),
  7203392814 => 
  array (
    'id' => 18301541,
    'name' => 'ООО "МЕДТРОНИКА ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524802821,
    'updated_at' => 1524802822,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975885,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975885',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203392814',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975885,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975885',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301541',
        'method' => 'get',
      ),
    ),
  ),
  2221229917 => 
  array (
    'id' => 18022967,
    'name' => 'ООО "МЕД ЗАЩИТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522129729,
    'updated_at' => 1524802937,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33616029,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33616029',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2221229917',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33616029,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33616029',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18022967',
        'method' => 'get',
      ),
    ),
  ),
  3849066279 => 
  array (
    'id' => 18032201,
    'name' => 'ООО "ЛИДЕРСИБСТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1522211769,
    'updated_at' => 1524804249,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33625623,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33625623',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525664700,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3849066279',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33625623,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33625623',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18032201',
        'method' => 'get',
      ),
    ),
  ),
  2465234541 => 
  array (
    'id' => 18301663,
    'name' => 'ООО "АСФАЛЬТ-ИНФРАРЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524804814,
    'updated_at' => 1524804814,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976011,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976011',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2465234541',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976011,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976011',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301663',
        'method' => 'get',
      ),
    ),
  ),
  3849055990 => 
  array (
    'id' => 18301671,
    'name' => 'ООО "СК ПРОГРЕСС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524804949,
    'updated_at' => 1524805053,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976025,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976025',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3849055990',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976025,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976025',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301671',
        'method' => 'get',
      ),
    ),
  ),
  2466167633 => 
  array (
    'id' => 18301765,
    'name' => 'ООО "ПРОФСТАНДАРТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524806277,
    'updated_at' => 1524806343,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976137,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976137',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2466167633',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976137,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976137',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301765',
        'method' => 'get',
      ),
    ),
  ),
  7203371758 => 
  array (
    'id' => 18301795,
    'name' => 'ООО ЭКСПЕРТНАЯ ОРГАНИЗАЦИЯ "РАЗВИТИЕ И ОСТОРОЖНОСТЬ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524806692,
    'updated_at' => 1524806692,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976161,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976161',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7203371758',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976161,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976161',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301795',
        'method' => 'get',
      ),
    ),
  ),
  3811440858 => 
  array (
    'id' => 18301809,
    'name' => 'ООО "ДРАФТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524806941,
    'updated_at' => 1524806992,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976175,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976175',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811440858',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976175,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976175',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301809',
        'method' => 'get',
      ),
    ),
  ),
  6367011336 => 
  array (
    'id' => 18301917,
    'name' => 'ооо "МЕТРОЛОГ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524808049,
    'updated_at' => 1524808049,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976311,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976311',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6367011336',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '12',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976311,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976311',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301917',
        'method' => 'get',
      ),
    ),
  ),
  6672303952 => 
  array (
    'id' => 18301637,
    'name' => 'ооо "САПФИР - ЭКСПЕРТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524804612,
    'updated_at' => 1524808276,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975997,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975997',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524808800,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672303952',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975997,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975997',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301637',
        'method' => 'get',
      ),
    ),
  ),
  4611004126 => 
  array (
    'id' => 18262257,
    'name' => 'ООО "Санаторий "Моква"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524486509,
    'updated_at' => 1524809916,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33936837,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936837',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4611004126',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33936837,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33936837',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18262257',
        'method' => 'get',
      ),
    ),
  ),
  7724334459 => 
  array (
    'id' => 14948703,
    'name' => 'Проектплюс, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1490073964,
    'updated_at' => 1524810092,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29334257,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334257',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724334459',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29334257,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29334257',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14948703',
        'method' => 'get',
      ),
    ),
  ),
  7723850669 => 
  array (
    'id' => 18302359,
    'name' => 'ООО "Тимтек"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524811772,
    'updated_at' => 1524811772,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976871,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976871',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723850669',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976871,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976871',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302359',
        'method' => 'get',
      ),
    ),
  ),
  3808047372 => 
  array (
    'id' => 14959041,
    'name' => 'СИБНА, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1490156071,
    'updated_at' => 1524811953,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29357213,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357213',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3808047372',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Иркутская область(GMT +8)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29357213,
        1 => 29565681,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29357213,29565681',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14959041',
        'method' => 'get',
      ),
    ),
  ),
  7721290468 => 
  array (
    'id' => 18302463,
    'name' => 'ООО "Прогрессинженерстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524812478,
    'updated_at' => 1524812478,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976975,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976975',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721290468',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976975,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976975',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302463',
        'method' => 'get',
      ),
    ),
  ),
  7705722311 => 
  array (
    'id' => 18302519,
    'name' => 'ООО "МИЛАНСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524812771,
    'updated_at' => 1524812772,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33977019,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977019',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7705722311',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33977019,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977019',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302519',
        'method' => 'get',
      ),
    ),
  ),
  7708744183 => 
  array (
    'id' => 15113067,
    'name' => 'Руспрофстрой, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491559791,
    'updated_at' => 1524812938,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29551971,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29551971',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7708744183',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29551971,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29551971',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15113067',
        'method' => 'get',
      ),
    ),
  ),
  7704590905 => 
  array (
    'id' => 15813509,
    'name' => 'ООО "Монтаж-Строй',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1499071533,
    'updated_at' => 1524813638,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30535155,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30535155',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524813638,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7704590905',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30535155,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30535155',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15813509',
        'method' => 'get',
      ),
    ),
  ),
  323087908 => 
  array (
    'id' => 18305821,
    'name' => 'ООО "СТРОИТЕЛЬНО-МОНТАЖНЫЙ ПОЕЗД №398"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524828454,
    'updated_at' => 1524828454,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33980453,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980453',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323087908',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33980453,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980453',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305821',
        'method' => 'get',
      ),
    ),
  ),
  7842511320 => 
  array (
    'id' => 15417603,
    'name' => 'Ростстрой, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1494933013,
    'updated_at' => 1524816819,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29969289,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29969289',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842511320',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29969289,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29969289',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15417603',
        'method' => 'get',
      ),
    ),
  ),
  1659177770 => 
  array (
    'id' => 18303553,
    'name' => 'ООО «ИНТЕРЗАПЧАСТЬ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524817303,
    'updated_at' => 1524817303,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33977955,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977955',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659177770',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33977955,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977955',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18303553',
        'method' => 'get',
      ),
    ),
  ),
  3615002664 => 
  array (
    'id' => 18303665,
    'name' => 'ООО "Нижнедевицкавтодор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524817830,
    'updated_at' => 1524817857,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33978067,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33978067',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3615002664',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33978067,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33978067',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18303665',
        'method' => 'get',
      ),
    ),
  ),
  5035039400 => 
  array (
    'id' => 18303917,
    'name' => 'ООО "АРХИТЕКТУРНО-ПЛАНИРОВОЧНАЯ МАСТЕРСКАЯ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524818973,
    'updated_at' => 1524818973,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33978361,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33978361',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5035039400',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33978361,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33978361',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18303917',
        'method' => 'get',
      ),
    ),
  ),
  5906121934 => 
  array (
    'id' => 14478708,
    'name' => 'ООО "ОСНОВА-ПЛЮС"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1485336788,
    'updated_at' => 1524819240,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28704616,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704616',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5906121934',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'пермь плюс 5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28704616,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28704616',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14478708',
        'method' => 'get',
      ),
    ),
  ),
  8603187579 => 
  array (
    'id' => 18288809,
    'name' => 'ООО "Мобильная экспедиция" 
"мобэкс"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524715482,
    'updated_at' => 1524819589,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33964051,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964051',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8603187579',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'ханты манс плюс 5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33964051,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33964051',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18288809',
        'method' => 'get',
      ),
    ),
  ),
  542027891 => 
  array (
    'id' => 18304709,
    'name' => 'ОАО "ДОРОЖНО-СТРОИТЕЛЬНОЕ УПРАВЛЕНИЕ № 3"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524822689,
    'updated_at' => 1524822689,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33979267,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979267',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '542027891',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3 астрахань',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33979267,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979267',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18304709',
        'method' => 'get',
      ),
    ),
  ),
  7735542228 => 
  array (
    'id' => 18304867,
    'name' => 'ООО  «Научно-производственное предприятие «Доза»',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524823446,
    'updated_at' => 1524823446,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33979399,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979399',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735542228',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33979399,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979399',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18304867',
        'method' => 'get',
      ),
    ),
  ),
  2373012370 => 
  array (
    'id' => 18305053,
    'name' => 'ООО "ЭкспрессДорУслуги"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524824463,
    'updated_at' => 1524824464,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33979629,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979629',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2373012370',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33979629,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979629',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305053',
        'method' => 'get',
      ),
    ),
  ),
  5029176574 => 
  array (
    'id' => 18305133,
    'name' => 'ООО Научно-производственное объединение "Инвест"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524824912,
    'updated_at' => 1524824973,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33979713,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979713',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5029176574',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33979713,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979713',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305133',
        'method' => 'get',
      ),
    ),
  ),
  7702778115 => 
  array (
    'id' => 18305193,
    'name' => 'ООО "СОФТЛАЙН АУТСОРСИНГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524825205,
    'updated_at' => 1524825205,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33979797,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979797',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702778115',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33979797,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979797',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305193',
        'method' => 'get',
      ),
    ),
  ),
  5753061178 => 
  array (
    'id' => 18305305,
    'name' => 'ООО "ЭЛЕКТРОТЕКС-ИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524825605,
    'updated_at' => 1524825606,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33979901,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979901',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5753061178',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33979901,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33979901',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305305',
        'method' => 'get',
      ),
    ),
  ),
  1650170026 => 
  array (
    'id' => 18302943,
    'name' => 'ООО «Кама-НК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524815283,
    'updated_at' => 1524826076,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33977497,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977497',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524893400,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1650170026',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33977497,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977497',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302943',
        'method' => 'get',
      ),
    ),
  ),
  1658033159 => 
  array (
    'id' => 18305393,
    'name' => 'ООО "Элмас"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524826154,
    'updated_at' => 1524826155,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33980001,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980001',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1658033159',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33980001,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980001',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305393',
        'method' => 'get',
      ),
    ),
  ),
  7706813868 => 
  array (
    'id' => 18305481,
    'name' => 'ООО "ИНСТИТУТ ПРИКЛАДНЫХ ПОЛИТИЧЕСКИХ ИССЛЕДОВАНИЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524826598,
    'updated_at' => 1524826618,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33980091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7706813868',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33980091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305481',
        'method' => 'get',
      ),
    ),
  ),
  7715926970 => 
  array (
    'id' => 18305549,
    'name' => 'ООО "ЮЗА-Газ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524827010,
    'updated_at' => 1524827011,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33980167,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980167',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715926970',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33980167,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980167',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305549',
        'method' => 'get',
      ),
    ),
  ),
  3435113950 => 
  array (
    'id' => 18305567,
    'name' => 'ооо "Стройсоюз"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524827119,
    'updated_at' => 1524827120,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33980189,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980189',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33980187,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33980187',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435113950',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33980189,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980189',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305567',
        'method' => 'get',
      ),
    ),
  ),
  506007875 => 
  array (
    'id' => 18305623,
    'name' => 'ООО "МЕДТРЕЙД"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524827498,
    'updated_at' => 1524827499,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33980261,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980261',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '506007875',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33980261,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33980261',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18305623',
        'method' => 'get',
      ),
    ),
  ),
  2411027524 => 
  array (
    'id' => 18309837,
    'name' => 'ООО "ПРОЕКТ-МН"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524889730,
    'updated_at' => 1524889765,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33983901,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983901',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2411027524',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33983901,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983901',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18309837',
        'method' => 'get',
      ),
    ),
  ),
  5406983110 => 
  array (
    'id' => 18309839,
    'name' => 'ООО "Контактные сети"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524889786,
    'updated_at' => 1524889876,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33983905,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983905',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406983110',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33983905,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983905',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18309839',
        'method' => 'get',
      ),
    ),
  ),
  6150071298 => 
  array (
    'id' => 18309845,
    'name' => 'ООО «ЮгАгроГрупп»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524889903,
    'updated_at' => 1524889903,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33983907,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983907',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6150071298',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33983907,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983907',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18309845',
        'method' => 'get',
      ),
    ),
  ),
  5407967584 => 
  array (
    'id' => 18309855,
    'name' => 'ООО "УТП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524890120,
    'updated_at' => 1524890242,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33983915,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983915',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407967584',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33983915,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983915',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18309855',
        'method' => 'get',
      ),
    ),
  ),
  2722103256 => 
  array (
    'id' => 18309887,
    'name' => 'ООО «Капитал-Восток»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524890578,
    'updated_at' => 1524890579,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33983939,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983939',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2722103256',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33983939,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983939',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18309887',
        'method' => 'get',
      ),
    ),
  ),
  6316231855 => 
  array (
    'id' => 16240663,
    'name' => 'ООО "МАСТЕР"',
    'responsible_user_id' => 1277505,
    'created_by' => 609492,
    'created_at' => 1503905397,
    'updated_at' => 1524892202,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31325021,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31325021',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6316231855',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31325021,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31325021',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678381,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16240663',
        'method' => 'get',
      ),
    ),
  ),
  277129484 => 
  array (
    'id' => 18310079,
    'name' => 'ООО «Матрикс Универсал»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524892840,
    'updated_at' => 1524892903,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984075,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984075',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '277129484',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984075,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984075',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310079',
        'method' => 'get',
      ),
    ),
  ),
  6230089886 => 
  array (
    'id' => 18310093,
    'name' => 'ООО «АРМАДАОИЛ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524892977,
    'updated_at' => 1524892978,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984079,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984079',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6230089886',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984079,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984079',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310093',
        'method' => 'get',
      ),
    ),
  ),
  2457065988 => 
  array (
    'id' => 18310205,
    'name' => 'ооо  "НорВент"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524894399,
    'updated_at' => 1524894427,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984227,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984227',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2457065988',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7 красно',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984227,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984227',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310205',
        'method' => 'get',
      ),
    ),
  ),
  2721222282 => 
  array (
    'id' => 18309971,
    'name' => 'ООО "ДВ-Строй"',
    'responsible_user_id' => 1261404,
    'created_by' => 1338850,
    'created_at' => 1524891473,
    'updated_at' => 1524895096,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33983987,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983987',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2721222282',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33983987,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33983987',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18309971',
        'method' => 'get',
      ),
    ),
  ),
  7327047994 => 
  array (
    'id' => 18302075,
    'name' => 'АО "Автодормостпроект"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524809584,
    'updated_at' => 1524895392,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976527,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976527',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1524895200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7327047994',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'ульяновск',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976527,
        1 => 33984319,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976527,33984319',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302075',
        'method' => 'get',
      ),
    ),
  ),
  2806007037 => 
  array (
    'id' => 14969119,
    'name' => 'ООО "СМУ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1490240050,
    'updated_at' => 1524895857,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29371849,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371849',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521548,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2806007037',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29371849,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29371849',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14969119',
        'method' => 'get',
      ),
    ),
  ),
  5402518987 => 
  array (
    'id' => 14892067,
    'name' => 'ООО "Медскан"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1489475044,
    'updated_at' => 1524896292,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29261437,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29261437',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605993,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5402518987',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29261437,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29261437',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14892067',
        'method' => 'get',
      ),
    ),
  ),
  9717012278 => 
  array (
    'id' => 18310307,
    'name' => 'ООО "Армада"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524895533,
    'updated_at' => 1524896574,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984331,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984331',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9717012278',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984331,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984331',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310307',
        'method' => 'get',
      ),
    ),
  ),
  6167135694 => 
  array (
    'id' => 18310567,
    'name' => 'ООО "Ростводосток"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524897426,
    'updated_at' => 1524897427,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984583,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984583',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6167135694',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984583,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984583',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310567',
        'method' => 'get',
      ),
    ),
  ),
  7743927831 => 
  array (
    'id' => 18310581,
    'name' => 'ООО «Научно-технический центр «ПромСтройЭкспертиза»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524897525,
    'updated_at' => 1524897618,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984599,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984599',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743927831',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984599,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984599',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310581',
        'method' => 'get',
      ),
    ),
  ),
  5047204231 => 
  array (
    'id' => 18310649,
    'name' => 'ООО «Сэйл АйТи»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524898024,
    'updated_at' => 1524898025,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984667,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984667',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5047204231',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984667,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984667',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310649',
        'method' => 'get',
      ),
    ),
  ),
  7702409809 => 
  array (
    'id' => 18310711,
    'name' => 'ООО «ЛАБОРАТОРИЯ КАРДИОЛОГИИ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524898398,
    'updated_at' => 1524898398,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984719,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984719',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702409809',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984719,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984719',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310711',
        'method' => 'get',
      ),
    ),
  ),
  7801408771 => 
  array (
    'id' => 18310717,
    'name' => 'ООО "Системный Специалист"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524898481,
    'updated_at' => 1524899033,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984733,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984733',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7801408771',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984733,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984733',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310717',
        'method' => 'get',
      ),
    ),
  ),
  1657140408 => 
  array (
    'id' => 18310883,
    'name' => 'ООО "БАРСПРОФИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524899610,
    'updated_at' => 1524899610,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984883,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984883',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657140408',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984883,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984883',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310883',
        'method' => 'get',
      ),
    ),
  ),
  7718654563 => 
  array (
    'id' => 18310999,
    'name' => 'ООО "ПРОЕКТНЫЕ ТЕХНОЛОГИИ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524900404,
    'updated_at' => 1524900405,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33985027,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985027',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718654563',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33985027,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985027',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310999',
        'method' => 'get',
      ),
    ),
  ),
  1644040050 => 
  array (
    'id' => 18302235,
    'name' => '"СОВРЕМЕННЫЕ СТРОИТЕЛЬНЫЕ СИСТЕМЫ"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1524810928,
    'updated_at' => 1524901004,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33976703,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976703',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1644040050',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33976703,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33976703',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302235',
        'method' => 'get',
      ),
    ),
  ),
  7714886066 => 
  array (
    'id' => 18311231,
    'name' => 'ООО "ШТАРК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524901678,
    'updated_at' => 1524901823,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33985243,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985243',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714886066',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33985243,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985243',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18311231',
        'method' => 'get',
      ),
    ),
  ),
  7721737403 => 
  array (
    'id' => 18311253,
    'name' => 'ООО "АЙ ДЖЕЙ ПРОДАКШИН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524901891,
    'updated_at' => 1524901892,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33985265,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985265',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721737403',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33985265,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985265',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18311253',
        'method' => 'get',
      ),
    ),
  ),
  7725395648 => 
  array (
    'id' => 18311265,
    'name' => 'ООО "СФЕРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524901991,
    'updated_at' => 1524901991,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33985279,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985279',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7725395648',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33985279,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985279',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18311265',
        'method' => 'get',
      ),
    ),
  ),
  1655180620 => 
  array (
    'id' => 18311287,
    'name' => 'ООО "Агентство "Мир Переводов"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524902172,
    'updated_at' => 1524902183,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33985313,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985313',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655180620',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'казань 3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33985313,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985313',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18311287',
        'method' => 'get',
      ),
    ),
  ),
  3913502091 => 
  array (
    'id' => 18311285,
    'name' => 'ООО «Коммунальщик»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524902155,
    'updated_at' => 1524902357,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33985311,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985311',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3913502091',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33985311,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985311',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18311285',
        'method' => 'get',
      ),
    ),
  ),
  9718071477 => 
  array (
    'id' => 17056015,
    'name' => 'ООО "НТБ СОЛЮШИОНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511760936,
    'updated_at' => 1524903531,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32330779,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330779',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718071477',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32330779,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32330779',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056015',
        'method' => 'get',
      ),
    ),
  ),
  2130101525 => 
  array (
    'id' => 15519575,
    'name' => 'ООО "Премиум Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1496038464,
    'updated_at' => 1524904061,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30119545,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119545',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1524904061,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130101525',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30119545,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30119545',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15519575',
        'method' => 'get',
      ),
    ),
  ),
  9102225828 => 
  array (
    'id' => 18311903,
    'name' => 'ООО ЛК РНКБ',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524904869,
    'updated_at' => 1524904870,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33985807,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985807',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102225828',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33985807,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33985807',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18311903',
        'method' => 'get',
      ),
    ),
  ),
  2801131200 => 
  array (
    'id' => 18312749,
    'name' => 'ООО "Авангард-Импорт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524908501,
    'updated_at' => 1524908586,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33986793,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33986793',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2801131200',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33986793,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33986793',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18312749',
        'method' => 'get',
      ),
    ),
  ),
  2130163930 => 
  array (
    'id' => 18313193,
    'name' => 'ООО "СТРОЙТЕХ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524909526,
    'updated_at' => 1524909540,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33987513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2130163930',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33987513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18313193',
        'method' => 'get',
      ),
    ),
  ),
  2304050060 => 
  array (
    'id' => 18313311,
    'name' => 'ооо "ЮККА"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1524909882,
    'updated_at' => 1524909883,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33987643,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987643',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2304050060',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33987643,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987643',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18313311',
        'method' => 'get',
      ),
    ),
  ),
  1326194226 => 
  array (
    'id' => 18313491,
    'name' => '"МП городского округа Саранск ""Спецавтохозяйство Саранское"""',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524911237,
    'updated_at' => 1524911238,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33987799,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987799',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1326194226',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33987799,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987799',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18313491',
        'method' => 'get',
      ),
    ),
  ),
  '8-8342-222377' => 
  array (
    'id' => 18313695,
    'name' => 'ООО "Дорожник"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524912495,
    'updated_at' => 1524912495,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33987983,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987983',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8-8342-222377',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33987983,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33987983',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18313695',
        'method' => 'get',
      ),
    ),
  ),
  5834054683 => 
  array (
    'id' => 18314127,
    'name' => 'ООО "ЦЕНТРГРАДПРОЕКТ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524915617,
    'updated_at' => 1524915683,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33989135,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33989135',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5834054683',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33989135,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33989135',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18314127',
        'method' => 'get',
      ),
    ),
  ),
  5032219108 => 
  array (
    'id' => 18314145,
    'name' => 'ООО "ТЕХНО-АРХИТЭК"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524915779,
    'updated_at' => 1524915780,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33989155,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33989155',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032219108',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33989155,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33989155',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18314145',
        'method' => 'get',
      ),
    ),
  ),
  6321432275 => 
  array (
    'id' => 17986755,
    'name' => 'ООО «КРОВЛЯ163»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1521691947,
    'updated_at' => 1525318729,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33557379,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33557379',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6321432275',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33557379,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33557379',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17986755',
        'method' => 'get',
      ),
    ),
  ),
  1701053073 => 
  array (
    'id' => 15727787,
    'name' => 'ООО "ВИКСАР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1498020799,
    'updated_at' => 1525318830,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30407281,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407281',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921361,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1701053073',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Тыва(GMT +7)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30407281,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30407281',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15727787',
        'method' => 'get',
      ),
    ),
  ),
  2222805056 => 
  array (
    'id' => 15372063,
    'name' => 'ООО "СтройПрестиж"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1494482207,
    'updated_at' => 1525319546,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29914287,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29914287',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2222805056',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29914287,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29914287',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15372063',
        'method' => 'get',
      ),
    ),
  ),
  2225188980 => 
  array (
    'id' => 18331877,
    'name' => 'ООО "МОНОЛИТ-СТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1525321177,
    'updated_at' => 1525321210,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004435,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004435',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525327200,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2225188980',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'алтай 6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004435,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004435',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18331877',
        'method' => 'get',
      ),
    ),
  ),
  5403033255 => 
  array (
    'id' => 18331837,
    'name' => 'ООО "Сибполимеравтодор"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525320636,
    'updated_at' => 1525320636,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004393,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004393',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5403033255',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004393,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004393',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18331837',
        'method' => 'get',
      ),
    ),
  ),
  3403027843 => 
  array (
    'id' => 18331883,
    'name' => 'ООО "СК "ВолгоградСтройМонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525321439,
    'updated_at' => 1525321511,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004443,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004443',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3403027843',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004443,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004443',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18331883',
        'method' => 'get',
      ),
    ),
  ),
  1435271713 => 
  array (
    'id' => 18331889,
    'name' => 'ООО "ДОРОГИ СЕВЕРА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525321623,
    'updated_at' => 1525321623,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004451,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004451',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435271713',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004451,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004451',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18331889',
        'method' => 'get',
      ),
    ),
  ),
  274146925 => 
  array (
    'id' => 18200899,
    'name' => 'ООО "Гринландия"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523940914,
    'updated_at' => 1525322545,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33859233,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859233',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274146925',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33859233,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33859233',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18200899',
        'method' => 'get',
      ),
    ),
  ),
  6317084995 => 
  array (
    'id' => 18331951,
    'name' => 'ООО Гидропомпа',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525322925,
    'updated_at' => 1525322926,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004533,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004533',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317084995',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004533,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004533',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18331951',
        'method' => 'get',
      ),
    ),
  ),
  323370256 => 
  array (
    'id' => 17430369,
    'name' => 'ООО "Спринт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1515989879,
    'updated_at' => 1525323683,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32751513,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32751513',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1515997520,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323370256',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32751513,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32751513',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17430369',
        'method' => 'get',
      ),
    ),
  ),
  5933272160 => 
  array (
    'id' => 18332031,
    'name' => 'ООО Стройэффект',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525323886,
    'updated_at' => 1525323926,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5933272160',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332031',
        'method' => 'get',
      ),
    ),
  ),
  274112549 => 
  array (
    'id' => 18332047,
    'name' => 'ООО "Здоровье"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525324105,
    'updated_at' => 1525324144,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004625,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004625',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274112549',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004625,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004625',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332047',
        'method' => 'get',
      ),
    ),
  ),
  2463252344 => 
  array (
    'id' => 18332061,
    'name' => 'ООО "КРАЙСТРОЙПРОЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525324214,
    'updated_at' => 1525324214,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004637,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004637',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2463252344',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004637,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004637',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332061',
        'method' => 'get',
      ),
    ),
  ),
  2543120174 => 
  array (
    'id' => 18332063,
    'name' => 'ооо «ИНФОРА»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1525324230,
    'updated_at' => 1525324230,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004639,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004639',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2543120174',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '10 приморье',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004639,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004639',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332063',
        'method' => 'get',
      ),
    ),
  ),
  5611077013 => 
  array (
    'id' => 18332079,
    'name' => 'ООО «СТК Александр»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525324388,
    'updated_at' => 1525324388,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004657,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004657',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5611077013',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004657,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004657',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332079',
        'method' => 'get',
      ),
    ),
  ),
  6678011836 => 
  array (
    'id' => 16211007,
    'name' => 'ООО "СЕВЕР-ИНВЕСТ"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1503480129,
    'updated_at' => 1525324605,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31286093,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31286093',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1503480417,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678011836',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31286093,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31286093',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16211007',
        'method' => 'get',
      ),
    ),
  ),
  5407224660 => 
  array (
    'id' => 18332155,
    'name' => 'ООО "СТРОЙПРОМКОНТИНЕНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525325448,
    'updated_at' => 1525325449,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004763,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004763',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407224660',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004763,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004763',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332155',
        'method' => 'get',
      ),
    ),
  ),
  6606003032 => 
  array (
    'id' => 18331935,
    'name' => 'ООО "УралавтоГАЗсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525322594,
    'updated_at' => 1525325941,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004505,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004505',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6606003032',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004505,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004505',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18331935',
        'method' => 'get',
      ),
    ),
  ),
  2710014611 => 
  array (
    'id' => 18332321,
    'name' => 'ооо монолит',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1525326433,
    'updated_at' => 1525326439,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004897,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004897',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2710014611',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '10 хабаровск',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004897,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004897',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332321',
        'method' => 'get',
      ),
    ),
  ),
  274142399 => 
  array (
    'id' => 18332361,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ КОМПАНИЯ ПРОМЖИЛСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525326937,
    'updated_at' => 1525326937,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34004949,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004949',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274142399',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34004949,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34004949',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332361',
        'method' => 'get',
      ),
    ),
  ),
  3123356570 => 
  array (
    'id' => 15099899,
    'name' => 'Стройинвест, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491460064,
    'updated_at' => 1525327453,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29536125,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29536125',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123356570',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Белгородская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29536125,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29536125',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15099899',
        'method' => 'get',
      ),
    ),
  ),
  3435117722 => 
  array (
    'id' => 18332539,
    'name' => 'ООО Волжская нефтебаза',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525328309,
    'updated_at' => 1525328421,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34005187,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005187',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3435117722',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34005187,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005187',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332539',
        'method' => 'get',
      ),
    ),
  ),
  7020028399 => 
  array (
    'id' => 14771306,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ ЛАБОРАТОРИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1488172799,
    'updated_at' => 1525329036,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29105748,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29105748',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7020028399',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29105748,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29105748',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14771306',
        'method' => 'get',
      ),
    ),
  ),
  5036126126 => 
  array (
    'id' => 18332661,
    'name' => 'ООО "ХОЗИНТОРГ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525329078,
    'updated_at' => 1525329079,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34005307,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005307',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5036126126',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34005307,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005307',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332661',
        'method' => 'get',
      ),
    ),
  ),
  6318002949 => 
  array (
    'id' => 18310261,
    'name' => '"Строительная компания Ресурс"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1524894962,
    'updated_at' => 1525329095,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984283,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984283',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318002949',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984283,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984283',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310261',
        'method' => 'get',
      ),
    ),
  ),
  7715929393 => 
  array (
    'id' => 18283987,
    'name' => 'НСК "Стройгарант", ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1524657370,
    'updated_at' => 1525329166,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33959357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7715929393',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33959357,
        1 => 33959369,
        2 => 33959377,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33959357,33959369,33959377',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18283987',
        'method' => 'get',
      ),
    ),
  ),
  3123399278 => 
  array (
    'id' => 18332799,
    'name' => 'ООО "АРТЕКС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525329784,
    'updated_at' => 1525329784,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34005455,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005455',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123399278',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34005455,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005455',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18332799',
        'method' => 'get',
      ),
    ),
  ),
  7807185320 => 
  array (
    'id' => 17997849,
    'name' => 'ООО "РУСАЛ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1521793986,
    'updated_at' => 1525331329,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33589777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33589777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7807185320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33589777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33589777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17997849',
        'method' => 'get',
      ),
    ),
  ),
  6678013343 => 
  array (
    'id' => 18333117,
    'name' => 'ООО "Комстар"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525331635,
    'updated_at' => 1525331656,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34005777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678013343',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34005777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34005777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18333117',
        'method' => 'get',
      ),
    ),
  ),
  7716878871 => 
  array (
    'id' => 18333391,
    'name' => 'ООО "Техно Лэнд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525332922,
    'updated_at' => 1525333604,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34006053,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006053',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7716878871',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34006053,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006053',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18333391',
        'method' => 'get',
      ),
    ),
  ),
  6673249850 => 
  array (
    'id' => 18333597,
    'name' => 'ООО "БерезовскДорСтрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525333793,
    'updated_at' => 1525333836,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34006253,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006253',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6673249850',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34006253,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006253',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18333597',
        'method' => 'get',
      ),
    ),
  ),
  7743165076 => 
  array (
    'id' => 18333673,
    'name' => 'ООО «ГП РАД»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525334142,
    'updated_at' => 1525334143,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34006331,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006331',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743165076',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34006331,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006331',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18333673',
        'method' => 'get',
      ),
    ),
  ),
  4826030207 => 
  array (
    'id' => 13814164,
    'name' => 'ООО "Липецксантехмонтаж-1"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1479196427,
    'updated_at' => 1525334151,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27677336,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27677336',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500606199,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826030207',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27677336,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27677336',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13814164',
        'method' => 'get',
      ),
    ),
  ),
  5006008848 => 
  array (
    'id' => 18333747,
    'name' => 'ЗАО "МОСИНТЕРМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525334360,
    'updated_at' => 1525334911,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34006411,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006411',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5006008848',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34006411,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006411',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18333747',
        'method' => 'get',
      ),
    ),
  ),
  572016311 => 
  array (
    'id' => 18334149,
    'name' => 'ООО "МИДИЯ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525335697,
    'updated_at' => 1525335697,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34006781,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006781',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '572016311',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34006781,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006781',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18334149',
        'method' => 'get',
      ),
    ),
  ),
  3811164132 => 
  array (
    'id' => 18334141,
    'name' => 'ООО  "РЕГИОНПРОЕКТ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525335676,
    'updated_at' => 1525336408,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34006777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525726740,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3811164132',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34006777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18334141',
        'method' => 'get',
      ),
    ),
  ),
  7724750466 => 
  array (
    'id' => 18334439,
    'name' => 'ООО "ТРАК-ТИР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525336934,
    'updated_at' => 1525336934,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34007077,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34007077',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724750466',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34007077,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34007077',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18334439',
        'method' => 'get',
      ),
    ),
  ),
  4029054380 => 
  array (
    'id' => 18334483,
    'name' => 'ООО "Деталия"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525337132,
    'updated_at' => 1525337132,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34007127,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34007127',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4029054380',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34007127,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34007127',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18334483',
        'method' => 'get',
      ),
    ),
  ),
  7813551374 => 
  array (
    'id' => 18334821,
    'name' => 'ООО "СТС-ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525338123,
    'updated_at' => 1525338511,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34007353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34007353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7813551374',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34007353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34007353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18334821',
        'method' => 'get',
      ),
    ),
  ),
  7805118939 => 
  array (
    'id' => 15541139,
    'name' => 'САНКТ-ПЕТЕРБУРГСКИЙ ИНСТИТУТ ТЕПЛОЭНЕРГЕТИКИ',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1496217316,
    'updated_at' => 1525338972,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30147489,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147489',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7805118939',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30147489,
        1 => 34007201,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30147489,34007201',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15541139',
        'method' => 'get',
      ),
    ),
  ),
  4703133386 => 
  array (
    'id' => 18095671,
    'name' => '"Конвера Лайн"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1522827257,
    'updated_at' => 1525339840,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33717537,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33717537',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 33717535,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=33717535',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4703133386',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33717537,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33717537',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18095671',
        'method' => 'get',
      ),
    ),
  ),
  7735571356 => 
  array (
    'id' => 18335873,
    'name' => 'ООО "Логика бизнеса"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525341462,
    'updated_at' => 1525341463,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34008341,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34008341',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7735571356',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34008341,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34008341',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18335873',
        'method' => 'get',
      ),
    ),
  ),
  7817321999 => 
  array (
    'id' => 18097081,
    'name' => 'ООО «А-ЭНЕРГОТЕХ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522832618,
    'updated_at' => 1525341601,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33719203,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33719203',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7817321999',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33719203,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33719203',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18097081',
        'method' => 'get',
      ),
    ),
  ),
  7721107948 => 
  array (
    'id' => 18239753,
    'name' => 'АНО "Аэроклуб "АИСТ-М"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524221885,
    'updated_at' => 1525341619,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33916759,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916759',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525467540,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7721107948',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33916759,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33916759',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18239753',
        'method' => 'get',
      ),
    ),
  ),
  1639000574 => 
  array (
    'id' => 15317927,
    'name' => 'ООО «Авторемстрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1493792365,
    'updated_at' => 1525342080,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29851461,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29851461',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605836,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1639000574',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29851461,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29851461',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15317927',
        'method' => 'get',
      ),
    ),
  ),
  5259114269 => 
  array (
    'id' => 18336461,
    'name' => 'ООО "КАЛЬДЕРА"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525343052,
    'updated_at' => 1525343053,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34008685,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34008685',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5259114269',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34008685,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34008685',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18336461',
        'method' => 'get',
      ),
    ),
  ),
  7839396860 => 
  array (
    'id' => 18337159,
    'name' => 'ООО «Абрис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525345011,
    'updated_at' => 1525345119,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34009085,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009085',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7839396860',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34009085,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009085',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18337159',
        'method' => 'get',
      ),
    ),
  ),
  4706019184 => 
  array (
    'id' => 18337243,
    'name' => 'ООО "Ритм плюс К"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525345401,
    'updated_at' => 1525345402,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34009181,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009181',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4706019184',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34009181,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009181',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18337243',
        'method' => 'get',
      ),
    ),
  ),
  5040139960 => 
  array (
    'id' => 14344222,
    'name' => 'ООО "ДОРОЖНЫЙ ЭКСПЕРТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1484130998,
    'updated_at' => 1525346504,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28479576,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28479576',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500606109,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5040139960',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28479576,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28479576',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14344222',
        'method' => 'get',
      ),
    ),
  ),
  7709922618 => 
  array (
    'id' => 18337489,
    'name' => 'ООО "КЛЯЙНЕВЕЛЬТ АРХИТЕКТЕН"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525346660,
    'updated_at' => 1525346753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34009433,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009433',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709922618',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34009433,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009433',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18337489',
        'method' => 'get',
      ),
    ),
  ),
  8602142790 => 
  array (
    'id' => 18337515,
    'name' => 'ООО "Авиакомпания "СКОЛ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525346821,
    'updated_at' => 1525346937,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34009461,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009461',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525410000,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602142790',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34009461,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009461',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18337515',
        'method' => 'get',
      ),
    ),
  ),
  2311069785 => 
  array (
    'id' => 18338965,
    'name' => 'ООО "Септа-Юг"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525350999,
    'updated_at' => 1525350999,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34010369,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34010369',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311069785',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34010369,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34010369',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18338965',
        'method' => 'get',
      ),
    ),
  ),
  4825025620 => 
  array (
    'id' => 18337141,
    'name' => 'ООО "ДСУ N7"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525344867,
    'updated_at' => 1525352378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34009061,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009061',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4825025620',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34009061,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34009061',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18337141',
        'method' => 'get',
      ),
    ),
  ),
  1513063248 => 
  array (
    'id' => 18339583,
    'name' => 'ООО Строительно-Ремонтная Компания "Мегапод"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525354521,
    'updated_at' => 1525354521,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34029949,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34029949',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1513063248',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34029949,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34029949',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18339583',
        'method' => 'get',
      ),
    ),
  ),
  5406984957 => 
  array (
    'id' => 18342333,
    'name' => 'ООО СК "АВЭГА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525406957,
    'updated_at' => 1525406958,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038543,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038543',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5406984957',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038543,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038543',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342333',
        'method' => 'get',
      ),
    ),
  ),
  4202051862 => 
  array (
    'id' => 18342339,
    'name' => 'ООО "МАСТЕР-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525407114,
    'updated_at' => 1525407115,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038547,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038547',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4202051862',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038547,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038547',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342339',
        'method' => 'get',
      ),
    ),
  ),
  2224188948 => 
  array (
    'id' => 18342367,
    'name' => 'ООО "СК СЛАВА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525407566,
    'updated_at' => 1525407692,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038563,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038563',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2224188948',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038563,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038563',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342367',
        'method' => 'get',
      ),
    ),
  ),
  2723192989 => 
  array (
    'id' => 18342381,
    'name' => 'ООО "ТЭК-ДВ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525407858,
    'updated_at' => 1525407880,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038581,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038581',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723192989',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038581,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038581',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342381',
        'method' => 'get',
      ),
    ),
  ),
  '0253007335' => 
  array (
    'id' => 8354348,
    'name' => 'Танып, ООО',
    'responsible_user_id' => 1261404,
    'created_by' => 553662,
    'created_at' => 1447740806,
    'updated_at' => 1525407928,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 20665072,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=20665072',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 20665070,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=20665070',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500521315,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0253007335',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 20665072,
        1 => 34038585,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=20665072,34038585',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=8354348',
        'method' => 'get',
      ),
    ),
  ),
  2204058387 => 
  array (
    'id' => 18342387,
    'name' => 'ООО "БВРП "НОВОТРАНС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525407929,
    'updated_at' => 1525408001,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038587,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038587',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2204058387',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038587,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038587',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342387',
        'method' => 'get',
      ),
    ),
  ),
  6501146467 => 
  array (
    'id' => 18333735,
    'name' => 'ДАЛЬМОРНЕФТЕГЕОФИЗИКА',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1525334316,
    'updated_at' => 1525408131,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34006401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6501146467',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '10',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34006401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34006401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18333735',
        'method' => 'get',
      ),
    ),
  ),
  2511104726 => 
  array (
    'id' => 18342401,
    'name' => 'ООО "КОМБАТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525408046,
    'updated_at' => 1525408207,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038603,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038603',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2511104726',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038603,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038603',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342401',
        'method' => 'get',
      ),
    ),
  ),
  274926410 => 
  array (
    'id' => 18187331,
    'name' => 'ООО "РЕГИОНАЛЬНОЕ УПРАВЛЕНИЕ СТРОИТЕЛЬСТВА ТЕХНОЛОГИЧЕСКИХ АВТОМОБИЛЬНЫХ МАГИСТРАЛЕЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1523856366,
    'updated_at' => 1525408445,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33839367,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33839367',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '274926410',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33839367,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33839367',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18187331',
        'method' => 'get',
      ),
    ),
  ),
  5257009364 => 
  array (
    'id' => 17969775,
    'name' => 'ООО Магистраль',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1521532633,
    'updated_at' => 1525408874,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33526933,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33526933',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5257009364',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'нижегородск обл',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33526933,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33526933',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17969775',
        'method' => 'get',
      ),
    ),
  ),
  5405974032 => 
  array (
    'id' => 15494779,
    'name' => 'ООО "СТС-ПЛЮС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1495688265,
    'updated_at' => 1525409372,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30078229,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078229',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921445,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5405974032',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Новосибирская область(GMT +6)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30078229,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30078229',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15494779',
        'method' => 'get',
      ),
    ),
  ),
  5905291154 => 
  array (
    'id' => 14860459,
    'name' => 'ООО "ДСК "Магистраль"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1489130085,
    'updated_at' => 1525409822,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29215995,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29215995',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5905291154',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29215995,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29215995',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14860459',
        'method' => 'get',
      ),
    ),
  ),
  8903020789 => 
  array (
    'id' => 18342525,
    'name' => 'ООО «САТЕК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525409653,
    'updated_at' => 1525409891,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038689,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038689',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8903020789',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038689,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038689',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342525',
        'method' => 'get',
      ),
    ),
  ),
  7723762067 => 
  array (
    'id' => 18342771,
    'name' => 'ООО "Тумед"-Волоть',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525412459,
    'updated_at' => 1525412459,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34038969,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038969',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7723762067',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34038969,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34038969',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18342771',
        'method' => 'get',
      ),
    ),
  ),
  8901017678 => 
  array (
    'id' => 14215257,
    'name' => 'ООО "ТрансСервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1482293590,
    'updated_at' => 1525414255,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28328823,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28328823',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8901017678',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28328823,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28328823',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14215257',
        'method' => 'get',
      ),
    ),
  ),
  7720735121 => 
  array (
    'id' => 15103547,
    'name' => 'НМ Центр, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491476054,
    'updated_at' => 1525417132,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29540901,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29540901',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7720735121',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'москва',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29540901,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29540901',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15103547',
        'method' => 'get',
      ),
    ),
  ),
  8622024795 => 
  array (
    'id' => 18383325,
    'name' => 'ООО "ПРАЙД"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525931407,
    'updated_at' => 1525931408,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34080277,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080277',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8622024795',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34080277,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080277',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18383325',
        'method' => 'get',
      ),
    ),
  ),
  7811492451 => 
  array (
    'id' => 18343985,
    'name' => 'ООО «ПСК»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525420089,
    'updated_at' => 1525420089,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34040245,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34040245',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7811492451',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34040245,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34040245',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18343985',
        'method' => 'get',
      ),
    ),
  ),
  3327116094 => 
  array (
    'id' => 18344073,
    'name' => 'ООО «Абсолют Проект»',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525420483,
    'updated_at' => 1525420533,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34040337,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34040337',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3327116094',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34040337,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34040337',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18344073',
        'method' => 'get',
      ),
    ),
  ),
  7449045514 => 
  array (
    'id' => 18301547,
    'name' => 'ООО ЧАСТНАЯ ОХРАННАЯ ОРГАНИЗАЦИЯ "НАВИГАТОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524803002,
    'updated_at' => 1525422863,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33975893,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975893',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1525422858,
    'closest_task_at' => 1525467540,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7449045514',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33975893,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33975893',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18301547',
        'method' => 'get',
      ),
    ),
  ),
  2315086118 => 
  array (
    'id' => 18346391,
    'name' => '"Арт ВИНД"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525430765,
    'updated_at' => 1525430765,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34042607,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34042607',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2315086118',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34042607,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34042607',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18346391',
        'method' => 'get',
      ),
    ),
  ),
  32200116206 => 
  array (
    'id' => 18346699,
    'name' => 'ИП Фомич Андрей Вячеславович',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525432252,
    'updated_at' => 1525432253,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34042893,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34042893',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '32200116206',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34042893,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34042893',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18346699',
        'method' => 'get',
      ),
    ),
  ),
  7448129458 => 
  array (
    'id' => 18347009,
    'name' => 'ООО "ЭнергоМеталлургМонтаж"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525433477,
    'updated_at' => 1525433477,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34043157,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34043157',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448129458',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34043157,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34043157',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18347009',
        'method' => 'get',
      ),
    ),
  ),
  6679096504 => 
  array (
    'id' => 18360019,
    'name' => 'ООО "УРАЛДОРСТРОЙ"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1525665945,
    'updated_at' => 1525665945,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34056455,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056455',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6679096504',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'екат 5',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34056455,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056455',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18360019',
        'method' => 'get',
      ),
    ),
  ),
  6317093439 => 
  array (
    'id' => 18360099,
    'name' => 'ООО Туристическая компания "РосВояж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525667237,
    'updated_at' => 1525667237,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34056531,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056531',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6317093439',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34056531,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056531',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18360099',
        'method' => 'get',
      ),
    ),
  ),
  1215209614 => 
  array (
    'id' => 18289945,
    'name' => 'ООО "ВДПО-ВОЛГА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524726004,
    'updated_at' => 1525667753,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33965237,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965237',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1525553940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1215209614',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'марий эл',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33965237,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33965237',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18289945',
        'method' => 'get',
      ),
    ),
  ),
  6372019034 => 
  array (
    'id' => 18360137,
    'name' => 'ООО "Домашняя ферма"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525667804,
    'updated_at' => 1525667970,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34056567,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056567',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6372019034',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34056567,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056567',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18360137',
        'method' => 'get',
      ),
    ),
  ),
  3810023097 => 
  array (
    'id' => 18360223,
    'name' => 'ООО "АРДОС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525668958,
    'updated_at' => 1525669070,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34056671,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056671',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3810023097',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34056671,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056671',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18360223',
        'method' => 'get',
      ),
    ),
  ),
  6362007424 => 
  array (
    'id' => 18360311,
    'name' => 'ООО творческо-производственная фирма "Среда-2"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525670324,
    'updated_at' => 1525670874,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34056799,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056799',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6362007424',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34056799,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056799',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18360311',
        'method' => 'get',
      ),
    ),
  ),
  6454106552 => 
  array (
    'id' => 15497501,
    'name' => 'ООО "РЕГИОН МЕД"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1495702784,
    'updated_at' => 1525671245,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30081523,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30081523',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921446,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6454106552',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Самарская область(GMT +4)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30081523,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30081523',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15497501',
        'method' => 'get',
      ),
    ),
  ),
  6364003658 => 
  array (
    'id' => 18360575,
    'name' => 'ООО Берег',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525672631,
    'updated_at' => 1525672692,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34057053,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34057053',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6364003658',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34057053,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34057053',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18360575',
        'method' => 'get',
      ),
    ),
  ),
  7460023296 => 
  array (
    'id' => 18278667,
    'name' => 'ООО "РЕАЛДОРСТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524633359,
    'updated_at' => 1525672984,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33953609,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953609',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7460023296',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33953609,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33953609',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18278667',
        'method' => 'get',
      ),
    ),
  ),
  1435221783 => 
  array (
    'id' => 18359923,
    'name' => 'ооо "Бастион"',
    'responsible_user_id' => 1261404,
    'created_by' => 1261404,
    'created_at' => 1525663783,
    'updated_at' => 1525673158,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34056357,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056357',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1435221783',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7 саха якутия',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34056357,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34056357',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18359923',
        'method' => 'get',
      ),
    ),
  ),
  7732108630 => 
  array (
    'id' => 17408477,
    'name' => 'ООО «Летно-исследовательский аэрогеофизический центр»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1515659710,
    'updated_at' => 1525674341,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32714833,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32714833',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1515665281,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7732108630',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32714833,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32714833',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17408477',
        'method' => 'get',
      ),
    ),
  ),
  1658184373 => 
  array (
    'id' => 13969772,
    'name' => 'ООО "Алсу"',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1479880030,
    'updated_at' => 1525677847,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27884706,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27884706',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500381861,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1658184373',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27884706,
        1 => 27922476,
        2 => 29819695,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27884706,27922476,29819695',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13969772',
        'method' => 'get',
      ),
    ),
  ),
  1657228290 => 
  array (
    'id' => 14235554,
    'name' => 'ООО «ГРОССМАСТЕР»',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1482492446,
    'updated_at' => 1525677854,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28352354,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28352354',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500381800,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657228290',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28352354,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28352354',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14235554',
        'method' => 'get',
      ),
    ),
  ),
  1660165498 => 
  array (
    'id' => 15028757,
    'name' => 'ООО «СТК «Акварэй»',
    'responsible_user_id' => 1261404,
    'created_by' => 1261428,
    'created_at' => 1490769399,
    'updated_at' => 1525678047,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29449017,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29449017',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1660165498',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29449017,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29449017',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15028757',
        'method' => 'get',
      ),
    ),
  ),
  7842044932 => 
  array (
    'id' => 18361605,
    'name' => 'ООО «МОДУЛЬ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525678494,
    'updated_at' => 1525678495,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34058083,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34058083',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7842044932',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34058083,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34058083',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18361605',
        'method' => 'get',
      ),
    ),
  ),
  3906248461 => 
  array (
    'id' => 15781253,
    'name' => 'ООО "Е.С.-СТРОЙ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1498637987,
    'updated_at' => 1525678689,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30490139,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30490139',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921354,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906248461',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Калининградская область(GMT +2)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30490139,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30490139',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15781253',
        'method' => 'get',
      ),
    ),
  ),
  7814650297 => 
  array (
    'id' => 18361827,
    'name' => 'ООО Лумина',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525679296,
    'updated_at' => 1525679297,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34058305,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34058305',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814650297',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34058305,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34058305',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18361827',
        'method' => 'get',
      ),
    ),
  ),
  6732060721 => 
  array (
    'id' => 15333637,
    'name' => 'ООО  "ДЭП-4"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1493901880,
    'updated_at' => 1525679530,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29871693,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29871693',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521498,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6732060721',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29871693,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29871693',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15333637',
        'method' => 'get',
      ),
    ),
  ),
  4821047165 => 
  array (
    'id' => 15152451,
    'name' => 'Технологии Строительства, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1491977064,
    'updated_at' => 1525679838,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29592603,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29592603',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4821047165',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29592603,
        1 => 29592643,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29592603,29592643',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15152451',
        'method' => 'get',
      ),
    ),
  ),
  6362010924 => 
  array (
    'id' => 16314767,
    'name' => 'Волга, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1504683306,
    'updated_at' => 1525680320,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31413741,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31413741',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 31413739,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=31413739',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 0,
    'closest_task_at' => 1526889600,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6362010924',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31413741,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31413741',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16314767',
        'method' => 'get',
      ),
    ),
  ),
  1657124526 => 
  array (
    'id' => 17250337,
    'name' => 'ООО «ДСК «ВЕЛЕС»',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1513667193,
    'updated_at' => 1525680535,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32544011,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32544011',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1514470173,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1657124526',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32544011,
        1 => 33207957,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32544011,33207957',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17250337',
        'method' => 'get',
      ),
    ),
  ),
  1616027474 => 
  array (
    'id' => 17189171,
    'name' => 'ООО МОЛОЧНЫЙ КОМБИНАТ «КАСЫМОВСКИЙ»',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1513057537,
    'updated_at' => 1525681076,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32475407,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32475407',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1514165982,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1616027474',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32475407,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32475407',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17189171',
        'method' => 'get',
      ),
    ),
  ),
  5257153657 => 
  array (
    'id' => 18362485,
    'name' => 'ООО "СТАРКАВТО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525681916,
    'updated_at' => 1525681916,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34059937,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34059937',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5257153657',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34059937,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34059937',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18362485',
        'method' => 'get',
      ),
    ),
  ),
  7718999800 => 
  array (
    'id' => 18362709,
    'name' => 'ООО "АРХИТЕКТУРНО-РЕСТАВРАЦИОННОЕ ТОВАРИЩЕСТВО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525682073,
    'updated_at' => 1525682074,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34059981,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34059981',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7718999800',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34059981,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34059981',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18362709',
        'method' => 'get',
      ),
    ),
  ),
  5407229386 => 
  array (
    'id' => 15189789,
    'name' => 'Фаворит, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1492399500,
    'updated_at' => 1525683481,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29651619,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29651619',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5407229386',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+6',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29651619,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29651619',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15189789',
        'method' => 'get',
      ),
    ),
  ),
  323395236 => 
  array (
    'id' => 15412553,
    'name' => 'ООО "ОСОБНЯК СТРОЙ"',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1494906153,
    'updated_at' => 1525684401,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29961485,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500004126,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323395236',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'бурятия',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29961485,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29961485',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15412553',
        'method' => 'get',
      ),
    ),
  ),
  4824017144 => 
  array (
    'id' => 12033008,
    'name' => 'ООО «НовоСтройка»',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1463398138,
    'updated_at' => 1525685849,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 25271948,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271948',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500381993,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4824017144',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Липецкая область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 25271948,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271948',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12033008',
        'method' => 'get',
      ),
    ),
  ),
  2904025980 => 
  array (
    'id' => 18310819,
    'name' => 'ООО "ПРОЕКТНО-МОНТАЖНАЯ КОМПАНИЯ - ЭНЕРГО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524899182,
    'updated_at' => 1525686538,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33984823,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984823',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2904025980',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'архангельск',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33984823,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33984823',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18310819',
        'method' => 'get',
      ),
    ),
  ),
  6138013589 => 
  array (
    'id' => 17918779,
    'name' => 'ООО "СВАОР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1520938711,
    'updated_at' => 1525688601,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33461811,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33461811',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1522395828,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6138013589',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'ростовская область',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33461811,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33461811',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17918779',
        'method' => 'get',
      ),
    ),
  ),
  5609171851 => 
  array (
    'id' => 14268220,
    'name' => 'ООО  "ОПЭНКАРД"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1482907690,
    'updated_at' => 1525691096,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28385748,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28385748',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521738,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5609171851',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28385748,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28385748',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14268220',
        'method' => 'get',
      ),
    ),
  ),
  572019129 => 
  array (
    'id' => 18365173,
    'name' => 'ООО "Дорожно-строительная компания № 2"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525692371,
    'updated_at' => 1525692844,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34062589,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34062589',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '572019129',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34062589,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34062589',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18365173',
        'method' => 'get',
      ),
    ),
  ),
  7810181446 => 
  array (
    'id' => 18361843,
    'name' => 'ООО "ВОКРУГ СВЕТА"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525679378,
    'updated_at' => 1525693001,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34058321,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34058321',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7810181446',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34058321,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34058321',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18361843',
        'method' => 'get',
      ),
    ),
  ),
  1655133885 => 
  array (
    'id' => 18152919,
    'name' => 'Бункер-Трейд, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523448672,
    'updated_at' => 1525693018,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33795661,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795661',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 1526556600,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655133885',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33795661,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33795661',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18152919',
        'method' => 'get',
      ),
    ),
  ),
  7722762829 => 
  array (
    'id' => 16993331,
    'name' => 'ООО ПСК "Азурит"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511182338,
    'updated_at' => 1525693541,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32243559,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243559',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1525693541,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7722762829',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32243559,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32243559',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16993331',
        'method' => 'get',
      ),
    ),
  ),
  5501065343 => 
  array (
    'id' => 18365957,
    'name' => 'ООО "РУСГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525695584,
    'updated_at' => 1525695585,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34063395,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34063395',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5501065343',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34063395,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34063395',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18365957',
        'method' => 'get',
      ),
    ),
  ),
  6312039100 => 
  array (
    'id' => 18366141,
    'name' => 'ООО "Новый Город"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525696570,
    'updated_at' => 1525696571,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34063621,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34063621',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6312039100',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+1',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34063621,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34063621',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18366141',
        'method' => 'get',
      ),
    ),
  ),
  2901239670 => 
  array (
    'id' => 12032890,
    'name' => 'ООО Трест Столовых',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1463397748,
    'updated_at' => 1525696759,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 25271808,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271808',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 25271806,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=25271806',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1508238519,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901239670',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Архангельская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 25271808,
        1 => 29450057,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271808,29450057',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12032890',
        'method' => 'get',
      ),
    ),
  ),
  2901245426 => 
  array (
    'id' => 12032822,
    'name' => 'ООО Архангельский Трест Столовых',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1463397512,
    'updated_at' => 1525696833,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 25271730,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271730',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 25271728,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=25271728',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1500382028,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901245426',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Архангельская область(GMT +3)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 25271730,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271730',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=12032822',
        'method' => 'get',
      ),
    ),
  ),
  2901212269 => 
  array (
    'id' => 15956023,
    'name' => 'ООО Фишка',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1500614333,
    'updated_at' => 1525696852,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 25271808,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271808',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
      'id' => 25271806,
      'name' => NULL,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/companies?id=25271806',
          'method' => 'get',
        ),
      ),
    ),
    'closed_at' => 1512027190,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2901212269',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 25271808,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25271808',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15956023',
        'method' => 'get',
      ),
    ),
  ),
  5953002444 => 
  array (
    'id' => 13739120,
    'name' => 'ООО Стройтехресурс',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1478497232,
    'updated_at' => 1525699215,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 27581308,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27581308',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5953002444',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 27581308,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=27581308',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=13739120',
        'method' => 'get',
      ),
    ),
  ),
  7806043250 => 
  array (
    'id' => 18366641,
    'name' => 'ООО "СТРОИТЕЛЬНАЯ КОМПАНИЯ "ПОДЗЕМСТРОЙРЕКОНСТРУКЦИЯ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525699313,
    'updated_at' => 1525699314,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34064149,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34064149',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806043250',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34064149,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34064149',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18366641',
        'method' => 'get',
      ),
    ),
  ),
  170600684906 => 
  array (
    'id' => 15246999,
    'name' => 'Сырат Аян Сергеевич, ИП',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1493006437,
    'updated_at' => 1525699922,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29733483,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29733483',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500382025,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '170600684906',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29733483,
        1 => 29733493,
        2 => 29818123,
        3 => 30038699,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29733483,29733493,29818123,30038699',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15246999',
        'method' => 'get',
      ),
    ),
  ),
  5408287172 => 
  array (
    'id' => 14476662,
    'name' => 'ООО  "Гранит-Н"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1485327864,
    'updated_at' => 1525701508,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28702120,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702120',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5408287172',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'новосиб',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28702120,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28702120',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14476662',
        'method' => 'get',
      ),
    ),
  ),
  26822052661 => 
  array (
    'id' => 11935358,
    'name' => 'ИП Нерсесян Славик Бегларович',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1462446537,
    'updated_at' => 1525753587,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 25111382,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25111382',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500522517,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '26822052661',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 25111382,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=25111382',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=11935358',
        'method' => 'get',
      ),
    ),
  ),
  8602242509 => 
  array (
    'id' => 18369731,
    'name' => 'ООО "Сибстройтеплоремонт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525754828,
    'updated_at' => 1525755488,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067091,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067091',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602242509',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067091,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067091',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369731',
        'method' => 'get',
      ),
    ),
  ),
  1659179383 => 
  array (
    'id' => 15318101,
    'name' => 'ООО «Эльбрус»',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1493793485,
    'updated_at' => 1525755509,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29851697,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29851697',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1659179383',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29851697,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29851697',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15318101',
        'method' => 'get',
      ),
    ),
  ),
  6829103136 => 
  array (
    'id' => 18369791,
    'name' => 'ООО "ПроМед"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525755580,
    'updated_at' => 1525755737,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067139,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067139',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6829103136',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067139,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067139',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369791',
        'method' => 'get',
      ),
    ),
  ),
  2723190780 => 
  array (
    'id' => 18369815,
    'name' => 'ООО "ТЭК-КОМПЛЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525755933,
    'updated_at' => 1525755933,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067167,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067167',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723190780',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067167,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067167',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369815',
        'method' => 'get',
      ),
    ),
  ),
  6678030250 => 
  array (
    'id' => 15305965,
    'name' => 'ООО «ЭКСПО-ГРУП»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1493705561,
    'updated_at' => 1525756066,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29835649,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29835649',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1525756066,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6678030250',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29835649,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29835649',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15305965',
        'method' => 'get',
      ),
    ),
  ),
  8602019540 => 
  array (
    'id' => 18369861,
    'name' => 'ООО «Связьинжстрой»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525756521,
    'updated_at' => 1525756521,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067215,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067215',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8602019540',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067215,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067215',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369861',
        'method' => 'get',
      ),
    ),
  ),
  4101098550 => 
  array (
    'id' => 18369865,
    'name' => 'ООО "Старкам-АВТО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525756609,
    'updated_at' => 1525756609,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067221,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067221',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4101098550',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067221,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067221',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369865',
        'method' => 'get',
      ),
    ),
  ),
  8622025206 => 
  array (
    'id' => 18369885,
    'name' => 'ООО "СТРОЙКОМПЛЕКТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525756890,
    'updated_at' => 1525756891,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8622025206',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369885',
        'method' => 'get',
      ),
    ),
  ),
  3814034653 => 
  array (
    'id' => 18369901,
    'name' => 'ООО "МБА-ТЕПЛОСНАБ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525756995,
    'updated_at' => 1525757373,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067265,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067265',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3814034653',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067265,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067265',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369901',
        'method' => 'get',
      ),
    ),
  ),
  7448181779 => 
  array (
    'id' => 14485406,
    'name' => 'ООО "МедЛАЙФ"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1485407076,
    'updated_at' => 1525757484,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28711916,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711916',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7448181779',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28711916,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28711916',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14485406',
        'method' => 'get',
      ),
    ),
  ),
  6672261830 => 
  array (
    'id' => 18369935,
    'name' => 'ООО "ГератТехно"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525757540,
    'updated_at' => 1525757627,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067313,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067313',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6672261830',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067313,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067313',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369935',
        'method' => 'get',
      ),
    ),
  ),
  7017001936 => 
  array (
    'id' => 18369947,
    'name' => 'ОАО Акционерная компания "Томские мельницы"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525757670,
    'updated_at' => 1525757729,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067331,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067331',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017001936',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067331,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067331',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369947',
        'method' => 'get',
      ),
    ),
  ),
  8901027147 => 
  array (
    'id' => 18369965,
    'name' => 'ООО "ЯмалСтройПроект"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525757836,
    'updated_at' => 1525757836,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067351,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067351',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8901027147',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067351,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067351',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369965',
        'method' => 'get',
      ),
    ),
  ),
  1901107992 => 
  array (
    'id' => 16105875,
    'name' => 'ООО "Дорожно-строительное предприятие-8"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1502257311,
    'updated_at' => 1525758362,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31152573,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152573',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1901107992',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31152573,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31152573',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16105875',
        'method' => 'get',
      ),
    ),
  ),
  5404012970 => 
  array (
    'id' => 14780992,
    'name' => 'ООО "ИНТЕРСТРОЙ-Н"',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1488260276,
    'updated_at' => 1525758423,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29122810,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29122810',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5404012970',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '+7',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29122810,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29122810',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14780992',
        'method' => 'get',
      ),
    ),
  ),
  8609223368 => 
  array (
    'id' => 18370037,
    'name' => 'ООО "Эко-сервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525758487,
    'updated_at' => 1525758521,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067429,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067429',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '8609223368',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067429,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067429',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18370037',
        'method' => 'get',
      ),
    ),
  ),
  2464133325 => 
  array (
    'id' => 18369879,
    'name' => 'ООО «Красноярская строительная компания»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525756814,
    'updated_at' => 1525758791,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067241,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067241',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2464133325',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067241,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067241',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18369879',
        'method' => 'get',
      ),
    ),
  ),
  2263009310 => 
  array (
    'id' => 18370081,
    'name' => 'ООО "Союз-97"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525758947,
    'updated_at' => 1525759177,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067477,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067477',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2263009310',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067477,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067477',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18370081',
        'method' => 'get',
      ),
    ),
  ),
  2723184177 => 
  array (
    'id' => 18370113,
    'name' => 'ООО «ПРОМТЕХЭНЕРГО»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525759241,
    'updated_at' => 1525759378,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34067511,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067511',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2723184177',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34067511,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34067511',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18370113',
        'method' => 'get',
      ),
    ),
  ),
  4632010258 => 
  array (
    'id' => 18371433,
    'name' => 'ОБЛАСТНОЕ БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ "ПРОЕКТНЫЙ ИНСТИТУТ ГРАЖДАНСКОГО СТРОИТЕЛЬСТВА, ПЛАНИРОВКИ И ЗАСТРОЙКИ ГОРОДОВ И ПОСЕЛКОВ "КУРСКГРАЖДАНПРОЕКТ"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525759904,
    'updated_at' => 1525759904,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34068829,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068829',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4632010258',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34068829,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068829',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371433',
        'method' => 'get',
      ),
    ),
  ),
  6153021760 => 
  array (
    'id' => 18371447,
    'name' => 'ООО "Строймаркет"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525759986,
    'updated_at' => 1525760065,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34068851,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068851',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6153021760',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34068851,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068851',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371447',
        'method' => 'get',
      ),
    ),
  ),
  2310204174 => 
  array (
    'id' => 17903129,
    'name' => 'ООО "Лабмед"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1520834148,
    'updated_at' => 1525760132,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33432349,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33432349',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1520834478,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2310204174',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33432349,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33432349',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17903129',
        'method' => 'get',
      ),
    ),
  ),
  7814521608 => 
  array (
    'id' => 18371487,
    'name' => 'ООО «Альтернативная Медицинская Группа»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525760327,
    'updated_at' => 1525760328,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34068909,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068909',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7814521608',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34068909,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068909',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371487',
        'method' => 'get',
      ),
    ),
  ),
  3327112540 => 
  array (
    'id' => 18371523,
    'name' => 'ООО «Бизнес Технологии»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525760552,
    'updated_at' => 1525760553,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34068949,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068949',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3327112540',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34068949,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068949',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371523',
        'method' => 'get',
      ),
    ),
  ),
  7806497119 => 
  array (
    'id' => 18371569,
    'name' => 'ООО «БАЛТПРОМ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525760841,
    'updated_at' => 1525760841,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34068993,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068993',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7806497119',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34068993,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34068993',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371569',
        'method' => 'get',
      ),
    ),
  ),
  7743148881 => 
  array (
    'id' => 18371851,
    'name' => 'ООО «Мир труб»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525762540,
    'updated_at' => 1525762621,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34069307,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069307',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7743148881',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34069307,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069307',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371851',
        'method' => 'get',
      ),
    ),
  ),
  4826134608 => 
  array (
    'id' => 18371865,
    'name' => 'ООО "ИНФОТЕХ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525762674,
    'updated_at' => 1525762674,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34069321,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069321',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4826134608',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34069321,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069321',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371865',
        'method' => 'get',
      ),
    ),
  ),
  7447155511 => 
  array (
    'id' => 18371887,
    'name' => 'ООО «НЕО-ИНТЕРЬЕР»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525762792,
    'updated_at' => 1525762793,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34069339,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069339',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7447155511',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34069339,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069339',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371887',
        'method' => 'get',
      ),
    ),
  ),
  7714971339 => 
  array (
    'id' => 18371931,
    'name' => 'ООО "Системы Технической Безопасности"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525762930,
    'updated_at' => 1525762930,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34069381,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069381',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7714971339',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34069381,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069381',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18371931',
        'method' => 'get',
      ),
    ),
  ),
  5433965336 => 
  array (
    'id' => 17957767,
    'name' => 'ООО "Латрек"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1521436176,
    'updated_at' => 1525763287,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33514777,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33514777',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1521436233,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5433965336',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33514777,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33514777',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17957767',
        'method' => 'get',
      ),
    ),
  ),
  1655397373 => 
  array (
    'id' => 18372049,
    'name' => 'ООО Аква Бренд Ко',
    'responsible_user_id' => 643314,
    'created_by' => 643314,
    'created_at' => 1525763556,
    'updated_at' => 1525763561,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34069509,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069509',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1655397373',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34069509,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069509',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 13670973,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18372049',
        'method' => 'get',
      ),
    ),
  ),
  7709080489 => 
  array (
    'id' => 17056433,
    'name' => 'ЗАО «СЕТИ»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511764014,
    'updated_at' => 1525764932,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32331345,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331345',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1515563633,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7709080489',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32331345,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32331345',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17056433',
        'method' => 'get',
      ),
    ),
  ),
  7713446697 => 
  array (
    'id' => 18372399,
    'name' => 'ООО «Синтако»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525765475,
    'updated_at' => 1525765476,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34069887,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069887',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7713446697',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34069887,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069887',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18372399',
        'method' => 'get',
      ),
    ),
  ),
  5032287517 => 
  array (
    'id' => 18372475,
    'name' => 'ООО "Барвиха Спец Строй"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525765903,
    'updated_at' => 1525765904,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34069987,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069987',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5032287517',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34069987,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34069987',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18372475',
        'method' => 'get',
      ),
    ),
  ),
  4345442783 => 
  array (
    'id' => 18372575,
    'name' => 'ООО "Калликрат 43"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525766481,
    'updated_at' => 1525766576,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34070109,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070109',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4345442783',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34070109,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070109',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18372575',
        'method' => 'get',
      ),
    ),
  ),
  6165162572 => 
  array (
    'id' => 14344874,
    'name' => 'ООО «КОНСАЛТ ПЛЮС»',
    'responsible_user_id' => 1338850,
    'created_by' => 892789,
    'created_at' => 1484133937,
    'updated_at' => 1525766904,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28480418,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28480418',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6165162572',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28480418,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28480418',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14344874',
        'method' => 'get',
      ),
    ),
  ),
  5024172897 => 
  array (
    'id' => 18372683,
    'name' => 'ООО «ОК системы»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525767024,
    'updated_at' => 1525767025,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34070249,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070249',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5024172897',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34070249,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070249',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18372683',
        'method' => 'get',
      ),
    ),
  ),
  7726349690 => 
  array (
    'id' => 17455595,
    'name' => 'ООО "РУСМЕДФАРМ"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1516184181,
    'updated_at' => 1525767406,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32818211,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32818211',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1516184281,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7726349690',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32818211,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32818211',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17455595',
        'method' => 'get',
      ),
    ),
  ),
  7802208912 => 
  array (
    'id' => 18372795,
    'name' => 'ООО "Фертоинг"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525767499,
    'updated_at' => 1525767544,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34070401,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070401',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7802208912',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34070401,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070401',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18372795',
        'method' => 'get',
      ),
    ),
  ),
  2311207690 => 
  array (
    'id' => 18373165,
    'name' => 'ООО "СП ГРУПП"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525767933,
    'updated_at' => 1525767980,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34070837,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070837',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2311207690',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34070837,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34070837',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18373165',
        'method' => 'get',
      ),
    ),
  ),
  2912006518 => 
  array (
    'id' => 14168206,
    'name' => 'ООО "АДАМАНТ"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1481780771,
    'updated_at' => 1525768421,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 28252796,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28252796',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500606154,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2912006518',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 28252796,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=28252796',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14168206',
        'method' => 'get',
      ),
    ),
  ),
  4346052828 => 
  array (
    'id' => 18374143,
    'name' => 'ООО "Заряд"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525769493,
    'updated_at' => 1525769494,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34071605,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34071605',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4346052828',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34071605,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34071605',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18374143',
        'method' => 'get',
      ),
    ),
  ),
  1203003320 => 
  array (
    'id' => 16263729,
    'name' => 'ООО "Мариэнергогидромеханизация"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1504091328,
    'updated_at' => 1525770033,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 31353999,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31353999',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921238,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '1203003320',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 31353999,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=31353999',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16263729',
        'method' => 'get',
      ),
    ),
  ),
  7702796210 => 
  array (
    'id' => 18374733,
    'name' => 'ООО "Флай"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525772180,
    'updated_at' => 1525772181,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34072243,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072243',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7702796210',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34072243,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072243',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18374733',
        'method' => 'get',
      ),
    ),
  ),
  5018186877 => 
  array (
    'id' => 15659295,
    'name' => 'ООО СК "МРС"',
    'responsible_user_id' => 1338850,
    'created_by' => 643314,
    'created_at' => 1497340211,
    'updated_at' => 1525773174,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30318209,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318209',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500605784,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5018186877',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30318209,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30318209',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15659295',
        'method' => 'get',
      ),
    ),
  ),
  3123139520 => 
  array (
    'id' => 18374975,
    'name' => 'ООО "Строительная компания Эталонстрой"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525773333,
    'updated_at' => 1525773333,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34072509,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072509',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123139520',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34072509,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072509',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18374975',
        'method' => 'get',
      ),
    ),
  ),
  7751013831 => 
  array (
    'id' => 18375195,
    'name' => 'ООО «Крист-Медикал»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525774342,
    'updated_at' => 1525774734,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34072737,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072737',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7751013831',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34072737,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072737',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18375195',
        'method' => 'get',
      ),
    ),
  ),
  3123334111 => 
  array (
    'id' => 18375347,
    'name' => 'ООО "Завод АрБет"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525775213,
    'updated_at' => 1525775214,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34072921,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072921',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3123334111',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34072921,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34072921',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18375347',
        'method' => 'get',
      ),
    ),
  ),
  5812006051 => 
  array (
    'id' => 18373839,
    'name' => 'ООО "Городищеавтодорсервис"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525768558,
    'updated_at' => 1525775247,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34071333,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34071333',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5812006051',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34071333,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34071333',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18373839',
        'method' => 'get',
      ),
    ),
  ),
  323369028 => 
  array (
    'id' => 15799037,
    'name' => '"ООО "ГАРАНТ СТРОЙ +""',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1498814801,
    'updated_at' => 1525777161,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30511353,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30511353',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1525777158,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '323369028',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'бурятия плюс 8',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30511353,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30511353',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15799037',
        'method' => 'get',
      ),
    ),
  ),
  7445020205 => 
  array (
    'id' => 18375849,
    'name' => 'ООО "Газприбормонтаж"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525777686,
    'updated_at' => 1525777687,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34073419,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34073419',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7445020205',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34073419,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34073419',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18375849',
        'method' => 'get',
      ),
    ),
  ),
  6451433390 => 
  array (
    'id' => 18376889,
    'name' => 'ООО "Техноцентр"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1525783730,
    'updated_at' => 1525783731,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34074491,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34074491',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6451433390',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34074491,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34074491',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18376889',
        'method' => 'get',
      ),
    ),
  ),
  3314008006 => 
  array (
    'id' => 17222709,
    'name' => 'ООО "ИНТЕРЬЕРНОЕ СТЕКЛО"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1513317477,
    'updated_at' => 1525925150,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32515013,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32515013',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513687659,
    'closest_task_at' => 1525985940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3314008006',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32515013,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32515013',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17222709',
        'method' => 'get',
      ),
    ),
  ),
  3906252757 => 
  array (
    'id' => 17104283,
    'name' => 'ООО  "ПрофЭксперт"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1512120033,
    'updated_at' => 1525925154,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32384719,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32384719',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513159015,
    'closest_task_at' => 1525985940,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3906252757',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32384719,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32384719',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17104283',
        'method' => 'get',
      ),
    ),
  ),
  7017436704 => 
  array (
    'id' => 18082757,
    'name' => 'ООО "КМР"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1522728656,
    'updated_at' => 1525926406,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33704755,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33704755',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7017436704',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33704755,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33704755',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18082757',
        'method' => 'get',
      ),
    ),
  ),
  9102000817 => 
  array (
    'id' => 18273987,
    'name' => '"ТРАСТ ИНСАЙДЕР"',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1524569884,
    'updated_at' => 1525926443,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33949065,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33949065',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1525700504,
    'closest_task_at' => 1526468400,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9102000817',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33949065,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33949065',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18273987',
        'method' => 'get',
      ),
    ),
  ),
  6685054968 => 
  array (
    'id' => 15596841,
    'name' => 'ООО«АйТиБокс»',
    'responsible_user_id' => 1338850,
    'created_by' => 1310841,
    'created_at' => 1496641277,
    'updated_at' => 1525927241,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 30236247,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236247',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1513921277,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6685054968',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => 'Свердловская область(GMT +5)',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 30236247,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=30236247',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=15596841',
        'method' => 'get',
      ),
    ),
  ),
  6318013228 => 
  array (
    'id' => 18149361,
    'name' => 'СКАДО Оснежение и Пылеподавление, ООО',
    'responsible_user_id' => 1277505,
    'created_by' => 1277505,
    'created_at' => 1523434451,
    'updated_at' => 1525927628,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33786963,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33786963',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1523949049,
    'closest_task_at' => 1526504340,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6318013228',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '4',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33786963,
        1 => 33795099,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33786963,33795099',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 142,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18149361',
        'method' => 'get',
      ),
    ),
  ),
  5410072600 => 
  array (
    'id' => 18383011,
    'name' => 'ООО "ГАРАНТСТРОЙСЕРВИС"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525927931,
    'updated_at' => 1525928047,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34079965,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34079965',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5410072600',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34079965,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34079965',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18383011',
        'method' => 'get',
      ),
    ),
  ),
  2724171445 => 
  array (
    'id' => 17706539,
    'name' => 'ООО "ПРЕДПРИЯТИЕ ВОСТОК"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1518494799,
    'updated_at' => 1525928258,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33164549,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33164549',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1518494903,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2724171445',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33164549,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33164549',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=17706539',
        'method' => 'get',
      ),
    ),
  ),
  2807015640 => 
  array (
    'id' => 14936077,
    'name' => 'АО "ДЭП №191"',
    'responsible_user_id' => 1261404,
    'created_by' => 609492,
    'created_at' => 1489982331,
    'updated_at' => 1525929068,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 29315941,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315941',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 1500521586,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '2807015640',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 29315941,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=29315941',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 143,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=14936077',
        'method' => 'get',
      ),
    ),
  ),
  5534020997 => 
  array (
    'id' => 16997145,
    'name' => 'ООО «Стройсервис»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1511241382,
    'updated_at' => 1525929503,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 32247535,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247535',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '5534020997',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 32247535,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=32247535',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=16997145',
        'method' => 'get',
      ),
    ),
  ),
  7202246296 => 
  array (
    'id' => 18383199,
    'name' => 'ООО «Дана-Строй»',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525930290,
    'updated_at' => 1525930290,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34080173,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080173',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7202246296',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34080173,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080173',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18383199',
        'method' => 'get',
      ),
    ),
  ),
  6319196334 => 
  array (
    'id' => 18383295,
    'name' => 'ООО "СамМед"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525931099,
    'updated_at' => 1525931100,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34080251,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080251',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '6319196334',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34080251,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080251',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18383295',
        'method' => 'get',
      ),
    ),
  ),
  3849061834 => 
  array (
    'id' => 18383393,
    'name' => 'ООО "ВЕКТОР М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525932014,
    'updated_at' => 1525932060,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34080359,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080359',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '3849061834',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34080359,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080359',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18383393',
        'method' => 'get',
      ),
    ),
  ),
  7724893672 => 
  array (
    'id' => 18383591,
    'name' => 'ООО "ГИП-Инжиниринг"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1525933608,
    'updated_at' => 1525934084,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 34080585,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080585',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '7724893672',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 34080585,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=34080585',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 12678384,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18383591',
        'method' => 'get',
      ),
    ),
  ),
  9718080168 => 
  array (
    'id' => 18302591,
    'name' => 'ООО "МедИмпульс"',
    'responsible_user_id' => 1310841,
    'created_by' => 1310841,
    'created_at' => 1524813198,
    'updated_at' => 1525934636,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33977101,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977101',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9718080168',
          ),
        ),
        'is_system' => false,
      ),
      1 => 
      array (
        'id' => 529504,
        'name' => 'Часовой пояс',
        'values' => 
        array (
          0 => 
          array (
            'value' => '0',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33977101,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33977101',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18302591',
        'method' => 'get',
      ),
    ),
  ),
  9723042016 => 
  array (
    'id' => 18235517,
    'name' => 'ООО "АРТ-М"',
    'responsible_user_id' => 1338850,
    'created_by' => 1338850,
    'created_at' => 1524202807,
    'updated_at' => 1525935198,
    'account_id' => 9392874,
    'is_deleted' => false,
    'main_contact' => 
    array (
      'id' => 33912491,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912491',
          'method' => 'get',
        ),
      ),
    ),
    'group_id' => 0,
    'company' => 
    array (
    ),
    'closed_at' => 0,
    'closest_task_at' => 0,
    'tags' => 
    array (
    ),
    'custom_fields' => 
    array (
      0 => 
      array (
        'id' => 521956,
        'name' => 'ИНН',
        'values' => 
        array (
          0 => 
          array (
            'value' => '9723042016',
          ),
        ),
        'is_system' => false,
      ),
    ),
    'contacts' => 
    array (
      'id' => 
      array (
        0 => 33912491,
      ),
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/contacts?id=33912491',
          'method' => 'get',
        ),
      ),
    ),
    'status_id' => 9392880,
    'sale' => 0,
    'loss_reason_id' => 0,
    'pipeline' => 
    array (
      'id' => 72673,
      '_links' => 
      array (
        'self' => 
        array (
          'href' => '/api/v2/pipelines?id=72673',
          'method' => 'get',
        ),
      ),
    ),
    '_links' => 
    array (
      'self' => 
      array (
        'href' => '/api/v2/leads?id=18235517',
        'method' => 'get',
      ),
    ),
  ),
);