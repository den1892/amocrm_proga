<?php
$amoApiObj = new AmoAPI();
if (isset($_POST['logout'])){
    $amoApiObj->logout();
    Main::header('auth');
}
if (isset($_POST) && $_POST) {
    $auth = $amoApiObj->auth($_POST);
    Main::setMsg($auth ? 'Успешно' : 'Ключ или пароль не верны', $auth ? 'success' : 'danger');
    Main::header('auth');
}
//208dbbc3b2111e3d16ba4a789ec4a4c3
?>


<div class="col-md-12">
    <?php if (!$amoApiObj->testAuth()) : ?>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label>Логин</label>
                <input class="form-control" name="USER_LOGIN" type="text" value="makarov.m@gip-rf.ru">
            </div>
            <div class="form-group">
                <label>Ключ</label>
                <input class="form-control" name="USER_HASH" type="text" value="">
                
            </div>
            <div class="form-group">
                <button class="btn btn-success">Войти</button>
            </div>

        </form> 
    <?php else : ?>
        <div class="alert alert-info alert-dismissible" role="alert">
            Вы авторизированы
        </div>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <button class="btn btn-success" name="logout" value="1">Выйти</button>
            </div>
        </form>
    <?php endif; ?>
</div>

