<?php
$apiObj = new AmoAPI();
if (!$apiObj->testAuth()) {
    Main::setMsg('Необходимо сначала авторизоваться', 'danger');
    Main::header('auth');
}

$companies = $apiObj->getCompanies(['limit_rows'=>100]);
$fields = array_keys($companies[0]);

?>

<table class="table">
    <thead>
        <tr>
            <?php foreach ($fields as $v) :?>
            <th><?=$v?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($companies as $v) :?>
        <tr>
            <?php foreach ($fields as $v1) :?>
            <td><?= is_array($v[$v1]) ? dump($v[$v1]) : $v[$v1]?></td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>