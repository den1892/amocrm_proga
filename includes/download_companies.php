<?php
$apiObj = new AmoAPI();
if (!$apiObj->testAuth()) {
    Main::setMsg('Необходимо сначала авторизоваться', 'danger');
    Main::header('auth');
}

$companiesFromFile = Main::getArrayFromFile('companies_files');
//dump($companiesFromFile,1);
$companiesFromFileCount = $companiesFromFile ? count($companiesFromFile) : 0;
if (Main::reqGet('del')){
    $del = Main::sendToBackup('companies.php');
    if ($del)
        Main::setMsg ('Успешно');
    else
        Main::setMsg ('Данные для удаления отсутствуют','danger');
    Main::header('download_companies');
}
if (isset($_POST) && $_POST) {
    $companiesFromApi = $apiObj->getLeads($_POST);
    if ($companiesFromApi){
        $saveSession['limit_rows'] = isset($_POST['limit_rows']) ? (int)$_POST['limit_rows'] : 0;
        $saveSession['limit_offset'] = isset($_POST['limit_offset']) ? (int)$_POST['limit_offset'] + $saveSession['limit_rows']  : 0;
        Main::setSession($saveSession, ['limit_rows','limit_offset']);
    }
    $companiesSave=[];
    if ($companiesFromApi) {
        foreach ($companiesFromApi as $v) {
            $companiesSave[Main::getINN($v)] = $v;
        }
        Main::setArrayToFile('companies_files/'.time().'_companies.php', $companiesSave);
        Main::setMsg('Успешно');
    } else
        Main::setMsg('Компании по запросу не найдено','danger');

        if (Main::reqGet('json')){
            if (isset($_SESSION['msg']))
            unset($_SESSION['msg']);
            ob_end_clean();
            echo $companiesFromApi ? 1 : 0;
            die();
        }

    Main::header('download_companies');
}
?>
<div class="col-md-12">
    <div class="alert alert-info alert-dismissible" role="alert">
        Компаний загружено <?= $companiesFromFileCount ?>
    </div>
    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Количество элементов (максимум 500, ограничения апи):</label>
            <input class="form-control" name="limit_rows" type="number" value="<?=Main::getSession('limit_rows')?>">
        </div>
        <div class="form-group">
            <label>С какого элемента начинать:</label>
            <input class="form-control" name="limit_offset" type="number" value="<?=Main::getSession('limit_offset')?>">
        </div>
        <div class="form-group">
            <button class="btn btn-success">Загрузить</button>
   
            <button class="btn btn-primary" type="button" onclick="getAmoInfoAjax();">Загрузить все</button>
        </div>


    </form> 


 

    <a href="/?r=download_companies&del=1">
        <button class="btn btn-danger">
            Очистить загруженные данные
        </button>
    </a>

</div>


<div class="col-md-12" id="loading-parse" style="margin-top: 15px; display: none;">

<div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
           НЕ ЗАКРЫВАЙТЕ ВКЛАДКУ
            </div>

</div>


<div class="col-md-12" id="result-parse" style="margin-top: 15px;">


</div>

<script>

  function getAmoInfoAjax(data){
      if (!data){
          data = {
              'limit_rows' : 500,
              'limit_offset' : 0
          };
      } 
      $('#loading-parse').show();
      $.post('/?r=download_companies&json=1',data,function(response){
          
           if (response == '1'){
              setResultParse(data);
              data.limit_offset = data.limit_offset + 500;
              getAmoInfoAjax(data);
           } else {
              $('#loading-parse').show();
              alert('Загрузка окончена'); 
           }
          // setResultParse(data);
      });

  }

  function setResultParse(data){
    var  html = '<div class="alert alert-info alert-dismissible" role="alert">'+
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
            '<span aria-hidden="true">&times;</span></button>'+
            'Компании '+data.limit_rows+' - '+data.limit_offset+' добавлено!'+
            '</div>';
     $('#result-parse').append(html);       
  }

</script>