<?php
if (!$users = Main::getArrayFromFile('users.php')) {
    Main::setMsg('Необходимо сначала загрузить файл', 'danger');
    Main::header('upload_file');
}

//phpinfo();die();
if (!$companies = Main::getArrayFromFile('companies_files')) {
    Main::setMsg('Необходимо сначала выгрузить список компаний', 'danger');
    Main::header('download_companies');
}
$apiObj = new AmoAPI();

if (!$apiObj->testAuth()) {
    Main::setMsg('Необходимо сначала авторизоваться', 'danger');
    Main::header('auth');
}
if (!$admins = $apiObj->getAdmins()) {
    Main::setMsg('Админы не найдены', 'danger');
    Main::header('download_companies');
}

if (isset($_POST['msg']) && $_POST['msg']) {
    require ROOT_DIR.'/libs/PHPMailer/src/Exception.php';
    require ROOT_DIR.'/libs/PHPMailer/src/PHPMailer.php';
    require ROOT_DIR.'/libs/PHPMailer/src/SMTP.php';
    $sendMsgArray = [];
    foreach ($users as $v) {
        $inn = $v[8];
        if (isset($companies[$inn]) && $admins[$companies[$inn]['responsible_user_id']]) {
            $mail = $admins[$companies[$inn]['responsible_user_id']]['login'];
            $msg = Main::replaceMSG((string)$_POST['msg'], $v);
            $sendMsgArray[$mail] = isset($sendMsgArray[$mail]) ? $sendMsgArray[$mail].'<br><br>'.$msg : $msg;
           // Main::sendMail($mail, $_POST['msg'], $v);
        }
    }
  //  dump($sendMsgArray,1);
    if ($sendMsgArray){
        foreach ($sendMsgArray as $k=>$v)
            Main::sendMail($k, $v);
    }
  //  dump($sendMsgArray,1);
    Main::setMsg('Успешно');
    Main::header('send_message');
}
?>


<form method="post" enctype="multipart/form-data">
    <div class="form-group">
        <textarea style="width:500px; height: 400px;" id="msg-text" name="msg"><?= file_get_contents(ROOT_DIR.'/mail_shab/msg_to_admin.txt')?></textarea>
    </div>
    <div class="form-group">
        <button class="btn btn-success">Разослать письма</button>
    </div>

</form>

<script>

   CKEDITOR.replace('msg-text');

</script>