<?php
if (isset($_FILES['file']['tmp_name']) && is_file($_FILES['file']['tmp_name'])) {

    $XLSObj = (new ParseXlsCsv())->setPath($_FILES['file']['tmp_name']);
    $method = false;
    if (Main::validateFileName($_FILES['file']['name'], ['csv']))
        $method = 'parseCSV';
    else if (Main::validateFileName($_FILES['file']['name'], ['xlsx', 'xls']))
        $method = 'parseXLS';
    $XLSObj->setMethodParse($method);
    $arr = $XLSObj->parseFile();
    if (!$arr) {
        Main::setMsg('Не верный формат или структура файла', 'danger');
        Main::header('upload_file');
    } else {
        Main::setArrayToFile('users.php', $arr);
        Main::setMsg('Успешно!');
        Main::header('send_message');
    }
}
?>
<div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <input name="file" type="file">
        </div>
        <div class="form-group">
            <button class="btn btn-success">Загрузить файл</button>
        </div>

    </form> 
</div>


