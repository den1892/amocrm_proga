<?php
session_start();
ini_set('memory_limit', '512M');
define('ROOT_DIR', substr($_SERVER['DOCUMENT_ROOT'], -1,1) == '/' ? substr($_SERVER['DOCUMENT_ROOT'], 0,-1) : $_SERVER['DOCUMENT_ROOT']);

function dump($var,$kill=false){
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    if ($kill)
        die();
}

$clasesDir = scandir('clases');
foreach ($clasesDir as $v)
    if (is_file('clases/' . $v))
        require_once 'clases/' . $v;
if (Main::reqGet('r') && !is_file('includes/' . (string) Main::reqGet('r') . '.php'))
    $include_file = 'error_404';
else
    $include_file = Main::reqGet('r') ? (string) Main::reqGet('r') : 'index';

$headerMenu = Main::getMenu();
$head = Main::getHeder($include_file);
$msg = Main::getMsg();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>AMO</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="/libs/ckeditor/ckeditor.js"></script>
    </head>

    <body>

    <content style="margin-top: 5px;">
        <!-- Классы navbar и navbar-default (базовые классы меню) -->
        <nav class="navbar navbar-default">
            <!-- Контейнер (определяет ширину Navbar) -->
            <div class="container-fluid">
                <!-- Заголовок -->
                <div class="navbar-header">
                    <!-- Кнопка «Гамбургер» отображается только в мобильном виде (предназначена для открытия основного содержимого Navbar) -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Бренд или название сайта (отображается в левой части меню) -->
                    <a class="navbar-brand" href="/">Главная</a>
                </div>
                <!-- Основная часть меню (может содержать ссылки, формы и другие элементы) -->
                <div class="collapse navbar-collapse" id="navbar-main">
                    <ul class="nav navbar-nav navbar-left hidden-sm hidden-md">

                        <?php if ($headerMenu) : ?>
                            <?php foreach ($headerMenu as $v): ?>
                                <li> <a href="<?= $v['url'] ?>"><?= $v['name'] ?></a></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>

                </div>
            </div>
        </nav>
        <h1><?= $head ?></h1>
        <?php if ($msg) :?>
        <div class="alert alert-<?=$msg['status']?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <?=$msg['msg']?>
        </div>
        <?php endif;?>
        <?php require_once 'includes/' . $include_file . '.php'; ?>
    </content>

</body>
</html>




